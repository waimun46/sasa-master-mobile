import React, { Component } from 'react';
import { AsyncStorage, Image, ScrollView, StyleSheet, Text, View, FlatList, ImageBackground } from 'react-native';
import { Container, Header, Content, Card, CardItem, Body, Thumbnail } from "native-base";
import IconFA from "react-native-vector-icons/FontAwesome";
import { office_data, contact_data } from './data'


class StructureOffice extends Component {

    _renderItem = ({ item }) => {
        return (
            <Body>
                <View style={{ marginBottom: 20 }}>
                    <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                        <Text style={{ width: '20%' }}>No : {item.no}</Text>
                        <Text style={{ width: '80%' }}>{item.company_name}</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={{ width: '20%' }}>{global.t('Phone')} : </Text>
                        <Text style={{ width: '80%' }}>{item.phone}</Text>
                    </View>
                </View>
            </Body>
        )
    }

    render() {

        //console.log(partner[0].gsa, 'dataPartner')

        return (
            <View style={{ flex: 1, backgroundColor: '#fff', }}>
                <ScrollView>
                    <View >

                        {/**********************************  Our_Structure **********************************/}
                        <View style={[styles.topimgwarp, { marginBottom: 10, }]}>
                            <Image source={require('../../../../assets/images/img/st.jpeg')} style={styles.topimg} />
                            <View style={[styles.overlay, { height: 200 }]}></View>
                            <View style={styles.toptextwarp}>
                                <Text style={styles.toptext}>{global.t('Our_Structure')}</Text>
                            </View>
                        </View>

                        <View>
                            <View style={[styles.texttitlewarp, { marginTop: 15 }]}>
                                <View style={styles.texttitle}>
                                    <Text style={styles.textsty}>{global.t('Operating_Structure')}</Text>
                                </View>
                            </View>
                            <View style={styles.line}><Text></Text></View>
                            <View style={{ paddingLeft: 50, paddingRight: 50 }}>
                                <View style={styles.linewarp}>
                                    <View style={[styles.line, { height: 20, }]}><Text></Text></View>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', padding: 5 }}>
                                <View style={styles.ThumbnailWarp}>
                                    <Thumbnail square large source={require('../../../../assets/images/img/service.jpg')}
                                        style={styles.ThumbnailSty} />
                                    <Text style={styles.textcontent}>{global.t('Customer_Agents')}</Text>
                                </View>
                                <View style={styles.ThumbnailWarp}>
                                    <Thumbnail square large source={require('../../../../assets/images/img/marketing.jpg')}
                                        style={styles.ThumbnailSty} />
                                    <Text style={styles.textcontent}>{global.t('Marketing_Team')}</Text>
                                </View>
                                <View style={styles.ThumbnailWarp}>
                                    <Thumbnail square large source={require('../../../../assets/images/img/fbc.jpg')}
                                        style={styles.ThumbnailSty} />
                                    <Text style={styles.textcontent}>{global.t('Customer_FBC')}</Text>
                                </View>
                            </View>
                        </View>

                        {/**********************************  Our_Structure **********************************/}
                        <View style={[styles.topimgwarp, { marginBottom: 30, marginTop: 30 }]}>
                            <Image source={require('../../../../assets/images/img/office.jpg')} style={styles.topimg} />
                            <View style={[styles.overlay, { height: 200 }]}></View>
                            <View style={styles.toptextwarp}>
                                <Text style={styles.toptext}>{global.t('Our_Office')}</Text>
                            </View>
                        </View>

                        <View style={{ padding: 10, }}>
                            <Card>
                                <CardItem header bordered style={{ backgroundColor: '#de2d30' }}>
                                    <Text style={styles.cardtitle}>{global.t('Office_Branch')}</Text>
                                </CardItem>
                                <CardItem bordered>
                                    <FlatList
                                        data={office_data}
                                        renderItem={this._renderItem}
                                        keyExtractor={(item, index) => item.key}
                                    />
                                </CardItem>
                            </Card>
                        </View>

                        <View style={{ padding: 10, marginBottom: 80, }}>
                            <View >
                                <View >
                                    <Image source={contact_data[0].logo} style={styles.logostyle} />
                                </View >
                                <View style={{ marginTop: 10, }}>
                                    <Text style={{ textAlign: 'center' }}>{contact_data[0].company_name}</Text>
                                </View>
                            </View>
                            <View style={{ marginTop: 0, }}>
                                <View style={{ padding: 20, }}>
                                    <Text style={{ textAlign: 'center' }} >{contact_data[0].address}</Text>
                                </View>
                                <View style={{ flexDirection: 'row', }}>
                                    <View style={styles.contentwarp}>
                                        <View style={styles.contentWarpsty}>
                                            <IconFA name="envelope-open" style={[styles.iconsty, { color: '#de2d30' }]} />
                                            <Text style={styles.textcontentsty} >{contact_data[0].email}</Text>
                                        </View>
                                    </View>
                                    <View style={styles.contentwarp}>
                                        <View style={styles.contentWarpsty}>
                                            <IconFA name="facebook-square" style={[styles.iconsty, { color: '#3e5d98' }]} />
                                            <Text style={styles.textcontentsty} >{contact_data[0].fb}</Text>
                                        </View>
                                    </View>
                                    <View style={styles.contentwarp}>
                                        <View style={styles.contentWarpsty}>
                                            <IconFA name="phone-square" style={[styles.iconsty, { color: '#029e74' }]} />
                                            <Text style={styles.textcontentsty} >{contact_data[0].phone}</Text>
                                        </View>
                                    </View>
                                </View>

                            </View>
                        </View>


                    </View>

                </ScrollView>
            </View>
        );
    }
}



export default StructureOffice;

const styles = StyleSheet.create({
    topimgwarp: { height: 200 },
    topimg: { resizeMode: 'cover', width: '100%', height: 200 },
    toptext: { color: '#fff', fontWeight: 'bold', fontSize: 30, textAlign: 'center' },
    toptextwarp: { position: 'absolute', alignItems: 'center', width: '100%', top: 80 },
    overlay: {
        flex: 1, position: 'absolute', left: 0, top: 0, opacity: 0.5, backgroundColor: 'black',
        width: '100%'
    },
    line: {
        borderLeftWidth: 1, borderColor: '#de2d30', height: 30, width: 10, marginLeft: 'auto',
        marginRight: 'auto',
    },
    textsty: { textAlign: 'center', fontSize: 20, color: '#fff', fontWeight: 'bold', marginBottom: 10 },
    ThumbnailSty: { width: '100%', marginBottom: 10, padding: 0 },
    ThumbnailWarp: { width: '33.33%', padding: 5 },
    textcontent: { textAlign: 'center', },
    texttitle: { paddingTop: 10, backgroundColor: '#de2d30', width: '85%', marginLeft: 'auto', marginRight: 'auto' },
    texttitlewarp: { padding: 20, paddingBottom: 0, },
    linewarp: { borderWidth: 1, borderColor: '#de2d30', borderBottomWidth: 0, },
    logostyle: { resizeMode: 'contain', width: '100%', height: 100 },
    contentwarp: { width: '33.33%', padding: 5, },
    contentWarpsty: { backgroundColor: '#eee', padding: 5, paddingTop: 15, paddingBottom: 15, height: 105 },
    iconsty: { fontSize: 20, textAlign: 'center' },
    textcontentsty: { textAlign: 'center', marginTop: 10, justifyContent: 'center' },
    cardtitle: { marginLeft: 'auto', marginRight: 'auto', fontSize: 18, color: '#fff', fontWeight: 'bold', }





})