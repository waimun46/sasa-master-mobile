var HorizontalStatus = {
    rainy: {
        ios: 'ios-rainy',
        android: 'md-rainy',
    },
    cloud: {
        ios: 'ios-cloud',
        android: 'md-cloud'
    },
    sunny: {
        ios: 'ios-sunny',
        android: 'md-sunny'
    },
    thunderstorm: {
        ios: 'ios-thunderstorm',
        android: 'md-thunderstorm'
    }
};

var HorizontalFlatListData = [
    {
        day: 'today', status: HorizontalStatus.rainy, degrees: 57
    },
    {
        day: 'mon', status: HorizontalStatus.cloud, degrees: 42
    },
    {
        day: 'tue', status: HorizontalStatus.sunny, degrees: 55
    },
    {
        day: 'wed', status: HorizontalStatus.sunny, degrees: 56
    },
    {
        day: 'thu', status: HorizontalStatus.rainy, degrees: 56
    },
    {
        day: 'fri', status: HorizontalStatus.cloud, degrees: 56
    },
    {
        day: 'Sat', status: HorizontalStatus.thunderstorm, degrees: 56
    },
    {
        day: 'Sun', status: HorizontalStatus.thunderstorm, degrees: 56
    },
    {
        day: 'mon', status: HorizontalStatus.sunny, degrees: 56
    },
    {
        day: 'tue', status: HorizontalStatus.rainy, degrees: 56
    },
];

export { HorizontalStatus };
export { HorizontalFlatListData };