import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image, Text, Platform } from 'react-native';
import { Actions } from 'react-native-router-flux';
import IconION from "react-native-vector-icons/Ionicons";
//import { HorizontalFlatListData, HorizontalStatus } from './HorizontalFlatListData';



class ForecastCard extends Component {
    render() {

        /************************* Get Weather Api Current Time Display *************************/
        let time;
        let date = new Date(this.props.detail.dt * 1000);
        let hours = date.getHours();
        let minutes = "0" + date.getMinutes();
        time = hours + ':' + minutes.substr(-2);

        return (
            <View style={styles.cardwarpper}>
                <Text style={styles.locationstyle}>{this.props.location}</Text>
                <View style={styles.imgstatuswarp}>
                    <Image
                        style={{ width: 80, height: 45, }}
                        source={{
                            uri: "https://openweathermap.org/img/w/"
                                + this.props.detail.weather[0].icon
                                + ".png"
                        }}
                    />
                </View>
                <Text style={styles.weatherstatus}>{this.props.detail.weather[0].main}</Text>
                <Text style={styles.time}>{time}</Text>
            </View>
        )
    }
}



class WeatherPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            latitude: 0,
            longitude: 0,
            forecast: [],
            error: ''
        };
    }

    componentDidMount() {

        this.getLocation();

        /************************* Alert Current Locations *************************/
        navigator.geolocation.getCurrentPosition(
            // function (position) {
            //   alert("Lat: " + position.coords.latitude + "\nLon: " + position.coords.longitude);
            // },
            function (error) {
                alert(error.message);
            }, {
                enableHighAccuracy: true
                , timeout: 20000
            }
        );
    }

    /************************* Get the current position of the user *************************/
    getLocation() {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                this.setState(
                    (prevState) => ({
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude
                    }), () => { this.getWeather(); }
                );
            },
            (error) => this.setState({ forecast: error.message }),
            { enableHighAccuracy: false, timeout: 20000 },
        );
    }

    /************************* Fetch the weather api display in state *************************/
    getWeather(lat = this.state.latitude, lon = this.state.longitude, API_KEY = '7a522fdd67758b65b2eb6872f9f08d45') {
        let url = `https://api.openweathermap.org/data/2.5/forecast/?lat=${lat}&lon=${lon}&APPID=${API_KEY}&units=metric`
        fetch(url)
            .then(response => response.json())
            .then(data => {
                this.setState((prevState, props) => ({
                    forecast: data
                }));
            })
    }


    render() {
        return (
            <View>
                <View style={{
                    flex: 1, flexDirection: 'column',
                    //marginTop: Platform.OS === 'ios' ? 34 : 0
                }}>
                    <FlatList
                        data={this.state.forecast.list}
                        horizontal={true}
                        keyExtractor={item => item.dt_txt}
                        renderItem={({ item, index }) => {
                            return (
                                <ForecastCard
                                    detail={item}
                                    location={this.state.forecast.city.name}
                                />
                            )
                        }}
                    />
                </View>
            </View>
        );
    }
}


export default WeatherPage;

const styles = StyleSheet.create({
    locationstyle: { fontSize: 12, fontWeight: 'bold', color: '#fff', margin: 10, textTransform: 'uppercase', textAlign: 'center' },
    time: {
        fontSize: 16, color: '#fff',
        ...Platform.select({
            ios: { paddingBottom: 30, },
            android: { paddingBottom: 10, }
        })
    },
    cardwarpper: {
        flex: 1, flexDirection: 'column', alignItems: 'center', width: 100, marginRight: 1,
        backgroundColor: 'black',
    },
    imgstatuswarp: { flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' },
    weatherstatus: { fontSize: 16, color: '#fff', margin: 10 }
})
