import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image, Text, Platform, AsyncStorage } from 'react-native';
import { Container, Header, Content, Accordion, Icon } from "native-base";

import { Actions } from 'react-native-router-flux';
import IconANT from "react-native-vector-icons/AntDesign";
import IconION from "react-native-vector-icons/Ionicons";
import IconFA5 from "react-native-vector-icons/FontAwesome5";
import IconMCI from "react-native-vector-icons/MaterialCommunityIcons";
import { DepartingDetailApi } from '.././../../../../PostApi';

const dataLocal = [
    {
        airline: "Not Available",
    },

];


class FlightPage extends Component {

    constructor(props) {
        super(props)
        this.state = {
            data: [],
            AsyncStorageId: 0
        }
    }

    componentDidMount() {

        /************************************ Get Store Data From AsyncStorage  ***********************************/
        AsyncStorage.getItem('USER_DATA').then(asyncStorageRes => {
            console.log(asyncStorageRes, '----------------AsyncStorage-----FlightPage')
            this.setState({
                AsyncStorageId: asyncStorageRes
            })
            console.log(this.state.AsyncStorageId, '-------------fetchDataApi----FlightPage')

            /************************************ Fetch Api Data  ***********************************/
            DepartingDetailApi(this.state.AsyncStorageId).then((fetchData) => {
                console.log(fetchData, '-----------------fetchData-----FlightPage')
                this.setState({
                    data: fetchData[0].flight_detail
                })
            })

        });


    }

    _renderHeader(item, expanded) {
        return (
            <View style={{ backgroundColor: "white", marginBottom: 20, borderRadius: 5 }}>
                <View style={styles.renderHeaderstyle}>
                    <View style={{ flexDirection: "row" }}>
                        <View style={{ width: '25%' }}>
                            <View style={{ width: '100%' }}>
                                <Image
                                    source={{ uri: item.flight_airline_logo }}
                                    style={styles.logoImage}
                                />
                            </View>
                        </View>

                        <View style={{ width: '75%' }}>
                            <View style={{ width: '100%', flexDirection: "row" }}>
                                <Text style={styles.renderHeaderTitle}>
                                    {" "}{item.flight_airline === null ? dataLocal[0].airline : item.flight_airline}
                                </Text>

                                {expanded
                                    ? <IconANT style={styles.iconStyle} name="caretup" />
                                    : <IconANT style={styles.iconStyle} name="caretdown" />
                                }
                            </View>

                            <View style={{ flexDirection: "row", width: '100%' }}>
                                <View style={{ width: '50%', padding: 10 }}>
                                    <Text style={styles.headertext}>{global.t('Flight_Date')}:</Text>
                                    <Text style={{ marginTop: 5 }}>{item.flight_date}</Text>
                                    {/* <Text style={styles.headertext}>{item.time}</Text> */}
                                </View>
                                <View style={{ width: '50%', padding: 10 }}>
                                    <Text style={styles.headertext}>{global.t('Flight_Time')}:</Text>
                                    <Text style={{ marginTop: 5 }}>{item.flight_time}</Text>
                                    {/* <Text style={styles.headertext}>Arr: <Text>{item.etd_eta}</Text></Text> */}
                                    {/* <Text style={styles.headertext}>{item.time}</Text> */}
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        );
    }

    _renderContent(item) {
        return (
            <View style={styles.textInnerWarp}>
                <View style={{ flexDirection: "row" }}>
                    <View style={{ width: '50%', borderRightWidth: 1, borderRightColor: '#e9ecef', flexDirection: 'row', padding: 10 }}>
                        {/* <Text style={styles.innertitle}>{item.sectorf}</Text> */}
                        <Icon name='ios-jet' style={styles.textinnericon} />
                        <Text style={styles.innertitle}>{item.flight_sector}</Text>
                    </View>
                    <View style={{ width: '50%', padding: 10, }}>
                        <Text style={styles.textBottomTitle}>{global.t('Flight_Number')}</Text>
                        <Text style={styles.textBottom}>{item.flight_num}</Text>
                    </View>
                    {/* <View style={{ width: '35%', padding: 10 }}>
                        <Text style={styles.textBottomTitle}>{global.t('Transit')}</Text>
                        <Text style={styles.textBottom}>{item.transit}</Text>
                    </View> */}
                </View>
            </View>
        );
    }


    render() {
        const { data } = this.state;
        console.log(data, '-----------render data')
        return (
            <View style={{ backgroundColor: '#e9ecef', flex: 1 }}>
                <View style={{ position: 'relative', }}>
                    <Image
                        source={require('../../../../assets/images/img/wordmap.jpg')}
                        style={styles.TopImage}
                    />
                    {/* <View style={[styles.overlay, { height: 250}]}></View> */}
                </View>
                <View style={styles.AccordionWarp}>
                    <Accordion
                        dataArray={data}
                        animation={true}
                        expanded={true}
                        renderHeader={this._renderHeader}
                        renderContent={this._renderContent}
                        style={{ borderWidth: 0 }}
                    />
                </View>
            </View>
        );
    }
}


export default FlightPage;

const styles = StyleSheet.create({
    TopImage: { width: "100%", height: 230, resizeMode: 'cover', flex: 1, },
    AccordionWarp: { marginTop: -30, width: '90%', marginLeft: 'auto', marginRight: 'auto' },
    renderHeaderstyle: { flexDirection: "row", padding: 10, justifyContent: "space-between", alignItems: "center", },
    renderHeaderTitle: { fontWeight: "600", textAlign: 'left', width: '80%', fontSize: 18, color: '#de2d30' },
    iconStyle: { fontSize: 18, color: '#de2d30', marginLeft: 30 },
    logoImage: { width: "100%", height: 60, resizeMode: 'cover', marginTop: 10, textAlign: 'center', },
    headertext: { color: '#999' },
    textBottom: { textAlign: 'center', fontSize: 15, },
    textBottomTitle: { textAlign: 'center', fontSize: 14, marginBottom: 5, color: '#999' },
    textinnericon: { textAlign: 'center', fontSize: 35, color: '#de2d30', width: '20%' },
    textInnerWarp: { backgroundColor: "white", padding: 10, marginTop: -18, marginBottom: 10, borderRadius: 5 },
    innertitle: { textAlign: 'center', fontWeight: "bold", fontSize: 16, textTransform: 'uppercase', marginTop: 10, width: '80%' },
    overlay: { flex: 1, position: 'absolute', left: 0, top: 0, opacity: 0.3, backgroundColor: 'black', width: '100%' },


})
