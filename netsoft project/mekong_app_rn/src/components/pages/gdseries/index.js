import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button, Image, ScrollView, TouchableOpacity, Dimensions, FlatList, ImageBackground, AsyncStorage } from 'react-native';
import { Container, Header, Content, Tab, Tabs } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { DestinationApi } from '../../../../PostApi';
import HTML from 'react-native-render-html';
import { IGNORED_TAGS } from 'react-native-render-html/src/HTMLUtils'

const numColumns = 3;

const dataImg = [
  {
    "imgrp": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRw5WrlkUQT9BMeXPL4NVAwuD1n9hqczs__ff8Uv8XJnPfau98e"
  }
]

class GdSeries extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      typedata: [],
      typeimg: [],
      dataU: []
    }
  }

  componentDidMount() {
    DestinationApi('2').then((fetchData) => {
      //console.log(fetchData, 'fetch');
      AsyncStorage.setItem('TYPE_ID', '2');
      this.setState({
        data: fetchData[0].country_list,
        typedata: fetchData[0].type_description,
        typeimg: fetchData[0]
      })
    })
  }

  /************************* Render Item in FlatList *************************/
  renderItem = ({ item }) => {
    //console.log(dataImg, 'GDPremium------------imgrp-------item');
    return (
      <TouchableOpacity onPress={() => Actions.countryinfor({ data: item })} style={{ width: '33.33%', }}>
        <View>
          <ImageBackground source={{ uri: item.pic_url === "" ? dataImg[0].imgrp : item.pic_url }} style={styles.item}>
            <Text style={styles.TextImage}>{item.title}</Text>
          </ImageBackground>
        </View>
      </TouchableOpacity>
    )
  }

  render() {

    const htmlContent = this.state.typedata;
    const { data, typeimg } = this.state;
    //console.log(this.state.data, 'country_list');

    const unique = this.state.dataU;
    data.map(x => unique.filter(a => a.country_list == x.destination_list).length > 0 ? null : unique.push(x));
    console.log(unique, 'unique');


    return (
      <View style={{ backgroundColor: 'white', flex: 1 }}>
        <ScrollView>
          <View >
            <Image source={{ uri: typeimg.type_pic }} style={styles.PremiumImage} />
          </View>

          <View>
            <Tabs style={{ backgroundColor: 'white' }}
              //locked
              tabBarUnderlineStyle={{ backgroundColor: "#de2d30" }}
            >
              <Tab
                heading={global.t('Information')}
                tabStyle={{ backgroundColor: "white" }}
                textStyle={{ color: '#ccc', }}
                activeTextStyle={{ color: '#de2d30' }}
                activeTabStyle={{ backgroundColor: "white", }}
                tabContainerStyle={{ backgroundColor: "#de2d30" }}

              >

                <HTML html={htmlContent}
                  imagesInitialDimensions={{ width: '60%', height: 200 }}
                  tagsStyles={{
                    p: { width: '100%' },
                    h2: { padding: 20, fontSize: 16, marginBottom: -30 },
                    em: { fontSize: 30, },
                  }}
                  ignoredStyles={['font-family', 'display',]}
                  ignoredTags={[...IGNORED_TAGS, 'iframe', 'img',]}
                />

              </Tab>

              <Tab heading={global.t('Country')}
                tabStyle={{ backgroundColor: "white" }}
                textStyle={{ color: '#ccc', }}
                activeTextStyle={{ color: '#de2d30' }}
                activeTabStyle={{ backgroundColor: "white", }}
                tabContainerStyle={{ backgroundColor: "#de2d30" }}
              >
                <FlatList
                  data={unique}
                  renderItem={this.renderItem}
                  numColumns={numColumns}
                  style={styles.Flatcontainer}
                />
              </Tab>

            </Tabs>
          </View>
        </ScrollView>
      </View>

    );
  }
}



export default GdSeries;

const styles = StyleSheet.create({
  PremiumImage: { width: "100%", resizeMode: 'cover', height: 180, },
  Flatcontainer: {
    flex: 1,
    marginVertical: 10,
  },
  item: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    margin: 2,
    height: Dimensions.get('window').width / numColumns,
  },
  TextImage: {
    fontSize: 18, color: 'white', fontWeight: 'bold', textShadowOffset: { width: 2, height: 2 },
    textShadowRadius: 2, textShadowColor: '#000', textAlign: 'center'
  }

})