/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import { Flex, NoticeBar, Icon, List, Card } from '@ant-design/react-native';
import React, { Component } from 'react';
import { Dimensions, Image, StatusBar, StyleSheet, Text, View, ScrollView, Alert } from 'react-native';
import Carousel from 'react-native-looped-carousel';
import { WebView } from 'react-native-webview';
import Video from 'react-native-video';
import MediaControls, { PLAYER_STATES } from 'react-native-media-controls';
// import Geolocation from '@react-native-community/geolocation';
import Geolocation from 'react-native-geolocation-service';


import logo from './assets/logo/logo.jpg';
import sun from './assets/logo/sun.png';
import up from './assets/logo/up.png';
import down from './assets/logo/down.png';
import img01 from './assets/01.jpg';
import img02 from './assets/02.jpg';
import img1 from './assets/500/01.jpg';
import img2 from './assets/500/02.jpg';
import qr from './assets/qr.jpeg'

import video01 from './assets/video/video01.mp4';
import video02 from './assets/video/video02.mp4'

const { width, height } = Dimensions.get('window');
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
const DEVICE_WIDTH = Dimensions.get('window').width;

const dataImg = [
  {
    img: require('./assets/01.jpg'),
  },
  {
    img: require('./assets/02.jpg'),
  }
]



class App extends Component {
  scrollRef = React.createRef();

  constructor(props) {
    super(props);
    this.state = {
      size: { width, height },
      currentTime: 0,
      duration: 0,
      isFullScreen: false,
      isLoading: true,
      paused: false,
      playerState: PLAYER_STATES.PLAYING,
      screenType: 'content',
      bunnyVideo: false,
      songVideo: false,
      loop: true,
      timer: 7000,
      pageLenght: '',
      imageSlide: true,
      selectIndex: 0,
      isBunnyPaused: false,
      isSongPaused: false,
      songPlay: true,
      bunnyPlay: true,
      currentDate: '',
      latitude: 0,
      longitude: 0,
      error: null,
    };
  }

  /************************************************ componentDidMount *************************************************/
  async componentDidMount() {
    // this.getLocation();
    this.getCurrentDate();
    this.onPageLoop();

    // Geolocation.getCurrentPosition(
    //   (position) => {
    //     this.setState(({
    //       latitude: position.coords.latitude,
    //       longitude: position.coords.longitude
    //     }), () => {}
    //     );
    //   },
    //   (error) => {
    //     console.error(error);
    //   },
    //   { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 },
    // );

    if(Platform.OS === 'ios'){
      this.getCurrentLocation();
    }else{
      try {
       const granted = await PermissionsAndroid.request(
         PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
         {
           title: 'Device current location permission',
           message:
             'Allow app to get your current location',
           buttonNeutral: 'Ask Me Later',
           buttonNegative: 'Cancel',
           buttonPositive: 'OK',
         },
       );
       if (granted === PermissionsAndroid.RESULTS.GRANTED) {
         this.getCurrentLocation();
       } else {
         console.log('Location permission denied');
       }
     } catch (err) {
       console.warn(err);
     }
    }

    // setTimeout(() => {
    //   this.setState({
    //     loop: false,
    //     imageSlide: false,
    //     bunnyVideo: true,
    //   })
    // }, 14000)


    /************************************************ setInterval for scrollView slider *************************************************/
    this.interval = setInterval(() => {
      this.setState(prev => ({
        selectIndex: prev.selectIndex === dataImg.length - 1 ? 0 : prev.selectIndex + 1
      }),
        () => {
          this.scrollRef.current.scrollTo({
            animated: true,
            y: 0,
            x: DEVICE_WIDTH * this.state.selectIndex
          })
        })
    }, 3000)
  }


  getCurrentLocation(){
    Geolocation.requestAuthorization();
    Geolocation.getCurrentPosition(
       (position) => {
           console.log(position);
       },
       (error) => {
         console.log("map error: ",error);
           console.log(error.code, error.message);
       },
       { enableHighAccuracy: false, timeout: 15000, maximumAge: 10000 }
   );
  }



  /************************************************ stopSlider for scrollView slider *************************************************/
  stopSlider = () => {
    clearInterval(this.interval);
  }

  /************************************************ setSelectIndex for scrollView slider *************************************************/
  setSelectIndex = event => {
    const viewSize = event.nativeEvent.layoutMeasurement.width;
    const contentOffset = event.nativeEvent.contentOffset.x;
    const selectIndex = Math.floor(contentOffset / viewSize);
    this.setState({
      selectIndex
    })
  }

  getLocation() {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        this.setState(({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude
        }), () => {}
        );
      },
      (error) => this.setState({ forecast: error.message }),
      { enableHighAccuracy: true, timeout: 500000 },
    );
  }

  /************************************************ getCurrentDate *************************************************/
  getCurrentDate() {
    let date = new Date().getDate();
    let month = new Date().getMonth() + 1;
    let year = new Date().getFullYear();
    let d = new Date();
    let weekday = new Array(7);
    weekday[0] = "Sun";
    weekday[1] = "Mon";
    weekday[2] = "Tue";
    weekday[3] = "Wed";
    weekday[4] = "Thu";
    weekday[5] = "Fri";
    weekday[6] = "Sat";
    let day = weekday[d.getDay()];
    this.setState({
      currentDate: day + ' ' + date + '-' + month + '-' + year,
    });
  }








  /************************************************ bunnyEnd for video end *************************************************/
  bunnyEnd = () => {
    console.log('end bunny');
    this.setState({
      bunnyVideo: false,
      songVideo: true,
      songPlay: true,
    })
  }

  /************************************************ onPageLoop *************************************************/
  onPageLoop = (pages) => {
    console.log(pages, 'p')
    this.setState({
      pageLenght: pages
    })
  }

  // looperImage = () => {
  //   setTimeout(() => {
  //     this.setState({
  //       loop: false,
  //       bunnyVideo: true,
  //       // isLoading: true
  //     })
  //   }, 14000)

  //   setTimeout(() => {
  //     this.setState({
  //       loop: false,
  //       songVideo: true,
  //     })
  //   }, 600000)
  // }


  render() {
    const { currentDate, latitude, longitude, } = this.state;
    //console.log(this.state.duration, 'duration')
    //console.log(this.state.bunnyTrigleEnd, 'bunnyTrigleEnd')
    console.log('latitude', latitude)
    console.log('longitude', longitude)


    return (
      <View style={{ position: 'relative', flex: 1, }}>

        <StatusBar hidden={true} />

        {/******************************** header *********************************/}

        <View style={{ width: '100%', marginBottom: 10, height: 130 }}>

          <View style={{ flexDirection: 'row' }}>
            <View style={{ width: '37.5%', marginTop: 10 }}>
              <View style={{ backgroundColor: '#000', width: '90%', padding: 5, }}>
                <Text style={{ color: '#fff', fontSize: 20, fontWeight: 'bold' }}>ADVERTISE</Text>
              </View>
              <View style={{ marginTop: 3, paddingLeft: 5 }}>
                <Text style={{ color: '#000', fontSize: 20, fontWeight: 'bold' }}>HERE</Text>
              </View>
              <Card style={{ borderWidth: 0 }}>
                <Card.Header
                  title={<View>
                    <Text style={{ color: 'blue', fontWeight: 'bold' }}>Jessie</Text>
                    <Text style={{ color: 'blue', }}>+60 10-806 8808</Text>
                  </View>}
                  thumbStyle={{ width: 50, height: 50 }}
                  thumb='http://infosasa.com/wp-content/uploads/2015/01/QR-code.png'
                  style={{ marginLeft: 5, paddingRight: 0, paddingVertical: 0, }}
                />
              </Card>
              {/* <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start' }}>
                <View style={{ width: '40%', justifyContent: 'flex-start', alignItems: 'flex-start' }}>
                  <Image source={qr} style={{ width: '100%', height: 100, resizeMode: 'contain', alignItems: 'flex-start' }} />
                </View>
                <View style={{ width: '60%' }}>
                  <Text style={{ color: 'blue', fontSize: 17, fontWeight: 'bold' }}>Jessie</Text>
                  <Text style={{ color: 'blue', fontSize: 17, fontWeight: 'bold' }}>+60 10-806 8808</Text>
                </View>
              </View> */}
            </View>

            <View style={{ width: '25%', height: 120, backgroundColor: '#000', position: 'relative', }}>
              <Image source={logo} style={{ width: '100%', height: 100, position: 'absolute', bottom: 0 }} />
            </View>

            <View style={{ width: '37.5%', }} >
              <View style={{ width: '95%', }}>

                <View style={{ width: '100%', paddingTop: 10, paddingBottom: 10 }}>
                  <Text style={{ textAlign: 'right', }}>{currentDate}</Text>
                </View>

                <View style={{ flexDirection: 'row', justifyContent: 'flex-end', width: '100%', }}>

                  <View style={{ justifyContent: 'center', width: '50%', alignItems: 'center' }}>
                    <Image source={sun} style={{ width: 75, height: 75, textAlign: 'center', }} />
                  </View>

                  <View style={{ paddingLeft: 5 }}>
                    <View><Text style={{ fontSize: 35, textAlign: 'center', }}>28°</Text></View>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', paddingRight: 10 }}>
                      <Text style={{ textAlign: 'center' }}>
                        <Image source={up} style={{ width: 5, height: 13, opacity: .4 }} /> 32
                      </Text>
                      <Text style={{ textAlign: 'center' }}>
                        <Image source={down} style={{ width: 5, height: 13, opacity: .4 }} /> 18
                    </Text>
                    </View>
                  </View>

                </View>

              </View>

            </View>

          </View>
        </View>



        {/******************************** Carousel *********************************/}
        {
          this.state.imageSlide === true ? (
            <View>
              <ScrollView
                horizontal={true}
                pagingEnabled
                onMomentumScrollEnd={this.setSelectIndex}
                ref={this.scrollRef}
              >
                <View style={{ flex: 1, flexDirection: 'row', flexWrap: 'wrap' }}>
                  {
                    dataImg.map((item, index) => {
                      return (
                        <View style={{ width: this.state.size.width, height: this.state.size.height - 210 }}>
                          <Image source={item.img} style={{ width: '100%', height: '100%', resizeMode: 'stretch', }} />
                        </View>
                      )
                    })
                  }
                </View>
              </ScrollView>
              {/* <Carousel
                delay={this.state.timer}
                style={{ width: this.state.size.width, height: this.state.size.height - 210 }}
                onAnimateNextPage={(pages) => this.onPageLoop(pages)}
                pageInfo={false}
                bullets={false}
                autoplay
                bulletsContainerStyle={{ bottom: 30 }}
                bulletStyle={{ borderColor: '#ccc', }}
                chosenBulletStyle={{ backgroundColor: '#ccc', }}
                isLooped={this.state.loop}
              >
                {/* <View style={{ width: this.state.size.width, height: this.state.size.height - 210 }}></View> 

                {
                  dataImg.map((item) => {
                    return (
                      <View style={{ width: this.state.size.width, height: this.state.size.height - 210 }}>
                        <Image
                          source={item.img}
                          style={{ width: '100%', height: '100%', resizeMode: 'stretch', }}
                        />
                      </View>
                    )
                  })
                }
              </Carousel> */}
            </View>
          ) : (
              null
            )
        }




        {/******************************** video *********************************/}


        <View style={styles.container}>
          {
            this.state.bunnyVideo === true ? (
              <Video
                onEnd={this.bunnyEnd}
                resizeMode='contain'
                source={video01}
                style={styles.mediaPlayer}
                volume={1}
                paused={this.state.isBunnyPaused}
                autoplay={this.state.bunnyPlay}
              />
            ) : (
                null
              )
          }
          {
            this.state.songVideo === true ? (
              <Video
                resizeMode='contain'
                autoplay={this.state.songPlay}
                paused={this.state.isSongPaused}
                source={video02}
                style={styles.mediaPlayer}
                volume={1}
              />
            ) : (
                null
              )
          }
          <MediaControls
            duration={this.state.duration}
            isLoading={this.state.isLoading}
            mainColor="#333"
            //onFullScreen={this.onFullScreen}
            onPaused={this.onPaused}
            onReplay={this.onReplay}
            onSeek={this.onSeek}
            onSeeking={this.onSeeking}
            playerState={this.state.playerState}
            progress={this.state.currentTime}
          //toolbar={this.renderToolbar()}
          />
        </View>


        {/******************************** NoticeBar *********************************/}
        <View style={{ position: 'absolute', bottom: 0, width: '100%', height: 50, }}>
          <NoticeBar
            onPress={() => alert('click')}
            marqueeProps={{ loop: true, leading: 800, trailing: 800, fps: 150, style: { fontSize: 23, color: 'yellow', } }}
            style={{ height: 50, backgroundColor: '#000', color: 'yellow', }}
            // icon={false}
            icon={null}
          >
            Important Announcement: The arrival time of incomes and transfers of Yu 'E Bao will be
            delayed during National Day.
          </NoticeBar>
        </View>
      </View>
    )
  }

}


const styles = StyleSheet.create({
  container: {
    flex: 1,

  },
  toolbar: {
    marginTop: 30,
    backgroundColor: 'white',
    padding: 10,
    borderRadius: 5,
  },
  mediaPlayer: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: 'black',

  },

  bannerwarp: { paddingVertical: 3, position: 'relative' },
  bannertextwarp: { position: 'absolute', top: 10, flexDirection: 'row', padding: 10, },
  bannerSty: { width: '100%', height: '100%', resizeMode: 'contain', },

});



export default App;
