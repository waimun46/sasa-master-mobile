/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { useState, useEffect, useRef } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  YellowBox,
  ActivityIndicator,
} from 'react-native';
import { Button, NoticeBar } from '@ant-design/react-native';
import AsyncStorage from '@react-native-community/async-storage';
import LottieView from 'lottie-react-native';

import DeviceInfo from 'react-native-device-info';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

import cloudys from './assets/lottie/cloudy.json';
import clears from './assets/lottie/clear.json';
import mists from './assets/lottie/mist.json';
import rains from './assets/lottie/rain.json';
import snows from './assets/lottie/snow.json';
import storms from './assets/lottie/storm.json';
import windys from './assets/lottie/windy.json';

import { useFetchApi } from './src/module/fetchApi';
import { MediaDisplay } from './src/components/media';
import { Marquee } from './src/components/marquee';
import { Information } from './src/components/infor';

YellowBox.ignoreWarnings(['Remote debugger']);

const App = () => {
  const scrollRef = useRef();
  const [getUUID, setGetUUID] = useState(DeviceInfo.getUniqueId());
  console.log('getUUID------App', getUUID)
  
  const { latitude,longitude, isLoading } = useFetchApi(`https://www.ktic.com.my/lookHere/api/media_list_landscape.php?security_code=36BBA69CBEA56FBFE09E17B31C26D57B&uid=${getUUID}`);


  return (
    <View style={styles.container}>
      <StatusBar hidden={true} />
      <View style={{ flexDirection: 'row', flex: 1 }}>
        {/********************************************* Information **********************************************/}
        <View style={{ width: '15%', backgroundColor: '#fff' }}>
          <Information getUUID={getUUID} isLoading={isLoading} latitude={latitude} longitude={longitude} />
        </View>
        {/********************************************* Media Display **********************************************/}
        <View style={{ width: '85%' }}>
          <MediaDisplay getUUID={getUUID} scrollRef={scrollRef} />
          <Marquee getUUID={getUUID} />
        </View>
      </View>
    </View>
  );
}


const styles = StyleSheet.create({
  container: { position: 'relative', flex: 1, },


});

export default App;
