import React, { useLayoutEffect, useRef, useState } from "react";
import { StyleSheet, View, Text, ScrollView, Image, ActivityIndicator, Dimensions } from 'react-native';


export const Test = () => {

  const [show, doShow] = useState({
    itemOne: false,
    itemTwo: false,
    itemThree: false
  });

  const ourRef = useRef(null),
        anotherRef = useRef(null),
        refThree = useRef(null);

  useLayoutEffect(() => {
    const topPos = element => element.getBoundingClientRect().top;
   //added to reduce redundancy
    const div1Pos = topPos(ourRef.current),
          div2Pos = topPos(anotherRef.current),
          div3Pos = topPos(refThree.current);

    const onScroll = () => {
      const scrollPos = window.scrollY + window.innerHeight;
      if (div1Pos < scrollPos) {
        doShow(state => ({ ...state, itemOne: true }));
      } else if (div2Pos < scrollPos) {
        doShow(state => ({ ...state, itemTwo: true }));
      } else if (div3Pos < scrollPos) {
        doShow(state => ({ ...state, itemThree: true }));
      }
    };

    window.addEventListener("scroll", onScroll);
    return () => window.removeEventListener("scroll", onScroll);
  }, []);
  return (
    <>
       <View animate={show.itemThree} ref={refThree} style={styles.test} />
       <View animate={show.itemTwo} ref={anotherRef} />
       <View animate={show.itemOne} ref={ourRef} />
    </>
  );

  }

  const styles = StyleSheet.create({
    test: {}
  
  });