import React, { useState, useEffect, useRef, useCallback, useLayoutEffect, } from 'react';
import Video from 'react-native-video';
import { StyleSheet, View, Text, ScrollView, Image, ActivityIndicator, Dimensions, Animated, Easing, } from 'react-native';
import { useFetchApi } from '../module/fetchApi';
import AsyncStorage from '@react-native-community/async-storage';

const DEVICE_WIDTH = Dimensions.get('window').width;

export const MediaDisplay = ({ getUUID }) => {
  const scrollRef = React.createRef()

  // const { data, isLoading, } = useFetchApi(`https://www.ktic.com.my/lookHere/api/media_list_landscape.php?security_code=36BBA69CBEA56FBFE09E17B31C26D57B&uid=${getUUID}`);

  // console.log('data---length', data.length)
  // console.log('data---MediaDisplay', data)

  const [width, setWidth] = useState(Dimensions.get('window').width);
  const [height, setHeight] = useState(Dimensions.get('window').height);

  // const [selectIndex, setSelectIndex] = useState(0);
  // const [videoList, setVideoList] = useState([]);



  const [state, setState] = useState({
    // selectIndex: 0,
    // total_run: 0,
    videoList: [],
    catcherr: null,
    isLoading: true,
    progress: new Animated.Value(0),

  })
  const [data, setData] = useState([]);
  const [selectIndex, setSelectIndex] = useState(0);
  const [total_run, setTotalRun] = useState(0);

  //////////////////////////////////////////// selectIndexFuc ////////////////////////////////////////////
  const selectIndexFuc = (event) => {
    const viewSize = event.nativeEvent.layoutMeasurement.width;
    const contentOffset = event.nativeEvent.contentOffset.x;
    const selectIndex = Math.floor(contentOffset / viewSize);
    setSelectIndex(selectIndex)

  }
  //console.log('videoList-----MediaDisplay', state.videoList);
  console.log('scrollRef-----', scrollRef.current);

  // useLayoutEffect(() => {


  // }, [])


  //////////////////////////////////////////// useEffect ////////////////////////////////////////////
  useEffect(() => {
    nextSlide()
    fetchMediaList();
    runAnimation();
  }, [])


  //////////////////////////////////////////// fetchMediaList ////////////////////////////////////////////
  const fetchMediaList = () => {
    return fetch(
      `https://www.ktic.com.my/lookHere/api/media_list_landscape.php?security_code=36BBA69CBEA56FBFE09E17B31C26D57B&uid=${getUUID}`,
    )
      .then(response => response.json())
      .then(fetchData => {
        //console.log('fetchData', fetchData);
        AsyncStorage.setItem('STORE_LIST', JSON.stringify(fetchData));
        setState({
          isLoading: false,
        })
        setData(fetchData)
        setSelectIndex(selectIndex - 1)
      })
      .catch((error) => {
        console.log(error, 'response----------error');
        setState({
          catcherr: error.message,
        })
      });

  };

  


  //////////////////////////////////////////// nextSlide ////////////////////////////////////////////
  const nextSlide = () => {
    console.log('nextSlide run');
    console.log(total_run, "total_run")

    clearTimeout(interval())

    if (data && data.lenght === 0) {
      ;
    } else {
      console.log('nextSlide run before setState');
      setSelectIndex(selectIndex === data.lenght - 1 ? 0 : selectIndex + 1);
      setTotalRun(total_run + 1);
      if (scrollRef.current === null) {
        ;
      } else {
        scrollRef.current.scrollTo({
          animated: true,
          y: 0,
          x: DEVICE_WIDTH * selectIndex,
        })
        interval()
        // if (data[selectIndex].md_type === 2) {
        //   ;
        // } else {
        //   interval()
        // }
      }


    }

  }

  //////////////////////////////////////////// interval ////////////////////////////////////////////
  const interval = () => {
    setTimeout(() => {
      nextSlide()
    }, 3000)
    console.log('interval=============', data[selectIndex]);

  }







  // useEffect(() => {
  //   setTimeout(() => {
  //     console.log('useEffect-----MediaDisplay', );
  //   }, 3000)

  // }, []);



  // useEffect(
  //   () => {
  //     setState(state.total_run + 1,state.selectIndex ===  data && data.lenght - 1 ? 0 : state.selectIndex + 1,)
  //     // let interval = setTimeout(() => nextSlide(), 3000)

  //     // this will clear Timeout when component unmont like in willComponentUnmount
  //     return () => {
  //       clearTimeout(interval)
  //     }
  //   },
  //   [] 
  // )


  // const nextSlide = () => {
  //   console.log('nextSlide run-----------');
  //   // clearTimeout(interval())

  //   if (data && data.lenght === 0) {
  //     ;
  //   } else {
  //     console.log('nextSlide run before setState');
  //     setState({
  //       total_run: state.total_run + 1,
  //       selectIndex: state.selectIndex === data && data.lenght - 1 ? 0 : state.selectIndex + 1
  //     }, () => {
  //       console.log('nextSlide run-----------selectIndex', state.selectIndex);
  //       scrollRef.current.scrollTo({
  //         animated: true,
  //         y: 0,
  //         x: DEVICE_WIDTH * state.selectIndex,
  //       })
  //       if (data[state.selectIndex].md_type === 2) {
  //         ;
  //       } else {
  //         interval()

  //       }
  //     })
  //   }

  // // }






  //////////////////////////////////////////// videoEnd ////////////////////////////////////////////
  const videoEnd = (index) => {
    console.log('videoEnd=============');
    setTimeout(() => {
      nextSlide()
    }, 100);
    setTimeout(() => {
      state.videoList[index].seek(0);
    }, 500);
  }

  //////////////////////////////////////////// runAnimation ////////////////////////////////////////////
  const runAnimation = () => {
    Animated.timing(state.progress, {
      toValue: 1,
      duration: 3000,
      easing: Easing.linear,
    }).start(() => {
      runAnimation();
    });
  }

  //////////////////////////////////////////// setAnim ////////////////////////////////////////////
  const setAnim = anim => {
    this.anim = anim;
  };






  console.log('scrollRef----', scrollRef)


  console.log('data----', data)

  return (

    <View style={{ width: '100%' }}>
      {
        state.isLoading ?
          <View style={styles.indicatorWarp}>
            <ActivityIndicator size="large" color="#de2d30" />
          </View>
          :
          <ScrollView
            horizontal={true}
            pagingEnabled
            automaticallyAdjustContentInsets={true}
            onMomentumScrollEnd={selectIndexFuc}
            ref={scrollRef}
            contentContainerStyle={{ flexGrow: 1, }}
            style={{ height: '100%', width: width }}
          >
            <View style={styles.sliderWarp}>
              {
                data && data.map((item, index) => {
                  return (
                    item.md_type === 2 ?
                      <View style={[styles.container, { width: width, height: height - 50 }]} key={index}>
                        <Video
                          ref={(ref) => {
                            state.videoList[index] = ref
                          }}
                          onEnd={videoEnd.bind(this, index)}
                          resizeMode='contain'
                          source={{ uri: item.md_videoPath }}
                          style={[styles.mediaPlayer, { width: width + 90 }]}
                          volume={1}
                          paused={state.selectIndex === index ? false : true}
                        // rate={15.0}
                        />
                      </View>
                      :
                      <View style={{ width: width - 50, height: height - 50, }} key={index}>
                        <Image source={{ uri: item.md_imagePath }} style={styles.sliderImg} />
                      </View>
                  )
                })
              }
            </View>
          </ScrollView>
      }
    </View>
  )
}


const styles = StyleSheet.create({
  mediaPlayer: { position: 'absolute', top: 0, left: 0, bottom: 0, right: 0, backgroundColor: 'black', },
  indicatorWarp: { flex: 1, justifyContent: 'center', alignItems: 'center', },
  sliderWarp: { flex: 1, flexDirection: 'row', flexWrap: 'wrap', },
  sliderImg: { width: '100%', height: '100%', resizeMode: 'stretch', },


});