import React, { useEffect, useState, useRef } from 'react';
import AsyncStorage from '@react-native-community/async-storage';



export const useFetchApi = (url) => {

  const [state, setState] = useState({ data: null, isLoading: true, latitude: '', longitude: '', });

  useEffect(() => {

    setState({ data: state.data, isLoading: true, latitude: state.latitude, longitude: state.longitude, });

    fetch(url).then((res) => res.json())
      .then((fetchData) => {
        // console.log(JSON.stringify(fetchData));
        setState({
          data: fetchData,
          latitude: fetchData[0].dev_latitude,
          longitude: fetchData[0].dev_longitude,
          isLoading: false,
        })
      })
      .catch(err => console.log('failed : ' + err.message));
  }, [url, state.latitude, state.longitude])

  // console.log('useFetchApi------------url', url)

  return state;
}




