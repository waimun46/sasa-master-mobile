import React, { useEffect, useRef, useState, useReducer } from 'react';
import { uuid } from './getUUID';

export const fetchMediaList = () => {
  const [state, setState] = useState({
    videoList: [],
    catcherr: null,
    isLoading: true,

  })
  const [data, setData] = useState([]);
  const [selectIndex, setSelectIndex] = useState(0);

  return fetch(
    `https://www.ktic.com.my/lookHere/api/media_list_landscape.php?security_code=36BBA69CBEA56FBFE09E17B31C26D57B&uid=${uuid}`,
  )
    .then(response => response.json())
    .then(fetchData => {
      //console.log('fetchData', fetchData);
      setState({
        isLoading: false,
      })
      setData(fetchData)
      setSelectIndex(selectIndex - 1)
    })
    .catch((error) => {
      console.log(error, 'response----------error');
      setState({
        catcherr: error.message,
      })
    });

};
