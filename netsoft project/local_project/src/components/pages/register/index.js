import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, ImageBackground, Image, Dimensions, Platform, } from 'react-native';
import { Container, Header, Content, Button, Text, Icon, Form, Item, Input, Label, Picker } from 'native-base';
import { Actions } from 'react-native-router-flux';
import IconANT from "react-native-vector-icons/AntDesign";




class RegisterScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selected: "key1"
        };
    }
    onValueChange(value: string) {
        this.setState({
            selected: value
        });
    }



    render() {
        return (
            <ImageBackground
                source={require('../../../assets/images/background/redbg.png')}
                style={styles.Container}
            >
             
                <View style={styles.Container}>
                
                    <Image
                        source={require('../../../assets/images/logotext.png')}
                        style={{ width: '100%', height: 100, resizeMode: 'contain' }}
                    />
          
                    <Form style={{ width: '60%', marginTop: 50, marginBottom: 50 }}>
                    <ScrollView >
                        <Item style={{ borderColor: 'white' }}>
                            <Input placeholder="Underline Textbox" placeholderTextColor='white' />
                        </Item>
                        <Item >
                            <Item style={{ width: '30%', }}>
                                <Input
                                    placeholder="+60"
                                    placeholderTextColor='white'
                                    style={styles.InputStyle}
                                //multiline={true}

                                />
                            </Item>
                            <Item style={{ width: '70%', borderColor: 'white' }}>
                                <Input
                                    placeholder="121234567"
                                    placeholderTextColor='white'
                                    style={styles.InputStyle2}
                                />
                            </Item>
                        </Item>
                        <Item style={{ borderColor: 'white', borderColor: 'white', height: 60 }}>
                            <Input placeholder="Underline Textbox" placeholderTextColor='white' />
                        </Item>
                        <Item style={{ borderColor: 'white', borderColor: 'white', height: 60 }}>
                            <Input placeholder="Underline Textbox" placeholderTextColor='white' />
                        </Item>

                        <Item >
                            <Item style={{ width: '60%', }}>
                                <Input
                                    placeholder="Kuala Lumpur"
                                    placeholderTextColor='white'
                                    style={styles.InputStyle}
                                //multiline={true}

                                />
                            </Item>
                            <Item style={{ width: '40%', borderColor: 'white' }}>
                                <Input
                                    placeholder="PostCode"
                                    placeholderTextColor='white'
                                    style={styles.InputStyle3}
                                />
                            </Item>
                        </Item>
                  
                            <Picker
                                mode="dropdown"
                                iosHeader="Select your SIM"
                                iosIcon={<Icon name="arrow-dropdown-circle" />}
              
                                selectedValue={this.state.selected}
                                onValueChange={this.onValueChange.bind(this)}
                                
                            >
                                <Picker.Item label="Wallet" value="key0" />
                                <Picker.Item label="ATM Card" value="key1" />
                                <Picker.Item label="Debit Card" value="key2" />
                                <Picker.Item label="Credit Card" value="key3" />
                                <Picker.Item label="Net Banking" value="key4" />
                                
                            </Picker>
                            
                     
                        </ScrollView>
                    </Form>
                   


                    <Button block style={styles.bottomStyle} >
                        <Text style={styles.BottomText} uppercase={false}>Create Account</Text>
                    </Button>
                   
                </View>
               
            </ImageBackground >
        );
    }
}


export default RegisterScreen;

const styles = StyleSheet.create({
    Container: { flex: 1, justifyContent: "center", alignItems: 'center', width: '100%', height: '100%', position: 'relative' },
    headerText: { fontSize: 24, textAlign: "center", margin: 10, color: "white", fontWeight: "bold", marginTop: 50 },
    bottomStyle: {
        position: 'absolute', bottom: 0, width: '100%', backgroundColor: '#fc564e',
        ...Platform.select({
            ios: { height: 70 },
            android: { height: 60 }
        })
    },
    BottomText: { fontSize: 16 },
    InputStyle: { borderRightColor: 'white', borderRightWidth: 1, height: 60 },
    InputStyle2: { paddingLeft: 15, height: 60, },
    InputStyle3: { height: 60, },
    pickerStyle: { position: 'absolute', bottom: 0, left: 0, right: 0 }



})
