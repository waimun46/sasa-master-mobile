import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image } from 'react-native';
import { Container, Header, Content, Form, Item, Input, Label, Text } from 'native-base';

import { Actions } from 'react-native-router-flux';
import IconANT from "react-native-vector-icons/AntDesign";




class Genaral extends Component {

    render(item) {
        return (

            <View style={styles.GenaralContainer}>
                <Form>
                    <View style={styles.TopTextContainer}>
                        <View style={{ width: '50%', paddingLeft: 10 }}>
                            <Text style={styles.TopTextTitle}>Genaral</Text>
                        </View>
                        <View style={styles.TopIconContainer}>
                            <IconANT name="form" size={25} style={styles.TopIconStyle} />
                        </View>
                    </View>
                    <Item stackedLabel style={{ marginBottom: 30, borderColor: '#959da5' }}>
                        <Label style={styles.LabelStyle}>Email</Label>
                        <Input placeholder="gdapp@gmail.com" style={styles.InputStyle} />


                    </Item>
                    <Item stackedLabel style={{ borderColor: '#959da5' }}>
                        <Label style={styles.LabelStyle}>Password</Label>
                        <Input placeholder="********" secureTextEntry/>
                    </Item>
                </Form>
            </View>


        );
    }
}


export default Genaral;

const styles = StyleSheet.create({
    GenaralContainer: { backgroundColor: 'white', paddingTop: 20, paddingBottom: 30, paddingLeft: 30, paddingRight: 50, },
    TopTextContainer: { flex: 1, flexDirection: 'row', width: '100%', },
    TopTextTitle: { fontSize: 30, color: '#de2d30', fontWeight: 'bold', marginBottom: 10 },
    TopIconContainer: { width: '50%', alignItems: 'flex-end', },
    TopIconStyle: { color: '#de2d30', },
    LabelStyle: { textTransform: 'uppercase', color: '#9fa6ad', fontWeight: 'bold', },
    InputStyle: { color: 'black' },
    BottomContainer: { backgroundColor: '#f2f2f2', paddingTop: 20, paddingBottom: 100, paddingLeft: 20, paddingRight: 30 },
    AddressContainer: { flex: 1, flexDirection: 'row', width: '100%', justifyContent: 'space-between', },
    AddressItem: { width: '40%', marginTop: 30, borderColor: '#959da5' }
})
