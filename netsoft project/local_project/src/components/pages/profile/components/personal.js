import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image } from 'react-native';
import { Container, Header, Content, Form, Item, Input, Label, Text } from 'native-base';

import { Actions } from 'react-native-router-flux';
import IconANT from "react-native-vector-icons/AntDesign";




class Personal extends Component {

    render(item) {
        return (

            <View style={styles.BottomContainer} >
                <Form>
                    <View style={styles.TopTextContainer}>
                        <View style={{ width: '50%', paddingLeft: 10 }}>
                            <Text style={styles.TopTextTitle}>Personal</Text>
                        </View>
                        <View style={styles.TopIconContainer}>
                            <IconANT name="form" size={25} style={styles.TopIconStyle} />
                        </View>
                    </View>
                    <Item stackedLabel style={{ marginBottom: 30, borderColor: '#959da5' }}>
                        <Label style={styles.LabelStyle}>Phone</Label>
                        <Input placeholder="0123456789" style={styles.InputStyle} />


                    </Item>
                    <Item stackedLabel style={{ borderColor: '#959da5' }}>
                        <Label style={styles.LabelStyle}>Address</Label>
                        <Input placeholder="Puchong" />
                    </Item>
                    <View style={styles.AddressContainer}>
                        <Item stackedLabel style={styles.AddressItem}>
                            <Label style={styles.LabelStyle}>City</Label>
                            <Input placeholder="Selangor" />
                        </Item>
                        <Item stackedLabel style={styles.AddressItem}>
                            <Label style={styles.LabelStyle}>Postcode</Label>
                            <Input placeholder="47170" />
                        </Item>
                    </View>
                    <Item stackedLabel style={{ marginTop: 30, borderColor: '#959da5' }}>
                        <Label style={styles.LabelStyle}>State</Label>
                        <Input placeholder="Selangor" />
                    </Item>
                </Form>
            </View>


        );
    }
}


export default Personal;

const styles = StyleSheet.create({
    GenaralContainer: { backgroundColor: 'white', paddingTop: 20, paddingBottom: 30, paddingLeft: 30, paddingRight: 50, },
    TopTextContainer: { flex: 1, flexDirection: 'row', width: '100%', },
    TopTextTitle: { fontSize: 30, color: '#de2d30', fontWeight: 'bold', marginBottom: 10 },
    TopIconContainer: { width: '50%', alignItems: 'flex-end', },
    TopIconStyle: { color: '#de2d30', },
    LabelStyle: { textTransform: 'uppercase', color: '#9fa6ad', fontWeight: 'bold', },
    InputStyle: { color: 'black' },
    BottomContainer: { backgroundColor: '#e9ecef', paddingTop: 20, paddingBottom: 100, paddingLeft: 30, paddingRight: 50 },
    AddressContainer: { flex: 1, flexDirection: 'row', width: '100%', justifyContent: 'space-between', },
    AddressItem: { width: '40%', marginTop: 30, borderColor: '#959da5' }
})
