import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image } from 'react-native';
import { Container, Header, Content, Form, Item, Input, Label, Text } from 'native-base';

import { Actions } from 'react-native-router-flux';
import IconANT from "react-native-vector-icons/AntDesign";

import Genaral from './components/genaral';
import Personal from './components/personal';




class ProfilePage extends Component {

    render(item) {
        return (
            <View style={{ backgroundColor: 'white' }}>
                <ScrollView>
                    <Genaral />
                    <Personal />
                </ScrollView>
            </View>
        );
    }
}


export default ProfilePage;

const styles = StyleSheet.create({
   
})
