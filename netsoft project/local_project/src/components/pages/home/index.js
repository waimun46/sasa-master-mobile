import React, { Component } from 'react';
import { StyleSheet, View, ScrollView } from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';
import { Actions } from 'react-native-router-flux';

import TimeLocations from './components/TimeLocations';
import LocationsSelect from './components/LocationsSelect';
import Upcoming from './components/Upcoming'


class HomesScreen extends Component {

    render() {
        return (
            <View style={styles.HotpickContainer}>
                <TimeLocations />
                <ScrollView >
                    <LocationsSelect />
                    <View style={{padding: 7, backgroundColor: '#e9ecef'}}></View>
                  <Upcoming/>
                </ScrollView>
            </View>
        );
    }
}


export default HomesScreen;

const styles = StyleSheet.create({
    HotpickContainer: {
        backgroundColor: 'white',
    }
})
