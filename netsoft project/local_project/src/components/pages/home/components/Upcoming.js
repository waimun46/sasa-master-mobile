import React, { Component } from 'react';
import { Platform, StyleSheet, View, FlatList } from 'react-native';
import { Container, Header, Content, List, ListItem, Thumbnail, Text, Left, Body, Right, Button } from 'native-base';
import IconANT from "react-native-vector-icons/AntDesign";

const data = [
  { title: '6D4N Korea Wonder', price: '4444', ststus: 'Avalible', data: 3, month: 'Jun' },
  { title: '7D5N Melbourne 8 Distinctive', price: '43334', ststus: 'Full', data: 15, month: 'Aug' },
  { title: '9D1N Dubai', price: '22222', ststus: 'Avalible', data: 7, month: 'Dec' },
]

class Upcoming extends Component {

  keyExtractor = (item, index) => item.key;

  renderItem = ({ item, index }) => {
    return (
      <ListItem thumbnail>
        <Left>
          <Thumbnail style={{ position: 'relative' }} 
            square source={require('../../../../assets/images/background/iconbgred.png')} 
          />
          <View style={styles.ThumbnailTextContainer}>
            <Text style={styles.ThumbnailText1}>{item.data}</Text>
            <Text style={styles.ThumbnailText2}>{item.month}</Text>
          </View>
        </Left>
        <Body>
          <Text numberOfLines={1} style={styles.TitleList}>{item.title}</Text>
          <Text note numberOfLines={1} >Price: <Text style={{fontWeight: 'bold',color: '#606c77'}}>RM{item.price}</Text></Text>
          <Text note numberOfLines={1}>Status: <Text style={{ color: 'green' ,fontWeight: 'bold'}}>{item.ststus}</Text></Text>
        </Body>
      </ListItem>
    )
  }

  render() {
    return (
      <View >
        <Text style={styles.TitleText}>Upcoming Departure</Text>
        <Content style={{ paddingBottom: 100 }}>
          <List >
            <FlatList
              data={data}
              renderItem={this.renderItem}
              keyExtractor={this.keyExtractor}
            >
            </FlatList>
          </List>
          <Text style={styles.TextMore}> More <IconANT name='right' size={15}/></Text>
        </Content>
      </View>
    );
  }
}

export default Upcoming;

const styles = StyleSheet.create({
  TitleText: {
    textAlign: 'left', fontSize: 23, padding: 10, fontWeight: 'bold', borderWidth: 0.5,
    ...Platform.select({
      ios: { color: 'black', borderColor: '#c9c9c9', },
      android: { color: 'black', borderColor: '#c9c9c9', }
    })
  },
  ThumbnailTextContainer: { position: 'absolute', width: '100%', textAlign: 'center', },
  ThumbnailText1: {
    color: 'white', fontWeight: 'bold', fontSize: 20,
    ...Platform.select({
      ios: { paddingTop: 5, },
      android: { paddingTop: 3, }
    }),

  },
  ThumbnailText2: {
    color: 'white', fontWeight: 'bold',
    ...Platform.select({
      android: { marginTop: -4 }
    }),
  },
  TitleList: {fontSize: 18, fontWeight: 'bold', marginBottom: 5 ,color: '#606c77'},
  TextMore: {paddingBottom: 20, textAlign: 'right', padding: 20, marginBottom: 10, fontSize: 20, color: '#de2d30' }

})

