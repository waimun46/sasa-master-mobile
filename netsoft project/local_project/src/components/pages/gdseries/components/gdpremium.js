import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image } from 'react-native';
import { Container, Header, Content, Tab, Tabs, Text } from 'native-base';
import { Actions } from 'react-native-router-flux';
import CountryData from './components/countrydata';


class GDPremium extends Component {


    render() {
        return (
            <View style={{ backgroundColor: 'white' }}>
                <ScrollView>
                    <View >
                        <Image source={require('../../../../assets/images/img/gd02.png')} style={styles.PremiumImage} />
                    </View>
                    <View>

                        <Tabs style={{ backgroundColor: 'white' }} tabBarUnderlineStyle={{ backgroundColor: "#de2d30" }}>
                            <Tab
                                heading="GD Premium"
                                tabStyle={{ backgroundColor: "white" }}
                                activeTabStyle={{ backgroundColor: "white", }}
                                tabContainerStyle={{ backgroundColor: "#de2d30" }}
                                textStyle={{ color: '#606c77' }}
                                activeTextStyle={{ color: 'red' }}
                            >
                            
                                <Text style={{ padding: 20, textAlign: 'center' }}>
                                    Lorem Ipsum is simply dummy text of the printing and
                                    typesetting industry. Lorem Ipsum has been the industry's
                                    standard dummy text ever since the 1500s, when an unknown printer
                                    took a galley of type and scrambled it to make a type specimen book.
                                    It has survived not only five centuries, but also the leap into electronic
                                    typesetting, remaining essentially unchanged. It was popularised in
                                    the 1960s with the release of Letraset sheets
                                    containing Lorem Ipsum passages, and more recently with
                                    desktop publishing software like Aldus PageMaker including
                                    versions of Lorem Ipsum Aldus PageMaker including .
                                </Text>

                            </Tab>
                            <Tab heading="Country"
                                tabStyle={{ backgroundColor: "white" }}
                                activeTabStyle={{ backgroundColor: "white", }}
                                textStyle={{ color: '#606c77' }}
                                activeTextStyle={{ color: '#de2d30' }}
                            >

                                <CountryData />

                            </Tab>

                        </Tabs>
                    </View>
                </ScrollView>
            </View>
        );
    }
}


export default GDPremium;

const styles = StyleSheet.create({
    PremiumImage: { width: "100%", resizeMode: 'cover', height: 180, }
})
