import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, FlatList, Image } from 'react-native';
import { Container, Header, Content, List, ListItem, Thumbnail, Text, Left, Body, Right, Button } from 'native-base';
import { Actions } from 'react-native-router-flux';
import IconFA5 from "react-native-vector-icons/FontAwesome5";


keyExtractor = (item) => item.key;

const data = [
    { key: 'MH-1090523-NZF', caption: '11D8N NEW Zealand North And South Island', price: '34334', },
    { key: 'RH-6040524-NZE', caption: '21C7F NEW Zealand North And South Island', price: '22234', },
    { key: 'KH-4000563-FZF', caption: '31F4N NEW Zealand North And South Island', price: '11324', },
]


class CountryInforList extends Component {

    renderItem = ({ item }) => {
        return (
            <List style={{ backgroundColor: 'white' }}>
                <ListItem thumbnail>
                    <Left>
                        <IconFA5 name="file-download" size={50} style={styles.IconColor} />
                    </Left>
                    <Body>
                        <Text numberOfLines={1} style={styles.TextTitle}>{item.key}</Text>
                        <Text note style={{ marginTop: 5 }}>Caption: {item.caption}</Text>
                        <Text note numberOfLines={1} style={styles.TextPrice}>Price: RM{item.price}</Text>
                    </Body>
                </ListItem>
            </List>
        )
    }


    render() {
        return (
            <Content>
                <FlatList
                    data={data}
                    renderItem={this.renderItem}
                    keyExtractor={this.keyExtractor}
                />

            </Content>

        );
    }
}


export default CountryInforList;

const styles = StyleSheet.create({
    IconColor: { color: '#de2d30' },
    TextTitle: { color: '#de2d30', fontWeight: 'bold' },
    TextPrice: { marginTop: 5, color: '#00953b' }
})
