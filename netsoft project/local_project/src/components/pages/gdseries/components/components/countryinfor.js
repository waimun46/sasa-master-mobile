import React, { Component } from 'react';
import { Platform, FlatList, StyleSheet, Text, View, Image, ScrollView, TouchableOpacity, Dimensions } from 'react-native';


import { Actions } from 'react-native-router-flux';
import IconION from "react-native-vector-icons/Ionicons";

const data = [
    { key: 'China01' }, { key: 'China02' }, { key: 'China03' }, { key: 'China04' }, { key: 'China05' }, { key: 'China06' }
];

const formatData = (data, numColumns) => {
    const numberOfFullRows = Math.floor(data.length / numColumns);
    return data;
};


const numColumns = 2;

class CountryInfor extends Component {

    renderItem = ({ item }) => {
        return (

            <View style={styles.ImageWarpper}>
            <TouchableOpacity onPress={() => Actions.countryinforlist()}>
                <View style={styles.ImageInner} >
                    <View style={styles.ImageInnerWarp}>
                        <Image source={require('../../../../../assets/images/img/co01.png')}
                            style={styles.ImageStyle} />
                    </View>
                    <View style={{ borderRadius: 5, }}>
                        <Text style={styles.TextImage}>{item.key}</Text>
                    </View>
                </View>
                </TouchableOpacity>
            </View>


        )
    }

    render() {
        return (
            <View style={{ backgroundColor: 'white', }}>
                <ScrollView>
                    <View>
                        <Image
                            source={require('../../../../../assets/images/img/co02.png')}
                            style={styles.CountryInforImage}
                        />
                    </View>
                    <View style={{ padding: 20 }} >
                        <IconION name="ios-briefcase" size={35} style={styles.IconStyle} />
                        <Text style={styles.TitleText}>Trip to China</Text>
                        <Text style={styles.ContentText}>
                            Lorem Ipsum is simply dummy text of the printing and
                            typesetting industry. Lorem Ipsum has been the industry's
                            standard dummy text ever since the 1500s, when an unknown printer
                            took a galley of type and scrambled it to make a type specimen book.
                        </Text>
                    </View>
                    <View style={{ backgroundColor: '#e9ecef', padding: 20, }}>
                        <FlatList
                            data={formatData(data, numColumns)}
                            renderItem={this.renderItem}
                            numColumns={numColumns}
                            style={styles.FlatContainer}
                        />
                    </View>
                </ScrollView>
            </View>

        );
    }
}



export default CountryInfor;

const styles = StyleSheet.create({
    CountryInforImage: { width: "100%", resizeMode: 'cover', height: 180, },
    IconStyle: {
        textAlign: 'center',
        ...Platform.select({
            ios: { color: 'black' },
            android: { color: 'black' }
        })
    },
    TitleText: {
        textAlign: 'center', fontSize: 18, marginTop: 5, fontWeight: 'bold',
        ...Platform.select({
            ios: { color: 'black' },
            android: { color: 'black' }
        })
    },
    ContentText: { textAlign: 'center', marginTop: 20, paddingLeft: 10, paddingRight: 10, color: '#606c77' },
    FlatContainer: { flex: 1, marginVertical: 10, },
    ImageWarpper: { width: '50%', paddingLeft: 10, paddingRight: 10, marginBottom: 20, },
    ImageInner: { backgroundColor: 'white', borderRadius: 10, },
    ImageInnerWarp: { borderTopRightRadius: 10, borderTopLeftRadius: 10, overflow: 'hidden', },
    ImageStyle: { height: 150, width: null, flex: 1, },
    TextImage: { textAlign: 'center', padding: 10, fontWeight: 'bold',
        ...Platform.select({
            ios: { color: 'black' },
            android: { color: 'black' }
        })
    },

})