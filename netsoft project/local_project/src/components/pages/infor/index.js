import React, { Component } from 'react';
import { Container, Header, Content, List, ListItem, Icon, Left, Body, Right, Switch, Button } from 'native-base';
import { Platform, StyleSheet, Text, View, Image, StatusBar } from 'react-native';
import { Actions } from 'react-native-router-flux';
import IconSIM from "react-native-vector-icons/SimpleLineIcons";


class InforScreen extends Component {
  render() {
    return (
      <Container style={{ backgroundColor: '#e9ecef' }}>
        <Content>
          <ListItem icon style={{ marginTop: 30, backgroundColor: 'white', marginLeft: 0 }}>
            <Left>
              <IconSIM active name="briefcase" style={{ color: '#de2d30', fontSize: 25, marginLeft: 15 }} />
            </Left>
            <Body><Text>Company Profile</Text></Body>
            <Right><Icon active name="arrow-forward" /></Right>
          </ListItem>

          <ListItem icon style={{ backgroundColor: 'white', marginLeft: 0 }}>
            <Left>
              <IconSIM active name="social-google" style={{ color: '#de2d30', fontSize: 25, marginLeft: 15 }} />
            </Left>
            <Body><Text>GD Profile</Text></Body>
            <Right><Icon active name="arrow-forward" /></Right>
          </ListItem>

          <ListItem icon style={{ marginTop: 30, backgroundColor: 'white', marginLeft: 0 }}>
            <Left>
              <IconSIM active name="lock" style={{ color: '#de2d30', fontSize: 25, marginLeft: 15 }} />
            </Left>
            <Body><Text>Privacy Policy</Text></Body>
            <Right><Icon active name="arrow-forward" /></Right>
          </ListItem>
          <ListItem icon style={{ backgroundColor: 'white', marginLeft: 0 }}>
            <Left>
              <IconSIM active name="note" style={{ color: '#de2d30', fontSize: 25, marginLeft: 15 }} />
            </Left>
            <Body><Text>Terms and Condition</Text></Body>
            <Right><Icon active name="arrow-forward" /></Right>
          </ListItem>
          <ListItem icon style={{ marginTop: 30, backgroundColor: 'white', marginLeft: 0 }}>
            <Left>
              <IconSIM active name="bubbles" style={{ color: '#de2d30', fontSize: 25, marginLeft: 15 }} />
            </Left>
            <Body><Text>Feedback</Text></Body>
            <Right><Icon active name="arrow-forward" /></Right>
          </ListItem>
        </Content>
      </Container>

    );
  }
}



export default InforScreen;

const styles = StyleSheet.create({


})