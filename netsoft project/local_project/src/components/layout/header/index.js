import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import { Container, Header, Left, Body, Right, Icon, Button, Title, StyleProvider, Text , Content} from 'native-base'
import getTheme from '../../../themes/components';
import appColor from '../../../themes/variables/appColor'
import IconMCI from "react-native-vector-icons/MaterialCommunityIcons";
import { Actions } from 'react-native-router-flux'; 




class HeaderScreen extends Component {
  render() {
    return (
      
      <StyleProvider style={getTheme(appColor)}>
      <Container>
      <Header>
        <Left style={{flex:1}}>
          <Button transparent >
            <Icon name='left' />
          </Button>
        </Left>
        <Body style={{flex:1, alignItems:'center'}}>
          <Title style={{textAlign:'center'}}>Header</Title>
        </Body>
       
        <Right style={{flex:1}}>
          <Button transparent>
            <Icon name='infocirlceo'/>
          </Button>
      
          <Button transparent onPress={() => Actions.footer()} >
            <IconMCI name='account' style={{fontSize: 30, color: 'white'}}/>
          </Button>
        </Right>
        </Header>
    </Container>
    
    </StyleProvider>
     
    );
  }
}


export default HeaderScreen;


