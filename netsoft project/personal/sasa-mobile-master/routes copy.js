import React, { Component } from 'react';
import { Text, TouchableOpacity, Image, View, Alert, Dimensions, ImageBackground, ScrollView } from 'react-native';
import { createStackNavigator, createBottomTabNavigator, DrawerNavigator, TabNavigator, createDrawerNavigator, SideMenu } from 'react-navigation';
import SplashScreen from './src/screens/SplashScreen';
import LoginScreen from './src/screens/LoginScreen';
import ForgotPassword from './src/screens/ForgotPassword';
import NewPassword from './src/screens/NewPassword';
import ChangePassWord1 from './src/screens/ChangePassWord1';
import PhoneVerification from './src/screens/PhoneVerification';
import PhoneVerification1 from './src/screens/PhoneVerification1';
import HomeScreen from './src/screens/TabBarScreen/HomeScreen';
import Evoucher from './src/screens/TabBarScreen/Evoucher';
import MapScreen from './src/screens/TabBarScreen/MapScreen'
import MyCartScreen from './src/screens/TabBarScreen/MyCartScreen'
import SignUp from './src/screens/SignUp';
import Activate from './src/screens/Activate';
import Activate1 from './src/screens/Activate1';
import BillNoScreen from './src/screens/BillNoScreen';
import ReadyToCollect from './src/screens/ReadyToCollect';
import HomeMenu from './src/screens/SideMenuScreen/HomeMenu'
import Help from './src/screens/SideMenuScreen/Help';
import PromotionScreen from './src/screens/SideMenuScreen/PromotionsScreen';
import TrickNdTreat from './src/screens/SideMenuScreen/TrickNdTreat';
import InboxScreen from './src/screens/SideMenuScreen/InboxScreen';
import MyAccountScreen from './src/screens/MyAccountScreen';
import CustomHeader from './src/screens/TabBarScreen/CustomeHeader';
import OnlinePurchaseTransaction from './src/screens/OnlinePurchaseTransaction';
import ActivationScreen from './src/screens/ActivationScreen'
import BirthdayVoucher from './src/screens/BirthdayVoucher';
import WelcomeGiftVoucher from './src/screens/TabBarScreen/WelcomeGiftVoucher';
import EvoucherInStore from './src/screens/TabBarScreen/EvoucherInStore';
import EvoucherInApp from './src/screens/TabBarScreen/EvoucherInApp';
import CheckoutScreen from './src/screens/TabBarScreen/CheckoutScreen';
import SelectStoreScreen from './src/screens/TabBarScreen/SelectStoreScreen';
import MakeUp from './src/screens/MakeUp';
import CheckoutPaymentScreen from './src/screens/TabBarScreen/CheckoutPaymentScreen'
import CheckoutPaymentSuccess from './src/screens/TabBarScreen/CheckoutPaymentSuccess'
import BackArrow from './src/components/BackArrow';
import ProductDetailsScreen from './src/screens/TabBarScreen/ProductDetailsScreen';
import LikedProducts from './src/screens/TabBarScreen/LikedProducts';
import ItemsPictures from './src/screens/TabBarScreen/ItemsPictures';
import PromotionsList from './src/screens/TabBarScreen/PromotionsList';
import StoreLocationListView from './src/screens/TabBarScreen/StoreLocationListView';
import StoreLocationListView1 from './src/screens/TabBarScreen/StoreLocationListView1';
import Location from './src/screens/TabBarScreen/Location';
import SelectedCatogiries from './src/screens/TabBarScreen/SelectedCatogiries';
import MyTransaction from './src/screens/MyTransaction';
import StorePurchase from './src/screens/StorePurchase';
import MyReviewsScreen from './src/screens/MyReviewsScreen';
import AddReview from './src/screens/TabBarScreen/AddReview';
import ProductList from './src/screens/TabBarScreen/ProductList'
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon1 from 'react-native-vector-icons/SimpleLineIcons';
import Icon2 from 'react-native-vector-icons/EvilIcons';
import Icon3 from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon4 from 'react-native-vector-icons/FontAwesome5';
import Slider from './src/components/slider';
import RewardsScreen from './src/screens/SideMenuScreen/RewardsScreen';
import NavigationDrawer from './src/screens/SideMenuScreen/NavigationDrawer';
import ExchangeAndReturn from './src/screens/SideMenuScreen/ExchangeAndReturn'
import Privacy from './src/screens/SideMenuScreen/Privacy'
import Terms from './src/screens/SideMenuScreen/Terms'
import CompanyProfile from './src/screens/SideMenuScreen/CompanyProfile'
import Membership from './src/screens/SideMenuScreen/Membership'
import SideDrawer from './src/components/SideDrawer';
import { Badge } from 'react-native-elements'
import MyCard from './src/components/myCard'
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
const Tab = TabNavigator({



  EvoucherInApp: {
    screen: EvoucherInApp,
    navigationOptions: (navigation) => ({
      tabBarLabel: 'MOBILE APP',
      headerLeft: null,

    })
  },
  EvoucherInStore: {
    screen: EvoucherInStore,
    navigationOptions: (navigation) => ({

      tabBarLabel: 'IN STORE',
      labelStyle: {
        fontSize: 20,

      },

      headerLeft: null,

    })
  },




},
  {
    tabBarOptions: {
      style: {
        backgroundColor: '#fff',
        height: 50,


        borderTopColor: 'transparent',
        borderTopWidth: 1,
        paddingRight: 10,
        paddingLeft: 10,
        borderTopWidth: 1,

      },
      labelStyle: { fontSize: 16, fontFamily: 'futura-pt-medium', },
      indicatorStyle: {
        backgroundColor: '#ff4da6', // color of the indicator
        height: 4,
      },
      activeTintColor: 'black',
      inactiveTintColor: 'gray',
      activeBackgroundColor: 'white',
      inactiveBackgroundColor: '#F6F6F6',
      tabBarPosition: "top",



    }
  }


)
const Tab1 = TabNavigator({



  StoreLocationListView1: {
    screen: StoreLocationListView1,
    navigationOptions: (navigation) => ({
      tabBarLabel: 'LIST VIEW',
      headerLeft: null,

    })
  },
  MapScreen: {
    screen: MapScreen,
    navigationOptions: (navigation) => ({

      tabBarLabel: 'MAP VIEW',
      headerLeft: null,

    })
  },




},
  {
    tabBarOptions: {
      style: {
        backgroundColor: '#fff',
        height: 50,
        borderTopColor: 'transparent',

        borderTopWidth: 1,
        paddingRight: 10,
        paddingLeft: 10,
        borderTopWidth: 1,

      },
      labelStyle: { fontSize: 16, fontFamily: 'futura-pt-medium' },
      indicatorStyle: {
        backgroundColor: '#ff4da6', // color of the indicator
        height: 4,
      },
      activeTintColor: 'black',
      inactiveTintColor: 'gray',
      activeBackgroundColor: 'white',
      inactiveBackgroundColor: '#F6F6F6',
      tabBarPosition: "top",



    }
  }


)
const Tab2 = TabNavigator({



  MyTransaction: {
    screen: MyTransaction,
    navigationOptions: (navigation) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          'futura-pt-medium'
      }}>Trick and Treat..</Text>,
      tabBarLabel: 'ONLINE PURCHASES',
      headerLeft: null,

    })
  },
  StorePurchase: {
    screen: StorePurchase,
    navigationOptions: (navigation) => ({

      tabBarLabel: 'IN-STORE PURCHASES',
      headerLeft: null,

    })
  },




},
  {
    tabBarOptions: {
      style: {
        backgroundColor: '#fff',
        height: 50,
        borderTopColor: 'transparent',
        borderTopWidth: 1,


      },
      labelStyle: { fontSize: 14, fontFamily: 'futura-pt-medium' },
      indicatorStyle: {
        backgroundColor: '#ff4da6', // color of the indicator
        height: 3,
      },
      activeTintColor: 'black',
      inactiveTintColor: 'gray',
      activeBackgroundColor: 'white',
      inactiveBackgroundColor: '#F6F6F6',
      tabBarPosition: "top",



    }
  }


)
const Tab3 = TabNavigator({



  StoreLocationListView: {
    screen: StoreLocationListView,
    navigationOptions: (navigation) => ({
      headerTitle: <Text style={{
        flex: 1, color: 'red', fontSize: 22, fontFamily:
          'futura-pt-medium'
      }}>Select Store</Text>,
      tabBarLabel: 'LIST VIEW',
      headerLeft: null,
      tabBarIcon: ({ tintColor }) => (
        <Image style={{ height: 25, width: 25 }} source={require('./src/assets/home.png')} />
      )
    })
  },
  MapScreen: {
    screen: MapScreen,
    navigationOptions: (navigation) => ({

      tabBarLabel: 'MAP VIEW',
      headerLeft: null,
      tabBarIcon: ({ tintColor }) => (
        <Image style={{ height: 25, width: 25 }} source={require('./src/assets/home.png')} />
      )
    })
  },




},
  {
    tabBarOptions: {
      style: {
        backgroundColor: '#fff',
        height: 50,
        borderTopColor: 'transparent',
        borderTopWidth: 1,
        paddingRight: 5,
        paddingLeft: 5,
        borderTopWidth: 1,

      },
      labelStyle: { fontSize: 16, fontFamily: 'futura-pt-medium' },
      indicatorStyle: {
        backgroundColor: '#ff4da6', // color of the indicator
        height: 3,
      },
      activeTintColor: 'black',
      inactiveTintColor: 'gray',
      activeBackgroundColor: 'white',
      inactiveBackgroundColor: '#F6F6F6',
      tabBarPosition: "top",



    }
  }


)
const BottomNavigator = createBottomTabNavigator({



  HomeScreen: {
    screen: HomeScreen,
    navigationOptions: (navigation) => ({

      title: 'Home',

      headerLeft: null,
      // tabBarIcon: ({tintColor}) => (
      //   <Icon3  name="home-outline" color='grey' size={30}
      //         />
      // )
    })
  },
  Evoucher: {
    screen: Tab,
    navigationOptions: () => ({
      title: 'E-Voucher',
      tabBarIcon: ({ tintColor }) => (
        <Icon3 name="wallet-giftcard" color='grey' size={30}
        />

      )
    }), StoreLocationListView: {
      screen: StoreLocationListView,

    },
  },

  PromotionsList: {
    screen: PromotionsList,
    navigationOptions: () => ({
      tabBarIcon: ({ tintColor }) => (
        <Icon3 name="map-marker-outline" color='grey' size={30}
        />

      )
    }), 
  },
  MyCartScreen: {
    screen: MyCartScreen,
    EvoucherInStore: {
      screen: EvoucherInStore
    },

    navigationOptions: (navigation) => ({

      tabBarIcon: ({ tintColor }) => (
        <Icon3 name="cart-outline" color='grey' size={30}
        />

      )
    })
  },
},
  {
    tabBarOptions: {
      activeTintColor: '#ff4da6',
      inactiveTintColor: 'gray',
      activeBackgroundColor: 'white',
      inactiveBackgroundColor: '#F6F6F6',
      tabBarPosition: "top",




    }
  }

)


export const RootStack = createStackNavigator({
  Splash: { screen: SplashScreen },
  Login: {
    screen: LoginScreen,
    navigationOptions: ({ navigation }) => ({
    }),
  },


  MyReviewsScreen: {
    screen: MyReviewsScreen,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          'futura-pt-medium'
      }}>Sign Up</Text>,
      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />

    }),
  },
  ForgotPassword: {
    screen: ForgotPassword,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          'futura-pt-medium'
      }}>Forgot Password</Text>,
      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />

    }),
  },
  NewPassword: {
    screen: NewPassword,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          'futura-pt-medium'
      }}>New Password</Text>,
      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />
    }),
  },
  TrickNdTreat: {
    screen: TrickNdTreat,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          'futura-pt-medium'
      }}>Trick and Treat..</Text>,
      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />,
      headerRight:
        <TouchableOpacity  >
          <Image style={{ height: 20, width: 20, marginRight: 10 }} source={require('./src/assets/share-26.png')} />
        </TouchableOpacity>
    }),
  },
  ChangePassWord1: {
    screen: ChangePassWord1,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          'futura-pt-medium'
      }}>Change Password</Text>,
      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />,

    }),
  },
  PhoneVerification: {
    screen: PhoneVerification,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          'futura-pt-medium'
      }}>Phone Verification</Text>,
      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />
    }),
  },
  BillNoScreen: {
    screen: BillNoScreen,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          'futura-pt-medium'
      }}>Bill No:#0123</Text>,
      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />
    }),
  },
  AddReview: {
    screen: AddReview,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          'futura-pt-medium'
      }}>Add Review</Text>,
      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />

    }),
  },
  // StoreLocationListView:{screen:Tab3,
  //   navigationOptions: ({navigation}) => ({
  //     headerTitle: <Text style={{  flex: 1,color:'#fff',fontSize:22, fontFamily: 
  //     'futura-pt-medium'}}>Store Location</Text>,
  //     headerLeft:
  //     <BackArrow onPress={()=>navigation.goBack(null)} />
  //      }),
  //     },
  PhoneVerification1: {
    screen: PhoneVerification1,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          'futura-pt-medium'
      }}>Phone Verification</Text>,
      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />
    }),

  },
  // HomeMenu:{screen:MyDrawerNavigator,

  //       },


  NavigationDrawer: {
    screen: NavigationDrawer
  },
  HomeScreen: {
    screen: BottomNavigator,

    navigationOptions: ({ navigation }) =>
      ({

        header: <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: 'black', height: 60 }}>



          <TouchableOpacity style={{ marginLeft: 10, marginTop: 25 }} onPress={() => navigation.openDrawer()}>

            <Icon name="navicon" size={25} color="white" />
          </TouchableOpacity>
          <Image

            style={{
              width: 80,
              height: 30,
              marginTop: 20
            }}
            source={require('./src/assets/sasalogo1.png')}
          />
          <TouchableOpacity onPress={() => navigation.navigate('MyAccountScreen')}>
            <Image

              style={{
                width: 35,
                height: 35,
                marginTop: 20,
                marginRight: 10
              }}
              source={require('./src/assets/sasahead.png')}
            />
            {/* <Icon4 style={{height:30,width:30,marginLeft:10,marginTop:15,marginRight:10}}name="user-circle" size={25}  color="white" /> */}
          </TouchableOpacity>
        </View>
      }),

  },


  SignUp: {
    screen: SignUp,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          'futura-pt-medium'
      }}>Sign Up</Text>,
      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />
      // <TouchableOpacity onPress={()=>navigation.goBack(null)}>
      // <Icon style={{height:30,width:30,marginLeft:10}}name="arrow-left" size={25}  color="white" />
      // </TouchableOpacity>

    }),
  },
  MyAccountScreen: {
    screen: MyAccountScreen,
    navigationOptions: ({ navigation }) => ({
      headerTitle:
        <View style={{ flex: 1, ustifyContent: 'center', alignItems: 'center', marginRight: 30 }}>
          <Image source={require('./src/assets/sasalogo1.png')} />
        </View>,
      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />

    }),


  },
  PromotionScreen: {
    screen: PromotionScreen,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          'futura-pt-medium'
      }}>Promotions</Text>,

      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />

    }),
  },
  InboxScreen: {
    screen: InboxScreen,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          'futura-pt-medium'
      }}>Inbox</Text>,
      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />

    }),
  },
  MapScreen: {
    screen: MapScreen,
    navigationOptions: ({ navigation }) => ({

      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />

    }),
  },
  ProductList: {
    screen: ProductList,
    navigationOptions: ({ navigation }) => ({

      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />

    }),
  },
  RewardsScreen: {
    screen: RewardsScreen,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          'futura-pt-medium'
      }}>Rewards</Text>,
      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />

    }),
  },
  Activate: {
    screen: Activate,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          'futura-pt-medium'
      }}>Activate</Text>,
      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />

    }),
  },
  Activate1: {
    screen: Activate1,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          'futura-pt-medium'
      }}>Activate</Text>,
      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />

    }),
  },
  ReadyToCollect: {
    screen: ReadyToCollect,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          'futura-pt-medium'
      }}>Bill No:#0123</Text>,
      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />

    }),
  },
  BirthdayVoucher: {
    screen: BirthdayVoucher,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          'futura-pt-medium'
      }}>Birthday Voucher</Text>,
      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />

    }),
  },
  WelcomeGiftVoucher: {
    screen: WelcomeGiftVoucher,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          'futura-pt-medium'
      }}>Gift Voucher</Text>,
      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />

    }),
  },
  MyReviewsScreen: {
    screen: MyReviewsScreen,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          'futura-pt-medium'
      }}>My Reviews</Text>,
      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />

    }),
  },
  OnlinePurchaseTransaction: {
    screen: Tab2,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          'futura-pt-medium'
      }}>My Transaction</Text>,
      title: 'My Transactions',
      headerLeft: null,
      headerStyle: {
        backgroundColor: 'black'
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontSize: 16
      },

      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />

    }),

  },
  ActivationScreen: {
    screen: ActivationScreen,
    navigationOptions: ({ navigation }) => ({

      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />

    }),
  },
  Evoucher: {
    screen: Evoucher,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          'futura-pt-medium'
      }}>Evoucher</Text>,
      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />

    }),
  },
  EvoucherInStore: {
    screen: EvoucherInStore,
    navigationOptions: ({ navigation }) => ({

      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />

    }),
  },

  CheckoutScreen: {
    screen: CheckoutScreen,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          'futura-pt-medium'
      }}>Checkout</Text>,
      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />

    }),
  },
  SelectStoreScreen: {
    screen: Tab3,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          'futura-pt-medium'
      }}>Select Store</Text>,
      title: 'My Transactions',
      headerLeft: null,
      headerStyle: {
        backgroundColor: 'black'
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontSize: 16
      },

      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />,
      // headerRight:
      // <Image source={require('./assets/ic_search_24px.png')} />,

    }),
  },
  CheckoutPaymentScreen: {
    screen: CheckoutPaymentScreen,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          'futura-pt-medium'
      }}>Payment</Text>,
      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />

    }),
  },

  MakeUp: {
    screen: MakeUp,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          'futura-pt-medium'
      }}>Make Up</Text>,
      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />,
      headerRight:
        <TouchableOpacity style={{ marginRight: 10 }} onPress={() => navigation.navigate('MyCartScreen')} >
          <Icon3 name="cart-outline" color='white' size={30}
          />
        </TouchableOpacity>

    }),
  },
  MyCard: {
    screen: MyCard,

  },
  CheckoutPaymentSuccess: {
    screen: CheckoutPaymentSuccess,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          'futura-pt-medium'
      }}>Payment Success</Text>,
      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />

    }),
  },
  Help: {
    screen: Help,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          'futura-pt-medium'
      }}>Help </Text>,
      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />

    }),
  },
  ExchangeAndReturn: {
    screen: ExchangeAndReturn,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          'futura-pt-medium'
      }}>Exchange and Return </Text>,
      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />

    }),
  },
  Membership: {
    screen: Membership,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          'futura-pt-medium'
      }}>Membership</Text>,
      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />

    }),
  },
  Terms: {
    screen: Terms,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          'futura-pt-medium'
      }}>Terms & Condition</Text>,
      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />

    }),
  },
  CompanyProfile: {
    screen: CompanyProfile,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          'futura-pt-medium'
      }}>CompanyProfile</Text>,
      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />

    }),
  },
  Privacy: {
    screen: Privacy,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          'futura-pt-medium'
      }}>Privacy Policy</Text>,
      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />

    }),
  },
  SelectedCatogiries: {
    screen: SelectedCatogiries,
    navigationOptions: ({ navigation }) => ({

      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />

    }),
  },
  ProductDetailsScreen: {
    screen: ProductDetailsScreen,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          'futura-pt-medium'
      }}>Cyber Colors-Serum Lipistick</Text>,
      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />,
      //             headerRight:
      //   <TouchableOpacity  onPress={() => navigation.navigate('MyCartScreen')} >
      //   <Image  style={{height:20,width:20,marginRight:10}}source={require('./src/assets/shopingicon.png')} /> 
      // </TouchableOpacity>

    }),
  },
  Slider: {
    screen: Slider,
    navigationOptions: ({ navigation }) => ({

      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />

    }),
  },

  ItemsPictures: {
    screen: ItemsPictures,
    navigationOptions: ({ navigation }) => ({


      headerRight:
        <TouchableOpacity onPress={() => navigation.goBack(null)}  >
          <Image style={{ height: 20, width: 20, marginRight: 10 }} source={require('./src/assets/cross.png')} />
        </TouchableOpacity>

    }),
  },
  LikedProducts: {
    screen: LikedProducts,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <Text style={{
        flex: 1, color: '#fff', fontSize: 22, fontFamily:
          'futura-pt-medium'
      }}>Liked Products</Text>,
      headerLeft:
        <BackArrow onPress={() => navigation.goBack(null)} />,
      headerRight:
        <TouchableOpacity  >
          <Image style={{ height: 30, width: 30, marginRight: 10 }} source={require('./src/assets/mycart.png')} />
        </TouchableOpacity>

    }),




  },


},

  {
    initialRouteName: 'Splash'
  }
);
// export const Drawer=createDrawerNavigator({
//   HomeScreen:{screen:HomeScreen},


export const Drawer = createDrawerNavigator({

  RootStack: {
    screen: RootStack,
    navigationOptions: {

      drawerLabel: <View style={{ justifyContent: 'center', alignItems: 'center', }}>
        <ImageBackground style={{ height: 160, }} source={require('./src/assets/base1.png')}>





          <Image style={{
            marginLeft: 70,
            marginRight: 80,
            marginTop: 40
          }} source={require('./src/assets/base2.png')} />
        </ImageBackground>
      </View>
    }
  },
  MyAccountScreen: {
    screen: MyAccountScreen,
    navigationOptions: {

      drawerLabel: <View style={{ flexDirection: 'row', backgroundColor: '#fff', width: width / 1 }}>
        <Icon4 style={{ marginTop: 20, marginLeft: 20 }} name="user-circle" color='black' size={25}>
        </Icon4>
        <Text style={{ fontSize: 16, fontFamily: 'futura-pt-medium', fontWeight: '300', marginTop: 23, color: '#000000' - 87, marginLeft: 20 }}>MY ACCOUNT</Text>



      </View>
    }
  },
  InboxScreen: {
    screen: InboxScreen,
    navigationOptions: {
      drawerLabel:
        <View style={{ flexDirection: 'row', backgroundColor: '#fff', width: width / 1 }}>
          <Icon3 style={{ marginTop: 20, marginLeft: 20 }} name="bell-outline" color='black' size={30}>
          </Icon3>
          <Text style={{ fontSize: 16, fontFamily: 'futura-pt-medium', fontWeight: '300', color: '#000000' - 87, marginLeft: 20, marginTop: 25 }}>INBOX</Text>
          <Badge
            containerStyle={{ backgroundColor: '#F48FB1', marginLeft: 100, height: 32, width: 32, marginTop: 20 }}
            value={5}
            textStyle={{ color: '#fff' }}
          />
        </View>
    }
  },
  Location: {
    screen: Tab1,
    navigationOptions: {
      drawerLabel:
        <View style={{ flexDirection: 'row', backgroundColor: '#fff', width: width / 1 }}>
          <Icon3 style={{ marginTop: 20, marginLeft: 20 }} name="map-marker-outline" color='black' size={30}>
          </Icon3>
          <Text style={{ fontSize: 16, fontFamily: 'futura-pt-medium', fontWeight: '300', color: '#000000' - 87, marginLeft: 20, marginTop: 25 }}>LOCATION</Text>

        </View>
    }
  },
  RewardsScreen: {
    screen: RewardsScreen,
    navigationOptions: {

      drawerLabel:
        <View style={{ flexDirection: 'row', backgroundColor: '#fff', width: width / 1 }}>
          <Icon3 style={{ marginTop: 20, marginLeft: 20 }} name="wallet-giftcard" color='black' size={27}>
          </Icon3>
          <Text style={{ fontSize: 16, fontFamily: 'futura-pt-medium', fontWeight: '300', marginTop: 25, color: '#000000' - 87, marginLeft: 20, }}>REWARDS</Text>
        </View>
    }
  },
  Help: {
    screen: Help,
    navigationOptions: {

      drawerLabel:
        <View style={{ flexDirection: 'row', backgroundColor: '#fff', width: width / 1 }}>
          <Icon3 style={{ marginTop: 20, marginLeft: 20 }} name="information-outline" color='black' size={25}>
          </Icon3>
          <Text style={{ fontSize: 16, fontFamily: 'futura-pt-medium', fontWeight: '300', marginTop: 25, color: '#000000' - 87, marginLeft: 20, marginBottom: 20 }}>HELP & FAQS</Text>
        </View>
    }
  },
  ExchangeAndReturn: {
    screen: ExchangeAndReturn,
    navigationOptions: {

      drawerLabel: <View style={{ flexDirection: 'column', backgroundColor: '#fff', marginTop: 10, width: 400, marginBottom: 10 }}><Text style={{ fontSize: 16, marginLeft: 20, fontFamily: 'futura-pt-book', fontWeight: '300', marginTop: 20, color: 'lightgrey', }}>ABOUT SASA</Text>
        <Text style={{ fontSize: 16, marginLeft: 20, fontFamily: 'futura-pt-book', fontWeight: '300', marginTop: 30, color: '#000000' - 87 }}>EXCHANGE AND RETURN</Text>
        <Text style={{ fontSize: 16, marginLeft: 20, fontFamily: 'futura-pt-book', fontWeight: '300', marginTop: 30, color: '#000000' - 87 }}>TERMS & CONDITIONS</Text>
        <Text style={{ fontSize: 16, marginLeft: 20, fontFamily: 'futura-pt-book', fontWeight: '300', marginTop: 30, color: '#000000' - 87 }}>PRIVACY POLICY</Text>
        <Text style={{ fontSize: 16, marginLeft: 20, fontFamily: 'futura-pt-book', fontWeight: '300', marginTop: 30, color: '#000000' - 87 }}>MEMBERSHIP AND BENIFITS</Text>
        <Text style={{ fontSize: 16, marginLeft: 20, fontFamily: 'futura-pt-book', fontWeight: '300', marginTop: 30, color: '#000000' - 87, marginBottom: 20 }}>COMPANY PROFILE</Text>

      </View>


    }
  },
  Privacy: {
    screen: Privacy,
    navigationOptions: {

      drawerLabel: <View style={{ flexDirection: 'column', backgroundColor: "#fff", width: 400 }}>
        <Text style={{ fontSize: 16, marginLeft: 20, fontFamily: 'futura-pt-book', fontWeight: '300', marginTop: 20, color: 'lightgrey' }}>FOLLOW US</Text>
        <View style={{ flexDirection: 'row', marginLeft: 20, marginTop: 10 }}>
          <Icon4 style={{ marginTop: 10 }} name="facebook" color='black' size={30}>
          </Icon4>
          <Text style={{ fontSize: 16, marginLeft: 20, fontFamily: 'futura-pt-book', fontWeight: '300', color: '#000000' - 87, marginTop: 15 }}>FACEBOOK</Text>

        </View>
        <View style={{ flexDirection: 'row', marginLeft: 20 }}>
          <Icon3 style={{ marginTop: 20 }} name="instagram" color='black' size={30}>
          </Icon3>
          <Text style={{ fontSize: 16, marginLeft: 20, fontFamily: 'futura-pt-book', fontWeight: '300', color: '#000000' - 87, marginTop: 25, marginBottom: 20 }}>INSTAGRAM</Text>

        </View>

      </View>

    }
  },
  Terms: {
    screen: Terms,
    navigationOptions: {

      drawerLabel: <View style={{ flexDirection: 'column', marginTop: 10, backgroundColor: '#fff', width: 400 }}><Text style={{ fontSize: 16, marginLeft: 20, fontFamily: 'futura-pt-book', fontWeight: '300', marginTop: 20, color: 'lightgrey' }}>TELL US WHAT YOU THINK</Text>
        <Text style={{ fontSize: 16, marginLeft: 20, fontFamily: 'futura-pt-book', fontWeight: '300', marginTop: 30, color: '#000000' - 87 }}>CONTACT US</Text>
        <Text style={{ fontSize: 16, marginLeft: 20, fontFamily: 'futura-pt-book', fontWeight: '300', marginTop: 30, color: '#000000' - 87, marginBottom: 20 }}>RATE THE APP</Text>

      </View>




    }
  },
  Membership: {
    screen: Membership,
    navigationOptions: {

      drawerLabel: <View style={{ backgroundColor: '#fff', width: 400, marginTop: 10 }}>
        <Text style={{ fontSize: 16, marginLeft: 20, fontFamily: 'futura-pt-book', fontWeight: '300', marginTop: 20, color: 'lightgrey', marginBottom: 20 }}>APP VERSION 2.1.0</Text></View>





    }
  },

},
  {
    style: {
      backgroundColor: '#F5F5F5'
    }
  }



);

// })




// const MyDrawerNavigator = createDrawerNavigator({
//   HomeMenu:{
//     screen:RootStack
//   },

// },
// {
// //initialRouteName: 'SettingsScreen',
// drawerOpenRoute: 'DrawerOpen',
// drawerCloseRoute: 'DrawerClose',
// drawerToggleRoute: 'DrawerToggle',

// contentComponent: (props) => <HomeMenu/>
// }

// );



