import React, { Component } from 'react';
import styles from '../../styles/styleStoreLocationListView'
import { Platform, FlatList, Text, View, Image, ScrollView, TextInput, TouchableOpacity, Alert, Dimensions } from 'react-native';
import { Container, Header, Content, Button, ListItem, List, Left, Right, Icon } from 'native-base';

import Progressbar from 'react-native-loading-spinner-overlay';
import Service from '../../config/Service';
import CanonicalPath from '../../config/CanonicalPath';

import PropTypes from 'prop-types';
import { futuraPtMedium, futuraPtBook, futuraPtLight } from '../../styles/styleText'

export default class StoreLocationListView1 extends Component {
    static navigationOptions = ({

        headerLeft: null,
        header: {
            style: {
                backgroundColor: 'red',
            }
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontSize: 22,
            fontFamily: futuraPtMedium,
            fontWeight: '500'
        },
    })
    static propTypes = {

        onPress: PropTypes.func.isRequired,

    }

    constructor(props) {
        super(props);
        this.state = {
            check: true,
            selectedIndex: 0,
            locations: [],
            isHaveRecord: false,
            latitude: '0',
            longitude: '0',

        }
    }

    getCurrentPosition() {
        try {
            navigator.geolocation.getCurrentPosition(
                (position) => {
                    //   const region = {
                    //     latitude: position.coords.latitude,
                    //     longitude: position.coords.longitude,
                    //     latitudeDelta: LATITUDE_DELTA,
                    //     longitudeDelta: LONGITUDE_DELTA,
                    //   };
                    this.setState({
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                    })
                    this.locations()

                },
                (error) => {
                    //TODO: better design
                    switch (error.code) {
                        case 1:
                            if (Platform.OS === "ios") {
                                Alert.alert("", "To find your location, activate the permissions for the application in Settings - Privacy - Location");
                            } else {
                                Alert.alert("", "To find your location, activate the permissions for the application in Settings - Privacy - Location");
                            }
                            break;
                        default:
                            Alert.alert("", "To find your location, activate the permissions for the application in Settings - Privacy - Location");
                    }
                }
            );
        } catch (e) {
            alert(e.message || "");
        }
    };

    setStateAsync(state) {
        return new Promise((resolve) => {
            this.setState(state, resolve)
        });
    }

    onSuccess = async (filter, json) => {
        this.setStateAsync({ isLoading: false });
        switch (filter) {
            case "LOCATIONS":
                if (json.length > 0) {
                    this.setState({
                        isHaveRecord: true,
                        locations: json,
                    })
                } else {
                    this.setState({
                        isHaveRecord: false,
                    })
                }
                break
        }
    }

    onError = (filter, e) => {
        this.setStateAsync({ isLoading: false });
        alert(e)
    }

    async locations() {
        await this.setStateAsync({ isLoading: true });

        var data = await {
            "lat": this.state.latitude,
            "lng": this.state.longitude,
            "pickup": 0,
        }

        var params = await {
            contentType: "multipart/form-data",
            Accept: "application/json",
            keepAlive: true,
            method: 'GET',
            useToken: true,
            data: data,
            canonicalPath: CanonicalPath.LOCATIONS
        };

        await Service.request(this.onSuccess, this.onError, "LOCATIONS", params);
    }

    componentDidMount() {
        this.getCurrentPosition()
    }

    _keyExtractor = (items, index) => items.id;

    renderRow = (items) => {

        var item = items.item

        return (
            <View style={{ backgroundColor: '#fff', marginBottom: 10 }}>


                <View style={styles.viewText}>
                    <View style={styles.styleViewOne} >

                        {/* <Text style={{ fontSize: 15, color: 'black' }}>1</Text> */}

                        <Text style={{ color: '#000', fontWeight: '300', fontFamily: futuraPtMedium, fontSize: 18 }}> {item.name} </Text>
                        <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                            <Text style={styles.styleTextRight}>{parseFloat(item.distance_in_km).toFixed(2)} km </Text>
                        </View>

                    </View>

                    <View style={styles.styleViewOne}>
                        <Image />
                        <Text style={{ color: '#000000', fontFamily: futuraPtBook, fontWeight: '300', fontSize: 16 }}>{item.address} </Text>
                    </View>

                    <View style={{ backgroundColor: '#EEEEEE', height: 0.50, marginLeft: 40, marginRight: 40, marginTop: 10 }}></View>
                </View>

                <List>
                    <View style={styles.viewText}>
                        <ListItem>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                                <Image source={require('../../assets/icon_watch.png')} />
                                <Text style={{ color: '#000', fontFamily: futuraPtBook, fontSize: 16,}}> Open Daily </Text>
                                <View style={{ flex: 1 }}>
                                    <Text style={{ textAlign: 'right', fontFamily: futuraPtLight, fontSize: 16, marginTop: 3 }}>{item.open_hours} </Text>
                                </View>

                            </View>
                        </ListItem>





                        <View style={styles.viewText}>
                            <ListItem>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Image source={require('../../assets/icon_phone.png')} />
                                    <Text style={{ color: '#000', fontSize: 16, fontFamily: futuraPtBook, marginTop: 3 }}> {item.tel} </Text>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('CheckoutScreen', { cartFill: "yes" })}
                                    style={{ flex: 1, height: 35, backgroundColor: '#E4007C', justifyContent: "center", alignItems: 'center', marginLeft: 100 }}
                                    >
                                        <Text style={{ color: '#fff', fontWeight: '300', fontFamily: futuraPtMedium, fontSize: 16 }}>DIRECTION</Text>


                                    </TouchableOpacity>

                                </View>
                            </ListItem>
                        </View>

                    </View>
                </List>
            </View>
        )
    }

    render() {
        const { onPress } = this.props;
        return (
            <ScrollView style={styles.containerBackground}>

                <Progressbar cancelable={true} visible={this.state.isLoading} overlayColor="rgba(0, 0, 0, 0.8)" animation="fade" textContent="" style={{ fontSize: 16, textAlign: 'center', justifyContent: 'center', height: 30, alignItems: 'center' }} />


                {
                    (this.state.isHaveRecord) ?
                        <FlatList
                            vertical
                            data={this.state.locations}
                            renderItem={items => this.renderRow(items)}>
                        </FlatList>
                        :
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                            <Text>No Record</Text>
                        </View>

                }

            </ScrollView>
        );
    }
}
