import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Image,ScrollView,TextInput,TouchableOpacity,ImageBackground,Dimensions} from 'react-native';
import { Container, Header, Content, Button,ListItem,List  } from 'native-base';

const dimWidth= Dimensions.get('window').width
const dimHeight= Dimensions.get('window').height
export default class Splash extends Component{
     render(){
        return(
           
            <ImageBackground style={{width: dimWidth , height: dimHeight}}source={require('../assets/splashBg.png')} >


                <View style={{justifyContent:"center",alignItems:'center' , marginTop:80, }}>
                        <Image source={require('../assets/logo.png')}>
                        </Image>

                        
                </View>



            </ImageBackground>
             )
    }

}