
import CheckBox from 'react-native-checkbox';
import { Col, Row, Grid } from "react-native-easy-grid";
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, ScrollView, TextInput, TouchableOpacity, ImageBackground, Dimensions, Alert, ActivityIndicator } from 'react-native';
import { Container, Header, Content, Button, ListItem, List } from 'native-base';
import styles from '../styles/signUp';
import { SIGNUP_URL } from '../config/Global'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CanonicalPath from '../config/CanonicalPath';
import Config from '../config/Config'
import { futuraPtMedium, futuraPtBook } from '../styles/styleText'
import { Formik } from 'formik'
import * as Yup from 'yup'

import Input from '../components/Input'

const dimWidth = Dimensions.get('window').width
const dimHeight = Dimensions.get('window').height

const schema = Yup.object().shape({
    name: Yup.string()
            .required( "Please enter your name" ),
    phoneNumber: Yup.number()
            .required( "Please enter your phone number" )
            .min(5, "Phone number not valid" ),
    email: Yup.string()
            .email("Please enter valid Email address")
            .required( "Please enter valid Email address" ),
    password: Yup.string()
            .required( "Please enter your password" ),
    confirmPassword: Yup.string()
            .oneOf([Yup.ref('password')], 'Password and confirm password must be same')
            .required( "Please enter your confirm password" ),
    term1: Yup.boolean()
            .required()
            .oneOf([true], ''),
    term2: Yup.boolean()
            .required()
            .oneOf([true], '')
})

export default class SignUp extends Component {

    static navigationOptions = ({
        title: 'Sign Up',
        titleStyle: {
            fontFamily: futuraPtBook
        },
        headerLeft: null,
        headerStyle: {
            backgroundColor: 'black'
        },
        headerTintColor: '#fff'
    })

    constructor(props) {
        super(props);
        this.state = {
            check: true,
            selectedIndex: 0,
            secCode: 'sasaapp20181019'
        }
    }

    async SignUp( values ) {
        console.log('on signup ', values)
        
        var details = {
            'secode': this.state.secCode,
            'name': values.name,
            'email': values.email,
            'phone': values.phoneNumber,
            'password': values.password,
            'subscribe': 0
        };
        var formBody = [];
        for (var property in details) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(details[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        formBody = formBody.join("&");
        fetch(SIGNUP_URL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body: formBody
        })
            .then((serviceResponse) => { return serviceResponse.json() })
            .catch((error) => console.warn("fetch error:", error))
            .then(async (serviceResponse) => {
                console.log('responser ', serviceResponse)
                const { status, error } = serviceResponse[0];
                if (status == 1) {
                    const res = await this.sendTac( values.phoneNumber );
                    console.log('res sendTac ', res)
                    if (res.success) {
                        this.props.navigation.navigate('PhoneVerification1', { ContactNo: values.phoneNumber })
                    } else {
                        Alert.alert(res.message)
                    }
                } else {
                    Alert.alert(error)
                }

                console.log(JSON.stringify(serviceResponse));
            });
    }

    async sendTac(phoneNumber) {
        let result = { success: false, message: "tac not sent" };
        try {
            const url = Config.baseUrl + CanonicalPath.SEND_TAC + "?secode=" + Config.secode + "&phone=" + phoneNumber;
            const response = await fetch(url, { method: "POST" });
            const res = await response.json();
            console.log('res ', res, response)
            const { status, error } = res[0];
            if (status == 1) {
                result = { success: true, message: "tac has sent" }
            } else {
                result = { success: false, message: error }
            }
        } catch (err) {
            result.message = err.message;
        }
        return result;
    }

    async handleSubmit( values, actions ){

        const res = await this.SignUp( values );
        actions.setSubmitting(false)

        return false;
    }

    renderForm( props ){
        return(
            <View style={{ flex: 1 }}>
                <Input
                    title="Name"
                    name="name"
                    autoCapitalize='none'
                    value={props.values.name}
                    error={props.errors.name}
                    setFieldValue={props.setFieldValue}
                    setFieldTouched={()=>{}}
                />
                <Input
                    title="Phone Number"
                    name="phoneNumber"
                    autoCapitalize='none'
                    keyboardType="numeric"
                    value={props.values.phoneNumber}
                    error={props.errors.phoneNumber}
                    setFieldValue={props.setFieldValue}
                    setFieldTouched={()=>{}}
                />
                <Input
                    title="Email Address"
                    name="email"
                    autoCapitalize='none'
                    keyboardType="email-address"
                    value={props.values.email}
                    error={props.errors.email}
                    setFieldValue={props.setFieldValue}
                    setFieldTouched={()=>{}}
                />
                <Input
                    title="Password"
                    name="password"
                    autoCapitalize='none'
                    value={props.values.password}
                    error={props.errors.password}
                    setFieldValue={props.setFieldValue}
                    setFieldTouched={()=>{}}
                    secureTextEntry={true}
                />
                <Input
                    title="Confirm Password"
                    name="confirmPassword"
                    autoCapitalize='none'
                    value={props.values.confirmPassword}
                    error={props.errors.confirmPassword}
                    setFieldValue={props.setFieldValue}
                    setFieldTouched={()=>{}}
                    secureTextEntry={true}
                />
                {/* *************** Check Box 1 **************************** */}

                <View style={styles.Container1}>
                    <View style={styles.checkboxContainer1}>
                        <CheckBox
                            style={styles.checkboxStyle1}
                            label=''
                            onChange={( checked ) => {
                                console.log('onChange checkbox1 ', checked)
                                props.setFieldValue( "term1", checked)
                            }} />
                    </View>

                    <View style={styles.terms1}>
                        <Text style={{ marginRight: 50, fontSize: 18, fontWeight: '300', fontFamily: futuraPtBook, color: '#000' }}>
                            I agree and accept all Terms and Conditions and Privacy Policy of SASA Malaysia.
                        </Text>
                        {
                            ( props.errors.term1 && props.errors.term1.length > 0)
                            ? <View><Text style={ styles.nameError }>Please tick Agree to policies</Text></View>
                            : null
                        }
                    </View>
                </View>

                {/* *************** Check Box 2  **************************** */}
                <View style={styles.Container2}>
                    <View style={styles.checkboxContainer2}>
                        <CheckBox
                            style={styles.checkboxStyle1}
                            label=''
                            onChange={( checked ) => {
                                console.log('onChange checkbox1 ', checked)
                                props.setFieldValue( "term2", checked)
                            }} />
                    </View>

                    <View style={styles.terms2}>
                        <Text style={{ marginRight: 50, fontSize: 18, fontWeight: '300', fontFamily: futuraPtBook, color: '#000' }}>
                            I agree to receive newsletter and SMS from SASA Malaysia.
                        </Text>
                        {
                            ( props.errors.term2 && props.errors.term2.length > 0)
                            ? <View><Text style={ styles.nameError }>Please tick Agree to policies</Text></View>
                            : null
                        }
                    </View>
                </View>

                <View style={{ marginTop: 20, marginLeft: 20, marginRight: 20, height: 50 }}>
                    {
                        props.isSubmitting
                        ?   <ActivityIndicator size="small"/>
                        :   <TouchableOpacity
                                style={{ backgroundColor: '#E4007C', justifyContent: "center", alignItems: 'center', height: 50 }}
                                onPress={ (e) => {
                                    props.handleSubmit(e)
                                }}
                            >
                                <Text style={{ color: 'white', fontSize: 20, fontFamily: futuraPtMedium }}>SIGN UP</Text>
                            </TouchableOpacity>
                    }
                </View>
            </View>
        )
    }

    render() {
        return (
            <KeyboardAwareScrollView>
                <ScrollView style={styles.container} scrollEnabled={true} >

                    <Formik
                        onSubmit={ this.handleSubmit.bind(this) }
                        validationSchema={ schema }
                        render={ this.renderForm.bind(this) }
                        validateOnChange={ false }
                    />
                    <View>
                        <Text style={styles.phoneText} > </Text>
                        <Text style={styles.phoneText} > </Text>

                    </View>

                </ScrollView>
            </KeyboardAwareScrollView>

        );
    }
}
