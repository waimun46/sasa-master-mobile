import React, { Component } from 'react';
import styles from '../styles/styleNewPassword'
import { Platform, Alert, Text, View, Image, ScrollView, TextInput, TouchableOpacity, ImageBackground, Dimensions } from 'react-native';
import { Container, Header, Content, Button, ListItem, List } from 'native-base';

import Progressbar from 'react-native-loading-spinner-overlay';
import Service from '../config/Service';
import CanonicalPath from '../config/CanonicalPath';
import { futuraPtMedium } from '../styles/styleText'

export default class ChangePassWord1 extends Component {
    static navigationOptions = ({
        title: 'Change Password',
        headerLeft: null,
        headerStyle: {
            backgroundColor: 'black'
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontSize: 22,
            fontFamily: futuraPtMedium,
            fontWeight: '500'
        },
    })

    constructor(props) {
        super(props);
        this.state = {
            check: true,
            selectedIndex: 0,
            newPassword: '',
            newPasswordError: false,
            confirmPassword: '',
            confirmPasswordError: false

        }
    }


    validation = () => {
        this.setState({
            newPasswordError: false,
            confirmPasswordError: false
        })

        if (this.state.newPassword == '') {
            this.setState({
                newPasswordError: true
            })
        }
        else if (this.state.confirmPassword != this.state.newPassword) {
            this.setState({
                confirmPasswordError: true
            })
        }
        else {
            this.update_password()
        }

    }

    setStateAsync(state) {
        return new Promise((resolve) => {
            this.setState(state, resolve)
        });
    }

    onSuccess = async (filter, json) => {
        this.setStateAsync({ isLoading: false });

        switch (filter) {
            case "UPDATE_PASSWORD":

                Alert.alert("Update password successfully!");

                break;
        }

    }

    onError = (filter, e) => {
        this.setStateAsync({ isLoading: false });

        Alert.alert(e)
    }

    async update_password() {
        await this.setStateAsync({ isLoading: true });

        var formData = new FormData();

        formData.append('password', this.state.newPassword);

        var params = await {
            contentType: "multipart/form-data",
            Accept: "application/json",
            keepAlive: true,
            method: 'POST',
            useToken: true,
            data: formData,
            canonicalPath: CanonicalPath.UPDATE_PASSWORD
        };

        await Service.request(this.onSuccess, this.onError, "UPDATE_PASSWORD", params);

    }



    render() {
        return (
            <ScrollView style={styles.containerBackground}>

                <Progressbar cancelable={true} visible={this.state.isLoading} overlayColor="rgba(0, 0, 0, 0.8)" animation="fade" textContent="" style={{ fontSize: 16, textAlign: 'center', justifyContent: 'center', height: 30, alignItems: 'center' }}></Progressbar>

                <View style={styles.viewNewPsw}>
                    <Text style={styles.textNewPsw} >Old Password</Text>
                    <TextInput style={styles.inputNewPsw} autoCapitalize='none' placeholder='**********' secureTextEntry={true}
                        onChangeText={newPassword => this.setState({ newPassword })}
                    />
                </View>
                <View style={styles.lineDown}></View>
                <View style={styles.viewNewPsw}>
                    <Text style={styles.textNewPsw} >New Password</Text>
                    <TextInput style={styles.inputNewPsw} autoCapitalize='none' placeholder='**********' secureTextEntry={true}
                        onChangeText={newPassword => this.setState({ newPassword })}
                    />
                </View>
                <View style={styles.lineDown}></View>

                {this.state.newPasswordError == true &&
                    <View>
                        <Text style={styles.pswError}> Please enter password </Text>
                    </View>
                }


                <View style={styles.viewConfirmPsw}>
                    <Text style={styles.textConfirmPsw} >Confirm New Password</Text>
                    <TextInput style={styles.inputConfirmPsw} autoCapitalize='none' placeholder='**********' secureTextEntry={true}
                        onChangeText={confirmPassword => this.setState({ confirmPassword })}
                    />
                </View>
                <View style={styles.lineDown}></View>

                {this.state.confirmPasswordError == true &&
                    <View>
                        <Text style={styles.conFirError}> Password and confirm password must be same </Text>
                    </View>
                }

                <View style={styles.viewButton}>
                    <TouchableOpacity style={styles.touch}
                        onPress={this.validation} >
                        <Text style={styles.textButton}>CHANGE PASSWORD</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        );
    }
}
