import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, ScrollView, TextInput, TouchableOpacity, ImageBackground, Dimensions, Alert } from 'react-native';
import { Input, Header, Content, Button, ListItem, List } from 'native-base';
import CanonicalPath from '../config/CanonicalPath';
import Config from '../config/Config'
import { futuraPtMedium, futuraPtBook } from '../styles/styleText'

export default class PhoneVerification1 extends Component {
    static navigationOptions = ({
        title: 'Phone Verification',
        headerLeft: null,
        headerStyle: {
            backgroundColor: 'black'
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontSize: 22,
            fontFamily: futuraPtMedium,
            fontWeight: '500'
        },
    })

    constructor(props) {
        super(props);
        this.state = {
            check: true,
            selectedIndex: 0,
            oneText: false,
            oneTextError: false,
            twoText: false,
            threeText: false,
            fourText: false,
            fiveText: false,
            sixText: false,
            accesCode: '',

        }
    }


    validation = async (contactNo) => {

        this.setState({
            oneTextError: false

        })

        // if (contactNo == '' || this.state.oneText == '' || this.state.twoText == '' || this.state.threeText == '' || this.state.fourText == '' || this.state.fiveText == '' || this.state.sixText == '') {
        //     this.setState({
        //         oneTextError: true
        //     })
        // }
        if (contactNo == '' || this.state.accesCode == '') {
            this.setState({
                oneTextError: true
            })
            return;
        }
        else {
            // const tacCode = this.state.oneText + this.state.twoText + this.state.threeText + this.state.fourText + this.state.fiveText + this.state.sixText;
            const tacCode = this.state.accesCode;
            const res = await this.phoneVerify(contactNo, tacCode)
            if (res.success) {
                this.props.navigation.navigate('Login')
            } else {
                Alert.alert(res.message)
            }
        }
    }

    async sendTac(phoneNumber) {
        let result = { success: false, message: "tac invalid" };
        try {
            const url = Config.baseUrl + CanonicalPath.SEND_TAC + "?secode=" + Config.secode + "&phone=" + phoneNumber;
            console.log('sendTac ', url)
            const response = await fetch(url, { method: "POST" });
            console.log('response ', response)
            const res = await response.json();
            console.log('res ', res, response)
            const { status, error } = res[0];
            if (status === 1) {
                result = { success: true, message: error }
            } else {
                result = { success: false, message: error }
            }
        } catch (err) {
            console.log('on error ', err)
            result.message = err.message;
        }
        Alert.alert(result.message)
        return result;
    }

    async phoneVerify(phoneNumber, tacCode) {
        let result = { success: false, message: "wrong tac code" };
        try {
            const url = Config.baseUrl + CanonicalPath.PHONE_VERIFY;
            console.log('phoneVerify ', url, phoneNumber, tacCode)
            const body = new FormData();
            body.append("secode", Config.secode);
            body.append("phone", phoneNumber);
            body.append("tac", tacCode);
            const response = await fetch(url, { method: "POST", body: body });
            const res = await response.json();
            console.log('res ', res, response)
            const { status, error } = res[0];
            if (status === 1) {
                result = { success: true, message: "tac code valid" }
            } else {
                result = { success: false, message: error }
            }
        } catch (err) {
            console.log('error ', err)
            result.message = err.message;
        }
        return result;
    }

    render() {
        const { navigation } = this.props;
        const contactNo = navigation.getParam('ContactNo');
        return (
            <ScrollView style={{ flex: 1, backgroundColor: '#fff' }} >

                {/******************* Text 1 ************************* */}
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 20, marginLeft: 20, marginRight: 20 }}>
                    <Text style={{ color: '#000', justifyContent: "center", alignItems: 'center', fontWeight: '300', fontFamily: futuraPtBook, fontSize: 18, textAlign: 'center' }} >
                        We have sent you a TAC code via SMS to verify
                </Text>
                </View>


                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 3, marginLeft: 20, marginRight: 20 }}>
                    <Text style={{ color: '#000', justifyContent: "center", alignItems: 'center', fontWeight: '300', fontFamily: futuraPtBook, fontSize: 18 }} >
                        your phone number.
                </Text>
                </View>

                {/******************* Text 2 ************************* */}
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 10, marginLeft: 20, marginRight: 20 }}>
                    <Text style={{ color: '#000', justifyContent: "center", alignItems: 'center', fontFamily: futuraPtBook, fontWeight: '300', fontSize: 16 }} >
                        Sent to
                </Text>
                </View>



                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: -3, marginLeft: 20, marginRight: 20 }}>
                    <Text style={{ color: '#000', justifyContent: "center", alignItems: 'center', fontFamily: futuraPtMedium, fontWeight: '500', fontSize: 16 }} >
                        +60{contactNo}
                    </Text>
                </View>

                {/********************* Text 3 *********************** */}

                <View style={{ marginTop: 25, justifyContent: 'center', flex: 1, alignItems: "center", height: 80, }}>
                    <Input
                        keyboardType="numeric"
                        style={{ fontSize: 50, padding: 0, margin: 0, letterSpacing: 25, height: 50, width: 335 }}
                        value={this.state.accesCode}
                        maxLength={6}
                        onChangeText={accesCode => this.setState({ accesCode })}
                    />

                </View>
                <View style={{ marginBottom: 25, justifyContent: 'center', flex: 1, alignItems: "center", }}>
                    <View style={{ flexDirection: 'row', flex: 1, }}>
                        <View style={{ borderColor: 'black', borderWidth: 2, width: 45, height: 2, marginRight: 8 }} />
                        <View style={{ borderColor: 'black', borderWidth: 2, width: 45, height: 2, marginRight: 8 }} />
                        <View style={{ borderColor: 'black', borderWidth: 2, width: 45, height: 2, marginRight: 8 }} />
                        <View style={{ borderColor: 'black', borderWidth: 2, width: 45, height: 2, marginRight: 8 }} />
                        <View style={{ borderColor: 'black', borderWidth: 2, width: 45, height: 2, marginRight: 8 }} />
                        <View style={{ borderColor: 'black', borderWidth: 2, width: 45, height: 2, marginRight: 8 }} />
                    </View>
                </View>

                {/* <View style={{ flex: 1, flexDirection: 'row' }}>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 20, marginLeft: 20, marginRight: 20 }}>
                        <TextInput maxLength={1} returnKeyType={"next"} style={{
                            color: '#ff4da6', placeholderTextColor: "blue",
                            justifyContent: "center", alignItems: 'center', width: 30
                        }} placeholder='1'
                            onChangeText={oneText => this.setState({ oneText })}
                            returnKeyType='done'
                        />
                        <View style={{ borderBottomColor: '#ff4da6', borderBottomWidth: 1, backgroundColor: '#000', height: 0.5, width: 30, marginTop: -6 }}></View>
                    </View>





                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 20, marginLeft: 20, marginRight: 20 }}>
                        <TextInput maxLength={1} returnKeyType={"next"} style={{
                            color: '#ff4da6', placeholderTextColor: "blue",
                            justifyContent: "center", alignItems: 'center', width: 30
                        }} placeholder='2'
                            onChangeText={twoText => this.setState({ twoText })}
                            returnKeyType='done'
                        />
                        <View style={{ borderBottomColor: '#ff4da6', borderBottomWidth: 1, backgroundColor: '#000', height: 0.5, width: 30, marginTop: -6 }}></View>
                    </View>

                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 20, marginLeft: 20, marginRight: 20 }}>
                        <TextInput maxLength={1} returnKeyType={"next"} style={{
                            color: '#ff4da6', placeholderTextColor: "blue",
                            justifyContent: "center", alignItems: 'center', width: 30
                        }} placeholder='3'
                            onChangeText={threeText => this.setState({ threeText })}
                        />
                        <View style={{ borderBottomColor: '#ff4da6', borderBottomWidth: 1, backgroundColor: '#000', height: 0.5, width: 30, marginTop: -6 }}></View>
                    </View>

                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 20, marginLeft: 20, marginRight: 20 }}>
                        <TextInput maxLength={1} returnKeyType={"next"} style={{
                            color: '#ff4da6', placeholderTextColor: "blue",
                            justifyContent: "center", alignItems: 'center', width: 30
                        }} placeholder='4'
                            onChangeText={fourText => this.setState({ fourText })}
                        />
                        <View style={{ borderBottomColor: '#ff4da6', borderBottomWidth: 1, backgroundColor: '#000', height: 0.5, width: 30, marginTop: -6 }}></View>
                    </View>

                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 20, marginLeft: 20, marginRight: 20 }}>
                        <TextInput maxLength={1} returnKeyType={"next"} style={{
                            color: '#ff4da6', placeholderTextColor: "blue",
                            justifyContent: "center", alignItems: 'center', width: 30
                        }} placeholder='5'
                            onChangeText={fiveText => this.setState({ fiveText })}
                        />
                        <View style={{ borderBottomColor: '#ff4da6', borderBottomWidth: 1, backgroundColor: '#000', height: 0.5, width: 30, marginTop: -6 }}></View>
                    </View>

                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 20, marginLeft: 20, marginRight: 20 }}>
                        <TextInput maxLength={1} returnKeyType={"next"} style={{
                            color: '#ff4da6', placeholderTextColor: "blue",
                            justifyContent: "center", alignItems: 'center', width: 30
                        }} placeholder='6'
                            onChangeText={sixText => this.setState({ sixText })}
                        />
                        <View style={{ borderBottomColor: '#ff4da6', borderBottomWidth: 1, backgroundColor: '#000', height: 0.5, width: 30, marginTop: -6 }}></View>
                    </View>

                </View> */}

                {this.state.oneTextError == true &&
                    <View>
                        <Text style={{ color: 'red', fontSize: 16, marginLeft: 15, marginTop: 10, fontFamily: futuraPtBook, }}> Please enter OTP </Text>
                    </View>
                }

                {/******************* Text 4 ************************* */}


                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 24, marginLeft: 20, marginRight: 20 }}>
                    <Text style={{ color: '#000', justifyContent: "center", alignItems: 'center', fontFamily: futuraPtBook, fontSize: 16, fontWeight: '300' }} >
                        I didn't receive a
                </Text>
                </View>


                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 6, marginLeft: 20, marginRight: 20 }}>
                    <Text style={{ color: '#000', justifyContent: "center", alignItems: 'center' }} >
                        code!
                </Text>
                </View>

                <TouchableOpacity
                    style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 6, marginLeft: 20, marginRight: 20 }}
                    onPress={() => { this.sendTac(contactNo) }}
                >
                    <Text style={{ color: '#ff4da6', justifyContent: "center", alignItems: 'center', fontSize: 18, fontFamily: futuraPtMedium }} >
                        Resend
                </Text>
                </TouchableOpacity>

                {/************************ Submit ******************** */}
                <View style={{ marginTop: 20, height: 50,  alignItems: 'center', }}>
                    <TouchableOpacity
                        style={{ backgroundColor: '#E4007C',width: '90%', justifyContent: "center", alignItems: 'center', height: 50 }}
                        onPress={() => { this.validation(contactNo) }} >
                        <Text style={{ color: 'white', fontSize: 20, fontFamily: futuraPtMedium }}>SUBMIT</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        );
    }
}
