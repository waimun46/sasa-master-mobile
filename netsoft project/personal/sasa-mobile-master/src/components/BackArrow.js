
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Platform, StyleSheet, Text, View,Image,ScrollView,TextInput,TouchableOpacity,ImageBackground,Dimensions} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
export default class BackArrow extends Component{
    static propTypes = {
        
        onPress : PropTypes.func.isRequired,
        height:PropTypes.number.isRequired,
        width:PropTypes.number.isRequired,
        marginLeft:PropTypes.number.isRequired,
        name:PropTypes.string.isRequired,
        color:PropTypes.string.isRequired,
        size:PropTypes.number.isRequired
        
       
      }
    render(){
        const { onPress,height,width,marginLeft,name,color,size } = this.props;
        return(
            <View style={{marginLeft:10}}>
                <TouchableOpacity onPress={onPress}>
                    <Icon height={20} width={40} marginLeft={10} name="arrow-back" color='white' size={25}
                    />
                    </TouchableOpacity>
                </View>
        )
    }
}
