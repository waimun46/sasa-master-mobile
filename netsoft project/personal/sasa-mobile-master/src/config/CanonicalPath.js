export default class CanonicalPath {
    static LOGIN               = "/api/user/login.php"
    static SEND_TAC            = "api/send_tac.php"
    static PHONE_VERIFY        = "api/user/phone_verify.php"
    static SIGN_UP             = "/api/user/sign_up.php"
    static ACTIVATE            = "/api/user/activate.php"
    static FORGOT_PASSWORD     = "/api/user/forgot_password.php"
    static UPDATE_PASSWORD     = "/api/user/update_password.php"

    static GET_DETAIL          = "/api/get_details.php"
    static LIKE_PRODUCT        = "api/products/like.php"
    static UNLIKE_PRODUCT      = "api/products/unlike.php"

    static CATEGORIES          = "/api/categories.php"
    static LIST_PRODUCT        = "/api/category.php"
    static IDS                 = "/api/ids.php"
    static REVIEWS             = "/api/reviews.php"

    static TRANSACTION_STORE   = "/api/store.php"
    static TRANSACTION_ONLINE  = "/api/online.php"
    static LIKED_PRODUCTS      = "/api/liked.php"
    static MY_REVIEWS          = "/api/my_reviews.php"
    static WRITE_REVIEWS       = "/api/products/review_create.php"

    static EVOUCHER            = "/api/vouchers.php"
    static PROMOTIONS          = "/api/promotions.php"

    static NOTIFICATIONS       = "/api/notifications.php"
    static UNREAD_COUNT        = "/api/notifications/unread_count"

    static LOCATIONS           = "/api/locations.php"

    static SUMMARY             = "/api/orders/summary.php"
    static ORDERS              = "/api/orders/order_create.php"
    static GET_PAYMENT_STATUS  = "/api/get_payment_status.php"

    static CONNECT_ONESIGNAL   = "/api/user/connect_onesignal.php"
    static ABOUTS              = "/api/abouts.php"

}
