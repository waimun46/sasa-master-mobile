
// export const BASE_URL         =    "http://cekapbuy.com/api/user/";
export const BASE_URL         =    'https://sasa.com.my/test/_adminCP/api/user/'

export const SIGNUP_URL       =    BASE_URL+"sign_up.php";
export const ACTIVATE_URL     =    BASE_URL+"activate.php";
export const PHONEVERIFY_URL  =    BASE_URL+"phone_verify.php";
export const UPDATEEMAIL_URL  =    BASE_URL+"update_email.php";
export const LOGIN_URL        =    BASE_URL+"login.php";
export const FORGOTPASS_URL   =    BASE_URL+"forgot_password.php";
export const UPDATEPASS_URL   =    BASE_URL+"update_password.php";
export const CONNECTFB_URL    =    BASE_URL+"connect_fb.php";
export const GETDETAILS_URL   =    BASE_URL+"get_details.php";
export const LOGOUT_URL       =    BASE_URL+"logout.php";
export const VOUCHER_URL      =    BASE_URL+"vouchers.php";
export const LOCATIONS_URL    =    BASE_URL+"locations.php";
export const PRODUCTLIST_URL  =    'https://sasa.com.my/test/_adminCP/api/category.php';
export const DASHBOARD_PRODUCT_URL= 'https://sasa.com.my/test/_adminCP/api/categories.php';
