import {StyleSheet, Dimensions, Platform} from 'react-native';
import { futuraPtMedium, futuraPtBook } from './styleText'

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
const OS = Platform.OS

const styles=StyleSheet.create({
    container:{
         backgroundColor:'#fff',
          flex:1
    },
  backgroundImage:{
      height:height
  },
  backarrowImage:{
    marginTop:30,
    marginLeft:10
  },
  logoContainer:{
    justifyContent:"center",
    alignItems:'center',
    marginTop:30
  },
  phoneInptContainer:{
    marginTop:10,
    marginBottom: OS == 'ios' ? 10 : 0,
    marginRight:10,
    marginLeft:10,


  },
  phoneText:{
    color:'#fff',
    marginLeft:10,
    marginRight:10,
    textAlign: 'center',
    marginTop:10,
    fontSize:16,
    fontFamily:futuraPtMedium

  },
  phoneBottomLine:{
    backgroundColor:'#D3D3D3',
    height:0.50,
    marginTop:10,
    marginRight:20,
    marginLeft:20
  },
  phoneErrorText:{
    color:'red',
    fontSize:16,
    marginLeft:20,
    fontFamily:futuraPtBook,
    fontWeight:'300'

  },
  passInptContainer:{

    marginTop:10,
    marginBottom: OS == 'ios' ? 10 : 0,
    marginRight:10,
    marginLeft:10

  },
  passText:{
    color:'#FFFFFF',
    marginLeft:10,
    marginRight:10,
    fontSize:16,
    textAlign: 'center',
    fontFamily:futuraPtMedium


  },
  passBottomLine:{
    backgroundColor:'#D3D3D3',
    height:0.50,
   marginTop:10,
    marginRight:20,
    marginLeft:20
  },
  passErrorText:{
    color:'red',
    fontSize:15,
    marginLeft:20,

  },
  signinContainer:{
    marginTop:20,
    marginLeft:20,
    marginRight:20,
    height:50
  },
  signinTouch:{
    backgroundColor:'#E4007C',
    justifyContent:"center",
    alignItems:'center',
    height:50
  },
  signinText:{
    color:'white',
    fontSize:20,
    fontFamily:futuraPtMedium,


  },
  forgotPassContainer:{
    justifyContent:'center',
    alignItems:'center'
  },
  forgotPassText:{
    color:'#fff',
    marginTop:20,
    justifyContent:'center',
    alignItems:'center',
    fontFamily:futuraPtMedium,
    fontSize:16,
    fontWeight:'300',
  },
  signinBottomLine:{
    backgroundColor:'#D3D3D3',
    height:0.40,
    marginTop:20,
    marginRight:0,
    marginLeft:0
  },
  activateContainer:{
    marginLeft:20,
    marginRight:20,

  },
  activateTouch:{
    backgroundColor:'#E4007C',
    marginTop:10,
    height:50,
    justifyContent:"center",
    alignItems:'center'
  },
  activateText:{
    color:'white',
    fontSize:20,
    fontFamily:futuraPtMedium,

  },
  fbContainer:{
    marginTop:10,
    marginLeft:20,
    marginRight:20,
  },
  fbTouch:{
    backgroundColor:'#5B7AB9',
    height:50,
    marginTop:10,
    justifyContent:"center",
    alignItems:'center',
    flexDirection:'row'
  },
  fbText:{
    color:'white',
    fontSize:20,
    fontFamily:futuraPtMedium,

    marginLeft:10,
    marginRight:10
  },
  fbIcon:{
    height:30,
    width:30
  },
  fbBottomLine:{
    backgroundColor:'#D3D3D3',
    height:0.40,
    marginTop:30,
    marginRight:10
  },
  signupContainer:{
    justifyContent:'center',alignItems:'center',flexDirection:'row'
  },
  signupText1:{
    color:'#fff',marginTop:20, fontFamily:futuraPtMedium, fontSize:16
  },
  signupText2:{
    color:'#fff',marginTop:20,fontSize:18,fontWeight:'bold', fontFamily:futuraPtMedium,
  },
  firstLogin:{

    color:'#fff',
    marginTop:20,
    justifyContent:'center',
    alignItems:'center',
    fontFamily:futuraPtMedium,
    fontSize:16,
    fontWeight:'300',
  },
  alertBox :{
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft:30,
    backgroundColor:'#FCE7ED',
    borderRadius:6,
    marginRight:30,
    marginTop:10,
    borderColor:'#f10b42',
    borderWidth:1

  },
  alertText:{
    color:'#f10b42',
    fontWeight: 'bold',
    marginTop:20,
    marginBottom: 30,
    fontSize:16
    },


});
export default styles;
