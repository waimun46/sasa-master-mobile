import {StyleSheet, Dimensions} from 'react-native';
import { futuraPtBook, futuraPtLight } from './styleText'
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
const styles=StyleSheet.create({
    container:{
     backgroundColor:'#fff',
      flex:1
    },
    nameText:{
        color:'#000000',
        marginTop: 16,
        marginLeft: 10,
        fontSize: 16,
        fontFamily:futuraPtLight,
        fontWeight:'300'
    },
    nameInputText:{
        marginLeft: 13,
        fontSize:16,
        fontFamily:futuraPtBook,
        fontWeight:'300',
        color:'#000'


    },
    nameBottomLine:{
        backgroundColor:'#D3D3D3',
        height:0.50,
        marginRight:20,
        marginLeft:20
   },
    nameError:{
        color:'red',
        fontSize:16,
        marginLeft:13,
        fontFamily:futuraPtBook,
        fontWeight:'300'

    },
    phoneText:{
        color:'#000000',
        marginTop: 16,
        marginLeft: 10,
        fontSize: 16,
        fontFamily:futuraPtLight,
        fontWeight:'300'
    },
    phoneInputText:{
        marginLeft: 13,
        fontFamily:futuraPtBook,
        fontWeight:'300',
        color:'#000',
        fontSize:16,

   },
    phoneBottomLine:{
        backgroundColor:'#D3D3D3',
        height:0.50,
        marginRight:20,
        marginLeft:20


    },
    phoneError:{
        color:'red',
        fontSize:16,
        marginLeft:13,
        fontFamily:futuraPtBook,
        fontWeight:'300'

    },
    emailText:{
        color:'#000000',
        marginTop: 16,
        marginLeft: 10,
        fontSize: 16,
        fontFamily:futuraPtLight,
        fontWeight:'300'

    },
    emailInputText:{
        marginLeft: 13,
        fontFamily:futuraPtBook,
        fontWeight:'300',
        color:'#000' ,
        fontSize:16,

   },
    emailBottomLine:{
        backgroundColor:'#D3D3D3',
        height:0.50,
        marginRight:20,
        marginLeft:20


    },
    emailError:{
        color:'red',
        fontSize:16,
        marginLeft:13,
        fontFamily:futuraPtBook,
        fontWeight:'300'

    },
    passText:{
        color:'#000000',
        marginTop: 16,
        marginLeft: 10,
        fontSize: 16,
        fontFamily:futuraPtLight,
        fontWeight:'300'

    },
    passInputText:{
        marginLeft: 13,
        fontFamily:futuraPtBook,
        fontWeight:'300',
        color:'#000',
        fontSize:16,

   },
    passBottomLine:{
        backgroundColor:'#D3D3D3',
        height:0.50,
        marginRight:20,
        marginLeft:20


    },
    passError:{
        color:'red',
        fontSize:16,
        marginLeft:13,
        fontFamily:futuraPtBook,
        fontWeight:'300'

    },
    confirmpassText:{
        color:'#000000',
        marginTop: 16,
        marginLeft: 10,
        fontSize: 16,
        fontFamily:futuraPtLight,
        fontWeight:'300'

    },
    confirmpassInputText:{
        marginLeft: 13,
        fontFamily:futuraPtBook,
        fontWeight:'300',
        color:'#000' ,
        fontSize:16,


  },
    confirmpassBottomLine:{
        backgroundColor:'#D3D3D3',
        height:0.50,
        marginRight:20,
        marginLeft:20


    },
    confirmpassError:{
        color:'red',
        fontSize:16,
        marginLeft:13,
        fontFamily:futuraPtBook,
        fontWeight:'300'

    },
    Container1:{
        flex: 1,
        flexDirection: 'row'
    },
    checkboxContainer1:{
        marginTop: 16,
        marginLeft: 10,
        marginRight: 10
    },
    checkboxStyle1:{
        backgroundColor: '#f2f2f2',
        color:'#900',
        borderRadius: 5,
        marginTop: 16,
        marginLeft: 10,
        marginRight: 10
    },
    terms1:{
        marginTop: 16,
        marginLeft: 10,
        marginRight: 10
    },
    Container2:{
        flex: 1,
        flexDirection: 'row'
    },
    checkboxContainer2:{
        marginTop: 16,
        marginLeft: 10,
        marginRight: 10
    },
    checkboxStyle2:{
        backgroundColor: '#f2f2f2',
        color:'#900',
        borderRadius: 5,
        marginTop: 16,
        marginLeft: 10,
        marginRight: 10
    },
    terms2:{
        marginTop: 16,
        marginLeft: 10,
        marginRight: 10
    },
    submitContainer:{
       flex:1,
       marginLeft:20,
       marginRight:20,
       height:50
    },
    submitTouch:{
        backgroundColor:'#E4007C',
        justifyContent:"center",
        alignItems:'center',
        height:50
    },
    submitText:{
        color:'white',fontSize:18,
    }

})
export default styles;
