import {StyleSheet, Dimensions} from 'react-native';
import { futuraPtMedium } from './styleText'

const dimWidth= Dimensions.get('window').width
const dimHeight= Dimensions.get('window').height
const buttonColor='#E4007C'
const textColor='#fff'
const backgroundColor='#fff'
const newBackgroundColor='#C0C0C0'

const styles=StyleSheet.create({
    container:{
        height:dimHeight,
        marginTop:20
    },
  containerBackground:{
         backgroundColor:backgroundColor
    },
    blankView:{
        marginTop:4,marginBottom:4,backgroundColor:'#C0C0C0',height:20
    },

     viewText:{

        justifyContent:"center",
        alignItems:'center',
        marginTop:10

    },
    styleText:{
        marginTop: 10,
        color:'#111111',
        fontSize:20,
        fontWeight:'300',
        fontFamily:futuraPtMedium
    },
    logoImg:{
        width: dimWidth,
        marginBottom: 10
    },
    optionView:{
        flex:1 ,
        marginTop:20,
        flexDirection: 'row' ,
        justifyContent:'space-between',
        marginRight:20,
        marginLeft:20
    },
    optionView1:{
        flex:1 ,
        marginTop:20,
        flexDirection: 'row' ,
        justifyContent:'space-between',
        marginRight:20,
        marginLeft:20
    },
    equalView:{
        width: dimWidth/3,
         justifyContent:"center",
        alignItems:'center'
    },
      viewButton:{
        marginTop:20,
        marginLeft:20,
        marginRight:20,
        height:50,
        backgroundColor:buttonColor
    },
    textButton:{
        color:textColor,
        fontSize:20,
        fontFamily:futuraPtMedium,
        fontWeight:'300'
    },

  touch:{
      justifyContent:"center",
      alignItems:'center',
      height:50
  }
});
export default styles;
