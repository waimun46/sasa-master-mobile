
// SELECTING ALL TEXT ELEMENTS
var username = document.forms['vform']['username'];
var email = document.forms['vform']['email'];
var password = document.forms['vform']['password'];
var password_confirm = document.forms['vform']['password_confirm'];
var isValid = true;

// SELECTING ALL ID
var userId = document.getElementById('username_div');
var emailId = document.getElementById('email_div');
var passwordId = document.getElementById('password_div');
var password_confirm_Id = document.getElementById('password_confirm_div');

// SELECTING ALL ERROR DISPLAY ELEMENTS
var name_error = document.getElementById("name_error");
var email_error = document.getElementById("email_error");
var password_error = document.getElementById("password_error");
var password_error2 = document.getElementById("password_error2");
var password_confirm_error = document.getElementById("password_error3");


// SETTING ALL EVENT LISTENERS
username.addEventListener('blur', nameVerify, true);
email.addEventListener('blur', emailVerify, true);
password.addEventListener('blur', passwordVerify, true);
password_confirm.addEventListener('blur', confirmVerify, true);


function Validate() {

    // validation username
    if(username.value == null || username.value == "") {
        username.style.border = "1px solid red";
        userId.style.color = "red";
        name_error.style.visibility = "visible";
        name_error.textContent = "username is required";
        isValid = false;
    }
    else {
        name_error.style.visibility = "hidden";
    }

    // validation email
    if (email.value == null || email.value == "") {
        email.style.border = "1px solid red";
        emailId.style.color = "red";
        email_error.style.visibility = "visible";
        email_error.innerHTML = "email is required"
        isValid = false;
    }
    else {
        email_error.style.visibility = "hidden";
    }

    // validation password
    if (password.value == null || password.value == "") {
        password.style.border = "1px solid red";
        passwordId.style.color = "red";
        password_error2.style.visibility = "visible";
        password_error2.innerHTML = "password is required"
        isValid = false;
    }
    else {
        password_error2.style.visibility = "hidden";
    }

    // validation password_confirm
    if (password_confirm.value == null || password_confirm.value == "") {
        password_confirm.style.border = "1px solid red";
        password_confirm_Id.style.color = "red";
        password_error.style.visibility = "visible";
        password_confirm_error.innerHTML = "password confirm is required";
        isValid = false;
    }
    else {
        password_confirm_error.innerHTML = "";
        password_error.style.visibility = "hidden";
    }

    // check if the two passwords match
    if (password.value != password_confirm.value) {
        password_confirm.style.border = "1px solid red";
        password_confirm_Id.style.color = "red";
        password_error.style.visibility = "visible";
        password_error.innerHTML = "The two passwords do not match"
        isValid = false;

        if(password_confirm.value == null ||password_confirm.value == ""){
            password_error.innerHTML = "";
        }
    }
    else {
        password_error.style.visibility = "hidden";
    }
    
    return isValid;

}


// event handler functions
function nameVerify() {
    if (username.value != "") {
        username.style.border = "1px solid #5e6e66";
        userId.style.color = "#5e6e66";
        name_error.innerHTML = "";
        return true;
    }
}
function emailVerify() {
    if (email.value != "") {
        email.style.border = "1px solid #5e6e66";
        emailId.style.color = "#5e6e66";
        email_error.innerHTML = "";
        return true;
    }
}
function passwordVerify() {
    if (password.value != "") {
        password.style.border = "1px solid #5e6e66";
        password_confirm.style.border = "1px solid #5e6e66";
        password_confirm_Id.style.color = "#5e6e66";
        passwordId.style.color = "#5e6e66";
        password_error.innerHTML = "";
        return true;
    }
    if (password.value === password_confirm.value) {
        password.style.border = "1px solid #5e6e66";
        password_confirm_Id.style.color = "#5e6e66";
        password_error.innerHTML = "";
        return true;
    }
}

function confirmVerify() {
    if (password_confirm.value != "") {
        password_confirm.style.border = "1px solid #5e6e66";
        password_confirm_Id.style.color = "#5e6e66";
        password_error.innerHTML = "";
        return true;
    }
}

