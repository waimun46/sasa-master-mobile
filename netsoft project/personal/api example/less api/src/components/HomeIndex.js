import React, {Component} from 'react';
import {browserHistory} from 'react-router';

class HomeIndex extends Component {

    tablemap(){
        browserHistory.push('tablemap');
    }
    pagintable(){
        browserHistory.push('pagintable');
    }
    anttable(){
        browserHistory.push('anttable');
    }

    render() {
        return (
            <div className="HomeIndex">
                <h1>Welcome to API</h1>
                <button type="button" onClick={this.tablemap}>TableMap</button>
                <button type="button" onClick={this.pagintable}>PaginTable</button>
                <button type="button" onClick={this.anttable}>AntTable</button>
                <button type="button">TableMap</button>
            </div>
        );
    }
}

export default HomeIndex;
