import React, {Component} from 'react';
import '../../node_modules/datatables.net-dt/css/jquery.dataTables.css';

const $ = require('jquery');
$.DataTable = require('datatables.net');


class PaginTable extends Component {
    
    componentDidMount() {

        $(document).ready(function () {
            $('#myTable').DataTable({
                "processing": true,
                "ajax": {
                    url: 'https://jsonplaceholder.typicode.com/photos',
                    dataSrc: ''
                },
                "columns": [
                    {data: "id"},
                    {data: "title"},
                    {data: "thumbnailUrl"}
                ],
                "columnDefs": [{
                    "targets": 2,
                    "render": function (url, type, full) {
                        return '<img src="' + url + '"/>';
                    }
                }]

            });
        });
    }


    render() {
        return (
            <div className="PaginTable">
                <table id='myTable' className="display">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Position</th>
                            <th>Office</th>
                        </tr>
                    </thead>
                </table>
            </div>
        );
    }
}

export default PaginTable;
