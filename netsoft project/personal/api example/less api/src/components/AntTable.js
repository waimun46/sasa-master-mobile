import React, {Component} from 'react';
import {Table, Select} from 'antd';
import {PostApi} from '../PostApi';

const Option = Select.Option;



const columns = [
    {
        title: 'id',
        dataIndex: 'id',
    },
    {
        title: 'title',
        dataIndex: 'title',
    },
    {
        title: 'userId',
        dataIndex: 'userId',
    }
];


class AntTable extends Component {


    constructor(props) {
        super(props);
        this.state = {
            data: [],
            pagination: {},
            completed: true
        }
    }

    componentDidMount() {
        this.dataPost();
    }

    handleTableChange = (pagination, filters, sorter) => {
        const pager = {...this.state.pagination};
        pager.current = pagination.current;
        this.setState({
            pagination: pager,
        });
        this.dataPost({
            results: pagination.pageSize,
            page: pagination.current,
            sortField: sorter.field,
            sortOrder: sorter.order,
            ...filters,
        });

    }

    handleChange = () => {
        let paramsval = {
            completed : true
        }
        PostApi('todos', paramsval, this.props)
            .then((fetchData) => {
                console.log(fetchData);


                
                this.setState({
                    data: fetchData.completed,
                })
            })
    }
    
    dataPost = (params = {}) => {
        console.log('params:', params);
        this.setState({loading: true});
        PostApi('todos', {
            data: {
                ...params,
            }
        })
            .then((fetchData) => {
                //console.log(fetchData);
                const pagination = {...this.state.pagination};
                pagination.total = 200;
                this.setState({
                    loading: false,
                    data: fetchData,
                    pagination,
                })
            })
    }


    render() {
        return (
            <div className="AntTable">
                <div className="select_sty">
                    <Select labelInValue defaultValue={{ key: 'All' }} >
                        <Option value="True" onClick={this.handleChange}>True</Option>
                        <Option value="False" onClick={this.handleChange}>False</Option>
                        <Option value="" onClick={this.handleChange}>All</Option>
                    </Select>
                </div>
                <Table
                    columns={columns}
                    dataSource={this.state.data}
                    bordered
                    rowKey={record => record.id}
                    onChange={this.handleTableChange}
                    loading={this.state.loading}
                    pagination={this.state.pagination}
                    title={() => 'Ant Table Api'}
                    footer={() => 'Footer'}
                />
            </div>
        );
    }
}

export default AntTable;
