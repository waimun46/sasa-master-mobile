import RNLanguages from 'react-native-languages';
import i18n from 'i18n-js';
import { AsyncStorage } from "react-native"

import en from './translations/en.json';
import zh from './translations/zh.json';

determinLanguage = async () => {
    try{
      global.language = await AsyncStorage.getItem("language")
    }catch(error){
      global.language = "en"
      console.error(error)
    }
    console.info("== language is : " + global.language)
    // i18n.locale = language
  }
  
  determinLanguage()
  
  // i18n.locale = RNLanguages.language
  // 这里只是一个默认的语言. 具体的语言还是需要靠上面的 global.language 来确定。
  // 这里无法通过 async 方法来设置 i18n.locale
  // 在后续的所有页面的渲染中，都要使用  i18n.t('title', { locale: global.language})
  i18n.fallbacks = true;
  i18n.translations = { en, zh };
  
  // 记得这里必须是 locale!
  global.t = (key) => {
    return i18n.t(key, { locale: global.language })
  }
  
  export default i18n;
