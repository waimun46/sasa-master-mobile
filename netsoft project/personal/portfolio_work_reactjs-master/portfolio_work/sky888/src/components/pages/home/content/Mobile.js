import React, {Component} from 'react';
import img01 from '../../../../images/m2.png';
import img02 from '../../../../images/ad.png';
import img03 from '../../../../images/ios.png';

class Mobile extends Component {
    render() {
        return (
            <section id="Mobile">
                <div className="mobile_inner">
                    <div className="mb_img">
                        <img src={img01} alt="img01"/>
                    </div>
                    <div className="mb_content">
                        <div className="mb_con_warp" >
                            <h1>DOWNLOAD YOUR FIRST APP</h1>
                            <p>THE MOST DIVERSE BETTING GAME PLATFORM</p>
                            <ul>
                                <li>High-quality gaming platform.</li>
                                <li>Diverse betting platform.</li>
                                <li>Safe, simple, fast and worth to trust!</li>
                                <li>User's most favorite cash network.</li>
                            </ul>
                            <div className="mb_icon">
                                <div className="mb_icon_mg">
                                <div className="mb_dw">
                                    <img src={img02} alt="img02"/>
                                </div>
                                <div className="mb_dw">
                                    <img src={img03} alt="img03"/>
                                </div>
                                <div className="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="clearfix"></div>
                </div>
            </section>
        );
    }
}

export default Mobile;
