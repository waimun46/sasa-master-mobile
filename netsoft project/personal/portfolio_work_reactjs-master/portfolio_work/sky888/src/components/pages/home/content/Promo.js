import React, {Component} from 'react';
import img01 from '../../../../images/promo-1.jpg';
import img02 from '../../../../images/promo-2.jpg';
import img03 from '../../../../images/mall.jpg';
import img04 from '../../../../images/promo-4.jpg';
import img05 from '../../../../images/promo-5.jpg';
import img06 from '../../../../images/promo-6.jpg';

class Promo extends Component {
    render() {
        return (
            <section id="Promo">
                <div className="pro_warp">
                    <div className="item">
                        <div className="item_img">
                            <img src={img01} alt="img01"/>
                        </div>
                    </div>
                    <div className="item">
                        <div className="item_img">
                            <img src={img02} alt="img01"/>
                        </div>
                    </div>
                    <div className="item">
                        <div className="item_img">
                            <img src={img03} alt="img01"/>
                        </div>
                    </div>
                    <div className="item">
                        <div className="item_img">
                            <img src={img04} alt="img01"/>
                        </div>
                    </div>
                    <div className="item">
                        <div className="item_img">
                            <img src={img05} alt="img01"/>
                        </div>
                    </div>
                    <div className="item">
                        <div className="item_img">
                            <img src={img06} alt="img01"/>
                        </div>
                    </div>
                    <div className="clearfix"></div>
                </div>
            </section>
        );
    }
}

export default Promo;
