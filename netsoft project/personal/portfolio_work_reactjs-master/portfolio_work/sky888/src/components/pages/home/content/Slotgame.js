import React, {Component} from 'react';
import SlotGame from './content/SlotGame';
import Online from './content/Online';
import P2P from './content/P2P';


class Slotgame extends Component {


    render() {
        return (
            <section id="Slotgame">
                <article>
                    <header>
                        <h1>Game List</h1>
                        <div className="top_warp" style={{margin: 'auto'}}>
                            <div className="marWarp">
                                <div className="marquee">
                                    <span>Dear member, I am very sorry, because the current SMS channel is abnormal, it does not affect the normal game and charge.</span>
                                </div>
                            </div>
                            <div style={{clear: 'both'}}></div>
                        </div>
                        <div style={{clear: 'both'}}></div>
                    </header>

                    <div className="cd-tabs js-cd-tabs">
                        <nav>
                            <ul className="cd-tabs__navigation js-cd-navigation">
                                <li>
                                    <a data-content="inbox" className="cd-selected" href="#0">
                                        Slot Game
                                    </a>
                                </li>
                                <li>
                                    <a data-content="new" href="#0">
                                        Online Casino
                                    </a>
                                </li>
                                <li>
                                    <a data-content="gallery" href="#0">
                                        P2P
                                    </a>
                                </li>
                            </ul>
                        </nav>

                        <ul className="cd-tabs__content js-cd-content">
                            <li data-content="inbox" className="cd-selected">
                                <SlotGame/>
                            </li>

                            <li data-content="new">
                                <Online/>
                            </li>

                            <li data-content="gallery">
                                <P2P/>
                            </li>
                        </ul>
                    </div>


                </article>
            </section>
        );
    }
}

export default Slotgame;
