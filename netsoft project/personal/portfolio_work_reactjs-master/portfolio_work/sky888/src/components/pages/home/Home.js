import React, {Component} from 'react';
import Banner from './content/Banner';
import Promo from './content/Promo';
import Slotgame from './content/Slotgame';
import Mobile from './content/Mobile';
import SlotIntro from './content/SlotIntro';

class Home extends Component {
    render() {
        return (
            <div id="Home">
                <Banner/>
                <Promo/>
                <Slotgame/>
                <Mobile/>
                <SlotIntro/>
            </div>
        );
    }
}

export default Home;
