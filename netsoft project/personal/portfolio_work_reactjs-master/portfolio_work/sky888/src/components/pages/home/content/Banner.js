import React, {Component} from 'react';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';

import img01 from '../../../../images/banner-01.jpg';
import img02 from '../../../../images/banner-03.jpg';
import img03 from '../../../../images/banner-10.jpg';
import img01m from '../../../../images/banner-01m.jpg';
import img02m from '../../../../images/banner-03m.jpg';
import img03m from '../../../../images/banner-10m.jpg';

class Banner extends Component {
    render() {
        return (
            <section id="Banner">
                <OwlCarousel
                    className="owl-carousel owl-theme"
                    loop={true}
                    autoplay={true}
                    nav={false}
                    dots={false}
                    items={1}
                >
                    <div className="item">
                        <img className="banner_a" src={img01} alt="img01"/>
                        <img className="banner_m" src={img01m} alt="img01m"/>
                    </div>
                    <div className="item">
                        <img className="banner_a" src={img02} alt="img02"/>
                        <img className="banner_m" src={img02m} alt="img02m"/>
                    </div>
                    <div className="item">
                        <img className="banner_a" src={img03} alt="img03"/>
                        <img className="banner_m" src={img03m} alt="img03m"/>
                    </div>
                </OwlCarousel>
            </section>
        );
    }
}

export default Banner;
