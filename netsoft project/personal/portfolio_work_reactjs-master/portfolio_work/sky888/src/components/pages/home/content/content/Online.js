import React, {Component} from 'react';
import img02 from '../../../../../images/slot/2.png';
import img04 from '../../../../../images/slot/4.png';
import img06 from '../../../../../images/slot/6.png';
import img08 from '../../../../../images/slot/8.png';


class Online extends Component {
    render() {
        return (
            <div className="Online">
                <div className="slot_warp">
                    <img src={img02} alt="img01"/>
                </div>
                <div className="slot_warp">
                    <img src={img04} alt="img01"/>
                </div>

                <div className="slot_warp">
                    <img src={img06} alt="img01"/>
                </div>
                <div className="slot_warp">
                    <img src={img08} alt="img01"/>
                </div>
                <div className="clearfix"></div>
            </div>
        );
    }
}

export default Online;
