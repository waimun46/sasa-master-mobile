import React, {Component} from 'react';
import $ from 'jquery';

class Menu2 extends Component {

    render() {
        return (
            <div className="Menu2">
                <div class="menu-container">
                    <div class="menu">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="http://marioloncarek.com">About</a>
                                <ul>
                                    <li><a href="http://marioloncarek.com">School</a>
                                        <ul>
                                            <li><a href="http://marioloncarek.com">Lidership</a></li>
                                            <li><a href="#">History</a></li>
                                            <li><a href="#">Locations</a></li>
                                            <li><a href="#">Careers</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Study</a>
                                        <ul>
                                            <li><a href="#">Undergraduate</a></li>
                                            <li><a href="#">Masters</a></li>
                                            <li><a href="#">International</a></li>
                                            <li><a href="#">Online</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Research</a>
                                        <ul>
                                            <li><a href="#">Undergraduate research</a></li>
                                            <li><a href="#">Masters research</a></li>
                                            <li><a href="#">Funding</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Something</a>
                                        <ul>
                                            <li><a href="#">Sub something</a></li>
                                            <li><a href="#">Sub something</a></li>
                                            <li><a href="#">Sub something</a></li>
                                            <li><a href="#">Sub something</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="http://marioloncarek.com">News</a>
                                <ul>
                                    <li><a href="http://marioloncarek.com">Today</a></li>
                                    <li><a href="#">Calendar</a></li>
                                    <li><a href="#">Sport</a></li>
                                </ul>
                            </li>
                            <li><a href="http://marioloncarek.com">Contact</a>
                                <ul>
                                    <li><a href="#">School</a>
                                        <ul>
                                            <li><a href="#">Lidership</a></li>
                                            <li><a href="#">History</a></li>
                                            <li><a href="#">Locations</a></li>
                                            <li><a href="#">Careers</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Study</a>
                                        <ul>
                                            <li><a href="#">Undergraduate</a></li>
                                            <li><a href="#">Masters</a></li>
                                            <li><a href="#">International</a></li>
                                            <li><a href="#">Online</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Study</a>
                                        <ul>
                                            <li><a href="#">Undergraduate</a></li>
                                            <li><a href="#">Masters</a></li>
                                            <li><a href="#">International</a></li>
                                            <li><a href="#">Online</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Empty sub</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
   
    );
    }
    }

    export default Menu2;
