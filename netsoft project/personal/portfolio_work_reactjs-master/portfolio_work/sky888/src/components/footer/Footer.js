import React, {Component} from 'react';


class Footer extends Component {
    render() {
        return (
            <footer id="Footer">
                <p>Copyright © 2018 SKY888. All Rights Reserved.</p>
            </footer>
        );
    }
}

export default Footer;
