import React from 'react';
import ReactDOM from 'react-dom';
import {Route, Router, browserHistory, IndexRoute} from 'react-router';
import App from './App';
import Home from './components/pages/home/Home';


ReactDOM.render(
    <Router history={browserHistory}>
        <Route path="/" component={App}>
            <IndexRoute component={Home}/>
        </Route>
    </Router>,
    document.getElementById('root'));

