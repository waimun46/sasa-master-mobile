import React, {Component} from 'react';
import {Link} from 'react-router';
import r01 from '../../../../images/r01.jpg';
import a02 from '../../../../images/a02.jpg';
import a03 from '../../../../images/a03.jpg';
import a04 from '../../../../images/a04.jpg';


class ActionsMovie extends Component {
    render() {
        return (
            <div className="ActionsMovie">
                <div className="top_01">
                    <header>
                        <h4>Action Movie</h4>
                        <Link to="action">
                            <span className="span_home">view more<i className="fal fa-angle-double-right"></i></span>
                        </Link>
                        <div className="clearfix"></div>
                    </header>

                    <Link to="actionvideo">
                        <div className="card text-center">
                            <img className="card-img-top" src={r01} alt="r01"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="actionvideo">
                        <div className="card text-center">
                            <img className="card-img-top" src={a02} alt="a02"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="actionvideo">
                        <div className="card text-center">
                            <img className="card-img-top" src={a03} alt="a03"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="actionvideo">
                        <div className="card text-center">
                            <img className="card-img-top" src={a04} alt="a04"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <div className="clearfix"></div>
                </div>
            </div>
        );
    }
}

export default ActionsMovie;
