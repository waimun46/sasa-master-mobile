import React, {Component} from 'react';
import {Link} from 'react-router';

class SingupPage extends Component {
    render() {
        return (
            <div id="SingupPage">
                <div className="sign_sy">
                    <Link to="/">
                        <i className="far fa-long-arrow-left sing_i"></i>
                    </Link>
                    <h3>Signup Today</h3>
                    <div className="clearfix"></div>
                </div>
                <form id="login-form" className="col-lg-12">

                    <div className="group">
                        <input type="text" name="kullanici_adi" required/>
                        <span className="highlight"></span>
                        <span className="bar"></span>
                        <label>
                            <i className="fal fa-user"></i>
                            <span className="span-input">Username</span>
                        </label>
                    </div>

                    <div className="group">
                        <input type="password" name="sifre" required/>
                        <span className="highlight"></span>
                        <span className="bar"></span>
                        <label>
                            <i className="far fa-envelope"></i>
                            <span className="span-input">Email</span>
                        </label>
                    </div>
                    <div className="group">
                        <input type="password" name="sifre" required/>
                        <span className="highlight"></span>
                        <span className="bar"></span>
                        <label>
                            <i className="fal fa-unlock-alt"></i>
                            <span className="span-input">Password</span>
                        </label>
                    </div>
                    <div className="group">
                        <input type="password" name="sifre" required/>
                        <span className="highlight"></span>
                        <span className="bar"></span>
                        <label>
                            <i className="fal fa-unlock-alt"></i>
                            <span className="span-input">Password Confirmation</span>
                        </label>
                    </div>

                    <div className="sign_btn">
                        <Link to="home">
                            <button type="submit" className="giris-yap-buton">Submit</button>
                        </Link>
                    </div>


                </form>
            </div>
        );
    }
}

export default SingupPage;
