import React, {Component} from 'react';
import {Link} from 'react-router';
import logo from '../../../images/logo.png'

class LoginPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',

        };
        this.onChange = this.onChange.bind(this);

    }
    onChange(e) {
        this.setState({[e.target.name]: e.target.value});
    }
     
    render() {
        return (
            <div id="LoginPage"  className="login">
                <form id="login-form" className="col-lg-12" action="" method="">

                    <div className="col-lg-12 logo-kapsul logo_login">
                        <img width="200" className="logo" src={logo} alt="logo" draggable="false"/>
                    </div>
                    <div className="clearfix"></div>
                    <span className="text-danger"></span>
                    <span className="text-info"></span>
                    <div className="group">
                        <input
                            id="name"
                            type="text"
                            name="kullanici_adi"
                            onChange={this.onChange}
                            />
                            <span className="highlight"></span>
                            <span className="bar"></span>
                            <label>
                                <i className="fal fa-user"></i>
                                <span className="span-input">Username</span>
                            </label>
                    </div>

                    <div className="group">
                        <input
                            id="pass"
                            type="password"
                            name="sifre"
                            onChange={this.onChange}
                            />
                            <span className="highlight"></span>
                            <span className="bar"></span>
                            <label>
                                <i className="fal fa-unlock-alt"></i>
                                <span className="span-input">Password</span>
                            </label>
                    </div>

                    <Link to="home">
                        <button type="submit" className="giris-yap-buton" >Login</button>
                    </Link>

                    <div className="forgot-and-create tab-menu " >
                        <span>Dont't have an account ? </span>
                        <Link to="signup">Signup Now.</Link>
                    </div>
                </form>
            </div>
        );
    }
}

export default LoginPage;
