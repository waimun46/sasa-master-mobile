import React, {Component} from 'react';
import {Badge} from 'antd-mobile';
import {Link} from 'react-router';
import img01 from '../../../../images/img01.jpg';
import img02 from '../../../../images/img02.jpg';
import img03 from '../../../../images/img03.jpg';
import img04 from '../../../../images/img04.jpg';

class HomeNew extends Component {
    render() {
        return (
            <article className="HomeNew">
                <div className="top_01">
                    <header>
                        <h4>New</h4>
                    </header>

                    <Link to="video">
                        <div className="card text-center">
                            <img className="card-img-top" src={img02} alt="img01"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="video">
                        <div className="card text-center">
                            <img className="card-img-top" src={img01} alt="img02"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="video01">
                        <div className="card text-center">
                            <Badge text="EP 10"
                                   style={{
                                       marginLeft: 12,
                                       padding: '0 3px',
                                       backgroundColor: '#e91e63',
                                       borderRadius: 2
                                   }}/>
                            <img className="card-img-top" src={img03} alt="img03"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="video">
                        <div className="card text-center">
                            <Badge text="EP 08"
                                   style={{
                                       marginLeft: 12,
                                       padding: '0 3px',
                                       backgroundColor: '#e91e63',
                                       borderRadius: 2
                                   }}/>
                            <img className="card-img-top" src={img04} alt="img04"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <div className="clearfix"></div>
                </div>
            </article>
        );
    }
}

export default HomeNew;
