import React, {Component} from 'react';
import PropTypes from 'prop-types';
import YouTube from 'react-youtube';
import Menu from '../../../menu/Menu';
import ComedySlider from './ComedySlider'
import ComedyRecom from './ComedyRecom'

class ComedyVideo extends Component {
    render() {
        const opts = {
            playerVars: { // https://developers.google.com/youtube/player_parameters
                autoplay: 1
            }
        };
        return (
            <div>
                <main>
                    <section id="VideoPage">
                        <div className="top">
                            <YouTube
                                videoId="D86RtevtfrA"
                                opts={opts}
                                onReady={this._onReady}
                            />
                            <div className="title">
                                <p>Title</p>
                            </div>
                                <div className="arrow_back" onClick={this.context.router.goBack}>
                                    <i className="far fa-long-arrow-left"></i>
                                </div>

                            <div className="title_content">
                                <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    Lorem Ipsum has been the industry's standard dummy.
                                </p>
                            </div>
                            <div className="clearfix"></div>
                        </div>
                        <div className="bottom">
                            <ComedySlider/>
                            <hr/>
                            <ComedyRecom/>
                        </div>
                    </section>
                </main>
                <Menu/>
            </div>
        );
    }

    _onReady(event) {
        // access to player in all event handlers via event.target
        event.target.pauseVideo();
    }
}
ComedyVideo.contextTypes = {
    router: PropTypes.object.isRequired
}
export default ComedyVideo;


