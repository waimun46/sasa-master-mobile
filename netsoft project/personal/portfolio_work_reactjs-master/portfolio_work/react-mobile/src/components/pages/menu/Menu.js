import React, {Component} from 'react';
import $ from 'jquery';
import {Link} from 'react-router';


class Menu extends Component {
    componentDidMount() {

        $(function() {
            let pgurl = window.location.href.substr(window.location.href
                    .lastIndexOf("/") + 1);
            $("#Menu li a").each(function() {
                if ($(this).attr("href") === pgurl || $(this).attr("href") === '')
                    $(this).addClass("active");
            })
        });
    }

    render() {
        return (
            <div id="Menu">
                <nav>
                    <ul className="nav">
                        <li >
                            <Link to="home" >
                                <i className="fas fa-home"></i>
                            </Link>
                        </li>

                        <li >
                            <Link to="profile">
                                <i className="fas fa-user"></i>
                            </Link>
                        </li>


                        <li >
                            <Link to="/">
                                <i className="fas fa-sign-out-alt"></i>
                            </Link>
                        </li>
                    </ul>
                </nav>
            </div>
        );
    }
}

export default Menu;
