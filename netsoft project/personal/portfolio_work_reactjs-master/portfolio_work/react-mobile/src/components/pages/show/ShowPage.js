import React, {Component} from 'react';
import Reality from './content/Reality'
import Musical from './content/Musical'
import Award from './content/Award'


class ShowPage extends Component {
    render() {
        return (
            <article id="ShowPage">
                <Reality/>
                <Musical/>
                <Award/>
            </article>
        );
    }
}

export default ShowPage;
