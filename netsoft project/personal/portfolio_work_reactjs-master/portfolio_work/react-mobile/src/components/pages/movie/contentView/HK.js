import React, {Component} from 'react';
import TitleMenu from '../../menu/TitleMenu'
import {Link} from 'react-router';
import Menu from '../../menu/Menu';
import m02 from '../../../../images/m05.jpg';

class HK extends Component {
    render() {
        return (
            <section id="HK">
                <article>
                    <TitleMenu title="HK Movie"/>
                    <main>
                        <div className="top_01">
                            <Link to="hkvideo">
                                <div className="card text-center">
                                    <img className="card-img-top" src={m02} alt="m02"/>
                                    <div className="card-body" id="card">
                                        <h5 className="card-title">Title</h5>
                                    </div>
                                </div>
                            </Link>
                            <Link to="hkvideo">
                                <div className="card text-center">
                                    <img className="card-img-top" src={m02} alt="m02"/>
                                    <div className="card-body" id="card">
                                        <h5 className="card-title">Title</h5>
                                    </div>
                                </div>
                            </Link>
                            <Link to="hkvideo">
                                <div className="card text-center">
                                    <img className="card-img-top" src={m02} alt="m02v"/>
                                    <div className="card-body" id="card">
                                        <h5 className="card-title">Title</h5>
                                    </div>
                                </div>
                            </Link>
                            <Link to="hkvideo">
                                <div className="card text-center">
                                    <img className="card-img-top" src={m02} alt="m02"/>
                                    <div className="card-body" id="card">
                                        <h5 className="card-title">Title</h5>
                                    </div>
                                </div>
                            </Link>
                            <Link to="hkvideo">
                                <div className="card text-center">
                                    <img className="card-img-top" src={m02} alt="m02"/>
                                    <div className="card-body" id="card">
                                        <h5 className="card-title">Title</h5>
                                    </div>
                                </div>
                            </Link>
                            <Link to="hkvideo">
                                <div className="card text-center">
                                    <img className="card-img-top" src={m02} alt="m02"/>
                                    <div className="card-body" id="card">
                                        <h5 className="card-title">Title</h5>
                                    </div>
                                </div>
                            </Link>
                            <Link to="hkvideo">
                                <div className="card text-center">
                                    <img className="card-img-top" src={m02} alt="m02"/>
                                    <div className="card-body" id="card">
                                        <h5 className="card-title">Title</h5>
                                    </div>
                                </div>
                            </Link>
                            <Link to="hkvideo">
                                <div className="card text-center">
                                    <img className="card-img-top" src={m02} alt="m02"/>
                                    <div className="card-body" id="card">
                                        <h5 className="card-title">Title</h5>
                                    </div>
                                </div>
                            </Link>
                            <Link to="hkvideo">
                                <div className="card text-center">
                                    <img className="card-img-top" src={m02} alt="m02"/>
                                    <div className="card-body" id="card">
                                        <h5 className="card-title">Title</h5>
                                    </div>
                                </div>
                            </Link>
                            <Link to="hkvideo">
                                <div className="card text-center">
                                    <img className="card-img-top" src={m02} alt="m02"/>
                                    <div className="card-body" id="card">
                                        <h5 className="card-title">Title</h5>
                                    </div>
                                </div>
                            </Link>
                            <Link to="hkvideo">
                                <div className="card text-center">
                                    <img className="card-img-top" src={m02} alt="m02"/>
                                    <div className="card-body" id="card">
                                        <h5 className="card-title">Title</h5>
                                    </div>
                                </div>
                            </Link>
                            <Link to="hkvideo">
                                <div className="card text-center">
                                    <img className="card-img-top" src={m02} alt="m02"/>
                                    <div className="card-body" id="card">
                                        <h5 className="card-title">Title</h5>
                                    </div>
                                </div>
                            </Link>
                            <Link to="hkvideo">
                                <div className="card text-center">
                                    <img className="card-img-top" src={m02} alt="m02"/>
                                    <div className="card-body" id="card">
                                        <h5 className="card-title">Title</h5>
                                    </div>
                                </div>
                            </Link>
                            <Link to="hkvideo">
                                <div className="card text-center">
                                    <img className="card-img-top" src={m02} alt="m02"/>
                                    <div className="card-body" id="card">
                                        <h5 className="card-title">Title</h5>
                                    </div>
                                </div>
                            </Link>
                            <Link to="hkvideo">
                                <div className="card text-center">
                                    <img className="card-img-top" src={m02} alt="m02"/>
                                    <div className="card-body" id="card">
                                        <h5 className="card-title">Title</h5>
                                    </div>
                                </div>
                            </Link>
                            <div className="clearfix"></div>
                        </div>


                    </main>

                </article>
                <Menu/>
            </section>


        );
    }
}

export default HK;
