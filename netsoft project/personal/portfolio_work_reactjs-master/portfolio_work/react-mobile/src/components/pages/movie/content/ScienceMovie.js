import React, {Component} from 'react';
import {Link} from 'react-router';
import s01 from '../../../../images/r03.jpg';
import s02 from '../../../../images/r02.jpg';
import s03 from '../../../../images/r04.jpg';
import s04 from '../../../../images/s02.jpg';

class ScienceMovie extends Component {
    render() {
        return (
            <div className="ScienceMovie">
                <div className="top_01">
                    <header>
                        <h4>Science Fiction Movie</h4>
                        <Link to="science">
                            <span className="span_home">view more<i className="fal fa-angle-double-right"></i></span>
                        </Link>
                        <div className="clearfix"></div>
                    </header>

                    <Link to="sciencevideo">
                        <div className="card text-center">
                            <img className="card-img-top" src={s01} alt="s01"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="sciencevideo">
                        <div className="card text-center">
                            <img className="card-img-top" src={s02} alt="s02"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="sciencevideo">
                        <div className="card text-center">
                            <img className="card-img-top" src={s03} alt="s03"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="sciencevideo">
                        <div className="card text-center">
                            <img className="card-img-top" src={s04} alt="s04"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <div className="clearfix"></div>
                </div>
            </div>
        );
    }
}

export default ScienceMovie;
