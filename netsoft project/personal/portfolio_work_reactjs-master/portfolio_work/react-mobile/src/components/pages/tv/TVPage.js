import React, {Component} from 'react';
import HKTV from './content/HKTV'
import ChinaTV from './content/ChinaTV'
import KoreanTV from './content/KoreanTV'
import USTV from './content/USTV'

class TVPage extends Component {
    render() {
        return (
            <article id="TVPage">
                <HKTV/>
                <ChinaTV/>
                <KoreanTV/>
                <USTV/>
            </article>
        );
    }
}

export default TVPage;
