import React, {Component} from 'react'
import {Badge} from 'antd-mobile';
import {Link} from 'react-router';
import re01 from '../../../../images/re01.jpg';
import re02 from '../../../../images/re02.jpg';
import re03 from '../../../../images/re03.jpg';
import re04 from '../../../../images/re04.jpg';


class Reality extends Component {
    render() {
        return (
            <div className="Reality">
                <div className="top_01">
                    <header>
                        <h4>Reality Show</h4>
                        <Link to="realityview">
                            <span className="span_home">view more<i className="fal fa-angle-double-right"></i></span>
                        </Link>
                        <div className="clearfix"></div>
                    </header>

                    <Link to="realityvideo">
                        <div className="card text-center">
                            <Badge text="EP 177"
                                   style={{
                                       marginLeft: 12,
                                       padding: '0 3px',
                                       backgroundColor: '#e91e63',
                                       borderRadius: 2
                                   }}/>
                            <img className="card-img-top" src={re01} alt="re01"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="realityvideo">
                        <div className="card text-center">
                            <Badge text="EP 54"
                                   style={{
                                       marginLeft: 12,
                                       padding: '0 3px',
                                       backgroundColor: '#e91e63',
                                       borderRadius: 2
                                   }}/>
                            <img className="card-img-top" src={re02} alt="re02"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="realityvideo">
                        <div className="card text-center">
                            <Badge text="EP 56"
                                   style={{
                                       marginLeft: 12,
                                       padding: '0 3px',
                                       backgroundColor: '#e91e63',
                                       borderRadius: 2
                                   }}/>
                            <img className="card-img-top" src={re03} alt="re03"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <Link to="realityvideo">
                        <div className="card text-center">
                            <Badge text="EP 122"
                                   style={{
                                       marginLeft: 12,
                                       padding: '0 3px',
                                       backgroundColor: '#e91e63',
                                       borderRadius: 2
                                   }}/>
                            <img className="card-img-top" src={re04} alt="re04"/>
                            <div className="card-body" id="card">
                                <h5 className="card-title">Title</h5>
                            </div>
                        </div>
                    </Link>
                    <div className="clearfix"></div>
                </div>
            </div>
        );
    }
}

export default Reality;
