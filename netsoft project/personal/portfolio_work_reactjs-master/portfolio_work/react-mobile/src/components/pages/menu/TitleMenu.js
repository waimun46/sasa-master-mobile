import React, {Component} from 'react';
//import PropTypes from 'prop-types';
import {Link} from 'react-router';




class TitleMenu extends Component {
    render() {
        const {title} = this.props;
        return (
            <div id="TitleMenu">
                <header>
                    <Link to="home">
                        <div className="arrow_back">
                            <i className="far fa-long-arrow-left"></i>
                        </div>
                    </Link>
                    <h5>{title}</h5>
                </header>
            </div>
        );
    }
}
/*
 TitleMenu.contextTypes = {
 router: PropTypes.object.isRequired

 //onClick={this.context.router.goBack}
 }

 */
export default TitleMenu;
