import React, {Component} from 'react';
import {Link} from 'react-router';
import v01 from '../../../images/img04.jpg';

class VideoRecom01 extends Component {
    render() {
        return (
            <div className="VideoRecom">
                <div className="title_recom">
                    <p>Recommend related</p>
                </div>
                <div className="recom_sty">
                    <Link to="video">
                        <figure>
                            <img src={v01} alt="03"/>
                            <figcaption><h5>Title</h5></figcaption>
                            <div className="clearfix"></div>
                        </figure>
                    </Link>
                    <Link to="video">
                        <figure>
                            <img src={v01} alt="03"/>
                            <figcaption><h5>Title</h5></figcaption>
                            <div className="clearfix"></div>
                        </figure>
                    </Link>
                    <Link to="video">
                        <figure>
                            <img src={v01} alt="03"/>
                            <figcaption><h5>Title</h5></figcaption>
                            <div className="clearfix"></div>
                        </figure>
                    </Link>
                    <Link to="video">
                        <figure>
                            <img src={v01} alt="03"/>
                            <figcaption><h5>Title</h5></figcaption>
                            <div className="clearfix"></div>
                        </figure>
                    </Link>
                    <Link to="video">
                        <figure>
                            <img src={v01} alt="03"/>
                            <figcaption><h5>Title</h5></figcaption>
                            <div className="clearfix"></div>
                        </figure>
                    </Link>
                </div>
            </div>
        );
    }
}


export default VideoRecom01;
