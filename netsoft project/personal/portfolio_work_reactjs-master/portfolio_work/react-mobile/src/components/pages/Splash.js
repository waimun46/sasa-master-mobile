import React, {Component} from 'react';
import logo from '../../images/logo.png'

class Splash extends Component {
    render() {
        return (

            <div id="Splash">
                <div className="spl_logo">
                    <img className="logo" src={logo} alt="logo" draggable="false"/>
                </div>
                <p>Powerd by movie cinema.</p>
                <div className="loading_icon">
                    <div className="lds-spinner">
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
            </div>

        );
    }
}

export default Splash;


