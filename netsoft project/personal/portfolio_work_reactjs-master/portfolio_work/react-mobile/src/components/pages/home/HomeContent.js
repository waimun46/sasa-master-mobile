import React, {Component} from 'react';
import HomeNew from './content/HomeNew';
import HomeMovie from './content/HomeMovie';
import HomeTV from './content/HomeTV';
import HomeShow from './content/HomeShow';
import TopRate from './content/TopRate';
import Background01 from '../../../images/04.jpg';
import Background02 from '../../../images/01.jpg';
import Background03 from '../../../images/03.jpg';
import Background04 from '../../../images/02.jpg';
import Background05 from '../../../images/05.jpg';



class HomeContent extends Component {

    componentDidMount(){

    }

    render() {
        return (
            <div id="HomeContent">

                <div id="myCarousel" className="carousel slide" data-ride="carousel" >
                    <div className="carousel-inner">
                        <div className="item active">
                            <img src={Background01}  alt="01"/>
                        </div>

                        <div className="item">
                            <img  src={Background02} alt="04"/>
                        </div>

                        <div className="item">
                            <img  src={Background03}  alt="03"/>
                        </div>
                        <div className="item">
                            <img  src={Background04} alt="02" />
                        </div>
                        <div className="item">
                            <img  src={Background05} alt="05" />
                        </div>
                    </div>
                    <ol className="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" className="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="3"></li>
                        <li data-target="#myCarousel" data-slide-to="4"></li>
                    </ol>
                    <a className="left carousel-control" href="#myCarousel" data-slide="prev">
                        <span className="glyphicon glyphicon-chevron-left"></span>
                        <span className="sr-only">Previous</span>
                    </a>
                    <a className="right carousel-control " href="#myCarousel" data-slide="next">
                        <span className="glyphicon glyphicon-chevron-right"></span>
                        <span className="sr-only">Next</span>
                    </a>
                </div>
                <HomeNew/>
                <HomeMovie/>
                <HomeTV/>
                <HomeShow/>
                <TopRate/>
            </div>
        );
    }
}

export default HomeContent;
