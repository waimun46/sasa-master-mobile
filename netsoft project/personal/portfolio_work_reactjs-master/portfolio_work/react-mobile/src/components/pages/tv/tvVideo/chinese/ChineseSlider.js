import React, {Component} from 'react';
import {Link} from 'react-router';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';

import img04 from '../../../../../images/tvv01.jpg';

class ChineseSlider extends Component {

    render() {
        return (
            <div className="VideoSlider">
                <div className="title_slider">
                    <p>Episodes</p>
                    <span>6 Episodes > </span>
                    <div className="clearfix"></div>
                </div>
                <OwlCarousel
                    className="owl-theme"
                    loop={false}
                    margin={7}
                    nav={false}
                    dots={false}
                >
                    <Link to="chinesevideo">
                        <div className="item">
                            <img src={img04} alt="img04"/>
                            <h4>Title EP01</h4>
                        </div>
                    </Link>
                    <Link to="chinesevideo">
                        <div className="item">
                            <img src={img04} alt="img04"/>
                            <h4>Title EP02</h4>
                        </div>
                    </Link>
                    <Link to="chinesevideo">
                        <div className="item">
                            <img src={img04} alt="img04"/>
                            <h4>Title EP03</h4>
                        </div>
                    </Link>
                    <Link to="chinesevideo">
                        <div className="item">
                            <img src={img04} alt="img04"/>
                            <h4>Title EP04</h4>
                        </div>
                    </Link>
                    <Link to="chinesevideo">
                        <div className="item">
                            <img src={img04} alt="img04"/>
                            <h4>Title EP05</h4>
                        </div>
                    </Link>
                    <Link to="chinesevideo">
                        <div className="item">
                            <img src={img04} alt="img04"/>
                            <h4>Title EP06</h4>
                        </div>
                    </Link>

                </OwlCarousel>
            </div>
        );
    }
}

export default ChineseSlider;

