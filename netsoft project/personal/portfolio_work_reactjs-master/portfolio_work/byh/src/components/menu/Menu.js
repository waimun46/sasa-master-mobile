import React, {Component} from 'react';
import $ from 'jquery';
import Logo from '../../images/logo01.png';
import Login from './content/Login';
import Register from './content/Register';



class Menu extends Component {

    componentDidMount() {
        $(document).ready(function () {
            $('.popup-btn').click(function (e) {
                $('.popup-wrap').fadeIn(500);
                $('.popup-box').removeClass('transform-out').addClass('transform-in');

                e.preventDefault();
            });

            $('.popup-close').click(function (e) {
                $('.popup-wrap').fadeOut(500);
                $('.popup-box').removeClass('transform-in').addClass('transform-out');

                e.preventDefault();
            });


            $('.popup-btn2').click(function (e) {
                $('.popup-wrap2').fadeIn(500);
                $('.popup-box2').removeClass('transform-out').addClass('transform-in');

                e.preventDefault();
            });
            $('.popup-close').click(function (e) {
                $('.popup-wrap2').fadeOut(500);
                $('.popup-box2').removeClass('transform-in').addClass('transform-out');

                e.preventDefault();
            });

        });

    }

    render() {
        return (
            <section id="Menu">
                <header id="header-main">
                    <div className="container">
                        <h1 className="logo">
                            <img src={Logo} alt="logo"/>
                        </h1>

                        <div id="menu-icon">
                            <span className="icon-menu-hamburguer"></span>
                        </div>

                        <nav id="menu-main">
                            <ul>
                                <div className="center_li">
                                    <li>
                                        <div className="a_style"><a>首页</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="a_style"><a>优惠</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="a_style"><a>充值提款</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="a_style"><a>在线客服</a>
                                        </div>
                                    </li>
                                </div>
                                <div className="righr_li">
                                    <div className="righr_float">
                                        <li>
                                            <a href="#" className="btn popup-btn">
                                                登录
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" className="btn popup-btn2">
                                                注册
                                            </a>
                                        </li>
                                        <div style={{clear: 'both'}}></div>
                                    </div>
                                </div>
                                <div style={{clear: 'both'}}></div>
                            </ul>
                        </nav>
                        <Login/>
                        <Register/>
                    </div>
                </header>
            </section>
        );
    }
}

export default Menu;
