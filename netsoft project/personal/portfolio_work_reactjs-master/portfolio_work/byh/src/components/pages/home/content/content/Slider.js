import React, {Component} from 'react';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';

import img01 from '../../../../../images/s1.png';
import img02 from '../../../../../images/s2.png';
import img03 from '../../../../../images/s3.png';

class Slider extends Component {
    render() {
        return (
            <section id="Slider">
                <OwlCarousel
                    className="owl-carousel owl-theme"
                    loop={true}
                    autoplay={true}
                    nav={false}
                    dots={false}
                    items={1}
                >
                    <div className="item">
                        <img src={img01} alt="img01"/>
                    </div>
                    <div className="item">
                        <img src={img02} alt="img02"/>
                    </div>
                    <div className="item">
                        <img src={img03} alt="img03"/>
                    </div>
                </OwlCarousel>
            </section>
        );
    }
}

export default Slider;
