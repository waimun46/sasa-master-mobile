import React, {Component} from 'react';
import Slider from './content/Slider.js';
import Marquee from './content/Marquee.js';
import Banner from './content/Banner.js';

class RightContent extends Component {

    render() {
        return (
            <article id="RightContent">
                <Slider/>
                <Banner/>
            </article>
        );
    }
}

export default RightContent;
