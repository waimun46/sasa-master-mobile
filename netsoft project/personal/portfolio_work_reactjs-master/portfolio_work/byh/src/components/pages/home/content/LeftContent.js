import React, {Component} from 'react';
import LeftBarcode from './content/LeftBarcode.js';

class LeftContent extends Component {
    render() {
        return (
            <article id="LeftContent">
                <LeftBarcode/>
            </article>
        );
    }
}

export default LeftContent;
