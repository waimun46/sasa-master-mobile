import React, {Component} from 'react';
import LeftContent from './content/LeftContent';
import RightContent from './content/RightContent';
import Marquee from './content/content/Marquee.js';

class Home extends Component {
    render() {
        return (
            <section className="Home">
                <LeftContent/>
                <RightContent/>
                <Marquee/>
                <div style={{clear: 'both'}}></div>
            </section>

        );
    }
}

export default Home;
