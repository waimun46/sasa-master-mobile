import React, {Component} from 'react';
import logo from '../../../../../images/logo_b.png';
import barcode from '../../../../../images/barcode.jpg'
import Register from '../../../../menu/content/Register';
import $ from "jquery";

class LeftBarcode extends Component {

    componentDidMount() {
        $(document).ready(function () {
            $('.popup-btn2').click(function (e) {
                $('.popup-wrap2').fadeIn(500);
                $('.popup-box2').removeClass('transform-out').addClass('transform-in');

                e.preventDefault();
            });
            $('.popup-close').click(function (e) {
                $('.popup-wrap2').fadeOut(500);
                $('.popup-box2').removeClass('transform-in').addClass('transform-out');

                e.preventDefault();
            });

        });

    }


    render() {
        return (
            <section id="LeftBarcode">
                <div className="logo_left">
                    <img src={logo} alt="logo"/>
                </div>
                <div className="botton_left">
                    <div className="inner_code">
                        <div className="title_code">
                            <h3>游戏下载</h3>
                        </div>
                        <div className="bar_img_sty">
                            <img src={barcode} alt="barcode"/>
                        </div>
                        <div className="game_btn">
                            <div className="box popup-btn2">立即注册</div>
                        </div>
                        <div style={{clear: 'both'}}></div>
                    </div>
                    <Register/>
                </div>
            </section>
        );
    }
}

export default LeftBarcode;
