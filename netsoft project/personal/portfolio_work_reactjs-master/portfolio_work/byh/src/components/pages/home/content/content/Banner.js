import React, {Component} from 'react';


class Banner extends Component {
    render() {
        return (
            <section id="Banner">
                <div className="top_banner">
                    <div className="banner01">
                        <div className="left_side">
                            <div className="img01"></div>

                        </div>
                        <div className="right_side">
                            <div className="top01">
                                <div className="top_img01"></div>
                                <p className="title" style={{color: '#FFEB3B'}}>真钱游戏</p>
                                <div className="overlay"></div>
                                <div className="button">
                                    <a href="#"> 进入 </a>
                                </div>
                            </div>
                            <div className="top01">
                                <div className="top_img02"></div>
                                <p className="title" style={{color: '#00BCD4'}}>免费试玩</p>
                                <div className="overlay"></div>
                                <div className="button">
                                    <a href="#"> 进入 </a>
                                </div>
                            </div>
                            <div style={{clear: 'both'}}></div>
                        </div>
                    </div>
                </div>
                <div className="bottom_banner">
                    <div className="banner02">
                        <div className="right_side">
                            <div className="top01">
                                <div className="top_img01"></div>
                                <p className="title" style={{color: '#FFEB3B'}}>真钱游戏</p>
                                <div className="overlay"></div>
                                <div className="button">
                                    <a href="#"> 进入 </a>
                                </div>
                            </div>
                            <div className="top01">
                                <div className="top_img02"></div>
                                <p className="title" style={{color: '#00BCD4'}}>免费试玩</p>
                                <div className="overlay"></div>
                                <div className="button">
                                    <a href="#"> 进入 </a>
                                </div>
                            </div>
                            <div style={{clear: 'both'}}></div>
                        </div>
                        <div className="left_side">
                            <div className="img01"></div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default Banner;
