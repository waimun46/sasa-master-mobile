import React, {Component} from 'react';
import MenuPage from '../../menu/MenuPage';
import CheckoutForm from './CheckoutForm';


class CheckoutPage extends Component {
    render() {
        return (
            <section id="CheckoutPage">
                <MenuPage title="Checkout" />
                <main>
                    <article>

                        <CheckoutForm/>
                    </article>
                </main>

            </section>
        );
    }
}

export default CheckoutPage;
