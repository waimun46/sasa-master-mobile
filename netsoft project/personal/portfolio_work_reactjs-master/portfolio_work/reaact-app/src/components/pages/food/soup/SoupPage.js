import React, {Component} from 'react';
import MenuPage from '../../../menu/MenuPage';
import SoupForm from './SoupForm';
import BottomMenu from '../../../menu/BottomMenu';
import Cart from '../../cart/Cart'

class SoupPage extends Component {
    render() {
        return (
            <section id="SoupPage">
                <MenuPage title="Soup"/>
                <main>
                    <SoupForm/>
                    <Cart/>
                </main>
                <BottomMenu/>
            </section>
        );
    }
}

export default SoupPage;
