import React, {Component} from 'react';
import MenuPage from '../../menu/MenuPage';
import Slider from './Slider';
import Content from './Content';
import BottomMenu from '../../menu/BottomMenu';
import Cart from '../cart/Cart'




class HomePage extends Component {


    render() {

        return (
            <section id="HomePage">
                <MenuPage title="Home"/>
                <main>
                    <Slider/>
                    <Content/>
                    <Cart/>
                </main>
                <BottomMenu/>
            </section>
        );
    }
}

export default HomePage;
