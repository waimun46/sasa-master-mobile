import React, {Component} from 'react';
import MenuPage from '../../menu/MenuPage';
import BottomMenu from '../../menu/BottomMenu';
import BeverageForm from './BeverageForm';
import Cart from '../cart/Cart'


class BeveragePage extends Component {
    render() {
        return (
            <section id="BeveragePage">
                <MenuPage title="Beverage"/>
                <main>
                    <BeverageForm/>
                    <Cart/>
                </main>
                <BottomMenu/>
            </section>
        );
    }
}

export default BeveragePage;
