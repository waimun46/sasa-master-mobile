import React, {Component} from 'react';
import * as jquery from 'jquery';
import { browserHistory} from 'react-router';
import $ from "jquery";
import {DatePicker, Modal, Button} from 'antd';
import moment from 'moment';

const dateFormat = 'YYYY-MM-DD';


function success() {
    Modal.success({
        title: 'Successful',
        content: 'Your payment is Complete.',
        onOk() {
            return browserHistory.push('home')
        },
    });
}

function onChange(value, dateString) {
    console.log('Selected Time: ', value);
    console.log('Formatted Selected Time: ', dateString);
}

function onOk(value) {
    console.log('onOk: ', value);
}


class CheckoutForm extends Component {
    state = {
        loading: false,
        visible: false,
    }
    showModal = () => {
        this.setState({
            visible: true,
        });
    }

    handleOk = () => {
        this.setState({loading: true});
        setTimeout(() => {
            this.setState({loading: false, visible: false, });
        }, 3000);
    }
    handleCancel = () => {
        this.setState({visible: false});
    }



    componentDidMount() {

        function scroll_to_class(element_class, removed_height) {
            let scroll_to = $(element_class).offset().top - removed_height;
            if ($(window).scrollTop() != scroll_to) {
                $('html, body').stop().animate({scrollTop: scroll_to}, 0);
            }
        }

        function bar_progress(progress_line_object, direction) {
            let number_of_steps = progress_line_object.data('number-of-steps');
            let now_value = progress_line_object.data('now-value');
            let new_value = 0;
            if (direction == 'right') {
                new_value = now_value + ( 100 / number_of_steps );
            }
            else if (direction == 'left') {
                new_value = now_value - ( 100 / number_of_steps );
            }
            progress_line_object.attr('style', 'width: ' + new_value + '%;').data('now-value', new_value);
        }

        $('.f1 fieldset:first').fadeIn('slow');

        $('.f1 input[type="text"], .f1 input[type="password"], .f1 textarea').on('focus', function () {
            $(this).removeClass('input-error');
        });


        $(document).ready(function () {
            /*Form*/
            $('.f1 fieldset:first').fadeIn('slow');

            $('.f1 input[type="text"], .f1 input[type="password"], .f1 textarea').on('focus', function () {
                $(this).removeClass('input-error');
            });

            // next step
            $('.f1 .btn-next').on('click', function () {
                let parent_fieldset = $(this).parents('fieldset');
                let next_step = true;
                // navigation steps / progress steps
                let current_active_step = $(this).parents('.f1').find('.f1-step.active');
                let progress_line = $(this).parents('.f1').find('.f1-progress-line');

                // fields validation
                parent_fieldset.find('input[type="text"], input[type="password"], input[type="number"], textarea').each(function () {
                    if ($(this).val() == "") {
                        $(this).addClass('input-error');
                        next_step = false;
                    }
                    else {
                        $(this).removeClass('input-error');
                    }
                });


                // fields validation

                if (next_step) {
                    parent_fieldset.fadeOut(400, function () {
                        // change icons
                        current_active_step.removeClass('active').addClass('activated').next().addClass('active');
                        // progress bar
                        bar_progress(progress_line, 'right');
                        // show next step
                        $(this).next().fadeIn();
                        // scroll window to beginning of the form
                        scroll_to_class($('.f1'), 20);
                    });
                }

            });

            // previous step
            $('.f1 .btn-previous').on('click', function () {
                // navigation steps / progress steps
                let current_active_step = $(this).parents('.f1').find('.f1-step.active');
                let progress_line = $(this).parents('.f1').find('.f1-progress-line');

                $(this).parents('fieldset').fadeOut(400, function () {
                    // change icons
                    current_active_step.removeClass('active').prev().removeClass('activated').addClass('active');
                    // progress bar
                    bar_progress(progress_line, 'left');
                    // show previous step
                    $(this).prev().fadeIn();
                    // scroll window to beginning of the form
                    scroll_to_class($('.f1'), 20);
                });
            });

            // submit
            $('.f1').on('submit', function (e) {
                // fields validation
                $(this).find('input[type="text"], input[type="password"], input[type="number"], textarea').each(function () {
                    if ($(this).val() == "") {
                        e.preventDefault();
                        $(this).addClass('input-error');
                    }
                    else {
                        $(this).removeClass('input-error');
                    }
                });
                // fields validation
            });
        });

    }


    render() {
        const {visible, loading} = this.state;
        return (
            <div className="CheckoutForm">
                <div className=" form-box">
                    <form role="form" className="f1">

                        <div className="f1-steps">
                            <div className="f1-progress">
                                <div className="f1-progress-line" data-now-value="16.66" data-number-of-steps="3"></div>
                            </div>
                            <div className="f1-step active">
                                <div className="f1-step-icon"><i className="fas fa-file-edit"></i></div>
                                <p>Shipping</p>
                            </div>
                            <div className="f1-step">
                                <div className="f1-step-icon"><i className="fas fa-credit-card"></i></div>
                                <p>Payment</p>
                            </div>
                            <div className="f1-step">
                                <div className="f1-step-icon"><i className="fas fa-check"></i></div>
                                <p>Confirm</p>
                            </div>
                        </div>

                        {/*  Shipping */}
                        <fieldset>
                            <div className="ship_style">
                                <h4>Customer information</h4>
                                <div className="user">
                                    <div className="user2">
                                        <ul>
                                            <li>
                                                <span className="fas fa-user" style={{backgroundColor: '#333'}}></span>
                                                <span className="user2_span">Mary</span>
                                            </li>
                                            <li>
                                                <span className="fas fa-phone" style={{backgroundColor: '#333'}}></span>
                                                <span className="user2_span">0123456789</span>
                                            </li>
                                            <li>
                                                <span className="fas fa-envelope"
                                                      style={{backgroundColor: '#333'}}></span>
                                                <span className="user2_span">mary@gmail.com</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="clearfix"></div>
                                </div>
                            </div>

                            <div className="ship_style" style={{marginTop: '10px'}}>
                                <h4>Shipping address</h4>
                                <div className="user">
                                    <div className="user2">
                                        <ul>
                                            <li>
                                                <span className="fas fa-map-marker-alt"
                                                      style={{backgroundColor: '#333'}}></span>
                                                <span className="user2_span">Petaling Jaya, Selangor</span>
                                                <span className="user2_span edit_sty"
                                                      onClick={this.showModal}>Edit</span>
                                                <Modal
                                                    visible={visible}
                                                    title="Edit address"
                                                    onOk={this.handleOk}
                                                    onCancel={this.handleCancel}
                                                    footer={[
                                                        <Button key="submit" type="primary" loading={loading}
                                                                onClick={this.handleOk}>
                                                            Submit
                                                        </Button>,
                                                    ]}
                                                >

                                                    <form id="contact" name="contact" method="post">
                                                        <div className="form-group col-md-6">
                                                            <input type="text" className="form-control" name="InputName"
                                                                   id="InputName"
                                                                   placeholder="Address" required/>
                                                        </div>
                                                        <div className="form-group col-md-6">
                                                            <select id="input_country"
                                                                    className="f1-last-name form-control"
                                                                    title="Country">
                                                                <option>Country</option>
                                                                <option>Malaysia</option>
                                                            </select>
                                                        </div>

                                                        <div className="form-group col-md-6">
                                                            <select id="input_state" className=" form-control">
                                                                <option>City</option>
                                                                <option>Kuala Lumpur</option>
                                                                <option selected="">Selangor</option>
                                                                <option>Seremban</option>
                                                                <option>Perak</option>
                                                                <option>Penang</option>
                                                                <option>Shah Alam</option>
                                                                <option>Ipoh</option>
                                                                <option>Malacca</option>
                                                            </select>
                                                        </div>
                                                        <div className="form-group col-md-12">
                                                            <input type="text" className="form-control" name="InputWeb"
                                                                   id="InputWeb"
                                                                   placeholder="Zip Code"/>
                                                        </div>
                                                    </form>
                                                </Modal>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div className="ship_style" style={{marginTop: '10px', marginBottom: '10px'}}>
                                <h4>Item</h4>
                                <div className="user">
                                    <div className="user2">
                                        <ul style={{paddingBottom: '30px'}}>
                                            <li>
                                                <span className="fas fa-star"
                                                      style={{color: '#e76b63'}}></span>
                                                <span className="user2_span">Product01</span>
                                                <span className="user2_span pr_sty">RM 29.90</span>
                                            </li>
                                            <li>
                                                <span className="fas fa-star"
                                                      style={{color: '#e76b63'}}></span>
                                                <span className="user2_span">Product02</span>
                                                <span className="user2_span pr_sty">RM 9.90</span>
                                            </li>
                                            <li>
                                                <span className="user2_span total_sty"><small>Total:</small><big> RM 39.80</big></span>
                                            </li>
                                        </ul>
                                    </div>

                                </div>
                            </div>

                            <div className="f1-buttons ship_btn">
                                <button type="button" className="btn btn-next">Next
                                    <i className="faa-passing animated far fa-angle-double-right"
                                       style={{fontSize: 18, color: '#fff', float: 'right'}}></i></button>
                            </div>
                        </fieldset>
                        {/*  End Shipping */}

                        {/*  Payment */}
                        <fieldset>
                            <div className="pay_style">
                                <div className="pay_padding">
                                    <h4>Payment method</h4>
                                    <div className="paymentCont">
                                        <div className="paymentWrap">
                                            <div className="btn-group paymentBtnGroup btn-group-justified"
                                                 data-toggle="buttons">
                                                <label className="btn paymentMethod">
                                                    <div className="method visa"></div>
                                                    <input type="radio" name="options" checked/>
                                                </label>
                                                <label className="btn paymentMethod">
                                                    <div className="method master-card"></div>
                                                    <input type="radio" name="options"/>
                                                </label>
                                                <label className="btn paymentMethod">
                                                    <div className="method vishwa"></div>
                                                    <input type="radio" name="options"/>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="pay_padding" style={{marginTop: '10px', marginBottom: '10px'}}>
                                    <h4>Details</h4>

                                    <div className="form-group">
                                        <select id="input_state" className=" form-control">
                                            <option>Card Type</option>
                                            <option>Russia</option>
                                            <option selected="">United States</option>
                                            <option>India</option>
                                            <option>Afganistan</option>
                                        </select>
                                    </div>
                                    <div className="form-group">
                                        <input type="text" placeholder="Card Holder's Name"
                                               className="form-control"/>
                                    </div>

                                    <div className="form-group">
                                        <input type="number" placeholder="Card Number"
                                               className="form-control"/>
                                    </div>

                                    <div className="col_style form-group">
                                        <DatePicker
                                            showTime
                                            format="YYYY-MM-DD"
                                            placeholder="Expery Date"
                                            onChange={onChange}
                                            onOk={onOk}
                                        />
                                    </div>
                                    <div className="col_style form-group">
                                        <input type="number" placeholder="CVV"
                                               className="form-control"/>
                                    </div>
                                    <div className="clearfix"></div>

                                    <div className="previous_btn">
                                        <i className="faa-passing-reverse animated fal fa-angle-left"></i>
                                        <span className="btn-previous">Previous</span>
                                    </div>
                                </div>

                                <div className="f1-buttons pay_btn">
                                    <button type="button" className="btn btn-next">Next
                                        <i className="faa-passing animated far fa-angle-double-right"
                                           style={{fontSize: 18, color: '#fff', float: 'right'}}></i></button>
                                </div>

                            </div>
                        </fieldset>
                        {/*  End Payment */}

                        {/* Confirm */}
                        <fieldset>
                            <div className="con_sty">
                                <div className="con_pay">
                                    <h4>Payment method</h4>
                                    <label className="label_sty">Visa</label>
                                    <div className="img_sty">
                                        <img
                                            src="http://www.paymentscardsandmobile.com/wp-content/uploads/2015/08/Amex-icon.jpg"/>
                                    </div>
                                    <div className="clearfix"></div>
                                </div>

                                <div className="con_pay02">
                                    <h4>Payment details</h4>

                                    <div className="det_sty">
                                        <label>Name : </label>
                                        <div className="spn_sty">
                                            <span>Mary</span>
                                        </div>
                                        <div className="clearfix"></div>
                                    </div>
                                    <div className="det_sty">
                                        <label>Email : </label>
                                        <div className="spn_sty">
                                            <span>mary@hotmail.com</span>
                                        </div>
                                        <div className="clearfix"></div>
                                    </div>
                                    <div className="det_sty">
                                        <label>Phone : </label>
                                        <div className="spn_sty">
                                            <span>0123456789</span>
                                        </div>
                                        <div className="clearfix"></div>
                                    </div>
                                    <div className="det_sty" style={{paddingBottom: '10px'}}>
                                        <label>Shipping address : </label>
                                        <div className="spn_sty">
                                            <span>Petaling Jaya, Selangor</span>
                                        </div>
                                        <div className="clearfix"></div>
                                    </div>
                                </div>

                                <div className="con_pay amount">
                                    <h4>Amount</h4>
                                    <label className="label_sty">Total Amount:</label>
                                    <div className="amt_sty" style={{color: '#f69791'}}>
                                        <span><small>RM</small>100.00</span>
                                    </div>
                                    <div className="clearfix"></div>
                                </div>

                                <div className="con_pd_btn">
                                    <div className="previous_btn">
                                        <i className="faa-passing-reverse animated fal fa-angle-left"></i>
                                        <span className="btn-previous">Previous</span>
                                    </div>
                                </div>
                            </div>

                            <div className=" con_btn">
                                <input className=" btn_con" type="button"  value="Confrim Payment" onClick={success} />
                            </div>

                        </fieldset>
                        {/* End Confirm */}

                    </form>
                </div>
            </div>


        );
    }
}


export default CheckoutForm;
