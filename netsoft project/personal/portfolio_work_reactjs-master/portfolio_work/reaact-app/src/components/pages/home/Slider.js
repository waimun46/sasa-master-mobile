import React, {Component} from 'react';
import $ from "jquery";
import {Link} from 'react-router';
import {Icon} from 'antd';

import Background01 from '../../../images/b1.jpg';
import Background02 from '../../../images/b2.jpg';
import Background03 from '../../../images/b3.jpg';




class Slider extends Component {

    componentDidMount() {


    }


    render() {


        return (
            <div id="Slider">


                <div id="myCarousel" className="carousel slide" data-ride="carousel">

                    <ol className="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" className="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                    </ol>


                    <div className="carousel-inner">

                        <div className="item active">

                            <img src={Background01} />
                            <div className="carousel-caption">
                                <h3>Discover Delicious Food </h3>
                            </div>
                        </div>

                        <div className="item">
                            <img  src={Background03} />
                            <div className="carousel-caption">
                                <h3>Only The Best Beverages And Dessert</h3>
                            </div>
                        </div>

                        <div className="item">
                            <img  src={Background02}  />
                            <div className="carousel-caption">
                                <h3>Online Food Ordering <br/>ON THE GO</h3>
                            </div>
                        </div>

                    </div>

                    <a className="left carousel-control" href="#myCarousel" data-slide="prev">
                        <span className="glyphicon glyphicon-chevron-left"></span>
                        <span className="sr-only">Previous</span>
                    </a>
                    <a className="right carousel-control " href="#myCarousel" data-slide="next">
                        <span className="glyphicon glyphicon-chevron-right"></span>
                        <span className="sr-only">Next</span>
                    </a>
                </div>

            </div>
        );
    }
}

export default Slider;
