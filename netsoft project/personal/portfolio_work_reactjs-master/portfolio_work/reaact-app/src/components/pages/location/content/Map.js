import React, {Component} from 'react';
import {Avatar } from 'antd';

class Map extends Component {
    render() {
        return (
            <div className="MapForm">
                <div className="Map_top">
                    <iframe className="map_sty" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3984.084356675091!2d101.60470641475713!3d3.0721368977628267!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31cc4c8776586d9b%3A0x5e19a549d4f26f25!2sSunway+Pyramid!5e0!3m2!1sen!2smy!4v1530696469791" allowfullscreen></iframe>
                </div>

                <div className="map_list">

                    <div className="comments-list">
                        <div className="media">
                            <p className="pull-right"><small>5 days ago</small></p>
                            <a className="media-left" href="#">
                                <Avatar style={{ backgroundColor: '#87d068' }} icon="user" />
                            </a>
                            <div className="media-body">

                                <h4 className="media-heading user_name">Baltej Singh</h4>
                                Wow! this is really great.

                                <p><small><a href="">Like</a> - <a href="">Share</a></small></p>
                            </div>
                        </div>
                        <div className="media">
                            <p className="pull-right"><small>5 days ago</small></p>
                            <a className="media-left" href="#">
                                <Avatar style={{ backgroundColor: '#87d068' }} icon="user" />
                            </a>
                            <div className="media-body">

                                <h4 className="media-heading user_name">Baltej Singh</h4>
                                Wow! this is really great.

                                <p><small><a href="">Like</a> - <a href="">Share</a></small></p>
                            </div>
                        </div>
                        <div className="media">
                            <p className="pull-right"><small>5 days ago</small></p>
                            <a className="media-left" href="#">
                                <Avatar style={{ backgroundColor: '#87d068' }} icon="user" />
                            </a>
                            <div className="media-body">

                                <h4 className="media-heading user_name">Baltej Singh</h4>
                                Wow! this is really great.

                                <p><small><a href="">Like</a> - <a href="">Share</a></small></p>
                            </div>
                        </div>
                        <div className="media">
                            <p className="pull-right"><small>5 days ago</small></p>
                            <a className="media-left" href="#">
                                <Avatar style={{ backgroundColor: '#87d068' }} icon="user" />
                            </a>
                            <div className="media-body">

                                <h4 className="media-heading user_name">Baltej Singh</h4>
                                Wow! this is really great.

                                <p><small><a href="">Like</a> - <a href="">Share</a></small></p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default Map;
