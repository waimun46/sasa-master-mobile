import React, {Component} from 'react';
import MenuPage from '../../../menu/MenuPage';
import CakeForm from './CakeForm';
import BottomMenu from '../../../menu/BottomMenu';
import Cart from '../../cart/Cart'

class CakePage extends Component {
    render() {
        return (
            <section id="CakePage">
                <MenuPage title="Cake"/>
                <main>
                    <CakeForm/>
                    <Cart/>
                </main>
                <BottomMenu/>
            </section>
        );
    }
}

export default CakePage;
