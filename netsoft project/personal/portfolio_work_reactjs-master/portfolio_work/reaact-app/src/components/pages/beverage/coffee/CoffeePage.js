import React, {Component} from 'react';
import MenuPage from '../../../menu/MenuPage';
import CoffeeForm from './CoffeeForm';
import BottomMenu from '../../../menu/BottomMenu';
import Cart from '../../cart/Cart'

class CoffeePage extends Component {
    render() {
        return (
            <section id="CoffeePage">
                <MenuPage title="Coffee"/>
                <main>
                    <CoffeeForm/>
                    <Cart/>
                </main>
                <BottomMenu/>
            </section>
        );
    }
}

export default CoffeePage;
