import React, {Component} from 'react';
import {Link} from 'react-router';

import icon01 from '../../../images/i01.png';
import icon02 from '../../../images/i02.png';
import icon03 from '../../../images/i03.png';
import icon04 from '../../../images/i04.png';

class FoodForm extends Component {



    render() {
        return (
            <div className="FoodForm">
                <div className="food_banner">
                    <div className="banner">
                        <div className="banner_title">
                            <h2>Menu Category</h2>
                        </div>
                    </div>
                </div>
                <div className="food_menu">
                    <ul>
                        <li>
                            <Link to="noodle">
                                <div className="inner_li">
                                    <div className="img_sty">
                                        <img src={icon01}/>
                                    </div>
                                    <span>Noddles</span>
                                </div>
                            </Link>
                        </li>

                        <li>
                            <Link to="rice">
                                <div className="inner_li">
                                    <div className="img_sty">
                                        <img src={icon02}/>
                                    </div>
                                    <span>Rice</span>
                                </div>
                            </Link>
                        </li>
                        <li>
                            <Link to="soup">
                                <div className="inner_li">
                                    <div className="img_sty">
                                        <img src={icon03}/>
                                    </div>
                                    <span>Soup</span>
                                </div>
                            </Link>
                        </li>
                        <li>
                            <Link to="vege">
                                <div className="inner_li">
                                    <div className="img_sty">
                                        <img src={icon04}/>
                                    </div>
                                    <span>Vegetarian</span>
                                </div>
                            </Link>
                        </li>
                        <div className="clearfix"></div>
                    </ul>

                </div>

            </div>
        );
    }
}

export default FoodForm;
