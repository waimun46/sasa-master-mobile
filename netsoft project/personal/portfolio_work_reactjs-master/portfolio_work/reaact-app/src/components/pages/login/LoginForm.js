import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router';
import $ from "jquery";
import {browserHistory} from 'react-router';
import {Modal, Button} from 'antd';
import {connect} from 'react-redux';
import foodlogo from '../../../images/food.png';
import TextFieldGroup from '../../common/TextFieldGroup'


class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',

        };
        this.onChange = this.onChange.bind(this);

    }

    state = {
        loading: false,
        visible: false,
    }
    showModal = () => {
        this.setState({
            visible: true,
        });
    }
    handleOk = () => {
        this.setState({loading: true});
        setTimeout(() => {
            this.setState({loading: false, visible: false});
        }, 3000);
    }
    handleCancel = () => {
        this.setState({visible: false});
    }


    onChange(e) {
        this.setState({[e.target.name]: e.target.value});
    }


    login () {
        let username=$("#name").val();
        let password=$("#pass").val();

        if(username == 'food' && password == 123456){
           return browserHistory.push('home');
        }else if(username == '' || password == ''){
            $(".text-danger").html("Both Field Required.").show().fadeOut(8000);
        }else{
            $(".text-info").html("User Does Not Match.").show().fadeOut(8000);
        }

    }

    render() {
        const {password, username, errors} = this.state;
        const {visible, loading} = this.state;
        return (
            <div id="LoginForm">
                <div className="login_logo">
                    <img src={foodlogo} alt="foodlogo"/>
                </div>
                <form id="form"  action="" method="">
                        <span className="text-danger"></span>
                        <span className="text-info"></span>

                    <div className="form-group">
                        <label>User Name</label>
                        <input
                            id="name"
                            field="username"
                            onChange={this.onChange}
                            placeholder="Enter Username"
                            type="text"
                        />

                    </div>
                    <div className="form-group">
                        <label>Password</label>
                        <input
                            id="pass"
                            onChange={this.onChange}
                            field="pass"
                            placeholder="Enter Password"
                            type="password"
                        />

                    </div>

                    <div className="login_btn">
                        <input type="button" name="submit" value="Login" onClick={this.login}/>
                    </div>


                    <div className="forget_pass" onClick={this.showModal}>
                        <span>Forgot password ?</span>
                    </div>
                    <Modal
                        visible={visible}
                        title="Forget password"
                        onOk={this.handleOk}
                        onCancel={this.handleCancel}
                        footer={[
                            <Button key="submit" type="primary" onClick={this.handleOk}>
                                Submit
                            </Button>,
                        ]}
                    >
                        <p>Enter your email and will send you instruction on how to reset it.</p>
                        <div className="form-group">
                            <input type="email" className="form-control" name="InputEmail" id="InputEmail"
                                   placeholder="Your Email" required/>
                        </div>

                    </Modal>
                    <hr/>

                    <div className="register_btn">
                        <span>Don't have an Account ?
                            <Link to="signup">
                                <span className="register_span"> Register Now !</span>
                            </Link>
                        </span>
                    </div>
                </form>

            </div>
        );
    }
}


LoginForm.contextTypes = {
    router: PropTypes.object.isRequired
}

export default(LoginForm);