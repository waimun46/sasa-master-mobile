import React, {Component} from 'react';
import {Rate} from 'antd';
import $ from "jquery";
import item01 from '../../../../images/vege2.png';


class VegeForm extends Component {
    render() {
        return (
            <div className="VegeForm NoodleForm">
                <div className="item_list">
                    <div className="item_sty">
                        <span className="thumbnail">
                            <img src={item01}/>
                            <h4 >Product01</h4>
                            <div className="ratings">
                               <Rate disabled defaultValue={2}/>
                            </div>
                            <hr className="line"/>
                            <div className="row">
                                <div className="col-md-6 col-sm-6">
                                    <p className="price"><small>RM</small>29.90</p>
                                </div>
                                <div className="col-md-6 col-sm-6">
                                 <a href="#" className="add-to-cart" data-name="Product01" data-price="29.90">
                                     <button className="btn btn-info right"> Add to cart</button>
                                 </a>
                                </div>

                            </div>
                        </span>
                    </div>

                    <div className="item_sty">
                        <span className="thumbnail">
                            <img src={item01}/>
                            <h4>Product02</h4>
                            <div className="ratings">
                               <Rate disabled defaultValue={2}/>
                            </div>

                            <hr className="line"/>
                            <div className="row">
                                <div className="col-md-6 col-sm-6">
                                    <p className="price"><small>RM</small>18.90</p>
                                </div>
                                <div className="col-md-6 col-sm-6">
                                   <a href="#" className="add-to-cart" data-name="Product02" data-price="18.90">
                                     <button className="btn btn-info right"> Add to cart</button>
                                 </a>
                                </div>
                            </div>
                        </span>
                    </div>

                    <div className="item_sty">
                        <span className="thumbnail">
                            <img src={item01}/>
                            <h4>Product03</h4>
                            <div className="ratings">
                               <Rate disabled defaultValue={2}/>
                            </div>

                            <hr className="line"/>
                            <div className="row">
                                <div className="col-md-6 col-sm-6">
                                    <p className="price"><small>RM</small>20.00</p>
                                </div>
                                <div className="col-md-6 col-sm-6">
                                  <a href="#" className="add-to-cart" data-name="Product03" data-price="20.00">
                                     <button className="btn btn-info right"> Add to cart</button>
                                 </a>
                                </div>
                            </div>
                        </span>
                    </div>

                    <div className="item_sty">
                        <span className="thumbnail">
                            <img src={item01}/>
                            <h4>Product04</h4>
                            <div className="ratings">
                               <Rate disabled defaultValue={2}/>
                            </div>

                            <hr className="line"/>
                            <div className="row">
                                <div className="col-md-6 col-sm-6">
                                    <p className="price"><small>RM</small>9.90</p>
                                </div>
                                <div className="col-md-6 col-sm-6">
                                  <a href="#" className="add-to-cart" data-name="Product04" data-price="9.90">
                                     <button className="btn btn-info right"> Add to cart</button>
                                 </a>
                                </div>
                            </div>
                        </span>
                    </div>

                    <div className="item_sty">
                        <span className="thumbnail">
                            <img src={item01}/>
                            <h4>Product05</h4>
                            <div className="ratings">
                               <Rate disabled defaultValue={2}/>
                            </div>

                            <hr className="line"/>
                            <div className="row">
                                <div className="col-md-6 col-sm-6">
                                    <p className="price"><small>RM</small>29.90</p>
                                </div>
                                <div className="col-md-6 col-sm-6">
                                 <a href="#" className="add-to-cart" data-name="Product05" data-price="12.00">
                                     <button className="btn btn-info right"> Add to cart</button>
                                 </a>
                                </div>
                            </div>
                        </span>
                    </div>

                    <div className="item_sty margin_last">
                        <span className="thumbnail">
                            <img src={item01}/>
                            <h4>Product06</h4>
                            <div className="ratings">
                               <Rate disabled defaultValue={2}/>
                            </div>

                            <hr className="line"/>
                            <div className="row">
                                <div className="col-md-6 col-sm-6">
                                    <p className="price"><small>RM</small>29.90</p>
                                </div>
                                <div className="col-md-6 col-sm-6">
                                   <a href="#" className="add-to-cart" data-name="Product06" data-price="15.90">
                                     <button className="btn btn-info right"> Add to cart</button>
                                 </a>
                                </div>
                            </div>
                        </span>
                    </div>





                </div>
            </div>
        );
    }
}

export default VegeForm;
