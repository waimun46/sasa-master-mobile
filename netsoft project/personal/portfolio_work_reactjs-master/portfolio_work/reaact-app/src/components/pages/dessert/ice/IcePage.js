import React, {Component} from 'react';
import MenuPage from '../../../menu/MenuPage';
import IceForm from './IceForm';
import BottomMenu from '../../../menu/BottomMenu';
import Cart from '../../cart/Cart'

class IcePage extends Component {
    render() {
        return (
            <section id="IcePage">
                <MenuPage title="Ice Cream"/>
                <main>
                    <IceForm/>
                    <Cart/>
                </main>
                <BottomMenu/>
            </section>
        );
    }
}

export default IcePage;
