import React, {Component} from 'react';
import MenuPage from '../../../menu/MenuPage';
import DrinkForm from './DrinkForm';
import BottomMenu from '../../../menu/BottomMenu';
import Cart from '../../cart/Cart'

class DrinkPage extends Component {
    render() {
        return (
            <section id="DrinkPage">
                <MenuPage title="Soft Drink"/>
                <main>
                    <DrinkForm/>
                    <Cart/>
                </main>
                <BottomMenu/>
            </section>
        );
    }
}

export default DrinkPage;
