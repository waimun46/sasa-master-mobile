import React, {Component} from 'react';
import MenuPage from '../../menu/MenuPage';
import LocationForm from './LocationForm';
import BottomMenu from '../../menu/BottomMenu';
import Cart from '../cart/Cart'


class LocationPage extends Component {
    render() {
        return (
            <section id="LocationPage">
                <MenuPage title="Location"/>
                <main>
                    <LocationForm/>
                    <Cart/>
                </main>
                <BottomMenu/>
            </section>
        );
    }
}

export default LocationPage;
