import React, {Component} from 'react';
import MenuPage from '../../../menu/MenuPage';
import RiceForm from './RiceForm';
import BottomMenu from '../../../menu/BottomMenu';

import Cart from '../../cart/Cart'


class RicePage extends Component {
    render() {
        return (
            <section id="RicePage">
                <MenuPage title="Rice"/>
                <main>
                    <RiceForm/>
                    <Cart/>
                </main>
                <BottomMenu/>
            </section>
        );
    }
}

export default RicePage;
