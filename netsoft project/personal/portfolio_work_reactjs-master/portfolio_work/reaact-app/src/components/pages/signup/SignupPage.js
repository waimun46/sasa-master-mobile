import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router';
import {connect} from 'react-redux';
import SignupForm from './SignupForm';
import {userSingupRequest} from '../../../actions/TodoActions';


class SignupPage extends Component {
    render() {
        const {userSingupRequest} = this.props;
        return (
            <section id="SignupPage">
                <header>
                    <h4>Signup Today!</h4>
                    <Link to="/">
                        <div className="icon_signpage">
                            <i className=" faa-horizontal animated fal fa-long-arrow-left"
                               style={{fontSize: 28, color: '#fff'}}></i>

                        </div>
                    </Link>
                    <div className="clearfix"></div>
                </header>
                <hr/>
                <SignupForm userSingupRequest={userSingupRequest}/>
            </section>
        );
    }
}
SignupPage.propTypes = {
    userSingupRequest: PropTypes.func.isRequired
}


export default connect(null, {userSingupRequest})(SignupPage);
