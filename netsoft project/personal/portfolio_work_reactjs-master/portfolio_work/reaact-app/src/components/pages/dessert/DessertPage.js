import React, {Component} from 'react';
import $ from "jquery";
import MenuPage from '../../menu/MenuPage';
import BottomMenu from '../../menu/BottomMenu';
import DessertForm from './DessertForm';
import Cart from '../cart/Cart'

class DessertPage extends Component {
    render() {
        return (
            <section id="DessertPage">
                <MenuPage title="Dessert"/>
                <main>
                    <DessertForm/>
                    <Cart/>
                </main>
                <BottomMenu/>
            </section>
        );
    }
}

export default DessertPage;
