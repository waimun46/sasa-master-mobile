import React, {Component} from 'react';
import $ from "jquery";
import {Link} from 'react-router';
import {Icon, Avatar} from 'antd';


class HomeMenu extends Component {

    componentDidMount(){
        $(document).ready(function(){

            $('.menu-toggle').on('click', function(e){
                e.preventDefault();
                $('.menu').toggleClass('off');
            });

            $('.navicon').on('click', function() {
                $(this).toggleClass('open');
            });

        });
    }

    render() {
        return (
            <header id="MenuPage">
                <div className="menu-top">
                    <div className="icon_pos">
                        <a href="#" className="menu-toggle navicon menu1">
                            <span></span>
                            <span></span>
                            <span></span>
                        </a>
                    </div>
                    <nav className="menu off">
                        <ul className="nav">


                            <li className="li_height">
                                <div className="pro_sty">
                                    <div className="pro_pic">
                                        <Avatar style={{backgroundColor: '#858585'}} icon="user"/>
                                    </div>
                                    <div className="pro_name">
                                        <span>Mary</span>
                                        <div className="pro_balance">
                                            <span>your balance</span>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <hr/>

                            <li className="menu_title">
                                <div className="logo">
                                    <div className="logo_bg" style={{background: '#FF9800'}}>
                                        <i className="fal fa-utensils" style={{fontSize: 18, color: '#fff'}}></i>
                                    </div>
                                </div>
                                <div className="title">
                                    <span>Food</span>
                                </div>
                                <div className="clearfix"></div>
                            </li>

                            <li className="menu_title">
                                <div className="logo">
                                    <div className="logo_bg" style={{background: '#4CAF50'}}>
                                        <i className="fal fa-glass-martini" style={{fontSize: 18, color: '#fff'}}></i>
                                    </div>
                                </div>
                                <div className="title">
                                    <span>Beverage</span>
                                </div>
                                <div className="clearfix"></div>
                            </li>

                            <li className="menu_title">
                                <div className="logo">
                                    <div className="logo_bg" style={{background: '#FF5722'}}>
                                        <i className="fal fa-map-marker-alt" style={{fontSize: 18, color: '#fff'}}></i>
                                    </div>
                                </div>
                                <div className="title">
                                    <span>Restaurant</span>
                                </div>
                                <div className="clearfix"></div>
                            </li>

                            <li className="menu_title">
                                <div className="logo">
                                    <div className="logo_bg" style={{background: '#f03978'}}>
                                        <i className="fal fa-cart-arrow-down" style={{fontSize: 18, color: '#fff'}}></i>
                                    </div>
                                </div>
                                <div className="title">
                                    <span>Your order</span>
                                </div>
                                <div className="clearfix"></div>
                            </li>

                            <hr/>

                            <div className="menu_bottom">
                                <li className="menu_title">
                                    <div className="logo">
                                        <i className="fal fa-globe" style={{fontSize: 20, color: '#fff'}}></i>
                                    </div>
                                    <div className="title">
                                        <span>language</span>
                                    </div>
                                    <div className="clearfix"></div>
                                </li>
                                <li className="menu_title">
                                    <div className="logo">
                                        <i className="fal fa-sliders-h" style={{fontSize: 20, color: '#fff'}}></i>
                                    </div>
                                    <div className="title">
                                        <span>Setting</span>
                                    </div>
                                    <div className="clearfix"></div>
                                </li>
                                <Link to="login">
                                    <li className="menu_title">
                                        <div className="logo ">
                                            <i className="faa-horizontal animated  fal fa-sign-out-alt"
                                               style={{fontSize: 20, color: '#fff'}}></i>
                                        </div>
                                        <div className="title">
                                            <span>Logout</span>
                                        </div>
                                        <div className="clearfix"></div>
                                    </li>
                                </Link>
                            </div>

                        </ul>

                    </nav>

                </div>

            </header>
        );
    }
}

export default HomeMenu;
