import React, {Component} from 'react';
import $ from "jquery";
import {Badge} from 'antd';
import {Link} from 'react-router';
import ReactTimeout from 'react-timeout'

class BottomMenu extends Component {

    componentDidMount() {
        $(document).ready(function () {
            $('.material-button-toggle').on("click", function () {
                $(this).toggleClass('open');
                $('.option').toggleClass('scale-on');
            });
        });
    }


    render() {
        return (
            <section id="BottomMenu">
                <nav className="navigation navigation--inline">
                    <ul>
                        <Link to="profile">
                            <li>
                                <div className="icon">
                                    <i className="fal fa-user-alt"></i>
                                </div>
                            </li>
                        </Link>

                            <Link to="home"  >
                                <li>
                                    <div className="icon">
                                        <i className="fal fa-home"></i>
                                    </div>

                                </li>
                            </Link>



                        <Link to="">
                            <li>
                                <div className="icon" data-toggle="modal" data-target="#cart">
                                    <i className="fal fa-cart-plus"></i>
                                    <span className="total-count"></span>
                                </div>
                            </li>
                        </Link>
                    </ul>
                </nav>
            </section>
        );
    }
}

export default BottomMenu;
