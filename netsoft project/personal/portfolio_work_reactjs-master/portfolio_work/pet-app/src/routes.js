import React, {Component} from 'react';
import {Route, Router, browserHistory} from 'react-router';
import LoginPage from './components/pages/login/LoginPage';
import MenuPage from './components/menu/MenuPage';
import ShopPage from './components/pages/shop/ShopPage';
import AdopPage from './components/pages/adop/AdopPage';
import VideoPage from './components/pages/video/VideoPage';


class Routes extends Component {

    render() {
        return (
            <Router onUpdate={() => window.scrollTo(0, 0)} history={browserHistory}>
                <Route exact path="/" component={LoginPage}/>
                <Route exact path="/home" component={MenuPage}/>
                <Route exact path="/shop" component={ShopPage}/>
                <Route exact path="/adopt" component={AdopPage}/>
                <Route exact path="/video" component={VideoPage}/>
            </Router>
        );
    }
}

export default Routes;
