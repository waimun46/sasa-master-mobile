import React, {Component} from 'react';
import MenuTop from '../../menu/MenuTop'
import BottomMenu from '../../menu/BottomMenu'
import TopVideo from './content/TopVideo'
import VideoMid from './content/VideoMid'
import Modal from '../../modal/Modal'


class VideoPage extends Component {
    render() {
        return (
            <main id="VideoPage">
                <MenuTop title="FUNNY VIDEO"/>
                <TopVideo/>
                <VideoMid/>
                <Modal/>
                <div className="clearfix"></div>
                <BottomMenu/>
            </main>
        );
    }
}

export default VideoPage;
