import React, {Component} from 'react';
import {Icon, Avatar} from 'antd';
import YouTube from '@u-wave/react-youtube';

import vlogo from '../../../../images/vlogo.png'

const IconText = ({type, text}) => (
    <span>
    <Icon type={type} style={{marginRight: 8}}/>
        {text}
  </span>
);

class TopVideo extends Component {
    render() {

        return (
            <section id="TopVideo">
                <YouTube
                    className="video_top"
                    video="0faomPR2BJ8"
                    autoplay={false}
                    showRelatedVideos={false}
                />
                <div className="top_title">
                    <Avatar src={vlogo}/>
                    <div className="top_res">
                    <h3>Funny Dancing Dogs Compilation</h3>
                    <p>92,617,269 <small>views</small></p>
                    </div>
                    <div className="clearfix"></div>
                    <div className="icon_text">
                        <div className="icon_sty">
                            <IconText type="star-o" text="156"/>
                        </div>
                        <div className="icon_sty">
                            <IconText type="like-o" text="156"/>
                        </div>
                        <IconText type="message" text="2"/>
                    </div>
                </div>
                <div className="clearfix"></div>
            </section>
        );
    }
}

export default TopVideo;
