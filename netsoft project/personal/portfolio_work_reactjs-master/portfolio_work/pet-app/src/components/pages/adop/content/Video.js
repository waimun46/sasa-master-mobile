import React, {Component} from 'react';
import pet from '../../../../video/pet.mp4'


class Buttons extends React.Component {
    render() {
        return (
            <div>
                <button style={{marginRight: '20px'}} className="myBtn" id='playButton' onClick={this.props.playVideo}>
                    <i className="fas fa-play"></i>
                </button>
                <button className="myBtn" id='pauseButton' onClick={this.props.pauseVideo}>
                    <i className="fas fa-pause"></i>
                </button>
            </div>
        );
    }
}

class Video extends Component {

    playVideo() {
        this.refs.vidRef.play();
    }

    pauseVideo() {
        this.refs.vidRef.pause();
    }

    render() {
        return (
            <section id="Video">
                <video ref="vidRef" autoPlay loop id="myVideo">
                    <source src={pet} type="video/mp4"/>
                    Your browser does not support HTML5 video.
                </video>
                <div className="content">
                    <Buttons playVideo={this.playVideo.bind(this)} pauseVideo={this.pauseVideo.bind(this)}/>
                </div>
                <div className="clearfix"></div>
            </section>
        );
    }
}

export default Video;
