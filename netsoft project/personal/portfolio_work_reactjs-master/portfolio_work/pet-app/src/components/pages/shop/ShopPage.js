import React, {Component} from 'react';
import MenuTop from '../../menu/MenuTop'
import BottomMenu from '../../menu/BottomMenu'
import BannerSlider from './content/BannerSlider'
import LogoSlider from './content/LogoSlider'
import DogFood from './content/DogFood'
import PetAccessories from './content/PetAccessories'
import Modal from '../../modal/Modal'

class ShopPage extends Component {
    render() {
        return (
            <main id="ShopPage">
                <MenuTop title="PET SHOP"/>
                <BannerSlider/>
                <LogoSlider/>
                <DogFood/>
                <PetAccessories/>
                <Modal/>
                <div className="clearfix"></div>
                <BottomMenu/>
            </main>
        );
    }
}

export default ShopPage;
