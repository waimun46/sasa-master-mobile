import React, {Component} from 'react';
import {Icon, Card, Avatar} from 'antd';
import YouTube from '@u-wave/react-youtube';
import vlogo from '../../../../images/v2.jpg'

const {Meta} = Card;

const IconText = ({type, text}) => (
    <span>
    <Icon type={type} style={{marginRight: 8}}/>
        {text}
  </span>
);

class VideoMid extends Component {
    render() {
        return (
            <section id="VideoMid">
                <Card title="- More Funny -">
                    <div className="card_pan">
                        <Card
                            cover={
                                <YouTube
                                    className="video_mid"
                                    video="DOxtzaUFpyY"
                                    autoplay={false}
                                    showRelatedVideos={false}
                                />}
                            actions={[<IconText type="star-o" text="196" />, <IconText type="like-o" text="1886" />, <IconText type="message" text="4" />]}
                        >
                            <Meta
                                avatar={<Avatar src={vlogo}/>}
                                title="Animals Scaring Kids At Zoo"
                                description={ <p>13,817,929 <small>views</small></p>}
                            />
                        </Card>
                    </div>
                    <div className="card_pan">
                        <Card
                            cover={
                                <YouTube
                                    className="video_mid"
                                    video="HnOmci2OhIY"
                                    autoplay={false}
                                    showRelatedVideos={false}
                                />}
                            actions={[<IconText type="star-o" text="656" />, <IconText type="like-o" text="106" />, <IconText type="message" text="62" />]}
                        >
                            <Meta
                                avatar={<Avatar src={vlogo}/>}
                                title="Cães e gato engraçados"
                                description={ <p>42,577,729 <small>views</small></p>}
                            />
                        </Card>
                    </div>
                    <div className="card_pan">
                        <Card
                            cover={
                                <YouTube
                                    className="video_mid"
                                    video="8D_mrFt8_Mk"
                                    autoplay={false}
                                    showRelatedVideos={false}
                                />}
                            actions={[<IconText type="star-o" text="196" />, <IconText type="like-o" text="56" />, <IconText type="message" text="25" />]}
                        >
                            <Meta
                                avatar={<Avatar src={vlogo}/>}
                                title="Funny Ducks and Dogs Playing"
                                description={ <p>62,517,929 <small>views</small></p>}
                            />
                        </Card>
                    </div>
                    <div className="card_pan">
                        <Card
                            cover={
                                <YouTube
                                    className="video_mid"
                                    video="KQ48jTfOtvE"
                                    autoplay={false}
                                    showRelatedVideos={false}
                                />}
                            actions={[<IconText type="star-o" text="756" />, <IconText type="like-o" text="856" />, <IconText type="message" text="20" />]}
                        >
                            <Meta
                                avatar={<Avatar src={vlogo}/>}
                                title="top 10 Smartest Dogs in the world"
                                description={ <p>19,567,229 <small>views</small></p>}
                            />
                        </Card>
                    </div>
                    <div className="card_pan">
                        <Card
                            cover={
                                <YouTube
                                    className="video_mid"
                                    video="3Z4fQ19TQvU"
                                    autoplay={false}
                                    showRelatedVideos={false}
                                />}
                            actions={[<IconText type="star-o" text="856" />, <IconText type="like-o" text="256" />, <IconText type="message" text="28" />]}
                        >
                            <Meta
                                avatar={<Avatar src={vlogo}/>}
                                title="Cute Parrot and Cat Videos"
                                description={ <p>2,517,269 <small>views</small></p>}
                            />
                        </Card>
                    </div>
                    <div className="card_pan">
                        <Card
                            cover={
                                <YouTube
                                    className="video_mid"
                                    video="LgBKhj48VDY"
                                    autoplay={false}
                                    showRelatedVideos={false}
                                />}
                            actions={[<IconText type="star-o" text="126" />, <IconText type="like-o" text="156" />, <IconText type="message" text="2" />]}
                        >
                            <Meta
                                avatar={<Avatar src={vlogo}/>}
                                title="Useful Dog Tricks 2"
                                description={ <p>42,597,516<small>views</small></p>}
                            />
                        </Card>
                    </div>
                    <div className="card_pan">
                        <Card
                            cover={
                                <YouTube
                                    className="video_mid"
                                    video="0vnyBg0lMaw"
                                    autoplay={false}
                                    showRelatedVideos={false}
                                />}
                            actions={[<IconText type="star-o" text="166" />, <IconText type="like-o" text="86" />, <IconText type="message" text="21" />]}
                        >
                            <Meta
                                avatar={<Avatar src={vlogo}/>}
                                title="抖音里的白鲸之恋"
                                description={ <p>7,011,395<small>views</small></p>}
                            />
                        </Card>
                    </div>
                    <div className="card_pan">
                        <Card
                            cover={
                                <YouTube
                                    className="video_mid"
                                    video="Jy2PVETB9JY"
                                    autoplay={false}
                                    showRelatedVideos={false}
                                />}
                            actions={[<IconText type="star-o" text="186" />, <IconText type="like-o" text="56" />, <IconText type="message" text="78" />]}
                        >
                            <Meta
                                avatar={<Avatar src={vlogo}/>}
                                title="Dachshunds Are Awesome!"
                                description={ <p>42,597,299 <small>views</small></p>}
                            />
                        </Card>
                    </div>
                    <div className="card_pan">
                        <Card
                            cover={
                                <YouTube
                                    className="video_mid"
                                    video="1-BfDBTFLSc"
                                    autoplay={false}
                                    showRelatedVideos={false}
                                />}
                            actions={[<IconText type="star-o" text="556" />, <IconText type="like-o" text="196" />, <IconText type="message" text="72" />]}
                        >
                            <Meta
                                avatar={<Avatar src={vlogo}/>}
                                title="Angry Dogs Compilation"
                                description={ <p>17,587,229 <small>views</small></p>}
                            />
                        </Card>
                    </div>
                    <div className="card_pan">
                        <Card
                            cover={
                                <YouTube
                                    className="video_mid"
                                    video="la35T-czwyg"
                                    autoplay={false}
                                    showRelatedVideos={false}
                                />}
                            actions={[<IconText type="star-o" text="456" />, <IconText type="like-o" text="186" />, <IconText type="message" text="29" />]}
                        >
                            <Meta
                                avatar={<Avatar src={vlogo}/>}
                                title="Dog Really Hates Middle Finger"
                                description={ <p>5,268,687 <small>views</small></p>}
                            />
                        </Card>
                    </div>
                    <div className="card_pan">
                        <Card
                            cover={
                                <YouTube
                                    className="video_mid"
                                    video="rbLZ0RsRmy0"
                                    autoplay={false}
                                    showRelatedVideos={false}
                                />}
                            actions={[<IconText type="star-o" text="456" />, <IconText type="like-o" text="56" />, <IconText type="message" text="72" />]}
                        >
                            <Meta
                                avatar={<Avatar src={vlogo}/>}
                                title="A genius Malamute dog"   
                                description={ <p>12,517,229 <small>views</small></p>}
                            />
                        </Card>
                    </div>
                    <div className="card_pan">
                        <Card
                            cover={
                                <YouTube
                                    className="video_mid"
                                    video="r31zuLWiUk0"
                                    autoplay={false}
                                    showRelatedVideos={false}
                                />}
                            actions={[<IconText type="star-o" text="756" />, <IconText type="like-o" text="168" />, <IconText type="message" text="52" />]}
                        >
                            <Meta
                                avatar={<Avatar src={vlogo}/>}
                                title="Funny Dachshund Compilation"
                                description={ <p>12,831,856 <small>viewsfsdf</small></p>}

                            />
                        </Card>
                    </div>
                </Card>
            </section>
        );
    }
}

export default VideoMid;
