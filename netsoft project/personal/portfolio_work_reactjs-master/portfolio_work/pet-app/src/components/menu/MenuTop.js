import React, {Component} from 'react';


class MenuTop extends Component {
    render() {
        const {title} = this.props;
        return (
            <article id="MenuTop">
                <div className="title">
                    <h3>{title}</h3>
                </div>
                <div className="back_btn">
                    <i className="far fa-angle-left"></i>
                </div>
            </article>
        );
    }
}

export default MenuTop;
