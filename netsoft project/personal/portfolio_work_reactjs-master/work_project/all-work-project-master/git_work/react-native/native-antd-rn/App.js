/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,ScrollView} from 'react-native';
import Button from '@ant-design/react-native/lib/button';
import Icon  from '@ant-design/react-native/lib/icon';
import {  Drawer, List, WhiteSpace } from '@ant-design/react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});


export default class App extends Component{
  constructor() {
    super(...arguments);
    this.onOpenChange = isOpen => {
      /* tslint:disable: no-console */
      console.log('是否打开了 Drawer', isOpen.toString());
    };
  }
  render() {
    const itemArr = Array.apply(null, Array(20))
    .map(function(_, i) {
      return i;
    })
    .map((_i, index) => {
      if (index === 0) {
        return (
          <List.Item
            key={index}
            thumb="https://zos.alipayobjects.com/rmsportal/eOZidTabPoEbPeU.png"
            multipleLine
          >
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}
            >
              <Text>Categories - {index}</Text>
              <Button
                type="primary"
                size="small"
                onPress={() => this.drawer.closeDrawer()}
              >
                close drawer
              </Button>
            </View>
          </List.Item>
        );
      }
      return (
        <List.Item
          key={index}
          thumb="https://zos.alipayobjects.com/rmsportal/eOZidTabPoEbPeU.png"
        >
          <Text>Categories - {index}</Text>
        </List.Item>
      );
    });
  // Todo: https://github.com/DefinitelyTyped/DefinitelyTyped
  const sidebar = (
    <ScrollView style={[styles.container]}>
      <List>{itemArr}</List>
    </ScrollView>
  );
    return (
      <Drawer
      sidebar={sidebar}
      position="right"
      open={false}
      drawerRef={el => (this.drawer = el)}
      onOpenChange={this.onOpenChange}
      drawerBackgroundColor="#ccc"
    >
      <View style={styles.container}>
        <View style={styles.navbar} >
          <Text style={{color: 'white'}}><Icon name="left" size="md" color="white" /></Text>
          <Text style={{color: 'white', fontSize: 20}}>Home</Text>
          
      
          <Text style={{color: 'white'}} onPress={() => this.drawer && this.drawer.openDrawer()}><Icon name="ellipsis" size="md" color="white" /></Text>
          
        </View>
      
     
      </View>
      </Drawer>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F5FCFF',
  },
  navbar: {
    backgroundColor: 'black',
    padding: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  }

});
