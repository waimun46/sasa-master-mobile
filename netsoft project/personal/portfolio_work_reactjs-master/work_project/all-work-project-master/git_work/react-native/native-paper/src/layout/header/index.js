import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import { Appbar } from 'react-native-paper';

class Header extends Component{
  render() {
    return (
      <Appbar.Header>
        <Appbar.BackAction
          onPress={this._goBack}
        />
        <Appbar.Content
          title="Title"
          subtitle="Subtitle"
        />
        <Appbar.Action icon="search" onPress={this._onSearch} />
        <Appbar.Action icon="more-vert" onPress={this._onMore} />
      </Appbar.Header>
    );
  }
}



export default Header;