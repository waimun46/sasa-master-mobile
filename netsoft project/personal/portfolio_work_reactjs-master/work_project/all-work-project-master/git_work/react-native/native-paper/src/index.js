import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import Header from './layout/header'
import Footer from './layout/footer'
import MainContainer from './layout/main'

class Layout extends Component{
  render() {
    return (
        <View> 
            <Header/>
                <MainContainer/> 
            <Footer/>
        </View>
    );
  }
}



export default Layout;