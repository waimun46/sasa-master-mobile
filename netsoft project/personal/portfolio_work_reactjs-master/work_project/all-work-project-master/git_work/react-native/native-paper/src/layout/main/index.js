import React, {Component} from 'react';
import ContentWarp from '../../components'

function MainContainer({ children }) {
    return <ContentWarp>{children}</ContentWarp>  
} 

export default MainContainer;
