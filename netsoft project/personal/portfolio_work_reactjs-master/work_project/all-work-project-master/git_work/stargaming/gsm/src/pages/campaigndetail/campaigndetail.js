import React, { Component } from 'react';
import _fetch from "../../fetchApi";
import sessionExpired from "../../components/SessionExpired";
import {connect} from "react-redux";
import Pagination from '../../components/Pagination/Pagination';


class CampaignDetail extends Component {

    state = {
        campaign: localStorage.getItem('campaign') || 0,
        optStatus: '-1',

        initDone: false,
        content: [],
        mapStatus: [],
        totalRecord: 0,
        totalPage: 0,
        curPage: 1
    }

    componentDidMount = () => {
        this.list(1)
    }

    list = (page, status) => {

        let optStatus = status || this.state.optStatus;

        _fetch({
            api: 'Client/Campaign',
            data: {
                action: 'detail',
                campaign: this.state.campaign,
                status: optStatus,
                page: page || 1
            }
        }).then(res => res.json()).then((data) => {
            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {

                }
                sessionExpired(data)
            } else {
                this.setState({
                    initDone: true,
                    content: data.data.reports,
                    mapStatus: data.data.map_status,
                    totalRecord: data.data.total_record,
                    totalPage: data.data.total_page,
                    curPage: data.data.cur_page,
                    optStatus: optStatus,
                });
            }
        })
    }

    back = () => {
        localStorage.removeItem('campaign');
        this.props.history.push('/Campaigns')
    }

    render() {
        return (
            <article className="main_content">
                <header>
                    <h2>{window.intl.get('action.detail')}</h2>
                </header>
                <section className="page_content">

                    <div className="title-btn">
                        <span className="backBtn" onClick={this.back}>{window.intl.get('other.back')}</span>
                    </div>

                    {/*Content insert here*/}
                    <div className="userprofile_content">

                        {
                            this.state.initDone &&
                            <div>
                                <div className="row select_margin">
                                    <label>{window.intl.get('other.status')} :  &nbsp;</label>
                                    <select className="formSelect" value={this.state.optStatus}
                                            onChange={(e) => {this.list(1, e.target.value)}}>
                                        <option value='-1'>{window.intl.get('select.all')}</option>
                                        {
                                            this.state.mapStatus && this.state.mapStatus[this.props.local] && this.state.mapStatus[this.props.local].map((item, index) => {
                                                return (
                                                    <option value={item.key.toString()} key={index}>{item.value}</option>
                                                )
                                            })
                                        }
                                    </select>
                                </div>

                                <div id="table_body">
                                    <table className="table_style report">
                                        <thead>
                                        <tr>
                                            <th>{window.intl.get('sms.phone')}</th>
                                            <th>{window.intl.get('campaign.group')}</th>
                                            <th>{window.intl.get('sms.content')}</th>
                                            <th>{window.intl.get('other.status')}</th>
                                            <th>{window.intl.get('date.datetime')}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            this.state.content && this.state.content[this.props.local] &&
                                            this.state.content[this.props.local].map((item, key) => {
                                                return (
                                                    <tr key={key}>
                                                        <td>{item.receiver}</td>
                                                        <td>{item.group}</td>
                                                        <td>{item.message}</td>
                                                        <td>{item.status}</td>
                                                        <td>{item.created_date}</td>
                                                    </tr>
                                                )
                                            })
                                        }
                                        </tbody>
                                    </table>
                                </div>
                                <Pagination totalRecord={this.state.totalRecord} totalPage={this.state.totalPage} curPage={this.state.curPage} switchPage={this.list}/>
                            </div>
                        }

                    </div>
                    {/*Content insert here*/}
                </section>
            </article>
        );
    }
}

function mapProps(state){
    return {
        local:state.local
    }
}

export default connect(mapProps)(CampaignDetail)
