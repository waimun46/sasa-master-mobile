import React, {Component} from 'react';
import {connect} from 'react-redux';
import {handleLocal} from "../../redux/action";
import NavLang from '../NavLang/NavLang';
import './PageWrap.css';

class PageWrap extends Component {
    render() {
        return (
            <div>
                <div className="navLang"><NavLang/></div>
                <div className="body">
                    {
                        this.props.children
                    }
                </div>
            </div>
        )
    }
}


function mapStateToProps(state) {
    return {
        local: state.local
    }
}

function mapDispatchToProps(dispatch) {
    return {
        handleLocal: (local) => {
            dispatch(handleLocal(local))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PageWrap);