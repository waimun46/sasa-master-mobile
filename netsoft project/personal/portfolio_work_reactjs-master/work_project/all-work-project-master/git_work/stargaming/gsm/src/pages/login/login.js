import React, {Component} from 'react';
import {connect} from 'react-redux';
import {handleLogin, saveClient, refreshPage, allowMenu} from "../../redux/action";
import _fetch from '../../fetchApi';

import logo from '../../images/logo.png';
import Footer from '../../components/Footer';
import NavLang from '../../components/NavLang/NavLang';

import './login.css';

class Login extends Component {

    constructor() {
        super();

        this.state = {
            account: '',
            password: '',
            loginLoad: false,
            errorMsg: ''
        }
    }

    componentDidMount = () => {
        if (this.props.isLogin)
            this.props.history.push('/welcome')
    }

    handleChangeField = (field, value) => {
        this.setState({
            [field]: value
        })
    };

    login = () => {
        if (this.state.loginLoad) {
            return
        }
        if (this.state.account.trim().length === 0) {
            this.setState({
                errorMsg: window.intl.get('error.accountEmpty'),
            });
            return
        }
        if (this.state.password.trim().length === 0) {
            this.setState({
                errorMsg: window.intl.get('error.passwordEmpty'),
            });
            return
        }
        this.setState({
            loginLoad: true,
        });
        _fetch({
            api: 'Client/Login',
            data: {
                username: this.state.account,
                password: this.state.password
            }
        }).then(res => res.json()).then((data) => {
            this.setState({
                loginLoad: false,
            });

            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {

                }
                if (data.msg) {
                    this.setState({
                        errorMsg: data.msg[this.props.local],
                    });
                }
            } else {
                this.props.handleLogin(true);
                this.props.saveClient(this.state.account);
                this.props.refreshPage(true);
                this.props.allowMenu(data.data.menu);
            }
        })
    };

    render() {
        return (
            <div id="login-page" className="login-page">
                <div className="mylogo">
                    <a href={process.infoUrl}><img src={logo} alt="" /></a>
                </div>
                <span className="errorMsg">{this.state.errorMsg}</span>
                <div className="form">
                    <form className="login-form">
                        <div className="input-group">
                            <span className="input-group-addon"><i className="glyphicon glyphicon-user"/></span>
                            <input type="text" placeholder={window.intl.get('account.account')}
                                   onInput={(event) => {
                                        this.handleChangeField('account', event.target.value.trim())
                                   }}/>
                        </div>
                        <div className="input-group">
                            <span className="input-group-addon"><i className="glyphicon glyphicon-user"/></span>
                            <input type="password" placeholder={window.intl.get('account.password')}
                                   onInput={(event) => {
                                        this.handleChangeField('password', event.target.value.trim())
                                    }}/>
                        </div>

                        <div className="form_button" onClick={this.login}>{window.intl.get('button.login')}</div>
                        <div className="box">
                            {
                                /*
                                <p className="forgotp"><span>{window.intl.get('account.forgot')}?</span></p>
                                 */
                            }
                            <p className="forgotp"><span>&nbsp;</span></p>
                            <div className="navLang">
                                <NavLang/>
                            </div>
                        </div>
                    </form>
                </div>
                <Footer />
            </div>
        )
    }
}

function mapProps(state){
    return {
        local:state.local,
        isLogin:state.isLogin
    }
}

function mapAction(dispatch){
    return {
        handleLogin:(isLogin)=>{
            dispatch(handleLogin(isLogin))
        },
        saveClient:(client)=>{
            dispatch(saveClient(client))
        },
        refreshPage:(bool)=>{
            dispatch(refreshPage(bool))
        },
        allowMenu:(menu) => {
            dispatch(allowMenu(menu))
        }

    }
}

export default connect(mapProps,mapAction)(Login)

