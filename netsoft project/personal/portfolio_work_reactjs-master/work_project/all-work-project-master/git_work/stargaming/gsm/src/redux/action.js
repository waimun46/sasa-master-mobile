/**
 * action 类型
 */
export const ADD_TODO = 'ADD_TODO';
export const COMPLETE_TODO = 'COMPLETE_TODO';
export const SET_VISIBILITY_FILTER = 'SET_VISIBILITY_FILTER';

/*
* action 其他常量
 */
export const VisibilityFilters = {
    SHOW_ALL:'SHOW_ALL',
    SHOW_COMPLETED:'SHOW_COMPLETED',
    SHOW_ACTIVE:'SHOW_ACTIVE'
}


/*
* action 构造函数
 */
export function addTodo(text){
    return{
        type:ADD_TODO,
        text
    }
}

export function completeTodo(index){
    return{
        type:COMPLETE_TODO,
        index
    }
}

export function setVisibilityFilters(filter){
    return{
        type:SET_VISIBILITY_FILTER,
        filter
    }
}

export function changeLanguage(language){
    return {
        type:'changeLanguage',
        language:language
    }
}

export function saveUserInfo(userinfo){
    return {
        type:'saveUserInfo',
        userinfo:userinfo
    }
}

export function changeUsername(username){
    return {
        type:'changeUsername',
        username
    }
}

export function handleLocal (local) {
    return {
        type: 'handleLocal',
        local,
    }
}

export function handleLogin (isLogin) {
    return {
        type: 'handleLogin',
        isLogin,
    }
}

export function saveClient (client) {
    return {
        type: 'saveClient',
        client,
    }
}

export function refreshPage (bool) {
    return {
        type: 'refreshPage',
        bool,
    }
}

export function handleForwardMsg (campaign) {
    return {
        type: 'handleForwardMsg',
        campaign,
    }
}

export function allowMenu (menu) {
    return {
        type: 'allowMenu',
        menu,
    }
}