import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import { VisibilityFilters} from '../../redux/action'
import Footer from '../../components/Footer'

import Login from '../login/login';


class App extends Component {

    render() {
        return (
            <div className="App">
                <Login/>
                <Footer/>
            </div>
        );
    }
}

App.propTypes = {
    visibleTodos:PropTypes.arrayOf(PropTypes.shape({
        text:PropTypes.string.isRequired,
        completed:PropTypes.bool.isRequired
    }).isRequired).isRequired,
    visibilityFilter:PropTypes.oneOf([
        'SHOW_ALL',
        'SHOW_COMPLETED',
        "SHOW_ACTIVE"
    ]).isRequired
}

function selectTodos(todos,filter){
    console.log(todos);
    switch (filter){
        case VisibilityFilters.SHOW_ALL:
            return todos
        case VisibilityFilters.SHOW_COMPLETED:
            return todos.filter(todo => {return todo.completed})
        case VisibilityFilters.SHOW_ACTIVE:
            return todos.filter(todo => {return !todo.completed})
        default:
            console.log('not match any filter')
    }
}

function select(state){
    // console.log(state.todos);
    return {
        visibleTodos:selectTodos(state.todos,state.visibilityFilter),
        visibilityFilter:state.visibilityFilter
    }
}


export default connect(select)(App);
