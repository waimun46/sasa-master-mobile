import React, {Component} from 'react';
import { Router, Route, Switch } from "react-router-dom";
import createHistory from 'history/createBrowserHistory';

import {LocaleProvider} from 'antd';
import zh_CN from 'antd/lib/locale-provider/zh_CN';
import en_US from 'antd/lib/locale-provider/en_US';
import 'moment/locale/zh-cn';

import Login from './pages/login/login';
import Main from './pages/main/main';
import Userprofile from './pages/userprofile/userProfile';
import Apidoc from "./pages/apidoc/apidoc";
import Userpage from "./pages/userpage/userpage";
import Groupreceiver from "./pages/groupreceiver/groupreceiver";
import Viewgroup from "./pages/viewgroup/viewgroup";
import Createcamp from "./pages/createcamp/createcamp";
import Createuser from "./pages/createuser/createuser";
import Sendmsg from "./pages/sendmsg/sendmsg";
import Topuppage from "./pages/topuppage/topuppage";
import Addgroup from "./pages/addgroup/addgroup";
import Transferpage from "./pages/transferpage/transferpage";
import Voucherpackage from "./pages/voucherpackage/voucherpackage";
import Voucherlist from "./pages/voucherlist/voucherlist";
import Addvoucher from "./pages/addvoucher/addvoucher";
import {connect} from "react-redux";
import Footer from "./components/Footer";
import Menu from "./pages/menu/menu";
import Campaigns from "./pages/campaigns/campaigns";
import Campaigndetail from "./pages/campaigndetail/campaigndetail";
import Welcome from "./pages/welcome/welcome";

const history = createHistory();

class Routes extends Component {

    componentWillReceiveProps = (nextprops) => {
        if (nextprops.refreshPage) {
            window.location.href = window.location.href;
        }
    };

    render() {

        const notFound = () => <h1>404.. This page is not found!</h1>
        return (
            !this.props.refreshPage &&
            <Router history={history}>
                <LocaleProvider locale={this.props.local === "chinese" ? zh_CN : en_US}>
                    <Switch>
                        <Route exact path="/" component={Login}/>
                        <Route exact path="/login.html" component={Login}/>

                        <Route render={(obj) => {
                            return (

                                (
                                    obj.location.pathname === '/welcome' ||
                                    obj.location.pathname === '/main' ||
                                    (obj.location.pathname === '/Userprofile' && (this.props.menu.split && (this.props.menu.split(',').indexOf('SMS')!== -1))) ||
                                    (obj.location.pathname === '/Userpage' && (this.props.menu.split && (this.props.menu.split(',').indexOf('SMS')!== -1))) ||
                                    (obj.location.pathname === '/Createuser' && (this.props.menu.split && (this.props.menu.split(',').indexOf('SMS')!== -1))) ||
                                    (obj.location.pathname === '/Addgroup' && (this.props.menu.split && (this.props.menu.split(',').indexOf('SMS')!== -1))) ||
                                    (obj.location.pathname === '/Viewgroup' && (this.props.menu.split && (this.props.menu.split(',').indexOf('SMS')!== -1))) ||
                                    (obj.location.pathname === '/Apidoc' && (this.props.menu.split && (this.props.menu.split(',').indexOf('SMS')!== -1))) ||
                                    (obj.location.pathname === '/Sendmsg' && (this.props.menu.split && (this.props.menu.split(',').indexOf('SMS')!== -1))) ||
                                    (obj.location.pathname === '/Campaigns' && (this.props.menu.split && (this.props.menu.split(',').indexOf('SMS')!== -1))) ||
                                    (obj.location.pathname === '/Createcamp' && (this.props.menu.split && (this.props.menu.split(',').indexOf('SMS')!== -1))) ||
                                    (obj.location.pathname === '/Campaigndetail' && (this.props.menu.split && (this.props.menu.split(',').indexOf('SMS')!== -1))) ||
                                    (obj.location.pathname === '/Topuppage' && (this.props.menu.split && (this.props.menu.split(',').indexOf('SMS')!== -1))) ||
                                    (obj.location.pathname === '/Transferpage' && (this.props.menu.split && (this.props.menu.split(',').indexOf('SMS')!== -1))) ||
                                    (obj.location.pathname === '/Groupreceiver' && (this.props.menu.split && (this.props.menu.split(',').indexOf('SMS')!== -1))) ||
                                    (obj.location.pathname === '/Voucherpackage' && (this.props.menu.split && (this.props.menu.split(',').indexOf('Voucher')!== -1))) ||
                                    (obj.location.pathname === '/Voucherlist' && (this.props.menu.split && (this.props.menu.split(',').indexOf('Voucher')!== -1))) ||
                                    (obj.location.pathname === '/Addvoucher' && (this.props.menu.split && (this.props.menu.split(',').indexOf('Voucher')!== -1)))

                                )  ?
                                    <div id="page">
                                        <section className="page">
                                            <Menu pathname={obj.location.pathname}/>
                                            <Switch>
                                                <Route  path="/welcome" component={Welcome}/>
                                                <Route  path="/main" component={Main} />
                                                <Route  path="/Userprofile" component={Userprofile}/>
                                                <Route  path='/Userpage' component={Userpage}/>
                                                <Route  path='/Createuser' component={Createuser}/>
                                                <Route  path='/Addgroup' component={Addgroup}/>
                                                <Route  path='/Viewgroup' component={Viewgroup}/>
                                                <Route  path='/Apidoc' component={Apidoc}/>
                                                <Route  path='/Sendmsg' component={Sendmsg}/>
                                                <Route  path='/Campaigns' component={Campaigns}/>
                                                <Route  path='/Createcamp' component={Createcamp}/>
                                                <Route  path='/Campaigndetail' component={Campaigndetail}/>
                                                <Route  path='/Topuppage' component={Topuppage}/>
                                                <Route  path='/Transferpage' component={Transferpage}/>
                                                <Route  path='/Groupreceiver' component={Groupreceiver}/>
                                                <Route  path='/Voucherpackage' component={Voucherpackage}/>
                                                <Route  path='/Voucherlist' component={Voucherlist}/>
                                                <Route  path='/Addvoucher' component={Addvoucher}/>
                                            </Switch>
                                            <Footer/>
                                        </section>
                                    </div> :
                                    <Route component={notFound} />
                            )
                        }}/>

                    <Footer/>
                    </Switch>
                </LocaleProvider>
            </Router>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        refreshPage: state.refreshPage,
        local: state.local,
        menu: state.menu
    }
};

export default connect(mapStateToProps)(Routes);