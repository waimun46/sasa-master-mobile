import React , { Component } from 'react';

import _fetch from "../../fetchApi";
import sessionExpired from "../../components/SessionExpired";
import {connect} from "react-redux";
import Pagination from '../../components/Pagination/Pagination';

class Viewgroup extends  Component{

    state = {
        initDone: false,
        groupId: localStorage.getItem('group'),
        content: [],
        totalRecord: 0,
        totalPage: 0,
        curPage: 1,
        groupDetail: []
    };

    componentDidMount = () => {
        this.getList()
    };

    getList = (page) => {
        _fetch({
            api: 'Client/Contact',
            data: {
                action: 'detail',
                group: this.state.groupId,
                page: page || 1
            }
        }).then(res => res.json()).then((data) => {
            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {

                }
                sessionExpired(data)
            } else {
                this.setState({
                    groupDetail: data.data.group,
                    content: data.data.reports,
                    totalRecord: data.data.total_record,
                    totalPage: data.data.total_page,
                    curPage: data.data.cur_page,
                    initDone: true
                });
            }
        })
    };

    back = () => {
        localStorage.removeItem('group');
        this.props.history.push('/Groupreceiver')
    }

    render(){
        return (
            <article className="main_content">
                <header>
                    <h2>{window.intl.get('groupReceiver.title')}</h2>
                </header>
                <section className="page_content">
                    {/*content here*/}

                    <div className="title-btn" style={{padding:20}}>
                        <span className="backBtn" onClick={this.back}>{window.intl.get('other.back')}</span>
                    </div>

                    {
                        this.state.initDone &&
                        <div>
                            <p style={{paddingLeft:20}}><span>{window.intl.get('groupReceiver.groupName')}&nbsp;:&nbsp;{this.state.groupDetail['name']}</span></p>
                            <p style={{paddingLeft:20}}><span>{window.intl.get('date.created')}&nbsp;:&nbsp;{this.state.groupDetail['created_date']}</span></p>
                            <div id="table_body">
                                <table className="table_style report">
                                    <thead>
                                    <tr>
                                        <th>{window.intl.get('table.no')}</th>
                                        <th>{window.intl.get('group.phone')}</th>
                                        <th>{window.intl.get('group.firstName')}</th>
                                        <th>{window.intl.get('group.lastName')}</th>
                                        <th>{window.intl.get('group.gender')}</th>
                                        <th>{window.intl.get('group.birthDate')}</th>
                                        <th>{window.intl.get('group.state')}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {
                                        this.state.content && this.state.content[this.props.local] &&
                                        this.state.content[this.props.local].map((item, key) => {
                                            return (
                                                <tr key={key}>
                                                    <td>{key+1}</td>
                                                    <td>{item.phone}</td>
                                                    <td>{item.first_name}</td>
                                                    <td>{item.last_name}</td>
                                                    <td>{item.gender}</td>
                                                    <td>{item.birth_date}</td>
                                                    <td>{item.state}</td>
                                                </tr>
                                            )
                                        })
                                    }
                                    </tbody>
                                </table>
                            </div>
                            <Pagination totalRecord={this.state.totalRecord} totalPage={this.state.totalPage} curPage={this.state.curPage} switchPage={this.getList}/>
                        </div>
                    }
                    {/*content here*/}
                </section>
            </article>
        )
    }
}

function mapProps(state){
    return {
        local:state.local
    }
}

export default connect(mapProps)(Viewgroup);