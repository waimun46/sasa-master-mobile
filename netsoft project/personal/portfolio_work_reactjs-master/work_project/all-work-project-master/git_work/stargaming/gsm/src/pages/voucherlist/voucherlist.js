import React , { Component } from 'react';
import { Input, Modal, Button } from 'antd';
import _fetch from '../../fetchApi';
import {connect} from 'react-redux';

import {Select} from "antd/lib/index";
import sessionExpired from '../../components/SessionExpired';
import Pagination from '../../components/Pagination/Pagination';
import './voucherlist.css';
import '../../css/pages.css';

const Option = Select.Option;

class Voucherlist extends  Component{

    state = {
        visible: false,
        errorMsg: '',
        updateAction: '',
        voucherIds: [],
        optStatus: 0,

        content: [],
        totalRecord: 0,
        totalPage: 0,
        curPage: 1,

        status: '0',
        package: '0'
    };

    componentDidMount = () => {
        this.init();
        this.getVoucherList(1,0,0)
    };

    init = () => {
        _fetch({
            api: 'Client/Voucher',
            data: {
                action: 'init',
                action_init: 'list'
            }
        }).then(res => res.json()).then((data) => {
            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {

                }

                sessionExpired(data)
            } else {

                this.setState({
                    voucherPackage : data.data.voucherPackage,
                    voucherStatus: data.data.voucherStatus
                });
            }
        })
    };

    handlePackage = (value) => {
        this.getVoucherList('',value,this.state.status)
    };

    handleStatus =(value) => {
        this.getVoucherList('',this.state.package,value)
    };

    handleChangeField = (field, value) => {
        this.setState({
            [field]: value
        })
    };

    getVoucherList = (page,packageId,status,code) => {

        let voucherTarget = undefined;
        this.state.content && this.state.content[this.props.local] && this.state.content[this.props.local].forEach((item, index) => {
            voucherTarget = document.getElementById('chkVoucher' + item.id);
            voucherTarget.checked = false;
        });

        _fetch({
            api: 'Client/Voucher',
            data: {
                action: 'list',
                page: page || 1,
                status: status || this.state.status,
                package_id: packageId || this.state.package,
                code: code || this.state.code,
            }
        }).then(res => res.json()).then((data) => {
            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {

                }

                sessionExpired(data)
            } else {

                this.setState({
                    content: data.data.reports,
                    totalRecord: data.data.total_record,
                    totalPage: data.data.total_page,
                    curPage: data.data.cur_page,
                    errorMsg: '',
                    status: status || this.state.status,
                    package: packageId || this.state.package,
                    code: code || this.state.code
                });
            }
        })
    };

    createUser = () => {
        this.props.history.push('/Addvoucher')
    };

    checkAll = (event) => {
        let target = document.getElementById('chkAll');
        let voucherTarget = undefined;
        if (event.target.tagName === 'TH')
            target.checked = !target.checked;

        if (target.checked) {
            let voucherIds = [];
            this.state.content && this.state.content[this.props.local] && this.state.content[this.props.local].forEach((item, index) => {
                voucherTarget = document.getElementById('chkVoucher' + item.id);
                voucherTarget.checked = true;
                voucherIds.push(item.id);
            });

            this.setState({
                voucherIds: voucherIds
            })
        } else {
            this.state.content && this.state.content[this.props.local] && this.state.content[this.props.local].forEach((item, index) => {
                voucherTarget = document.getElementById('chkVoucher' + item.id);
                voucherTarget.checked = false;
            });

            this.setState({
                voucherIds: []
            })
        }
    };

    checkOne = (event, voucherId) => {
        let target = document.getElementById('chkVoucher' + voucherId);
        if (event.target.tagName === 'TD')
            target.checked = !target.checked;

        this.updateCheckedVoucher(target)
    };

    updateCheckedVoucher = (target) => {
        let voucherIds = this.state.voucherIds;
        let value = target.value;
        let index = voucherIds.indexOf(value);

        if (target.checked) {
            if (index === -1) {
                voucherIds.push(value);
                this.setState({
                    voucherIds: voucherIds
                })
            }
        } else {
            if (index !== -1) {
                voucherIds.splice(index, 1);
                this.setState({
                    voucherIds: voucherIds
                })
            }
        }
    };

    showUpdate = (action) => {
        this.setState({
            visible: true,
            updateAction: action,
            errorMsg: ''
        });
    };

    updateCancel = () => {
        this.setState({
            visible: false,
            errorMsg: ''
        });
    };


    updateVoucher = () => {
        _fetch({
            api: 'Client/Voucher',
            data: {
                action: this.state.updateAction,
                voucher: this.state.voucherIds
            }
        }).then(res => res.json()).then((data) => {
            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {

                }

                sessionExpired(data);
                if (data.msg) {
                    this.setState({
                        errorMsg: data.msg[this.props.local],
                    });
                }
            } else {

                this.setState({
                    visible: false,
                    updateAction: '',
                    errorMsg: '',
                    voucherIds: []
                });

                this.getVoucherList()
            }
        })
    };

    render(){
        return (
            <article className="main_content">
                <header>
                    <h2>{window.intl.get('voucher.voucherList')}</h2>
                </header>
                <section className="page_content">
                    <div id="userpage">
                        <div className="title-btn">
                            <span className="createuser_btn createBtn" onClick={this.createUser}>{window.intl.get('voucher.generateVoucher')}</span>
                        </div>

                        {/*content over here*/}
                        <div id="table_body">
                            <div className="select_margin">
                                <label>{window.intl.get('voucher.package')} :  &nbsp;</label>
                                <Select className="formSelect" value={this.state.package} onChange={this.handlePackage}//
                                   >
                                    <Option value='0'>{window.intl.get('select.all')}</Option>
                                    {
                                        this.state.voucherPackage && this.state.voucherPackage.map((item, index) => {
                                            return (
                                                <Option value={item.id} key={index}>{item.name}</Option>
                                            )
                                        })
                                    }
                                </Select>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <label>{window.intl.get('other.status')} :  &nbsp;</label>
                                <Select className="formSelect" value={this.state.status} onChange={this.handleStatus}>
                                    <Option value='0'>{window.intl.get('select.all')}</Option>
                                    {
                                        this.state.voucherStatus && this.state.voucherStatus[this.props.local] && this.state.voucherStatus[this.props.local].map((item, index) => {
                                            return (
                                                <Option value={item.id} key={index}>{item.status}</Option>
                                            )
                                        })
                                    }
                                </Select>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <label>{window.intl.get('voucher.code')}:&nbsp;</label>
                                <Input className="voucherTextBox" name="code" onInput={(event) => {
                                    this.handleChangeField('code', event.target.value.trim())
                                }}/>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <Button onClick={() => {this.getVoucherList(1, this.state.package, this.state.status,this.state.code)}} className=""> {window.intl.get('button.search')}</Button>


                            </div>
                            <Button style={{backgroundColor:'#228b22'}} onClick={() => {this.showUpdate('activate')}}> {window.intl.get('action.activate')}</Button>&nbsp;&nbsp;
                            <Button style={{backgroundColor:'#ff0000'}} onClick={() => {this.showUpdate('ban')}}> {window.intl.get('action.ban')}</Button>

                            <div className='totalRecord'>
                                <label>{window.intl.get('pagination.totalRecord')} : </label>
                                <span>{this.state.totalRecord}</span>
                                &nbsp;&nbsp;&nbsp;
                                <label>{window.intl.get('pagination.curPage')} : </label>
                                <span>{this.state.curPage}</span>
                            </div>
                            <table className="report table_style">
                                <thead>
                                <tr>
                                    <th onClick={(event) => {this.checkAll(event)}}>
                                        {window.intl.get('select.all')}<br/><input type="checkbox" name="chkAll" id="chkAll" />
                                    </th>
                                    <th>{window.intl.get('voucher.package')}</th>
                                    <th>{window.intl.get('voucher.code')}</th>
                                    <th>{window.intl.get('other.status')}</th>
                                    <th>{window.intl.get('date.created')}</th>
                                </tr>
                                </thead>

                                <tbody>
                                {
                                    this.state.content && this.state.content[this.props.local] &&
                                    this.state.content[this.props.local].map((item, key) => {
                                        return (
                                            <tr key={key}>
                                                <td onClick={(event) => {this.checkOne(event, item.id)}}>
                                                    <input type="checkbox" name={`chkVoucher${item.id}`} id={`chkVoucher${item.id}`} value={item.id}/>
                                                </td>
                                                <td>{item.package_display}</td>
                                                <td>{item.code}</td>
                                                <td>{item.status_display}</td>
                                                <td>{item.created_date}</td>
                                            </tr>
                                        )
                                    })
                                }
                                </tbody>
                            </table>

                        </div>
                        <Pagination totalRecord={this.state.totalRecord} totalPage={this.state.totalPage} curPage={this.state.curPage} switchPage={this.getVoucherList}/>

                        <Modal className="userpage"
                               visible={this.state.visible}
                               onCancel={this.updateCancel}
                        >
                            <div className='pop-msg'>
                                <h1 className="title_color">{this.state.updateAction === 'activate' ? window.intl.get('action.activate') : window.intl.get('action.ban')}</h1>
                                <span className="errorMsg">{this.state.errorMsg}</span>
                                <p className="content_text">{this.state.updateAction === 'activate' ? window.intl.get('voucher.activateMsg') : window.intl.get('voucher.banMsg')}?</p>

                                <button className="yes_button" onClick={this.updateVoucher}>{window.intl.get('button.yes')}</button>
                                <button className="no_button" onClick={this.updateCancel}>{window.intl.get('button.no')}</button>
                            </div>
                        </Modal>
                    </div>
                    {/*content over here*/}
                </section>
            </article>
        )
    }
}

function mapProps(state){
    return {
        local:state.local
    }
}

export default connect(mapProps)(Voucherlist)