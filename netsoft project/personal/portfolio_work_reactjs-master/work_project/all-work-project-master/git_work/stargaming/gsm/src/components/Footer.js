import React , { Component } from 'react';
import moment from 'moment';

class Footer extends  Component{
            render(){
                return (
                    <footer className="footer Aligner">
                        © {moment().format('YYYY')} by SMS Message
                    </footer>
                )
            }
        }

export default Footer;