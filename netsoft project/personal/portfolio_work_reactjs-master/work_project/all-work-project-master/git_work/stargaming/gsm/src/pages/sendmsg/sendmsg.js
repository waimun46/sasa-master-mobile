import React , { Component } from 'react';
import { Select, Input } from 'antd';
import _fetch from "../../fetchApi";
import {connect} from "react-redux";
import sessionExpired from "../../components/SessionExpired";

const Option = Select.Option;
const { TextArea } = Input;

class Sendmsg extends  Component{

    state = {
        user: '',
        phone: '',
        message: '',
        submitLoad: false,
        errorMsg: '',

        mapUser: [],
        notice: [],
        initDone: false
    }

    componentDidMount = () => {
        this.init()
    }

    init = () => {

        _fetch({
            api: 'Client/Send',
            data: {
                action: 'init'
            }
        }).then(res => res.json()).then((data) => {
            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {

                }

                sessionExpired(data)
            } else {
                this.setState({
                    mapUser: data.data.user,
                    user: data.data.user[0] ? data.data.user[0].id : '',
                    notice: data.data.notice,
                    initDone: true
                });
            }
        })
    }

    handleUser = (user) => {
        this.setState({
            user: user
        })
    }

    handleChangeField = (field, value) => {
        this.setState({
            [field]: value
        })
    };

    handleUser = (value) =>{
        this.setState({
            user: value
        })
    }

    create = () => {
        if (this.state.submitLoad) {
            return
        }
        if (this.state.phone.trim().length === 0) {
            this.setState({
                errorMsg: window.intl.get('error.phoneEmpty'),
            });
            return
        }
        if (this.state.message.trim().length === 0) {
            this.setState({
                errorMsg: window.intl.get('error.messageEmpty'),
            });
            return
        }
        this.setState({
            submitLoad: true,
        });
        _fetch({
            api: 'Client/Send',
            data: {
                action: 'submit',
                phone: this.state.phone,
                user: this.state.user,
                message: this.state.message

            }
        }).then(res => res.json()).then((data) => {
            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {

                }
                sessionExpired(data)
                if (data.msg) {
                    this.setState({
                        errorMsg: data.msg[this.props.local],
                    });
                }
                this.setState({
                    submitLoad: false,
                });
            } else {
                this.props.history.push('/main')
            }
        })
    }

    render(){
        return (
            <article className="main_content">
                <header>
                    <h2>{window.intl.get('sms.singleSMS')}</h2>
                </header>
                {
                    this.state.initDone &&
                    <section className="page_content">
                        {/*content here*/}
                        <div className="half-page">
                            <span>{this.state.notice[this.props.local]}</span><br/>
                            <span className="errorMsg">{this.state.errorMsg}</span>
                            <div className=" select_margin">
                                <label className="label_space">{window.intl.get('user.user')} :</label>
                                <Select value={this.state.user} onChange={this.handleUser}>
                                    {
                                        this.state.mapUser && this.state.mapUser.map((item, index) => {
                                            return (
                                                <Option value={item.id} key={index}>{item.name + ' ' + window.intl.get('credit.credit') + ': ' + item.credit}</Option>
                                            )
                                        })
                                    }
                                </Select>
                            </div>

                            <div className="select_margin">
                                <label className="label_space">{window.intl.get('sms.phone')} :</label>
                                <Input className="select_style" onInput={(event) => {
                                    this.handleChangeField('phone', event.target.value.trim())
                                }}/>
                            </div>

                            <div className="select_margin">
                                <label className="label_space">{window.intl.get('sms.content')} :</label>
                                <TextArea className="select_style" rows={4} onInput={(event) => {
                                    this.handleChangeField('message', event.target.value.trim())
                                }}/>
                            </div>

                            <div className="button-align-setting">
                                <span className="sendmsg_button submitBtn" onClick={this.create}>{window.intl.get('button.submit')}</span>
                            </div>
                        </div>
                        {/*content here*/}
                    </section>
                }
            </article>
        )
    }
}

function mapProps(state){
    return {
        local:state.local
    }
}

export default connect(mapProps)(Sendmsg);