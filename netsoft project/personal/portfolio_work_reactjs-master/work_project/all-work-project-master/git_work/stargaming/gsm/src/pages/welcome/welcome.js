import React, {Component} from 'react';


class Welcome extends Component {
    render() {
        return (
            <article className="main_content">
                <header>
                    <h2>{window.intl.get('welcome.welcome')}</h2>
                </header>
                <section className="page_content">
                    <div id="Welcome">
                        <h3>{window.intl.get('welcome.welcometosms')}.</h3>
                    </div>
                </section>
            </article>
        );
    }
}

export default Welcome; 
