import {combineReducers} from 'redux';
import {
    ADD_TODO,
    COMPLETE_TODO,
    SET_VISIBILITY_FILTER,
    VisibilityFilters
} from "./action";

const {SHOW_ALL} = VisibilityFilters

function visibilityFilter(state = SHOW_ALL,action){
    switch (action.type){
        case SET_VISIBILITY_FILTER:
            return action.filter;
        default:
            return state
    }
}

function todos(state = [] , action){
    switch (action.type) {
        case ADD_TODO:
            return [
                ...state,
                {
                    text: action.text,
                    completed: false,
                }
            ];
        case COMPLETE_TODO:
            return [
                ...state.slice(0,action.index),
                Object.assign( {} , state[action.index] ,{
                    completed:true
                }),
                ...state.slice(action.index+1)
            ];
        default:
            return state;
    }
}


function userInfo (state={username:'aa',balance:10},action){
    switch (action.type){
        case 'saveUserInfo':
            return action.userinfo;
        case 'changeUsername':
            return Object.assign({},state,{username:action.username});
        default:
            return state;
    }
}

function changeLanguage (state={language:'english'},action){
    switch (action.type){
        case 'changeLanguage':
            return action.language;
        default:
            return state;
    }
}

function handleLocal(state = localStorage.getItem('currentLocale') || 'english', action) {
    switch (action.type) {
        case 'handleLocal':
            localStorage.setItem('currentLocale', action.local);
            return action.local;
        default:
            return state
    }
}

function handleLogin(state = localStorage.getItem('isLogin') || false, action) {
    switch (action.type) {
        case 'handleLogin':
            localStorage.setItem('isLogin', action.isLogin);
            return action.isLogin;
        default:
            return state
    }
}

function saveClient(state = localStorage.getItem('client') || '', action) {
    switch (action.type) {
        case 'saveClient':
            localStorage.setItem('client', action.client);
            return action.client;
        default:
            return state
    }
}

function refreshPage(state = false, action) {
    switch (action.type) {
        case 'refreshPage':
            return action.bool;
        default:
            return state
    }
}

function handleForwardMsg(state = 0, action) {
    switch (action.type) {
        case 'handleForwardMsg':
            return action.campaign;
        default:
            return state
    }
}

function allowMenu(state = localStorage.getItem('menu') || [], action) {
    switch (action.type) {
        case 'allowMenu':
            localStorage.setItem('menu', action.menu);
            return action.menu;
        default:
            return state
    }
}



//总reducers
const todoApp = combineReducers({
    visibilityFilter,
    local:handleLocal,
    todos,
    user:userInfo,
    changeLanguage,
    isLogin:handleLogin,
    refreshPage,
    client:saveClient,
    campaign:handleForwardMsg,
    menu:allowMenu
});

export default todoApp

