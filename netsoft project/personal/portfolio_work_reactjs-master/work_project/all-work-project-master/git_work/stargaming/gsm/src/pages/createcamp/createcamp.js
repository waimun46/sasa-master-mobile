import React, {Component} from 'react';
import {Icon, Input, Checkbox, Row, Col} from 'antd';
import {Select} from "antd/lib/index";
import _fetch from "../../fetchApi";
import sessionExpired from "../../components/SessionExpired";
import {connect} from "react-redux";
import {handleForwardMsg} from "../../redux/action";

const Option = Select.Option;
const {TextArea} = Input;

class Createcamp extends Component {

    state = {
        name: '',
        group: [],
        user: '',
        message: '',
        submitLoad: false,
        errorMsg: '',
        mapGroup: [],
        mapUser: [],
        initDone: false,
        notice: []


    };

    componentDidMount = () => {
        this.init();


    };



    init = () => {

        _fetch({
            api: 'Client/Campaign',
            data: {
                action: 'init',
                campaign: this.props.campaign
            }
        }).then(res => res.json()).then((data) => {
            this.props.handleForwardMsg(0);
            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {

                }

                sessionExpired(data)
            } else {
                this.setState({
                    mapGroup: data.data.group,
                    mapUser: data.data.user,
                    user: data.data.user[0] ? data.data.user[0].id : '',
                    notice: data.data.notice,
                    message: data.data.default_campaign_message,
                    name: data.data.default_campaign_name,
                    initDone: true
                });

                if (data.data.default_campaign_message.length > 0) {
                    document.getElementById('campaignMessage').value = data.data.default_campaign_message;
                }

                if (data.data.default_campaign_name.length > 0) {
                    document.getElementById('campaignName').value = data.data.default_campaign_name;
                }
            }
        })
    };



    getCursorPosition = (el) => {
        let pos = 0;
        if('selectionStart' in el) {
            pos = el.selectionStart;
        } else if('selection' in document) {
            el.focus();
            let Sel = document.selection.createRange();
            let SelLength = document.selection.createRange().text.length;
            Sel.moveStart('character', -el.value.length);
            pos = Sel.text.length - SelLength;
        }
        return pos;
    };

    setSelectionRange = (input, selectionStart, selectionEnd) => {
        if (input.setSelectionRange) {
            input.focus();
            input.setSelectionRange(selectionStart, selectionEnd);
        }
        else if (input.createTextRange) {
            var range = input.createTextRange();
            range.collapse(true);
            range.moveEnd('character', selectionEnd);
            range.moveStart('character', selectionStart);
            range.select();
        }
    };

    setCaretToPos = (input, pos) => {
        this.setSelectionRange(input, pos, pos);
    };


    insertTag = (tag) => {
        let message = document.getElementById('campaignMessage');
        let content = message.value;
        let position = this.getCursorPosition(message);
        let offset = 1;

        document.getElementById('campaignMessage').value = [content.slice(0, position), '[##', tag, '##]', content.slice(position)].join('');
        this.setCaretToPos(document.getElementById("campaignMessage"), position + 6 + tag.length );

    };

    handleUser = (user) => {
        this.setState({
            user: user
        })
    };

    handleChangeField = (field, value) => {
        this.setState({
            [field]: value
        })
    };

    handleUser = (value) => {
        this.setState({
            user: value
        })
    };

    handleGroup = (event) => {
        let target = event.target;
        let group = this.state.group;
        let value = target.value;
        let index = group.indexOf(value);

        if (target.checked) {
            if (index === -1) {
                group.push(value);
                this.setState({
                    group: group
                })
            }
        } else {
            if (index !== -1) {
                group.splice(index, 1);
                this.setState({
                    group: group
                })
            }
        }
    };

    create = () => {
        if (this.state.submitLoad) {
            return
        }
        if (this.state.name.trim().length === 0) {
            this.setState({
                errorMsg: window.intl.get('error.campaignNameEmpty'),
            });
            return
        }
        if (this.state.group.length === 0) {
            this.setState({
                errorMsg: window.intl.get('error.groupEmpty'),
            });
            return
        }
        if (this.state.message.trim().length === 0) {
            this.setState({
                errorMsg: window.intl.get('error.messageEmpty'),
            });
            return
        }
        this.setState({
            submitLoad: true,
        });
        _fetch({
            api: 'Client/Campaign',
            data: {
                action: 'create',
                name: this.state.name,
                user: this.state.user,
                message: this.state.message,
                group: JSON.stringify(this.state.group)
            }

        }).then(res => res.json()).then((data) => {

            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {

                }
                sessionExpired(data);
                if (data.msg) {
                    this.setState({
                        errorMsg: data.msg[this.props.local],
                    });
                }
                this.setState({
                    submitLoad: false,
                });
            } else {
                this.props.history.push('/Campaigns')
            }
        })
    };

    back = () => {
        this.props.history.push('/Campaigns')
    }

    render() {

        return (
            <article id="createcamp_tag" className="main_content">

                <header>
                    <h2>{window.intl.get('campaign.create')}</h2>
                </header>

                {
                    this.state.initDone &&
                    <section className="page_content">
                        <div className="title-btn">
                            <span className="backBtn" onClick={this.back}>{window.intl.get('other.back')}</span>
                        </div>

                        {/*content here*/}
                        <div className="half-page">
                            <span>{this.state.notice[this.props.local]}</span><br/>
                            <span className="errorMsg">{this.state.errorMsg}</span>
                            <div className="select_margin">
                                <label className="label_space">{window.intl.get('user.user')} :</label>
                                <Select value={this.state.user} onChange={this.handleUser}>
                                    {
                                        this.state.mapUser && this.state.mapUser.map((item, index) => {
                                            return (
                                                <Option value={item.id}
                                                        key={index}>{item.name + ' (' + window.intl.get('credit.credit') + ': ' + item.credit + ') '}</Option>
                                            )
                                        })
                                    }
                                </Select>
                            </div>

                            <div className="select_margin">
                                <label className="label_space">{window.intl.get('campaign.campaignName')} :</label>
                                <Input className="select_style" id="campaignName" onInput={(event) => {
                                    this.handleChangeField('name', event.target.value.trim())
                                }}/>
                            </div>

                            <div className="select_margin">
                                <label className="label_space">{window.intl.get('sms.content')} :</label>
                                <TextArea   className="select_style" id="campaignMessage" rows={4} onInput={(event) => {
                                    this.handleChangeField('message', event.target.value.trim())
                                }}/>

                            </div>

                            <div className="select_margin">
                                <div className="tag_sty">
                                    <label className="label_space">{window.intl.get('campaign.inserttags')} :</label>
                                    <div id="step3-tags" className="select_style">
                                        <div className="li_sty">
                                            <button onClick={() => this.insertTag('mobile')}>[##mobile##]<Icon type="tags" /></button>
                                        </div>
                                        <div className="li_sty">
                                            <button onClick={() => this.insertTag('firstname')}>[##firstname##]<Icon type="tags"  /></button>
                                        </div>
                                        <div className="li_sty">
                                            <button onClick={() => this.insertTag('lastname')}>[##lastname##]<Icon type="tags" /></button>
                                        </div>
                                        <div className="li_sty">
                                            <button onClick={() => this.insertTag('gender')}>[##gender##]<Icon type="tags"  /></button>
                                        </div>
                                        <div className="li_sty">
                                            <button onClick={() => this.insertTag('birthdate')}>[##birthdate##]<Icon type="tags"  /></button>
                                        </div>
                                        <div className="li_sty">
                                            <button onClick={() => this.insertTag('state')}>[##state##]<Icon type="tags" /></button>
                                        </div>

                                    </div> 
                                </div>
                            </div>


                            <div className="select_margin group">
                                <label className="label_space">{window.intl.get('campaign.group')} :</label>
                                <div className="cat-group">
                                    <Row>
                                        {
                                            this.state.mapGroup && this.state.mapGroup.map((item, index) => {
                                                return (
                                                    <Col key={index} id="camp-option" span={6}>
                                                        <Checkbox value={item.id.toString()} onChange={(event) => {
                                                            this.handleGroup(event)
                                                        }}>{item.name}</Checkbox>
                                                    </Col>
                                                )
                                            })
                                        }
                                    </Row>
                                </div>
                            </div>

                            <div className="row button-align-setting">
                                <span className="sendmsg_button submitBtn"
                                      onClick={this.create}>{window.intl.get('button.submit')}</span>
                            </div>
                        </div>


                        {/*content here*/}
                    </section>
                }


            </article>
        )
    }
}

function mapProps(state) {
    return {
        local: state.local,
        campaign: state.campaign
    }
}

function mapAction(dispatch) {
    return {
        handleForwardMsg: (campaign) => {
            dispatch(handleForwardMsg(campaign))
        }
    }
}

export default connect(mapProps, mapAction)(Createcamp);