import React, {Component} from 'react';
import {Input} from 'antd';
import _fetch from '../../fetchApi';
import sessionExpired from '../../components/SessionExpired';
import {Select} from "antd/lib/index";
import {connect} from "react-redux";

const Option = Select.Option;

class Addvoucher extends Component {

    state = {
        initDone: false,

    };

    componentDidMount = () => {
        this.init()
    };

    init = () => {
        _fetch({
            api: 'Client/Voucher',
            data: {
                action: 'init',
                action_init: 'create',
            }
        }).then(res => res.json()).then((data) => {
            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {

                }

                sessionExpired(data)
            } else {

                this.setState({
                    voucherPackage: data.data.voucherPackage,
                    voucherStatus: data.data.voucherStatus,
                    status: 1,
                    package: data.data.voucherPackage[0]['id'],
                });
            }
        })
    };

    create = () => {
        _fetch({
            api: 'Client/Voucher',
            data: {
                action: 'create',
                status : this.state.status,
                package_id : this.state.package,
                quantity : this.state.quantity
            }
        }).then(res => res.json()).then((data) => {
            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {

                }

                sessionExpired(data);
                if (data.msg) {
                    this.setState({
                        errorMsg: data.msg[this.props.local],
                    });
                }
            } else {
                this.props.history.push('/Voucherlist')
            }
        })
    }

    handleChangeField = (field, value) => {
        this.setState({
            [field]: value
        })
    };

    handleStatus = (value) => {
        this.setState({
            status: value
        })
    }

    handlePackage = (value) => {
        this.setState({
            package: value
        })
    }

    back = () => {
        this.props.history.push('/Voucherlist')
    }

    render() {
        return (
            <article className="main_content">
                <header>
                    <h2>{window.intl.get('voucher.addVoucher')}</h2>
                </header>
                <section className="page_content">
                    {/*content here*/}
                    <div className="title-btn">
                        <span className="backBtn" onClick={this.back}>{window.intl.get('other.back')}</span>
                    </div>
                    <div className="half-page">
                        <span className="errorMsg">{this.state.errorMsg}</span>

                        <div className="select_margin">
                            <label className="label_space">{window.intl.get('voucher.quantity')}:</label>
                            <Input className="select_style" type="number" onInput={(event) => {
                                this.handleChangeField('quantity', event.target.value.trim())
                            }}/>
                        </div>

                        <div className="select_margin">
                            <label className="label_space">{window.intl.get('voucher.package')} :</label>
                            <Select className="formSelect" value={this.state.package} onChange={this.handlePackage}>
                                {
                                    this.state.voucherPackage && this.state.voucherPackage.map((item, index) => {
                                        return (
                                            <Option value={item.id} key={index}>{item.name}</Option>
                                        )
                                    })
                                }
                            </Select>
                        </div>

                        <div className="select_margin">
                            <label className="label_space">{window.intl.get('other.status')} :</label>
                            <Select className="formSelect" value={this.state.status} onChange={this.handleStatus}>
                                {
                                    this.state.voucherStatus && this.state.voucherStatus[this.props.local] && this.state.voucherStatus[this.props.local].map((item, index) => {
                                        return (
                                            <Option value={item.id} key={index}>{item.status}</Option>
                                        )
                                    })
                                }
                            </Select>
                        </div>


                        <div className="button-align-setting">
                            <span className="sendmsg_button submitBtn" onClick={this.create}>{window.intl.get('button.submit')}</span>
                        </div>
                    </div>
                    {/*content here*/}
                </section>
            </article>
        )
    }
}

function mapProps(state) {
    return {
        local: state.local
    }
}

export default connect(mapProps)(Addvoucher)