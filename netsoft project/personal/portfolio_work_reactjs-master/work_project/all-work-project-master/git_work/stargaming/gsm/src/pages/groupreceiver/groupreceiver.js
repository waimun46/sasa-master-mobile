import React , { Component } from 'react';
import {Link} from 'react-router-dom';
import { Modal } from 'antd';

import _fetch from "../../fetchApi";
import sessionExpired from "../../components/SessionExpired";
import {connect} from "react-redux";
import Pagination from '../../components/Pagination/Pagination';
import '../../css/pages.css';

class Groupreceiver extends  Component{

    state = {
        initDone: false,
        errorMsg: '',

        content: [],
        totalRecord: 0,
        totalPage: 0,
        curPage: 1,

        visible_update: false,
        updateId: 0,
        groupName: '',
        file: '',

        visible_delete: false,
        deleteId: 0
    };

    componentDidMount = () => {
        this.getList()
    };

    getList = (page) => {
        _fetch({
            api: 'Client/Contact',
            data: {
                action: 'list',
                page: page || 1
            }
        }).then(res => res.json()).then((data) => {
            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {

                }
                sessionExpired(data)
            } else {
                this.setState({
                    content: data.data.reports,
                    totalRecord: data.data.total_record,
                    totalPage: data.data.total_page,
                    curPage: data.data.cur_page,
                    errorMsg: '',
                    initDone: true
                });
            }
        })
    };

    viewGroup = (group) => {
        localStorage.setItem('group', group);
        this.props.history.push('/Viewgroup')
    };

    showUpdate = (group, name) => {
        this.setState({
            visible_update: true,
            updateId: group,
            groupName: name,
            errorMsg: ''
        });
    };

    updateCancel = () => {
        this.setState({
            visible_update: false,
            updateId: 0,
            groupName: '',
            errorMsg: ''
        });
    };

    handleFileUpload = (upload) => {
        this.setState({
            file: upload.target.files[0]
        })
    };

    handleChangeField = (field, value) => {
        this.setState({
            [field]: value
        })
    };

    updateSubmit = (group) => {

        let form_data = new FormData();
        form_data.append('action', 'update');
        form_data.append('file', this.state.file);
        form_data.append('group', group);
        form_data.append('name', this.state.groupName);

        _fetch({
            api: 'Client/Contact',
            headers: false,
            body:form_data
        }).then(res => res.json()).then((data) => {
            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {

                }
                sessionExpired(data)
                if (data.msg) {
                    this.setState({
                        errorMsg: data.msg[this.props.local],
                    });
                }
            } else {
                this.setState({
                    visible_update: false,
                    updateId: 0,
                    groupName: '',
                    errorMsg: ''
                });

                this.getList()
            }
        })
    };

    deleteConfirm = (group) => {
        this.setState({
            visible_delete: true,
            deleteId: group,
            errorMsg: ''
        });
    };

    deleteCancel = () => {
        this.setState({
            visible_delete: false,
            deleteId: 0,
            errorMsg: ''
        });
    };

    deleteSubmit = (group) => {
        _fetch({
            api: 'Client/Contact',
            data: {
                action: 'remove_group',
                group: group
            }
        }).then(res => res.json()).then((data) => {
            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {

                }
                sessionExpired(data)
            }

            this.setState({
                visible_delete: false,
                deleteId: 0,
                errorMsg: ''
            });

            this.getList()
        })
    };

    render(){
        return (
            <article className="main_content">
                <header>
                    <h2>{window.intl.get('groupReceiver.title')}</h2>
                </header>
                <section className="page_content">
                    {/*content here*/}

                    <div className="title-btn">
                        <Link to="Addgroup"><button className="createuser_btn">{window.intl.get('groupReceiver.add')}</button></Link>
                    </div>

                    {
                        this.state.initDone &&
                        <div>
                            <div id="table_body">
                                <div className='totalRecord'>
                                    <label>{window.intl.get('pagination.totalRecord')} : </label>
                                    <span>{this.state.totalRecord}</span>
                                    &nbsp;&nbsp;&nbsp;
                                    <label>{window.intl.get('pagination.curPage')} : </label>
                                    <span>{this.state.curPage}</span>
                                </div>
                                <table className="table_style report">
                                    <thead>
                                    <tr>
                                        <th>{window.intl.get('table.no')}</th>
                                        <th>{window.intl.get('groupReceiver.groupName')}</th>
                                        <th>{window.intl.get('date.datetime')}</th>
                                        <th>{window.intl.get('groupReceiver.subscribe')}</th>
                                        <th>{window.intl.get('action.title')}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {
                                        this.state.content &&
                                        this.state.content.map((item, key) => {
                                            return (
                                                <tr key={key}>
                                                    <td>{key+1}</td>
                                                    <td>{item.name}</td>
                                                    <td>{item.created_date}</td>
                                                    <td>{item.subscribe}</td>
                                                    <td>
                                                        <button className="view_button" onClick={() => {this.viewGroup(item.id)}}>{window.intl.get('action.view')}</button>
                                                        <button className="pay_button" onClick={() => this.showUpdate(item.id, item.name)}>{window.intl.get('action.update')}</button>
                                                        <button className="del_button" onClick={() => this.deleteConfirm(item.id)}>{window.intl.get('action.delete')}</button>
                                                    </td>
                                                </tr>
                                            )
                                        })
                                    }
                                    </tbody>
                                </table>
                            </div>
                            <Pagination totalRecord={this.state.totalRecord} totalPage={this.state.totalPage} curPage={this.state.curPage} switchPage={this.getList}/>
                        </div>
                    }

                    <Modal className="userpage" visible={this.state.visible_update} onCancel={this.updateCancel}>
                        <div className='pop-msg'>
                            <h1 className="title_color">{window.intl.get('groupReceiver.update')}</h1>
                            <div className="update-msg">
                                <span className="errorMsg">{this.state.errorMsg}</span>
                                <div className="row select_margin">
                                    <label className="col-md-6 col-xs-12 label_space">{window.intl.get('groupReceiver.groupName')}</label>
                                    <span className="col-md-6 col-xs-12 label_space">{this.state.groupName}</span>
                                </div>

                                <div className="row select_margin">
                                    <label className="col-md-4 col-xs-12 label_space">{window.intl.get('groupReceiver.excel')}</label>
                                    <span className="col-md-6 col-xs-12 label_space"><input type="file" onChange={this.handleFileUpload} /></span>
                                </div>
                            </div>
                            <button type="button" className="sendmsg_button" onClick={() => {this.updateSubmit(this.state.updateId)}}>{window.intl.get('button.submit')}</button>
                        </div>
                    </Modal>

                    <Modal className="userpage" visible={this.state.visible_delete} onCancel={this.deleteCancel}>
                        <div className='pop-msg'>
                            <h1 className="title_color">{window.intl.get('groupReceiver.delete')}</h1>
                            <span className="errorMsg">{this.state.errorMsg}</span>
                            <p className="content_text">{window.intl.get('groupReceiver.deleteMsg')}?</p>

                            <button className="yes_button" onClick={() => {this.deleteSubmit(this.state.deleteId)}}>{window.intl.get('button.yes')}</button>
                            <button className="no_button" onClick={() => {this.deleteCancel()}}>{window.intl.get('button.no')}</button>
                        </div>
                    </Modal>

                    {/*content here*/}
                </section>
            </article>
        )
    }
}

function mapProps(state){
    return {
        local:state.local
    }
}

export default connect(mapProps)(Groupreceiver);