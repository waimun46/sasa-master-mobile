import React, {Component} from 'react';
import {Input, DatePicker} from 'antd';
import {Select} from "antd/lib/index";
import _fetch from "../../fetchApi";
import sessionExpired from "../../components/SessionExpired";
import {connect} from "react-redux";
import moment from 'moment';
import {Modal} from 'antd';
import Pagination from '../../components/Pagination/Pagination';
import '../../css/pages.css';

const {TextArea} = Input;
const Option = Select.Option;

class VoucherPackage extends Component {

    state = {
        listingSection: false,
        createPackageSection: true,
        name: '',
        desc: '',
        amount: 0,
        multipleUse: 'Y',
        maxRedeem: "",
        startDate: moment().format("YYYY-MM-DD"),
        endDate: moment().format("YYYY-MM-DD"),
        status: 1,
        statusForFilter: '0',
        initDone: false,
        showViewVoucher: false,
        showUpdateVoucher: false,
        voucherDetail:[],

        voucherPackageId: 0,
        showUpdateVoucherStatus: false,

        content: [],
        totalRecord: 0,
        totalPage: 0,
        curPage: 1
    }

    componentDidMount = () => {
        this.init()
    };

    init = () => {
        this.setState({
            name: '',
            desc: '',
            amount: '',
            multipleUse: 'Y',
            maxRedeem: '',
            startDate: moment().format("YYYY-MM-DD"),
            endDate: moment().format("YYYY-MM-DD"),
            status: 1,
            errorMsg: ''
        })
    }

    changeSection = (event) => {
        if (event.currentTarget.value === 'listing') {
            this.setState({
                listingSection: true,
                createPackageSection: false,
                statusForFilter: '0'
            });

            this.voucherPackageList(1)
        } else {
            this.setState({
                listingSection: false,
                createPackageSection: true,
            });


        }
        this.init()
    }

    handleChangeField = (field, value) => {
        this.setState({
            [field]: value
        })
    };

    start = (date, dateString) => {
        this.setState({
            startDate: dateString
        })
    }

    end = (date, dateString) => {
        this.setState({
            endDate: dateString
        })
    }

    handleYesNo = (value) => {
        this.setState({
            multipleUse: value
        })

        if (value === 'N') {
            this.setState({
                maxRedeem: ''
            })
        }
    }

    handleStatus = (value) => {
        this.setState({
            status: value
        })
    }

    filterByStatus = (value) => {
        this.setState({
            statusForFilter: value
        })
        this.voucherPackageList('',value)
    }

    voucherPackageList = (page,status) => {
        _fetch({
            api: 'Client/VoucherPackage',
            data: {
                action: 'list',
                page: page || 1,
                status: status || this.state.statusForFilter
            }
        }).then(res => res.json()).then((data) => {
            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {
                }
                sessionExpired(data)
            } else {
                this.setState({
                    content: data.data.reports,
                    totalRecord: data.data.total_record,
                    totalPage: data.data.total_page,
                    curPage: data.data.cur_page,
                });
            }
        })
    };

    create = () => {
        if (this.state.name.trim().length === 0) {
            this.setState({
                errorMsg: window.intl.get('error.packageNameEmpty'),
            });
            return
        }

        let amount = this.state.amount.trim().toString();
        if (amount.charAt(0) === '0' || (amount.charAt(0) === '-' && amount.charAt(1) === '0') || !(/^-?\d+$/.test(amount))) {
            this.setState({
                errorMsg: window.intl.get('error.amountNotANumber'),
            });
            return
        }

        let multiple_use = this.state.multipleUse;
        if(multiple_use === 'Y'){
            let maxRedeem = this.state.maxRedeem.trim().toString();
            if (maxRedeem.charAt(0) === '0' || (maxRedeem.charAt(0) === '-' && maxRedeem.charAt(1) === '0') || !(/^\d+$/.test(maxRedeem))) {
                this.setState({
                    errorMsg: window.intl.get('error.invalidMaxRedeem'),
                });
                return
            }
        }

        _fetch({
            api: 'Client/VoucherPackage',
            data: {
                action: 'create',
                name: this.state.name,
                desc: this.state.desc,
                amount: this.state.amount,
                multiple_use: this.state.multipleUse,
                maxRedeem: this.state.maxRedeem,
                start_date: this.state.startDate,
                end_date: this.state.endDate,
                status: this.state.status,
            }

        }).then(res => res.json()).then((data) => {
            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {

                }
                sessionExpired(data);
                if (data.msg) {
                    this.setState({
                        errorMsg: data.msg[this.props.local],
                    });
                }

            } else {
                this.setState({
                    listingSection: true,
                    createPackageSection: false,
                    initDone: false,
                    errorMsg: ''
                });
                this.voucherPackageList()
            }
        })
    };

    viewVoucher = (id) => {
        _fetch({
            api: 'Client/VoucherPackage',
            data: {
                action: 'detail',
                id: id
            }
        }).then(res => res.json()).then((data) => {
            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {

                }
                sessionExpired(data);
            } else {
                this.setState({
                    voucherDetail: data.data,
                    desc : data.data[this.props.local]['desc'],
                    maxRedeem : data.data[this.props.local]['max_redeem'],
                    startDate : data.data[this.props.local]['start_date'] ,
                    endDate: data.data[this.props.local]['end_date']
                })
            }
        })
    }

    closeViewVoucher = () => {
        this.setState({
            showViewVoucher: false
        })
    }

    showViewVoucher = (id) => {
        this.setState({
            showViewVoucher: true
        })
        this.viewVoucher(id);
    }

    showUpdateForm = (id,name) => {
        this.setState({
            showUpdateVoucher: true,
            voucherPackageName: name,
            voucherId: id,
            errorMsg: ''
        })

        this.viewVoucher(id);
    }

    updateSubmit = (id) => {
        if (this.state.maxRedeem.length === 0) {
            this.setState({
                errorMsg: window.intl.get('error.invalidMaxRedeem'),
            });
            return
        }
        let maxRedeem = this.state.maxRedeem.toString().trim();

        if(this.state.voucherDetail[this.props.local]['multiple_use'] === 'Y') {
            if (maxRedeem.charAt(0) === '0' || (maxRedeem.charAt(0) === '-' && maxRedeem.charAt(1) === '0') || !(/^\d+$/.test(maxRedeem))) {
                this.setState({
                    errorMsg: window.intl.get('error.invalidMaxRedeem'),
                });
                return
            }
        }else{
            maxRedeem = 0;
        }

        _fetch({
            api: 'Client/VoucherPackage',
            data: {
                action: 'update',
                id: id,
                desc: this.state.desc,
                maxRedeem: maxRedeem,
                start_date: this.state.startDate,
                end_date: this.state.endDate,
                status: this.state.status,
            }

        }).then(res => res.json()).then((data) => {
            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {

                }
                sessionExpired(data)
                if (data.msg) {
                    this.setState({
                        errorMsg: data.msg[this.props.local],
                    });
                }
            } else {
                this.setState({
                    showUpdateVoucher: false,
                    errorMsg: ''
                });

                this.voucherPackageList()
            }
        })
    };

    closeUpdateVoucher = () => {
        this.setState({
            showUpdateVoucher: false
        })
    };

    updateVoucher = (action, voucherPackageId) => {
        this.setState({
            showUpdateVoucherStatus: true,
            updateAction: action,
            voucherPackageId: voucherPackageId,
            errorMsg: ''
        });
    };

    updateVoucherCancel = () => {
        this.setState({
            showUpdateVoucherStatus: false,
            voucherPackageId: 0,
            errorMsg: ''
        });
    };


    updateVoucherStatus = () => {
        _fetch({
            api: 'Client/VoucherPackage',
            data: {
                action: this.state.updateAction,
                id: this.state.voucherPackageId
            }
        }).then(res => res.json()).then((data) => {
            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {

                }

                sessionExpired(data);
                if (data.msg) {
                    this.setState({
                        errorMsg: data.msg[this.props.local],
                    });
                }
            } else {

                this.setState({
                    showUpdateVoucherStatus: false,
                    voucherPackageId: 0,
                    errorMsg: ''
                });

                this.voucherPackageList(this.state.curPage)
            }
        })
    };

    render() {
        return (
            <article className="main_content">
                <header>
                    <h2>{window.intl.get('voucher.voucherPackage')}</h2>
                </header>
                <section className="page_content">
                    {/*content here*/}
                    <div id="voucherpackage">
                        <section id="tabs" className="tabs">
                            <input id="tab-1" type="radio" name="radio-set" className="myinput tab-selector-1"
                                   value="createPackage"
                                   onChange={this.changeSection} checked={this.state.createPackageSection}/>
                            <label htmlFor="tab-1"
                                   className="mylabel tab-label-1">{window.intl.get('voucher.createPackage')}</label>
                            <input id="tab-2" type="radio" name="radio-set" className="myinput tab-selector-2"
                                   value="listing" onChange={this.changeSection}
                                   checked={this.state.listingSection}/>
                            <label htmlFor="tab-2"
                                   className="mylabel tab-label-2">{window.intl.get('voucher.packageList')}</label>
                            {
                                <div className="content">
                                    <div className="content-1 items">
                                        {/*content here*/}
                                        <div className="half-page">
                                            <span className="errorMsg">{this.state.errorMsg}</span>
                                            <div className="select_margin">
                                                <label
                                                    className="label_space">{window.intl.get('field.packageName')}:</label>
                                                <Input className="select_style" name="name" value={this.state.name} onInput={(event) => {
                                                    this.handleChangeField('name', event.target.value)
                                                }}/>
                                            </div>

                                            <div className="select_margin">
                                                <label
                                                    className="label_space">{window.intl.get('field.desc')}:</label>
                                                <TextArea className="select_style" name="desc" value={this.state.desc} rows={4}
                                                          onInput={(event) => {
                                                              this.handleChangeField('desc', event.target.value)
                                                          }}/>
                                            </div>

                                            <div className="select_margin">
                                                <label className="label_space">{window.intl.get('field.amount')}
                                                    :</label>
                                                <Input className="select_style" name="amount" value={this.state.amount} onInput={(event) => {
                                                    this.handleChangeField('amount', event.target.value.trim())
                                                }}/>
                                            </div>

                                            <div className="select_margin">
                                                <label
                                                    className="label_space">{window.intl.get('field.multipleUse')}:</label>
                                                <Select className="formSelect" name="multipleuse"
                                                        value={this.state.multipleUse}
                                                        onChange={this.handleYesNo}>
                                                    <Option value='Y'>{window.intl.get('other.yes')}</Option>
                                                    <Option value='N'>{window.intl.get('other.no')}</Option>
                                                </Select>
                                            </div>

                                            <div className="select_margin">
                                                <label
                                                    className="label_space">{window.intl.get('field.maxRedeem')}:</label>
                                                <Input className="select_style" onInput={(event) => {
                                                    this.handleChangeField('maxRedeem', event.target.value)
                                                }} value={this.state.maxRedeem} disabled={this.state.multipleUse !== 'Y'}/>
                                            </div>

                                            <div className="select_margin">
                                                <label
                                                    className="label_space">{window.intl.get('date.from')}</label>
                                                <DatePicker
                                                    value={this.state.startDate ? moment(new Date(this.state.startDate), "YYYY-MM-DD") : ''}
                                                    onChange={this.start}/>&nbsp;&nbsp;
                                                {window.intl.get('date.to')}&nbsp;&nbsp;
                                                <DatePicker
                                                    value={this.state.endDate ? moment(new Date(this.state.endDate), "YYYY-MM-DD") : ''}
                                                    onChange={this.end}/>&nbsp;&nbsp;
                                            </div>
                                            <div className="select_margin">
                                                <label className="label_space">{window.intl.get('other.status')}
                                                    :</label>
                                                <Select className="formSelect" value={this.state.status}
                                                        onChange={this.handleStatus}>
                                                    <Option value={1}>{window.intl.get('status.active')}</Option>
                                                    <Option value={9}>{window.intl.get('status.inactive')}</Option>
                                                </Select>
                                            </div>

                                            <div className="button-align-setting">
                                                <span className="sendmsg_button submitBtn"
                                                      onClick={this.create}>{window.intl.get('button.submit')}</span>
                                            </div>
                                        </div>
                                        {/*content here*/}
                                    </div>

                                    <div className="content-2 items">

                                        {/*content over here*/}
                                        <div id="table_body">
                                            <div className="row select_margin">
                                                <label className="">{window.intl.get('other.status')}:</label>
                                                <Select className="formSelect user-select" value={this.state.statusForFilter}
                                                        onChange={this.filterByStatus}>
                                                    <Option value='0'>{window.intl.get('select.all')}</Option>
                                                    <Option value='1'>{window.intl.get('status.active')}</Option>
                                                    <Option value='9'>{window.intl.get('status.inactive')}</Option>
                                                </Select>
                                            </div>
                                            <div className='totalRecord'>
                                                <label>{window.intl.get('pagination.totalRecord')} : </label>
                                                <span>{this.state.totalRecord}</span>
                                                &nbsp;&nbsp;&nbsp;
                                                <label>{window.intl.get('pagination.curPage')} : </label>
                                                <span>{this.state.curPage}</span>
                                            </div>
                                            <table className="report table_style">
                                                <thead>
                                                <tr>
                                                    <th>{window.intl.get('field.packageName')}</th>
                                                    <th>{window.intl.get('field.amount')}</th>
                                                    <th>{window.intl.get('other.status')}</th>
                                                    <th>{window.intl.get('voucher.voucherStatus')}</th>
                                                    <th>{window.intl.get('action.title')}</th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                {
                                                    this.state.content && this.state.content[this.props.local] &&
                                                    this.state.content[this.props.local].map((item, key) => {
                                                        return (
                                                            <tr key={key}>
                                                                <td>{item.name}</td>
                                                                <td>{item.amt}</td>
                                                                <td>{item.status_display}</td>
                                                                <td>
                                                                    {
                                                                        item.voucher_status.map((i, j) => {
                                                                            return (
                                                                                <div key={j}>{i.key}&nbsp;:&nbsp;{i.value}<br/></div>
                                                                            )
                                                                        })
                                                                    }
                                                                </td>
                                                                <td>
                                                                    <span className="inv_button" onClick={() => this.showViewVoucher(item.id)}>{window.intl.get('action.view')}</span>
                                                                    <span className="pay_button" onClick={() => this.showUpdateForm(item.id,item.name)}>{window.intl.get('action.update')}</span>
                                                                    {
                                                                        item.total_voucher ?
                                                                            <span className="inv_button"
                                                                                  onClick={() => window.open(item.download_voucher)}>{window.intl.get('action.download')}</span> : ''
                                                                    }

                                                                    {
                                                                        item.total_voucher ?
                                                                            <div>
                                                                                <br/>
                                                                                {
                                                                                    item.activate_voucher ? <span className="activate_button" onClick={() => this.updateVoucher('activate', item.id)}>{window.intl.get('action.activate')}&nbsp;{window.intl.get('voucher.voucher')}</span> : ''
                                                                                }
                                                                                {
                                                                                    item.ban_voucher ? <span className="del_button" onClick={() => this.updateVoucher('ban', item.id)}>{window.intl.get('action.ban')}&nbsp;{window.intl.get('voucher.voucher')}</span> : ''
                                                                                }
                                                                            </div>
                                                                            : ''
                                                                    }
                                                                </td>
                                                            </tr>
                                                        )
                                                    })
                                                }
                                                </tbody>
                                            </table>
                                        </div>
                                        <Pagination totalRecord={this.state.totalRecord} totalPage={this.state.totalPage} curPage={this.state.curPage} switchPage={this.voucherPackageList}/>

                                        <Modal className="userpage"
                                               visible={this.state.showViewVoucher}
                                               onCancel={this.closeViewVoucher}
                                        >
                                        {
                                            this.state.voucherDetail && this.state.voucherDetail[this.props.local] &&
                                            <div className='pop-msg'>
                                            <h1 className="title_color">{window.intl.get('voucher.detail')}</h1>
                                                <div>
                                                    <div className="row select_margin">
                                                        <label className="col-md-6 col-xs-12 label_space">{window.intl.get('voucher.package')}:</label>
                                                        <span className="col-md-6 col-xs-12 label_space">{this.state.voucherDetail[this.props.local]['name']}</span>
                                                    </div>
                                                    <div className="row select_margin">
                                                        <label className="col-md-6 col-xs-12 label_space">{window.intl.get('field.desc')}:</label>
                                                        <span className="col-md-6 col-xs-12 label_space">{this.state.voucherDetail[this.props.local]['desc'].length === 0 ? '-' : this.state.voucherDetail[this.props.local]['desc']}</span>
                                                    </div>
                                                    <div className="row select_margin">
                                                        <label className="col-md-6 col-xs-12 label_space">{window.intl.get('field.startDate')}:</label>
                                                        <span className="col-md-6 col-xs-12 label_space">{this.state.voucherDetail[this.props.local]['start_date']}</span>
                                                    </div>
                                                    <div className="row select_margin">
                                                        <label className="col-md-6 col-xs-12 label_space">{window.intl.get('field.endDate')}:</label>
                                                        <span className="col-md-6 col-xs-12 label_space">{this.state.voucherDetail[this.props.local]['end_date']}</span>
                                                    </div>
                                                    <div className="row select_margin">
                                                        <label className="col-md-6 col-xs-12 label_space">{window.intl.get('field.multipleUse')}:</label>
                                                        <span className="col-md-6 col-xs-12 label_space">{this.state.voucherDetail[this.props.local]['multiple_use_option']}</span>
                                                    </div>
                                                    <div className="row select_margin">
                                                        <label className="col-md-6 col-xs-12 label_space">{window.intl.get('field.maxRedeem')}:</label>
                                                        <span className="col-md-6 col-xs-12 label_space">{this.state.voucherDetail[this.props.local]['max_redeem']}</span>
                                                    </div>
                                                    <div className="row select_margin">
                                                        <label className="col-md-6 col-xs-12 label_space">{window.intl.get('field.redeemCount')}:</label>
                                                        <span className="col-md-6 col-xs-12 label_space">{this.state.voucherDetail[this.props.local]['redeem_count']}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        }
                                        </Modal>
                                        <Modal className="userpage"
                                               visible={this.state.showUpdateVoucher}
                                               onCancel={this.closeUpdateVoucher}
                                        >
                                        {
                                            this.state.voucherDetail && this.state.voucherDetail[this.props.local] &&
                                            <div className='pop-msg'>
                                                <h1 className="title_color">{window.intl.get('voucher.updateVoucherPackage')}</h1>
                                                <span className="errorMsg">{this.state.errorMsg}</span>
                                                <div className="row select_margin">
                                                    <label
                                                        className="col-md-4 col-xs-12 label_space">{window.intl.get('field.startDate')}:</label>
                                                    <DatePicker className="select_style"
                                                                value={(this.state.startDate === '0000-00-00 00:00:00' || !this.state.startDate) ? "" :moment(new Date(this.state.startDate), "YYYY-MM-DD")}
                                                                onChange={this.start}/>&nbsp;&nbsp;
                                                </div>
                                                <div className="row select_margin">
                                                    <label
                                                        className="col-md-4 col-xs-12 label_space">{window.intl.get('field.endDate')}:</label>
                                                    <DatePicker className="select_style"
                                                                value={(this.state.endDate === '0000-00-00 00:00:00' || !this.state.endDate) ? "" : moment(new Date(this.state.endDate), "YYYY-MM-DD")}
                                                                onChange={this.end}/>&nbsp;&nbsp;
                                                </div>
                                                <div className="row select_margin">
                                                    <label
                                                        className="col-md-4 col-xs-12 label_space">{window.intl.get('other.status')}:</label>
                                                    <Select className="select_style" value={this.state.status}
                                                            onChange={this.handleStatus}>
                                                        <Option value={1}>{window.intl.get('status.active')}</Option>
                                                        <Option value={9}>{window.intl.get('status.inactive')}</Option>
                                                    </Select>
                                                </div>
                                                {
                                                    this.state.voucherDetail[this.props.local]['multiple_use'] ==='Y' &&
                                                    <div className="row select_margin">
                                                        <label
                                                            className="col-md-4 col-xs-12 label_space">{window.intl.get('field.maxRedeem')}:</label>
                                                        <input className="select_style" defaultValue={this.state.maxRedeem} onInput={(event) => {
                                                            this.handleChangeField('maxRedeem', event.target.value)
                                                        }}/>
                                                    </div>
                                                }


                                                <div className="row select_margin">
                                                    <label
                                                        className="col-md-4 col-xs-12 label_space">{window.intl.get('field.desc')}:</label>
                                                    <TextArea rows={4} value={this.state.desc}
                                                              onInput={(event) => {
                                                                  this.handleChangeField('desc', event.target.value)
                                                              }}/>
                                                </div>

                                                <button type="button" className="sendmsg_button" onClick={() => {
                                                    this.updateSubmit(this.state.voucherId)
                                                }}>{window.intl.get('button.submit')}</button>
                                            </div>
                                        }
                                        </Modal>
                                        <Modal className="userpage"
                                               visible={this.state.showUpdateVoucherStatus}
                                               onCancel={this.updateVoucherCancel}
                                        >
                                            <div className='pop-msg'>
                                                <h1 className="title_color">{this.state.updateAction === 'activate' ? window.intl.get('action.activate') : window.intl.get('action.ban')}</h1>
                                                <span className="errorMsg">{this.state.errorMsg}</span>
                                                <p className="content_text">{this.state.updateAction === 'activate' ? window.intl.get('voucher.activatePackageMsg') : window.intl.get('voucher.banPackageMsg')}?</p>

                                                <button className="yes_button" onClick={this.updateVoucherStatus}>{window.intl.get('button.yes')}</button>
                                                <button className="no_button" onClick={this.updateVoucherCancel}>{window.intl.get('button.no')}</button>
                                            </div>
                                        </Modal>
                                    </div>
                                </div>
                                }
                        </section>
                    </div>
                    {/*content here*/}
                </section>
            </article>
        )
    }
}

function mapProps(state) {
    return {
        local: state.local
    }
}

export default connect(mapProps)(VoucherPackage)