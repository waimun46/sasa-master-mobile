import React , { Component } from 'react';
import { Input } from 'antd';
import {connect} from "react-redux";
import _fetch from "../../fetchApi";
import sessionExpired from "../../components/SessionExpired";


class Createuser extends  Component{

    state = {
        groupName: '',
        file: '',
        errorMsg: '',

        notice: [],
        initDone: false
    };

    componentDidMount = () => {
        this.init()
    }

    init = () => {
        _fetch({
            api: 'Client/Contact',
            data: {
                action: 'init'
            }
        }).then(res => res.json()).then((data) => {
            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {

                }
                sessionExpired(data)
            } else {
                this.setState({
                    notice: data.data.notice,
                    initDone: true
                });
            }
        })
    }

    handleFileUpload = (upload) => {
        this.setState({
            file: upload.target.files[0]
        })
    };

    handleChangeField = (field, value) => {
        this.setState({
            [field]: value
        })
    };

    submit = () => {

        let form_data = new FormData();
        form_data.append('action', 'add');
        form_data.append('file', this.state.file);
        form_data.append('name', this.state.groupName);

        _fetch({
            api: 'Client/Contact',
            headers: false,
            body:form_data
        }).then(res => res.json()).then((data) => {
            console.log(data)
            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {

                }
                sessionExpired(data);
                if (data.msg) {
                    this.setState({
                        errorMsg: data.msg[this.props.local],
                    });
                }
            } else {
                this.props.history.push('/Groupreceiver')
            }
        })
    }

    back = () => {
        this.props.history.push('/Groupreceiver')
    }

    render(){
        return (
            <article className="main_content">
                <header>
                    <h2>{window.intl.get('groupReceiver.add')}</h2>
                </header>
                {
                    this.state.initDone &&
                    <section className="page_content">

                        <div className="title-btn">
                            <span className="backBtn" onClick={this.back}>{window.intl.get('other.back')}</span>
                        </div>

                        {/*content here*/}
                        <div className="half-page">
                            <p><span>{this.state.notice[this.props.local]}</span></p>
                            <p><span>{window.intl.get('sms.phoneFormat')}</span></p>
                            <p><span className="errorMsg">{this.state.errorMsg}</span></p>
                            <div className="select_margin">
                                <label className="label_space">{window.intl.get('groupReceiver.groupName')}</label>
                                <Input className="select_style" onInput={(event) => {this.handleChangeField('groupName', event.target.value.trim())}}/>
                            </div>

                            <div className="select_margin">
                                <label className="label_space">{window.intl.get('groupReceiver.excel')}</label>
                                <input type="file" onChange={this.handleFileUpload} />
                            </div>
                            <div className="button-align-setting">
                                <button type="button" className="sendmsg_button" onClick={() => {this.submit()}}>{window.intl.get('button.submit')}</button>
                            </div>
                        </div>
                        {/*content here*/}
                    </section>
                }
            </article>
        )
    }
}

function mapProps(state){
    return {
        local:state.local
    }
}

export default connect(mapProps)(Createuser);