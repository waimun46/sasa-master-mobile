import React, { Component } from 'react';
import Routes from './route';
import {connect} from 'react-redux';

//引入页面

import "./App.css";
import './css/bootstrap.css';
import './css/font-awesome.min.css';
import './css/pagehome_style.css';
import './css/bootstrap-dropdownhover.css';


import locales from './i18n/i18n'
import {handleLocal} from "./redux/action";

const intl = require('react-intl-universal');

class App extends Component {

    state = {
        initDone: false
    };

    componentDidMount = () => {
        this.loadLocal(this.props.local || 'english');
    };

    componentWillReceiveProps = (nextprops) => {
        if (this.props.local !== nextprops.local) {

            this.setState({
                initDone: false
            })

            this.loadLocal(nextprops.local);
        }
    };

    loadLocal = (local) => {
        // init method will load CLDR locale data according to currentLocale
        // react-intl-universal is singleton, so you should init it only once in your app
        intl.init({
            currentLocale: local, // TODO: determine locale here
            locales,
        }).then(() => {
            window.intl = intl;
            // After loading CLDR locale data, start to render
            this.setState({
                initDone: true
            })

            this.props.handleLocal(local);
        });
    }

    render() {
        return (
            this.state.initDone &&
            <div className="App">
                <Routes/>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        isLogin: state.isLogin,
        local: state.local
    }
};

function mapDispatchToProps(dispatch) {
    return {
        handleLocal: (local) => {
            dispatch(handleLocal(local))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
