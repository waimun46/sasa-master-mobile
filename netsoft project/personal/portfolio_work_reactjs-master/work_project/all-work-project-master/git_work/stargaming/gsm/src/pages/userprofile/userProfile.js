import React, { Component } from 'react';
import _fetch from "../../fetchApi";
import sessionExpired from "../../components/SessionExpired";
import {connect} from "react-redux";
import moment from "moment";
import { DatePicker, Button } from 'antd';
import Pagination from '../../components/Pagination/Pagination';

import './userProfile.css';

class Userprofile extends Component {

    state = {
        user: localStorage.getItem('user') || 0,

        name: '',
        authentication: '',
        credit: 0,
        status: [],
        createdDate: '',

        optStatus: '-1',
        type: 'campaign',
        startDate: moment().format("YYYY-MM-DD"),
        endDate: moment().format("YYYY-MM-DD"),

        initDone: false,
        content: [],
        mapStatus: [],
        totalRecord: 0,
        totalPage: 0,
        curPage: 1
    }

    componentDidMount = () => {
        this.init()
        this.history(1, this.state.type)
    }

    init = () => {
        _fetch({
            api: 'Client/User',
            data: {
                action: 'profile',
                user: this.state.user
            }
        }).then(res => res.json()).then((data) => {
            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {

                }
                sessionExpired(data)
            } else {
                this.setState({
                    name: data.data.name,
                    credit: data.data.credit,
                    authentication: data.data.authentication,
                    status: data.data.status_display,
                    createdDate: data.data.created_date
                });
            }
        })
    }

    start = (date, dateString) => {
        this.setState({
            startDate: dateString
        })
    }

    end = (date, dateString) => {
        this.setState({
            endDate: dateString
        })
    }

    changeContent = (type, status) => {
        this.setState({
            initDone: false
        });
        this.history(1, type, status)
    }

    history = (page, type, status) => {

        let optType = type || this.state.type;
        let optStatus = status || this.state.optStatus;

        _fetch({
            api: 'Client/User',
            data: {
                action: 'history',
                user: this.state.user,
                type: optType,
                status: optStatus,
                start_date: this.state.startDate,
                end_date: this.state.endDate,
                page: page || 1
            }
        }).then(res => res.json()).then((data) => {
            if (data.status.toLocaleLowerCase() === 'error') {
                if (data.error.toLowerCase() === 'unknown_error') {

                }
                sessionExpired(data)
            } else {
                this.setState({
                    initDone: true,
                    content: data.data.reports,
                    mapStatus: data.data.map_status,
                    totalRecord: data.data.total_record,
                    totalPage: data.data.total_page,
                    curPage: data.data.cur_page,
                    type: optType,
                    optStatus: optStatus,
                });
            }
        })
    }

    back = () => {
        localStorage.removeItem('user');
        this.props.history.push('/Userpage')
    }

    render() {
        return (
            <article className="main_content">
                <header>
                    <h2>{window.intl.get('user.profile')}</h2>
                </header>
                <section className="page_content">

                    <div className="title-btn">
                        <span className="backBtn" onClick={this.back}>{window.intl.get('other.back')}</span>
                    </div>

                    {/*Content insert here*/}
                    <div className="userprofile_content">
                        <div className="top_content profileInfo">
                            <div className="username">
                                <label>{window.intl.get('user.username')} :</label>
                                <p className="noMargin">{this.state.name}</p>
                            </div>
                            <div className="authentication">
                                <label>{window.intl.get('user.authentication')} :</label>
                                <p className="noMargin">{this.state.authentication}</p>
                            </div>
                            <div className="credit">
                                <label>{window.intl.get('credit.credit')} :</label>
                                <p className="noMargin">{this.state.credit}</p>
                            </div>
                            <div className="credit">
                                <label>{window.intl.get('other.status')} :</label>
                                <p className="noMargin">{this.state.status[this.props.local]}</p>
                            </div>
                            <div className="credit">
                                <label>{window.intl.get('date.created')} :</label>
                                <p className="noMargin">{this.state.createdDate}</p>
                            </div>
                            <div className="clearfix"></div>
                        </div>

                        <div className="bottom_content">
                            <h3>{window.intl.get('statistics.history')}</h3>
                            <hr/>
                            <div className="type_content">
                                <h4>{window.intl.get('other.type')} :</h4>
                                <div className="btn_style">
                                    <button className={this.state.type === 'campaign' ? 'activeBtn' : null} onClick={() => {this.changeContent('campaign', this.state.optStatus)}}>{window.intl.get('sms.campaign')}</button>
                                    <button className={this.state.type === 'single' ? 'activeBtn' : null} onClick={() => {this.changeContent('single', this.state.optStatus)}}>{window.intl.get('sms.singleSMS')}</button>
                                    <button className={this.state.type === 'api' ? 'activeBtn' : null} onClick={() => {this.changeContent('api', this.state.optStatus)}}>{window.intl.get('sms.apiSend')}</button>
                                </div>
                            </div>
                        </div>

                        {
                            this.state.initDone &&
                            <div>
                                <div className="row select_margin">
                                    <label>{window.intl.get('other.status')} :  &nbsp;</label>
                                    <select className="formSelect" value={this.state.optStatus}
                                            onChange={(e) => {this.changeContent(this.state.type, e.target.value)}}>
                                        <option value='-1'>{window.intl.get('select.all')}</option>
                                        {
                                            this.state.mapStatus && this.state.mapStatus[this.props.local] && this.state.mapStatus[this.props.local].map((item, index) => {
                                                return (
                                                    <option value={item.key.toString()} key={index}>{item.value}</option>
                                                )
                                            })
                                        }
                                    </select>
                                </div>
                                <div className="row select_margin">
                                    {window.intl.get('date.from')}&nbsp;&nbsp;
                                    <DatePicker defaultValue={moment(new Date(this.state.startDate), "YYYY-MM-DD")} onChange={this.start} />&nbsp;&nbsp;
                                    {window.intl.get('date.to')}&nbsp;&nbsp;
                                    <DatePicker defaultValue={moment(new Date(this.state.endDate), "YYYY-MM-DD")} onChange={this.end} />&nbsp;&nbsp;
                                    <Button onClick={() => {this.history(1, this.state.type, this.state.optStatus)}} className=""> {window.intl.get('button.search')}</Button>
                                </div>

                                <div id="table_body">
                                    <table className="table_style report">
                                        <thead>
                                        <tr>
                                            {
                                                this.state.type === 'campaign' ? <th>{window.intl.get('campaign.title')}</th> : <th>{window.intl.get('sms.phone')}</th>
                                            }
                                            <th>{window.intl.get('sms.content')}</th>
                                            <th>{window.intl.get('other.status')}</th>
                                            <th>{window.intl.get('date.datetime')}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            this.state.content && this.state.content[this.props.local] &&
                                            this.state.content[this.props.local].map((item, key) => {
                                                return (
                                                    <tr key={key}>
                                                        {
                                                            this.state.type === 'campaign' ? <td>{item.name}</td> : <td>{item.receiver}</td>
                                                        }
                                                        <td>{item.message}</td>
                                                        <td>{item.status}</td>
                                                        <td>{item.created_date}</td>
                                                    </tr>
                                                )
                                            })
                                        }
                                        </tbody>
                                    </table>
                                </div>
                                <Pagination totalRecord={this.state.totalRecord} totalPage={this.state.totalPage} curPage={this.state.curPage} switchPage={this.history}/>
                            </div>
                        }

                    </div>
                    {/*Content insert here*/}
                </section>
            </article>
        );
    }
}

function mapProps(state){
    return {
        local:state.local
    }
}

export default connect(mapProps)(Userprofile)
