import React , { Component } from 'react';

import Todo from '../../images/todo.png';
import Letter from '../../images/letter.png';
import Pie from '../../images/pie.png';
import Card from '../../images/card.png';

class Apidoc extends  Component{
    render() {
        return (
            <article className="main_content">
                <header>
                    <h2>{window.intl.get('apiDocument.title')}</h2>
                </header>
                <section className="page_content">
                    {/*content here*/}
                    <div className="row api-padd">
                        <div className="col-md-3 col-sm-6 col-xs-12">
                            <div className="square">
                                <img src={Todo} alt=""/>
                                <div className="apiTitle">{window.intl.get('apiDocument.authentication')}</div>
                            </div>
                        </div>
                        <div className="col-md-3 col-sm-6 col-xs-12">
                            <div className="square">
                                <img src={Letter} alt=""/>
                                <div className="apiTitle">{window.intl.get('apiDocument.send')}</div>
                            </div>
                        </div>
                        <div className="col-md-3 col-sm-6 col-xs-12">
                            <div className="square">
                                <img src={Pie} alt=""/>
                                <div className="apiTitle">{window.intl.get('apiDocument.checkStatus')}</div>
                            </div>
                        </div>
                        <div className="col-md-3 col-sm-6 col-xs-12">
                            <div className="square">
                                <img src={Card} alt=""/>
                                <div className="apiTitle">{window.intl.get('apiDocument.checkCredit')}</div>
                            </div>
                        </div>
                        <span className="inv_button" onClick={() => {window.open('http://d2vcbit7zsnyp7.cloudfront.net/web/sms_api.docx')}}>
                            {window.intl.get('action.download')}&nbsp;
                            <span className="gly-margin"><i className="glyphicon glyphicon-arrow-down"></i></span>
                        </span>
                    </div>

                    {/*content here*/}
                </section>
            </article>
        )
    }
}


export default Apidoc;