import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";

import {Link} from 'react-router-dom';

class ConfirmRequestMoney extends BaseComponent {
  render() {
    return (
	
	<section id="ConfirmRequestMoney">
	<div className="form">
	<h1>Confirm Request Money</h1>
	
	 <div>
		 <ul>
		 <li className="lineHeight_style">Username :</li>
		 <li className="lineHeight_style2">John</li>
		 <li className="lineHeight_style">Phone :</li>
		 <li className="lineHeight_style2">0123456789</li>
		 <li className="lineHeight_style">Amount :</li>
		 <li className="lineHeight_style2">50.00</li>
		 <li className="lineHeight_style">Remark :</li>
		 <li className="lineHeight_style3">xxxxxxxxx xxxxxxx xxxx</li>
		 
		 </ul>
	</div>
	 
	<Link to="./Home">	 
	  <button type="button" className="btn btn-danger button button-block button_margintop" onclick="logIn()">REQUEST</button>
	</Link>
	</div>
    </section> 
    );
  }
}

export default ConfirmRequestMoney;

