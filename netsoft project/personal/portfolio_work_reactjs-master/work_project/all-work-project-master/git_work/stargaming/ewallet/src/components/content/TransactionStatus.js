import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";

import visamastercard from '../../include/images/visamastercard.PNG';

class TransactionStatus extends BaseComponent {
  render() {
    return (
	
<section id="ConfirmSendMoney">
	<div className="form">
	<h1>Transaction Status</h1>
	
	 <div>
		 <ul>
		 <li style={{ textAlign:"left" ,lineHeight:"3"}}>Status Description :</li>
		 <li style={{ textAlign:"right" ,lineHeight:"3" ,color:"rgb(13, 189, 101)"}}>Successful</li>
		 <li style={{ textAlign:"left" ,lineHeight:"3"}}>Transaction Date :</li>
		 <li style={{ textAlign:"right" ,lineHeight:"3"}}>25/09/2017</li>
		 <li style={{ textAlign:"left" ,lineHeight:"3"}}>Payment Mode:</li>
		 <li style={{ textAlign:"right" ,lineHeight:"3"}}><figure><img src={visamastercard}/></figure></li>
		 <li style={{ textAlign:"left" ,lineHeight:"3"}}>Card Number</li>
		 <li style={{ textAlign:"right" ,lineHeight:"3"}}>2222222222</li>
		  <li style={{ textAlign:"left" ,lineHeight:"3"}}>Amount :</li>
		 <li style={{ textAlign:"right" ,lineHeight:"3"}}>RM 50.00</li>
		 
		 </ul>
	</div>
	 
	
	  <button style={{ marginTop:"300px" }} type="button" className="btn btn-danger button button-block" onclick="logIn()">SUBMINT</button>
	
	</div>
   </section>  
    );
  }
}

export default TransactionStatus;

