import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";
import {Link} from 'react-router-dom';
import store from '../../store';

class Security extends BaseComponent {
	
	constructor(props) {
		super(props);
		this.state = { 
			props: props,
			data: store.getContent()
		};
	}
	
  render() {
    return (
	
		<section id="Security">

			<header className="header-same">
				
				<div className="title-same">{this.state.data.content.security}</div>						
			</header>

			<div className="form">
				<form role="form" action="/login" method="POST" name="login" accept-charset="UTF-8" enctype="application/x-www-form-urlencoded" autocomplete="off">
					<div className="field-wrap security_style">
						<input style={{ height: '40px' ,textAlign: "center" ,fontSize: "14px"}} 
							type="password" name="password" placeholder=" *  *  *  *  *  *" 
							aria-describedby="basic-addon2" autocomplete="off" className="form-control" />
					</div>
				</form>
				<div className="button-align-setting">
					<Link to="History">
						<button type="button" className="btn btn-danger button button-block" >{this.state.data.content.submit} </button>
					</Link>				
				</div>
			</div>
		</section>
    );
  }
}

export default Security;

