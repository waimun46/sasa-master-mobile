import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";
import {Link} from 'react-router-dom';
import store from '../../store';

import { Select } from 'antd';
import {PostData} from "../services/PostData";
const Option = Select.Option;

class Withdraw extends BaseComponent {
	constructor(props) {
		super(props);
		this.state = { 
			props: props,
			data: store.getContent(),
            cookit: document.cookie,
            list:[],
		};
	}

    componentDidMount = () => {

        let session = this.state.cookit.replace(/(?:(?:^|.*;\s*)session\s*\=\s*([^;]*).*$)|^.*$/, "$1");

        let params = {
            'action' : 'getBankAccountList',
            'session' : session
        };

        PostData('BankAccount',params ,this.props).then((result) => {
            if(result.status == 'ok' && result.data != ''){
                console.log("this is the data",result.data.list);
                this.setState({
                    list: result.data.list,
                })
            }
            else{
                this.setState({
                    validationerror: this.state.data.content[result.error]
                });
            }

        });
    }

	
  render() {
    return (
	
	<section id="Withdraw" className="same-form-style">
		
		<header className="header-same">
			<Link to="Balance">
				<div className="back-btn">
					<i className="glyphicon glyphicon-menu-left"></i>
				</div>
			</Link>
			<div className="title-same">{this.state.data.content.withdraw}</div>						
		</header>
	
	
		<div className="form">	   
			<div className="select_margin">
				<Select className="select_style"  placeholder={this.state.data.content.selectaccount}>
                    {
                        this.state.list.map((dynamicData,key) =>
                            <Option value={dynamicData.id}>{dynamicData.bankname} {dynamicData.account}</Option>
                        )
                    }
				</Select>

			</div>

			<form role="form" action="/login" method="POST" name="login" accept-charset="UTF-8" enctype="application/x-www-form-urlencoded" autocomplete="off">
				<div className="field-wrap input_width">
					<ul>
						<li>{this.state.data.content.amount} :</li>
					</ul>
					<input type="text" name="text"  aria-describedby="basic-addon2" autocomplete="off" className="form-control" />
				</div>
				
				<div className="field-wrap input_style textarea_style">
					<ul>
						<li>{this.state.data.content.remark} :</li>
					</ul>
					<textarea rows="5" cols="10" aria-describedby="basic-addon2" autocomplete="off" className="form-control"></textarea>
				</div>
			</form>
			<div className="button-align-setting">
				<Link to="ConfirmWithdraw">	 
					<button type="button" className="btn btn-danger button button-block button_margintop">{this.state.data.content.submit} </button>
				</Link>				
			</div>
		</div>
	</section>
    );
  }
}

export default Withdraw;

