import React, { Component } from 'react';
import BaseComponent, {CollectionCreateForm} from "./BaseComponent";
import {Link} from 'react-router-dom';
import store from '../../store';
import {PostData} from "../services/PostData";
import $ from "jquery";
import Verify from './Verify';

class RequestVerifyCode extends BaseComponent {
	
	constructor(props) {
		super(props);
		this.state = { 
			props: props,
			data: store.getContent(),
            cookit: document.cookie,
		};
	}

    componentDidMount = () => {

    }

    submit = () => {
        let session = this.state.cookit.replace(/(?:(?:^|.*;\s*)session\s*\=\s*([^;]*).*$)|^.*$/, "$1");
        let params = {
            'action': 'forgotTransPass',
            'session': session
        };
        console.log(params);
        PostData('Profile',params ,this.props).then((result) => {
            if(result.status == 'ok' && result.data != ''){
                this.setState({
                    prefix : result.data.prefix,
                })
            }else{
                this.setState({
                    validationerror: this.state.data.content[result.error]
                });
            }
        });
    }

    verifysend = () => {
        let session = document.cookie.replace(/(?:(?:^|.*;\s*)session\s*\=\s*([^;]*).*$)|^.*$/, "$1");
        let prefix = this.state.prefix;
        let password = $('input[name="password"]').val();
        let params = {
            'action': 'forgotTransPassVerify',
            'otp': prefix + '-' + password,
            'session': session
        };
        console.log(params);
        PostData('Profile', params, this.props).then((result) => {
            console.log(result);
            if(result.status == 'ok') {
                this.props.history.push('/ConfirmSecurityCode');
            }else{
                this.setState({
                    //validationerror: result.error 
                    validationerror: this.state.data.content[result.error]
                });
            }
        });
    }

    resendverifycode = () => {
        let session = document.cookie.replace(/(?:(?:^|.*;\s*)session\s*\=\s*([^;]*).*$)|^.*$/, "$1");
        let params = {
            'action': 'resendVerify',
            'verifyAction': 'forgotTransPass',
            'session': session
        };
        console.log(params);
        PostData('Profile', params, this.props).then((result) => {
            console.log(result);
            if(result.status == 'ok') {
                this.setState({
                    prefix : result.data.prefix,
                });
                this.showModal();
            }else{
                this.setState({
                    //validationerror: result.error
                    validationerror: this.state.data.content[result.error]
                });
            }
        });

    }

  	render() {
		return (
            this.state.prefix ? 
            	<Verify 
            	    prefix={this.state.prefix}
                    onClick={this.verifysend}
                    onReSendVerify={this.resendverifycode}
            	    visible={this.state.visible}
                    handleCancel={this.handleCancel}
            	    validationerror={this.state.validationerror}
            	/>
            
            :

			<section id="RequestVerifyCode">

				<header className="header-same">
					<Link to="ChangeSecurityCode">
						<div className="back-btn">
							<i className="glyphicon glyphicon-menu-left"></i>
						</div>
					</Link>
					<div className="title-same">{this.state.data.content.requestverifycode}</div>

				</header>

				<div className="form">
					<form>
                        
						<div className="text_style">
							<p>{this.state.data.content.requestverifytext}</p>
						</div>

						<div className="button-align-setting">
							<button type="button" className="btn btn-danger button button-block" onClick={this.submit}>{this.state.data.content.submit}</button>
						</div>

					</form>

				</div>
			</section>
		);
  	}
}

export default RequestVerifyCode;

