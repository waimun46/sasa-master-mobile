import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";

import {Link} from 'react-router-dom';
import { Row, Col } from 'antd';
import store from '../../store';

import visamastercard from '../../include/images/visamastercard.PNG';

class ThirdPartyBankTransferConfirm extends BaseComponent {
	
	constructor(props) {
		super(props);
		this.state = { 
			props: props,
			data: store.getContent()
		};
	}
	
  render() {
    return (
		<section id="ThirdPartyBankTransferConfirm">
			<header className="header-same">
				<Link to="Home">
					<div className="back-btn">
						<i className="glyphicon glyphicon-menu-left"></i>
					</div>
				</Link>
				<div className="title-same">
					{this.state.data.content.thirdpartybanktransferconfirm}
				</div>
			</header>

			<div className="pricing-grids">
				<div className="col-md-12 pricing-grid">
					<div className="price-value">
						<h3>{this.state.data.content.totalpayment}</h3>
						<p className="price-label-1">RM<span>50.00</span></p>
					</div>

					<div className="price-bg">
						<ul className="count">
							<li>
								<Row>
									<Col className="align-left" span={12}>{this.state.data.content.paymentmode} :</Col>
									<Col className="align-right" span={12}><img src={visamastercard}/></Col>
								</Row>

							</li>
							<li>
								<Row>
									<Col className="align-left" span={12}>{this.state.data.content.cardnumber} :</Col>
									<Col className="align-right" span={12}>123456789</Col>
								</Row>

							</li>
						</ul>

						<Row className="btn-2">
							<Col className="btn-2-padding" span={12}>
								<button type="button" className="btn btn-danger button button-block" onclick="logIn()">
									{this.state.data.content.confirm}
								</button>
							</Col>
							<Col className="btn-2-padding" span={12}>
								<button type="button" className="btn btn-danger button button-block cencel-btn" onclick="logIn()">
									{this.state.data.content.cancel}
								</button>
							</Col>
						</Row>
					</div>
				</div>
			</div>
		</section>
    );
  }
}

export default ThirdPartyBankTransferConfirm;

