import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";
import {Link} from 'react-router-dom';


import { Select } from 'antd';
import { DatePicker } from 'antd';
import moment from 'moment';
import { Radio } from 'antd';
import { Table } from 'antd';
import store from '../../store';


const { MonthPicker, RangePicker } = DatePicker;

const Option = Select.Option;
const dateFormat = 'YYYY/MM/DD';
const RadioGroup = Radio.Group;


function handleChange(value) {
  console.log(value);
}


const columns = [{
  title: 'Date Time',
  dataIndex: 'datetime',
  key: 'datetime',
  render: text => <a href="#">{text}</a>,
}, 

{
  title: 'Status',
  dataIndex: 'status',
  key: 'status',
}, 

{
  title: 'Amount',
  dataIndex: 'amount',
  key: 'amount',
},


];


const data = [{
  key: '1',
  datetime: '28 Sept 2017',
  status: 'Received Money from Amber',
  amount: 'RM 20.00',
}, 

{
  key: '2',
  datetime: '25 Sept 2017',
  status: 'Withdraw Money',
  amount: '- RM 100.00',
}, 

{
  key: '3',
  datetime: '29 Sept 2017',
  status: 'Received Money from Kelvin',
  amount: 'RM 60.00',
},

{
  key: '4',
  datetime: '30 Sept 2017',
  status: 'Maybank Transfer to CTS',
  amount: '- RM 30.00',
}];


class History extends BaseComponent {
	constructor(props) {
		super(props);
		this.state = { 
			props: props,
			data: store.getContent()
		};
	}
	 state = {
     value: 1,
  }
  onChange = (e) => {
    console.log('radio checked', e.target.value);
    this.setState({
      value: e.target.value,
    });
  }
	
 
 render() {
    return (
	
	<section id="History" className="same-form-style">
		
		<header className="header-same">
			<Link to="Home">
				<div className="back-btn">
					<i className="glyphicon glyphicon-menu-left"></i>
				</div>
			</Link>
			<div className="title-same">
				{this.state.data.content.history}
			</div>
		</header>
		
		<div className="form">
			<div className="field-wrap input_width">
				<ul>
					<li>{this.state.data.content.from} :</li>
				</ul>
				<DatePicker defaultValue={moment('2017/09/18',dateFormat)} format={dateFormat} />
			</div>
			<div className="field-wrap input_width">
				<ul>
					<li>{this.state.data.content.to} :</li>
				</ul>
				<DatePicker defaultValue={moment('2017/09/18',dateFormat)} format={dateFormat} />
			</div>

				<div className="RadioGroup-style">
				  <ul>
					<li>{this.state.data.content.transaction} :</li>
				</ul>
				<RadioGroup className="radio_style" onChange={this.onChange} value={this.state.value}>
						<Radio value={1}>{this.state.data.content.moneyin}</Radio>
						<Radio value={2}>{this.state.data.content.moneyout}</Radio>
				 </RadioGroup>
			</div>
				
				<div className="table_style">
					 <Table columns={columns} dataSource={data} />				
				</div>

		 </div>
	   </section>  
    );
  }
}

export default History;

