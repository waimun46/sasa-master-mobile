import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";
import {Link} from 'react-router-dom';
import store from '../../store';
import {PostData} from "../services/PostData";

class ConfirmAddBank extends BaseComponent {

	constructor(props) {
		super(props);
		this.state = { 
		props: props,
		data: store.getContent(),
		cookit: document.cookie,
		bankid : '',
		name : '',
		account : '',
		bankname : '',
		};
	}
	
	componentDidMount = () => {
	}
	
	
	verifysend = () => {
		if (typeof this.props.onClick === 'function') {
		this.props.onClick();
	}}

    resendverifycode = () => {
        if (typeof this.props.onReSendVerify === 'function') {
            this.props.onReSendVerify();
        }
    }
	
	render() {
		return (
			<section id="ConfirmAddBank">
				<header className="header-same">
					<Link to="AddBank">
				  	<div className="back-btn">
						<i className="glyphicon glyphicon-menu-left"></i>
				  	</div>
				  	</Link>
				  	<div className="title-same">{this.state.data.content.confirmaddbank}</div>
			   	</header>
				<div className="chart">
					<div className="diagram">
						<div className="login-error field-wrap">
						  <label className="text-danger" >{this.props.validationerror}</label>
					  	</div>
					 	<p className="forgot3">{this.state.data.content.prefix} : {this.props.prefix}</p>
					 	<div className="field-wrap security_style">
							<input type="password" name="password" placeholder={this.state.data.content.securitycode} aria-describedby="basic-addon2" autocomplete="off" className="form-control" />
					 	</div>
					 	<a className="forgot4" onClick={this.resendverifycode}>{this.state.data.content.resendverifycode}</a>
				  	</div>
				  	<div className="chart_list">
						<ul>
							<li className="red  chart-p">{this.state.data.content.bank} :
						    <p className="percentage">{this.props.bankname}</p>
						</li>
						<li className="purple">
						    {this.state.data.content.accountno} :
						    <p className="percentage">{this.props.account}</p>
						</li>
						<li className="yellow chart-p-left">
						    {this.state.data.content.holdername} :
						    <p className="percentage">{this.props.name}</p>
						</li>
						<div className="clear"></div>
					 </ul>
				  </div>
			   	</div>
			   	<div className="padding-btn button-align-setting">
				   <button type="button" className="btn btn-danger button button-block button_margintop" onClick={this.verifysend}>{this.state.data.content.submit}</button>
			   	</div>
			</section>
		);
	}
}
export default ConfirmAddBank;