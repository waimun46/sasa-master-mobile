
let en = require('./data/en.json');
let ch = require('./data/ch.json');
let my = require('./data/my.json');
let constant = require('./data/constant.json');

let Api = {

	generatePublicKey() {
	  var text = "";
	  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	  for (var i = 0; i < 6; i++)
		text += possible.charAt(Math.floor(Math.random() * possible.length));

	  return text;
	},


	getContent(language = 'en', session = '') {
		switch(language){
			case 'ch':
				return {'content':ch,'constant':constant,'public':this.generatePublicKey(),'session':session, 'lang':language};
				break;
			case 'my':
				return {'content':my,'constant':constant,'public':this.generatePublicKey(),'session':session, 'lang':language};
				break;
			default:
				return {'content':en,'constant':constant,'public':this.generatePublicKey(),'session':session, 'lang':language};
				break;
		}
	}

};

module.exports = Api;