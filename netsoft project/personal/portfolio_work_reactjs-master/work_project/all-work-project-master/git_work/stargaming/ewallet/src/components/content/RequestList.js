import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";

import {Link} from 'react-router-dom';

import { Button } from 'antd';

class RequestList extends BaseComponent {
	 state = {
     value: 1,
  }
  onChange = (e) => {
    console.log('radio checked', e.target.value);
    this.setState({
      value: e.target.value,
    });
  }
	
 
 render() {
    return (
	<section id="RequestList">
	   <div className="form">
	   
		<header className=" header_position">
	
			<div className="title-same">Request List</div>						
		</header>
	
	   <br/><br/><br/><br/>
	   
	<div className="tabset">
		   <input type="radio" name="tabset" id="tab9" aria-controls="LEND" checked/>
		   <label for="tab9">LEND</label>
		   
		   <input type="radio" name="tabset" id="tab10" aria-controls="BORROW"/>
		   <label for="tab10">BORROW</label>
		   
		
		   
		   <div className="tab-panels">
			  <section id="LEND" className="tab-panel">
				  <div className="table_style">
									 
					  <div className="table_uploadco4">
					  <div className="table">
						 
					   <div className="table-cell">
							<span>28 Sept 2017</span>
						 </div>
						 <div className="table-cell">
							<span>Request Form John RM 10.00</span>
						 </div>
						 <div className="table-cell">
							<span>
								  <Button className="btn_actionstyle" type="primary">APPROVE </Button>
								  <span className="ant-divider" />
								 <Button className="btn2_actionstyle" type="danger">REJECT</Button>
								 </span>
						  </div>
						 
						<div className="table-cell">
							<span>25 Sept 2017</span>
						 </div>
						 <div className="table-cell">
							<span>Amy Request RM 30.00</span>
						 </div>
						 <div className="table-cell">
							<span>
								  <Button className="btn_actionstyle" type="primary">APPROVE </Button>
								  <span className="ant-divider" />
								 <Button className="btn2_actionstyle" type="danger">REJECT</Button>
								 </span>
						 </div>
						 
						
						</div>
					 </div>
	  
				 </div>
			  </section>
			  
			  
			  
			  
			  <section id="BORROW" className="tab-panel">
				  <div className="table_style">
									 
					  <div className="table_uploadco4">
					  <div className="table">
						 
					   <div className="table-cell">
							<span>28 Sept 2017</span>
						 </div>
						 <div className="table-cell">
							<span>Request Form John RM 10.00</span>
						 </div>
						 <div className="table-cell">
							<span>
								<Button className="btn2_actionstyle" type="danger">CANCEL</Button>
								 </span>
						  </div>
						 
					</div>
					 </div>
	  
				 </div>
			  </section>
			 
		
			  
			 
			  </div>
		</div>


	
	</div>
</section>


    );
  }
}

export default RequestList;

