import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";
import {Link} from 'react-router-dom';
import store from '../../store';
import {Table, Select, Radio, Button, Modal, DatePicker} from 'antd';
import actions from "../../actions";
import {PostData} from "../services/PostData";

const RadioGroup = Radio.Group;

const dataSource = [{
    key: '1',
    name: 'Total Deposit Amount',
    age: 'Unlimited',
}, {
    key: '2',
    name: 'Daily Limit',
    age: 'Unlimited',
}, {
    key: '3',
    name: 'Min Limit/ Max Limit',
    age: 'Rm50 / Rm50k',
}];

const columns = [{
    dataIndex: 'name',
    key: 'name',
}, {
    dataIndex: 'age',
    key: 'age',
}];

class TopupCardDeposit extends BaseComponent {

    constructor(props) {
        super(props);
        this.state = {
            props: props,
            data: store.getContent()
        };
        this.onChange = this.onChange.bind(this)
    }

    onChange(e){
        const re = /^[0-9\b]+$/;
        if (e.target.value == '' || re.test(e.target.value)) {
            this.setState({value: e.target.value})
        }
    }

    componentWillMount = () => {
        window.win = null;

        let mysession = document.cookie.replace(/(?:(?:^|.*;\s*)session\s*\=\s*([^;]*).*$)|^.*$/, "$1");

        let params = {
            'action' : 'getBalance',
            'session': mysession
        };

        PostData('Wallet', params, this.props).then((result) => {
            console.log(result);
            if(result.status == 'ok' && result.data.session != '') {
                this.setState({
                    balance: Number(result.data['balance']).toFixed(2),
                });
            }else{
                this.setState({
                    //validationerror: result.error
                    validationerror: this.state.data.content[result.error]
                });
                console.log(result);
            }
        });
    }

    onChange = (e) => {
        console.log('radio checked', e.target.value);
        this.setState({
            value: e.target.value,
        });
    }


    render() {
        const radioStyle = {
            fontSize: '14px',
            verticalAlign: 'center',
            height: '18px',
        }

        return (

            <section id="TopupCardDeposit">
                <header className="header-same">
                    <Link to="Deposit">
                        <div className="back-btn">
                            <i className="glyphicon glyphicon-menu-left"></i>
                        </div>
                    </Link>
                    <div className="title-same">{this.state.data.content.topupgateway}</div>
                </header>

                <div className="form">
                    <form role="form" action="/login" method="POST" name="login" accept-charset="UTF-8" enctype="application/x-www-form-urlencoded" autocomplete="off">
                        <div className="field-wrap security_style">
                            <Table
                                dataSource={dataSource}
                                columns={columns}
                                pagination={false}
                                showHeader={false}
                                bordered={false}/>
                        </div>

                        <div>{this.state.data.content.bank}</div>
                        <RadioGroup onChange={this.onChange} value={this.state.value} style={{ height: '40px'}}>
                            <Radio value={1} style={radioStyle}>MAYBANK</Radio>
                            <Radio value={2} style={radioStyle}>PUBLIC BANK</Radio>
                            <Radio value={3} style={radioStyle}>RHB BANK</Radio>
                            <Radio value={4} style={radioStyle}>HONG LEONG BANK</Radio>
                            <Radio value={5} style={radioStyle}>CIMB BANK</Radio>
                        </RadioGroup>

                        <div className="field-wrap input_width">
                            <ul>
                                <li>{this.state.data.content.amount} :</li>
                            </ul>
                            <input type="number" pattern="\d+" name="text" placeholder="Mininum of RM50 / Maximum of RM50,000" aria-describedby="basic-addon2" autocomplete="off" className="form-control" />
                        </div>
                        <br />
                        <div className="button-align-setting">
                            <Link to="ConfirmDeposit" >
                                <button type="button" className="btn btn-danger button button-block button_margintop">
                                    {this.state.data.content.submit}
                                </button>
                            </Link>
                        </div>
                    </form>
                </div>
            </section>
        );
    }
}

export default TopupCardDeposit;

