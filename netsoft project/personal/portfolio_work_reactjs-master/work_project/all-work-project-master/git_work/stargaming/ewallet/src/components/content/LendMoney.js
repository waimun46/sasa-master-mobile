import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";

import {Link} from 'react-router-dom';

import { Checkbox } from 'antd';

function onChange(e) {
  console.log(`checked = ${e.target.checked}`);
}

class LendMoney extends BaseComponent {
	
	
  render() {
    return (
	
	<section id="ConfirmSendMoney">
	<div className="form">
	<h1>Lend Money</h1>
   
 <div>
		 <ul>
		 <li className="lineHeight_style">Borrower Username :</li>
		 <li className="lineHeight_style2">John Wong</li>
		 <li className="lineHeight_style">Borrwer Phone Number :</li>
		 <li className="lineHeight_style2">0123456789</li>
		 <li className="lineHeight_style">Request Amount :</li>
		 <li className="lineHeight_style2">100.00</li>
		 <li className="lineHeight_style">Lend Amount :</li>
		 <li className="lineHeight_style2">100.00</li>
		 </ul>
	</div>


	 <form role="form" action="/login" method="POST" name="login" accept-charset="UTF-8" enctype="application/x-www-form-urlencoded" autocomplete="off">
		 
		<div className="field-wrap security_style">
			 <input type="password" name="password" placeholder="Security Code" aria-describedby="basic-addon2" autocomplete="off" className="form-control" />
			</div>
			
			<Checkbox onChange={onChange}>Are you sure want make payment?</Checkbox>
		
			<Link to="./Home">	 
				<button type="button" className="btn btn-danger button button-block button_margintop" onclick="logIn()">CONFIRM</button>
			 </Link>
		</form>
		
		</div>
	   </section>  
    );
  }
}

export default LendMoney;

