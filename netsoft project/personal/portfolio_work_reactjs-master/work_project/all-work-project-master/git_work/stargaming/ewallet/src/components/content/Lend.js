import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";

import {Link} from 'react-router-dom';
import { Row, Col } from 'antd';
import { Radio } from 'antd';
import { Button } from 'antd';
import { Pagination } from 'antd';
import store from '../../store';
const RadioGroup = Radio.Group;


function handleChange(value) {
  console.log(value);
}



class Lend extends BaseComponent {
	 state = {
     value: 1,
  }
  onChange = (e) => {
    console.log('radio checked', e.target.value);
    this.setState({
      value: e.target.value,
    });
  }
	constructor(props) {
		super(props);
		this.state = { 
			props: props,
			data: store.getContent()
		};
	}
 
 render() {
    return (
	
<section id="Lend">

    <header className="header-same">
        <Link to="Home">
        <div className="back-btn">
            <i className="glyphicon glyphicon-menu-left"></i>
        </div>
        </Link>
        <div className="title-same">
            {this.state.data.content.lend}
        </div>
    </header>

    <div className="form">

        <div className="table_uploadco4">
            <div className="table">

                <div className="table-cell table-header">
                    <span>May, Malaysia</span>
                </div>
                <div className="table-cell">
                    <span>A loan of RM 2,000 to open a restaurant</span>
                </div>
                <div className="table-cell">
                <span>
					<Link to="ConfirmPayment">
						<Button className="action2_btnstyle">{this.state.data.content.lend} RM25</Button>
					</Link>
					
					<span className="ant-divider" />		
					
					<Link to="LearnMore">
						<Button className="action3_btnstyle" >{this.state.data.content.learnmore}</Button>
					</Link>
				</span>
                </div>

                <div className="table-cell table-header">
                    <span>Jun, Singapore</span>
                </div>
                <div className="table-cell">
                    <span>A loan of RM 500,000 for his son's school fee</span>
                </div>

                <div className="table-cell">
                    <span>
						<Button className="action2_btnstyle">{this.state.data.content.lend} RM25</Button>
						<span className="ant-divider" />
						<Button className="action3_btnstyle" >{this.state.data.content.learnmore}</Button>
					</span>
                </div>

            </div>
        </div>

    </div>
</section>
    );
  }
}

export default Lend;

