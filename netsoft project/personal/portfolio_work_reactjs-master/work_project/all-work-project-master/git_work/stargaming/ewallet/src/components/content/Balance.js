import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";
import react from 'react-dom'

//import the function
import {PostData} from '../services/PostData';
import {Link} from 'react-router-dom';
import store from '../../store';
import sha1 from "sha1";
import $ from "jquery";

class Balance extends BaseComponent {

	constructor(props) {
		super(props);
		this.state = {
			props: props,
			data: store.getContent()
		};
	}

    componentDidMount () {

        let mysession = document.cookie.replace(/(?:(?:^|.*;\s*)session\s*\=\s*([^;]*).*$)|^.*$/, "$1");
        let params = {
            'action' : 'getBalance',
            'session': mysession
        };

        PostData('Wallet',params , this.props).then((result) => {
            console.log(result);
            if(result.status == 'ok' && result.data.session != '') {
                this.setState({
                    balance: Number(result.data['balance']).toFixed(2),
                })
            }else{
                this.setState({
                    //validationerror: result.error
                    validationerror: this.state.data.content[result.error]
                });
                console.log(result);
            }
        });

    }
	
  render() {
    return (
	<section id="balance">
		<article className="profile-w3layouts">
		
				<header className="header-same">
					<Link to="Home">
						<div className="back-btn">
							<i className="glyphicon glyphicon-menu-left"></i>
						</div>
					</Link>
					<div className="title-same">{this.state.data.content.balance}</div>
				</header>
		
				<div className="profile-top">
					
					<div className=" profile-top-right">
						<h5>{this.state.data.content.totalbalance}</h5>
						<h1>{ this.state.balance }</h1>
					</div>
					<div className="clearfix"></div>
				</div>
				
				
				<div className="profile-ser">
					<Link to='Deposit'>
						<div className="profile-ser-grids border-right">
							<span className="glyphicon glyphicon-usd deposit" aria-hidden="true"></span>
							<h4>{this.state.data.content.deposit}</h4>
						</div>
					</Link>
					<Link to='Withdraw'>
					<div className="profile-ser-grids agileinfo">
						<span className="glyphicon glyphicon-briefcase withdraw" aria-hidden="true"></span>
						<h4>{this.state.data.content.withdraw}</h4>
					</div>
					</Link>
					<div className="clearfix"> </div>
				</div>
			</article>
	
	</section>
	
    );
  }
}

export default Balance;
