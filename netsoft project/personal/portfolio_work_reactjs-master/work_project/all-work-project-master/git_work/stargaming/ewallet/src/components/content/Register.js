import React, { Component } from 'react';
import BaseComponent, {CollectionCreateForm} from "./BaseComponent";

import {Link} from 'react-router-dom';
import store from '../../store';
import $ from 'jquery';
import actions from '../../actions';
import {PostData} from "../services/PostData";

class Register extends BaseComponent {
	
	constructor(props) {
		super(props);
		this.state = { 
			props: props,
			data: store.getContent(),
			getOTP: '',
			validationerror: '',
			formdata: {}
		};
	}
	  
	verify = () => {
		let params = {
			'action': 'verify',
			'otp': this.state.getOTP.concat('-').concat($('input[name="otp"]').val()),
			'username': $('input[name="username"]').val()
		};
        console.log(params);
        PostData('Register', params, this.props).then((result) => {
            console.log(result);
            if(result.status == 'ok' && result.data != ''){
                actions.switchLanguage(this.state.data.lang, result.data);
                document.cookie = "session="+result.data ;
                this.setState({
    				successVisible : true,
                })
            } else{
                this.setState({
                    //validationerror: result.error
                    validationerror: this.state.data.content[result.code]
                });
            }
        });

	}

    resendverifycode = () => {
        let params = {
            'action': 'resend',
            'confpass': $('input[name="confpass"]').val(),
            'email': $('input[name="email"]').val(),
            'password': $('input[name="password"]').val(),
            'phone': $('input[name="phone"]').val(),
            'username': $('input[name="username"]').val()
        };
        console.log(params);
        PostData('Register', params, this.props).then((result) => {
            console.log(result);
            if(result.status == 'ok') {
                this.setState({
                    getOTP: result.data.prefix,
                    validationerror: '',
                    formdata: {
                        username: $('input[name="username"]').val(),
                        email: $('input[name="email"]').val(),
                        phone: $('input[name="phone"]').val()
                    }
                });
                this.showModal();
            }else{
                this.setState({
                    //validationerror: result.error
                    validationerror: this.state.data.content[result.code]
                });
            }
        });
    }

    successRegister = () => {
		this.setState({
				validationerror : '',
				successVisible : false,
				visible : false,
        })
        
        this.props.history.push('/Home');
	}
	
	register = () => {
		let params = {
			'action': 'register',
			'confpass': $('input[name="confpass"]').val(),
			'email': $('input[name="email"]').val(),
			'password': $('input[name="password"]').val(), 
			'phone': $('input[name="phone"]').val(),
			'username': $('input[name="username"]').val()
		};

        console.log(params);
        PostData('Register', params, this.props).then((result) => {
            console.log(result);
            if(result.status == 'ok' && result.data.prefix != '') {
                this.setState({
                    getOTP: result.data.prefix,
                    validationerror: '',
                    formdata: {
                        username: $('input[name="username"]').val(),
                        email: $('input[name="email"]').val(),
                        phone: $('input[name="phone"]').val()
                    }
                });
            }else{
                this.setState({
                    //validationerror: result.error
                    validationerror: this.state.data.content[result.error]
                });
            }
        });
  	}
	  
render() {
    return (
		<section id="register">
			<header className="header-same">
				<Link to="./">
					<div className="back-1">
						<i className="glyphicon glyphicon-menu-left"></i>
					</div>
				</Link>
				<div className="title-1">{this.state.data.content.register}</div>
					
			</header>
			<div className="form-error">
				<label className="text-danger">{this.state.validationerror}</label>
			</div>
			<br />
			<Submitbutton chg={this} />
		</section>
    );
  }
}
function Submitbutton(props)
{
	const verify = props.chg.state.getOTP;
	if (verify)
		return (
		<div className="form">
			<div className="field-wrap">
				<div className="form-group input-group">
					<span id="basic-addon1" className="input-group-addon">
					<span className="glyphicon glyphicon-user"></span></span>
					<input type="text" name="username" aria-describedby="basic-addon1" className="form-control" disabled={true}/>
				</div>
			</div>
			
			<div className="field-wrap">
				<div className="form-group input-group">
					<span id="basic-addon2" className="input-group-addon">
					<span className="glyphicon glyphicon-lock"></span></span>
					<input type="password" name="password" aria-describedby="basic-addon2" autoComplete="off" className="form-control" disabled={true}/>
				</div>
			</div>
			
			<div className="field-wrap">
				<div className="form-group input-group">
					<span id="basic-addon2" className="input-group-addon">
					<span className="glyphicon glyphicon-lock"></span></span>
					<input type="password" name="confpass" aria-describedby="basic-addon2" autoComplete="off" className="form-control" disabled={true}/>
				</div>
			</div>
			
			<div className="field-wrap">
				<div className="form-group input-group">
					<span id="basic-addon2" className="input-group-addon">
					<span className="glyphicon glyphicon-earphone"></span></span>
					<input type="text" name="phone" aria-describedby="basic-addon1" className="form-control" disabled={true}/>
				</div>
			</div>
			
			<div className="field-wrap">
				<div className="form-group input-group">
					<span id="basic-addon2" className="input-group-addon">
					<span className="glyphicon glyphicon-envelope"></span></span>
					<input type="text" name="email" aria-describedby="basic-addon1" className="form-control" disabled={true}/>
				</div>
			</div>
			
			<span className="otp-code-style">{props.chg.state.data.content.prefix}: {verify}</span>
			<div className="field-wrap">
				<div className="form-group input-group">
					<span id="basic-addon2" className="input-group-addon">
					<span className="glyphicon glyphicon-lock"></span></span>
					<input type="text" name="otp" placeholder={props.chg.state.data.content.otp} aria-describedby="basic-addon2" autoComplete="off" className="form-control"/>
				</div>
			</div>

            <a className="forgot2" type="primary" onClick={props.chg.resendverifycode}>
                {props.chg.state.data.content.resendverifycode} ?
            </a>
            <CollectionCreateForm
			    visible={props.chg.state.visible}
				onCancel={props.chg.handleCancel}
				onCreate={props.chg.handleCancel}
				btnLabel="OK"
				content="The Verify Code Had Sent."
			/>

			<br /><br />
			<div className="button-align-setting">
			<button type="button" className="btn btn-danger button button-block" onClick={props.chg.verify}>{props.chg.state.data.content.submit}</button>
			</div>
			
			<CollectionCreateForm
                visible={props.chg.state.successVisible}
        	    onCancel={props.chg.successRegister}
            	onCreate={props.chg.successRegister}
            	btnLabel="OK"
                content="Congratulations. The Registration had Done."
            />
		</div>

	);
		
	else
		return (
		<div className="form-error-margin">
			<div className="field-wrap">
				<div className="form-group input-group">
					<span id="basic-addon1" className="input-group-addon">
					<span className="glyphicon glyphicon-user"></span></span>
					<input type="text" name="username" placeholder={props.chg.state.data.content.username} aria-describedby="basic-addon1" className="form-control" />
				</div>
			</div>

			<div className="field-wrap">
				<div className="form-group input-group">
					<span id="basic-addon2" className="input-group-addon">
					<span className="glyphicon glyphicon-lock"></span></span>
					<input type="password" name="password" placeholder={props.chg.state.data.content.password} aria-describedby="basic-addon2" autoComplete="off" className="form-control"/>
				</div>
			</div>
			
			<div className="field-wrap">
				<div className="form-group input-group">
					<span id="basic-addon2" className="input-group-addon">
					<span className="glyphicon glyphicon-lock"></span></span>
					<input type="password" name="confpass" placeholder={props.chg.state.data.content.confpassword} aria-describedby="basic-addon2" autoComplete="off" className="form-control" />
				</div>
			</div>
			
			<div className="field-wrap">
				<div className="form-group input-group">
					<span id="basic-addon2" className="input-group-addon">
					<span className="glyphicon glyphicon-earphone"></span></span>
					<input type="number" name="phone" placeholder={props.chg.state.data.content.phone} aria-describedby="basic-addon2" autoComplete="off" className="form-control" />
				</div>
			</div>
			
			<div className="field-wrap">
				<div className="form-group input-group">
					<span id="basic-addon2" className="input-group-addon">
					<span className="glyphicon glyphicon-envelope"></span></span>
					<input type="text" name="email" placeholder={props.chg.state.data.content.email} aria-describedby="basic-addon2" autoComplete="off" className="form-control" />
				</div>
			</div>
			<div className="button-align-setting">
			<input type="button" className="button btn btn-danger button button-block" value={props.chg.state.data.content.getotp} onClick={props.chg.register}></input>
			</div>
		</div>
	);
}


export default Register;
