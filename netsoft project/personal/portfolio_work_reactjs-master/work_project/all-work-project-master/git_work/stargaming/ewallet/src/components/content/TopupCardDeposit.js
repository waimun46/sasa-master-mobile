import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";
import {Link} from 'react-router-dom';
import store from '../../store';


class TopupCardDeposit extends BaseComponent {
	
	constructor(props) {
		super(props);
		this.state = { 
			props: props,
			data: store.getContent()
		};
	}
	
  render() {
    return (
	<section id="TopupCardDeposit">
		<header className="header-same">
			<Link to="Deposit">
				<div className="back-btn">
					<i className="glyphicon glyphicon-menu-left"></i>
				</div>
			</Link>
			<div className="title-same">{this.state.data.content.topupcarddeposit}</div>						
		</header>
		<div className="form">
			<form role="form" action="/login" method="POST" name="login" accept-charset="UTF-8" enctype="application/x-www-form-urlencoded" autocomplete="off">
				<div className="field-wrap security_style">
				 <input style={{ height: '40px' ,textAlign: "center" ,fontSize:"13px"}} 
					type="password" name="password" placeholder={this.state.data.content.topupcardcode} 
					aria-describedby="basic-addon2" autocomplete="off" className="form-control" />
				</div>
				<Link to="History">
					<div className="button-align-setting">
						<button type="button" className="btn btn-danger button button-block">{this.state.data.content.topup}</button>
					</div>
				</Link>
			</form>			
		</div>
    </section>
    );
  }
}

export default TopupCardDeposit;

