import React, { Component } from 'react';
import BaseComponent from "./content/BaseComponent";

import { Avatar } from 'antd';
import { Row, Col } from 'antd';
import {Link} from 'react-router-dom';
import store from '../store';
import sha1 from "sha1";
import $ from "jquery";

class Menu extends BaseComponent {

	constructor(props) {
		super(props);
		this.state = { 
			props: props,
			data: store.getContent()
		};
	}

    componentDidMount () {
        let mysession = document.cookie.replace(/(?:(?:^|.*;\s*)session\s*\=\s*([^;]*).*$)|^.*$/, "$1");
        let params = {
            'action' : 'profile',
            'session': mysession,
        };

        let iv = this.state.data.public;
        let key = this.state.data.constant.key;

        params['signature'] = sha1(JSON.stringify(params).concat(key).concat(iv));
        params['iv'] = iv;

        $.post('http://elson.stargt.com.my/ewalletmember/index.php/Member/Profile', params,(result) => {
            if(result.status == 'ok' && result.data.username != '') {
                this.setState({
                    // what ever title
                    username: result.data.username,
                })
            }else{
                console.log(result.error);
            }
        });
    }
    //this line delete cookie while logout
    logOut =() => {
        document.cookie = "session=" + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    };

  render() {
    return (
<div className="menu-1">

          <input type="checkbox" id="offcanvas-menu" className="toggle" />

  	<div className="container">

        <aside className="menu-container">

	  		<div className="menu-heading clearfix">
	  			<label htmlFor="offcanvas-menu" className="close-btn">
	  				<i className="fa fa-times"></i>
	  			</label>
	  		</div>


            <div className="profile-logo">
                <Avatar  size="large" icon="user" />
                <li>{ this.state.username }</li>
            </div>

        <div className="menu-col-width hi-icon-wrap hi-icon-effect-1 hi-icon-effect-1a menu-position">
            <Row>
                <Col span={12}>
					
						<Col span={8} >
							<Link to="PersonalProfile" className="hi-icon hi-icon-user">
									<li>{this.state.data.content.personalprofile}</li>
							</Link>
						</Col>
					
                    <Col span={8}>
                        	<Link to="ChangePassword"  className="hi-icon hi-icon-locked">
                                <li>{this.state.data.content.changepassword}</li>
                            </Link>

                    </Col>
                    <Col span={8}>
                        	<Link to="ChangeSecurityCode" className="hi-icon hi-icon-pencil">
                                <li>{this.state.data.content.changesecuritycode}</li>
                            </Link>

                    </Col>
                    <Col span={8}>
                        	<Link to="Setting" className="hi-icon hi-icon-cog">
                                <li>{this.state.data.content.setting}</li>
                            </Link>

                    </Col>
                    <Col span={8}>
						
                        	<Link to="Languagelogin" className="hi-icon hi-icon-earth">
                                <li>{this.state.data.content.language}</li>
                            </Link>

                    </Col>
                    <Col span={8}>
                        	<Link to="Bank" className="hi-icon hi-icon-bookmark">
                                <li>{this.state.data.content.bank}</li>
                            </Link>

                    </Col>
                </Col>
            </Row>
        </div>



		<Link to="/">
			<div className="btn-logout" onClick={ this.logOut }>
				<i className="fa fa-sign-out" aria-hidden="true"></i>
				<span>{this.state.data.content.logout}</span>			
			</div>
		</Link>



    	</aside>

		<section className="content-menu">

			<label htmlFor="offcanvas-menu" className="full-screen-close"></label>

			<div className="menu">
                <div className="back-1 trans-back"><i className="glyphicon glyphicon-menu-left"></i></div>
                <div className="title-1">{this.state.data.content.home}</div>
                <div className="icon-1">
				<label htmlFor="offcanvas-menu" className="toggle-btn">
		        	<i className="glyphicon glyphicon-th-list"></i>
		        </label>
                </div>
		    </div>





	  	</section>

  	</div>












</div>







    );
  }
}

export default Menu;
