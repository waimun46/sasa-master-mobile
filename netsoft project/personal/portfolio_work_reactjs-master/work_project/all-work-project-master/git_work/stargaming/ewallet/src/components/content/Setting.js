import React, { Component } from 'react';
import BaseComponent, {CollectionCreateForm} from "./BaseComponent";
import { Avatar, Row, Col, Input } from 'antd';
import {Link} from 'react-router-dom';
import store from '../../store';
import {PostData} from "../services/PostData";
import $ from "jquery";
import Verify from './Verify';

class Setting extends BaseComponent {
	
	constructor(props) {
		super(props);
		this.state = { 
			props: props,
			data: store.getContent(),
			cookit: document.cookie,
            username : '',
            pertransferamountlimit : 0,
            perdaytransferlimit : 0,
            minimumsecuritycodeamount : 0,
		};

        this.handleInputChange = this.handleInputChange.bind(this);
	}

    componentDidMount = () => {

        let session = this.state.cookit.replace(/(?:(?:^|.*;\s*)session\s*\=\s*([^;]*).*$)|^.*$/, "$1");
        let params = {
            'action': 'profile',
            'session': session
        };
        console.log(params);
        PostData('Profile',params ,this.props).then((result) => {
            if(result.status == 'ok' && result.data != ''){
                this.setState({
                    username: result.data.username,
				})
            }else{
                this.setState({
                    validationerror: this.state.data.content[result.error]
                });
            }
        });

        params = {
            'action': 'getSetting',
            'session': session
        };
        console.log(params);
        PostData('Setting',params ,this.props).then((result) => {
            if(result.status == 'ok'){
                this.setState({
					pertransferamountlimit :result.data.maxAmtPerTrx,
                    perdaytransferlimit : result.data.maxAmtPerDay,
                    minimumsecuritycodeamount : result.data.minAmtForCode,

                });
            }else{
                this.setState({
                    validationerror: this.state.data.content[result.error]
                });
            }
        });
        
    }

    submit = () => {
        let session = document.cookie.replace(/(?:(?:^|.*;\s*)session\s*\=\s*([^;]*).*$)|^.*$/, "$1");
        let pertransferamountlimit = $('input[name="pertransferamountlimit"]').val();
        let perdaytransferlimit = $('input[name="perdaytransferlimit"]').val();
        let minimumsecuritycodeamount = $('input[name="minimumsecuritycodeamount"]').val();
        let params = {
            'action': 'updateSetting',
            'setting' : {'maxAmtPerTrx':pertransferamountlimit,'maxAmtPerDay':perdaytransferlimit,'minAmtForCode':minimumsecuritycodeamount},
            'session': session
        };

        PostData('Setting',params ,this.props).then((result) => {
            console.log(result);
            if(result.status == 'ok'){
                console.log(result);
                this.setState({
                    pertransferamountlimit :result.data.maxAmtPerTrx,
                    perdaytransferlimit : result.data.maxAmtPerDay,
                    minimumsecuritycodeamount : result.data.minAmtForCode,
                    prefix : result.data.prefix,
                });
            }else{
            	this.setState({
                    validationerror: this.state.data.content[result.error]
                });
            }
        });

    }

    verifysend = () => {

        let session = document.cookie.replace(/(?:(?:^|.*;\s*)session\s*\=\s*([^;]*).*$)|^.*$/, "$1");
        let prefix = this.state.prefix;
        let pertransferamountlimit = this.state.pertransferamountlimit;
        let perdaytransferlimit = this.state.perdaytransferlimit;
        let minimumsecuritycodeamount = this.state.minimumsecuritycodeamount;
        let password = $('input[name="password"]').val();

        let params = {
            'action': 'updateSettingVerify',
            'setting' : {'maxAmtPerTrx':pertransferamountlimit,'maxAmtPerDay':perdaytransferlimit,'minAmtForCode':minimumsecuritycodeamount},
            'otp': prefix + '-' + password,
			'session': session
        };
        console.log(params);
        PostData('Setting', params, this.props).then((result) => {
            console.log(result);
            if(result.status == 'ok') {
            	this.setState({
    				successVisible : true,
                })
            }else{
                this.setState({
                    //validationerror: result.error
                    validationerror: this.state.data.content[result.error]
                });
            }
        });
    }

    resendverifycode = () => {
        let session = document.cookie.replace(/(?:(?:^|.*;\s*)session\s*\=\s*([^;]*).*$)|^.*$/, "$1");
        let params = {
            'action': 'resendVerify',
            'verifyAction': 'updateSetting',
            'session': session
        };
        console.log(params);
        PostData('Setting', params, this.props).then((result) => {
            console.log(result);
            if(result.status == 'ok') {
                this.setState({
                    prefix : result.data.prefix,
                });
                this.showModal();
            }else{
                this.setState({
                    //validationerror: result.error
                    validationerror: this.state.data.content[result.error]
                });
            }
        });

    }

    successChangeSetting = () => {
		this.setState({
				validationerror : '',
				successVisible : false,
				visible : false,
        })
        
        this.props.history.push('/Home');
	}
    render() {console.log(this.state.pertransferamountlimit);
    return (
        this.state.prefix ? 
            <div>
                <Verify prefix={this.state.prefix}
                    pertransferamountlimit={this.state.pertransferamountlimit}
                    perdaytransferlimit={this.state.perdaytransferlimit}
                    minimumsecuritycodeamount={this.state.minimumsecuritycodeamount}
                    validationerror={this.state.validationerror}
                    onClick={this.verifysend}
                    visible={this.state.visible}
                    onReSendVerify={this.resendverifycode}
                    handleCancel={this.handleCancel}
                  />
                
                <CollectionCreateForm
                    visible={this.state.successVisible}
                    onCancel={this.successChangeSetting}
                    onCreate={this.successChangeSetting}
                    btnLabel="OK"
                    content="Info had Changed Successfully."
                />
                
              </div>
        :
	<section id="Setting">
		<header className="header-same">
			<Link to="Home">
				<div className="back-btn">
					<i className="glyphicon glyphicon-menu-left"></i>
				</div>
			</Link>
			<div className="title-same">{this.state.data.content.setting}</div>
		</header>
		<div className="profile-top_personal2">					
					<div className=" profile-top-right_personal">
						<div className="profile-logo">
							<Avatar  size="large" icon="user" />
							<li>{this.state.username}</li>
						</div>						
					</div>
		</div>
			<div className="register">
					<div className="">
						<laber className="text-danger">{this.state.validationerror}</laber>
					</div>
					<form>

                        <Row className="profile-from">
                            <Col span={12}>
                                <span className="username-style-color">{this.state.data.content.pertransferamountlimit} :</span>
                            </Col>
                            <Col span={12}>
                                <Input name="pertransferamountlimit" onChange={this.handleInputChange}  value={this.state.pertransferamountlimit} placeholder={this.state.pertransferamountlimit} / >
                            </Col>
                        </Row>

                        <Row className="profile-from">
                            <Col span={12}>
                                <span className="username-style-color">{this.state.data.content.perdaytransferlimit} :</span>
                            </Col>
                            <Col span={12}>
                                <Input name="perdaytransferlimit" onChange={this.handleInputChange}  value={this.state.perdaytransferlimit} placeholder={this.state.perdaytransferlimit} / >
                            </Col>
                        </Row>

                        <Row className="profile-from">
                            <Col span={12}>
                                <span className="username-style-color">{this.state.data.content.minimumsecuritycodeamount} :</span>
                            </Col>
                            <Col span={12}>
                                <Input name="minimumsecuritycodeamount" onChange={this.handleInputChange}  value={this.state.minimumsecuritycodeamount} placeholder={this.state.minimumsecuritycodeamount} / >
                            </Col>
                        </Row>

						<div className="button-align-setting">
							<button type="button" className="btn btn-danger button button-block" onClick={this.submit}>{this.state.data.content.updatesetting}</button>
                        </div>
					</form>
				</div>	
		
    </section>
    );
  }
}

export default Setting;

