import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";

import {Link} from 'react-router-dom';
import store from '../../store';

class BillPayment extends BaseComponent {
	
	constructor(props) {
		super(props);
		this.state = { 
			props: props,
			data: store.getContent()
		};
	}
	
  render() {
    return (
	
	<section id="BillPayment" className="same-form-style">
		<header className="header-same">
			<Link to="PayBill">
				<div className="back-btn">
					<i className="glyphicon glyphicon-menu-left"></i>
				</div>
			</Link>
			<div className="title-same">
				{this.state.data.content.billpayment}
			</div>
		</header>

		<div className="form">
			<form role="form" action="/login" method="POST" name="login" accept-charset="UTF-8" 
				enctype="application/x-www-form-urlencoded" autocomplete="off">
				<div className="field-wrap input_width">
					<ul>
						<li>{this.state.data.content.payto} :</li>
					</ul>
					<div className="span-style-text">
						<span>Tenaga Nasional</span>
					</div>
				</div>

				<div className="field-wrap input_width">
					<ul>
						<li>{this.state.data.content.companycodeid}/ID :</li>
					</ul>
					<div className="span-style-text">
						<span>88888888</span>
					</div>
				</div>

				<div className="field-wrap input_width">
					<ul>
						<li>{this.state.data.content.accountno} :</li>
					</ul>
					<input type="text" name="text"  aria-describedby="basic-addon2" autoComplete="off" className="form-control" />
				</div>

				<div className="field-wrap input_width">
					<ul>
						<li>{this.state.data.content.invoiceno}/<br/>
							{this.state.data.content.billno}:</li>
					</ul>
					<input type="text" name="text"  aria-describedby="basic-addon2" autocomplete="off" className="form-control" />
				</div>

				<div className="field-wrap input_width">
					<ul>
						<li>{this.state.data.content.amount} :</li>
					</ul>
					<input type="text" name="text"  aria-describedby="basic-addon2" autocomplete="off" className="form-control" />
				</div>
			</form>
			<div className="button-align-setting">
				<Link to="ConfirmPaymentBill">
					<button type="button" className="btn btn-danger button button-block" onclick="logIn()">{this.state.data.content.submit}</button>
				</Link>
			</div>
		</div>
	</section>
    );
  }
}

export default BillPayment;

