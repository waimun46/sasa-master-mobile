import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";

import {Link} from 'react-router-dom';
import store from '../../store';

class ConfirmPaymentBill extends BaseComponent {
	
	constructor(props) {
		super(props);
		this.state = { 
			props: props,
			data: store.getContent()
		};
	}
	
  render() {
    return (
	
	<section id="ConfirmPaymentBill" className="same-form-style">
		<header className="header-same">
			<Link to="BillPayment">
				<div className="back-btn">
					<i className="glyphicon glyphicon-menu-left"></i>
				</div>
			</Link>
			<div className="title-same">
				{this.state.data.content.confirmpayment}
			</div>
		</header>

		
		<div className="chart">
			<h3>{this.state.data.content.totalpayment}</h3>
			<div className="diagram">
				<h4>50.00</h4>
			</div>

			<div className="chart_list">
				<ul>
					<li className="red  chart-p"> 
						{this.state.data.content.payto} :
						<p className="percentage">Tenaga Nasional</p>
					</li>
					<li className="purple">
						{this.state.data.content.companycodeid}/ID :
						<p className="percentage">88888888</p>
					</li>
					
					<div className="clear"></div>
				</ul>
			</div>
			<div className="chart_list">
				<ul>
					<li className="yellow  chart-p">
						{this.state.data.content.accountno} :
						<p className="percentage">B3333333</p>
					</li>
					<li className="blue">
						{this.state.data.content.invoiceno}/
							{this.state.data.content.billno}:
						<p className="percentage">A2222222</p>
					</li>
					<div className="clear"></div>
				</ul>
			</div>
			
		</div>
		
		<div className="button-align-setting">
			<Link to="Security">
				<button type="button" className="btn btn-danger button button-block" onclick="logIn()">{this.state.data.content.submit}</button>
			</Link>
		</div>
		
		
	</section>
    );
  }
}

export default ConfirmPaymentBill;

