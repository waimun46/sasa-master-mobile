
import sha1 from 'sha1';
import store from '../../store';
import {Ksort} from './MyFunctions';

export function PostData(action, params, props)
{
	
	let BaseUrl = process._root;

	return new Promise((resolve, reject) => {
		let data = store.getContent();
		let iv = data.public;
		let key = data.constant.key;
		
		params = Ksort(params);
        console.log(JSON.stringify(params).concat(key).concat(iv));
		params['signature'] = sha1(JSON.stringify(params).concat(key).concat(iv));
		params['iv'] = iv;

		fetch(BaseUrl+action,{
			method : 'POST',
			body: JSON.stringify(params)
		})
		.then((response) => response.json())
		.then((responseJson) => {
			console.log(responseJson);
			
			if(responseJson.status == 'error' && responseJson.error == 'session_expired')
			{
				props.history.push('/');
			}
			else
			{
				resolve(responseJson);
			}
			
		})
		.catch((error) => {
			console.log(error);
			reject(error);
			
		});
	});
}