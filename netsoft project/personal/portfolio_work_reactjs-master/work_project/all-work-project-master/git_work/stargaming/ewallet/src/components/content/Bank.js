import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";
import {Link} from 'react-router-dom';
import store from '../../store';
import { DatePicker, Select, Button, Radio } from 'antd';
import {PostData} from "../services/PostData";
import EditBank from './EditBank';

class Bank extends BaseComponent {

	constructor(props) {
		super(props);
		this.state = { 
		props: props,
		data: store.getContent(),
		cookit: document.cookie,
		list:[],
        id: '',
		};

        console.log('a',this.state);
	}

    handleRowClick(Id){
        let session = this.state.cookit.replace(/(?:(?:^|.*;\s*)session\s*\=\s*([^;]*).*$)|^.*$/, "$1");
        let params = {
            'action' : 'editBankAccount',
            'id':Id,
            'session' : session
        };
        console.log(params);
        PostData('BankAccount',params ,this.props).then((result) => {
            if(result.status == 'ok' && result.data != '')
            {
                this.setState({
                    id:Id,
                })
            } else {
                this.setState({
                    validationerror: this.state.data.content[result.error]
                });
            }
        });
	}

    renderItem(item) {
        const clickCallback = () => this.handleRowClick(item.id);
        const itemRows = [
            <tr class="ant-table-row  ant-table-row-level-0" key={"row-data-" + item.id}>
                <td>{item.bankname}</td>
                <td>{item.holdername}</td>
                <td>{item.account}</td>
                <td onClick={clickCallback}><Button type="primary">{this.state.data.content.edit}</Button></td>
            </tr>
        ];

        return itemRows;
    }

	componentDidMount = () => {
		let session = this.state.cookit.replace(/(?:(?:^|.*;\s*)session\s*\=\s*([^;]*).*$)|^.*$/, "$1");
		let params = {
		'action' : 'getBankAccountList',
		'session' : session
		};
        console.log(params);
		PostData('BankAccount',params ,this.props).then((result) => {
			if(result.status == 'ok' && result.data != '')
			{
                this.setState({
                    list: result.data.list,
                })
		    } else {
                this.setState({
                    validationerror: this.state.data.content[result.error]
                });
            }
        });
    }

    backtoup = () => {
        this.setState({
            id: '',
        })
    }

	render() {

        let ALLItemList  = [];
        this.state.list.forEach(item => {
            const perItemRows = this.renderItem(item);
            ALLItemList = ALLItemList.concat(perItemRows);
        });

		return (
            this.state.id ? <EditBank props={this.state.props} id={this.state.id} onBacktoup={this.backtoup}/> :
		    <section id="Bank">
                <header className="header-same">
                    <Link to="Home">
                        <div className="back-btn">
                            <i className="glyphicon glyphicon-menu-left"></i>
                        </div>
                    </Link>
				    <div className="title-same">{this.state.data.content.bank}</div>
			   </header>
			   
			   <div className="form">
                   <div className="table_style">
                        <table className="form_wrapper">
                            <thead className="ant-table-thead">
                                <tr>
                                    <th class=""><span>{this.state.data.content.bank}</span></th>
                                    <th class=""><span>{this.state.data.content.holdername}</span></th>
                                    <th class=""><span>{this.state.data.content.accountno}</span></th>
                                    <th class=""><span>{this.state.data.content.action}</span></th>
                                </tr>
                            </thead>
                            <tbody className="ant-table-tbody">
                                    {ALLItemList}
                            </tbody>
                        </table>
                   </div>
                    <div className="button-align-setting">
                        <Link to="AddBank">
                            <button type="button" className="btn btn-danger button button-block button_margintop">{this.state.data.content.addbank}</button>
                        </Link>
                    </div>
               </div>
            </section>
        );
	}
}
export default Bank;