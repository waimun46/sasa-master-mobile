import React, { Component } from 'react';
import BaseComponent from "./content/BaseComponent";
import ReactDOM from 'react-dom';


class Table extends BaseComponent {

    constructor(){
        super();
        this.state={
            data:[],
        }
    }

    componentDidMount(){
        return fetch('https://facebook.github.io/react-native/movies.json')
        .then((response)=>response.json())
        .then((responseJson)=>
            {
                this.setState({
                    data:responseJson.movies
                })
                console.log(this.state.data)
            })
    }

  render() {
    return (
        <div>

            <table>
                <thead>
                    <tr>
                        <th>title</th>
                        <th>releaseYear</th>
                    </tr>
                </thead>
                {
                    this.state.data.map((myData,key) =>
                    <tr>
                        <td>{myData.title}</td>
                        <td>{myData.releaseYear}</td>
                    </tr>
                    )
                }
            </table>

        </div>
    );
  }
}

export default Table;
