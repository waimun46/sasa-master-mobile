import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";

import {Link} from 'react-router-dom';
import { Avatar } from 'antd';
import { Rate } from 'antd';
import store from '../../store';
import { Select } from 'antd';
const Option = Select.Option;

class LearnMore extends BaseComponent {
	 state = {
     value: 1,
  }
  onChange = (e) => {
    console.log('radio checked', e.target.value);
    this.setState({
      value: e.target.value,
    });
  }
	constructor(props) {
		super(props);
		this.state = { 
			props: props,
			data: store.getContent()
		};
	}
 
 render() {
    return (
	
<section id="LearnMore">

	<header className="header-same">
		<Link to="Lend">
			<div className="back-btn">
				<i className="glyphicon glyphicon-menu-left"></i>
			</div>
		</Link>
		<div className="title-same">
			{this.state.data.content.learnmore}
		</div>
	</header>
	
    <div className="form">
        
        <div className="icon_style">
            <Avatar size="large" icon="user" />
            <h4 className="icon_textstyle">John</h4>
        </div>

        <div className="table">
            <div className="table-cell lern-title">
                <span>{this.state.data.content.country}  :</span>
            </div>
            <div className="table-cell">
                <span>Malaysis</span>
            </div>
            <div className="table-cell lern-title">
                <span>{this.state.data.content.story} :</span>
            </div>
            <div className="table-cell">
                <span>John is 39 years old.he lives with her family. he has two children.</span>
            </div>
            <div className="table-cell lern-title">
                <span>{this.state.data.content.aboutloan} :</span>
            </div>
            <div className="table-cell">
                <span>The loan plan RM XXX for what, RM XXX for what.....</span>
            </div>
            <div className="table-cell lern-title">
                <span>{this.state.data.content.theloanisspecialbacause} :</span>
            </div>
            <div className="table-cell">
                <span>it supports the well being of women and their families a discounted interest rate.</span>
            </div>
            <div className="table-cell lern-title">
                <span>{this.state.data.content.loanlength} :</span>
            </div>
            <div className="table-cell">
                <span>14 months</span>
            </div>
            <div className="table-cell lern-title">
                <span>{this.state.data.content.disburseddate} :</span>
            </div>
            <div className="table-cell">
                <span>August 21,2017</span>
            </div>
            <div className="table-cell lern-title">
                <span>{this.state.data.content.currencyexchangeloss} :</span>
            </div>
            <div className="table-cell">
                <span>Passible</span>
            </div>
            <div className="table-cell lern-title">
                <span>{this.state.data.content.facilitatedbyfieldpartner} :</span>
            </div>
            <div className="table-cell">
                <span>Arvand is borrower paying interest? Yes</span>
            </div>
            <div className="table-cell lern-title">
                <span>{this.state.data.content.fieldpartnerriskrating} :</span>
            </div>
            <div className="table-cell">
                <Rate disabled defaultValue={5} />
            </div>
        </div>
        <div className="select_margin select_style">
            <Select className="select_style" placeholder="RM 25" >
                <Option value="RM 50">RM 50</Option>
                <Option value="RM 100">RM 100</Option>
                <Option value="RM 500">RM 500</Option>
            </Select>
			
		</div>
		
		<div className="button-align-setting">
            <Link to="ConfirmPayment">
				<button type="button" className="btn btn-danger button button-block btn_bottom">{this.state.data.content.lendnow}</button>
            </Link>
		
		</div>
    </div>
</section>
    );
  }
}

export default LearnMore;

