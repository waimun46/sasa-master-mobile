import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";
import ReactDOM  from 'react-dom';
import {Link} from 'react-router-dom';
import store from '../../store';
import SampleBarcode from '../../include/images/SampleBarcode.PNG';
import {PostData} from "../services/PostData";

class MyWallet extends BaseComponent {
	
	constructor(props) {
		super(props);
		this.state = { 
			props: props,
			data: store.getContent(),
            cookit: document.cookie,
            qrcode : '',
		};
	}

    componentDidMount = () => {

        let session = this.state.cookit.replace(/(?:(?:^|.*;\s*)session\s*\=\s*([^;]*).*$)|^.*$/, "$1");
        let params = {
            'action': 'getBalance',
            'session': session
        };
        console.log(params);
        PostData('Wallet',params ,this.props).then((result) => {
            console.log(result);
            if(result.status == 'ok' && result.data.qrcode!=''){
                console.log(result);
                this.setState({
                    qrcode: result.data.qrcode,
                })
                console.log(result);
            }else{
                this.setState({
                    validationerror: this.state.data.content[result.error]
                });
            }
        });
    }

  render() {
    return (
	<section id="MyWallet">

		<header className="header-same">
			<Link to="Home">
				<div className="back-btn">
					<i className="glyphicon glyphicon-menu-left"></i>
				</div>
			</Link>
			<div className="title-same">{this.state.data.content.mywallet}</div>
		</header>

		<div className="form">

			<div className="figure_style">
				<div className="qr-codetitle-style">
					<header className="select-top">
						<i className="glyphicon glyphicon-qrcode"></i>
						<span>{this.state.data.content.myqrcode}</span>
					</header>
				</div>
				<figure className="qr"><img src={"data:image/png;base64, "+this.state.qrcode} /></figure>
			</div>

		</div>
	</section>
    );
  }
}

export default MyWallet;

