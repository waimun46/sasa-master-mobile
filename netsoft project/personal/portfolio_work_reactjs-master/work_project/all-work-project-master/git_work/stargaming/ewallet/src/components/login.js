import React, { Component } from 'react';
import BaseComponent from "./content/BaseComponent";
import ReactDOM from 'react-dom';
import {Link} from 'react-router-dom';
import {PostData} from './services/PostData';

//import blue from '../include/images/payment.png';
import payment from '../include/images/payment.png';
//import validateInput from './validation/loginpage';
import axios from 'axios';
import $ from 'jquery';
import sha1 from 'sha1';
import store from '../store';
import actions from '../actions';

class Login extends BaseComponent {

	constructor(props) { 
			super(props);
			this.state = { 
			props: props,
			data: store.getContent(),
		};
	}

	componentDidMount () {
        console.log(process._root);


        document.cookie = "session=" + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
	};

	 login = () => {

		  let params = {
		   'password': $('input[name="password"]').val(),
		   'username': $('input[name="username"]').val(),
		  };
		  
		  PostData('Login', params, this.props).then((result) => {
			  console.log(result);
			  if(result.status == 'ok' && result.data.session != '') {
				  //actions.switchLanguage(this.state.data.lang, result.session);
				  document.cookie = "session="+result.data.session ;
				  this.props.history.push('/Home');
			  }else{
				  this.setState({
					  //validationerror: result.error
					  validationerror: this.state.data.content[result.error]
				  });
				  console.log(result);
			  }
		  });
		  
	 }

    render() {
	  //const {errors, identifier, password, isLoading} = this.state;
    return (
           <section id="login">

			<div className="content">
				<div id="form_wrapper" className="form_wrapper">

					<form className="login active " onSubmit={this.onSubmit}>
						<div className="img-login">
						
                           <img src={payment}/>
                            <div className="img-paymenttext">
                                <p></p>
                            </div>
                        </div>
                        <div className="login-error">
                            <laber className="text-danger" >{this.state.validationerror}</laber>
                        </div>
					<div className="form-group input-group border-login">
                        <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                        <input className="form-control boder-input" type="text" name='username' placeholder={this.state.data.content.username}/>
                    </div>
				 				
                    <div className="form-group input-group border-login">
                        <span className="input-group-addon">
							<i className="glyphicon glyphicon-lock"></i>
						</span>
                        <input className="form-control boder-input" type="password" name='password' placeholder={this.state.data.content.password}/>
                    </div>
					
                    <div className="login-btn">
						<input type="button" value={this.state.data.content.login} onClick={this.login}></input>
                    </div>
				
					
                    	<div className="bottom">
							<div className="remember">
								<span>
									<Link className="forget-password" to="./ForgetPassword">
									{this.state.data.content.forgetpassword} ?
									</Link>
								</span>
							</div>

                            <div className="remember">
							<Link className="linkform" to="./Register">
							    {this.state.data.content.signup} ? {this.state.data.content.clickhere}
							</Link>
                            </div>
                        <div className="clear"></div>
						</div>
					</form>

				</div>
				<div className="clear"></div>
			</div>
			<Link className="back login-lang" to="./Language">
				
					<i className="glyphicon glyphicon-globe"></i>
					<span>{this.state.data.content.language}</span>
				
			</Link>
    	</section>
    );
  }
}

export default Login;
