import React, { Component } from 'react';
import BaseComponent from "./BaseComponent";

import { Link } from 'react-router-dom';
import $ from "jquery";
import store from "../../store";
import sha1 from "sha1";

class VerifyForgetPassword extends BaseComponent {

    constructor(props) {
        super(props);
        this.state = {
            props: props,
            data: store.getContent(),
        };
    }

    componentDidMount() {

    }

    verifysend = () => {
        if (typeof this.props.onClick === 'function') {
            this.props.onClick();
        }
    }

    render() {

        return (

            <section style={{display:this.props.display?"none":"block"}} id="VerifyForgetPassword">

                <header className="header-same">

                    <div className="title-same">Verify</div>
                </header>

                <div className="form">
                    
                    <form role="form" action="/login" method="POST" name="login">
                    	<div className="login-error field-wrap">
                    		<label className="text-danger" >{this.props.validationerror}</label>
                    	</div>
                
                        <div className="field-wrap security_style">
                            <input style={{ height: '40px', textAlign: "center", fontSize: "16px" }} type="password" name="password" placeholder=" *  *  *  *  *  *" aria-describedby="basic-addon2" autocomplete="off" className="form-control" />
                        </div>

                        <div className="button-align-setting">
                            <button type="button" className="btn btn-danger button button-block" onClick={this.verifysend}>VERIFY</button>
                        </div>

                    </form>
                </div>
            </section>
        );
    }
}

export default VerifyForgetPassword;

