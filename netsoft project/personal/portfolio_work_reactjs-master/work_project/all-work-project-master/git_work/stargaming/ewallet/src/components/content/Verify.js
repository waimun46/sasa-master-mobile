import React, { Component } from 'react';
import BaseComponent, {CollectionCreateForm} from "./BaseComponent";
import {Link} from 'react-router-dom';
import $ from "jquery";
import store from "../../store";
import {PostData} from "../services/PostData";

	
class Verify extends BaseComponent {

	constructor(props) {
        super(props);
        this.state = {
			props: props,
			cookit: document.cookie,
			data: store.getContent(),
			prefix:'',
		};

        console.log(this.state.data);
	    console.log(this.state.props);

	}

	componentDidMount() {
		
	}

	

	
	verifysend = () => {
		if (typeof this.props.onClick === 'function') {
			this.props.onClick();
		}
	}

    resendverifycode = () => {
        if (typeof this.props.onReSendVerify === 'function') {
            this.props.onReSendVerify();
        }
    }

  	render() {
   		return (
			<section id="Verify">

				<header className="header-same">

					<div className="title-same">Verify</div>
				</header>

				<div className="form">
					<form>
						
						
						<div className="login-error field-wrap">
							<label className="text-danger" >{this.props.validationerror}</label>
						</div>
						
						<p className="forgot3">
							{this.state.data.content.prefix} : {this.props.prefix}
						</p>
						<div className="field-wrap security_style">
							<input  style={{ height: '40px' ,textAlign: "center" ,fontSize:"16px"}} type="password" name="password" placeholder=" *  *  *  *  *  *" aria-describedby="basic-addon2" className="form-control" />
						</div>

						<div className="button-align-setting">
							<button type="button" className="btn btn-danger button button-block" onClick={this.verifysend}>VERIFY</button>
						</div>
						
						
						
						
						
					
					
					<a className="forgot2" type="primary" onClick={this.resendverifycode}>{this.state.data.content.resendverifycode} ?</a>
					<CollectionCreateForm
					  visible={this.props.visible}
					  onCancel={this.props.handleCancel}
					  onCreate={this.props.handleCancel}
					  btnLabel="OK"
					  content="The Verify Code Had Sent."
					/>

					</form>
				</div>
			 </section>
    	);
  	}
}

export default Verify;

