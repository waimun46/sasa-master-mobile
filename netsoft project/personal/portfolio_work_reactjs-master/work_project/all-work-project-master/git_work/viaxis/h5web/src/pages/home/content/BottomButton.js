import React, {Component} from 'react';
import { Row, Col, Icon } from 'antd';
import Link from 'umi/link';

import home from '../../../assets/images/icon/home.png';
import prize from '../../../assets/images/icon/prize.png';
import gift from '../../../assets/images/icon/gift.png';
import user from '../../../assets/images/icon/user.png';

class BottomButton extends Component {

  render() {
    return (
      <div className="bottom-button">
        <Row>
          <Col span={6}>
            <img src={home} alt="home"/>
            <p>首页</p>
          </Col>   
          <Link to="/priceHall">
          <Col span={6}>
            <img src={prize} alt="prize"/>
            <p>开奖大厅</p>
          </Col>
          
          </Link>
          
          <Link to="/promotion">
            <Col span={6}>
              <img src={gift} alt="gift"/>
              <p>优惠活动</p>
            </Col>
          </Link>
          <Link to="/personal_information">
            <Col span={6}>
              <img src={user} alt="user"/>
              <p>我的</p>
            </Col>
          </Link>
        </Row>
      </div>
    );
  }
}

export default BottomButton;
