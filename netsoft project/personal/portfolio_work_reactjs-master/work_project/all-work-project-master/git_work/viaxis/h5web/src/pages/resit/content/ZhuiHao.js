import React, {Component} from 'react';
import { connect } from 'dva';
import {Timeline, Icon, Checkbox, notification } from 'antd';
import {Modal, Toast} from 'antd-mobile';
import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';
import * as BetHelper from "@/utils/bet_helper";
import router from 'umi/router';
import * as Constants from "../../../app_constants";
//import $ from 'jquery';
//import { isConstructorDeclaration } from 'typescript';

const alert = Modal.alert;

function onChange(e) {
  console.log(`checked = ${e.target.checked}`);
}

class ZhuiHao extends Page {
title = "智能追号"

constructor(props) {
  super(props);
  this.state = {
    multiple: 1,
    multiples:{},
    stopOnWin : false,
    numOfQh : 1,
    displayUnOpened:[],
    ltotalBetCount:0,
    ltotalBetAmount:0,
    lfinalBetAmount:0,
    lCurrentQh:0
  }
}


  /*  input 加减   */
  componentDidMount(){
    const { totalBetCount, totalBetAmount } = this.props.betCart;

    this.setState({ltotalBetCount:totalBetCount});
    this.setState({ltotalBetAmount:totalBetAmount});
    this.setState({lfinalBetAmount:totalBetAmount});

     
  }

  componentWillMount(){
    const {current,unopen,group} = this.props;
    if (!unopen || !unopen[group])
      this.props.dispatch({ type: "gamedata/getGameDataUnopen", payload: current.Group });
   
    let multiples = {};
    multiples[this.props.current.Qh] = 1;
    multiples = Object.assign(this.state.multiples, {}, multiples)
    this.setState({multiples});
    this.setState({lCurrentQh: this.props.current.Qh});
}

minusQh(){
  let currentNumOfQh = this.state.numOfQh;
  if(currentNumOfQh - 1 < 1)
    return;

    let currQh = this.state.lCurrentQh + currentNumOfQh -1;
    let multiples = this.state.multiples;
    delete multiples[currQh];
    Object.assign(this.state.multiples, this.state.multiples, multiples)


  
  this.setState({numOfQh: currentNumOfQh-1});
}

plusQh(){
  let currentNumOfQh = this.state.numOfQh;

  this.setState({numOfQh: currentNumOfQh + 1})

  let currQh = this.state.lCurrentQh + currentNumOfQh;
  let multiples = {};
    multiples[currQh] = 1;
    multiples = Object.assign(this.state.multiples, this.state.multiples, multiples)
    this.setState({multiples});


}

onMultipleAdd(qh){
  let currentMultiple = this.state.multiples[qh] || this.state.multiple;
  
  var multiples = {};
  multiples[qh] = currentMultiple+1;
  multiples = Object.assign(this.state.multiples,{},multiples);
  this.setState({multiples});

  let tbetAmount = this.state.ltotalBetAmount;
  tbetAmount += tbetAmount * (currentMultiple+1);
  this.setState({lfinalBetAmount:tbetAmount});

}

onMultipleMinus(qh){
  let currentMultiple = this.state.multiples[qh] || this.state.multiple;
  if(currentMultiple < 2)
    return;

    var multiples = {};
    multiples[qh] = currentMultiple-1;
    Object.assign(this.state.multiples,{},multiples);
    this.setState({multiples});

}

onNumberOfQhChange(e){

  var val = e.target.value;
  if (/^\d+$/.test(val))
      this.setState({ numOfQh: parseInt(val) });
      
  else if (val == "")
      this.setState({ numOfQh: 0 });

}

onCheckBoxChange(e){
  this.setState({stopOnWin:e.target.checked});
}

getLocalTotalBetAmount(){
  const {multiples} = this.state;

  
  return  this.sum(multiples) * this.state.ltotalBetAmount;
}

get turnover() {
  const { isTrace, traceData, totalBetAmount } = this.props.betCart;

  if (!isTrace || !traceData) return totalBetAmount

  let turnover = 0;
  traceData.forEach(it => turnover += it.multiple * totalBetAmount);
  return turnover;
}

submit(){
  const {numOfQh,multiples,multiple,stopOnWin} = this.state;

  const { unopen } = this.props;
  
  let filteredUnopen = (unopen.length > 0) && unopen.slice(0,this.state.numOfQh);

  const selectedRowKeys = unopen.slice(0,this.state.numOfQh).map(it=>it.Qh);

        const traceData = selectedRowKeys.map(qh => {
            return {
                qh,
                multiple: multiples[qh] || multiple
            };
        });

        this.props.dispatch({type: 'betCart/updateTrace', payload : {traceData, stopOnWin}});

        const { betNums, totalBetCount, isTrace, traceStopOnWin } = this.props.betCart;
        const turnover = this.getLocalTotalBetAmount();

        if (totalBetCount <= 0 || turnover <= 0) {
          Toast.info('请您先添加有效的投注计划!',2);
            //message.warning('请您先添加有效的投注计划!');
          return;
        }

        let msg = `您共选择了${betNums.length || 0} 笔, 共${totalBetCount}注，共￥${turnover}元`;
        if (isTrace) {
            msg = `您共选择了${betNums.length || 0} 笔, 共${totalBetCount}注，追${Object.keys(traceData).length}期，共￥${turnover}元`
        }

        alert('投注需知', '一旦接受投注，不可撤回，您确定吗?', [
          { text: 'Cancel', onPress: () => console.log('cancel') },
          { text: 'Ok', onPress: () => {this.bet.bind(this); this.bet();} },
        ])

        // Modal.confirm({
        //     iconType: "exclamation-circle",
        //     title: msg,
        //     content: "一旦接受投注，不可撤回，您确定吗?",
        //     onOk: this.bet.bind(this)
        // });
}


bet() {
  var { betNums, traceData, traceStopOnWin } = this.props.betCart;
  var qh = this.props.current.Qh;
  var data = [];

  betNums.map((item,i)=>{

    let betItem = BetHelper.buildBetNumData(item.gameType, qh, item.wager, item.betNum, item.unitPos, traceData, traceStopOnWin);
      data.push(betItem);

  });
  
  this.setState({ betting: true });
  this.props.dispatch({ type: "dealinfo/bet", payload: data }).then(it => {
      this.setState({ betting: false, isTrace: false });
      var resp = it;
      if (resp.code == 0) {
        
        let successMsg = '投注成功,共接受' + resp.data.length + '笔';

        Toast.success(successMsg, 2);
      } else {
          var msg = Constants.ERROR_CODES.BET[resp.code] || resp.message || "未知的错误";
          alert('投注失败', msg, [
            { text: 'Ok', onPress: () => console.log('error') },
          ])
      }
      this.resetBetNum();
     // router.push("/bet_history");
     


  });

}

resetBetNum() {
  this.props.dispatch({type: 'betCart/clear'});
}

sum( obj ) {
  var sum = 0;
  for( var el in obj ) {
    if( obj.hasOwnProperty( el ) ) {
      sum += parseFloat( obj[el] );
    }
  }
  return sum;
}


  render() {

    const { unopen } = this.props;
    const { totalBetCount, totalBetAmount } = this.props.betCart;
    
    let filteredUnopen = (unopen.length > 0) && unopen.slice(0,this.state.numOfQh);
   
    return (
      <div className="zhuihao">
        <div className="top-header">
        </div>
        <div className="zhuihao-select">
        <Timeline>
        {
          filteredUnopen.length > 0? filteredUnopen.map((item,i)=>{

            let isDisabled = (this.state.lCurrentQh == item.Qh)? true : false;
            const multiple = this.state.multiples[item.Qh] || this.state.multiple;

            return(
              
              <Timeline.Item key={i}
              dot={<div className="dot-warp"><span>{i}</span></div>} color="red"
            >
              <div className="left-content">
                <b>第{item.Qh}期</b>
                <div class="input-group">
                  <input type="button" value="-" class="button-minus" data-field="quantity" disabled={isDisabled} onClick={this.onMultipleMinus.bind(this,item.Qh)}/>
                  <input 
                    type="number" 
                    step="1" 
                    max="1000" 
                    value={multiple} 
                    name="quantity" 
                    class="quantity-field"
                    disabled={isDisabled}
                    onChange={(e)=>{
                      let value = e.target.value;
                      var multiples = {};
                      multiples[item.Qh] = value;
                      multiples = Object.assign(this.state.multiples,{},multiples);
                      this.setState({multiples});
                    }} />
                  <input type="button" value="+" class="button-plus" disabled={isDisabled} data-field="quantity" onClick={this.onMultipleAdd.bind(this,item.Qh)}/>
                </div>
                <div className="clearfix"></div>
              </div>
              <div className="right-content">
                <p>投入金额: <span>{totalBetAmount}</span></p>
                <p>累计金额: <span style={{color: 'red'}}>{totalBetAmount * multiple}元</span></p>
                <div className="clearfix"></div>
              </div>
              <div className="clearfix"></div>
            </Timeline.Item>
            )
          }):<p></p>
        
        }

        </Timeline>
        </div>
        <div className="zhuihao-number-select">
          <div className="left-content">
            <Checkbox onChange={this.onCheckBoxChange.bind(this)} checked={this.state.stopOnWin}>中奖后停止追号</Checkbox>
          </div>
          <div className="right-content">
            <div class="input-group">
              <input type="button" value="-" class="button-minus" data-field="quantity" onClick={this.minusQh.bind(this)}/>
              <input type="number" step="1" max="" value={this.state.numOfQh} name="quantity" class="quantity-field" onChange={this.onNumberOfQhChange.bind(this)}/>
              <input type="button" value="+" class="button-plus" data-field="quantity" onClick={this.plusQh.bind(this)}/>
            </div>
          </div>
          <div className="clearfix"></div>
        </div>
        <div className="ButtonBottom">
            <div className="btn_left">
            <div className="bet-total">
              共<span>{this.state.ltotalBetCount}</span>注, 金额<span>{this.getLocalTotalBetAmount()}</span>元
            </div>
            </div>

          <a onClick={this.submit.bind(this)}>
            <div className="btn_right">
              立即投注
            </div>
          </a>
          <div className="clearfix"></div>
        </div>
      </div>
    );
  }
}
export default connect(state => ({
    current: state.gamedata.current,
    unopen: state.gamedata.unopen,
    betCart : state.betCart,

}))(Form.create()(ZhuiHao));
