import {connect} from 'dva';
import PropTypes from 'prop-types';
import React from 'react';
import Router from 'umi/router';
import {message, Button, Icon, Input, Checkbox, Spin, Radio} from "antd";
import { Modal } from 'antd-mobile';
import { object } from 'prop-types';
import BasicComponent from "@/components/BasicComponent";
 import Textarea from "@/components/Textarea";
import BetCount from "./BetCount";
import BetOrderList from '../components/BetOrderList';
import {factorial, combinatorics} from "@/utils/math";
import * as Constants from "@/app_constants";
import * as BetHelper from "@/utils/bet_helper";
import * as Utils from "@/utils/common";
import * as RenderHelper from '../../../utils/render_helper';

import AddBet from '../components/AddBet';
import router from 'umi/router';

const ButtonGroup = Button.Group;

/* ############################## 选码器基类 ################################## */
/**
 * 选码器基类
 */
class BasicSelect extends BasicComponent {

    constructor(props) {
        super(props);
    }

    /* ---------------------------- 系统函数 ------------------------------*/

    componentWillMount() {
        this.resetState();
    }

    componentWillReceiveProps(nextProps) {
        // GameType 更改时把选中号码球清除。
        if (nextProps.GameType != this.props.GameType) {
            this.resetState(nextProps.GameType);
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps != this.props || nextState != this.state;
    }

    /* ---------------------------- 自定义函数 ------------------------------*/
    get unitPos() {
        throw "NotImplementedException";
    }

    get betNums() {
        throw "NotImplementedException";
    }

    reset() {
        throw "NotImplementedException";
    }

    resetState(gameType) {
        gameType = gameType || this.props.GameType;
        var unitPos = null;
        if (gameType && gameType.UnitPosCount > 0) {
            unitPos = Array(gameType.UnitPosCount).fill(0).fill(1, gameType.UnitPosCount - gameType.BetNumCount);
        }

        this.setState({betCount: 0, unitPos});
    }

    /* ---------------------------- 事件 ------------------------------*/

    onBallChange() {
        const {GameType} = this.props;
        if (!GameType) return;

        let betCountCalurator = BetCount[GameType.BetCountCalurator];
        if (betCountCalurator) {
            var count = betCountCalurator.calc(GameType, this.betNums, this.state.unitPos);
            if (!isNaN(count) && count != this.state.betCount) {
                this.setState({betCount: count});
                this.props.onBetCountChange && this.props.onBetCountChange(count, this.betNums, this.unitPos);
            }
        } else {
            console.log(`Can not found ${GameType.BetCountCalurator}`);
        }
    }

    /**
     * 任选数位占位符改变事件
     * @param  {[type]} index [description]
     * @return {[type]}       [description]
     */
    onUnitPosChange(index) {
        const {GameType} = this.props;
        var val = this.state.unitPos[index] ? 0 : 1;
        var unitPos = [...this.state.unitPos.slice(0, index), val, ...this.state.unitPos.slice(index + 1)];

        // 如果投注号码数与数位栏数相等，则说明是任选直选复式类的，要求数位栏随着任选栏变化，且任选栏的数量必须等于BetNumCount
        if (GameType.BetNumCount == GameType.DigitPosCount) {
            const posIndexArr = [];
            for (let i = 0; i < unitPos.length; i++) {
                if (unitPos[i] == 1)
                    posIndexArr.push(i);
            }

            if (posIndexArr.length > GameType.BetNumCount) {
                let reverseIndexs = null;
                // 本次选择的任选位置是相对第一个
                if (index == posIndexArr[0]) {
                    reverseIndexs = posIndexArr.slice(GameType.BetNumCount);
                } else {
                    // 本次选择的任选位置的相对最后一个或其他位置
                    reverseIndexs = posIndexArr.slice(0, posIndexArr.length - GameType.BetNumCount);
                }
                reverseIndexs.forEach(it => {
                    unitPos[it] = 0;
                });
            }

            this.setState({unitPos});

        } else {
            this.setState({unitPos});
        }
    }

    get defaultUnits() {
        return BetHelper.getGameTypeDefaultUnits(this.props.Group);
    }

    get units() {
        const {GameType} = this.props;

        // 任选直选复式
        if (GameType.UnitPosCount > 0 && GameType.BetNumCount == GameType.DigitPosCount) {
            const unitPos = this.state.unitPos;
            return this.defaultUnits.filter((it, index) => unitPos[index] == 1);
        }
        return BetHelper.getGameTypeUnits(this.props.Group, GameType);
    }

    /* ---------------------------- Renders ------------------------------*/

    /**
     * 任选数位
     * @return {[type]} [description]
     */
    renderUnitPos() {
        const {GameType} = this.props;
        var units = this.defaultUnits.slice(this.defaultUnits.length - GameType.UnitPosCount);
        var unitCount = this.state.unitPos.filter(it => it == 1).length;
        var planCount = combinatorics(unitCount, GameType.BetNumCount);

        return (
            <div className="unit">
                {units.map((x, i) =>
                    <Checkbox key={i} checked={this.state.unitPos && this.state.unitPos[i]}
                              onChange={it => this.onUnitPosChange(i)}>{x}</Checkbox>
                )}
                <span>温馨提示：您选择了{unitCount}个位置，系统自动根据位置组合{planCount}个方案。</span>
            </div>
        );
    }
}

BasicSelect.propTypes = {
    // 游戏组
    Group: PropTypes.string.isRequired,
    // 目前选中的游戏玩法
    GameType: PropTypes.object,
    // 投注注数变更事件
    onBetCountChange: PropTypes.func
}

/* ############################## 复式/组选 选码器 ################################## */
/**
 * 复式/组选 选码器
 */
class TreeStyleSelect extends BasicSelect {

    constructor(props) {
        super(props);
        this.state = {
            // 选中号码球，key: int(index) ,value:[num,]
            selectedBalls: {},
            // 数位占位符，[0,0,1,1,1] 1代表选中
            unitPos: null,
            // 注单数
            betCount: 0,
            currentMiss: 2,
        };
    }

    static missTypes = {
        2: "当前遗漏",
        4: "最大遗漏"
    }

    /* ---------------------------- 系统函数 ------------------------------*/

    componentDidUpdate(prevProps, prevState) {
        if (this.state.selectedBalls != prevState.selectedBalls || this.state.unitPos != prevState.unitPos) {
            this.onBallChange();
        }
    }


    /* ---------------------------- 自定义函数 ------------------------------*/
    get unitPos() {
        return this.state.unitPos;
    }

    get betNums() {
        return this.state.selectedBalls;
    }

    reset() {
        this.setState({selectedBalls: {}, betCount: 0});
    }

    resetState(gameType) {
        super.resetState(gameType);
        this.setState({selectedBalls: {}});
    }

    get balls() {
        const {GameType} = this.props;

        if (GameType.Name == "前后大小") {
            return ["前大", "后大"];
        } else if (GameType.GameTypeName.endsWith("BSOE")) {
            return ["大", "小", "单", "双"];
        } else
            return Utils.range(GameType.MinNum, GameType.MaxNum + 1);
    }

    get ballClass() {
        const name = this.props.GameType.GameTypeName;
        return name.endsWith("FBBS") ? "box" : "ball";
    }

    get showActionButton() {
        const name = this.props.GameType.GameTypeName;
        return !(name.endsWith("BSOE") || name.endsWith("FBBS"));
    }

    /* ---------------------------- Renders ------------------------------*/

    renderMissing() {
        return (
            <div className="game-selector-missing">
                <ButtonGroup>
                    {Object.keys(TreeStyleSelect.missTypes).map(it =>
                        <Button key={it} onClick={() => this.setState({currentMiss: it})}
                                className={this.state.currentMiss == it ? "active" : ""}>{TreeStyleSelect.missTypes[it]}</Button>
                    )}
                </ButtonGroup>
            </div>
        );
    }

    render() {
        const {Group, GameType, missing} = this.props;
        let units = this.units;

        const ballLen = this.balls.length;
        const missList = BetHelper.getMissingData(Group, GameType, this.balls.length, missing.data && missing.data.length && missing.data[this.state.currentMiss].DataMiss);

        return (

            <div>

                {this.renderMissing()}
                <div className="game-selector-boxpick">

                    {GameType.UnitPosCount > 0 && this.renderUnitPos()}
                    {units.map((unit, index) =>
                        <div className="game-selector-boxpick-digitpos fn-clear" key={unit}>
                            <div className="game-type-title">
                                <div id="pointer"><span>{unit}</span></div>

                            </div>

                            {this.showActionButton &&
                            <div className="bet-select-all">
                                <div className="type_select">
                                    <Radio.Group buttonStyle="solid" className="actions">
                                        <Radio.Button value="a"
                                                      onClick={() => this.onAllClick(index)}>全</Radio.Button>
                                        <Radio.Button value="b"
                                                      onClick={() => this.onBigClick(index)}>大</Radio.Button>
                                        <Radio.Button value="c"
                                                      onClick={() => this.onSmallClick(index)}>小</Radio.Button>
                                        <Radio.Button value="d"
                                                      onClick={() => this.onOddClick(index)}>奇</Radio.Button>
                                        <Radio.Button value="e"
                                                      onClick={() => this.onEvenClick(index)}>偶</Radio.Button>
                                        <Radio.Button value="f"
                                                      onClick={() => this.onClearClick(index)}>清</Radio.Button>
                                    </Radio.Group>
                                </div>
                            </div>
                            }

                            <div className="clearfix"></div>


                            <span className="label">
                                <label>{unit}</label>
                                <b>{TreeStyleSelect.missTypes[this.state.currentMiss]}</b>
                            </span>
                            <div className="nums">
                                <div className="bet_table">
                                    {this.balls.map((x, i) =>
                                        <div className="aline-abll">
                                            <ul className="num" key={i}>

                                                <li
                                                    className={this.ballClass + (this.state.selectedBalls[index] && this.state.selectedBalls[index].includes(x) ? " active" : "")}
                                                    onClick={() => this.onBallClick(x, index)}>
                                                    <div className="ball">{x}</div>

                                                </li>

                                                {/* <li className="miss">
                                                    {missList && missList[index * ballLen + i] || 0}
                                                </li>*/}
                                            </ul>
                                        </div>
                                    )}
                                    <div className="clearfix"></div>
                                </div>

                            </div>
                        </div>
                    )}

                    <div className="fn-clear"></div>
                </div>
            </div>
        );
    }

    /* ---------------------------- 事件 ------------------------------*/

    /**
     * 号码球点击
     * @param  {[type]} 号码   [description]
     * @param  {[type]} 号码所在行 [description]
     * @return {[type]}       [description]
     */
    onBallClick(num, index) {
        let nums = this.state.selectedBalls[index];
        if (!nums)
            nums = [num];
        else {
            if (nums.includes(num)) {
                var num_index = nums.indexOf(num);
                nums = [...nums.slice(0, num_index), ...nums.slice(num_index + 1)];
            } else {
                nums = [
                    ...nums,
                    num
                ];
            }
        }
        let tmp = {};
        tmp[index] = nums;
        let selectedBalls = Object.assign({}, this.state.selectedBalls, tmp);
        this.setState({selectedBalls});
    }

    /**
     * 全 选择
     * @return {[type]} [description]
     */
    onAllClick(index) {
        const {GameType} = this.props;
        let nums = Utils.range(GameType.MinNum, GameType.MaxNum + 1);
        let tmp = {};
        tmp[index] = nums;
        let selectedBalls = Object.assign({}, this.state.selectedBalls, tmp);
        this.setState({selectedBalls});
    }

    /**
     * 大 选择
     * @return {[type]} [description]
     */
    onBigClick(index) {
        const {GameType} = this.props;
        let nums = Utils.range(GameType.MinNum, GameType.MaxNum + 1);
        let tmp = {};
        tmp[index] = nums.slice(nums.length / 2);
        let selectedBalls = Object.assign({}, this.state.selectedBalls, tmp);
        this.setState({selectedBalls});
    }

    /**
     * 小 选择
     * @return {[type]} [description]
     */
    onSmallClick(index) {
        const {GameType} = this.props;
        let nums = Utils.range(GameType.MinNum, GameType.MaxNum + 1);
        let tmp = {};
        tmp[index] = nums.slice(0, nums.length / 2);
        let selectedBalls = Object.assign({}, this.state.selectedBalls, tmp);
        this.setState({selectedBalls});
    }

    /**
     * 奇 选择
     * @return {[type]} [description]
     */
    onOddClick(index) {
        const {GameType} = this.props;
        let nums = Utils.range(GameType.MinNum, GameType.MaxNum + 1);
        let tmp = {};
        tmp[index] = nums.filter(it => it % 2 != 0);
        let selectedBalls = Object.assign({}, this.state.selectedBalls, tmp);
        this.setState({selectedBalls});
    }

    /**
     * 偶 选择
     * @return {[type]} [description]
     */
    onEvenClick(index) {
        const {GameType} = this.props;
        let nums = Utils.range(GameType.MinNum, GameType.MaxNum + 1);
        let tmp = {};
        tmp[index] = nums.filter(it => it % 2 == 0);
        let selectedBalls = Object.assign({}, this.state.selectedBalls, tmp);
        this.setState({selectedBalls});
    }

    /**
     * 清 选择
     * @return {[type]} [description]
     */
    onClearClick(index) {
        const {GameType} = this.props;
        let tmp = {};
        tmp[index] = [];
        let selectedBalls = Object.assign({}, this.state.selectedBalls, tmp);
        this.setState({selectedBalls});
    }

}

/* ############################## 直选/单式/混合 选码器 ################################## */
class LinearStyleSelect extends BasicSelect {
    constructor(props) {
        super(props);
        this.state = {
            // 填写的号码字符串
            betNumStr: "",
            // 数位占位符，[0,0,1,1,1] 1代表选中
            unitPos: null,
            // 注单数
            betCount: 0
        }
    }

    /* ---------------------------- 系统函数 ------------------------------*/

    componentDidUpdate(prevProps, prevState) {
        if (this.state.betNumStr != prevState.betNumStr || this.state.unitPos != prevState.unitPos) {
            this.onBallChange();
        }
    }

    /* ---------------------------- 自定义函数 ------------------------------*/
    get unitPos() {
        return this.state.unitPos;
    }

    get betNums() {
        return this.state.betNumStr;
    }

    reset() {
        this.setState({betNumStr: "", betCount: 0});
        this.setState({unitPos:null});
    }

    resetState(gameType) {
        super.resetState(gameType);
        this.setState({betNumStr: ""});
    }

    /* ---------------------------- 事件 ------------------------------*/
    onBetNumStrChange(e) {
        const {GameType} = this.props;
        var val = e.target.value;
        if (/^[\d,\s,\,,\;]+$/.test(val) || val == "")
            this.setState({betNumStr: val});
    }

    /**
     * 去除无效投注号码
     * @return {[type]} [description]
     */
    onRemoveInvalidClick() {
        var {GameType} = this.props;
        if (!GameType) return;

        var nums_split_symbols = GameType.MaxNum > 9 ? Constants.TWO_DIGITS_NUMS_SPLIT_CHARS : Constants.NUMS_SPLIT_CHARS;
        var num_split_symbols = GameType.MaxNum > 9 ? Constants.NUM_SPLIT_CHAR : null;

        var betNumStr = BetHelper.removeInvalidBetNum(this.state.betNumStr, GameType.BetNumCount, nums_split_symbols, num_split_symbols);
        this.setState({betNumStr});
    }

    /**
     * 删除重复投注号码
     * @return {[type]} [description]
     */
    onDeduplicateClick() {
        const {GameType} = this.props;
        if (!GameType) return;

        const nums_split_symbols = GameType.MaxNum > 9 ? Constants.TWO_DIGITS_NUMS_SPLIT_CHARS : Constants.NUMS_SPLIT_CHARS;
        const num_split_symbols = GameType.MaxNum > 9 ? Constants.NUM_SPLIT_CHAR : null;

        const betNumStr = BetHelper.deduplicateBetNum(this.state.betNumStr, nums_split_symbols, num_split_symbols);
        this.setState({betNumStr});
    }

    /* ---------------------------- Renders ------------------------------*/

    render() {
        const {GameType} = this.props;
        const format_explain = GameType.MaxNum > 9 ? Constants.LINEAR_STYLE_TWO_DIGIT_FORMAT_EXPLAIN : Constants.LINEAR_STYLE_FORMAT_EXPLAIN;
        const formatExplain = BetHelper.getLinearStyleFormatExample(GameType.MinNum, GameType.MaxNum, GameType.BetNumCount);
        return (
            <div className="game-selector-straightpick">
                {GameType.UnitPosCount > 0 && this.renderUnitPos()}
                <p className="explain fn-clear" dangerouslySetInnerHTML={{__html: format_explain}}/>
                <Textarea className="ant-input" placeholder={formatExplain} value={this.state.betNumStr}
                          onChange={this.onBetNumStrChange.bind(this)}/>
                <span className="actions">
                    <Button type="blue" onClick={() => this.setState({betNumStr: ""})}>清空</Button>
                    <Button type="blue" onClick={this.onRemoveInvalidClick.bind(this)}>删除无效</Button>
                    <Button type="blue" onClick={this.onDeduplicateClick.bind(this)}>删除重复</Button>
                </span>

            </div>
        );
    }

}


/* ############################## 投注号码选择区 ################################## */

class BetNumSelect extends BasicComponent {
    constructor(props) {
        super(props);
        this.state = {
            // 投注号码
            betNums: null,
            // 填写的号码字符串
            // 数位占位符，[0,0,1,1,1] 1代表选中
            unitPos: null,
            // 注单数
            betCount: 0,
            // 单注投注额
            wager: 1,
            // 投注总金额
            amount: 0,
            visible: false,
           
        }
        
    }

    /* ---------------------------- 系统函数 ------------------------------*/

    // componentWillMount(){
    //     this.currentUrl = this.props.location.pathname;
    // }

    componentWillReceiveProps(nextProps) {
        if (nextProps.GameType != this.props.GameType) {
            this.resetState();
        }

        const isMissingUpdate = nextProps.missingLoading;
        const missingData = nextProps.missing;
        if (!isMissingUpdate &&
            (missingData.length == 0 ||
                missingData[0].GroupName != this.props.Group ||
                this.props.prevData != nextProps.prevData)) {
            //this.props.dispatch({type: "gamedata/getGameDataMissing", payload: this.props.Group});
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextState != this.state ||
            nextProps.game_type_info != this.props.game_type_info ||
            nextProps.missing != this.props.missing;
    }

    /* ---------------------------- 自定义函数 ------------------------------*/
    resetState() {
        this.setState({betCount: 0, betNums: null, unitPos: null});
        this.Select && this.Select.reset();
    }

    /* ---------------------------- 事件 ------------------------------*/
    onBetCountChange(betCount, betNums, unitPos) {
        this.setState({betCount, betNums, unitPos});
    }

    /* ---------------------------- 金额区事件 ------------------------------*/
    /**
     * 金额减
     * @return {[type]} [description]
     */
    onSubtractBtnClick() {
        var wager = this.state.wager;
        if (wager > 0)
            wager--;
        this.setState({wager:wager});
    }

    /**
     * 金额加
     * @return {[type]} [description]
     */
    onIncreaseBtnClick() {
        var wager = this.state.wager;
        wager++;
        this.setState({wager:wager});
    }

    onWagerUpdate(val){
        this.setState({wager : val});
    }

    onWagerChange(e) {
        var val = e.target.value;
        if (/^\d+$/.test(val))
            this.setState({wager: parseInt(val)});
        else if (val == "")
            this.setState({wager: 0});
    }
    onConfirmClick(){
      //  if(this.state.betCount > 0 && this.state.wager > 0)
     // router.push("/resit");
           // if(this.props.onSubmit && this.props.onSubmit());
    }

    /**
     * 添加按钮点击事件
     * @return {[type]} [description]
     */
    onAddBtnClick() {
        if (this.state.betCount == 0) {
            message.warning("您还未选择有效的投注号码！");
            return;
        }

        if (this.state.wager < 1) {
            message.warning("单注金额必须大于0");
            return;
        }

        var {GameType} = this.props;
        var {betCount, betNums, wager, unitPos} = this.state;
        if (this.props.onBetNumAdd)
            this.props.onBetNumAdd(GameType, betCount, wager, betNums, unitPos);

        //message.success("添加成功");
        this.resetState();
        this.Select && this.Select.reset();
    }

    /* ---------------------------- Renders ------------------------------*/

    renderConfirm() {
        const {GameType, user} = this.props;
        var odds = GameType && user && user.data ? (GameType.Scale + GameType.ScaleBase * user.data.Backpct) : null;
        if (odds && !Number.isInteger(odds))
            odds = parseFloat(odds.toFixed(2));

        const showDetail = () => {
            const {GameType} = this.props;
            var {betCount, betNums, wager, unitPos} = this.state;
            var betNum = [{ GameType, betCount, wager, betNums, unitPos }];

            const dialog = RenderHelper.dialog(null,
                <AddBet user = {this.props.user}
                    scale = {GameType.Scale}
                    kind = {GameType.Kind}
                    betCount = {this.state.betCount}
                    betNum = {betNum}
                    onWagerUpdate = {this.onWagerUpdate.bind(this)}  
                    onAddBet = {this.onAddBtnClick.bind(this)}         
                close={ ()=> close() } />);
    
            const close = () => {
              dialog.destroy(); 
        }
    }


        return (
            <div className="game-selector-confirm">
                <div className="bet-value">
                <span className="betcount">
                    共
                    <b>{this.state.betCount}</b>
                     注
                </span>
                <span className="amount">
                    金额
                    <b>￥{(this.state.wager * this.state.betCount).toFixed(2)}</b>
                    元
                </span>
                </div>

                  <div className="ButtonBottom">
                   

                     <a  onClick={this.resetState.bind(this)}>
                      <div className="btn_left">
                          <Icon type="reload"/> 重置
                      </div>
                    </a>

                     {/* <a href = "#"
                       className={this.state.betCount ? "active" : ""}
                       disabled={this.state.betCount <= 0}
                       onClick={()=>{showDetail();}}
                       >
                      <div className="btn_left">
                          <Icon type="reload"/> 添加
                      </div>
                    </a> */}


                      <a href="#popup-game-orders"
                        className={this.state.betCount ? "active" : ""}
                        //onClick={this.onConfirmClick.bind(this)}
                        onClick={this.onAddBtnClick.bind(this)}
                        disabled={this.state.betCount <= 0}
                       
                      >
                        <div className="btn_right ">
                          立即投注
                        </div>
                      </a>
                      <div className="clearfix"></div>
                  </div>
            </div>
    
        );
    }

    renderSelect() {
        let {Group, GameType, missing} = this.props;

        if (GameType.Style == 0) {
            return <LinearStyleSelect Group={Group} GameType={GameType}
                                      onBetCountChange={this.onBetCountChange.bind(this)} ref={it => {
                this.Select = it;
            }}/>
        } else {
            return <TreeStyleSelect Group={Group} GameType={GameType} missing={missing}
                                    onBetCountChange={this.onBetCountChange.bind(this)} ref={it => {
                this.Select = it;
            }}/>
        }
    }

    render() {
        const { GameType, game_type_info, loading } = this.props;
        return (
            <Spin spinning={loading || GameType == null}>
                <div className="game-selector">
                    {!loading && GameType && this.renderSelect()}
                    {this.renderConfirm()}
                </div>
            </Spin>


        );
    }
}

BetNumSelect.propTypes = {
    // 游戏组
    Group: PropTypes.string.isRequired,
    // 目前选中的游戏玩法
    GameType: PropTypes.object,
    /**
     * 添加投注内容事件
     * @param { GameType } gameType 玩法
     * @param { number } betCount 注数
     * @param { number } wager 单注金额
     * @param { dict|str } betNum 投注号码
     * @param { array|null } unitPos 任选数位
     */
    onBetNumAdd: PropTypes.func,
}

export default connect(state => ({
    game_type_info: state.gamedata.game_type_info,
    prevData: state.gamedata.prev,
    missing: state.gamedata.missing,
    user: state.user.user,
    loading: state.loading.models.gamedata.game_type_info,
    missingLoading: state.loading.effects["gamedata/getGameDataMissing"]
}))(BetNumSelect);
