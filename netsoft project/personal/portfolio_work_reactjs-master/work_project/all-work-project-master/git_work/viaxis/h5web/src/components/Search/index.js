
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Input, Select, DatePicker, TimePicker, Icon, Button, Form as AForm, Row, Col } from 'antd';
import { List, Flex, InputItem, Picker, WhiteSpace } from 'antd-mobile';
import DPicker from '@/components/DatePicker';
import DateRange from '@/components/DateRange';

import Form from '../Form';
import "./index.less";

class Search extends PureComponent {
    header = null;
    body = null;
    footer = null;
    state = {
        expandMore: false
    }

    /* -------------------------- Events ------------------------- */
    onSubmit(e) {
        e.preventDefault();
        this.form.validateFields && this.form.validateFields((errors, values) => {
            if (errors) return;

            this.setState({ expandMore: false });
            this.props.onSubmit && this.props.onSubmit(values);
        });
    }

    onReset(e) {
        this.props.form.resetFields();
        this.props.onReset && this.props.onReset();
    }


    /* -------------------------- Render ------------------------- */
    render() {
        const { expandMore } = this.state;
        this._parseChildren();
        return (
            <div className="search-box">
                {this.renderHeader()}
                {this.renderBody()}
            </div>
        );
    }

    renderHeader() {
        const { expandMore } = this.state;
        const btnTitle = this.props.textMore || "筛选";
        return (
            <div className="header">
                <Flex>
                    {this._parseHeaderElements()}
                    <Flex.Item>
                        <a className="btn-more" onClick={() => this.setState({ expandMore: !this.state.expandMore })}>
                            {btnTitle} &nbsp;
                    <Icon type={`caret-${expandMore ? "up" : "down"}`} theme="filled" />
                        </a>
                    </Flex.Item>
                </Flex>
            </div>
        );
    }

    _parseHeaderElements() {
        if (this.header === null || this.header === undefined) return [];

        let children = React.Children.toArray(this.header.props.children);
        const elements = [];
        children.forEach(ele => {
            if (this._isMobileComponent(ele)) {
                if (ele.props.getFieldProps) {
                    let eleType = this._getMobileComponentType(ele);
                    ele = this._wrapperMobileComponent(ele, eleType);
                }
                elements.push(<Flex.Item>{ele}</Flex.Item>);
            }
        });
        return elements;
    }

    renderBody() {
        const { expandMore } = this.state;
        return (
            <div className="body" style={{ display: expandMore ? "block" : "none" }}>
                <Form form={this.props.form} ref={el => this.form = el} onSubmit={this.onSubmit.bind(this)}>
                    {this.body && this._reduceChildren(this.body.props.children)}
                    <Flex className="footer">
                        <Flex.Item>
                            <Button type="default" onClick={this.onReset.bind(this)}>重置</Button>
                        </Flex.Item>
                        <Flex.Item>
                            <Button type="primary" htmlType="submit">确定</Button>
                        </Flex.Item>
                    </Flex>
                </Form>
            </div>
        );
    }

    _reduceChildren(eleChildren) {
        if (eleChildren === null || eleChildren === undefined) eleChildren;

        const { getFieldProps, getFieldDecorator } = this.props.form;

        eleChildren = React.Children.toArray(eleChildren);
        let elements = [];

        const innerTypes = [Row, Col, AForm.Item, List, Flex, Flex.Item];
        eleChildren.forEach(ele => {
            if (this._isMobileComponent(ele) && ele.props.getFieldProps) {
                let eleType = this._getMobileComponentType(ele);
                // elements.push(React.cloneElement(ele, { getFieldProps: null, ...getFieldProps(key, options) }));
                elements.push(this._wrapperMobileComponent(ele, eleType));
            }
            else if (this._isFormComponent(ele) && ele.props.getFieldDecorator) {
                let eleType = this._getFormComponentType(ele);
                elements.push(this._wrapperFormComponent(ele, eleType));
            }
            else if (innerTypes.includes(ele.type))//List === ele.type || AForm.Item === ele.type)
                elements.push(React.cloneElement(ele, { children: this._reduceChildren(ele.props.children) }));
            else
                elements.push(ele);
        });
        return elements;
    }

    /* --------------------------- Antd Mobile Elements ---------------------------  */
    MobileTypes = [InputItem, Picker, DPicker, DateRange]
    _isMobileComponent(ele) {
        for (var i in this.MobileTypes) {
            if (this.MobileTypes[i] === ele.type)
                return true;
        }
        return false;
    }
    _getMobileComponentType(ele) {
        for (var i in this.MobileTypes) {
            if (this.MobileTypes[i] === ele.type)
                return this.MobileTypes[i];
        }
        return null;
    }
    _wrapperMobileComponent(ele, Type) {
        const { getFieldProps } = this.props.form;
        let key = Object.keys(ele.props.getFieldProps)[0];
        let options = ele.props.getFieldProps[key];
        let { getFieldProps: fieldProps, children, ...props } = ele.props;
        return React.createElement(Type, { key: `Search-${key}`, ...props, ...getFieldProps(key, options) }, children);
    }

    /* --------------------------- Antd  Form  Elements ---------------------------  */

    FormTypes = [Input, Select, DatePicker, TimePicker, DPicker]
    _isFormComponent(ele) {
        for (var i in this.FormTypes) {
            if (this.FormTypes[i] === ele.type)
                return true;
        }
        return false;
    }
    _getFormComponentType(ele) {
        for (var i in this.FormTypes) {
            if (this.FormTypes[i] === ele.type)
                return this.FormTypes[i];
        }
        return null;
    }
    _wrapperFormComponent(ele, Type) {
        const { getFieldDecorator } = this.props.form;

        let { getFieldDecorator: fieldDecorator, children, ...props } = ele.props;
        let key = Object.keys(fieldDecorator)[0];
        let options = fieldDecorator[key];
        return getFieldDecorator(key, options)(React.createElement(Type, { key: `Search-${key}`, ...props }, children))
    }

    _parseChildren() {
        let { children } = this.props;
        children = React.Children.toArray(children);
        children.forEach(ele => {
            if (Header === ele.type)
                this.header = ele;
            else if (Body === ele.type)
                this.body = ele;
            else if (Footer === ele.type)
                this.footer = ele;
        });
    }
};
Search.propTypes = {
    onSubmit: PropTypes.func,
    onReset: PropTypes.func,
}


class Header extends PureComponent {
    render() {
        return this.props.children;
    }
}
Header.propTypes = {
    // 更多按钮名称
    BtnMoreName: PropTypes.string,
}

class Body extends PureComponent {
    render() {
        return this.props.children;
    }
}

class Footer extends PureComponent {
    render() {
        return this.props.children;
    }
}

Search.Header = Header;
Search.Body = Body;
Search.Footer = Footer;

export default Form.create()(Search);

