import React, {Component} from 'react';
import { Row, Col, Icon } from 'antd';
import Link from 'umi/link';

import game01 from '../../../assets/images/game-icon/ssc.png';
import game02 from '../../../assets/images/game-icon/pk10.png';
import game03 from '../../../assets/images/game-icon/xylc.png';
import game04 from '../../../assets/images/game-icon/kl8.png';
import game05 from '../../../assets/images/game-icon/fc3d.png';
import game06 from '../../../assets/images/game-icon/pl3.png';
import game07 from '../../../assets/images/game-icon/keno.png';

class HotCaiZhong extends Component {

  render() {
    return (
      <div className="hot-cai-zhong">
        <div className="sub-title">
          热门彩种
        </div>
        <Row>
        <Link to="/game/cqssc/bet">
          <Col span={6}>
            <img src={game01} alt=""/>
            <p>重庆时时彩</p>
          </Col>
        </Link>
        <Link to="/game/pk10/bet">
          <Col span={6}>
            <img src={game02} alt=""/>
            <p>北京PK10</p>
          </Col>
        </Link>
        <Link to="/game/kl10/bet">
          <Col span={6}>
            <img src={game03} alt=""/>
            <p>重庆幸运农场</p>
          </Col>
        </Link>
        <Link to="/game/kl8/bet">
          <Col span={6}>
            <img src={game04} alt=""/>
            <p>快乐8</p>
          </Col>
        </Link>
        <Link to="/game/fc3d/bet">
          <Col span={6}>
            <img src={game05} alt=""/>
            <p>福彩3D</p>
          </Col>
        </Link>
        <Link to="/game/pl3/bet">
          <Col span={6}>
            <img src={game06} alt=""/>
            <p>排列三</p>
          </Col>
        </Link>
        <Link to="/game/keno/bet">
          <Col span={6}>
            <img src={game07} alt=""/>
            <p>KENO</p>
          </Col>
        </Link>

          <Col span={6} className="more-game">
            <label htmlFor="modal" >
              <div className="more">
                <Icon type="plus" />
              </div>
            </label>
            <p style={{color: '#40a9ff'}}>更多彩种</p>
          </Col>
          <div className="modal">
            <input id="modal" type="checkbox" name="modal" tabIndex="1"/>
            <div className="modal__overlay">
              <div className="top-header">
                  <h3>更多彩种</h3>
                  <label htmlFor="modal" className="close-game">
                    <Icon type="close" style={{color: 'white'}}/>
                  </label>
              </div>
              <div className="sub-title">
                  <label>彩种类型</label>
              </div>
              <div className="modal__box">
                {/* <Col span={6}>
                  <img src={game01} alt=""/>
                  <p>上海快3</p>
                </Col>
                <Col span={6}>
                  <img src={game01} alt=""/>
                  <p>上海快3</p>
                </Col>
                <Col span={6}>
                  <img src={game01} alt=""/>
                  <p>上海快3</p>
                </Col>
                <Col span={6}>
                  <img src={game01} alt=""/>
                  <p>上海快3</p>
                </Col>
                <Col span={6}>
                  <img src={game01} alt=""/>
                  <p>上海快3</p>
                </Col>
                <Col span={6}>
                  <img src={game01} alt=""/>
                  <p>上海快3</p>
                </Col>
                <Col span={6}>
                  <img src={game01} alt=""/>
                  <p>上海快3</p>
                </Col>
                <Col span={6}>
                  <img src={game01} alt=""/>
                  <p>上海快3</p>
                </Col> */}
              </div>
            </div>
          </div>
        </Row>
      </div>
    );
  }
}

export default HotCaiZhong;
