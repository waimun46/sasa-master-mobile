import { GET, POST } from '@/utils/request';

export function submitPaymentRequest(value) {
    let body = JSON.stringify({PayAmount : value});
    return POST("/api/deposit/bipipay/payrequest", body);

}

export function queryPaymentChannel(cid) {

    let body = JSON.stringify({ChannelId : cid});
    return POST("/api/deposit/bipipay/payrequest", body);
}
