import { GET, POST } from '@/utils/request';

// login
export function login(username, password, captcha) {
    var body = JSON.stringify({
        username,
        password,
        captcha
    });

    return POST("/api/passport/login", body);
}

export function logout() {
    return GET("/logout");
}

/**
 * 注册接口
 * @param  {str} email    邮箱
 * @param  {str} password 密码
 * @param  {str} qq       qq
 * @param  {str} mobile   手机
 * @param  {str} captcha  验证码
 * @param  {str} referral 上级推荐
 * @return {[type]}          [description]
 */
export function register(email, password, qq, mobile, captcha, referral) {
    var body = JSON.stringify({
        email,
        password,
        qq,
        mobile,
        captcha,
        referral
    });
    let url = '/api/passport/register';
    // if (referral)
    //     url = `${url}/${referral}`;
    return POST(url, body);
}

/**
 * 发送忘记密码重置链接
 * @param  {[type]} email   [description]
 * @param  {[type]} captcha [description]
 * @return {[type]}         [description]
 */
export function forgetPassword(email, captcha) {
    var payload = JSON.stringify({ email, captcha });

    return POST("/api/passport/password/forget", payload);
}

/**
 * 检查重置密码Ticke是否有效
 * @param  {[type]} ticket [description]
 * @return {[type]}        [description]
 */
export function checkResetTicket(ticket) {
    return POST(`/api/user/ticket/${ticket}`);
}

/**
 * 重置登陆密码
 * @param  {[type]} ticket   [description]
 * @param  {[type]} password [description]
 * @return {[type]}          [description]
 */
export function resetLoginPassword(ticket, password) {
    const payload = JSON.stringify({ password });
    return POST(`/api/passport/password/reset/${ticket}`, payload);
}

/**
 * 检查邮箱是否存在
 * @param  {[type]} email [description]
 * @return {[type]}       [description]
 */
export function checkUsername(payload) {
   // const payload = JSON.stringify({ email });
   if(payload.email==undefined)
    return '1001';

    return POST(`/api/user/checkusername?email=${payload.email}`);
}

export function getUserInfo() {
    return dispatch => {
        dispatch({
            type: "UserInfo",
            state: ActionState.Doing,
        });
        socket.send("UserInfo");
        // 告诉调用代码不需要再等待。
        return Promise.resolve();
    };
}

/**
 * 获取用户信息
 * @return {[type]} [description]
 */
export function queryUserInfo() {
    return POST("/api/user");
}

/**
 * 修改登陆密码
 * @param  {[type]} oldPassword [description]
 * @param  {[type]} newPassword [description]
 * @param  {[type]} captcha     [description]
 * @return {[type]}             [description]
 */
export function updateLoginPassword(oldPassword, newPassword, captcha) {
    var body = JSON.stringify({
        old_password: oldPassword,
        password: newPassword,
        captcha
    });
    return POST("/api/passport/password/update", body);
}

/**
 * 修改安全密码
 * @param  {[type]} oldPassword 当前安全密码，如果是第一次设置安全密码时为null
 * @param  {[type]} newPassword [description]
 * @param  {[type]} captcha     [description]
 * @return {[type]}             [description]
 */
export function updateSafePassword(oldPassword, newPassword, captcha) {
    var body = JSON.stringify({
        old_password: oldPassword,
        password: newPassword,
        captcha
    });
    return POST("/api/passport/safepassword/update", body);
}

/**
 * 重置安全密码
 * @param  {[type]} password [description]
 * @param  {[type]} captcha  [description]
 * @return {[type]}          [description]
 */
export function resetSafePassword(password, captcha) {
    var body = JSON.stringify({
        password,
        captcha
    });
    return POST("/api/passport/safepassword/reset", body);
}

/**
 * 是否已设置安全密码
 * @return {Boolean} [description]
 */
export function hasSafePassword() {
    return POST("/api/passport/safepassword");
}

/**
 * 发送验证码
 * @param  {Boolean} isShort [description]
 * @return {[type]}          [description]
 */
export function sendTicket(isShort = true) {
    return POST(`/api/passport/ticket/${isShort ? 0 : 1}`);
}

export function sendPhoneTicket(isShort = true) {
    return POST(`/api/passport/ticket/phone/${isShort ? 0 : 1}`);
}

export function sendNewPhoneTicket(newPhoneNumber, isShort = true) {
    var body = JSON.stringify({
        newPhoneNumber
    });

    return POST(`/api/passport/ticket/newPhone/${isShort ? 0 : 1}`, body);
}

/**
 * 获取上一次登陆信息
 * @return {[type]} [description]
 */
export function getLastLoginLog() {
    return POST("/api/user/log");
}

/**
 * 获取团队下级信息,只获取直接下级
 * @param  {[type]} userId [description]
 * @param  {[type]} pageIndex   [description]
 * @return {[type]}        [description]
 */
export function queryTeam(userId, pageIndex) {
    var body = JSON.stringify({ pageIndex });
    let url = `/api/user/team`;
    if (userId)
        url = `/api/user/${userId}/team`;

    return POST(url, body);
}

/**
 * 查询下级用户
 * @param  {[type]} pageIndex   [description]
 * @param  {[type]} querys [description]
 * @return {[type]}        [description]
 */
export function queryUsers(pageIndex, querys) {
    var queryParams = Object.assign({}, querys, {
        pageIndex
    });
    var body = JSON.stringify(queryParams);
    return POST(`/api/user/team/query`, body);
}

/**
 * 获取用户上级列表
 * @param  {[type]} userId [description]
 * @return {[type]}        [description]
 */
export function getParents(userId) {
    var body = JSON.stringify({ All: true });
    return POST(`/api/user/${userId}/parents`, body);
}

/**
 * [addAgent description]
 * @param {[type]} email      [description]
 * @param {[type]} password   [description]
 * @param {[type]} aid        [description]
 * @param {[type]} backpct    [description]
 * @param {[type]} dailywages [description]
 * @param {[type]} user_kind  [description]
 * @param {[type]} remark     [description]
 * @param {[type]} mobile     [description]
 * @param {[type]} qq         [description]
 * @param {[type]} captcha    [description]
 */
export function addAgent(email, password, aid, backpct, dailywages, user_kind, remark, mobile, qq, captcha) {
    var body = JSON.stringify({
        email,
        password,
        backpct,
        dailywages,
        user_kind,
        remark,
        mobile,
        qq,
        captcha
    });

    return POST(`/api/user/${aid}/add`, body);

}

/**
 * 修改下级用户信息
 * @param  {[type]} userId [description]
 * @param  {[type]} data   [description]
 * @return {[type]}        [description]
 */
export function updateUser(userId, data) {
    return POST(`/api/user/${userId}/update`, JSON.stringify(data));
}

/* ---------------------------- Referrals ----------------------------- */

/**
 * 查询用户下级发展链接
 * @param  {[type]} querys [description]
 * @return {[type]}        [description]
 */
export function queryReferral(querys) {
    var body = JSON.stringify(querys);
    return POST(`/api/user/referral`, body);
}

/**
 * 添加用户下级发展链接
 * @param {[type]} backpct [description]
 */
export function addReferral(backpct, captcha) {
    const data = JSON.stringify({
        backpct,
        captcha
    });
    return POST(`/api/user/referral/add`, data);
}

/**
 * 修改用户下级发展链接
 * @param  {[type]} id      [description]
 * @param  {[type]} backpct [description]
 * @param  {[type]} status  [description]
 * @return {[type]}         [description]
 */
export function updateReferral(id, backpct, status) {
    const data = JSON.stringify({ backpct, status });
    return POST(`/api/user/referral/update/${id}`, data);
}

/* ---------------- Config ---------------- */
export function getConfig() {
    return GET("/api/config.json");
}


export function bindEmail(payload){
    const data = JSON.stringify(payload);

    return POST('/api/user/bindEmail', data);
}

export function bindPhone(payload){
    const data = JSON.stringify(payload);

    return POST('/api/user/bindPhone', data);
}

export function bindNewPhone(payload){
    const data = JSON.stringify(payload);

    return POST('/api/user/bindNewPhone', data);
}

export function verifyEmail(payload){
    const data = JSON.stringify(payload);

    return POST('/api/passport/email/verify', data);
}

export function updateQQNumber(currentQQ, newQQ) {
    var body = JSON.stringify({
       currentQQ, newQQ
    });
    return POST("/api/passport/qqnumber/update", body);
}


export function verifyRealName(realName,idNumber) {
    var body = JSON.stringify({
       realName, idNumber
    });
    return POST("/api/passport/VerifyRealName", body);
}




