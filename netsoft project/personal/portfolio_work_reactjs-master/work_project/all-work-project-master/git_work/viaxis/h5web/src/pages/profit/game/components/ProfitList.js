import { connect } from 'dva';
import { message, Table, Breadcrumb, Button, Select, Input, Icon } from 'antd';
import Link from 'umi/link';

import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';
import SearchForm from '@/components/SearchForm';
import * as Utils from "@/utils/common";
import * as RenderHelper from "@/utils/render_helper";
import * as Enums from "@/app_enum";
import * as Constants from "@/app_constants";

import ProfitGroupGameType from './ProfitGroupGameType';
import ProfitGameType from './ProfitGameType';
import ProfitUserGameType from './ProfitUserGameType';

/**
 * 用于标示Profit中显示什么数据
 * @type {Object}
 */
const DataFlags = {
    Group: 0,
    GameType: 1,
    User: 2
};

class ProfitList extends Page {
    title = "游戏盈亏"

    constructor(props) {
        super(props);
        this.userId = this.getUserId(props);
        this.state = {
            flags: DataFlags.Group,
            gameType: null,
            querys: {}
        }
    }

    /* ------------------ Sys -------------------- */
    componentWillMount() {
        this.setTitle();
        const { groups } = this.props;
        if (!groups || groups.length == 0)
            this.props.dispatch({ type: "gameGroup/getGameGroup" });

        this.initGroup(groups);
    }

    componentWillUpdate(nextProps, nextState) {
        if (nextProps.groups != this.props.groups) {
            this.initGroup(nextProps.groups);
        }
    }

    componentWillReceiveProps(newProps) {
        if (newProps != this.props) {
            const userId = this.getUserId(newProps);
            if (userId != this.userId && !isNaN(userId)) {
                this.userId = userId;
            }
        }
    }

    /* ------------------ Functions -------------------- */

    getUserId(props) {
        let userId = props.match.params.userId;
        if (userId)
            userId = parseInt(userId);
        return userId;
    }

    initGroup(groups) {
        const group = {};
        for (let i in groups) {
            group[groups[i].Id] = groups[i].Name;
        }
        this.group = group;
    }

    /* ------------------ Events -------------------- */

    onSearchClick(values) {
        this.setState({ querys: values });
    }

    onResetClick() {
        this.querys = {};
    }

    onGroupClick(groupId, fromDate, toDate) {
        this.setState({
            flags: DataFlags.GameType,
            groupId: groupId,
            fromDate,
            toDate
        })
    }

    onGameTypeClick(gameType, fromDate, toDate) {
        this.setState({
            flags: DataFlags.User,
            gameType
        });
    }

    onGameTypeGoBack() {
        this.setState({
            flags: DataFlags.Group,
            groupId: null
        });
    }

    onUserTypeGoBack() {
        this.setState({
            flags: DataFlags.GameType,
            gameType: null
        });
    }

    /* ------------------ Render ------------------ */
    render() {
        const { flags, gameType, querys } = this.state;
        return (
            <div>
                {this.renderSearch()}
                <div className="search-result-list">
                    {flags == DataFlags.Group &&
                        <ProfitGroupGameType {...this.state}
                            onGroupClick={this.onGroupClick.bind(this)} />
                    }
                    {flags == DataFlags.GameType &&
                        <ProfitGameType userId={this.userId}
                            {...this.state}
                            onGameTypeClick={this.onGameTypeClick.bind(this)}
                            onGoBackClick={this.onGameTypeGoBack.bind(this)} />
                    }
                    {flags == DataFlags.User &&
                        <ProfitUserGameType userId={this.userId}
                            {...this.state}
                            onGoBackClick={this.onUserTypeGoBack.bind(this)} />
                    }
                </div>
            </div>
        );
    }

    renderSearch() {
        const { groups } = this.props;

        const formItemLayout = {
            labelCol: { span: 8 },
            wrapperCol: { span: 16 }
        };

        return (
            <SearchForm onSearch={this.onSearchClick.bind(this)} onReset={this.onResetClick.bind(this)}>
                <group_id label="彩种" {...formItemLayout} options={{ initialValue: "" }}>
                    <Select>
                        <Option value="">全部</Option>
                        {groups.map(group => {
                            return <Option value={group.Id.toString()} key={group.Id}>{group.Name}</Option>
                        })}
                    </Select>
                </group_id>
                <date label="日期" {...formItemLayout} format={(times) => Utils.dateRangeFormat(times)}>
                    {RenderHelper.rangeDatePick()}
                </date>
            </SearchForm>
        );
    }
};

export default connect(state => ({
    groups: state.gameGroup,
    loading: state.loading.models.gameGroup,
}))(Form.create()(ProfitList));