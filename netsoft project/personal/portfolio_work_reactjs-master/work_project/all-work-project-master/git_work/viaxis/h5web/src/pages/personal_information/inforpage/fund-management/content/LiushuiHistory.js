import React, { Component } from 'react';
import { connect } from 'dva';
import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';
import { List } from 'antd-mobile';

import DateRange from '@/components/DateRange';
import pending from '../../../../../assets/images/icon/pending.png';
import zhui from '../../../../../assets/images/icon/zhui.png';
import game from '../../../../../assets/images/game-icon/game01.png';

const Item = List.Item;
const Brief = Item.Brief;



class LiushuiHistory extends Page {
  title = "流水纪录"

  render() {
    return (
      <div className="fund-history ">
        <div className="top-header"> </div>

        {/* date */}
        <List className="date-select">
          <Item><DateRange/></Item>
        </List>

        <div className="form_title">
          <h5>流水信息</h5>
        </div>
        <List className="my-list ">
          <a href="#popup_report">
            <Item
              extra={
                <div className="bet-money add">
                  <span>￥0.00</span>
                </div>
              }
              align="top"
              multipleLine
              onClick={() => {}}
            >
              <span className="title-warp">中国银行</span>
              -
              <span className="title-warp">ID13008</span>
              <Brief style={{fontSize: '12px', color: '#cccccc'}}>24-12-2018 15:30:17</Brief>
            </Item>
          </a>
          <a href="#popup_report">
            <Item
              extra={
                <div className="bet-money pending">
                  <span>￥0.00</span>
                  <div className="img-icon">
                    <img src={pending} alt="pending" />
                  </div>
                </div>
              }
              align="top"
              multipleLine
              onClick={() => {}}
            >
              <span className="title-warp">支付宝转账</span>
              -
              <span className="title-warp">ID13008</span>
              <Brief style={{fontSize: '12px', color: '#cccccc'}}>24-12-2018 15:30:17</Brief>
            </Item>
          </a>
          <a href="#popup_report">
            <Item
              extra={
                <div className="bet-money add">
                  <span>￥0.00</span>
                </div>
              }
              align="top"
              multipleLine
              onClick={() => {}}
            >
              <span className="title-warp">银行转账</span>
              -
              <span className="title-warp">ID13008</span>
              <Brief style={{fontSize: '12px', color: '#cccccc'}}>24-12-2018 15:30:17</Brief>
            </Item>
          </a>
          <a href="#popup_report">
            <Item
              extra={
                <div className="bet-money minus">
                  <span>￥0.00</span>
                </div>
              }
              align="top"
              multipleLine
              onClick={() => {}}
            >
              <span className="title-warp">QQ转账</span>
              -
              <span className="title-warp">ID13008</span>
              <Brief style={{fontSize: '12px', color: '#cccccc'}}>24-12-2018 15:30:17</Brief>
            </Item>
          </a>
        </List>


        {/**** pop-list popup ****/}
        <div className="pop-list">
          <div id="popup_report" class="overlay">
            <div class="popup">
              <div className="popup-inner">
                <h2>流水记录</h2>

                <div class="content">
                  <table>
                    <thead>
                    <tr className="tr-style">
                      <th>充值方式:</th>
                      <th>户名:</th>
                      <th>金额:</th>
                      <th>余额:</th>
                      <th>流水号:</th>
                      <th>状态:</th>
                      <th>备注:</th>
                      <th>申请时间:</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr className="tr2-style">
                      <td>银行转账</td>
                      <td>210015</td>
                      <td>1000.00</td>
                      <td>500.00</td>
                      <td>9</td>
                      <td  style={{ color: '#52c41a' }}>完成</td>
                      <td>****123</td>
                      <td>24-12-2018 15:30:17</td>
                    </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <a class="close" href="#">
                <div className="btn-popup close">
                  <h4>关闭</h4>
                </div>
              </a>
            </div>
          </div>
        </div>






      </div>
    );
  }
};

export default connect(state => ({

}))(Form.create()(LiushuiHistory));
