
import BasicComponent from "@/components/BasicComponent";

export default class Page extends BasicComponent {
    title = "";
    componentWillMount() {
        this.setTitle();
    }

    setTitle(title) {
        this.props.dispatch({ type: "global/setTitle", payload: title || this.title });
    }
}