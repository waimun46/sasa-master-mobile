import React, {Component} from 'react';
import Banner from './content/Banner';
import TopButton from './content/TopButton';
import HotCaiZhong from './content/HotCaiZhong';
import BottomButton from './content/BottomButton';

import Page from "@/layouts/PageBasic";

class Homepage extends Component {
  //title=""
  
  render() {
    return (
      <div className="homepage ">
        <Banner/>
        <TopButton/>
        <HotCaiZhong/>
        <BottomButton/>
        <div className="clearfix"></div>
      </div>
    );
  }
}

export default Homepage;


// export default connect(state => ({

// }))(Homepage);
