import {connect} from 'dva';
import PropTypes from 'prop-types';
import React from 'react';
import {Icon, Tooltip, Spin, Card, Collapse, Modal} from 'antd';

import BasicComponent from "@/components/BasicComponent";
import * as Utils from "@/utils/common";
import * as RenderHelper from "@/utils/render_helper";
import BetTypeSelection from "../components/BetTypeSelection";

const Panel = Collapse.Panel;

function callback(key) {
    console.log(key);
}

const text = `
  A dog is a type of domesticated animal.
  Known for its loyalty and faithfulness,
  it can be found as a welcome guest in many households across the world.
`;

class BetTypeSelect extends BasicComponent {

    constructor(props) {
        super(props);
        this.state = {
            // 玩法数据加载中
            loading: true,
            // 当前选中的玩法目录
            selectedCategory: null,
            // 当前选中的玩法种类
            selectedSort: null,
            // 当前选中的玩法
            selectedType: null,

        };

    }

    state = { visible: false }

    showModal = () => {
        this.setState({
          visible: true,
        });
      }
    
      handleOk = (e) => {
        console.log(e);
        this.setState({
          visible: false,
        });
      }
    
      handleCancel = (e) => {
        console.log(e);
        this.setState({
          visible: false,
        });
      }

    componentDidMount(){
      this.getUserInfo();
    }

    getUserInfo(){
      this.props.dispatch({ type: "user/queryUserInfo" });
    }

    /* ---------------------------- 系统函数 ------------------------------*/

    componentWillMount() {
        this.getGameTypeInfo(this.props);

        //  在非第一次加载时，如从追号面板切换回来,game_type_info不为空，而selectedCategory等为空，导致异常。
        //  所以如果加载时判断进行初始参数构造。
        let { game_type_info } = this.props;
        if (game_type_info && game_type_info && game_type_info.length)
            this.updateSelectType(game_type_info[0]);
    }

    componentWillReceiveProps(nextProps) {

        if (nextProps.Group != this.props.Group)
            this.getGameTypeInfo(nextProps);

        let {game_type_info} = this.props;
        let new_game_type_info = nextProps.game_type_info;
        if (game_type_info != new_game_type_info) {

            this.updateSelectType(new_game_type_info[0]);
        }

    //          // Sorts > kinds > types  
    //         // TODO:应该是由数据指定默认
    //         let filtered = new_game_type_info.map(item =>{
    //             return item.Sorts;


    //             // return item.filter(b => {
                    
    //             //     return b.Sorts.Kinds.Types.Name != '单试';
                
    //             // });
            
    //     }).map(sort =>{
    //         return sort.Kinds;
    //     });

    //     let xbp = filtered.map(item=>{
    //         let b = item.Foreach(
    //             k=>{
    //                 let j = k;
    //             }
    //         );
    //     })
    // }
    

           // this.updateSelectType(new_game_type_info[0]);
        
    }

    /* ---------------------------- 自定义函数 ------------------------------*/

    getGameTypeInfo(props) {
        this.props.dispatch({ type: "gamedata/getGameTypeInfo", payload: props.Group }).then(it => this.setState({
            loading: false
        }));
    }

    updateSelectType(selectedCategory) {
        if (!selectedCategory) return;
        this.setState({selectedCategory});
        let selectedSort = this.sort(selectedCategory.Sorts)[0];
        this.setState({selectedSort});
        
        let selectedType = this.sort(this.sort(selectedSort.Kinds)[0].Types)[0];
        // if(selectedType.Name==='单式')
        //         selectedType = this.sort(this.sort(selectedSort.Kinds)[0].Types)[1];
        this.setState({selectedType});
        this.props.onChange && this.props.onChange(selectedType);

    }

    sort(list) {
        list.sort((a, b) => b.Weight - a.Weight);
        return list;
    }

    /* ---------------------------- Events ------------------------------*/

    onCategoryClick(category) {
        if (category == this.state.selectedCategory) return;
        this.updateSelectType(category);
    }

    onSortClick(sort) {
        if (sort == this.state.selectedSort) return;
        this.setState({
            selectedSort: sort
        });

        let selectedType = this.sort(this.sort(sort.Kinds)[0].Types)[0];
        this.setState({selectedType});
        this.props.onChange && this.props.onChange(selectedType);
    }

    // onSortClick(sortType){
    //     this.props.onChange && this.props.onChange(sortType);

    // }

    onTypeClick(type) {
        if (type == this.state.selectedType) return;
        this.setState({
            selectedType: type
        });
        this.props.onChange && this.props.onChange(type);
    }

    showBetTypeSelection = (value) => {
        // const {GameType} = this.props;
        // var {betCount, betNums, wager, unitPos} = this.state;
        // var betNum = [{ GameType, betCount, wager, betNums, unitPos }];

        let selectedCategory = this.props.selectedCategory;
        let selectedSort = this.props.selectedSort;
        let selectedType = this.props.selectedType;

        const dialog = RenderHelper.dialog(null,
            <BetTypeSelection 
                otherCategorys = {value}
                selectedCategory = {this.state.selectedCategory}
                selectedSort = {this.state.selectedSort}
                selectedType = {this.state.selectedType}
                onSortClick = {this.onSortClick.bind(this)}
                close={ (val)=> close(val)}
                
            />
            // <AddBet user = {this.props.user}
            //     scale = {GameType.Scale}
            //     kind = {GameType.Kind}
            //     betCount = {this.state.betCount}
            //     betNum = {betNum}
            //     onWagerUpdate = {this.onWagerUpdate.bind(this)}  
            //     onAddBet = {this.onAddBtnClick.bind(this)}         
            // close={ ()=> close() } />
            
            );

        const close = (value) => {

            const {selectedType, selectedSort, selectedCategory } = value;
            this.setState({selectedType:selectedType});
            this.setState({selectedSort:selectedSort});
            this.setState({selectedCategory:selectedCategory});

            this.props.onChange && this.props.onChange(selectedType);
          dialog.destroy(); 
    }
}

    /* ---------------------------- Renders ------------------------------*/

    render() {
        const { game_type_info, loading } = this.props;

        return (
            !loading && game_type_info && game_type_info.length
                ?
                this.renderPanel()
                :
                <div className="game-bettype-loading">
                    <Spin/>
                    加载中...
                </div>
        );
    }

    renderPanel() {

        const { game_type_info, GameType} = this.props;
        const userInfo = this.props.userinfo;
        let selectedCategoryIndex = game_type_info.indexOf(this.state.selectedCategory);
        let otherCategorys = [...game_type_info.slice(0, selectedCategoryIndex), ...game_type_info.slice(selectedCategoryIndex + 1)];

        


        
        return (
            <div className="game-bettype">
                <div className="game-bettype-detail">
                    <div className="detaillist">
                        <Card title="赔率">
                            <p>9.760</p>
                        </Card>
                        <Card title={this.state.selectedSort.Name}>
                     
                                <Icon
                                    type="question-circle"
                                    className="game_infor uu"
                                    theme="twoTone" 
                                    twoToneColor="#9E9E9E"
                                    onClick={this.showModal}
                                />
                        

                            <p className="p_pos">{GameType.Name}</p>
                           {/* <Icon type="code-sandbox" className="game_select" onClick={() => this.showBetTypeSelection(otherCategorys)}/> */}
                          
                            

                            <div className="modal">
                                <input id="modal" type="checkbox" name="modal" />
                                
                                <label htmlFor="modal" className="modal_close">
                                    <Icon type="code-sandbox" className="game_select"/>
                                </label>
                                <div className="modal__overlay">
                                    <div className="modal__box">
                                      
                                        <div className="sub-title">
                                            <label>游戏类型</label>
                                            
                                        </div>

                                        <div className="game-bettype-group fn-clear">
                                            {otherCategorys.map(category =>
                                                <label key={category.Name}
                                                       onClick={() => this.onCategoryClick(category)}>
                                                    {category.Name}</label>)
                                            }
                                            <ul>

                                                {this.state.selectedCategory.Sorts.map(sort =>
                                                    <div className="bettype-warp">
                                                        <li key={sort.Name}
                                                           // className={sort == this.state.selectedSort ? "active" : ""}
                                                            onClick={() => this.onSortClick(sort)}>{sort.Name}</li>
                                                    </div>
                                                )
                                                }

                                                <div className="clearfix"></div>
                                            </ul>
                                        </div>

                                        <div className="game-bettype-types">
                                            <div className="sub-title">
                                                <label>玩法类型</label>
                                            </div>

                                            {this.state.selectedSort.Kinds.map((kind,i) =>

                                                <div className="fn-clear tab" key={kind.Name}>
                                                    <Collapse defaultActiveKey={['0','1','2']} onChange={callback}>
                                                        <Panel header={kind.Name} key={i.toString()}>
                                                            {this.sort(kind.Types)
                                                               
                                                                  .map(type =>
                                                                <label htmlFor="modal" className="modal_close">
                                                                    <p

                                                                        key={type.Name}
                                                                       // className={type == this.state.selectedType ? "active" : ""}
                                                                        onClick={() => this.onTypeClick(type)}>
                                                                        <a style={ ((type.Name == this.state.selectedType.Name) && (type.Kind==this.state.selectedType.Kind)) ? {color:'red'} : {}}>
                                                                            {type.Name}
                                                                            
                                                                        </a>
                                                                    </p>
                                                                </label>
                                                            )
                                                            }
                                                        </Panel>
                                                    </Collapse>
                                                </div>
                                            )}
                                        </div>                     
                                    </div>
                                </div>                 
                            </div>         
                        </Card>
                        <Card title="余额">
                            <p className="yen_sym"><span>&#165; </span>{userInfo && RenderHelper.money_format(userInfo.Balance)}</p>
                        </Card>
                        <div className="clearfix"></div>
                    </div>
                </div>
{/*
                <div className="infor-pop">
                    <div id="popup1" className="overlay">
                        <div className="popup">
                            <h2>范例</h2>
                            <a className="close" href="#">&times;</a>
                            <div className="content">
                                <Tooltip placement="topRight" trigger="click" title={this.state.selectedType.Explain}>
                                    <p>{this.state.selectedType.Explain}</p>
                                </Tooltip>
                                <div className="icon-infor">
                                    <Tooltip title={this.state.selectedType.Example}>
                                        <Icon type="question-circle"/>
                                    </Tooltip>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
*/}
                <Modal
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    footer={null}
                    header={null}
                    className="explain_modal_style"
                    >
                    <h2>范例</h2>
                   <p>{this.state.selectedType.Explain}</p>
                   <div className="tooltip_style">
                        <Tooltip title={this.state.selectedType.Example}>
                            <Icon type="question-circle"/>
                        </Tooltip>
                    </div>
                </Modal>
              
            </div>
        );
    }
}

BetTypeSelect.propTypes = {
    // 游戏组
    Group: PropTypes.string.isRequired,
    // 选中游戏玩法改变
    onChange: PropTypes.func
}

module.exports = connect(state => ({
    userinfo: state.user.user,
    game_type_info: state.gamedata.game_type_info,
    loading: state.loading.models.gamedata
}))(BetTypeSelect);
