import React from 'react';
import router from 'umi/router';
import {Modal, Toast} from "antd-mobile";
import { message } from 'antd';

import Auth from "@/utils/auth";


/**
 * 组件基类
 * 在这里实现一些通用的方法
 */

 const alert = Modal.alert;

class BasicComponent extends React.Component {

    constructor(props) {
        super(props);
    }

    /* ---------------------------- 系统函数 ------------------------------*/
    /**
     * executed before rendering
     * @return {[type]} [description]
     */
    componentWillMount() {
    }

    /**
     * executed after first render only on the client side
     * @return {[type]} [description]
     */
    componentDidMount() {
    }

    /**
     * invoked as soon as the props are updated before another render is called.
     * @param  {[type]} newProps [description]
     * @return {[type]}          [description]
     */
    componentWillReceiveProps(newProps) {
    }

    /**
     * should return true or false value. This will determine if component will be updated or not. 
     * This is set to true by default. If you are sure that component doesn't need to render after state or props are updated, 
     * you can return false value.
     * @param  {[type]} nextProps [description]
     * @param  {[type]} nextState [description]
     * @return {[type]}           [description]
     */
    shouldComponentUpdate(nextProps, nextState) {
        return nextProps != this.props || nextState != this.state;
    }

    /**
     * called just before rendering.
     * @param  {[type]} nextProps [description]
     * @param  {[type]} nextState [description]
     * @return {[type]}           [description]
     */
    componentWillUpdate(nextProps, nextState) {
    }

    /**
     * called just after rendering.
     * @param  {[type]} prevProps [description]
     * @param  {[type]} prevState [description]
     * @return {[type]}           [description]
     */
    componentDidUpdate(prevProps, prevState) {
    }

    /**
     * called after the component is unmounted from the dom
     * @return {[type]} [description]
     */
    componentWillUnmount() {
    }

    /* ---------------------------- 自定义函数 ------------------------------*/

    /**
     * 获取父级组件
     * @return {[type]} [description]
     */
    get parent() {
        return this._reactInternalInstance._currentElement._owner._instance;
    }

    get isLogin() {
        return Auth.isLogged();
    }

    get realname() {
        return Auth.get('realname');
    }

    logout() {
        alert('退出登入', '您确定要退出吗?', [
            { text: 'Cancel', onPress: () => console.log('cancel') },
            { text: 'Ok', onPress: () => {Auth.logout(); router.push("/"); } },
          ])

       
    }

    /**
     * 跳转到指定地址(Route跳转，不刷新)
     * @param  {[type]} url [description]
     * @return {[type]}     [description]
     */
    goto(url) {
        router.push(url);
    }

    /**
     * 跳转到指定页面(强制跳转，刷新)
     * @param  {[type]} url [description]
     * @return {[type]}     [description]
     */
    redirect(url) {
        window.location.replace(url);
    }

    /**
     * action断言，用于Action返回中调用。通用方法，避免编写大量重复代码
     * @param  {[type]} action  Action对象
     * @param  {dict} options {fnOK,fnError,...} 
     * @return {[type]}         [description]
     */
    assert(data, options) {
        if (data.code === 0) {
            const fnOk = options && options.fnOk;
            fnOk ? fnOk(data) : Toast.success("操作成功");//message.success("操作成功");
        } else {
            const fnError = options && options.fnError;
            fnError ? fnError(data) : Toast.fail(data.message);//message.error(data.message);
        }
    }
}

module.exports = BasicComponent;
