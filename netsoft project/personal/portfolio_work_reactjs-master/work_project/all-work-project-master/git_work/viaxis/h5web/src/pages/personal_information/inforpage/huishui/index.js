import React, { Component } from 'react';
import { connect } from 'dva';
import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';
import { List, Pagination, Icon  } from 'antd-mobile';
import DateRange from '@/components/DateRange';


import { object } from 'prop-types';
import * as Utils from "@/utils/common";
import * as Constants from "@/app_constants";
import * as Enums from "@/app_enum";
import * as RenderHelper from "@/utils/render_helper";
import BillFlow from '../../../../components/BillFlow';

import pending from '../../../../assets/images/icon/pending.png';
import zhui from '../../../../assets/images/icon/zhui.png';
import game from '../../../../assets/images/game-icon/game01.png';

const Item = List.Item;
const Brief = Item.Brief;



class HuiShui extends Page {
  title = "流水记录"

  constructor(props) {
      super(props);
      this.state = {
        visible: false,
        queryDate:[]
      }

      this.querys = this.defaultQuerys = Object.assign({}, { not_trace: true });

  }

  componentDidMount(){
    this.getBillflow();
  }

  getBillflow(){
    this.props.dispatch({ type: "billflow/queryBillflow", payload:this.querys});
  }

  onDateRangeChange = (value) => {
    console.log(value);
    //let fmtValue = dateHelper.dateRangeFormat(value);
    let date = [];
    date.push(value[0].toLocaleString());
    date.push(value[1].toLocaleString());
    this.setState({queryDate:date});

    Object.assign(this.querys,this.querys, {"CreateTime": date});
    this.getBillflow();
}

onPaginationChange(val){
  console.log(val);
  Object.assign(this.querys,this.querys, {PageIndex: val});
  this.getBillflow();
}

  getNumberOfPages(total, pageSize){

    var pg = Math.floor(parseInt(total)/parseInt(pageSize));
    if((parseInt(total)%parseInt(pageSize))!=0)
      pg+=1;
  
      return pg;
  
   }



  render() {
    var { billflow } = this.props;
    let data = billflow.Data || [];

    let page = this.getNumberOfPages(billflow.TotalCount, billflow.PageSize);
    let idx = billflow.PageIndex;

    const showDetail = (flow) => {
      const dialog = RenderHelper.dialog(null,
          <BillFlow flowData={flow}  close={ ()=> close() } />);

      const close = () => dialog.destroy();
  }


    return (
      <div className="fund-history ">
        <div className="top-header"> </div>

        {/* date */}
        <List className="date-select">
          <Item><DateRange onChange = {this.onDateRangeChange.bind(this)}/></Item>
        </List>

        <List renderHeader={() => '流水信息'} className="my-list ">

        { data.map((element,i) => {
          return(

            <Item key = {i}  extra={
              <div className="bet-money add">
                <span className={element.Amount >= 0 ? "fn-color-green" : "fn-color-red"}>￥{element.Amount}</span>
              </div>
            }
              align="top"
              multipleLine
              onClick={()=>{
                showDetail(element);
              }}>

              <span className="title-warp">流水类型：{Constants.GameSubType[element.SubType]}</span>
              
              {/* <span className="title-warp">流水号：{element.Txid}</span> */}
              <Brief style={{fontSize: '12px', color: '#cccccc'}}>流水号：{element.Txid}</Brief>
              <Brief style={{fontSize: '12px', color: '#cccccc'}}>{element.CreateTime}</Brief>

            </Item>
          )
        })}

         {
          billflow.TotalCount==0? (<span>没有数据</span>) : (<p></p>)
         }

  
        </List>
        {
            (page < 2) ? <p></p>:
            <Pagination total={page}
              className="custom-pagination-with-icon"
              current={idx}
              locale={{
                prevText: (<span className="arrow-align"><Icon type="left" />上一页</span>),
                nextText: (<span className="arrow-align">下一页<Icon type="right" /></span>),
              }}
              onChange={this.onPaginationChange.bind(this)}
           />
          }
      </div>
    );
  }
};

export default connect(state => ({
  billflow: state.billflow,
  loading: state.loading.models.billflow
}))(Form.create()(HuiShui));
