import * as gameService from '@/services/game';

export default {
    namespace: 'dealinfo',
    state: {
        deals: {},
    },
    reducers: {
        saveDeals(state, { payload }) {
            return { ...state, deals: payload.data };
        },
    },
    effects: {
        *DealInfo({ payload }, { call, put }) {
            const data = yield call(gameService.queryDealInfo, payload);
            yield put({
                type: 'saveDeals', payload: data
            });
        },
        *bet({ payload }, { call, put }) {
            const data = yield call(gameService.bet, payload);
            //TODO: fetch dealinfo to update.
            return data;
        }
    },
};