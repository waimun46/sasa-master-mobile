import React, {Component} from 'react';
import { connect } from 'dva';
import Loading from '@/components/Loading';

import { Picker, List, InputItem, WhiteSpace, Toast, Modal } from 'antd-mobile';
import { notification, Alert, Icon,Spin } from "antd";

import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';

import Link from 'umi/link';
import CountDownButton from "@/components/CountDownButton";
import * as Utils from "@/utils/common";
import * as RenderHelper from "@/utils/render_helper";
import { object } from 'prop-types';
import router from 'umi/router';

const Item = List.Item;
const alert = Modal.alert;

const colorStyle = {
    display: 'inline-block',
    verticalAlign: 'middle',
    width: '16px',
    height: '16px',
    marginRight: '10px',
  };

class Withdraw extends Page {
    title = "快速提现"

    constructor(props) {
        super(props);
        this.state = {
          loading: false,
          // 是否已设置安全密码
          hasSafePassword: true,
          cards : [],
          cardId : 0
        };
      }




      componentDidMount() {
        const { balance, cards, dispatch } = this.props;
        this.getWitdhrawInfo();

        if (!cards.data || cards.data.length == 0)
          this.queryBankCards();

        if (balance === null)
          dispatch({ type: "global/getBalance" });



      }

      handleResetSubmit(e) {
        e.preventDefault();
        this.props.form.resetFields();
        this.setState({cardId:0});

      }


      queryBankCards() {
        const { dispatch } = this.props;
        dispatch({ type: "bank/getBankCards" }).then(rsp => {
          this.assert(rsp, {
            fnOk: it => {
              if ((!it || it.data.length === 0))
              {
                Toast.fail('您未添加银行卡", "您需要添加银行卡后才可继续下列的操作', 2, () => {
                  this.goto("/bank?redirect=/withdraw");
                })

              }

              else
              {
                let data = [];
                for(var i=0; i< it.data.length; i++)  {
                    data.push({label: it.data[i].BankName +"  尾号"+ "(**"+it.data[i].AccountNo.slice(-4) +")", value: it.data[i].Id});
                }
                this.setState({
                  cards: data
                });
              }
            }
          });
        });
      }

      getWitdhrawInfo() {
        this.props.dispatch({ type: "withdraw/getWithdrawInfo" });
      }

      queryHasSafePassword() {
        this.props.dispatch(Passport.hasSafePassword()).then(it => {

          const resp = it.response;
          if (resp.code == 0) {
            this.setState({ hasSafePassword: resp.data });
          }

        });
      }


    onPickerChange = (card) => {
        this.setState({
          cardId: card[0],
        });
      };

      handleSubmit(e) {
        e.preventDefault();

        const { dispatch } = this.props;

        this.form.validateFields((errors, values) => {

          if (errors) return;

          this.setState({ loading: true });

          dispatch({
            type: "withdraw/addWithdraw",
            payload: { bankId: this.state.cardId, amount: values.amount, password: values.password }
          }).then(rsp => {
            this.setState({ loading: false });
            this.assert(rsp, {
              fnOk: (resp) => {

                alert('申请成功!', '您的提现申请已提交，你可以在提现历史查看当前进度', [
                  { text: 'OK', onPress: () => console.log('cancel') },
                  { text: '查看', onPress: () => router.push('/personal_information/inforpage/fund-management/content/WithdrawalHistory') },
                ])


                dispatch({ type: "global/getBalance" });
                this.props.form.resetFields();
              }
            });
          });
        });
      }

    render() {

      const isIPhone = new RegExp('\\biPhone\\b|\\biPod\\b', 'i').test(window.navigator.userAgent);
      let moneyKeyboardWrapProps;
      if (isIPhone) {
        moneyKeyboardWrapProps = {
          onTouchStart: e => e.preventDefault(),
      };
    }

    const { getFieldDecorator, getFieldProps } = this.props.form;

    const formItemLayout = {
      labelCol: { span: 6 },
      wrapperCol: { span: 12 },
    };

    const tailFormItemLayout = {
      wrapperCol: {
        span: 8,
        offset: 6,
      },
    };

    const { loading, balance, loadingBalance, withdraw, cards } = this.props;
    const minAmt = withdraw.info ? withdraw.info.Min : 0;
    const maxAmt = Math.min(balance || 0, withdraw.info ? withdraw.info.Max : 0);

    const infoTipsTemplate = "提现所产生的银行手续费由平台为您免除。提现限额：最低 <b>{0}</b> 元，最高 <b>{1}</b> 元, 已提现 <b>{2}</b> 次，还可以提现 <b>{3}</b> 次。";
        const infoTips = withdraw.info ?
            Utils.format(infoTipsTemplate,
                Utils.milliFormat(withdraw.info.Min),
                Utils.milliFormat(withdraw.info.Max),
                withdraw.info.Count,
                withdraw.info.MaxCount - withdraw.info.Count) :
            Utils.format(infoTipsTemplate, "loading", "loading", "loading", "loading");


    return (
        <div className="form_sty withdraw_sty">
          <div className="top-header"> </div>
          <div className="wrapper800">
                <Alert type="info" showIcon message="提现须知" description={<p dangerouslySetInnerHTML={{ __html: infoTips }}></p>} />
          </div>

          <Form form={this.props.form} ref={el => this.form = el} onSubmit={this.handleSubmit.bind(this)}>
            <List className="my-list">
              <Item extra={loadingBalance ? <Spin>loading...</Spin> : "¥" + Utils.milliFormat(balance)}>余额</Item>
            </List>

            <InputItem
              {...getFieldProps('amount', {
                rules: [{ required: true, message: '请输入提现金额' },
                        {
                          type: "number",
                          min: minAmt, max: maxAmt,
                          message: `提现金额必须大于等于${minAmt}小于等于${maxAmt}`,
                          transform(value) {
                              return parseInt(value);
                          }}],
              })}
              type="money"
              clear
              placeholder="0.00"
              extra="¥"
              ref="amountInput"
              moneyKeyboardAlign="left"
              moneyKeyboardWrapProps={moneyKeyboardWrapProps}
              >
                提现金额
              </InputItem>

            <List style={{ backgroundColor: 'white' }} className="picker-list">
              <Picker
              data={this.state.cards}
              value={{0:this.state.cardId}}
              cols={1}
              onChange={this.onPickerChange}>
                <List.Item className="withdraw_float" arrow="horizontal">提现银行卡{cards.length > 0 ? '': <Link className="bangding" to="/bank">绑定银行卡</Link>}</List.Item>
              </Picker>
            </List>

            <InputItem
              {...getFieldProps('password', {
                  rules: [{ required: true, message: '请输入安全密码' }],
              })}
              className="btn_withdraw"
              type="password"
              clear
              placeholder="安全密码"
              extra={<CountDownButton
                size="small" type="primary">
              <Link to="/passport/password/reset?redirect=/withdraw">忘记密码</Link>
              </CountDownButton>}
            >
                安全密码
            </InputItem>

            <div className="game-orders">
              <div className="ButtonBottom">
                <button className="btn_left" onClick={this.handleResetSubmit.bind(this)}>
                  <Icon type="reload"/> 重置
                </button>

                <button className="btn_right" loading={this.state.loading} htmlType="submit">
                  确定
                </button>

              </div>
              <div className="clearfix"></div>
            </div>

          </Form>
        </div>
        );
    }
};

export default connect(state => ({

  balance: state.global.balance,
  withdraw: state.withdraw,
  cards: state.bank.cards,
  loadingBalance: state.loading.models.global,
  loading: state.loading.models.witdhraw,
  loadingBank: state.loading.models.bank

}))(Form.create()(Withdraw));
