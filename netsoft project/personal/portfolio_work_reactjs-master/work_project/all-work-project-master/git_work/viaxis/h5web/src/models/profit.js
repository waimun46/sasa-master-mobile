import { routerRedux } from 'dva/router';
import * as reportService from '@/services/report';

export default {
    namespace: 'profit',
    state: {
        profitUser: {},
        profitGroupGameType: [],
        profitGameType: {},
        profitUserGameType: {}
    },
    reducers: {
        saveData(state, { payload }) {
            return { ...state, ...payload };
        },
    },
    effects: {
        *queryProfitUser({ payload: { userId, querys } }, { call, put }) {
            const resp = yield call(reportService.queryProfitUser, userId, querys);
            if (resp.code === 0)
                yield put({ type: "saveData", payload: { profitUser: resp.data } });
        },

        *queryProfitGameTypeGroup({ payload }, { call, put }) {
            const resp = yield call(reportService.queryProfitGameTypeGroup, payload);
            if (resp.code === 0)
                yield put({ type: "saveData", payload: { profitGroupGameType: resp.data } });
        },

        *queryProfitGameType({ payload: { userId, querys } }, { call, put }) {
            const resp = yield call(reportService.queryProfitGameType, userId, querys);
            if (resp.code === 0)
                yield put({ type: "saveData", payload: { profitGameType: resp.data } });
        },

        *queryProfitGameTypeByUser({ payload: { userId, querys } }, { call, put }) {
            const resp = yield call(reportService.queryProfitGameTypeByUser, userId, querys);
            if (resp.code === 0)
                yield put({ type: "saveData", payload: { profitUserGameType: resp.data } });
        },
    },
};