/**
 * Routes:
 *   - ./src/routes/AuthorizationRoute.js
 */

import { connect } from 'dva';
import { message, Row, Col, Alert, Tabs, Button, Input,Icon ,Avatar } from "antd";
import { List, InputItem, WhiteSpace } from 'antd-mobile';
import Link from 'umi/link';
import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';


import bankcard from '../../../../../../assets/images/icon/bankcard.png'
import alipay from '../../../../../../assets/images/icon/alipay.png'
import weixin from '../../../../../../assets/images/icon/weixin.png'
import QQ from '../../../../../../assets/images/icon/QQ.png'
import bipipay from '../../../../../../assets/images/icon/bipipay.png'

const Item = List.Item;
const Brief = Item.Brief;


class SelectRechargeMethod extends Page {
  title = "选择充值方式"


  render() {

    return (
      <div className="form_sty form-list-style form-select-transfer">
        <div className="top-header"> </div>
        <div className="form_title">
          <h5>选项列表</h5>
        </div>


        <List >
        <Link className="border-line" to="/personal_information/inforpage/fund-management/content/topup/Bipipay">
            <Item
              thumb={<img src={bipipay} alt="bipipay"/>}
              arrow="horizontal"
              onClick={() => {}}
            >电汇指引</Item>
          </Link>

          {/* <Link className="border-line" to="/personal_information/inforpage/fund-management/content/topup/BankTransfer">
            <Item
              thumb={<img src={bankcard} alt="bankcard"/>}
              arrow="horizontal"
              onClick={() => {}}
            >银行转账</Item>
          </Link>

          <Link className="border-line" to="/personal_information/inforpage/fund-management/content/topup/Alipay">
            <Item
              thumb={<img src={alipay} alt="alipay"/>}
              arrow="horizontal"
              onClick={() => {}}
            >支付宝转账</Item>
          </Link>

          <Link className="border-line" to="/personal_information/inforpage/fund-management/content/topup/weixin">
            <Item
              thumb={<img src={weixin} alt="weixin"/>}
              arrow="horizontal"
              onClick={() => {}}
            >微信支付</Item>
          </Link>

          <Link className="border-line" to="/personal_information/inforpage/fund-management/content/topup/QQpay">
            <Item
              thumb={<img src={QQ} alt="QQ"/>}
              arrow="horizontal"
              onClick={() => {}}
            >QQ钱包</Item>
          </Link>

          <Link className="border-line" to="/personal_information/inforpage/fund-management/content/topup/BankCardTransfer">
            <Item
              thumb={<img src={bankcard} alt="bankcard"/>}
              arrow="horizontal"
              onClick={() => {}}
            >银行卡支付</Item>
          </Link> */}

        </List>



      </div>


    );
  }
};

export default connect(state => ({
}))(Form.create()(SelectRechargeMethod));
