import React, { Component } from 'react';
import {Icon} from "antd";
import * as betHelper from '../../utils/bet_helper';


class BetRecord extends Component
{
    constructor(props){
        super(props);
    }

    render(){
        const {betData} = this.props;

        return(
            <div class="popup_new">
                            <div className="popup-inner ">
                                <h2>投注记录</h2>

                                <div class="content">
                                    <table>
                                        <thead>
                                            <tr className="tr-style">
                                                <th>彩种:</th>
                                                <th>期号:</th>
                                                <th>玩法:</th>
                                                <th>下注号码:</th>
                                                {/* <th>开奖号码:</th> */}

                                                <th>状态:</th>
                                                <th>投注金额:</th>
                                                <th>总金额:</th>
                                                <th>盈亏:</th>
                                                <th>赔率:</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr className="tr2-style">
                                                <td
                                                    style={{
                                                        color: 'blue'
                                                    }}>{betHelper.getGameNameByGroupId(betData.GroupId)}</td>
                                                <td>{betData.Qh}</td>
                                                <td>{betData.GameTypeName}</td>
                                                <td
                                                    style={{
                                                        color: 'red'
                                                    }}>{betHelper.betNumBeautify(betData.BetNumber)}</td>

                                                {/* <td>9</td> */}
                                                <td>{betHelper.getGameBetStatus(betData.Status,betData.WinBets,betData.BetAmount,betData.Scale,betData.Turnover)}</td>
                                                <td>￥{betData.BetAmount}</td>

                                                <td><span>￥</span>{betData.Turnover}</td>
                                                <td className={betData.Profit >= 0 ? "fn-color-green" : "fn-color-red"}><span>￥</span>{betData.Profit}</td>
                                                <td>{betData.Scale}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {/* <a class="close" href="#">
                                <div className="btn-popup close">
                                    <h4>关闭</h4>
                                </div>
                            </a> */}
                        </div>


        )
    }

}

export default BetRecord;
