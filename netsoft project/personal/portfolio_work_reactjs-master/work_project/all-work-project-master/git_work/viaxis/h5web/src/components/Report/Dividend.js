import React, { Component } from 'react';

import * as betHelper from '../../utils/bet_helper';


class Dividend extends Component
{
    constructor(props){
        super(props);
    }

    render(){
        const {teamData} = this.props;

        return(
            <div class="popup_new">
                            <div className="popup-inner">
                                <h2>查看下级</h2>

                                <div class="content">
                                    <table>
                                        <thead>
                                            <tr className="tr-style">
                                                
                                                <th>日期:</th>
                                                <th>彩种:</th>
                                                <th>用户数:</th>
                                                <th>订单数:</th>
                                                <th>总投注:</th>
                                                <th>有效投注:</th>
                                                <th>盈亏:</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr className="tr2-style">
                                                <td>{teamData.UserKind}</td>
                                                <td>{teamData.Balance}</td>
                                                <td>{teamData.ChildCount}</td>                                          
                                                <td>{teamData.ChildBalance}</td>
                                                <td>{teamData.Backpct}%</td>
                                                <td>{teamData.Status}</td>
                                                <td>{teamData.DailyWages}%</td> 
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {/* <a class="close" href="#">
                                <div className="btn-popup close">
                                    <h4>关闭</h4>
                                </div>
                            </a> */}
                        </div>


        )
    }

}

export default Dividend;
