
// ref: https://umijs.org/config/
export default {
  plugins: [
    // ref: https://umijs.org/plugin/umi-plugin-react.html
    ['umi-plugin-react', {
      antd: true,
      dva: true,
      dynamicImport: true,
      title: 'h5web',
      dll: false,
      routes: {
        exclude: [
          /components/,
        ],
      },
      hardSource: true,
    }],
  ],
  define: {
    // restful domain
    "apiUrl": "",
    // websocket url
    "url.ws": "",
    // 彩种号码球数量
    "config.GameDataNum": {
      FC3D: 3,
      CQSSC: 5,
      KL8: 20,
      KL10: 8,
      KENO: 20,
      PL3: 3,
      FFC: 5,
      ZRC: 5,
      PK10: 10
    }
  },
  proxy: {
    // "/api": {
    //   "target": "http://jsonplaceholder.typicode.com/",
    //   "changeOrigin": true,
    //   "pathRewrite": { "^/api": "" }
    // }
    "/captcha": {
      "target": "http://35.220.150.212:5000",
      "changeOrigin": true,
    },
    "/api": {
      "target": "http://35.220.150.212:5000",
      "changeOrigin": true,
    }
  },
}
