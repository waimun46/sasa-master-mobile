import PropTypes from 'prop-types';
import { connect } from 'dva';
import { message, Table, Breadcrumb, Button, Select, Input, Icon } from 'antd';
import Link from 'umi/link';

import BasicComponent from "@/components/BasicComponent";
import Form from '@/components/Form';
import * as RenderHelper from "@/utils/render_helper";

class ProfitGameType extends BasicComponent {

    constructor(props) {
        super(props);
        this.pageIndex = 1;
    }

    /* ------------------ Sys -------------------- */
    componentWillMount() {
        this.queryData(this.props);
    }

    componentWillReceiveProps(props) {
        if (props != this.props && props.querys != this.props.querys)
            this.queryData(props);
    }

    /* ------------------ Functions -------------------- */

    queryData(props) {
        const { userId, querys } = props;
        this.props.dispatch({
            type: "profit/queryProfitGameType",
            payload: { userId, querys: Object.assign({}, querys, { pageIndex: this.pageIndex }) }
        });
    }

    /* ------------------ Events -------------------- */


    /* ------------------ Renders -------------------- */

    render() {
        const self = this;
        const { profitGameType, loading } = this.props;
        let data = profitGameType.Data || [];

        const pagination = {
            total: profitGameType.count || 0,
            onChange(current) {
                self.pageIndex = current;
                self.queryData(self.props);
            },
        };

        return (
            <Table loading={loading} columns={this.columns} dataSource={data} pagination={pagination}
                title={() => <Button icon="rollback" type="primary" onClick={() => this.props.onGoBackClick()}>返回</Button>} rowKey="rn" />
        );
    }

    get columns() {
        const self = this;
        const columns = [
            { title: '#', render: RenderHelper.render_index },
            {
                title: "日期", render: it => {
                    if (it.ToDate != it.Date)
                        return `${it.Date} - ${it.ToDate}`;
                    return it.Date;
                }
            },
            { title: "彩种", dataIndex: "GroupName" },
            {
                title: "玩法", dataIndex: "GameName", render: (it, record) => {
                    return <a onClick={() => this.props.onGameTypeClick(record.GameType, record.Date, record.ToDate)}>{it}</a>
                }
            },
            { title: "用户数", dataIndex: "UserCount" },
            {
                title: "订单数", dataIndex: "OrderCount", className: "column-money", render: (it, record) => {
                    return <a target="_blank" href={`/deals?game_type=${record.GameType}&from_time=${record.Date}&to_time=${record.ToDate}235959`}>{it}</a>
                }
            },
            { title: "总投注", dataIndex: "TotalBet", className: "column-money", render: RenderHelper.money_format },
            { title: "有效投注", dataIndex: "ValidBet", className: "column-money", render: RenderHelper.money_format },
            { title: "盈亏", dataIndex: "Profit", className: "column-money", render: RenderHelper.money_format_color },
        ];

        return columns;
    }
}
ProfitGameType.PropTypes = {
    onGameTypeClick: PropTypes.func.isRequired,
    onGoBackClick: PropTypes.func.isRequired
}
export default connect(state => ({
    userinfo: state.global.userinfo,
    profitGameType: state.profit.profitGameType,
    loading: state.loading.models.profit,
}))(ProfitGameType);