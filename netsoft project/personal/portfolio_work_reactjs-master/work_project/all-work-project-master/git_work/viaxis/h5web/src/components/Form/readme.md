# README

用于Antd Mobile的Form组件

## Example

```jsx
 <Form form={this.props.form} ref={el => this.form = el} onSubmit={this.handleUpdateSubmit.bind(this)}>
     <InputItem
          {...getFieldProps('autofocus', {
              rules: [{ required: true, message: '请输入右侧验证码' }, {
                          min: 6, max: 20, message: '安全密码长度要求为6到20个字符之间。'
                          }],
                      })}
                      clear
                      placeholder="auto focus"
                      onBlur={(v) => { console.log('onBlur', v); }}
                  >标题</InputItem>
      <List>
          <InputItem
                {...getFieldProps('money2', {
                    normalize: (v, prev) => {
                        if (v && !/^(([1-9]\d*)|0)(\.\d{0,2}?)?$/.test(v)) {
                            if (v === '.') {
                                return '0.';
                            }
                            return prev;
                        }
                        return v;
                    },
                })}
                type='money'
                placeholder="money format"
                ref={el => this.inputRef = el}
                onVirtualKeyboardConfirm={v => console.log('onVirtualKeyboardConfirm:', v)}
                clear
            >数字键盘</InputItem>
      </List>
 </Form>


 handleUpdateSubmit(e) {
      e.preventDefault();

      const { dispatch } = this.props;

      this.form.validateFields((errors, values) => {
 
          console.log(errors);
          console.log(values);
 
          if (errors) return;
      });
 }
```


 validateFields   是自定义方法，用于代替默认form的validateFields方法, 该方法会改变组件显示的状态.也可以自行使用 props.form.validateFields。
 ref              为了使用组件validateFields方法,需要声明ref