import React, {Component} from 'react';
import { Picker, List } from 'antd-mobile';
import icon4 from '../../../../assets/images/game-icon/game01.png';


const colorStyle2 = {
    display: 'inline-block',
    verticalAlign: 'middle',
    width: '30px',
    height: '30px',
    marginRight: '10px',
  };

const gamesSelect = [
    {
        label:
        (<div>
          {/* <img src={icon4} style={{ ...colorStyle2}}/> */}
          <span>福彩3D</span>
        </div>),
        value: 1,
      },
      {
        label:
        (<div>
          {/* <img src={icon4} style={{ ...colorStyle2}}/> */}
          <span>重庆时时彩</span>
        </div>),
        value: 2,
      },
      {
        label:
        (<div>
          {/* <img src={icon4} style={{ ...colorStyle2}}/> */}
          <span>北京快乐8分</span>
        </div>),
        value: 3,
      },
      {
        label:
        (<div>
          {/* <img src={icon4} style={{ ...colorStyle2}}/> */}
          <span>重庆幸运农场</span>
        </div>),
        value: 4,
      },
      {
        label:
        (<div>
          {/* <img src={icon4} style={{ ...colorStyle2}}/> */}
          <span>KENO</span>
        </div>),
        value: 5,
      },
      {
        label:
        (<div>
          {/* <img src={icon4} style={{ ...colorStyle2}}/> */}
          <span>排列3</span>
        </div>),
        value: 6,
      },
      {
        label:
        (<div>
          {/* <img src={icon4} style={{ ...colorStyle2}}/> */}
          <span>分分彩</span>
        </div>),
        value: 7,
      },
      {
        label:
        (<div>
          {/* <img src={icon4} style={{ ...colorStyle2}}/> */}
          <span>北京PK10</span>
        </div>),
        value: 9,
      },
  ];


// const playselect = [
//     {
//       label:
//       (<div>
//         <span>大小单双</span>
//       </div>),
//       value: '#FF0000',
//     },
//     {
//       label:
//       (<div>
//         <span>三星</span>
//       </div>),
//       value: '#00FF00',
//     },
//   ];

//   const colors = [
//     {
//       label:
//       (<div>
//         <span>组选</span>
//       </div>),
//       value: '#FF0000',
//     },
//     {
//       label:
//       (<div>
//         <span>直选</span>
//       </div>),
//       value: '#00FF00',
//     },
//   ];

class GameType extends Component {
    state = {
       // colorValue: ['#00FF00'],
       // playSelect: ['#00FF00'],
        gamesSelect: 0,
      };

    onChangeGameSelect = (gamesSelect) => {
        this.setState({
            gamesSelect: gamesSelect[0],
        });
    };
    // onChangePlaySelect = (playselect) => {
    //     this.setState({
    //         playSelect: playselect,
    //     });
    // };

    // onChangeColor = (color) => {
    //     this.setState({
    //       colorValue: color,
    //     });
    // };


    render() {
        return (
            <div className="game-type">
                <div className="game-select-type">
                    <div className="game-inner">
                        <List renderHeader={() => '玩法选项'} className="picker-list">
                            <Picker
                                data={gamesSelect}
                                value={this.state.gamesSelect}
                                cols={1}
                                onChange={this.onChangeGameSelect}>
                                <List.Item arrow="horizontal">选择游戏</List.Item>
                            </Picker>
                            {/* <Picker
                                data={playselect}
                                value={this.state.playSelect}
                                cols={1}
                                onChange={this.onChangePlaySelect}
                            >
                                <List.Item arrow="horizontal">游戏类型</List.Item>
                            </Picker>
                            <Picker
                                data={colors}
                                value={this.state.colorValue}
                                cols={1}
                                onChange={this.onChangeColor}

                            >
                                <List.Item arrow="horizontal" >
                                  <div className="pick-sty">选三中三</div>
                                </List.Item>
                            </Picker>
                            <Picker
                                data={colors}
                                value={this.state.colorValue}
                                cols={1}
                                onChange={this.onChangeColor}

                            >
                                <List.Item arrow="horizontal" >
                                <div className="pick-sty">前三</div>
                                </List.Item>
                            </Picker> */}
                        </List>
                    </div>
                </div>

            </div>
        );
    }
};

export default GameType;
