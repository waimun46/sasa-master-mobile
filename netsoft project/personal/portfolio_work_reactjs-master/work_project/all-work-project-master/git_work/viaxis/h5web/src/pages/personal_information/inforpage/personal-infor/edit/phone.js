
import { connect } from 'dva';
import { Icon } from "antd";
import Link from 'umi/link';
import { List, InputItem, WhiteSpace,  Toast } from 'antd-mobile';
import router from 'umi/router';

import CountDownButton from "@/components/CountDownButton";
import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';

class EditPhone extends Page{

    constructor(props) {
        super(props);
        this.state = {
          // 提交按钮
          loading: false,
          // 发送验证码按钮
          captchaSending: false,
          // 发送按钮倒计时
          target: 0,
          phoneNumber:""
        }
    }


    handleResetSubmit(e) {
        e.preventDefault();
        this.props.form.resetFields(); 
    }

    sendCaptchaClick(e) {
        e.preventDefault();
        const { dispatch } = this.props;
      

        let newPhoneNumber = "";
        this.form.validateFields((errors, values)=>{
            newPhoneNumber = values.newPhoneNumber;
        });

        if(newPhoneNumber == undefined)   // need to validate phone
        {
            Toast.fail("请输入新手机号码");
            return;
        }

        dispatch({ type: "user/sendNewPhoneTicket", payload: {newPhoneNumber: newPhoneNumber, type: true }}).then(resp => {
          this.setState({ captchaSending: false });
          let seconds = 0;
          if (resp.code === 0) {
            seconds = 60;
            Toast.success("验证码已发送到您的新手机");
         
          } else {
            seconds = resp.data;
            Toast.fail(resp.message);
        
          }
          if (seconds > 0)
            this.setState({ target: new Date().getTime() + seconds * 1000 });
    
        });
        this.setState({ captchaSending: true });
    }

    handleSubmit(e){
        e.preventDefault();
        const { dispatch } = this.props;
        this.form.validateFields((errors, values) => {
          if (errors) return;
    
          this.setState({ loading: true });
    
          dispatch({
            type: "user/bindNewPhone",
            payload: { verificationCode: values.captcha, newPhoneNumber:values.newPhoneNumber }
          }).then(resp => {
                this.setState({ loading: false }); 
                if (resp.code === 0) {
                    let msg = "手机号码更新成功";
                    Toast.success(msg);
                    this.props.form.resetFields();
                    router.goBack();
                } 
                else {
                    let msg=resp.message;
                    Toast.fail(msg);
                }
            });
        })
    }


    render(){
        const { getFieldDecorator, getFieldProps } = this.props.form;

        return(
            
        <div className="form_sty ">
            <div className="top-header"> </div>
            <div className="form_title">
                <h5>填写信息</h5>
            </div>
            <Form form={this.props.form} ref={el => this.form = el} onSubmit={this.handleSubmit.bind(this)}>
                <InputItem
                    {...getFieldProps('newPhoneNumber', {
                    rules: [{
                        required: true, message: '请输入新手机号码',
                        }],
                    })}
                   
                    clear
                    placeholder="请输入新手机号码">
                    
                    手机号码
                </InputItem>

                <InputItem
                    {...getFieldProps('captcha', {
                    rules: [{ required: true, message: '请输入您接收到验证码' }],
                    })}
                    maxLength="6"
                    clear
                    placeholder="邮箱验证码"
                    extra={
                    <CountDownButton loading={this.state.captchaSending}
                                            size="small" type="primary"
                                            target={this.state.target}
                                            onClick={this.sendCaptchaClick.bind(this)}>
                        发送验证码
                    </CountDownButton>}>
                    验证码
                </InputItem>

            <div className="game-orders">
                <div className="ButtonBottom">
                    <button className="btn_left" onClick = {this.handleResetSubmit.bind(this)}>
                        <Icon type="reload"/> 
                        重置
                    </button>
                    <button className="btn_right" 
                            loading={this.state.loading} 
                            htmlType="submit">
                    确定
                    </button>
                </div>
                <div className="clearfix"></div>
            </div>
            
            </Form>
        </div>
        )
    }
}


export default connect(state => ({
   // loading: state.loading.models.user,
  }))(Form.create()(EditPhone));
  