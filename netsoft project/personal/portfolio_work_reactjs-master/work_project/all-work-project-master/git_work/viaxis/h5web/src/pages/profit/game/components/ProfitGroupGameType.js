import PropTypes from 'prop-types';
import { connect } from 'dva';
import { message, Table, Breadcrumb, Button, Select, Input, Icon } from 'antd';
import Link from 'umi/link';

import BasicComponent from "@/components/BasicComponent";
import Form from '@/components/Form';
import * as RenderHelper from "@/utils/render_helper";

class ProfitGroupGameType extends BasicComponent {

    constructor(props) {
        super(props);
    }

    /* ------------------ Sys -------------------- */
    componentWillMount() {
        this.queryData(this.props);
    }

    componentWillReceiveProps(props) {
        if (props != this.props && props.querys != this.props.querys)
            this.queryData(props);
    }

    /* ------------------ Functions -------------------- */

    queryData(props) {
        const { querys } = props;
        this.props.dispatch({ type: "profit/queryProfitGameTypeGroup", payload: querys });
    }

    /* ------------------ Events -------------------- */


    /* ------------------ Renders -------------------- */

    render() {
        const { profitGroupGameType, loading } = this.props;
        let data = profitGroupGameType || [];

        return (
            <Table loading={loading} columns={this.columns} dataSource={data} pagination={false} rowKey="GroupId" />
        );
    }

    get columns() {
        const self = this;
        const columns = [
            { title: '#', render: RenderHelper.render_index },
            {
                title: "日期", render: it => {
                    if (it.ToDate != it.Date)
                        return `${it.Date} - ${it.ToDate}`;
                    return it.Date;
                }
            },
            { title: "彩种", dataIndex: "GroupName", render: (it, profit) => <a onClick={() => this.props.onGroupClick(it.GroupId, it.Date, it.ToDate)}>{it}</a> },
            { title: "用户数", dataIndex: "UserCount" },
            {
                title: "订单数", dataIndex: "OrderCount", className: "column-money", render: (it, record) => {
                    return <a target="_blank" href={`/deals?group_id=${record.GroupId}&from_time=${record.Date}&to_time=${record.ToDate}235959`}>{it}</a>
                }
            },
            { title: "总投注", dataIndex: "TotalBet", className: "column-money", render: RenderHelper.money_format },
            { title: "有效投注", dataIndex: "ValidBet", className: "column-money", render: RenderHelper.money_format },
            { title: "盈亏", dataIndex: "Profit", className: "column-money", render: RenderHelper.money_format_color },
        ];

        return columns;
    }
}
ProfitGroupGameType.propTypes = {
    onGroupClick: PropTypes.func.isRequired
}
export default connect(state => ({
    userinfo: state.global.userinfo,
    profitGroupGameType: state.profit.profitGroupGameType,
    loading: state.loading.models.profit,
}))(ProfitGroupGameType);