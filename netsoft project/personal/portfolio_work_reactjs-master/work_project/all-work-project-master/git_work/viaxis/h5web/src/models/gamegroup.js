import * as gameService from '@/services/game';

export default {
    namespace: 'gameGroup',
    state: [],
    reducers: {
        saveGameGroup(state, { payload }) {
            return [...payload];
        },
    },
    effects: {

        *getGameGroup({ payload }, { call, put }) {
            const data = yield call(gameService.getGameGroup);
            yield put({
                type: 'saveGameGroup', payload: data.data
            });
        },
    }
}