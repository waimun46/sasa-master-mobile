import { GET, POST } from '@/utils/request';


export function getDepositList(payload) {
  let body = JSON.stringify(payload);
  return POST("/api/deposit/list", body);
}
