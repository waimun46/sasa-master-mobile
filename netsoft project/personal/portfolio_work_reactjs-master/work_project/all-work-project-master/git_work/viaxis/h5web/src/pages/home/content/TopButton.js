import React, {Component} from 'react';
import Link from 'umi/link';

import ybag2 from '../../../assets/images/icon/ybag2.png';
import wallet2 from '../../../assets/images/icon/wallet2.png';
import bettingrecord from '../../../assets/images/icon/bettingrecord.png';
import contact from '../../../assets/images/icon/contact.png';


class TopButton extends Component {


  render() {
    return (

      <div className="top-button">
        <Link to="/personal_information/inforpage/fund-management/content/topup/SelectRechargeMethod">
        <div className="warp">
          <div className="img-icon">
            <img src={ybag2} alt="ybag2"/>
          </div>
          <p>我要充值</p>
        </div>
        </Link>
        <Link to="/withdraw">
        <div className="warp">
          <div className="img-icon">
            <img src={wallet2} alt="wallet2"/>
          </div>
          <p>快速提现</p>
        </div>
        </Link>
        
        <Link to="/bet_history">
          <div className="warp">
            <div className="img-icon">
              <img src={bettingrecord} alt="bettingrecord"/>
            </div>
            <p>投注记录</p>
          </div>
        </Link>
        <div className="warp">
          <div className="img-icon">
            <img src={contact} alt="contact"/>
          </div>
          <p>在线客服</p>
        </div>
        <div className="clearfix"></div>
      </div>
    );
  }
}

export default TopButton;
