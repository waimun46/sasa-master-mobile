import React, {Component} from 'react';
import { connect } from 'dva';
import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';

import HotPromoBanner from './content/HotPromoBanner';


class Promotion extends Page {
    title = "优惠活动"

    render() {
        return (
          <div className="promotion">
            <div className="top-header"></div>
              <HotPromoBanner/>
          </div>
        );
    }
};

export default connect(state => ({

}))(Form.create()(Promotion));
