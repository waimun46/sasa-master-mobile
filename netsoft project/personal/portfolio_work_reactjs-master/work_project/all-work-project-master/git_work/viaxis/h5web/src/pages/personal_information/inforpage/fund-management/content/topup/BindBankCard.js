/**
 * Routes:
 *   - ./src/routes/AuthorizationRoute.js
 */

import { connect } from 'dva';
import { message, Row, Col, Alert, Tabs, Button, Input,Icon,Card  } from "antd";
import { List, InputItem, WhiteSpace } from 'antd-mobile';

import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';
import * as RenderHelper from "@/utils/render_helper";


const Item = List.Item;
const Brief = Item.Brief;


class BindBankCard extends Page {
  title = "绑定银行卡"
  constructor(props) {
    super(props);
    this.state = {
      // 提交按钮
      loading: false,
    }
    // 初始验证码地址,否则每次表单输内容验证码都会变化
    this.captchaUrl = RenderHelper.getCaptchaImgUrl();
  }

  /* ---------------------------- Sys Functions ------------------------------*/


  /* ---------------------------- Functions ------------------------------*/

  refreshCaptcha() {
    let captcha = this.props.form.getFieldInstance("captcha");
    captcha.clearInput();
    captcha.inputRef.focus();
    this.refs.captcha_img.src = RenderHelper.getCaptchaImgUrl();
  }

  /* ---------------------------- Events ------------------------------*/

  handleUpdateSubmit(e) {
    e.preventDefault();

    const { dispatch } = this.props;

    this.form.validateFields((errors, values) => {

      if (errors) return;

      dispatch({
        type: "user/updateLoginPassword",
        payload: { currentPwd: values.current_pwd, password: values.password, captcha: values.captcha }
      }).then(resp => {
        this.setState({ loading: false });
        if (resp.code == 0) {
          let msg = "您的密码已修改，请重新登录";
          message.success(msg);
          this.props.form.resetFields();
        } else {
          this.refreshCaptcha();
          message.error(resp.message);
        }

      });

      this.setState({ loading: true });
    })
  }

  checkPassword(rule, value, callback) {
    if (value && value !== rule.getFieldValue('password')) {
      callback(rule.message);
    } else {
      callback();
    }
  }

  /* ---------------------------- Renders ------------------------------*/

  render() {
    const { getFieldDecorator, getFieldProps } = this.props.form;
    return (
      <div className="form_sty">
        <div className="top-header"> </div>
        <div className="form_title">
          <h5>选项列表</h5>
        </div>
        <Form form={this.props.form} ref={el => this.form = el} onSubmit={this.handleUpdateSubmit.bind(this)}>

          <List >
            <Item
              arrow="down"
              onClick={() => {}}
            >选择银行</Item>

            <Item
              arrow="down"
              onClick={() => {}}
            >开户支行</Item>
          </List>


          <div className="form_title2">
            <h5>填写银行信息</h5>
          </div>
          <InputItem
            {...getFieldProps('current_pwd', {
              rules: [{
                required: true, message: '请输入开户人姓名',
              }],
            })}
            type=""
            cleartext
            placeholder="请输入开户人姓名">
            开户人姓名
          </InputItem>

          <InputItem
            {...getFieldProps('password', {
              rules: [{
                required: true, message: '请输入银行卡号',
              }],
            })}
            type="text"
            clear
            placeholder="请输入银行卡号">
            银行卡号
          </InputItem>

          <InputItem
            {...getFieldProps('cmf_pwd', {
              rules: [{
                required: true, message: '请再输入银行卡号',
              }],
            })}
            type=""
            cleartext
            placeholder="请再输入银行卡号">
            确认卡号
          </InputItem>

          <InputItem
            {...getFieldProps('cmf_name', {
              rules: [{
                required: true, message: '请输入安全密码',
              }],
            })}
            type="text"
            clear
            placeholder="请输入安全密码">
            安全密码
          </InputItem>


          <div className="game-orders">
            <div className="ButtonBottom">
              <button className="btn_left">
                <Icon type="reload"/> 重置
              </button>

              <button className="btn_right" loading={this.state.loading} htmlType="submit" >
                确定
              </button>

            </div>
            <div className="clearfix"></div>
          </div>


        </Form>

      </div>


    );
  }
};

export default connect(state => ({
  hasSafePassword: state.user.hasSafePassword,
  loading: state.loading.models.user,
}))(Form.create()(BindBankCard));
