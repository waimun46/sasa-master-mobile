import React from 'react';
import ReactDOM from 'react-dom';
import classNames from 'classnames';
import { Button, Modal, Icon } from 'antd';
import BasicComponent from "./BasicComponent";

/**
 * 自定义弹窗，主要用于倒计时通知
 */
class Textarea extends BasicComponent{

    constructor(props) {
        super(props);
    }

    state = {
        value: '',
        isFocus: false,
    };

    /* ------- Sys ------- */

    componentDidMount() {
        // this.resizeTextarea();
    }

    /* ------- Events -------*/

    handleKeyDown(e) {
        const {onPressEnter, onKeyDown} = this.props;
        if (e.keyCode === 13 && onPressEnter) {
            onPressEnter(e);
        }
        if (onKeyDown) {
            onKeyDown(e);
        }
    }

    handleTextareaChange(e) {
        this.setState({value:e.target.value});
        const onChange = this.props.onChange;
        if (onChange) {
            onChange(e);
        }
    }

    handleBlur(e){
        if (this.state.value == "" && this.props.placeholder)
            this.refs.input.value = this.props.placeholder;
    }

    handleFocus(e){
        if (this.state.value == "")
            this.refs.input.value = "";
    }

    /* ------- Function -------- */

    focus() {
        this.refs.input.focus();
    }

    get isFocus(){
        return this.refs.input === document.activeElement;
    }

    fixControlledValue(value) {
        if (typeof value === 'undefined' || value === null) {
            return '';
        }
        return value;
    }

    /* ------- Renders -------- */
    render(){
        const {props} = this;
        const {placeholder} = props;
        let value = 'value' in props ? this.fixControlledValue(props.value) : "";
        if (value == "" && !this.isFocus)
            value = placeholder;

        return (
            <textarea
                value={value}
                style={Object.assign({}, props.style, this.state.textareaStyles)}
                className={props.className}
                onKeyDown={this.handleKeyDown.bind(this)}
                onChange={this.handleTextareaChange.bind(this)}
                onBlur={this.handleBlur.bind(this)}
                onFocus={this.handleFocus.bind(this)}
                ref="input"
            />
        );
    }
}

module.exports = Textarea;
