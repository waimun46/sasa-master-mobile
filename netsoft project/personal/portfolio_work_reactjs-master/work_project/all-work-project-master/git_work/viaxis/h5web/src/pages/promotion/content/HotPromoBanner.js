import React, {Component} from 'react';
import pro01 from '../../../assets/images/banner/pro01.jpg';
import pro02 from '../../../assets/images/banner/pro02.jpg';
import { Card, Icon, Divider } from 'antd';

class HotPromoBanner extends Component {


    render() {
        return (
        <div className="promo-top-banner">
          <Divider orientation="left" >热门活动</Divider>
          <div className="promo-warp">
            <div className="warp-inner">
              <img src={pro01} alt="pro01" />
              <div className="infor">
                <p>彩票感恩月优惠</p>
                <p>查看详情 <Icon type="right" /></p>
                <div className="clearfix"></div>
              </div>
            </div>
          </div>
          <div className="promo-warp">
            <div className="warp-inner">
              <img src={pro02} alt="pro02" />
              <div className="infor">
                <p>彩票感恩月优惠</p>
                <p>查看详情 <Icon type="right" /></p>
                <div className="clearfix"></div>
              </div>
            </div>
          </div>
          <div className="promo-warp">
            <div className="warp-inner">
              <img src={pro01} alt="pro01" />
              <div className="infor">
                <p>彩票感恩月优惠</p>
                <p>查看详情 <Icon type="right" /></p>
                <div className="clearfix"></div>
              </div>
            </div>
          </div>
          <div className="promo-warp">
            <div className="warp-inner">
              <img src={pro02} alt="pro02" />
              <div className="infor">
                <p>彩票感恩月优惠</p>
                <p>查看详情 <Icon type="right" /></p>
                <div className="clearfix"></div>
              </div>
            </div>
          </div>







        {/*
          <Card title="热门活动">
          <div className="promo-inner">
            <Card
              type="inner"
              title="Inner Card title"
              extra={<a href="#">More</a>}
            >
              <img src={pro01} alt="pro01" />
            </Card>
            <Card
              style={{ marginTop: 16 }}
              type="inner"
              title="Inner Card title"
              extra={<a href="#">More</a>}
            >
              <img src={pro02} alt="pro02" />
            </Card>
            </div>
          </Card>
          */}
        </div>
        );
    }
};

export default HotPromoBanner;
