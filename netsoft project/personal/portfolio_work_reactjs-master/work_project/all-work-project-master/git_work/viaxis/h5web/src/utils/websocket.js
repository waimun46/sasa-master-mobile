import Auth from "./auth";
import { ActionState } from "../app_constants";
import notify from "./notify";
import * as Utils from "./common";


class Socket {

    constructor() {
        this.conn = null;
        this.store = window.g_app._store;
        this._time_tick = null;
    }

    connect() {
        if (this.conn == null || this.conn.readyState != 1)
            this.reconnect();
    }

    reconnect() {
        this.conn = new WebSocket(`${url.ws}?token=${Auth.getAuthToken()}`);
        this.conn.onopen = function () {
            // WebSocket每次建立连接成功，即发送sid,建立身份
            //this.init();

            // 对时包对时
            // this._timetick();
            // this._time_tick = setInterval(this._timetick.bind(this), 5000); //5秒对时一次

        }.bind(this);

        this.conn.onmessage = function (e) {
            var data = null;
            try {
                data = JSON.parse(e.data);
                // if (Config.Debug) {
                //     if (data.command != "TimeTick")
                //         console.log(data);
                // }
            } catch (ex) {
                console.log("parse json failed.", ex);
                return;
            }
            this._process_receive_data(data);
            // this.events.trigger(data.command, data.data, data.error_code, data.message);

        }.bind(this);

        this.conn.error = function (event) {
            //WebSocket Status:: Error was reported
            console.log("Error");
        }.bind(this);

        this.conn.onclose = this.disconnect.bind(this);
    }

    disconnect() {
        let self = this;
        if (this._time_tick != null)
            clearInterval(this._time_tick);
        setTimeout(function () {
            self.reconnect();
        }, 500);
    }

    send(command, data = {}) {
        if (this.conn == null || this.conn.readyState > 1)
            this.reconnect();
        var opt = {
            command,
            data
        };
        if (this.conn.readyState == 1)
            this.conn.send(JSON.stringify(opt));
        else
            this.waitForSocketConnection(() => this.conn.send(JSON.stringify(opt)));
    }

    init() {
        var sid = Cookie.get('sid');
        if (sid.startsWith('"') && sid.endsWith('"'))
            sid = sid.slice(1, sid.length - 1);
        this.send("Init", {
            sid: sid
        });
    }

    _process_receive_data(data) {
        if (data.command == "TimeTick")
            this._update_time(data.data);
        else if (data.command == "Login")
            Auth.logout();
        else if (data.command.endsWith("Notify")) {
            notify(data.command, data.data);
        }
        else
            this.store.dispatch({
                type: `ws/${data.command}`,
                payload: {
                    data: data.data,
                    code: data.error_code,
                    message: data.message
                }
            });
    }

    waitForSocketConnection(callback) {
        setTimeout(
            function () {
                if (this.conn.readyState === 1) {
                    if (callback != null) {
                        callback();
                    }
                } else if (this.conn.readyState > 1) {
                    this.reconnect();
                    this.waitForSocketConnection(callback);
                } else {
                    this.waitForSocketConnection(callback);
                }
            }.bind(this), 5);
    }

    _update_time(data) {
        try {
            var server_time = Utils.CalibrateServerTime(data.server, data.client);
            Utils.SaveServerTimeDiff(server_time);
        } catch (ex) {
            console.log(ex);
        }
    }

    _timetick() {
        this.send("TimeTick", {
            time: new Date().valueOf() / 1000
        });
    }

}

export default function getSocket() {
    if (window._WEBSOCKET == null || window._WEBSOCKET == undefined) {
        window._WEBSOCKET = new Socket();
        window._WEBSOCKET.connect();
    }
    return window._WEBSOCKET;
}
