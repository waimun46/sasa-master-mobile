import * as Constants from "@/app_constants";
import * as Utils from "@/utils/common";
import * as BetHelper from "@/utils/bet_helper";
import { factorial, combinatorics, permutation } from "@/utils/math";

/**
 * 注数计算器基类
 */
class BetCountCalurator {

    /**
     * 计算注数
     * @param  {[type]} gameType 游戏玩法
     * @param  {[type]} betNum   投注号码。若是选码器则是一个字典。若是直选则是字符串。
     * @param  {[type]} unitPos  数位选择器内容。若不需要则传入null,否则传入字符串。格式如下：
     * 号码投注占位符。占位为1，不占位为0。
     * 例如:
     * 11100 代表三星占位万千百
     * 01110 代表三星占位千百十
     * 00111 代表三星占位百十个
     * @return {[type]}          [description]
     */
    calc(gameType, betNum, unitPos) {
        throw "NotImplementedException";
    }

    /**
     * 计算数位占位生成的方案数量
     * @param  {[type]} gameType [description]
     * @param  {[type]} unitPos  [description]
     * @return {[type]}          [description]
     */
    calcUnitPosPlanCount(gameType, unitPos) {
        var multiple = 1;
        if (unitPos && Array.isArray(unitPos)) {
            var unitCount = unitPos.filter(it => it == 1).length;
            multiple = combinatorics(unitCount, gameType.BetNumCount);
        }
        return multiple;
    }
}

/**
 * 直选复式(X星直选)
 */
class StraightPickTreeStyleBetCount extends BetCountCalurator {
    calc(gameType, betNum, unitPos) {
        if (!gameType || !betNum) return 0;
        if (Object.keys(betNum).length != gameType.DigitPosCount) return 0;

        // 三星直选 百位(数量)×十位(数量)×个位(数量)
        // 其他同理
        var count = 1;
        for (var index in betNum) {
            var ballCount = betNum[index] ? betNum[index].length : 0;
            count *= ballCount;
        }
        return count * this.calcUnitPosPlanCount(gameType, unitPos);
    }
}

/**
 * 直选单式
 */
class StraightPickLinearStyleBetCount extends BetCountCalurator {
    calc(gameType, betNum, unitPos) {
        if (Utils.isEmptyOrNull(betNum)) return 0;

        var nums_group_split_symbols = gameType.MaxNum > 9 ? Constants.TWO_DIGITS_NUMS_SPLIT_CHARS : Constants.NUMS_SPLIT_CHARS;
        var num_split_symbols = gameType.MaxNum > 9 ? Constants.NUM_SPLIT_CHAR : null;

        var betNums = BetHelper.parseBetNumFromString(betNum, gameType.BetNumCount, nums_group_split_symbols, num_split_symbols);
        if (!betNums) return 0;

        var checkNumFn = (it) => !isNaN(it) && it >= gameType.MinNum && it <= gameType.MaxNum;
        betNums = betNums.filter(it => it.length == gameType.BetNumCount && it.every(checkNumFn));
        return betNums.length * this.calcUnitPosPlanCount(gameType, unitPos);
    }
}

/**
 * 组选复式
 */
class BoxPickTreeStyleBetCount extends BetCountCalurator {
    calc(gameType, betNum, unitPos) {
        if (!gameType || !betNum) return 0;
        if (Object.keys(betNum).length != gameType.DigitPosCount) return 0;

        // 一星组选 N
        // 二星组选 N×(N-1)/2
        // 三星组选 N×(N-1)×(N-2)/3/2
        // 四星组选 N×(N-1)×(N-2)×(N-3)/4/3/2
        // 五星组选 N×(N-1)×(N-2)×(N-3)×(N-4)/5/4/3/2
        // 六星组选 N×(N-1)×(N-2)×(N-3)×(N-4)×(N-5)/6/5/4/3/2
        // 选X中X组选 N×(N-1)×...×(N-X)/X/X-1/.../2int count = 0;
        var count = 0;
        for (var index in betNum) {
            var nums = betNum[index];
            if (nums.length < gameType.BetNumCount) continue;
            if (gameType.BetNumCount == 1) {
                count = nums.length;
                continue;
            }

            count = nums.length;
            for (var i = 1; i < gameType.BetNumCount; i++)
                count *= (nums.length - i);

            for (var i = gameType.BetNumCount; i > 1; i--)
                count /= i;
        }
        return count;
    }
}

/**
 * 无重排列
 * PK10 猜x(x>1)
 */
class UniquePermutationBetCount extends BetCountCalurator {
    calc(gameType, betNum, unitPos) {
        if (!gameType || !betNum) return 0;
        if (Object.keys(betNum).length != gameType.DigitPosCount) return 0;

        return permutation(false, ...Object.keys(betNum).map(it => betNum[it])).length;

    }
}

/**
 * 组六
 * [什么是组六？](http://caipiao.163.com/help/10/0728/13/6CMBEGF900754IHN.html#from=relevant)
 */
class N6BoxPickTreeStyleBetCount extends BetCountCalurator {
    calc(gameType, betNum, unitPos) {
        if (!gameType || !betNum) return 0;
        if (Object.keys(betNum).length != gameType.DigitPosCount) return 0;

        // N×(N-1)×(N-2)
        var len = betNum[0].length;
        var count = len * (len - 1) * (len - 2);
        return count * this.calcUnitPosPlanCount(gameType, unitPos);
    }
}

/**
 * 组三复式
 * [什么是组三？](http://caipiao.163.com/help/10/0728/13/6CMBDQMN00754IHN.html#from=relevant)
 */
class N3BoxPickTreeStyleBetCount extends BetCountCalurator {
    calc(gameType, betNum, unitPos) {
        if (!gameType || !betNum) return 0;
        if (Object.keys(betNum).length != gameType.DigitPosCount) return 0;

        // 组三       N×(N-1)×3
        var len = betNum[0].length;
        var count = len * (len - 1) * 3;
        return count * this.calcUnitPosPlanCount(gameType, unitPos);
    }
}

/**
 * 组三混合
 */
class N3MixBoxPickTreeStyleBetCount extends BetCountCalurator {
    calc(gameType, betNum, unitPos) {
        if (Utils.isEmptyOrNull(betNum)) return 0;

        var nums_split_symbols = gameType.MaxNum > 9 ? Constants.TWO_DIGITS_NUMS_SPLIT_CHARS : Constants.NUMS_SPLIT_CHARS;
        var num_split_symbols = gameType.MaxNum > 9 ? Constants.NUM_SPLIT_CHAR : null;

        var betNums = BetHelper.parseBetNumFromString(betNum, gameType.BetNumCount, nums_split_symbols, num_split_symbols);
        if (!betNums) return 0;

        var count = 0;
        betNums.forEach(function (num) {
            var numArr = [];
            for (var i in num) {
                if (!numArr.includes(num[i]))
                    numArr.push(num[i]);
            }
            if (numArr.length == 1)
                count += 1;
            else if (numArr.length == 2)
                count += 3;
            else if (numArr.length == 3)
                count += 6;
        });

        return count;
    }
}

/**
 * 组二混合
 */
class N2MixBoxPickTreeStyleBetCount extends BetCountCalurator {
    calc(gameType, betNum, unitPos) {
        if (Utils.isEmptyOrNull(betNum)) return 0;

        var nums_split_symbols = gameType.MaxNum > 9 ? Constants.TWO_DIGITS_NUMS_SPLIT_CHARS : Constants.NUMS_SPLIT_CHARS;
        var num_split_symbols = gameType.MaxNum > 9 ? Constants.NUM_SPLIT_CHAR : null;

        var betNums = BetHelper.parseBetNumFromString(betNum, gameType.BetNumCount, nums_split_symbols, num_split_symbols);
        if (!betNums) return 0;

        var count = 0;
        betNums.forEach(function (num) {
            var numArr = [];
            for (var i in num) {
                if (!numArr.includes(num[i]))
                    numArr.push(num[i]);
            }
            if (numArr.length == 1)
                count += 1;
            else if (numArr.length == 2)
                count += 2;
        });

        return count;
    }
}

/**
 * 组二复式
 */
class N2BoxPickTreeStyleBetCount extends BetCountCalurator {
    calc(gameType, betNum, unitPos) {
        if (!gameType || !betNum) return 0;
        if (Object.keys(betNum).length != gameType.DigitPosCount) return 0;
        // N×(N-1)
        var len = betNum[0].length;
        var count = len * (len - 1);
        return count * this.calcUnitPosPlanCount(gameType, unitPos);
    }
}

/**
 * 定位胆/不定位胆
 */
class PositionBetCount extends BetCountCalurator {
    calc(gameType, betNum, unitPos) {
        if (!gameType || !betNum) return 0;

        // 万位(数量)＋千位(数量)＋百位(数量)＋十位(数量)＋个位(数量)
        var count = 0;
        for (var index in betNum) {
            var ballCount = betNum[index] ? betNum[index].length : 0;
            count += ballCount;
        }
        return count;
    }
}

/**
 * 组选120 组选24
 */
class BasicBoxPickTreeStyleBetCount extends BetCountCalurator {
    calc(gameType, betNum, unitPos) {
        if (!gameType || !betNum) return 0;
        if (Object.keys(betNum).length != gameType.DigitPosCount) return 0;

        var numCount = betNum[0].length;
        if (numCount < gameType.BetNumCount) return 0;

        return combinatorics(numCount, gameType.BetNumCount);
    }
}

/**
 * 重号基类
 */
class DuplicateNumBetCount extends BetCountCalurator {
    /**
     * 单号数量
     * @return {[type]} [description]
     */
    get oddNumCount() {
        throw "NotImplementedException";
    }

    get duplicateNumCount() {
        throw "NotImplementedException";
    }

    /**
     * 重号数位在数位列表中的下标
     * @return {[type]} [description]
     */
    get duplicateDigitPosIndex() {
        return 0;
    }

    calc(gameType, betNum, unitPos) {
        if (!gameType || !betNum) return 0;
        if (Object.keys(betNum).length != gameType.DigitPosCount) return 0;

        var duplicateNums = betNum[this.duplicateDigitPosIndex];
        var oddNums = this.duplicateDigitPosIndex == 0 ? betNum[1] : betNum[0];

        if (duplicateNums.length < this.duplicateNumCount || oddNums.length < this.oddNumCount) return 0;

        var count = 0;
        // 单号为主计算(单号中去除与重号相同的号码)
        if (this.oddNumCount >= this.duplicateNumCount) {
            duplicateNums.forEach(function (duplicateNum) {
                // 去掉重复的号码
                var calcNums = oddNums.filter(it => it != duplicateNum);
                var calcOddCount = calcNums.length;
                if (calcOddCount >= this.oddNumCount) {
                    count += combinatorics(calcOddCount, this.oddNumCount);
                }
            }.bind(this));
        } else {
            // 重号为主计算(双号中去除与单号相同的号码,五星组选30)
            oddNums.forEach(function (oddNum) {
                // 去掉重复的号码
                var calcNums = duplicateNums.filter(it => it != oddNum);
                var calcOddCount = calcNums.length;
                if (calcOddCount >= this.duplicateNumCount) {
                    count += combinatorics(calcOddCount, this.duplicateNumCount);
                }
            }.bind(this));
        }
        return count;
    }
}

/**
 * 1二重号3单号(五星组选60)
 */
class Duplicate1Odd3BetCount extends DuplicateNumBetCount {
    get oddNumCount() {
        return 3;
    }

    get duplicateNumCount() {
        return 1;
    }
}

/**
 * 2二重号1单号(五星组选30)
 */
class Duplicate2Odd1BetCount extends DuplicateNumBetCount {
    get oddNumCount() {
        return 1;
    }

    get duplicateNumCount() {
        return 2;
    }
}

/**
 * 1三重号3单号(五星组选20)
 */
class Duplicate2Odd3BetCount extends DuplicateNumBetCount {
    get oddNumCount() {
        return 2;
    }

    get duplicateNumCount() {
        return 1;
    }
}

/**
 * 1三重号1二重号(五星组选10) 1四重号1单号(五星组选5) 1三重号1单号(四星组选4)
 */
class Duplicate1Odd1BetCount extends DuplicateNumBetCount {
    get oddNumCount() {
        return 1;
    }

    get duplicateNumCount() {
        return 1;
    }
}

/**
 * 1二重号2单号(四星组选12)
 */
class Duplicate1Odd2BetCount extends DuplicateNumBetCount {
    get oddNumCount() {
        return 2;
    }

    get duplicateNumCount() {
        return 1;
    }
}

/**
 * 2二重号(四星组选6)
 */
class Duplicate2BetCount extends DuplicateNumBetCount {
    calc(gameType, betNum, unitPos) {
        if (!gameType || !betNum) return 0;
        if (Object.keys(betNum).length != gameType.DigitPosCount) return 0;

        if (!betNum[0] || betNum[0].length < 2) return 0;

        return combinatorics(betNum[0].length, 2);
    }
}

/**
 * 总和-大小单双/前大后大
 */
class SUMBetCount extends DuplicateNumBetCount {
    calc(gameType, betNum, unitPos) {
        if (!gameType || !betNum) return 0;

        var count = 0;
        for (var index in betNum) {
            var ballCount = betNum[index] ? betNum[index].length : 0;
            count += ballCount;
        }
        return count;
    }
}

module.exports = {
    // 定位胆/不定位胆
    "PositionBetCount": new PositionBetCount(),
    // 直选复式(X星直选)
    "StraightPickTreeStyleBetCount": new StraightPickTreeStyleBetCount(),
    // 直选单式
    "StraightPickLinearStyleBetCount": new StraightPickLinearStyleBetCount(),
    // 组选复式
    "BoxPickTreeStyleBetCount": new BoxPickTreeStyleBetCount(),
    // 无重排列
    "UniquePermutationBetCount": new UniquePermutationBetCount(),
    // 组六
    "N6BoxPickTreeStyleBetCount": new N6BoxPickTreeStyleBetCount(),
    // 组三
    "N3BoxPickTreeStyleBetCount": new N3BoxPickTreeStyleBetCount(),
    // 组三混合
    "N3MixBoxPickTreeStyleBetCount": new N3MixBoxPickTreeStyleBetCount(),
    // 组二
    "N2BoxPickTreeStyleBetCount": new N2BoxPickTreeStyleBetCount(),
    // 组二混合
    "N2MixBoxPickTreeStyleBetCount": new N2MixBoxPickTreeStyleBetCount(),
    // 组选120 组选24
    "BasicBoxPickTreeStyleBetCount": new BasicBoxPickTreeStyleBetCount(),
    // 1二重号3单号(五星组选60)
    "Duplicate1Odd3BetCount": new Duplicate1Odd3BetCount(),
    // 2二重号1单号(五星组选30)
    "Duplicate2Odd1BetCount": new Duplicate2Odd1BetCount(),
    // 1三重号2单号(五星组选20)
    "Duplicate2Odd3BetCount": new Duplicate2Odd3BetCount(),
    // 1三重号1二重号(五星组选10) 1四重号1单号(五星组选5) 1三重号1单号(四星组选4)
    "Duplicate1Odd1BetCount": new Duplicate1Odd1BetCount(),
    // 1二重号2单号(四星组选12)
    "Duplicate1Odd2BetCount": new Duplicate1Odd2BetCount(),
    // 2二重号(四星组选6)
    "Duplicate2BetCount": new Duplicate2BetCount(),
    // 总和-大小单双/前大后大
    "SUMBetCount": new SUMBetCount(),
};
