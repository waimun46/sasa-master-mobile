# README

## Requiements

- Nodejs
- Umi
- Dva
- React
- Antd
- Antd Mobile

-----

## Install & Start Develop

### Install NodeJs

You must install Node.JS first. I recommand u use nvm instead of native nodejs , because nvm is very easy and kindly to manage your node versions.
You can found more in here :

- [使用 nvm 管理不同版本的 node 与 npm](http://bubkoo.com/2017/01/08/quick-tip-multiple-versions-node-nvm/)
- [github - nvm](https://github.com/creationix/nvm)

Or install nodejs in here :

- [NodeJS](https://nodejs.org)

### Install yarn and npm

yarn / npm are javascript package manage tools, you can found more in here: [Yarn vs npm: Everything You Need to Know](https://www.sitepoint.com/yarn-vs-npm/)

- [Installation | Yarn](https://yarnpkg.com/lang/en/docs/install/)
- [Install npm](https://www.npmjs.com/get-npm)

### Install Umi

You can see more in here : [Umi 快速上手](https://umijs.org/zh/guide/getting-started.html#%E7%8E%AF%E5%A2%83%E5%87%86%E5%A4%87)

```bash
$ yarn global add umi
$ umi -v
2.0.0
```

### Install all dependencies packages

```bash
$ yarn install
# or
$ npm install -i
```

### Complie and Start

```bash
$ umi dev
# or
$ yarn start
```

-----

## Helpful chrome extensions

- React Developer Tools
- Redux DevTools

-----

## References

Before start develop , I highly recommend u read below article first, It's very helpful and effectivity.

- [UmiJS官方文档](https://umijs.org/zh/guide/)
- [umi + dva，完成用户管理的 CURD 应用](https://github.com/sorrycc/blog/issues/62)
- [DvaJS的学习之路1 - dva+umi官网例子学习](https://juejin.im/post/5b67fed36fb9a04f8856f817)
- [DvaJS的学习之路2 - umi@2 + dva，完成用户管理的 CURD 应用](https://juejin.im/post/5b93c0cce51d450e9059a84e)
- [React Tutorial](https://reactjs.org/tutorial/tutorial.html)
- [Ant Design](https://ant.design/index-cn)
- [Ant Design Mobile](https://mobile.ant.design/index-cn)
- [Ant Landing](https://landing.ant.design/)
- [Umi Examples](https://github.com/umijs/umi-examples)
- [dva.js 知识导图](https://github.com/dvajs/dva-knowledgemap)

-----

## Usage

### Authority

If the page require auth, you can write below code at page file head . just like :

```js
/**
 * Routes:
 *   - ./src/routes/AuthorizationRoute.js
 */

import ...
// your code.

```