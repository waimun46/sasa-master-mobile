import React, { Component } from 'react';
import { connect } from 'dva';
import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';
import { List } from 'antd-mobile';

import * as BetHelper from "@/utils/bet_helper";
import TeamRecord from '@/components/teamRecord';
import * as RenderHelper from "@/utils/render_helper";
import DateRange from '@/components/DateRange';

import Link from 'umi/link';

const Item = List.Item;
const Brief = Item.Brief;



class ChaKanXiaJi2 extends Page {
  title = "查看下级"

  constructor(props) {
    super(props);
 
    this.pageIndex = 1;
    this.querys = {};

    const query = new URLSearchParams(this.props.location.search);
    this.id = query.get('id')
    this.gd = query.get('gd')
  
  }
  

  /* ---------------------------- System  ------------------------------*/
  componentDidMount() {
    this.queryTeam();

  }
  
  /* ---------------------------- Functions  ------------------------------*/

  queryTeam() {
    this.props.dispatch({ type: "user/queryTeam", payload: {userId: this.id ,pageIndex: this.pageIndex} })
  }

  isNullOrEmpty(values) {
    let isNull = true;
    try {
        Object.keys(values).forEach(it => {
            if (!Utils.isEmptyOrNull(values[it])) {
                isNull = false;
                throw "break";
            }
        });
    } catch (e) {
        if (e === "break") return;
        else
            throw e;
    }
    return isNull;
 }

  /* ---------------------------- Events ------------------------------*/
  handleChildClick (event) {
    event.stopPropagation();
  }

  render() {
    const team = this.props.team || [];
    const mainUser = this.props.mainUser || [];

    const showDetail = (team) => {
      const dialog = RenderHelper.dialog(null,
          <TeamRecord teamData={team}  close={ ()=> close() } />);

      const close = () => dialog.destroy();
    }
    
    return (
      <div className="fund-history ">
        <div className="top-header"> </div>

        {/* date */}
        {/* <List className="date-select">
          <Item><DateRange/></Item>
        </List> */}

        <div className="form_title">
          <h5>[大股东] {mainUser}/ [股东] {this.gd}/ 代理</h5>
        </div>
        <List className="my-list ">
        {team && team.map((element,i) => 
          
            <Item
              key={i}
              extra={
                <div className="bet-money add">
                  {(element.ChildCount > 0) ? <Link onClick={this.handleChildClick} to={`/personal_information/inforpage/agency/content/ChaKanXiaJi/huiYuan?id=${ element.UserId }&gd=${ this.gd }&dl=${ element.Email }`}>￥{element.Balance} ></Link> : <div>￥{element.Balance}</div>}                
                </div>
              }
              align="top"
              multipleLine
              onClick={() => {showDetail(element)}}
            >
              <span className="title-warp">{element.Email}</span>
              {/* <span className="title-warp">{element.Agents}</span> */}
              <Brief style={{fontSize: '12px', color: '#cccccc'}}>{element.CreateTime}</Brief>
            </Item>                 
        )} 
        </List>
      </div>
    );
  }
};

function mapStateToProps(state) {
  return {
    team: state.user.team.Data,
    mainUser: state.global.userinfo.User.Email,
  }
}

export default connect(mapStateToProps)(Form.create()(ChaKanXiaJi2));
