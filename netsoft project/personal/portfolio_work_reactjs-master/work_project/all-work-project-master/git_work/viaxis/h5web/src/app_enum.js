/**
 * 状态类。
 * 实例包含三个字段，分别是:
 * name   : 显示的名称
 * value  : 状态值
 * status : 状态 (用于赋给Badge或自己实现Status显示方法。)
 * 本类主要用于状态枚举，并搭配RenderHelper.render_status使用。

class Status {

	constructor(name, value, status) {
		this.name = name;
		this.value = value;
		this.status = status;
	}

	toString() {
		return this.name;
	}
}
 */


/**
 * [UserKindEnum description]
 * @type {Object}
 */
export const UserKind = {
    Super: 1,
    Master: 2,
    Agent: 3,
    Member: 4,
    Shadow: 5
};

/**
 * 通用状态
 * @type {Object}
 */
export const CommonStatus = {
    Enable: 0,
    Disabled: 1
};

/**
 * 充值状态
 * @type {Object}
 */
export const DepoisitRequestStatus = {
	0: "充值中",
	1: "完成",
	2: "失败",
};



/**
 * 流水类型
 * @type {Object}
 */
export const BillFlowType = {
    Deposit: 0,
    Withdraw: 1,
    Transfer: 2,
    Game: 3,
    Commission: 4,
    Bonus: 5
};

/**
 * 注单状态
 * @type {Object}
 */
export const DealStatus = {
    UnOpen: 0,
    Settle: 1,
    Profit: 2,
    Commission: 3,
    PreCancel: 4,
    Canceled: 5
};

/**
 * 公告类型
 * @type {Object}
 */
export const BulletinType = {
    Info: 0,
    Warning: 1
}

/**
 * 支付类型
 * @type {Object}
 */
export const BankType = {
    Bank: 0,
    TTP: 1,
    SP: 2
}
