/**
 * Routes:
 *   - ./src/routes/AuthorizationRoute.js
 */

import { connect } from 'dva';
import { message, Row, Col, Alert, Tabs, Button, Input,Icon } from "antd";
import { Flex, List, InputItem, WhiteSpace } from 'antd-mobile';

import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';
import CountDownButton from "@/components/CountDownButton";
import * as RenderHelper from "@/utils/render_helper";

class ResetSafePassword extends Page {
  title = "重置安全密码";
  constructor(props) {
    super(props);
    this.state = {
      // 提交按钮
      loading: false,
      // 发送验证码按钮
      captchaSending: false,
      // 发送按钮倒计时
      target: 0,
    }
    // 初始验证码地址,否则每次表单输内容验证码都会变化
    this.captchaUrl = RenderHelper.getCaptchaImgUrl();
  }


  /* ---------------------------- Functions ------------------------------*/

  refreshCaptcha() {
    let captcha = this.props.form.getFieldInstance("captcha");
    captcha.clearInput();
    captcha.inputRef.focus();
    // this.captchaUrl = RenderHelper.getCaptchaImgUrl();
  }

  /* ---------------------------- Events ------------------------------*/

  /**
   * 重置安全密码
   * @param  {[type]} e [description]
   * @return {[type]}   [description]
   */
  handleResetSubmit(e) {
    e.preventDefault();

    const { dispatch } = this.props;

    this.form.validateFields((errors, values) => {
      if (errors) return;

      dispatch({
        type: "user/resetSafePassword",
        payload: { password: values.password, captcha: values.captcha }
      }).then(resp => {
        this.setState({ loading: false });

        if (resp.code === 0) {
          let msg = "重置安全密码成功";
          message.success(msg);
          this.props.form.resetFields();
        } else {
          message.error(resp.message);
          this.refreshCaptcha();
        }

      });

      this.setState({ loading: true });
    })
  }

  sendCaptchaClick(e) {
    const { dispatch } = this.props;
    dispatch({ type: "user/sendTicket", payload: true }).then(resp => {
      this.setState({ captchaSending: false });
      let seconds = 0;
      if (resp.code === 0) {
        seconds = 60;
        message.success("验证码已发送到您的邮箱。");
      } else {
        seconds = resp.data;
        message.error(resp.message);
      }
      if (seconds > 0)
        this.setState({ target: new Date().getTime() + seconds * 1000 });

    });
    this.setState({ captchaSending: true });
  }

  checkPassword(rule, value, callback) {
    if (value && value !== rule.getFieldValue('password')) {
      callback(rule.message);
    } else {
      callback();
    }
  }

  /* ---------------------------- Renders ------------------------------*/

  render() {
    const { getFieldProps } = this.props.form;

    return (

      <div className="form_sty form-countdown">
        <div className="top-header"> </div>
        <div className="form_title">
          <h5>填写信息</h5>
        </div>

        <Form form={this.props.form} ref={el => this.form = el} onSubmit={this.handleResetSubmit.bind(this)}>

          <InputItem
            {...getFieldProps('psw', {
              rules: [{
                required: true, message: '请输入当前密码'
              }, {
                min: 6, max: 20, message: '当前密码长度要求为6到20个字符之间。'
              }],
            })}
            type="password"
            clear
            placeholder="请输入当前密码">
            当前密码
          </InputItem>


          <InputItem
            {...getFieldProps('password', {
              rules: [{
                required: true, message: '请输入新的密码'
              }, {
                min: 6, max: 20, message: '密码长度要求为6到20个字符之间。'
              }],
            })}
            type="password"
            clear
            placeholder="请输入新的密码">
            新的密码
          </InputItem>
          <InputItem
            {...getFieldProps('confirm_pwd', {
              rules: [{
                required: true, message: "请输入确认密码"
              }, {
                validator: this.checkPassword, message: "两个密码不一致。",
                getFieldValue: this.props.form.getFieldValue
              }],
            })}
            type="password"
            clear
            placeholder="请输入确认密码">
            确认密码
          </InputItem>
          <InputItem
            {...getFieldProps('captcha', {
              rules: [{ required: true, message: '请输入您接收到验证码' }],
            })}
            maxLength="6"
            clear
            placeholder="请输入验证码"
            extra={<CountDownButton loading={this.state.captchaSending}
                                    size="small" type="primary"
                                    target={this.state.target}
                                    onClick={this.sendCaptchaClick.bind(this)}>
              发送验证码
            </CountDownButton>}
          >
            验证码
          </InputItem>
          <div className="game-orders">
            <div className="ButtonBottom">
              <button className="btn_left">
                <Icon type="reload"/> 重置
              </button>

              <button className="btn_right" loading={this.state.loading} htmlType="submit" >
                确定
              </button>

            </div>
            <div className="clearfix"></div>
          </div>
        </Form>
      </div>
    );
  }
};

export default connect(state => ({
  hasSafePassword: state.user.hasSafePassword,
  loading: state.loading.models.user,
}))(Form.create()(ResetSafePassword));
