/**
 * Routes:
 *   - ./src/routes/AuthorizationRoute.js
 */

import { connect } from 'dva';
import { message, Row, Col, Alert, Tabs, Button, Input,Icon } from "antd";
import { List, InputItem, WhiteSpace } from 'antd-mobile';
import Link from 'umi/link';
import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';

const Item = List.Item;
const Brief = Item.Brief;

class agency extends Page {
  title = "代理中心"

  componentWillMount(){
    this.setTitle();
    this.getUserInfo();
  }

  getUserInfo() {
    this.props.dispatch({ type: "user/queryUserInfo" });
  }

  render() {

    let user = this.props.userinfo;

    return (
      <div className="form_sty">
        <div className="top-header"> </div>
        <div className="form_title">
          <h5>选项列表</h5>
        </div>

        { user && user.UserKind == 4 ? <p>此页只供代理查询</p> :
          <List >
          <Link className="border-line" to="/personal_information/inforpage/agency/content/ChaKanXiaJi">
            <Item arrow="horizontal" onClick={() => {}}>查看下级</Item>
          </Link>
          {/* <Link className="border-line" to="/personal_information/inforpage/agency/content/XiaJiKaiHu">
           <Item arrow="horizontal" onClick={() => {}}>下级开户</Item>
          </Link> */}
          <Link className="border-line" to="/personal_information/inforpage/agency/content/Report">
            <Item arrow="horizontal" onClick={() => {}}>报表（分成报表）</Item>
          </Link>

        </List>



        }


      

      </div>


    );
  }
};

export default connect(state => ({
  userinfo: state.user.user
}))(Form.create()(agency));
