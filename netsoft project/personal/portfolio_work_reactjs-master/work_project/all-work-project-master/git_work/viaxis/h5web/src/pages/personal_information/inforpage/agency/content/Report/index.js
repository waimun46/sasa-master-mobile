import React, { Component } from 'react';
import { connect } from 'dva';
import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';
import { List } from 'antd-mobile';
import Link from 'umi/link';

import DateRange from '@/components/DateRange';

const Item = List.Item;
const Brief = Item.Brief;



class Report2 extends Page {
  title = "报表"

  render() {
    return (
      <div className="form_sty">
        <div className="top-header"> </div>
        <div className="form_title">
          <h5>报表信息</h5>
        </div>


        <List >
          <Link className="border-line" to="/personal_information/inforpage/agency/content/Report/ProfitUser">
            <Item arrow="horizontal" onClick={() => {}}>用户盈亏</Item>
          </Link>
          <Link className="border-line" to="/personal_information/inforpage/agency/content/Report/ProfitGame">
           <Item arrow="horizontal" onClick={() => {}}>游戏盈亏</Item>
          </Link>
          <Link className="border-line" to="/personal_information/inforpage/agency/content/Report/ProfitDividend">
            <Item arrow="horizontal" onClick={() => {}}>游戏分红</Item>
          </Link>

        </List>

      </div>


    );
  }
};

export default connect(state => ({

}))(Form.create()(Report2));
