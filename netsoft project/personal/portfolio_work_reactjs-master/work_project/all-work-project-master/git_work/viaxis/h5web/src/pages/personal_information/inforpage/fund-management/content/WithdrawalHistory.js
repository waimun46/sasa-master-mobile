import React, { Component } from 'react';
import { connect } from 'dva';
import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';
import { List,Pagination, Icon } from 'antd-mobile';

import DateRange from '@/components/DateRange';
import * as RenderHelper from '@/utils/render_helper';
import HistoryList from '../../../../../components/WithdrawHistory';
import pending from '../../../../../assets/images/icon/pending.png';
import zhui from '../../../../../assets/images/icon/zhui.png';
import game from '../../../../../assets/images/game-icon/game01.png';

const Item = List.Item;
const Brief = Item.Brief;



class WithdrawHistory extends Page {
  title = "提现纪录"

  constructor(props){
    super(props);
    this.state={
      queryDate:[]
    }

    this.querys = this.defaultQuerys = Object.assign({}, { not_trace: true });
  }


  componentDidMount(){
    this.getWithdraHistory();
  }


  getWithdraHistory(){
    this.props.dispatch({ type: "withdraw/queryWithdraw", payload:this.querys});
  }

  onDateRangeChange = (value) => {
    console.log(value);
    //let fmtValue = dateHelper.dateRangeFormat(value);
    let date = [];
    date.push(value[0].toLocaleString());
    date.push(value[1].toLocaleString());
    this.setState({queryDate:date});

    Object.assign(this.querys,this.querys, {"CreateTime": date});
    this.getWithdraHistory();
}

onPaginationChange(val){
  console.log(val);
  Object.assign(this.querys,this.querys, {PageIndex: val});
  this.getWithdraHistory();
}

  getNumberOfPages(total, pageSize){

    var pg = Math.floor(parseInt(total)/parseInt(pageSize));
    if((parseInt(total)%parseInt(pageSize))!=0)
      pg+=1;
  
      return pg;
  
   }


  render() {

    var { history } = this.props;
    let data = history.Data || [];
    let page = this.getNumberOfPages(history.TotalCount, history.PageSize);
    let idx = history.PageIndex;


    const showDetail = (list) => {
    const dialog = RenderHelper.dialog(null,
      <HistoryList historytData={list}  close={ ()=> close() } />);

      const close = () => dialog.destroy();
    }



    return (
      <div className="fund-history ">
        <div className="top-header"> </div>

        {/* date */}
        <List className="date-select">
          <Item><DateRange onChange = {this.onDateRangeChange.bind(this)}/></Item>
        </List>

        <List renderHeader={() => '提现信息'} className="my-list ">

        { data.map((element,i) => {
          return(

            <Item key = {i}  extra={
              <div className="bet-money">
                <span style={{color: 'green'}}>￥{element.Amount}</span>
              </div>
            }
              align="top"
              multipleLine
              onClick={()=>{
                showDetail(element);
              }}>
              <span className="title-warp">订单号: {element.Id}</span>

              {/* <span className="title-warp">{element.AccountName}</span> */}
              &nbsp;&nbsp;&nbsp;
              <span className="title-warp">
              <span>收款卡：{element.BankName}</span></span>
              <Brief style={{fontSize: '12px', color: '#cccccc'}}>{element.CreateTime}</Brief>

            </Item>
          )
        })}
        {
          history.TotalCount==0? (<span>没有数据</span>) : (<p></p>)
        }

        </List>
        {
            (page < 2) ? <p></p>:
            <Pagination total={page}
              className="custom-pagination-with-icon"
              current={idx}
              locale={{
                prevText: (<span className="arrow-align"><Icon type="left" />上一页</span>),
                nextText: (<span className="arrow-align">下一页<Icon type="right" /></span>),
              }}
              onChange={this.onPaginationChange.bind(this)}
           />
          }
      </div>
    );
  }
};

export default connect(state => ({
  history: state.withdraw.historys,
}))(Form.create()(WithdrawHistory));
