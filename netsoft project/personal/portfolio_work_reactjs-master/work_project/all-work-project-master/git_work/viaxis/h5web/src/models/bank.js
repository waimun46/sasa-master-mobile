import * as bankService from '@/services/bank';

export default {
    namespace: 'bank',
    state: {
        banks: [],
        cards: []
    },
    reducers: {
        saveBanks(state, { payload }) {
            return { ...state, banks: [...payload.data] };
        },
        saveBankCards(state, { payload }) {
            return { ...state, cards: [...payload.data] };
        },
    },
    effects: {
        *getBanks({ payload }, { call, put }) {
            const data = yield call(bankService.getBank);
            yield put({
                type: 'saveBanks', payload: data
            });
            return data;
        },
        *getBankCards({ payload }, { call, put }) {
            const data = yield call(bankService.getBankCards);
            console.log('getBankCards', data);
            if (data.code === 0)
                yield put({
                    type: 'saveBankCards', payload: data
                });
            return data;
        },
        *addBankCard({ payload: { bank, account_no, account_name, branch, password } }, { call, put }) {
            const data = yield call(bankService.addBankCard, bank, account_no, account_name, branch, password);
            yield put({
                type: 'getBankCards'
            });
            return data;
        },
    },
};