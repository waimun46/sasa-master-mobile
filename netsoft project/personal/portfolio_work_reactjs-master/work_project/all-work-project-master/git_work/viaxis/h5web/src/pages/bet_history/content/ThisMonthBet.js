import React, {Component} from 'react';
import { List, Modal, Pagination, Icon } from 'antd-mobile';
import { connect } from 'dva';

import pending from '../../../assets/images/icon/pending.png';
import zhong from '../../../assets/images/icon/table-icon1.png';
import wei from '../../../assets/images/icon/table-icon4.png';

import xylc from '../../../assets/images/game-icon/xylc.png';
import fc3d from '../../../assets/images/game-icon/fc3d.png';
import keno from '../../../assets/images/game-icon/keno.png';
import kl8 from '../../../assets/images/game-icon/kl8.png';
import pk10 from '../../../assets/images/game-icon/pk10.png';
import pl3 from '../../../assets/images/game-icon/pl3.png';
import ssc from '../../../assets/images/game-icon/ssc.png';

import { object } from 'prop-types';
import * as Constants from "@/app_constants";
import * as betHelper from '../../../utils/bet_helper';
import BetRecord from '../../../components/BetRecord';
import * as RenderHelper from '../../../utils/render_helper';
// import { Utils } from 'tslint';
import * as Util from "../../../utils/common";

const Item = List.Item;
const Brief = Item.Brief;


class ThisMonthBet extends Component {

  constructor(props){
      super(props);
      this.state = {
        visible: false,

      }
      this.querys = this.defaultQuerys = Object.assign({}, { not_trace: true });
  }

  componentWillMount(){
    this.queryDealInfo();
  }

  componentWillUpdate(nextProps, nextState) {
    if(nextProps.query != this.props.query)
    {
      let values = this.clean(nextProps.query);
        if(Util.isEmptyObject(values))
            this.querys =  this.defaultQuerys;
        else
            this.querys = Object.assign({}, values);

      this.queryDealInfo();
    }
  }

  clean(obj) {
    let objH = obj;
    for (var propName in objH) { 
      if (objH[propName] === null || objH[propName] === undefined) {
        delete objH[propName];
      }
    }
    return objH;
  }

  queryDealInfo() {
    this.props.dispatch({ type: "dealinfo/DealInfo", payload: this.querys });
 }

 getNumberOfPages(total, pageSize){

  var pg = Math.floor(parseInt(total)/parseInt(pageSize));
  if((parseInt(total)%parseInt(pageSize))!=0)
    pg+=1;

    return pg;

 }

 onPaginationChange(val){
   console.log(val);
   Object.assign(this.querys,this.querys, {PageIndex: val});
   this.queryDealInfo();
 }

 getGameIcon(groupId){
   
    switch(groupId){
      case 1: return (<img className="game-icon" src={fc3d} alt="fc3d" />);
            break;
        case 2: return (<img className="game-icon" src={ssc} alt="ssc" />);
            break;
        case 3: return (<img className="game-icon" src={kl8} alt="kl8" />);
            break;
        case 4: return (<img className="game-icon" src={xylc} alt="kl10" />);
            break;
        case 5: return (<img className="game-icon" src={keno} alt="KENO" />);
            break;
        case 6: return (<img className="game-icon" src={pl3} alt="pl3" />);
            break;
        case 7: return "分分彩";
            break;
        case 8: return "真人彩";
            break;
        case 9 : return (<img className="game-icon" src={pk10} alt="pk10" />);
        break;


    }
 }

    render() {
      var { deals } = this.props;
      let data = deals.Data || [];
      let page = this.getNumberOfPages(deals.TotalCount, deals.PageSize);
      let idx = deals.PageIndex;


      const showDetail = (bet) => {
        const dialog = RenderHelper.dialog(null,
            <BetRecord betData={bet}  close={ ()=> close() } />);

        const close = (data) => {
          dialog.destroy();
        }
    }



      return(
          <div className="this-month-bet">
          <List renderHeader={() => '本月列表'} className="my-list">
            { data.map((element,i) => {

              let profit = 0;
              if(element.Status != 0 && element.Status != 5)
              {
                profit = element.WinBets > 0 ? element.BetAmount * element.Scale * element.WinBets : element.Turnover * -1;
              }



              return(

                  <Item key = {i}  extra={
                    <div className="bet-money">
                      <span className={profit >= 0 ? "fn-color-green" : "fn-color-red"} >
                        {(profit != 0)? "¥" + profit.toFixed(2) : <p></p> }
                        
                      </span>
                        <span>
                          {betHelper.getBetHistoryStatus(element.Status, element.WinBets,element.BetAmount,element.Scale,element.Turnover)}
                        </span>
                        {/* <div className="img-icon">
                          <img src={pending} alt="pending" />
                        </div> */}
                    </div>
                  }
                    align="top"
                    thumb={this.getGameIcon(element.GroupId)}
                    multipleLine
                    onClick={()=>{
                      showDetail(element);
                    }}>
                    
                      <Brief>
                        <span>{betHelper.getGameNameByGroupId(element.GroupId)} - </span>
                        <span>第{element.Qh}期 - </span>
                        <span><span>{element.GameTypeName}</span></span>
                      </Brief>
               
                    <Brief>
                      投注号码：<span style={{color: 'red'}}>{betHelper.betNumBeautify(element.BetNumber)}</span>
                    </Brief>

                    <Brief style={{fontSize: '12px', color: '#cccccc'}}>{element.CreateTime}</Brief>

                  </Item>

            )

            })}
          </List>
          {
            (page < 2) ? <p></p>:
            <Pagination total={page}
              className="custom-pagination-with-icon"
              current={idx}
              locale={{
                prevText: (<span className="arrow-align"><Icon type="left" />上一页</span>),
                nextText: (<span className="arrow-align">下一页<Icon type="right" /></span>),
              }}
              onChange={this.onPaginationChange.bind(this)}
           />
          }
          

        
          </div>)
    }
};

export default connect(state => ({
  deals: state.dealinfo.deals,
}))(ThisMonthBet);
