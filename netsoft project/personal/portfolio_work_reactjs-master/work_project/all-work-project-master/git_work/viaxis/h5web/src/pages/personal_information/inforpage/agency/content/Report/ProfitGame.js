import React, { Component } from 'react';
import { connect } from 'dva';
import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';
import { List } from 'antd-mobile';

import * as BetHelper from "@/utils/bet_helper";
import Game from '@/components/Report/Game';
import * as RenderHelper from "@/utils/render_helper";
import DateRange from '@/components/DateRange';

import Link from 'umi/link';

const Item = List.Item;
const Brief = Item.Brief;



class ProfitGame extends Page {
  title = "游戏盈亏"

  constructor(props) {
    super(props);
  }
  

  /* ---------------------------- System  ------------------------------*/
  componentDidMount() {
    this.queryProfitGameTypeGroup();
  }

  
  /* ---------------------------- Functions  ------------------------------*/

  queryProfitGameTypeGroup() {
      this.props.dispatch({ type: "profit/queryProfitGameTypeGroup"})
  }

  queryData() {
    if (this.isNullOrEmpty(this.querys)) {
      this.queryTeam();
    } else {
      this.props.dispatch({ type: "user/queryUsers", payload: {pageIndex: this.pageIndex, querys: this.querys } })
    }
  }

  getUserId() {
    let userId = this.props.userId
    if (userId)
        userId = parseInt(userId);
    return userId;
  }

  isNullOrEmpty(values) {
    let isNull = true;
    try {
        Object.keys(values).forEach(it => {
            if (!Utils.isEmptyOrNull(values[it])) {
                isNull = false;
                throw "break";
            }
        });
    } catch (e) {
        if (e === "break") return;
        else
            throw e;
    }
    return isNull;
 }



  render() {
    const profitGameType = this.props.profitGameType || [];
    const showDetail = (profitGameType) => {
      const dialog = RenderHelper.dialog(null,
          <Game profitGameTypeData={profitGameType}  close={ ()=> close() } />);

      const close = () => dialog.destroy();
    }
    
    return (
      <div className="fund-history ">
        <div className="top-header"> </div>

        {/* date */}
        {/* <List className="date-select">
          <Item><DateRange/></Item>
        </List> */}

        {/* {profitGameType[0] ?  
        <div className="form_title">
          <h5>股东</h5>
        </div>
        : <div>暂无记录</div>} */}

        <div className="form_title">
          <h5>游戏盈亏</h5>
        </div>
        {profitGameType[0] ? 
        <div>
        <List className="my-list ">
        {profitGameType && profitGameType.map((element,i) => 
          
            <Item
              key={i}
              extra={
                <div className="bet-money add">
                  {/* {(element.ChildCount > 0) ? <span>￥{element.Balance} ></span> : <span>￥{element.Balance}</span>} */}
                  {element.Profit < 0 ? <span style={{color:'red'}}>￥{element.Profit}</span> : <span>￥{element.Profit}</span> }
                </div>
              }
              align="top"
              multipleLine
              onClick={() => {showDetail(element)}}
            >
              <span className="title-warp"><Link to={`/personal_information/inforpage/agency/content/ChaKanXiaJi/daiLi?token=${ element.UserId }`}>{element.GroupName}</Link></span>
              <span className="title-warp">{element.Id}</span>
              <Brief style={{fontSize: '12px', color: '#cccccc'}}>{element.Date}</Brief>
            </Item>                 
        )} 
        </List>
        </div>
        : <div>暂无记录</div>}
      </div>
    );
  }
};

function mapStateToProps(state) {
  return {
    team: state.user.team.Data,
    profitGameType: state.profit.profitGroupGameType,
  }
}

export default connect(mapStateToProps)(Form.create()(ProfitGame));
