/**
 * Routes:
 *   - ./src/routes/AuthorizationRoute.js
 */

import { connect } from 'dva';
import { Input, Select, Button, Badge, Table, Icon } from "antd";

import BasicComponent from "@/components/BasicComponent";
import SearchForm from '@/components/SearchForm';
import * as Utils from "@/utils/common";
import * as Constants from "@/app_constants";
import * as Enums from "@/app_enum";
import * as RenderHelper from "@/utils/render_helper";


class WithdrawList extends BasicComponent {
    constructor(props) {
        super(props);
        this.querys = {}
    }

    /* ---------------------------- System  ------------------------------*/

    componentWillMount() {
        this.queryWithdraw();
    }

    /* ---------------------------- Functions  ------------------------------*/

    queryWithdraw() {
        const { dispatch } = this.props;
        dispatch({
            type: "withdraw/queryWithdraw",
            payload: this.querys
        });
    }

    /* ---------------------------- Events  ------------------------------*/

    onSearchClick(values) {

        this.querys = values;
        this.queryWithdraw();
    }

    onResetClick() {
        this.querys = {};
    }


    /* ---------------------------- Renders  ------------------------------*/

    render() {

        const self = this;
        const { withdraws, loading } = this.props;
        let data = withdraws.Data || [];

        const pagination = {
            total: withdraws.TotalCount || 0,
            onChange(current) {
                self.querys.pageIndex = current;
                self.queryWithdraw();
            },
        };

        return (
            <div>
                {this.renderSearch()}
                <div className="search-result-list">
                    <Table loading={loading} columns={this.columns} dataSource={data} rowKey="Id" pagination={pagination} />
                </div>
            </div>
        );
    }

    renderSearch() {
        const formItemLayout = {
            labelCol: { span: 8 },
            wrapperCol: { span: 16 }
        };

        return (
            <SearchForm onSearch={this.onSearchClick.bind(this)} onReset={this.onResetClick.bind(this)}>
                <create_time label="申请时间" {...formItemLayout} format={(times) => Utils.dateRangeFormat(times)}>
                    {RenderHelper.rangeDatePick()}
                </create_time>
                <txid label="流水号" {...formItemLayout}>
                    <Input />
                </txid>
                <status label="状态" {...formItemLayout} options={{ initialValue: "" }}>
                    <Select>
                        <Select.Option value="">全部</Select.Option>
                        {Object.keys(Constants.WithdrawStatus).map(it =>
                            <Select.Option key={it} value={it.toString()}>
                                {Constants.WithdrawStatus[it].toString()}
                            </Select.Option>
                        )}
                    </Select>
                </status>
            </SearchForm>
        );
    }

    get columns() {
        const self = this;
        const columns = [
            { title: '#', render: RenderHelper.render_index },
            {
                title: "银行", render(record) {
                    return (
                        <span>
                            <i className={`iconfont icon-${record.BankCode}`}></i>&nbsp;
                        {record.BankName}
                        </span>
                    );
                }
            },
            { title: "卡号", dataIndex: "AccountNo", render: (num) => Utils.maskStart(num, 4).slice(num.length - 6) },
            { title: "户名", dataIndex: "AccountName", render: (name) => Utils.maskStart(name, name.length - 1) },
            { title: "金额", dataIndex: "Amount", className: 'column-money', render: RenderHelper.money_format },
            { title: "余额", dataIndex: "Balance", className: 'column-money', render: RenderHelper.money_format },
            { title: "流水号", dataIndex: "Txid" },
            { title: "状态", dataIndex: "Status", render: (status) => RenderHelper.render_status(Constants.WithdrawStatus, status) },
            { title: "备注", dataIndex: "Remark" },
            { title: "申请时间", dataIndex: "CreateTime", render: RenderHelper.render_time },
        ];
        return columns;
    }
}

function mapStateToProps(state) {
    return {
        withdraws: state.withdraw.historys,
        loading: state.loading.models.withdraw
    };
}
export default connect(mapStateToProps)(WithdrawList);