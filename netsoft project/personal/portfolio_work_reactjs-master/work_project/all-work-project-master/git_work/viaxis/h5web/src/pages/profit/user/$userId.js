/**
 * Routes:
 *   - ./src/routes/AuthorizationRoute.js
 */

import ProfitUser from './components/ProfitUser';

export default function (props) {
    return <ProfitUser {...props} />
}