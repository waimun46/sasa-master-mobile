/**
 * Routes:
 *   - ./src/routes/AuthorizationRoute.js
 */

import { connect } from 'dva';
import { Input, Select, Button, Badge, Tooltip, Popover, Table, Icon, Modal, message } from "antd";
import {notification} from "antd";
import router from 'umi/router';
//import { Modal } from 'antd-mobile';


import BasicComponent from "@/components/BasicComponent";
import GameSummary from '../components/GameSummary';
import BetTypeSelect from "../components/BetTypeSelect";
import BetNumSelect from "../components/BetNumSelect";
import TraceSelect from "../components/TraceSelect";
import BetOrderList from "../components/BetOrderList";
import GameList from "../components/GameList";

import * as Utils from "@/utils/common";
import * as Constants from "@/app_constants";
import * as Enums from "@/app_enum";
import * as BetHelper from "@/utils/bet_helper";
import * as RenderHelper from "@/utils/render_helper";
import { history } from 'react-router';

//const alert = Modal.alert;


class Bet extends BasicComponent {
    

    constructor(props) {
        super(props);
        this.group = this.getGameGroup(props);
        this.state = {
            // 是否追号
            isTrace: false,
            // 是否投注中
            betting: false,
            gametype: null,
            // 总注数
            totalBetCount: 0,
            // 总金额
            totalBetAmount: 0,
            // 投注号码，格式:
            // {gameType,betCount,wager,betNum,unitPos}
            betNums: [],
            // 追号数据
            traceData: null,
            // 中奖停止追号
            traceStopOnWin: true,
            //
            temp:{},

            wager:1,
           
        };
    }

    /* ---------------------------- 系统函数 ------------------------------*/

    componentWillMount() {
        let name = BetHelper.getGameNameByGroup(this.group);
        this.props.dispatch({ type: "global/setTitle", payload: name });
        //this.props.dispatch({type: 'betCart/clear'});

        //this.setState({currentPath:this.props.location.pathname});

        //this.currentUrl = this.props.location.pathname;

    }

    componentWillReceiveProps(newProps) {
        if (newProps.params != this.props.params) {
            const group = this.getGameGroup(newProps);
            if (group != this.group) {
                this.group = group;
                this.clearBetOrders();
                this.props.dispatch({ type: "global/setTitle", payload: group });
                if (this.state.isTrace)
                    this.setState({ isTrace: false });
            }
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps != this.props || nextState != this.state;
    }

    /* ---------------------------- 自定义函数 ------------------------------*/

    getGameGroup(props) {
        let gameGroup = (props.match.params.group || Constants.GameGroup[0]).toUpperCase();
        if (Constants.GameGroup.indexOf(gameGroup) < 0)
            browserHistory.push("/404");
        return gameGroup;
    }


    bet() {
        var { betNums, traceData, traceStopOnWin } = this.state;
        var qh = this.props.current.Qh;
        var data = [];
        for (var i in betNums) {
            var { gameType, betCount, wager, betNum, unitPos } = betNums[i];
            var betItem = BetHelper.buildBetNumData(gameType, qh, wager, betNum, unitPos, traceData, traceStopOnWin);
            data.push(betItem);
        }

        // if (__DEV__)
        //     console.log(data);

        this.setState({ betting: true });
        this.props.dispatch({ type: "dealinfo/bet", payload: data }).then(it => {
            this.setState({ betting: false, isTrace: false });
            var resp = it;
            if (resp.code == 0) {
                this.resetBetNum();

                
                notification.open({
                    message: '投注成功',
                    description: `共接受${resp.data.length}笔`,
                    icon: <Icon type="smile-circle" style={{ color: '#108ee9' }} />,
                });
            } else {
                var msg = Constants.ERROR_CODES.BET[resp.code] || resp.message || "未知的错误";
                Modal.error({
                    title: `投注失败，${msg}`
                });
            }

            router.push("/");


        });

    }

    resetBetNum() {
        this.setState({
            totalBetCount: 0,
            totalBetAmount: 0,
            betNums: []
        });
    }

    displayTrace(show) {
        if (show == this.state.isTrace) return;

        this.setState({ isTrace: show });
    }

    /**
     * 总投资金额，包括追号在内。
     * @return {[type]} [description]
     */
    get turnover() {
        const { isTrace, traceData, totalBetAmount } = this.state;

        if (!isTrace || !traceData) return totalBetAmount

        let turnover = 0;
        traceData.forEach(it => turnover += it.multiple * totalBetAmount);
        return turnover;
    }

    clearBetOrders() {
        this.setState({ betNums: [], totalBetCount: 0, totalBetAmount: 0 })
        this.props.dispatch({type: 'betCart/clear'});
      
        //router.go(-1);

    }

    /* ---------------------------- 组件事件 ------------------------------*/

    onGameTypeChange(gametype) {
        this.setState({ gametype });
    }

    /**
     * 追号数据变更
     * @param  {[type]} traceData [description]
     * @param  {[type]} stopOnWin [description]
     * @return {[type]}           [description]
     */
    onTraceDataChange(traceData, stopOnWin) {
        this.setState({ traceStopOnWin: stopOnWin, traceData });
    }

    /**
     * 添加投注内容事件
     * @param { GameType } gameType 玩法
     * @param { number } betCount 注数
     * @param { number } wager 单注金额
     * @param { dict|str } betNum 投注号码
     * @param { array|null } unitPos 任选数位
     */
    onBetNumAdd(gameType, betCount, wager, betNum, unitPos) {
       
        var betNums = [{ gameType, betCount, wager, betNum, unitPos }, ...this.state.betNums];
        this.setState({temp:betNums});

         var { totalBetCount, totalBetAmount } = this.state;
        // totalBetCount += betCount;
        // totalBetAmount += betCount * wager;
         this.setState({ betNums, totalBetCount, totalBetAmount });
        // var { betNums, totalBetCount, totalBetAmount,traceData, traceStopOnWin } = this.state;
        // this.props.dispatch({type: 'betCart/add', payload : {betNums, totalBetCount, totalBetAmount, traceData, traceStopOnWin}});
       
    }

    onBetOrderRemove(index) {
        var { betNums, totalBetCount, totalBetAmount } = this.state;
        totalBetCount -= betNums[index].betCount;
        totalBetAmount -= betNums[index].betCount * betNums[index].wager;
        betNums = [...betNums.slice(0, index), ...betNums.slice(index + 1)];
        this.setState({ betNums, totalBetCount, totalBetAmount });
    }

    updateCartAndCheckout(){

        //var betNums = [{ gameType, betCount, betNum, unitPos }, ...this.state.temp];
        var betNums = this.state.temp;
        const [{gameType, betCount, betNum, unitPos}] = betNums;
        

        let wager = this.state.wager;
        betNums = [{...this.state.betNums[0], wager}];

        var { totalBetCount, totalBetAmount } = this.state;
        totalBetCount += betCount;
        totalBetAmount += betCount * wager;
        this.setState({ totalBetCount, totalBetAmount });

        var { traceData, traceStopOnWin } = this.state;
        this.props.dispatch({type: 'betCart/add', payload : {betNums, totalBetCount, totalBetAmount, traceData, traceStopOnWin}});

        router.push("/resit");

    }

    onWagerUpdate(val){
        this.setState({wager : val});
    }

    onBetOrderClear() {
        this.clearBetOrders();
    }

   

    /**
     * 确认投注按钮
     * @return {[type]} [description]
     */
    onSubmitClick() {
        const { betNums, totalBetCount, isTrace, traceData, traceStopOnWin } = this.state;
        const turnover = this.turnover;

        if (totalBetCount <= 0 || turnover <= 0) {
            message.warning('请您先添加有效的投注计划!');
            return;
        }

        let msg = `您共选择了${betNums.length || 0} 笔, 共${totalBetCount}注，共￥${turnover}元`;
        if (isTrace) {
            msg = `您共选择了${betNums.length || 0} 笔, 共${totalBetCount}注，追${Object.keys(traceData).length}期，共￥${turnover}元`
        }

        Modal.confirm({
            iconType: "exclamation-circle",
            title: msg,
            content: "一旦接受投注，不可撤回，您确定吗?",
            onOk: this.bet.bind(this)
        });
    }

    /* ---------------------------- Renders ------------------------------*/

    render() {
        var { current } = this.props;
        var betButtonDisabled = !(this.state.totalBetCount > 0 && this.state.totalBetAmount > 0 && BetHelper.isAcceptingBet(current));
        return (
            <div className="game-body">
                <GameSummary Group={this.group}   />
                <div className="game-body-left">
                    {this.state.isTrace
                        ?
                        <TraceSelect group={this.group} amount={this.state.totalBetAmount} onChange={this.onTraceDataChange.bind(this)} />
                        :
                        <div>
                            <BetTypeSelect Group={this.group} GameType={this.state.gametype} onChange={this.onGameTypeChange.bind(this)} />
                            <BetNumSelect 
                                Group={this.group} 
                                GameType={this.state.gametype} 
                                onBetNumAdd={this.onBetNumAdd.bind(this)} 
                                onSubmit={this.onSubmitClick.bind(this)} />
                        </div>
                    }
                    <BetOrderList 
                        data={this.state.betNums} 
                        totalBetAmount={this.state.totalBetAmount} 
                        onClear={this.onBetOrderClear.bind(this)} 
                        onRemove={this.onBetOrderRemove.bind(this)} 
                        onWagerUpdate = {this.onWagerUpdate.bind(this)}
                        updateCartAndCheckout = {this.updateCartAndCheckout.bind(this)}
                       // onSubmit={this.onBetNumAdd.bind(this)}
                        />
                </div>
               
                <div className="fn-clear"></div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { deals } = state.dealinfo;
    return {
        current: state.gamedata.current,
        user: state.user.userinfo,
        loading: state.loading.models.gamedata
    };
}
export default connect(mapStateToProps)(Bet);
