/**
 * Routes:
 *   - ./src/routes/AuthorizationRoute.js
 */

import { connect } from 'dva';
import { Input, Select, Button, Badge, Tooltip, Popover, Table, Icon } from "antd";

import BasicComponent from "@/components/BasicComponent";
import SearchForm from '@/components/SearchForm';
import * as Utils from "@/utils/common";
import * as Constants from "@/app_constants";
import * as Enums from "@/app_enum";
import * as BetHelper from "@/utils/bet_helper";
import * as RenderHelper from "@/utils/render_helper";

import GameDataContent from './components/GameDataContent';

class DealHistory extends BasicComponent {

    constructor(props) {
        super(props);
        this.querys = this.defaultQuerys = Object.assign({}, Utils.getQueryString(), { not_trace: true });
    }

    /* ---------------------------- 系统函数 ------------------------------*/

    componentWillMount() {
        const { groups } = this.props;
        if (!groups || groups.length == 0)
            this.queryGameGroup();

        this.initGroup(groups);

        this.queryDealInfo();
    }

    componentWillUpdate(nextProps, nextState) {
        if (nextProps.groups != this.props.groups) {
            this.initGroup(nextProps.groups);
        }
    }

    /* ---------------------------- 自定义函数 ------------------------------*/

    queryDealInfo() {
        this.props.dispatch({
            type: 'dealinfo/DealInfo',
            payload: this.querys,
        });
    }

    queryGameGroup() {
        this.props.dispatch({
            type: 'gameGroup/getGameGroup'
        });
    }

    initGroup(groups) {
        const group = {};
        for (let i in groups) {
            group[groups[i].Id] = groups[i].Name;
        }
        this.group = group;
    }

    get isShowEmail() {
        // const userinfo = this.props.userinfo.data;
        // return userinfo && userinfo.UserKind < Enums.UserKind.Member;
        false;
    }

    queryTrace(refId) {
        this.lastQuerys = this.querys;
        this.querys = { ref_id: refId };
        this.queryDealInfo();
    }

    clearTrace() {
        this.querys = this.lastQuerys || this.defaultQuerys;
        this.queryDealInfo();
    }

    /* ---------------------------- Events ------------------------------*/

    onSearchClick(values) {
        this.querys = Object.assign({}, this.querys, values);
        this.queryDealInfo();
    }

    onResetClick() {
        this.querys = this.defaultQuerys;
    }

    /* ---------------------------- Renders ------------------------------*/
    render() {
        const self = this;

        var { dealinfo } = this.props;
        let data = dealinfo.Data || [];

        const pagination = {
            current: dealinfo.PageIndex || 1,
            total: dealinfo.TotalCount || 0,
            onChange(current) {
                self.querys = Object.assign({}, self.querys, { PageIndex: current });
                self.queryDealInfo();
            },
        };

        const tableProps = {};
        if (this.querys.ref_id)
            tableProps['title'] = () => <Button icon="rollback" type="primary" onClick={() => this.clearTrace()}>返回</Button>;

        return (
            // <DashContainer title="下注列表">
            <div>
            <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
                {this.renderSearch()}
                <div className="search-result-list">
                    <Table
                        rowKey={record => record.Id}
                        columns={this.columns}
                        dataSource={data}
                        pagination={pagination} />
                </div>
            </div>
            // </DashContainer>
        );
    }

    get columns() {
        const self = this;
        const columns = [
            { title: '#', render: RenderHelper.render_index, width: 30, fixed: 'left' },
            {
                title: "彩种", dataIndex: "GroupId", render(id) {
                    return self.group[id] || "Unknow";
                }
            },
            {
                title: '期号', dataIndex: 'Qh', render: (qh, deal) => {
                    return (
                        <span>
                            {qh} &nbsp;&nbsp;
                        <Popover content={<GameDataContent group={deal.GroupId} qh={qh} />} trigger="click">
                                <a><Icon type="info-circle-o" /></a>
                            </Popover>
                        </span>
                    );
                }
            },
            {
                title: '玩法', dataIndex: 'GameTypeName', render: (name, it) => {
                    if (it.RefId > 0) {
                        return (
                            <span className="zhui">
                                {
                                    self.querys.ref_id == it.RefId ?
                                        name :
                                        <a onClick={() => self.queryTrace(it.RefId)}>
                                            {name}
                                        </a>
                                }
                                <label className="zhui"></label>
                            </span>
                        );
                    }
                    return name;
                }
            },
            {
                title: '投注内容', width: 120, dataIndex: 'BetNumber', render(num) {
                    const beautityNum = BetHelper.betNumBeautify(num);
                    return <Tooltip title={beautityNum}><p className="betnum">{beautityNum}</p></Tooltip>
                }
            },
            { title: "投注金额", dataIndex: "BetAmount", className: 'column-money', render: RenderHelper.money_format },
            { title: "总金额", dataIndex: "Turnover", className: 'column-money', render: RenderHelper.money_format },
            {
                title: "盈亏", className: "fn-text-right", render(deal) {
                    if (deal.Status == 0) return '';
                    else if (deal.Status == 5) return 0;
                    var profit = deal.WinBets > 0 ? deal.BetAmount * deal.Scale * deal.WinBets : deal.Turnover * -1;
                    return <span className={profit >= 0 ? "fn-color-green" : "fn-color-red"}>{profit.toFixed(2)}</span>;
                }
            },
            { title: "赔率", dataIndex: "Scale" },
            {
                title: "状态", dataIndex: "Status", render: (it, deal) => {
                    let status = null;
                    if (it == Enums.DealStatus.UnOpen)
                        status = Constants.DealStatus[0];
                    else if (it <= Enums.DealStatus.Commission) {
                        const profit = deal.WinBets > 0 ? deal.BetAmount * deal.Scale * deal.WinBets : deal.Turnover * -1;
                        if (profit > 0)
                            status = Constants.DealStatus[1];
                        else
                            status = Constants.DealStatus[2];
                    } else
                        status = Constants.DealStatus[3];

                    return <Badge status={status.status} text={status.toString()} />
                }
            },
            { title: "投注时间", dataIndex: "CreateTime", render: RenderHelper.render_time },
        ];
        if (this.isShowEmail)
            columns.splice(1, 0, { title: 'Email', dataIndex: "Email" });
        return columns;
    }

    renderSearch() {
        const { groups } = this.props;
        const formItemLayout = {
            labelCol: { span: 8 },
            wrapperCol: { span: 16 }
        };

        return (

            <SearchForm onSearch={this.onSearchClick.bind(this)} onReset={this.onResetClick.bind(this)}>
                {this.isShowEmail && <email label="Email" {...formItemLayout}>
                    <Input />
                </email>}
                <groupId label="彩种" {...formItemLayout} options={{ initialValue: "" }}>
                    <Select>
                        <Select.Option value="">全部</Select.Option>
                        {(groups || []).map(group => {
                            return <Select.Option value={group.Id.toString()} key={group.Id}>{group.Name}</Select.Option>
                        })}
                    </Select>
                </groupId>
                <qh label="期号">
                    <Input placeholder="期号" size="default" />
                </qh>
                <winloss_status label="状态" {...formItemLayout} options={{ initialValue: "" }}>
                    <Select>
                        <Select.Option value="">全部</Select.Option>
                        {Object.keys(Constants.DealStatus).map(it => {
                            return <Select.Option value={it} key={it}>{Constants.DealStatus[it].toString()}</Select.Option>
                        })}
                    </Select>
                </winloss_status>
                <create_time label="投注时间" {...formItemLayout} format={(times) => Utils.dateRangeFormat(times)}>
                    {RenderHelper.rangeDatePick()}
                </create_time>
            </SearchForm>
        );
    }
}

function mapStateToProps(state) {
    const { deals } = state.dealinfo;
    return {
        dealinfo: deals,
        groups: state.gameGroup,
        loading: state.loading.models.dealinfo
    };
}
export default connect(mapStateToProps)(DealHistory);
