import React, {Component} from 'react';
import { connect } from 'dva';
import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';
import * as BetHelper from "@/utils/bet_helper";
import router from 'umi/router';

import {Icon, Timeline, message, notification } from "antd";
import { NoticeBar, WhiteSpace,Tag, Modal, Toast} from 'antd-mobile';
import Link from 'umi/link';
import { isConstructorDeclaration } from 'typescript';

import * as Utils from "@/utils/common";
import * as Render from "@/utils/render_helper";
import * as Constants from "../../app_constants";

const alert = Modal.alert;

function onChange(selected) {
  console.log(`tag selected: ${selected}`);
}

class ResitList extends Page {
  title = "彩购列表"

  constructor(props){
    super(props);
    this.state = {
      betting: false,
    }
    this.onSubmitClick.bind(this);
  }

  onBetAgain = ( )=> {
    router.go(-2);
  }

  onBetOrderRemove(index) {
    // var { totalBetCount, totalBetAmount, traceData, traceStopOnWin  } =this.props.betCart;
    // let betNums = this.props.betCart.betNums[0];
    // totalBetCount -= betNums[index].betCount;
    // totalBetAmount -= betNums[index].betCount * betNums[index].wager;
    // betNums = [...betNums.slice(0, index), ...betNums.slice(index + 1)];
    this.props.dispatch({type: 'betCart/removeBet', payload : index});
   // this.setState({ betNums, totalBetCount, totalBetAmount });
}

onClearAllBet(){
  this.props.dispatch({type: 'betCart/clear'});
}

  onSubmitClick() {
    const { betNums, totalBetCount, isTrace, traceData, traceStopOnWin } = this.props.betCart;
    const turnover = this.turnover;

    if (totalBetCount <= 0 || turnover <= 0) {
        Toast.info('请您先添加有效的投注计划!',2);
        //message.warning('请您先添加有效的投注计划!');
        return;
    }

    let msg = `您共选择了${betNums.length || 0} 笔, 共${totalBetCount}注，共￥${turnover}元`;
    if (isTrace) {
        msg = `您共选择了${betNums.length || 0} 笔, 共${totalBetCount}注，追${Object.keys(traceData).length}期，共￥${turnover}元`
    }

    alert('投注需知', '一旦接受投注，不可撤回，您确定吗?', [
      { text: 'Cancel', onPress: () => console.log('cancel') },
      { text: 'Ok', onPress: () => {this.bet.bind(this); this.bet();} },
    ])


    // Modal.confirm({
    //     iconType: "exclamation-circle",
    //     title: msg,
    //     content: "一旦接受投注，不可撤回，您确定吗?",
    //     onOk: this.bet.bind(this)
    // });
}

onTraceClick(){

  const { betNums, totalBetCount, isTrace, traceData, traceStopOnWin } = this.props.betCart;
  const turnover = this.turnover;

  if (totalBetCount <= 0 || turnover <= 0) {
      Toast.info('请您先添加有效的投注计划!',2);
      //message.warning('请您先添加有效的投注计划!');
      return;
  }

  router.push('/resit/content/ZhuiHao');

}

bet() {
  var {  betNums, traceData, traceStopOnWin } = this.props.betCart;
  //let betNums = this.props.betCart.betNums;
  var qh = this.props.current.Qh;
  var data = [];

  betNums.map((item,i)=>{

    let betItem = BetHelper.buildBetNumData(item.gameType, qh, item.wager, item.betNum, item.unitPos, traceData, traceStopOnWin);
      data.push(betItem);

  });
  

  this.setState({ betting: true });
  this.props.dispatch({ type: "dealinfo/bet", payload: data }).then(it => {
      this.setState({ betting: false, isTrace: false });
      var resp = it;
      if (resp.code == 0) {
         
          let successMsg = '投注成功,共接受' + resp.data.length + '笔';

          Toast.success(successMsg, 2);
      } else {
          var msg = Constants.ERROR_CODES.BET[resp.code] || resp.message || "未知的错误";
        
          alert('投注失败', msg, [
            { text: 'Ok', onPress: () => console.log('error') },
          ])


      }
      this.resetBetNum.bind(this);
      this.resetBetNum();

    //  router.push("/bet_history");


  });

}

resetBetNum() {
  this.props.dispatch({type: 'betCart/clear'});
}

get turnover() {
  const { isTrace, traceData, totalBetAmount } = this.props.betCart;

  if (!isTrace || !traceData) return totalBetAmount

  let turnover = 0;
  traceData.forEach(it => turnover += it.multiple * totalBetAmount);
  return turnover;
}

getGameName(item){
  
  var sb = [item.gameType.Sort];
  if (item.gameType.Sort != item.gameType.Kind)
      sb.push(item.gameType.Kind);
  if (item.gameType.Name != item.gameType.Sort && item.gameType.Name != item.gameType.Kind)
      sb.push(item.gameType.Name);
  return sb.join("-");
}



  render() {

    const getBetNum =  (item) => {
      var betNum = item.betNum;
      if (Utils.isString(item)) {
          var nums_split_symbols = item.gameType.MaxNum > 9 ? Constants.TWO_DIGITS_NUMS_SPLIT_CHARS : Constants.NUMS_SPLIT_CHARS;
          var num_split_symbols = item.gameType.MaxNum > 9 ? Constants.NUM_SPLIT_CHAR : null;
          betNum = BetHelper.removeInvalidBetNum(item.betNum, item.gameType.BetNumCount, nums_split_symbols, num_split_symbols);
      }
      return BetHelper.convertBetNumToString(item.gameType, betNum, item.unitPos);


  }

    const { betNums, totalBetCount, totalBetAmount } = this.props.betCart;

    return (

      <div className="resit-list">

        <div className="top-header">
          <div className="top-content-warp">
            <div className="add warp">
              <div className="content-warp">
              <a onClick={this.onBetAgain}>
                <Icon type="plus"/>
                <label>再选一注</label>
              </a>
                
              </div>
            </div>
            <div className="random warp">
              <div className="content-warp">
                <Icon type="shake"/>
                <label>机选一注</label>
              </div>
            </div>
            <div className="delete warp">
              <div className="content-warp">

              <a onClick={this.onClearAllBet.bind(this)}>
                <Icon type="delete"/>
                <label>清空列表</label>
              </a>
       
              </div>
            </div>

            <hr/>

            <div className="bet-total">
              共<span>{totalBetCount}</span>注, 金额<span>￥{totalBetAmount}</span>元
            </div>
            <div className="clearfix"></div>
          </div>
        </div>


        <div className="payment-result">
        {
          betNums.map((item,i) =>{
            return (
             
              <Tag key={i} closable
               onClose={() => {
                this.onBetOrderRemove(i);
                 console.log('onClose');
               }}

               afterClose={() => {
                 console.log('afterClose');
               }}
          >
          <div className="warp timeLinecontainer">
          <ul> 
              <li>
                <span className="icon-1"><Icon type="shopping"/></span>
                <div className="text-warp">
                  <b style={{color: 'red'}}>{getBetNum(item)}</b>
                  <p>{this.getGameName(item)}</p>
                </div>
              </li>
              <li>
                <span className="icon-2"><Icon type="red-envelope"/></span>
                <div className="text-warp">
                  <b>{item.betCount} 注</b>
                  <p style={{color: 'red'}}>{item.wager}元</p>
                </div>
              </li>
            </ul>
          
          
          </div>
        
          

          </Tag>
            )

          
          })

          
        }

        </div>



        <div className="ButtonBottom">
        
          <a onClick={this.onTraceClick.bind(this)}>
          <div className="btn_left">
              智能追号
            </div>

          </a>
          
       
          <a onClick={this.onSubmitClick.bind(this)}>
            <div className="btn_right">
              立即投注
            </div>
          </a>
          <div className="clearfix"></div>
        </div>

      </div>

    );
  }
}

export default connect(state => ({

  betCart : state.betCart,
  current: state.gamedata.current,
  

}))(Form.create()(ResitList));
