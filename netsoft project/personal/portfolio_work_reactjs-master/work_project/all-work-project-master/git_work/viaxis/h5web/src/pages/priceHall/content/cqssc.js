import React, { Component } from 'react';
import { connect } from 'dva';
import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';
import { List } from 'antd-mobile';

import * as BetHelper from "@/utils/bet_helper";
import TeamRecord from '@/components/teamRecord';
import * as RenderHelper from "@/utils/render_helper";
import DateRange from '@/components/DateRange';
import cqssclogo from "../../../assets/images/game-icon/ssc.png";

import Link from 'umi/link';

const Item = List.Item;
const Brief = Item.Brief;


class Cqssc extends Page {
  title = "查看下级"

  constructor(props) {
    super(props);

    const query = new URLSearchParams(this.props.location.search);
    this.qr = query.get('qr')

  }

  
  

  /* ---------------------------- System  ------------------------------*/
  componentWillMount() {
    this.queryGameData();
  }

  
  /* ---------------------------- Functions  ------------------------------*/
   
  queryGameData() {
    this.props.dispatch({ type: "gamedata/queryGameData",payload: { group_name: 'CQSSC', querys: this.qr  }})
  } 
  
  splitNum(num) {
    let num1 = num.split(",");
    let num2 = [];
    for(let i = 0; i < num1.length; i++) {
      num2.push(num1);
      return num2;
    }
  }

 /* ---------------------------- Events ------------------------------*/



  render() {
    const data = this.props.data || [];
    
    return (
      <div className="danielph2">
        <div className="fund-history ">
        <div className="top-header"> </div>

        {/* date */}
        <List className="date-select">
          <Item><DateRange/></Item>
        </List>

        <div className="form_title">
          <h5>开奖数据</h5>
        </div>
        <List className="my-list ">
        {data && data.map((element,i) => 
            <Item
              key={i}
              extra={
                <div className="bet-money add">
                  <span>第{element.Qh}期 ></span>
                </div>
              }
              align="top"
              thumb={<img className="logosizee" src={cqssclogo}/>}
              multipleLine
            >
              <span className="title-warp" style={{ color:'black' ,fontSize:'20px'}} >重庆时时彩</span>
              {this.splitNum(element.Result).map((ele) => 
              <Brief style={{fontSize: '18px', color: '#000000'}}><div className="danielph" style={{display:"flex"}}>{ele.map((e) => <div className="dCircleNum" >{e}</div>)}</div></Brief>
              )}
            </Item>               
        )}  
        </List>
      </div>
      </div>
    );
  }
};

function mapStateToProps(state) {
    return {
      data: state.gamedata.data.Data,
    }
  }
  
  export default connect(mapStateToProps)(Cqssc);
