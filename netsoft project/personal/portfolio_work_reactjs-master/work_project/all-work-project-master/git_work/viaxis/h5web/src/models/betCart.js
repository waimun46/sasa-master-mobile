
export default {
    namespace: 'betCart',
    state: {
             
            betNums: [],
            // 追号数据
            traceData: null,
            // 中奖停止追号
            traceStopOnWin: true,
            // 总注数
            totalBetCount: 0,
            // 总金额
            totalBetAmount: 0,

            isTrace: false,

                
    },
    reducers: {
        add(state, { payload }) {

            let nextTotalBetCount =  state.totalBetCount + payload.totalBetCount;
            let nextTotalBetAmount =  state.totalBetAmount + payload.totalBetAmount;
           
            
            return { ...state, 
                    betNums:state.betNums.concat(payload.betNums),
                    totalBetAmount: nextTotalBetAmount,
                    totalBetCount : nextTotalBetCount
                     
                };
        },
        clear(state, { payload }) {
            
            return { ...state, 
                betNums: [],
                totalBetAmount: 0,
                totalBetCount : 0,
                traceData: null,
                isTrace: false,   
            };
        },
        removeBet(state, { payload }) {

           
            let index = payload;
            let bet = state.betNums[index];

            let nextBetNums = [...state.betNums.slice(0, index), ...state.betNums.slice(index + 1)];
            let nextTotalBetCount = state.totalBetCount - bet.betCount;
            let nextTotalBetAmount = state.totalBetAmount - ( bet.betCount *  bet.wager);

            
            return { ...state, 
                betNums: nextBetNums,
                totalBetAmount: nextTotalBetAmount,
                totalBetCount : nextTotalBetCount                 
            };
        },
        updateTrace(state, { payload }) {

            return { ...state, traceData : payload.traceData, traceStopOnWin: payload.stopOnWin, isTrace:true };
        },
        
        
    },

};
