import React, {Component} from 'react';
import { Card, Icon, Avatar, Button  } from 'antd';

import { connect } from 'dva';
import * as RenderHelper from "@/utils/render_helper";
import Link from 'umi/link';
import user from '../../../assets/images/icon/user01.png'
import time from '../../../assets/images/icon/time.png'
import money from '../../../assets/images/icon/money.png'
import ip from '../../../assets/images/icon/ip.png'

const { Meta } = Card;

class TopInfor extends Component {

  constructor(props) {
    super(props);
    
    }

    componentDidMount() {
      this.getUserInfo();
      this.getLastLogin();
  }

   getUserInfo() {
        this.props.dispatch({ type: "user/queryUserInfo" });
    }

    getLastLogin() {
        this.props.dispatch({ type: "user/getLastLoginLog" });
    }


    render() {

      const userInfo = this.props.userinfo;
      const lastLogin = this.props.lastlogin;

        return (
          <div className="top-content">
            <div className="top-warp">
              <div className="left-content"><img src={user} alt="user"/></div>
              <div className="right-content">
                <label>用户</label>
                <p>{userInfo && userInfo.Email}</p>
              </div>
              <div className="clearfix"></div>
            </div>
            <div className="top-warp">
              <div className="left-content"><img src={time} alt="time"/></div>
              <div className="right-content">
                <label>上次登入时间</label>
                <p>{lastLogin && RenderHelper.render_time(lastLogin.CreateTime)}</p>
              </div>
              <div className="clearfix"></div>
            </div>
            <div className="top-warp">
              <div className="left-content"><img src={money} alt="money"/></div>
              <div className="right-content">
                <label>账户余额</label>
                <p>{userInfo && RenderHelper.money_format(userInfo.Balance)}</p>
              </div>
              <div className="clearfix"></div>
            </div>
            <div className="top-warp">
              <div className="left-content"><img src={ip} alt="ip"/></div>
              <div className="right-content">
                <label>上次登入IP</label>
                <p>{lastLogin && lastLogin.IP}</p>
              </div>
              <div className="clearfix"></div>
            </div>
          
            <div className="top-btn">
              <Link to="/personal_information/inforpage/fund-management/content/topup/SelectRechargeMethod">
              <div className="btn-1 warp">
                <Button type="primary" icon="money-collect">充值</Button>
              </div>
              </Link>
              <div className="btn-2 warp">
              <Link to='/withdraw'>
                <Button type="primary" icon="wallet">提现</Button>
              </Link>
               
              </div>
              <div className="clearfix"></div>
            </div>
          <div className="clearfix"></div>
       
          </div>
        );
    }
};


export default connect(state => ({
    userinfo: state.user.user,
    lastlogin: state.user.lastLogin,
    loading: state.loading.models.user,
}))(TopInfor);

