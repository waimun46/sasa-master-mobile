/**
 * Routes:
 *   - ./src/routes/AuthorizationRoute.js
 */

import { connect } from 'dva';
import { message, Row, Col, Alert, Tabs, Button, Input,Icon } from "antd";
import { List, InputItem, WhiteSpace } from 'antd-mobile';

import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';
import * as RenderHelper from "@/utils/render_helper";

class LoginPassword extends Page {
    title = "修改登录密码"
    constructor(props) {
        super(props);
        this.state = {
            // 提交按钮
            loading: false,
        }
        // 初始验证码地址,否则每次表单输内容验证码都会变化
        this.captchaUrl = RenderHelper.getCaptchaImgUrl();
    }

    /* ---------------------------- Sys Functions ------------------------------*/


    /* ---------------------------- Functions ------------------------------*/

    refreshCaptcha() {
        let captcha = this.props.form.getFieldInstance("captcha");
        captcha.clearInput();
        captcha.inputRef.focus();
        this.refs.captcha_img.src = RenderHelper.getCaptchaImgUrl();
    }

    /* ---------------------------- Events ------------------------------*/

    handleUpdateSubmit(e) {
        e.preventDefault();

        const { dispatch } = this.props;

        this.form.validateFields((errors, values) => {

            if (errors) return;

            dispatch({
                type: "user/updateLoginPassword",
                payload: { currentPwd: values.current_pwd, password: values.password, captcha: values.captcha }
            }).then(resp => {
                this.setState({ loading: false });
                if (resp.code == 0) {
                    let msg = "您的密码已修改，请重新登录";
                    message.success(msg);
                    this.props.form.resetFields();
                } else {
                    this.refreshCaptcha();
                    message.error(resp.message);
                }

            });

            this.setState({ loading: true });
        })
    }

    checkPassword(rule, value, callback) {
        if (value && value !== rule.getFieldValue('password')) {
            callback(rule.message);
        } else {
            callback();
        }
    }

    /* ---------------------------- Renders ------------------------------*/

    render() {
        const { getFieldDecorator, getFieldProps } = this.props.form;
        return (
          <div className="form_sty">
            <div className="top-header"> </div>
            <div className="form_title">
              <h5>填写修改信息</h5>
            </div>
            <Form form={this.props.form} ref={el => this.form = el} onSubmit={this.handleUpdateSubmit.bind(this)}>
                <InputItem
                    {...getFieldProps('current_pwd', {
                        rules: [{
                            required: true, message: '请输入当前的安全密码',
                        }],
                    })}
                    type="password"
                    clear
                    placeholder="当前密码">
                    当前密码
                </InputItem>
                <InputItem
                    {...getFieldProps('password', {
                        rules: [{
                            required: true, message: '请输入新的安全密码'
                        }, {
                            min: 6, max: 20, message: '安全密码长度要求为6到20个字符之间。'
                        }],
                    })}
                    type="password"
                    clear
                    placeholder="新的安全密码">
                    新的密码
                </InputItem>
                <InputItem
                    {...getFieldProps('cmf_pwd', {
                        rules: [{
                            required: true, message: "请输入确认密码"
                        }, {
                            validator: this.checkPassword, message: "两个密码不一致。",
                            getFieldValue: this.props.form.getFieldValue
                        }],
                    })}
                    type="password"
                    clear
                    placeholder="确认密码">
                    确认密码
                </InputItem>
                <InputItem
                    {...getFieldProps('captcha', {
                        rules: [{ required: true, message: '请输入右侧验证码' }],
                    })}
                    maxLength="4"
                    clear
                    autoComplete="off"
                    placeholder="验证码"
                    extra={<img src={this.captchaUrl} ref="captcha_img" className="img-captcha" onClick={this.refreshCaptcha.bind(this)} />}
                >
                    验证码
                </InputItem>

              <div className="game-orders">
                <div className="ButtonBottom">
                  <button className="btn_left">
                    <Icon type="reload"/> 重置
                  </button>

                  <button className="btn_right" loading={this.state.loading} htmlType="submit" >
                    确定
                  </button>

                </div>
                <div className="clearfix"></div>
              </div>


            </Form>


          </div>


        );
    }
};

export default connect(state => ({
    hasSafePassword: state.user.hasSafePassword,
    loading: state.loading.models.user,
}))(Form.create()(LoginPassword));
