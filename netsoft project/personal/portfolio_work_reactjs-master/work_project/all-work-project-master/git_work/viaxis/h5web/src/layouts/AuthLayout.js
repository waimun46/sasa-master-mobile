import { connect } from 'dva';
import { PureComponent } from 'react';
import { NavBar, Icon as MIcon, Drawer, List } from 'antd-mobile';
import router from 'umi/router';
import NavMenu from './NavMenu';

class Layout extends PureComponent {

  componentWillMount(){
    window.onscroll = function() {scrollFunction()};
      function scrollFunction() {
        if(window.location.pathname == "/" || window.location.pathname == "/login"  || window.location.pathname == "/register")
            return;
          
        if (document.body.scrollTop > 10 || document.documentElement.scrollTop > 10) {
          document.getElementById("navbar").style.backgroundColor = "#2A2E43";
          document.getElementById("navbar").style.boxShadow = "0px 2px 20px rgba(0, 0, 0, 0.4)";
          document.getElementById("navbar").style.position = "fixed";
          document.getElementById("navbar").style.zIndex = "1";
          document.getElementById("navbar").style.height = "45px";
        } else {
          document.getElementById("navbar").style.backgroundColor = "transparent";
          document.getElementById("navbar").style.boxShadow = "none";
          document.getElementById("navbar").style.position = "initial";
          document.getElementById("navbar").style.zIndex = "0";

        }
      }
  }

  componentWillReceiveProps(newProps) {

    let children = this.props.children;
    if(children!= newProps.children)
    {
        if(this.state.open)
        {
            this.setState({open:false});
        }
    }

   }



    state = {
        open: false,
    }
    onOpenChange = (...args) => {
        this.setState({ open: !this.state.open });
    }
    render() {
        const { open } = this.state;
        const isHome = this.props.children.props.location.pathname == "/";
        return (
            <div className="main-wrap">
            {
                isHome ? this.props.children
                    : 
                    <NavMenu isOpen={this.state.open}
                    onOpenChange={this.onOpenChange}>
                     <div style={{position:'absolute', width:'100%'}} className="header">
                        <div id="navbar">
                          <div id="navbar-right" >
                          <div style={{width:'100%'}} class="danielph3">
                                <li style={{textAlign:'left'}} onClick={() => router.goBack()}><MIcon type="left" /></li>
                                <li style={{textAlign:'center', fontSize: '1.2em'}} >{this.props.title}</li>
                                <li style={{textAlign:'right'}} onClick={this.onOpenChange} ><MIcon key="more" type={open ? "cross" : "ellipsis"} /></li>
                                <div className="clearfix"></div>
                            </div>

                            {/* <li style={{position:'fixed', left:'10px', listStyle:'none',top:'10px'}} onClick={() => router.goBack()}><MIcon type="left" /></li>
                            <li style={{fontSize:'17px',position:'fixed',right:'221px', top:'10px' ,listStyle:'none'}} >{this.props.title}</li>
                            <li style={{position:'fixed', right:'16px', top:'10px', listStyle:'none'}} onClick={this.onOpenChange} ><MIcon key="more" type={open ? "cross" : "ellipsis"} /></li> */}
                          </div>
                        </div>
                      </div>
                    {this.props.children}
                </NavMenu>

            }

            </div >
        );
    }

}

export default connect(state => ({
    title: state.global.title,
}))(Layout);
