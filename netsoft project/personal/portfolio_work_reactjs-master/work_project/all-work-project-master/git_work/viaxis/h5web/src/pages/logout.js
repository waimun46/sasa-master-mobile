
import Redirect from 'umi/redirect';
import Auth from '@/utils/auth';

export default function ({ dispatch }) {
    Auth.logout();

    return <Redirect to="/login" />;
}
