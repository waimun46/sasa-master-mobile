import styles from './index.css';
import Search from '@/components/Search';
import { Flex, List, Picker, InputItem, WhiteSpace, DatePicker as MDatePicker, Calendar } from 'antd-mobile';
import { Form, Input, Select, DatePicker, TimePicker } from 'antd';
import DPicker from '@/components/DatePicker';
import DateRange from '@/components/DateRange';
import Homepage from './home'

function save(el) {

  console.log(el.getFieldProps);
}

const colorStyle = {
  display: 'inline-block',
  verticalAlign: 'middle',
  width: '16px',
  height: '16px',
  marginRight: '10px',
};

const colors = [
  {
    label:
      (<div>
        <span
          style={{ ...colorStyle, backgroundColor: '#FF0000' }}
        />
        <span>红色</span>
      </div>),
    value: '#FF0000',
  },
  {
    label:
      (<div>
        <span
          style={{ ...colorStyle, backgroundColor: '#00FF00' }}
        />
        <span>绿色</span>
      </div>),
    value: '#00FF00',
  },
  {
    label:
      (<div>
        <span
          style={{ ...colorStyle, backgroundColor: '#0000FF' }}
        />
        <span>蓝色</span>
      </div>),
    value: '#0000FF',
  },
];


export default function () {

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 4 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 8 },
    },
  };

  const config = {
    rules: [{ type: 'object', required: false, message: 'Please select time!' }],
  };
  const rangeConfig = {
    rules: [{ type: 'array', required: false, message: 'Please select time!' }],
  };

  return (
    <div className={styles.normal}>
    <Homepage/>
  
    </div>
  );
}
