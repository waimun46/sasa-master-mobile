import React, { Component } from 'react';
import { connect } from 'dva';
import Page from "@/layouts/PageBasic";
import Form from '@/components/Form';
import { List } from 'antd-mobile';

import * as BetHelper from "@/utils/bet_helper";
import TeamRecord from '@/components/teamRecord';
import * as RenderHelper from "@/utils/render_helper";
import DateRange from '@/components/DateRange';

import Link from 'umi/link';

const Item = List.Item;
const Brief = Item.Brief;


class Zrc extends Page {
  title = "查看下级"

  constructor(props) {
    super(props);

    const query = new URLSearchParams(this.props.location.search);
    this.qr = query.get('qr')

  }

  
  

  /* ---------------------------- System  ------------------------------*/
  componentWillMount() {
    this.queryGameData();
  }

  
  /* ---------------------------- Functions  ------------------------------*/

  queryGameData() {
    this.props.dispatch({ type: "gamedata/queryGameData",payload: { group_name: 'ZRC', querys: this.qr  }})
  }   

 /* ---------------------------- Events ------------------------------*/

  render() {
    const data = this.props.data || [];
    
    return (
      <div className="fund-history ">
        <div className="top-header"> </div>

        {/* date */}
        <List className="date-select">
          <Item><DateRange/></Item>
        </List>

        <div className="form_title">
          <h5>股东</h5>
        </div>
        <List className="my-list ">
        {data && data.map((element,i) => 
            <Item
              key={i}
              extra={
                <div className="bet-money add">
                  <Link onClick={this.handleChildClick} to={`/personal_information/inforpage/agency/content/ChaKanXiaJi/daiLi?id=`}>第{element.Qh}期 ></Link>
                </div>
              }
              align="top"
              multipleLine
            >
              <span className="title-warp">真人彩</span>
              <span className="title-warp">{element.Result}</span>
              <Brief style={{fontSize: '12px', color: '#cccccc'}}>{element.Result}</Brief>
            </Item>               
        )}  
        </List>
      </div>
    );
  }
};

function mapStateToProps(state) {
    return {
      data: state.gamedata.data.Data,
    }
  }
  
  export default connect(mapStateToProps)(Zrc);
