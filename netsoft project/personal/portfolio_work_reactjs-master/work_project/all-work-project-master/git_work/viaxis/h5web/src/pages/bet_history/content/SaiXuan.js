import React, {Component} from 'react';
import { connect } from 'dva';
import { Flex, List, Picker, Button, InputItem, WhiteSpace, DatePicker as MDatePicker, Calendar } from 'antd-mobile';
import { Form, Icon, Input, Select, DatePicker, TimePicker } from 'antd';
import DateRange from '@/components/DateRange';
import * as dateHelper from "../../../utils/common";

import GameSelect from './infor/GameSelect';
import { object } from 'prop-types';
import * as Util from '../../../utils/common';
import { util } from 'node-forge';

const Item = List.Item;

const colorStyle = {
    display: 'inline-block',
    verticalAlign: 'middle',
    width: '16px',
    height: '16px',
    marginRight: '17px'
};

const statuslselect = [
    {
        label: (
            <div>

              <span>全部</span>
            </div>
        ),
        value: 8
    },
    {
        label: (
            <div>

              <span>中奖</span>
            </div>
        ),
        value: 1
    },
     {
        label: (
            <div>

                <span>未中奖</span>
            </div>
        ),
        value: 2
    }, {
        label: (
            <div>

                <span>未开奖</span>
            </div>
        ),
        value: 0
    }, {
        label: (
            <div>

                <span>取消</span>
            </div>
        ),
        value: 3
    }
  ];
const gamesSelect = [
    {
        label: (
            <div>
             
              <span>全部</span>
            </div>
        ),
        value: 0
    },
    {
        label:
        (<div>
          {/* <img src={icon4} style={{ ...colorStyle2}}/> */}
          <span>福彩3D</span>
        </div>),
        value: 1,
      },
      {
        label:
        (<div>
          {/* <img src={icon4} style={{ ...colorStyle2}}/> */}
          <span>重庆时时彩</span>
        </div>),
        value: 2,
      },
      {
        label:
        (<div>
          {/* <img src={icon4} style={{ ...colorStyle2}}/> */}
          <span>北京快乐8分</span>
        </div>),
        value: 3,
      },
      {
        label:
        (<div>
          {/* <img src={icon4} style={{ ...colorStyle2}}/> */}
          <span>重庆幸运农场</span>
        </div>),
        value: 4,
      },
      {
        label:
        (<div>
          {/* <img src={icon4} style={{ ...colorStyle2}}/> */}
          <span>KENO</span>
        </div>),
        value: 5,
      },
      {
        label:
        (<div>
          {/* <img src={icon4} style={{ ...colorStyle2}}/> */}
          <span>排列3</span>
        </div>),
        value: 6,
      },
      {
        label:
        (<div>
          {/* <img src={icon4} style={{ ...colorStyle2}}/> */}
          <span>分分彩</span>
        </div>),
        value: 7,
      },
      {
        label:
        (<div>
          {/* <img src={icon4} style={{ ...colorStyle2}}/> */}
          <span>北京PK10</span>
        </div>),
        value: 9,
      },
  ];
class SaiXuan extends Component{


    constructor(props){
        super(props);
        this.state = {
            loading: false,
            statusSelect: 0,
            gamesSelect: 0,
            queryDate:[],
            query:{}
        }
    }

    componentWillMount(){
        let query = this.props.query;

        const {WinlossStatus,GroupId,CreateTime} = this.props.query;
        if(WinlossStatus!=undefined)
        {
            this.setState({statusSelect:{0:WinlossStatus}});

        }
        else
        {
            this.setState({statusSelect:{0:8}});
        }
        if(GroupId != undefined)
        {
            this.setState({gamesSelect:{0:GroupId}})
        }
        else
        {
            this.setState({gamesSelect:{0:0}});
        }
        if(CreateTime !=undefined)
        {
            let startTime = CreateTime[0];
            var bits = startTime.split(/\D/);
            var sDate = new Date(bits[2],this.adjustMonthForDate(bits[0]),bits[1]);

            let endTime = CreateTime[1];
            bits = endTime.split(/\D/);
            var eDate = new Date(bits[2],this.adjustMonthForDate(bits[0]),bits[1]);
            this.setState({queryDate:{0:sDate,1:eDate}});

        }
        else
        {
            var d = new Date();
            var c = new Date("2018","00","01");

            this.setState({queryDate:{0:c,1:d}});

        }



    }

    adjustMonthForDate(monthString){
        return (parseInt(monthString) - 1).toString();
    }

    componentDidMount() {
        //if (/bet_history/.test(window.location.href)) {
            //let element = document.body;
          ///  element.classList.add("saixuan-pop");
        //}
    }

    componentWillUpdate(nextProps, nextState) {
        let query = nextProps.query;

    }
    componentWillReceiveProps(){
        let query = this.props.query;

    }

    handleSubmit = (e)=>{
        console.log(e);
        //Object.assign(this.state.query,{});
        if(this.state.statusSelect[0] != 8)
         Object.assign(this.state.query, {WinlossStatus:this.state.statusSelect[0]});
        else
        Object.assign(this.state.query, {WinlossStatus:null});

        if(this.state.gamesSelect[0] != 0)
         Object.assign(this.state.query, {GroupId:this.state.gamesSelect[0]});
         else
         Object.assign(this.state.query, {GroupId:null});
        
            let date = [];
                date.push(this.state.queryDate[0].toLocaleString());
                date.push(this.state.queryDate[1].toLocaleString());
                Object.assign(this.state.query, {CreateTime:date});
               
        
        this.props.close && this.props.close(this.state.query);
        

    }

    onChangeStatusSelect = (value) =>{
        //this.setState({statusSelect: value[0]});

        this.setState({statusSelect: value});
    }
    onChangeGameSelect = (gamesSelect) => {
        this.setState({
           // gamesSelect: gamesSelect[0],
           gamesSelect: gamesSelect
        });
    };

    onDateRangeChange = (value) => {
        console.log(value);
        //let fmtValue = dateHelper.dateRangeFormat(value);
        let date = [];
        date.push(value[0].toLocaleString());
        date.push(value[1].toLocaleString());
        this.setState({queryDate:date});
    }

    onReset = () =>{
        this.setState({statusSelect:{0:8}});
        this.setState({gamesSelect:{0:0}});
        var d = new Date();
            var c = new Date("2018","00","01");

            this.setState({queryDate:{0:c,1:d}});


           
    }

    render(){
        return(
            <div className="saixuan-warp">
              <h2>筛选选项</h2>
                <Form form={this.props.form} ref={el => this.form = el} onSubmit={this.handleSubmit.bind(this)}>
                  <div className="warp-form">
                    <List renderHeader={() => '选择日期'} className="my-list date-select">
                        <Item><DateRange value={{startTime:this.state.queryDate[0],endTime:this.state.queryDate[1]}} onChange = {this.onDateRangeChange.bind(this)}/></Item>
                    </List>

                     <List renderHeader={() => '状态类型'} className="my-list status-select">
                        <Picker
                            data={statuslselect}
                            value={this.state.statusSelect}
                            cols={1}
                            onChange={this.onChangeStatusSelect}>
                            <List.Item arrow="horizontal">状态选项</List.Item>
                        </Picker>
                    </List>

                    <List renderHeader={() => '玩法选项'} className="picker-list my-list status-select">
                      <Picker
                        data={gamesSelect}
                        value={this.state.gamesSelect}
                        cols={1}
                        onChange={this.onChangeGameSelect}>
                        <List.Item arrow="horizontal">选择游戏</List.Item>
                      </Picker>
                    </List>
                    
                  </div>


                    {/* <GameSelect/> */}

                    {/* <a>
                    <div className="btn_right">
                        确定
                    </div>
                    </a> */}
                  <div className="form_btn">
                    <Button type="warning" inline  onClick={this.onReset.bind(this)}> 重置</Button>
                    <Button type="primary" inline  onClick={this.handleSubmit.bind(this)}> 确定</Button>
                  </div>
                </Form>
                
            </div>
        )

    }
}

export default (SaiXuan);
