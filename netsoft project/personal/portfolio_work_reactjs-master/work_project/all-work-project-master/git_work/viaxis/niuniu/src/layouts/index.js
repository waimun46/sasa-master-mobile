import { connect } from 'dva';
import React from 'react';
import langs from '@/lang';
import styles from './index.css';
import Header from './components/header';
import ReactOrientation from 'react-orientation';

function BasicLayout(props) {
    if (props.location.query.referral) { sessionStorage.referral = props.location.query.referral; }
    return (
        <React.Fragment>
            <ReactOrientation className={styles.orientation} type="landscape">
                <div className={styles.mob_cen}>
                    <i></i>
                </div>
                <p>{langs.orientationTips}</p>
            </ReactOrientation>
            <div className={styles.normal}>
                <div className="gameintro loadingpage">
                    <Header />
                    <main>
                        {props.children}
                    </main>
                </div>
            </div>
        </React.Fragment>
    );
}

export default connect(state => ({
    lang: state.global.lang,
}))(BasicLayout);
