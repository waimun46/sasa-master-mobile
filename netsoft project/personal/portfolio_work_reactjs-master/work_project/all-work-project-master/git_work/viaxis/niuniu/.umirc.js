
// ref: https://umijs.org/config/
export default {
    plugins: [
        // ref: https://umijs.org/plugin/umi-plugin-react.html
        ['umi-plugin-react', {
            antd: true,
            dva: true,
            dynamicImport: true,
            title: 'niuniu_front',
            dll: false,
            hardSource: true,
            routes: {
                exclude: [
                    /components/,
                ],
            },
        }],
    ],
    theme: {
        'modal-header-bg': '#1B1B56',
        'component-background': '#1B1B56',
        'heading-color': 'rgba(255, 255, 255, 0.85)',
        'text-color-secondary': 'rgba(255, 255, 255, 0.45)',
        'text-color': '#fff',
        'input-color': '#a2a3a4',
        'input-addon-bg': '#fea45b'
    },
    define: {
        // restful domain
        "configs.api": "",
        // websocket url
        "configs.ws": "",
        "configs.eos": {
            "endpoint": "https://mainnet.eoscannon.io",
            "chainId": "aca376f206b8fc25a6ed44dbdc66547c36c6c33e3a119ffbeaef943642f0e906"
        },
        "configs.contract": {
            "code": "eosniuniu"
        }
    },
    proxy: {
        "/api": {
            "target": "http://localhost:5000",
            "changeOrigin": true,
        }
    },

}
