import React, { Component } from 'react';
import eos from '../../../../assets/images/icon/Icon_EOS.png';
import noniu from '../../../../assets/images/icon/MeiNiu.png';
class Card extends Component {

  render() {
    return (
      <div className="card">
        <ul>
        <div className="diamond"><img src={eos} alt="eos" /></div>
          <li>
            <div className='wrapper'>
              <div className='front'></div>
              <div className='back'></div>
            </div>
          </li>
          <li>
            <div className='wrapper'>
              <div className='front'></div>
              <div className='back'></div>
            </div>
          </li>
          <li>
            <div className='wrapper'>
              <div className='front'></div>
              <div className='back'></div>
            </div>
          </li>
          <li>
            <div className='wrapper'>
              <div className='front'></div>
              <div className='back'></div>
            </div>
          </li>
          <li>
            <div className='wrapper'>
              <div className='front'></div>
              <div className='back'></div>
            </div>
          </li>
          <div className="clearfix"></div>
            <div className="niutop-icon-show">
              <img src={noniu} alt="noniu"/>
            </div>
        </ul>
      </div>
    );
  }
};

export default Card;
