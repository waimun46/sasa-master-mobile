import React, { Component } from 'react';
import {Icon, Tabs, Popconfirm, message, Button, Table, DatePicker, Input  } from 'antd';
const { RangePicker } = DatePicker;

function onChange(value, dateString) {
  console.log('Selected Time: ', value);
  console.log('Formatted Selected Time: ', dateString);
}

function onOk(value) {
  console.log('onOk: ', value);
}

const TabPane = Tabs.TabPane;

const formContent =  <form className="search_popout">
                      <div className="form-input">
                        <label>日期</label>
                        <RangePicker
                          showTime={{ format: 'HH:mm' }}
                          format="YYYY-MM-DD HH:mm"
                          placeholder={['Start Time', 'End Time']}
                          onChange={onChange}
                          onOk={onOk}
                         />
                        <div className="clearfix"></div>
                      </div>
                      <div className="form-input">
                        <label>账户</label>
                         <Input className="input-popover"/>
                        <div className="clearfix"></div>
                      </div>
                      <div className="form-input">
                        <label>推介奖金</label>
                         <Input className="input-popover"/>
                        <div className="clearfix"></div>
                      </div>
                      <div className="Submit-btn">
                        <Button type="primary">确定</Button>
                      </div>
                    </form>
            ;



const columns = [{
  title: '日期',
  dataIndex: 'date',
}, {
  title: '时间',
  dataIndex: 'time',
}, {
  title: '账户',
  dataIndex: 'account',
},{
  title: '下注金额',
  dataIndex: 'bet_amount',
},{
  title: '赢取奖金',
  dataIndex: 'win',
}
];
const data = [{
  key: '1',
  date: '03-01-2019',
  time: '04:55:18',
  account: 'dd555566',
  bet_amount: '2.5411 EOS',
  win: '0.1010'
}, {
  key: '2',
  key: '1',
  date: '03-01-2019',
  time: '04:55:18',
  account: 'dd555566',
  bet_amount: '2.5411 EOS',
  win: '0.1010'
},
];

class BetRecordList extends Component {

  render() {
    return (
      <div className="record-list">
        <Popconfirm  placement="bottomRight" title={formContent}>
          <div className="search-pop">
            <Button>搜索 <Icon type="caret-down" /></Button>
          </div>
        </Popconfirm>
        <Tabs>
          <TabPane tab="赌注记录" key="1">
            <div className="allbet">
              <Table columns={columns} dataSource={data}/>
            </div>
          </TabPane>
          <TabPane tab="我的赌注记录" key="2">
            <div className="allbet">
              <Table columns={columns} dataSource={data}/>
            </div>
          </TabPane>
        </Tabs>
      </div>
    );
  }
};

export default BetRecordList;
