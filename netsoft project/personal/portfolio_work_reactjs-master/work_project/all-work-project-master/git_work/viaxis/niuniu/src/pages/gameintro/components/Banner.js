import React, { Component } from 'react';
import lang from '@/lang';
import styles from '../index.less';

class BannerIntro extends Component {

  render() {
    return (
        <section className={styles.banner_intro}>
          <div className={styles.banner}>
            <div className={styles.banner_img}></div>
          </div>
          <div className={styles.content_intro}>
            <p>{lang.banner_title}</p>
          </div>
          <div className="clearfix"></div>
        </section>
    );
  }
};

export default BannerIntro;
