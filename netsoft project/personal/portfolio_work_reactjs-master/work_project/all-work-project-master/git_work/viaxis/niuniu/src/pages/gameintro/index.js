import React, { Component } from 'react';
import { BackTop } from 'antd';
import BannerIntro from './components/Banner';
import GameGuide from './components/GameGuide';
import PayoutOdds from './components/PayoutOdds';
import CardType from './components/CardType';
import Attention from './components/Attention';
import SlideOutPanel from '../bet/components/SlideOutPanel';
import styles from './index.less';

class GameIntro extends Component {
  componentWillMount() {
       if (/gameintro/.test(window.location.href)) {
           let element = document.getElementsByClassName('gameintro')[0];
           element.classList.add("gameintro_width");
       }

  }

  render() {
    return (
      <div className={styles.GameIntro}>
        <BannerIntro/>
        <GameGuide/>
        <PayoutOdds/>
        <CardType/>
        <Attention/>
        <SlideOutPanel />
        <BackTop />
      </div>
    );
  }
};

export default GameIntro;
