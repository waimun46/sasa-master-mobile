import { connect } from 'dva';
import React, { PureComponent } from 'react';
import Card from '../Card';
import * as Utils from '@/utils/common';
import { k_combinations } from '@/utils/math';
import styles from './index.less';
import stylesbet from '../betzone/index.less';

let delay = (time) => (result) => new Promise(resolve => setTimeout(() => resolve(result), time));

const sum = (set) => set.reduce((a, b) => a + b, 0);

class Hand extends PureComponent {

    /**
     * executed after first render only on the client side
     * @return {[type]} [description]
     */
    componentDidMount() {
        this.open();
    }

    /**
     * called just after rendering.
     * @param  {[type]} prevProps [description]
     * @param  {[type]} prevState [description]
     * @return {[type]}           [description]
     */
    componentDidUpdate(prevProps, prevState) {
        const { value } = this.props;
        if (prevProps.value != this.props.value) {
            this.open();
        }
    }



    open() {
        const { value } = this.props;
        const values = Utils.isNonEmptyString(value) && value.split(',') || null;
        if (values == null || values.length != 5) return;

        const self = this;
        Promise.resolve()
            .then(delay(50))
            .then(() => self.refs.card_0.open())
            .then(delay(50))
            .then(() => self.refs.card_1.open())
            .then(delay(50))
            .then(() => self.refs.card_2.open())
            .then(delay(50))
            .then(() => self.refs.card_3.open())
            .then(delay(50))
            .then(() => self.refs.card_4.open());
    }

    calcPoint(cards) {
        const values = this.getPokerValue(cards);
        const cards_sum = sum(values);
        const combinations = k_combinations(values, 3);
        const taurus = [];
        combinations.forEach(el => {
            let el_sum = sum(el);
            if (el_sum % 10 == 0) {
                var tauru = (cards_sum - el_sum) % 10;
                if (tauru == 0) tauru = 10;
                taurus.push(tauru);
            }
        });
        if (taurus == null || taurus.length == 0)
            return 0;

        return Math.max(...taurus);
    }

    getPokerValue(values) {
        const rtn = [];
        values.forEach(element => {
            let val = element.slice(1).toUpperCase();
            switch (val) {
                case 'A': val = 1; break;
                case 'J':
                case 'Q':
                case 'K': val = 10; break;
                default: val = parseInt(val); break;
            }
            rtn.push(val);
        });
        return rtn;
    }


    render() {
        const { value, lang } = this.props;
        const values = Utils.isNonEmptyString(value) && value.split(',') || null;
        const point = (values != null && values.length == 5) ? this.calcPoint(values) : null;

        return (
            <div className="hand-inner">
                <div className={styles.hand}>
                    <Card ref="card_0" value={values && values[0]} />
                    <Card ref="card_1" value={values && values[1]} />
                    <Card ref="card_2" value={values && values[2]} />
                    <Card ref="card_3" value={values && values[3]} />
                    <Card ref="card_4" value={values && values[4]} />
                </div>

                {point != null &&
                    <span ref="result" className={`${lang}_niu${point}`}></span>
                }

            </div>
        );
    }
};

export default connect(state => ({
    lang: state.global.lang,
}))(Hand);
