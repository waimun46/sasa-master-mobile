import React, { Component } from 'react';

import GameTable1 from './infor/GameTable1';
import GameTable2 from './infor/GameTable2';
import GameTable3 from './infor/GameTable3';
import GameTable4 from './infor/GameTable4';


class MiddleInfor extends Component {

  render() {
    return (
      <section className="MiddleInfor">
        <div className="warpper-middle">
          <GameTable1/>
          <GameTable2/>
          <GameTable3/>
          <GameTable4/>
          <div className="clearfix"></div>
        </div>
      </section>
    );
  }
};

export default MiddleInfor;
