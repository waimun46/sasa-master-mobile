import React, { Component } from 'react';
import router from 'umi/router';
import Link from 'umi/link';
import Wallet from '@/services/wallet';
import lang from '@/lang';
import FontIcon from '@/components/NiuIcon';
import { Menu, Icon, Select, Button } from 'antd';
import { NoticeBar, WhiteSpace } from 'antd-mobile';
import Trend from './Trend';
import RankingList from './RankingList';
import Menuphone from './MenuPhone';
import stylespanel from "../index.less";
import styles from '@/layouts/components/header/index.less'

const Option = Select.Option;

function handleChange(value) {
    console.log(value);
}


class SlideOutPanel extends Component {

    render() {

        return (
            <div id="SlideOutPanel" className={stylespanel.SlideOutPanel}>
                <div className="back-btn" onClick={() => router.goBack()}>
                    <Icon type="left" />
                </div>
                <Link to="/gameintro">
                    <div className="game-role-btn">
                        <Icon type="book" />
                    </div>
                </Link>
                <Menuphone/>
                <RankingList />
                <Trend />
            </div>
        );
    }


};


export default SlideOutPanel;
