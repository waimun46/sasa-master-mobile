import { connect } from 'dva';
import React, { PureComponent } from 'react';
import { Icon } from 'antd';
import classnames from 'classnames';
import lang from '@/lang';
import Ncon from '@/components/NiuIcon';
import styles from '../index.less';

class RankingList extends PureComponent {
    state = { slideClass: false }

    slide = (e) => {
        const { slideClass } = this.state;
        this.setState({ slideClass: !slideClass });

        if (!slideClass)
            this.props.dispatch({ type: "game/getRanking" });
    }

    render() {
        const { ranking, loading } = this.props;
        const { slideClass } = this.state;

        return (
            <div className={classnames(styles.rankinglist, slideClass && "showSlideOut" || "")}>
                <h5 onClick={this.slide} >
                    {slideClass ? <Icon type="right" /> : lang.rankingList}
                </h5>
                <div className={styles.slide_popup}>
                    <h3>{lang.rankingList}</h3>
                    <div className="content">
                        <label>
                            <span>{lang.rank}</span>
                            <span>{lang.account}</span>
                            <span>{lang.totalEOS}</span>
                        </label>
                        <ul>
                            {ranking.map((it, i) => <li key={i}>
                                <span>{this.renderIndex(i)} </span>
                                <span>{it.account}</span>
                                <span>{it.amount.toFixed(4)} EOS</span>
                            </li>)}
                        </ul>
                    </div>
                </div>
            </div>
        );
    }

    renderIndex(idx) {
        const medals = { 0: "icon-Rank_Gold", 1: "icon-Rank_Silver", 2: "icon-Rank_Bronze" };
        if (idx >= 0 && idx < 3)
            return <Ncon type={medals[idx]} />
        else
            return idx + 1;
    }
};

export default connect(state => ({
    lang: state.global.lang,
    ranking: state.game.ranking,
    loading: state.loading.effects["game/getRanking"]
}))(RankingList);
