import { connect } from 'dva';
import React, { PureComponent } from 'react';
import { Icon, Tabs, Popconfirm, message, Button, Table, DatePicker, Input } from 'antd';
import lang from '@/lang';
import SlideOutPanel from './components/SlideOutPanel';
import styles from './index.less';

const { RangePicker } = DatePicker;
const TabPane = Tabs.TabPane;

function onChange(value, dateString) {
    console.log('Selected Time: ', value);
    console.log('Formatted Selected Time: ', dateString);
}

function onOk(value) {
    console.log('onOk: ', value);
}

const formContent = <form className={styles.search_popout}>
                      <div className={styles.form_input}>
                          <label>{lang.date}</label>
                          <RangePicker
                              showTime={{ format: 'HH:mm' }}
                              format="YYYY-MM-DD HH:mm"
                              placeholder={['Start Time', 'End Time']}
                              onChange={onChange}
                              onOk={onOk}
                          />
                          <div className="clearfix"></div>
                      </div>
                      <div className={styles.form_input}>
                          <label>{lang.account}</label>
                          <Input className={styles.input_popover} />
                          <div className="clearfix"></div>
                      </div>
                      <div className={styles.form_input}>
                          <label>{lang.bonus}</label>
                          <Input className={styles.input_popover}/>
                          <div className="clearfix"></div>
                      </div>
                      <div className={styles.Submit_btn}>
                          <Button type="primary">{lang.oK}</Button>
                      </div>
                    </form>

class History extends PureComponent {

    /**
     * executed before rendering
     * @return {[type]} [description]
     */
    componentWillMount() {
        this.loadAllBets();
    }

    onTabChange = (key) => {
        if (key == 1)
            this.loadAllBets();
        else
            this.loadMyBets();

    }

    loadAllBets(querys) {
        this.props.dispatch({ type: "game/getAllBets", payload: querys })
    }

    loadMyBets(querys) {
        this.props.dispatch({ type: "game/getMyBets", payload: querys })
    }

    render() {
        return (
            <section id="niuniu" className={styles.bet_record}>
                <div className={styles.record_list}>
                    <Tabs onChange={this.onTabChange}>
                        {this.renderAll()}
                        {this.renderMine()}
                    </Tabs>
                </div>
                <SlideOutPanel />
            </section>
        );
    }

    renderAll() {
        const { history, loadingAll: loading } = this.props;
        const data = history.all && history.all.Data || [];

        const pagination = {
            total: history.all && history.all.TotalCount || 0,
            onChange: function (current) {
                this.loadAllBets({ pageIndex: current });
            }.bind(this),
        };

        return (
            <TabPane tab={lang.allBets} key="1">
                <Popconfirm placement="bottomRight" title={formContent}>
                    <div className={styles.search_pop}>
                        <Button>{lang.search} <Icon type="caret-down" /></Button>
                    </div>
                </Popconfirm>
                <div className={styles.allbet}>
                    <Table loading={loading} columns={this.columns} dataSource={data} pagination={pagination} />
                </div>
            </TabPane>
        );
    }
    renderMine() {
        const { history, loadingMine: loading } = this.props;
        const data = history.mine && history.mine.Data || [];
        const pagination = {
            total: history.all && history.all.TotalCount || 0,
            onChange: function (current) {
                this.loadMyBets({ pageIndex: current });
            }.bind(this),
        };

        return (
            <TabPane tab={lang.myBets} key="2">
                <Popconfirm placement="bottomRight" title={formContent}>
                    <div className={styles.search_pop}>
                        <Button>搜索 <Icon type="caret-down" /></Button>
                    </div>
                </Popconfirm>
                <div className={styles.allbet}>
                    <Table loading={loading} columns={this.columns} dataSource={data} pagination={pagination} />
                </div>
            </TabPane>
        );
    }

    get columns() {
        const columns = [{
            title: lang.time,
            dataIndex: 'bet_time',
        }, {
            title: lang.account,
            dataIndex: 'bettor',
        }, {
            title: lang.betAmount,
            dataIndex: 'bet_amt',
        }, {
            title: lang.payout,
            dataIndex: 'payout',
        }
        ];
        return columns;
    }
}

export default connect(state => ({
    lang: state.global.lang,
    history: state.game.betHistory,
    loadingAll: state.loading.effects["game/getAllBets"],
    loadingMine: state.loading.effects["game/getMyBets"]
}))(History);
