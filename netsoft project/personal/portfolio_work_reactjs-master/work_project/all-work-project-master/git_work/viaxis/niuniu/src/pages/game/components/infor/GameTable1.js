import React, { Component } from 'react';
import { Icon, Modal, Button } from 'antd';
import diamond from '../../../../assets/images/icon/Icon_EOS_Outline.png';
import token from '../../../../assets/images/icon/Icon_Token.png';
import card01 from '../../../../assets/images/icon/Hearts_2.png';
import card02 from '../../../../assets/images/icon/Hearts_3.png';
import card03 from '../../../../assets/images/icon/Hearts_4.png';
import card04 from '../../../../assets/images/icon/Hearts_5.png';
import card05 from '../../../../assets/images/icon/Hearts_6.png';
import niuniu from '../../../../assets/images/icon/NiuNiu.png';
import victory from '../../../../assets/images/svg/background/win.svg';
import lose from '../../../../assets/images/svg/background/lose.svg';

class GameTable1 extends Component {

  state = {
    visiblewin: false,
    visiblelose: false
  }

  showModalwin  = () => {
    this.setState({
      visiblewin: true,
    });
  }

  hideModalwin = () => {
    this.setState({
      visiblewin: false,
    });
  }

  showModallose  = () => {
    this.setState({
      visiblelose: true,
    });
  }

  hideModallose = () => {
    this.setState({
      visiblelose: false,
    });
  }

  render() {
    return (

          <div className="warp">
            <div className="warp-middle" onClick={this.showModalwin}>
            {/* banker  */}
              <div className="bet-banker">
                <p>100</p>
              </div>
            {/* coin  */}
              <div className="coin">
                <div className="coin-warp" style={{paddingRight: "100px"}}>
                  <img src={token} alt="token"/>
                </div>
                <div className="coin-warp">
                  <img src={token} alt="token"/>
                </div>
                <div className="coin-warp" style={{paddingLeft: "60px"}}>
                  <img src={token} alt="token"/>
                </div>
              </div>
            {/* bet money  */}
              <div className="money-bet">
                <div className="money-warp">
                  <div className="top">
                    <p>1472.4000</p>
                    <div className="img-warp">
                      <img src={diamond} alt="diamond"/>
                    </div>
                    <div className="clearfix"></div>
                  </div>
                  <div className="bottom">
                    <div className="tick">
                      <Icon type="check" />
                    </div>
                    <div className="close">
                      <Icon type="close" />
                    </div>
                  </div>
                </div>
              </div>
              {/* player  */}
              <div className="bet-player">
                <p>100</p>
              </div>
            </div>

            {/* card  */}
            <div className="card-table">
              <ul>
                <li><img src={card01} alt="card01"/></li>
                <li><img src={card02} alt="card02"/></li>
                <li><img src={card03} alt="card03"/></li>
                <li><img src={card04} alt="card04"/></li>
                <li><img src={card05} alt="card05"/></li>
                <div className="clearfix"></div>
                <div className="niu-icon-show">
                  <img src={niuniu} alt="niuniu"/>
                </div>
              </ul>

            </div>

            {/*  popup win  */}
              <Modal
                title="Modal"
                className="win-popup"
                visible={this.state.visiblewin}
                onCancel={this.hideModalwin}
               >
                <div className="vic-img">
                  <img src={victory} alt="victory"/>
                  <p className="logo-title">大吉大利，今晚吃鸡</p>
                  <div className="detial-win">
                    <p>+ 10 EOS</p>
                     <Button type="primary" onClick={this.hideModalwin}>确定</Button>
                  </div>
                </div>
               </Modal>

              {/*  popup lose  */}
               <Modal
                 title="Modal"
                 className="lose-popup"
                 visible={this.state.visiblelose}
                 onCancel={this.hideModallose}
                >
                 <div className="vic-img">
                   <img src={lose} alt="lose"/>
                   <p className="logo-title">再接再厉，再来一局</p>
                   <div className="detial-win">
                     <p>- 10 EOS</p>
                      <Button type="primary" onClick={this.hideModallose}>确定</Button>
                   </div>
                 </div>
                </Modal>
          </div>

    );
  }
};

export default GameTable1;
