import React, { Component } from 'react';
import { Select, Icon, Modal, Button, Input} from 'antd';
import diamond from '../../../assets/images/icon/EOS_yellowIcon-01.png';
import token from '../../../assets/images/icon/Icon_Token.png';
import diamond2 from '../../../assets/images/icon/Icon_EOS.png';

const Option = Select.Option;

function handleChange(value) {
  console.log(value);
}

class BottomInfor extends Component {
  state = {
    visible: false,
  }

    showModal = () => {
      this.setState({
        visible: true,
      });
    }
    handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  }

  render() {
    return (
      <section className="BottomInfor">
        <div className="betbtn-warp">

          <div className="warp">
            <div className="select-token">

              <Select className="opt2" labelInValue defaultValue={{ key: '1' }} onChange={handleChange}>
                <Option value="1" >
                  <img src={diamond} alt="diamond"/>
                  <span className="select-sty">2.000</span>
                  百万
                  <span className="select-sty">EOS</span>
                </Option>
                <Option value="2"><img src={token} alt="token"/>Token</Option>
              </Select>
            </div>
            <div className="add-token">
              <div className="token-warp" onClick={this.showModal}>
                <Icon type="plus" />
              </div>
            </div>
            <div className="clearfix"></div>
          </div>

          <div className="warp">
            <ul className="bet-nember">
              <li><span>10 EOS</span></li>
              <li><span>50 EOS</span></li>
              <li><span>100 EOS</span></li>
              <li><span>1000 EOS</span></li>
              <div className="clearfix"></div>
            </ul>
          </div>

          <div className="warp">
            <div className="bet-history">
              <span>投注记录</span>
            </div>
          </div>
          <div className="clearfix"></div>
        </div>

          <Modal
            title="充值提现"
            visible={this.state.visible}
            className="withdraw-role"
            onCancel={this.handleCancel}
          >
            <p>
              Lorem Ipsum is simply dummy text of the printing and typesetting industry.
              Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
              when an unknown printer took a galley of type and scrambled it to make a type
              specimen book.
             </p>

            <div className="balance">
              <label>Balance</label>
              <Input addonAfter={<img src={diamond2}/>} defaultValue="1472.4000" />
              <div className="clearfix"></div>
            </div>
            <div className="amount">
              <label>Amount</label>
              <Input addonAfter={<img src={diamond2}/>} defaultValue="1472.4000" />
              <div className="clearfix"></div>
            </div>

            <div className="btn-withdraw">
              <Button type="primary">提款</Button>
              <Button type="primary">存款</Button>
            </div>

          </Modal>

      </section>
    );
  }
};

export default BottomInfor;
