import React from 'react';
import ReactDOM from 'react-dom';
import { Badge, Button, DatePicker, Modal, Icon } from 'antd';

export function dialog(title, body, options, parentNode) {

    let div = document.createElement('div');
    if (!parentNode)
        // parentNode = document.getElementById("root");
        parentNode = document.body;
    parentNode.appendChild(div);

    function close() {
        const unmountResult = ReactDOM.unmountComponentAtNode(div);
        if (unmountResult && div.parentNode) {
            div.parentNode.removeChild(div);
        }
    }

    var cloneBody = React.cloneElement(body, { destroy: close });

    ReactDOM.render(
        <Modal title={title} {...options} footer='' visible onCancel={close}>
            {cloneBody}
        </Modal>
        , div);

    return {
        destroy: close,
    };

}
