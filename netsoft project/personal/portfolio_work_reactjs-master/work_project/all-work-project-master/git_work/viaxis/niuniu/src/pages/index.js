import styles from './index.css';
import Homepage from './home';

export default function() {
  return (
    <div className={styles.normal}>
      <Homepage/>
    </div>
  );
}
