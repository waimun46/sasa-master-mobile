import Countdown from './CountDown';
import { Button } from "antd";

export default class CountDownButton extends Countdown {
    constructor(props) {
        super(props);
    }

    format = (time) => {
        const s = Math.floor(time / 1000);
        return (
            <span style={{ fontSize: 11 }}>({fixedZero(s)})</span>
        );
    }

    render() {
        const { timeDiff, ...rest } = this.props;
        let { lastTime } = this.state;
        const result = this.format(lastTime);
        const disabled = this.props.disabled && lastTime > 0;

        return (
            <Button {...rest} disabled={disabled}>
                {this.props.children}
                {lastTime > 0 && result}
            </Button>
        );
    }
}

function fixedZero(val) {
    return val * 1 < 10 ? `${val}` : val;
}
