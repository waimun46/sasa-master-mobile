import React from 'react';
import ReactDOM from 'react-dom';
import { Modal } from 'antd';
import Button from '@/components/CountDownButton';
import lang from '@/lang';
import styles from './index.less';
import Sound from '@/utils/sound';
import coinSound from '@/assets/sounds/effect/getCoin.mp3';


export function Win(props) {
    const seconds = props.seconds || 2;
    Sound.play(coinSound);
    return (
        <Modal
            title={null} footer={null}
            className={styles.popup_win}
            {...props}
        >
            <div className="vic-img">
                <span className="slogan" />
                <p className="logo-title">{lang.winSlogan}</p>
                <div className="detial-win">
                    <p>+ {props.payout} EOS</p>
                    <Button type="primary" onEnd={props.onCancel} onClick={props.onCancel} target={new Date().getTime() + seconds * 1000}>{lang.oK}</Button>
                </div>
            </div>
        </Modal>
    );
}

export function Loss(props) {
    const seconds = props.seconds || 2;
    return (
        <Modal
            title={null} footer={null}
            className={styles.popup_loss}
            {...props}
        >
            <div className="vic-img">
                <span className="slogan" />
                <p className="logo-title">{lang.loseSlogan}</p>
                <div className="detial-loss">
                    <Button type="primary" onEnd={props.onCancel} onClick={props.onCancel} target={new Date().getTime() + seconds * 1000}>{lang.oK}</Button>
                </div>
            </div>
        </Modal>
    );
}

export default function (isWin, props) {
    console.log('props:', props);
    let div = document.createElement('div');
    const parentNode = document.body;
    parentNode.appendChild(div);

    function close() {
        const unmountResult = ReactDOM.unmountComponentAtNode(div);
        if (unmountResult && div.parentNode) {
            div.parentNode.removeChild(div);
        }
    }

    var modal = isWin ? Win : Loss;
    ReactDOM.render(modal({ ...props, visible: true, onCancel: close }), div);

    return {
        destroy: close,
    };
}