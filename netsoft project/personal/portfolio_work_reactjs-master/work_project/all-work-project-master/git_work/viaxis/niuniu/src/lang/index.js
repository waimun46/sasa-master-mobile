import LocalizedStrings from 'react-localization';
import zh from './zh';
import en from './en';

export default new LocalizedStrings({
    en, zh
});