import React, { Component } from 'react';
import AttentionListOne from './AttentionListOne';
import AttentionListTwo from './AttentionListTwo';
import lang from '@/lang';
import styles from '../index.less';


class Attention extends Component {

  render() {
    return (
      <section  className={styles.Attention}>
        <div className={styles.content}>
          <header>
              <h1>{lang.attention}</h1>
              <p>{lang.attention_subtitle}</p>
          </header>
          <AttentionListOne/>
          <AttentionListTwo/>
          <div className={styles.notice}>
            <ol type="1">
              <li>{lang.notice_1}</li>
              <li>{lang.notice_2}</li>
            </ol>
          </div>
        </div>
      </section>
    );
  }
};

export default Attention;
