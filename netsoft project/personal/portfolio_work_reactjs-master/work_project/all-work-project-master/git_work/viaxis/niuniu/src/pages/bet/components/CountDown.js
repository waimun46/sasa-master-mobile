import { Spin, Icon } from 'antd';
import CountdownBasic from '@/components/CountDown';
import classNames from "classnames";
import styles from "../index.less";
import countdownSound from '@/assets/sounds/effect/timer.mp3';
import lang from '@/lang';

const antIcon = <Icon type="loading" style={{ fontSize: 50 }} spin />;
export default class CountDown extends CountdownBasic {
    constructor(props) {
        super(props);
    }



    format = (time) => {
        const minutes = 60 * 1000;

        const m = Math.floor(time / minutes);
        const s = Math.floor((time - (m * minutes)) / 1000);
        return `${fixedZero(m)}: ${fixedZero(s)}`;

    }

    render() {
        const { timeDiff, game, } = this.props;
        let { lastTime } = this.state;
        const result = this.format(lastTime);
        const seconds = parseInt(lastTime / 1000);
        if (seconds == 5)
            this.sound.play();

        return (
            <div className={styles.countdown} >
{/*
            <div className={styles.time} >
                <span>{lang.waiting} </span>
                <p>00.00</p>
            </div>
*/}

                {game != null &&
                    <div className={styles.time} >
                        <span>期号 {game.id}</span>
                        <p>{result}</p>
                        {/* {<Sound url={countdownSound} playStatus={seconds > 0 && seconds <= 5 ? Sound.status.PLAYING : Sound.status.STOPPED} loop={false} />} */}
                     <audio ref={(sound) => { this.sound = sound; }}>
                            <source src={countdownSound} type="audio/mpeg" >
                            </source>
                        </audio>
                    </div>
                    ||
                    <div className={styles.time} >
                        <span>{lang.waiting}</span>
                        <br /><br />
                        <Spin indicator={antIcon} />
                    </div>
                }
            </div>

        );
    }
}

function fixedZero(val) {
    return val * 1 < 10 ? `0${val}` : val;
}
