import { connect } from 'dva';
import React, { Component } from 'react'
import Link from 'umi/link';
import Wallet from '@/services/wallet';
import $ from 'jquery';
import { Menu, Icon, Select, Button } from 'antd';
import FontIcon from '@/components/NiuIcon';
import stylespanel from "../index.less";
import lang from '@/lang';
import styles from '@/layouts/components/header/index.less'
const Option = Select.Option;

function handleChange(value) {
    console.log(value);
}


class Menuphone extends Component {

  state = { balance: null }

  /* ------------------- System --------------------- */

  /**
   * executed before rendering
   * @return {[type]} [description]
   */
  componentWillMount() {
      if (this.props.eosIdentity) {
          this.queryAccount(this.props);
      }
  }

  /**
   * invoked as soon as the props are updated before another render is called.
   * @param  {[type]} newProps [description]
   * @return {[type]}          [description]
   */
  componentWillReceiveProps(newProps) {
      if (newProps != this.props && newProps.eosIdentity && newProps.eosAccount == null) {
          this.queryAccount(newProps);
      }
  }

  queryAccount(props) {
      // Wallet.getBalance(props.eosIdentity.name).then(resp => {
      //     this.setState({ balance: resp });
      // });
      props.dispatch({ type: "global/getEOSAccount" });
  }

  /* ------------------- Events --------------------- */

  login = (e) => {
      Wallet.login();
  }
  logout = (e) => {
      Wallet.logout();
  }

  changeLang = (value) => {
      this.props.dispatch({ type: "global/changeLang", payload: value.key });
  }

    componentDidMount() {
        $('.toggle-hide').on('click', function () {
            $('.menu-hide').toggleClass('expanded');
            $('.span-hide').toggleClass('hidden-hide');
            $('.container-hide , .toggle-hide').toggleClass('close-hide');
        });
    }

  render() {
    const { eosIdentity, eosBalance, eosAccount } = this.props;
    return (
      <div className="menu-hide" id={stylespanel.menu_hide}>
          <div className="container-hide">
              <div className="toggle-hide"></div>
          </div>
          <span className="hidden-hide span-hide">
              <div className="lang-hide">
                <Select labelInValue defaultValue={{ key: lang.getLanguage() }} onChange={this.changeLang}>
                    <Option value="zh"><FontIcon type="icon-Flag_Chinese" className="lang_i"/> 简体中文</Option>
                    <Option value="en"><FontIcon type="icon-Flag_English" className="lang_i" /> English</Option>
                </Select>
              </div>
          </span>
          <span className="hidden-hide span-hide">
              <div className="rate-view">
                  <div className=" warp">
                      <div className="cpu">
                        {this.cpu}%
                        <p>CPU</p>
                      </div>
                  </div>
                  <div className=" warp">
                      <div className="ram">
                        {this.ram}%
                        <p>RAM</p>
                      </div>
                  </div>
                  <div className="clearfix"></div>
              </div>
          </span>
          <span className="hidden-hide span-hide">
              <div className="user-view">

              {eosIdentity == null
                  ?
                  <Button type="primary" onClick={this.login}>LOGIN</Button>
                  :
                  <Menu mode="horizontal">
                      <Menu.SubMenu title={<span><Icon type="user" />{eosIdentity.name}<Icon className={styles.icon_dropdown} type="caret-down" /></span>}>
                          <Menu.Item key="setting:1"><Icon type="wallet" />{this.state.balance}</Menu.Item>
                          <Menu.Item key="setting:3"><Icon type="link" />Referral Link</Menu.Item>
                          <Menu.Item key="logout" onClick={this.logout}><Icon type="logout" /> {lang.logout}</Menu.Item>
                      </Menu.SubMenu>
                  </Menu>
              }

              {/*
              <Menu mode="horizontal">
                  <Menu.SubMenu title={<span><Icon type="user" />bilytan<Icon className={styles.icon_dropdown} type="caret-down" /></span>}>
                      <Menu.Item key="setting:1" ><Icon type="wallet" />6,000,000,000</Menu.Item>
                      <Menu.Item key="setting:3"><Icon type="link" />Referral Link</Menu.Item>
                      <Menu.Item key="logout" onClick={this.logout}><Icon type="logout" /> {lang.logout}</Menu.Item>
                  </Menu.SubMenu>
              </Menu>
              */}
              </div>
          </span>
          <span className="hidden-hide span-hide">
              <div className="game-role-hide">
                <Link to="/gameintro">
                  <Button type="primary">{lang.game_role}</Button>
                </Link>
              </div>
          </span>
          <div className="clearfix"></div>
      </div>
    );
  }

  get cpu() {
      const { eosAccount } = this.props;
      if (eosAccount == null || !eosAccount.cpu_limit) return 'NaN';
      return percent(eosAccount.cpu_limit.used / eosAccount.cpu_limit.max);
  }

  get ram() {
      const { eosAccount } = this.props;
      if (eosAccount == null || !eosAccount.net_limit) return 'NaN';
      return percent(eosAccount.net_limit.used / eosAccount.net_limit.max);
  }

};

const percent = (val, digits = 2) => parseFloat((val * 100).toFixed(digits));

export default connect(state => ({
    eosIdentity: state.global.eosIdentity,
    eosAccount: state.global.eosAccount,
    eosBalance: state.global.eosBalance
}))(Menuphone);
