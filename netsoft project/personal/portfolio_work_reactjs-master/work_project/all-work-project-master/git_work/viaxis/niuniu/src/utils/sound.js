
import ReactDOM from 'react-dom';

const remove = (id) => {
    var elem = document.getElementById(id);
    return elem.parentNode.removeChild(elem);
}

export function play(url) {
    const id = `audio_${parseInt(new Date().getTime())}`;
    var audio = (<audio autoPlay={true} onEnded={() => remove(id)}>
        <source src={url} type="audio/mpeg" >
        </source>
    </audio>);

    let div = document.createElement('div');
    div.setAttribute('id', id);
    document.body.append(div);

    ReactDOM.render(audio, div);

}

export default { play };