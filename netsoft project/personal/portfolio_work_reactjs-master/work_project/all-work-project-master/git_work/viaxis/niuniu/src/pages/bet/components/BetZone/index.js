import { connect } from 'dva';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { message } from 'antd';
import Hand from '../Hand';
import Confirm from '../WagerConfirm';
import Coin from './Coin';
import styles from './index.less';
import Wallet from '@/services/wallet';
import Sound from '@/utils/sound';
import coinSound from '@/assets/sounds/effect/money-drop.mp3';

class BetZone extends Component {

    state = {
        confirm_amt: 0,
        confrim_offset: null
    }

    /* --------------- events -------------- */
    onCoinSectionClick = (e) => {
        let sectionOffset = ReactDOM.findDOMNode(this.refs['coinSection'])
            .getBoundingClientRect();
        let x = e.pageX - sectionOffset.x;
        let y = e.pageY - sectionOffset.y;
        this.setState({ confrim_offset: { x, y } });

        const { selectedWager } = this.props;
        const { confirm_amt } = this.state;
        this.setState({ confirm_amt: confirm_amt + selectedWager });

        // this.betSound.play();
        Sound.play(coinSound);
    }

    onConfirmOk = (e) => {
        let { confirm_amt } = this.state;
        const { game_id, heap, account } = this.props;
        Wallet.bet(game_id, `${confirm_amt.toFixed(4)} EOS`, heap, account.referral)
            .then(() => { message.success("下注成功"); this.setState({ confirm_amt: 0 }); })
            .catch(err => err.message && message.error(err.message) || message.error(err));
    }

    onConfirmCancel = (e) => {
        e.stopPropagation();
        this.setState({ confirm_amt: 0 });
        return false;
    }

    render() {
        const { bets, bets_mine, bets_total } = this.props;
        let { confirm_amt, confrim_offset } = this.state;
        const { hand } = this.props;

        return (
            <div className={styles.zone} id="zone-inner">
                <div className={styles.bet_area}>
                    <div className={styles.type_bg}>
                        <div className={styles.total_bet}>
                            {bets_total.toFixed(1)}
                        </div>

                        <div ref="coinSection" className={styles.coins} onClick={this.onCoinSectionClick}>
                            {bets.map((it, i) => <Coin key={i} />)}
                            <Confirm amount={confirm_amt} offset={confrim_offset} onOk={this.onConfirmOk} onCancel={this.onConfirmCancel} />
                        </div>

                        <div className={styles.my_bet}>
                            {bets_mine.toFixed(1)}
                        </div>
                    </div>
                </div>
                <ul>
                        <Hand value={hand && hand.result} />
                  {/* <Hand value="♣6,♥2,♠7,♥K,♠3" />*/}
                </ul>
                {/* <audio ref={(sound) => { this.betSound = sound; }}>
                    <source src={coinSound} type="audio/mpeg" >
                    </source>
                </audio> */}
            </div>

        );
    }

};

export default connect(state => ({
    selectedWager: state.global.selectedWageButton,
    account: state.global.account
}))(BetZone);
