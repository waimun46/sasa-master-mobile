import React, { Component } from 'react';
import Link from 'umi/link';

import TopInfor from './components/TopInfor';
import MiddleInfor from './components/MiddleInfor';
import BottomInfor from './components/BottomInfor';
import SlideOutPanel from './components/SlideOutPanel';


class Gamepage extends Component {

    render() {
        return (
            <div id="Gamepage">
                <TopInfor />
                <MiddleInfor />
                <BottomInfor />
                <SlideOutPanel />
            </div>
        );
    }
};

export default Gamepage;
