import { routerRedux } from 'dva/router';
import WebSocket from '@/websocket';

const ws = new WebSocket();

export default {
    namespace: 'game',
    state: {
        banker: null,
        actives: {},
        open: null,
        bets: {},
        bets_mine: {},
        bets_total: {},
        history: {},
        betHistory: {
            all: {},
            mine: {}
        },
        ranking: []
    },
    reducers: {
        saveData(state, { payload }) {
            return { ...state, ...payload };
        },
        reveal(state, { payload }) {
            return { ...state, open: payload };
        },
        saveBets(state, { payload }) {
            const { betHistory } = state;
            return { ...state, betHistory: { ...betHistory, ...payload } };
        }
    },
    effects: {
        *actives(action, { call, put, get }) {
            let data = yield ws.call("game.active");
            yield put({ type: "saveData", payload: { actives: data.games, banker: data.banker } });
        },
        *getBanker({ payload }, { select, call, put, get }) {

        },
        *start({ payload }, { select, call, put, get }) {
            const state = yield select(state => state.game);
            let actives = {
                ...state.actives,
                [payload.type]: {
                    id: payload.game_id,
                    type: payload.type,
                    open_time: payload.open_time
                }
            };
            const saveData = { actives, open: null, banker: payload.banker };
            const { open } = state;
            if (open != null && open.game_id) {
                const { [open.game_id]: del, ...bets } = state.bets;
                const { [open.game_id]: del2, ...bets_mine } = state.bets_mine;
                const { [open.game_id]: del3, ...bets_total } = state.bets_total;
                saveData['bets'] = bets;
                saveData['bets_mine'] = bets_mine;
                saveData['bets_total'] = bets_total;
            }
            yield put({ type: "saveData", payload: saveData });
        },
        *bet({ payload }, { select, call, put, get }) {
            const { game_id } = payload;
            const { summary, ...bet } = payload;
            const state = yield select(state => state.game);
            const account = yield select(state => state.global.account);
            const { bets, bets_mine, bets_total } = state;
            const game_bets = {};
            game_bets[game_id] = [...(bets[game_id] || []), bet];

            let mine_summary = bets_mine;
            const total_summary = { ...bets_total, [game_id]: summary.total };
            if (account && account.name == bet.account) {
                mine_summary = { ...bets_mine, [game_id]: summary.bettor };
            }

            const data = { bets: Object.assign(bets, game_bets), bets_total: total_summary, bets_mine: mine_summary }

            yield put({ type: "saveData", payload: data });
        },
        *getSummary({ payload }, { select, call, put, get }) {
            if (!payload || isNaN(payload)) return;
            const { bets, ...summary } = yield ws.call("game.GetSummary", { "game_id": payload });
            const state = yield select(state => state.game);
            const { bets_mine, bets_total } = state;

            const game_bets = {};
            game_bets[payload] = [...bets];

            const total_summary = { ...bets_total, [payload]: summary.total };
            const mine_summary = { ...bets_mine, [payload]: summary.bettor };
            const data = { bets: Object.assign(bets, game_bets), bets_total: total_summary, bets_mine: mine_summary }

            yield put({ type: "saveData", payload: data });
        },
        *getHistory({ payload }, { select, call, put, get }) {
            const history = yield ws.call("game.history", payload);
            yield put({ type: "saveData", payload: { history } });
        },
        *getAllBets({ payload }, { select, call, put, get }) {
            const all = yield ws.call("game.allbets", payload);
            yield put({ type: "saveBets", payload: { all } });
        },
        *getMyBets({ payload }, { select, call, put, get }) {
            const mine = yield ws.call("game.minebets", payload);
            yield put({ type: "saveBets", payload: { mine } });
        },
        *getRanking({ payload }, { select, call, put, get }) {
            const data = yield ws.call("game.rankinglist");
            yield put({ type: "saveData", payload: { ranking: data } });
        },
    },
    subscriptions: {
        setup({ dispatch, history }) {
            return history.listen(({ pathname, query }) => {

                if (pathname == "/bet") {
                    dispatch({ type: 'actives' });
                }
                ws.on("game.reveal", function (data) {
                    dispatch({ type: 'reveal', payload: data });
                });
                ws.on("bet.reveal", function (data) {
                    const { game_id, payout } = data;
                    const popup = require('../pages/bet/components/Popup').default;
                    popup(payout > 0, { payout });
                });
                ws.on("game.start", function (data) {
                    dispatch({ type: 'start', payload: data })
                    dispatch({ type: 'getBanker' });
                });
                ws.on("game.bet", function (data) {
                    dispatch({ type: 'bet', payload: data });
                });
                ws.on("account.info", function (data) {
                    dispatch({ type: 'global/saveData', payload: { account: data } });
                    dispatch({ type: 'global/getEOSBalance' });
                    dispatch({ type: 'global/getEOSAccount' });
                });
            });
        },
    },
};
