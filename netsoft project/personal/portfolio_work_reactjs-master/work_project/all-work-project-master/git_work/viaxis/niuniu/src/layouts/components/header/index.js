import { connect } from 'dva';
import React, { Component } from 'react';
import { Menu, Icon, Button } from 'antd';
import Link from 'umi/link';
import { NoticeBar, WhiteSpace } from 'antd-mobile';
import Wallet from '@/services/wallet';
import lang from '@/lang';
import FontIcon from '@/components/NiuIcon';

import styles from './index.less'

import { Select } from 'antd';

const Option = Select.Option;

class Header extends Component {

    state = { balance: null }

    /* ------------------- System --------------------- */

    /**
     * executed before rendering
     * @return {[type]} [description]
     */
    componentWillMount() {
        if (this.props.eosIdentity) {
            this.queryAccount(this.props);
        }
    }

    /**
     * invoked as soon as the props are updated before another render is called.
     * @param  {[type]} newProps [description]
     * @return {[type]}          [description]
     */
    componentWillReceiveProps(newProps) {
        if (newProps != this.props && newProps.eosIdentity && newProps.eosAccount == null) {
            this.queryAccount(newProps);
        }
    }

    queryAccount(props) {
        // Wallet.getBalance(props.eosIdentity.name).then(resp => {
        //     this.setState({ balance: resp });
        // });
        props.dispatch({ type: "global/getEOSAccount" });
    }

    /* ------------------- Events --------------------- */

    login = (e) => {
        Wallet.login();
    }
    logout = (e) => {
        Wallet.logout();
    }

    changeLang = (value) => {
        this.props.dispatch({ type: "global/changeLang", payload: value.key });
    }

    render() {
        const { eosIdentity, eosBalance, eosAccount } = this.props;

        return (
            <header id="niuniu" className={styles.header}>
                <div className={styles.header_warp}>
                    <div className={styles.header_left}>
                        &emsp;
                    </div>
                    <div className={styles.header_center}>
                        <NoticeBar marqueeProps={{ loop: true, style: { padding: '0 7.5px' } }}>
                            Notice: The arrival time of incomes and transfers of Yu &#39;E Bao will be delayed during National Day.
                            The arrival time of incomes and transfers of Yu &#39;E Bao will be delayed during National Day.
                      </NoticeBar>
                    </div>

                    <div className={styles.header_right}>
                        <div className={styles.right_warp}>
                            <div className={styles.lang_select}>
                                <Select labelInValue defaultValue={{ key: lang.getLanguage() }} onChange={this.changeLang}>
                                    <Option value="zh"><FontIcon type="icon-Flag_Chinese" /> 简体中文</Option>
                                    <Option value="en"><FontIcon type="icon-Flag_English" /> English</Option>
                                </Select>
                            </div>
                        </div>
                        <div className={styles.right_warp}>
                            <div className={styles.rate_view} >
                                <div className={styles.warp}>
                                    <div className={styles.cpu}>
                                        {this.cpu}%
                                    <p>CPU</p>
                                    </div>
                                </div>
                                <div className={styles.warp}>
                                    <div className={styles.ram}>
                                          {this.ram}%
                                    <p>RAM</p>
                                    </div>
                                </div>
                                <div className="clearfix"></div>
                            </div>
                        </div>
                        <div className={styles.right_warp}>
                            <div className={styles.user_view}>

                                {eosIdentity == null
                                    ?
                                    <Button type="primary" onClick={this.login}>{lang.login}</Button>
                                    :
                                    <Menu mode="horizontal">
                                        <Menu.SubMenu title={<span><Icon type="user" />{eosIdentity.name}<Icon className={styles.icon_dropdown} type="caret-down" /></span>}>
                                            <Menu.Item key="setting:1"><Icon type="wallet" />{this.state.balance}</Menu.Item>
                                            <Menu.Item key="setting:3"><Icon type="link" />Referral Link</Menu.Item>
                                            <Menu.Item key="logout" onClick={this.logout}><Icon type="logout" /> logout</Menu.Item>
                                        </Menu.SubMenu>
                                    </Menu>
                                }

                                {/*
                                <Menu mode="horizontal">
                                    <Menu.SubMenu title={<span><Icon type="user" />bilytan<Icon className={styles.icon_dropdown} type="caret-down" /></span>}>
                                        <Menu.Item key="setting:1" ><Icon type="wallet" />6,000,000,000</Menu.Item>
                                        <Menu.Item key="setting:3"><Icon type="link" />Referral Link</Menu.Item>
                                        <Menu.Item key="logout" onClick={this.logout}><Icon type="logout" /> logout</Menu.Item>
                                    </Menu.SubMenu>
                                </Menu>
                                */}
                            </div>
                        </div>
                        <div className="clearfix"></div>
                    </div>
                    <div className="clearfix"></div>
                </div>
            </header >
        );
    }

    get cpu() {
        const { eosAccount } = this.props;
        if (eosAccount == null || !eosAccount.cpu_limit) return 'NaN';
        return percent(eosAccount.cpu_limit.used / eosAccount.cpu_limit.max);
    }

    get ram() {
        const { eosAccount } = this.props;
        if (eosAccount == null || !eosAccount.net_limit) return 'NaN';
        return percent(eosAccount.net_limit.used / eosAccount.net_limit.max);
    }

};
const percent = (val, digits = 2) => parseFloat((val * 100).toFixed(digits));


export default connect(state => ({
    eosIdentity: state.global.eosIdentity,
    eosAccount: state.global.eosAccount,
    eosBalance: state.global.eosBalance
}))(Header);
