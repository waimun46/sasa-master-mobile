
/**
 * 千位符号
 * @param  {Function} () [description]
 * @return {[type]}      [description]
 */
export const milli = (() => {
    const DIGIT_PATTERN = /^-?\d+(?=\.?\d*($|\s))/g
    const MILLI_PATTERN = /(?=(?!\b)(\d{3})+$)/g

    return (input, digits = 2) => input && input.toFixed(digits)
        .replace(DIGIT_PATTERN, (m) => m.replace(MILLI_PATTERN, ','))
})();

export function datetime(date, fmt = "yyyy-MM-dd HH:mm:ss") {
    var o = {
        "M+": date.getMonth() + 1, //月份         
        "d+": date.getDate(), //日         
        "h+": date.getHours() % 12 == 0 ? 12 : date.getHours() % 12, //小时         
        "H+": date.getHours(), //小时         
        "m+": date.getMinutes(), //分         
        "s+": date.getSeconds(), //秒         
        "q+": Math.floor((date.getMonth() + 3) / 3), //季度         
        "S": date.getMilliseconds() //毫秒         
    };
    var week = {
        "0": "/u65e5",
        "1": "/u4e00",
        "2": "/u4e8c",
        "3": "/u4e09",
        "4": "/u56db",
        "5": "/u4e94",
        "6": "/u516d"
    };
    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
    }
    if (/(E+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, ((RegExp.$1.length > 1) ? (RegExp.$1.length > 2 ? "/u661f/u671f" : "/u5468") : "") + week[date.getDay() + ""]);
    }
    for (var k in o) {
        if (new RegExp("(" + k + ")").test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        }
    }
    return fmt;
};

/**
 * 数值百分比
 * @param  {[type]} val    [description]
 * @param  {Number} digits [description]
 * @return {[type]}        [description]
 */
export function percent(val, digits = 2) {
    return parseFloat((val * 100).toFixed(digits));
}



