import React, { Component } from 'react';
import Link from 'umi/link';
import { Modal, Button } from 'antd';
import diamond from '../../../../assets/images/icon/Icon_EOS.png';
import user from '@/assets/images/icon/Icon_Host.png';
import token from '@/assets/images/icon/Icon_Token.png';
import challenge from '@/assets/images/icon/Icon_Challenge.png';
import Icon from '@/components/NiuIcon';

class User extends Component {

    state = {
        visible: false,
        visiblejackpot: false,
    }

    /* shangzhuang popup */
    showModal = () => {
        this.setState({
            visible: true,
        });
    }

    handleOk = (e) => {
        console.log(e);
        this.setState({
            visible: false,
        });
    }

    handleCancel = (e) => {
        console.log(e);
        this.setState({
            visible: false,
        });
    }

    /* jackpot popup */

    showModaljackpot = () => {
        this.setState({
            visiblejackpot: true,
        });
    }

    handleOkjackpop = (e) => {
        console.log(e);
        this.setState({
            visiblejackpot: false,
        });
    }

    handleCanceljackpop = (e) => {
        console.log(e);
        this.setState({
            visiblejackpot: false,
        });
    }

    render() {
        return (
            <div className="user">
                <div className="user-card">
                    <div className="avatar">
                        <img src={user} alt="user" />
                    </div>
                    <div className="details">
                        <div className="name">
                            Johnnybarnes
            </div>
                        <div className="number fontsty">
                            <Icon type="dollar" style={{ color: 'yellow' }} />

                            <p>6,666,666,666,666</p>
                        </div>
                        <div className="user-up-btn" onClick={this.showModal}>
                            <img src={challenge} alt="challenge" />
                            <p>上庄</p>
                        </div>
                    </div>
                </div>
                <div className="jackport" onClick={this.showModaljackpot}>
                    <div className="warp-content">
                        <img src={token} alt="token" />
                    </div>
                    <div className="warp-content">
                        <p>JACKPOT</p>
                    </div>
                    <div className="warp-content fontsty">
                        <p>555555</p>
                    </div>
                    <div className="warp-content" style={{ fontWeight: 'bold' }}>
                        <p>万</p>
                    </div>
                    <div className="clearfix"></div>

                </div>

                {/* shangzhuang popup */}
                <Modal
                    title="上庄表"
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    className="shangzhuang"
                >
                    <div className="top-content">
                        <div className="left-side">
                            <div className="icon-user">
                                <img src={user} alt="user" />
                            </div>
                            <div className="user-title">
                                <p>Johnnybarnes</p>
                                <p className="diamond-sty">
                                    6,666,666,666,666 <img src={diamond} alt="diamond" />
                                </p>
                                <div className="clearfix"></div>
                            </div>
                        </div>
                        <div className="right-side">
                            <p>上庄条件 ：</p>
                            <p>10.00000</p>
                            <div className="clearfix"></div>
                        </div>
                        <div className="clearfix"></div>
                    </div>

                    <div className="bottom-content">
                        <ul>
                            <li>
                                <p>玩家</p>
                                <p>资金</p>
                                <div className="clearfix"></div>
                            </li>
                            <li>
                                <p>Markez1919</p>
                                <p>10.154875</p>
                                <div className="clearfix"></div>
                            </li>
                            <li>
                                <p>Markez1919</p>
                                <p>10.154875</p>
                                <div className="clearfix"></div>
                            </li>
                        </ul>
                        <div className="btn-shangzhuang">
                            <Button type="primary">上庄</Button>
                        </div>
                    </div>
                </Modal>

                {/* jackpot popup */}
                <Modal
                    title="奖池信息"
                    visible={this.state.visiblejackpot}
                    onOk={this.handleOkjackpop}
                    onCancel={this.handleCanceljackpop}
                    className="jackpot_popup"
                >
                    <Icon type="icon-Hearts_13" />
                </Modal>
            </div>
        );
    }
};

export default User;
