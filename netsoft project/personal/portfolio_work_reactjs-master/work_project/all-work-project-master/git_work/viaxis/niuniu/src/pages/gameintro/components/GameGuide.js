import React, { Component } from 'react';
import styles from '../index.less';
import lang from '@/lang';

class GameGuide extends Component {


  render() {
    return (
        <section className={styles.game_guide}>
          <div className={styles.content}>
            <header>
                <h1>{lang.game_guide_title}</h1>
            </header>
            <div className={styles.content_guide}>
              <div className={styles.step}>
                <div className={styles.icon}></div>
                <label>1. {lang.deal}</label>
                <p>{lang.deal_guide_content}</p>
              </div>
              <div className={styles.step}>
                <div className={styles.icon}></div>
                <label>2. {lang.place_wager}</label>
                <p>{lang.place_wager_content}</p>
              </div>
              <div className={styles.step}>
                <div className={styles.icon}></div>
                <label>3. {lang.settling}</label>
                <p>{lang.settling_content}</p>
              </div>
              <div className={styles.step}>
                <div className={styles.icon}></div>
                <label>4. {lang.result}</label>
                <p>{lang.result_content}</p>
              </div>
              <div className="clearfix"></div>
            </div>
          </div>
        </section>
    );
  }
};

export default GameGuide;
