import React, { Component } from 'react';
import router from 'umi/router';
import { Icon } from 'antd';
import PaihangbangPanel from './infor/PaihangbangPanel';
import ZhoushituPanel from './infor/ZhoushituPanel';


class SlideOutPanel extends Component {

  render() {
    return (
      <div id="SlideOutPanel">
        <div className="back-btn" onClick={() => router.goBack()}>
          <Icon type="left" />
        </div>
        <div className="game-role-btn">
          <Icon type="book" />
        </div>
        <PaihangbangPanel/>
        <ZhoushituPanel/>
      </div>
    );
  }
};

export default SlideOutPanel;
