import { Client } from 'rpc-websockets';

const Status = {
    "connecting": 0,
    "connected": 1,
    "closing": 2,
    "closed": 3
};

export default class WebSocket {

    ws = null;
    status = 3;
    connectPromise = null;

    constructor() {
        if (WebSocket.instance !== null) {
            return WebSocket.instance;
        }
        WebSocket.instance = this;
        this._connect();
    }

    _connect() {
        if (this.ws != null && this.status == Status.connecting && this.connectPromise != null)
            return this.connectPromise;

        return this.connectPromise = new Promise(function (resolve, reject) {
            if (this.ws != null && this.status == Status.connected) {
                return resolve(this.ws);
            }

            let query = {};
            if (sessionStorage.account)
                query['account'] = sessionStorage.account;
            if (sessionStorage.referral)
                query['referral'] = sessionStorage.referral;

            let queryString = Object.keys(query).map(key => key + '=' + query[key]).join('&');
            let ws = this.ws = new Client(`${configs.ws}?${queryString}`);
            this.status = Status.connecting;

            ws.on('open', () => {
                this.status = Status.connected;
                console.log("web socket opened.");
                resolve(ws);
            });
            ws.on('close', () => {
                this.status = Status.closed;
                console.log('web socket closed.');
                reject();
            });
            ws.on('error', (err) => {
                console.error(err);
                reject(err);
            });
        }.bind(this));
    }

    get Socket() {
        return this.ws;
    }

    /**
     * Calls a registered RPC method on server.
     * @method
     * @param {String} method - RPC method name
     * @param {Object|Array} params - optional method parameters
     * @param {Number} timeout - RPC reply timeout value
     * @param {Object} ws_opts - options passed to ws
     * @return {Promise}
     */
    call(method, params, timeout, ws_opts) {
        return this._connect().then(ws => ws.call(method, params, timeout, ws_opts));
    }

    /**
     * Sends a JSON-RPC 2.0 notification to server.
     * @method
     * @param {String} method - RPC method name
     * @param {Object} params - optional method parameters
     * @return {Promise}
     */
    notify(method, params) {
        return this._connect().then(ws => ws.notify(method, params));
    }

    on(method, cb) {
        return this._connect().then(ws => ws.on(method, cb));
    }

    subscribe(method) {
        return this._connect().then(ws => ws.subscribe(method));
    }

}
WebSocket.instance = null;
