import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'antd';
import Icon from '@/components/NiuIcon';
import styles from "./index.less";

class Confirm extends PureComponent {


    onOk = (e) => {
        e.stopPropagation();
        this.props.onOk && this.props.onOk(e);
    }
    onCancel = (e) => {
        e.stopPropagation();
        const { onCancel } = this.props;
        onCancel && onCancel(e);
    }

    render() {
        const { offset, amount } = this.props;
        let x = (offset && offset.x && offset.x - 75) || 150 / 2;
        let y = (offset && offset.y && offset.y - 15) || 174 / 2;
        if (x < 0) x = 0;

        if (amount <= 0) return null;
        return (
            <div className={styles.wager_confirm} style={{ "top": y, "left": x }}>
                <div className={styles.amount}>
                    {amount.toFixed(2)}
                    <Icon type="icon-Icon_EOS" />
                </div>
                <div className={styles.actions}>
                    <Button icon="check" size="small" onClick={this.onOk}></Button>
                    <Button icon="close" size="small" onClick={this.onCancel}></Button>
                </div>
            </div>
        );
    }
};

Confirm.propTypes = {
    amount: PropTypes.number.isRequired,
    offset: PropTypes.object,
    onOk: PropTypes.func,
    onCancel: PropTypes.func
};

export default Confirm;
