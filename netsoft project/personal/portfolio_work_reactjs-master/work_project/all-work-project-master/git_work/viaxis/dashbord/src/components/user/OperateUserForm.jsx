import React, { Component, PropTypes } from 'react';
import ReactDOM from "react-dom";
import { connect } from 'react-redux';
import { Link } from "react-router";
import { message, Icon, Switch, Modal, Form, Input, Select, InputNumber, Tooltip } from 'antd';

import BasicComponent from "../common/BasicComponent";
import EditorModal from "../common/EditorModal";

import { UserType, UserKind, UserStatus } from "../../app_enums";
import * as Constants from "../../app_const";
import * as Utils from "../../utils/common";
import * as Validators from "../../utils/validator";

class OperateUserForm extends BasicComponent{

    constructor(props) {
        super(props);
        this.state = {
            loading: false
        };
    }

    /* ----------------------------- Sys ------------------------------ */

    handleSubmit(values){
        const { parent, user, isadd } = this.props;

        if(!values.backpct) values.backpct = 0
        if(!values.daily_wages) values.daily_wages = 0

        const fnOperate = () => {
            this.setState({loading:true});
            if(isadd){
                values['aid'] = user.ID;
                values['user_kind'] = user.UserKind + 1;
                values['user_type'] = user.UserTyp;
                values['enable_payout'] = user.EnablePayout;
                values['enable_comm'] = user.EnableComm;
                values['enable_daily_wages'] = user.EnableDailyWages;
            }
            this.props.onSubmit && this.props.onSubmit(user,values).then(act => {

                    this.setState({
                        loading: false
                    });

                    const resp = act.response;
                    if (resp.code == 0) {
                        message.success(isadd ? "添加成功" : "修改成功");
                    } else {
                        message.error(resp.message);
                    }

                });
        };

        fnOperate();
    }

    render() {
        const { parent, user, isadd, modifypwd } = this.props;

        const isTrial = user.UserType == 1;
        const userKind = isadd ? UserKind[user.UserKind + 1] : UserKind[user.UserKind];
        const backpct = parseFloat((parent.Backpct * 100).toFixed(2));
        const dailyWages = parseFloat((parent.DailyWages * 100).toFixed(2));
            
        const currentbackpct = parseFloat((user.Backpct * 100).toFixed(2));
        const currentdailyWages = parseFloat((user.DailyWages * 100).toFixed(2));

        const title = isadd ? "添加" + user.Email + "的下级" + UserKind[user.UserKind+1] : "编辑" + user.Email;

        return (
            <EditorModal title={title} visible = {true}
                submiting = {this.state.loading}
                onSubmit = {this.handleSubmit.bind(this)}>
                {isadd&& <email label="邮箱" options={{
                    validateTrigger: "onBlur",
                        rules: [{
                            required: true, message: `请输入${userKind}的邮箱。`
                        },{
                            type: 'email',
                            message: '请输入合法信箱',
                        },{
                            validator: Validators.checkEmailExisted.bind(this)
                        }],
                    }}>
                    <Input type="email" autoComplete="off" />
                </email>}
                {(isadd || modifypwd)&&<password label="登陆密码" options={{
                        rules: [{
                                required: modifypwd&&isadd ? true : false,
                                message: `请输入${userKind}登陆密码`
                            }, {
                                min: 6,max:20,
                                message: "登陆密码长度必须在6与20之间"
                            }],
                    }}>
                    <Input type="password" autoComplete="off" />
                </password>}
                <backpct label="返点" options={{
                    initialValue: currentbackpct,
                        rules: [{
                            validator: Validators.checkPrecision,
                            message: "小数点后最多只允许存在2位!"
                        }]
                    }}>
                    <InputNumber type="backpct" min={0} max={backpct} step={Utils.getPrecision(backpct.toString())} />
                    <span className="ant-form-text"> % (最大{backpct}%)</span>
                    <Tooltip placement="right" title={Constants.COMMISSION_EXPLAIN}>
                            <Icon type="question-circle" />
                    </Tooltip>
                </backpct>
                {!isadd&&<enable_comm label="发放佣金" options={{
                        valuePropName: "checked",
                        initialValue: user.EnableComm,
                    }}>
                    <Switch disabled={isTrial} />
                </enable_comm>}  
                <daily_wages label="日工资" options={{
                        initialValue : currentdailyWages,
                        rules: [{
                            validator: Validators.checkPrecision,
                            message: "小数点后最多只允许存在2位!"
                        }]
                    }}>
                    <InputNumber min={0} max={dailyWages} step={Utils.getPrecision(dailyWages.toString())} />
                    <span className="ant-form-text"> % (最大{dailyWages}%)</span>
                    <Tooltip placement="right" title={Constants.DAILY_WAGES_EXPLAIN}>
                            <Icon type="question-circle" />
                    </Tooltip>
                </daily_wages>
                {!isadd&&<enable_daily_wages label="发放日工资" options={{
                        valuePropName: "checked",
                        initialValue: user.EnableDailyWages,
                    }}>
                     <Switch disabled={isTrial} />
                </enable_daily_wages>}
                {!isadd&&<enable_payout label="发放红利" options={{
                        valuePropName: "checked",
                        initialValue: user.EnablePayout,
                    }}>
                     <Switch disabled={isTrial} />
                </enable_payout>}  
                <remark label="备注" options={{
                        initialValue: isadd ? "" : user.Remark,
                            rules:[{
                                validator: Validators.checkChineseLength(),
                                message: "只允许英文数字下划线(最多20个)与汉字(最多10个)"
                            }]
                    }}>
                    <Input type="textarea" />
                </remark>
                <mobile label="手机号码" options={{
                        initialValue: isadd ? "" : user.Mobile,
                            rules: [
                                { type: "string", pattern: /\d{11}/, message: "请输入正确的手机号码(长度为11码)" }
                            ]
                    }}>
                     <Input type="mobile" autoComplete="off" />
                </mobile>
                <qq label="QQ" options={{
                        initialValue: isadd ? "" : user.QQ,
                            rules: [
                                { type: "string", pattern: /\d{5,20}/, message: "请输入正确的QQ号码" }
                            ]
                    }}>
                    <Input type="mobile" autoComplete="off" />
                </qq>
                {!isadd&&<status label="状态" options={{
                        initialValue: (user.Status || 0).toString(),
                        rules: [{
                            required: true,
                            message: '请选择状态'
                        }]
                    }}>
                    <Select>
                        {Object.keys(UserStatus).map(it=>{
                            return <Option key={it} value={it}>{UserStatus[it].name}</Option>;
                        })}
                    </Select>
                </status>}
                {!isadd&&<desc label="修改说明" options={{
                        initialValue: "",
                        rules: [ {required:true, message:"请填入修改原因"} ]
                    }}>
                     <Input type="textarea" placeholder="修改的原因(必填)" />
                </desc>}
            </EditorModal>
        );
    }
}

OperateUserForm.propTypes = {
    user: React.PropTypes.object.isRequired,
    parent: React.PropTypes.object.isRequired,
    isadd: React.PropTypes.bool.isRequired,
    OnSubmit:React.PropTypes.func,
};

export default Form.create()(OperateUserForm);