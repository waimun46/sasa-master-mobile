import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { message, Spin, Row, Col, Table, Tag, Button, Modal, Form, Input, Select, DatePicker } from 'antd';
import moment from 'moment';

import BasicComponent from "../common/BasicComponent";
import SearchForm from "../common/SearchForm";

import * as UserAction from '../../actions/user';
import * as BillAction from '../../actions/bill';
import * as BankAction from '../../actions/bank';
import * as RenderHelper from "../../utils/render";
import * as Enums from "../../app_enums";

class UserInfo extends BasicComponent {

    constructor(props) {
        super(props);
        this.userId = this.getUserId(props);
        console.log(this.userId);
    }

    /* ----------------------------- Sys ------------------------------ */

    componentWillMount() {
        const { userInfo } = this.props;
        if (!userInfo || userInfo.UserId != this.userId)
            this.getUserInfo();
        this.getBillflow();
        this.queryBank();
    }

    componentWillReceiveProps(newProps) {
        if (newProps != this.props) {
            const userId = this.getUserId(newProps);
            if (userId != this.userId && !isNaN(userId)) {
                this.userId = userId;
                this.getUserInfo();
            }
        }

    }

    /* ----------------------------- Functions ----------------------------- */

    getUserInfo() {
        this.props.dispatch(UserAction.getUser(this.userId));
    }

    getBillflow(fromTime, toTime) {
        this.props.dispatch(BillAction.sumBillFlow(this.userId, fromTime, toTime));
    }

    queryBank() {
        this.props.dispatch(BankAction.queryUserBank(1, { userId: this.userId })).then(act => {
            if (act.error)
                Message.error(act.error);
        });
    }

    getUserId(props) {
        let userId = props.params.userId;
        if (userId)
            userId = parseInt(userId);
        return userId;
    }

    /* ----------------------------- Events -------------------------------- */

    onTimeRangeChange(dates, dateStrings) {
        const format = "YYYY-MM-DD HH:mm:ss";

        dates[0].set('hour', 0).set('minute', 0).set('second', 0);
        dates[1].set('hour', 23).set('minute', 59).set('second', 59);

        const from_time = dates[0].format(format);
        const to_time = dates[1].format(format);

        this.getBillflow(from_time, to_time);
    }

    /* ----------------------------- Renders ------------------------------- */

    get colSpan() {
        return 4;
    }

    render() {
        const { userInfo, billflow, isFetching } = this.props;
        const balance = userInfo ? userInfo.Summary.Deposit -
            userInfo.Summary.Withdraw -
            userInfo.Summary.TransferOut +
            userInfo.Summary.TransferIn -
            userInfo.Summary.Profit +
            userInfo.Summary.Commission +
            userInfo.Summary.Bonus :
            null;
        return (
            <div className="userinfo">
                <h2>用户信息</h2>
                <Spin spinning={!userInfo}>
                    {this.renderUserInfo()}
                </Spin>

                <h2>
                    流水统计
                    &nbsp;&nbsp;
                    {RenderHelper.rangeDatePick({ onChange: this.onTimeRangeChange.bind(this) })}
                </h2>
                {this.renderBillFlow()}

                <h2>用户资金</h2>
                <table className="balance-table">
                    <tbody>
                        <tr>
                            <td>余额</td>
                            <td>+</td>
                            <td>冻结</td>
                            <td>=</td>
                            <td>当前余额</td>
                            <td>=</td>
                            <td>理论余额</td>
                            <td>=</td>
                            <td>总充值</td>
                            <td>-</td>
                            <td>总提现</td>
                            <td>-</td>
                            <td>总转出</td>
                            <td>+</td>
                            <td>总转入</td>
                            <td>+</td>
                            <td>盈亏</td>
                            <td>+</td>
                            <td>佣金</td>
                            <td>奖金</td>
                        </tr>
                        <tr>
                            <td>{userInfo && RenderHelper.money_format(userInfo.Balance)}</td>
                            <td>+</td>
                            <td>{userInfo && RenderHelper.money_format(userInfo.Freeze)}</td>
                            <td>=</td>
                            <td className="highlight">
                                {userInfo && RenderHelper.money_format(userInfo.Balance + userInfo.Freeze)}
                            </td>
                            <td>=</td>
                            <td className="highlight">
                                {balance && RenderHelper.money_format(balance)}
                            </td>
                            <td>=</td>
                            <td>{userInfo && RenderHelper.money_format(userInfo.Summary.Deposit)}</td>
                            <td>-</td>
                            <td>{userInfo && RenderHelper.money_format(userInfo.Summary.Withdraw)}</td>
                            <td>-</td>
                            <td>{userInfo && RenderHelper.money_format(userInfo.Summary.TransferOut)}</td>
                            <td>+</td>
                            <td>{userInfo && RenderHelper.money_format(userInfo.Summary.TransferIn)}</td>
                            <td>+</td>
                            <td>{userInfo && RenderHelper.money_color(userInfo.Summary.Profit)}</td>
                            <td>+</td>
                            <td>{userInfo && RenderHelper.money_format(userInfo.Summary.Commission)}</td>
                            <td>+</td>
                            <td>{userInfo && RenderHelper.money_format(userInfo.Summary.Bonus)}</td>
                        </tr>
                    </tbody>
                </table>
                <br />
                {this.renderBalance()}
                <br />
                <h2>银行卡信息</h2>
                {this.renderBank()}
            </div>
        );
    }

    renderBillFlow() {
        const { billflow, isBFFetching } = this.props;
        const cols = [];
        for (var i in Enums.BillFlowType) {
            cols.push(<Col className="label" span={this.colSpan}>
                {Enums.BillFlowType[i]}
            </Col>);
            cols.push(<Col className="gutter-row" span={this.colSpan}>
                {billflow && billflow[i] ? RenderHelper.money_color(billflow[i]) : 0}
            </Col>);
        }
        return (
            <Spin spinning={isBFFetching}>
                <Row>
                    {cols}
                </Row>
            </Spin>
        );
    }

    renderUserInfo() {
        const { userInfo, billflow, isFetching } = this.props;
        return (
            <Row>
                <Col className="label" span={this.colSpan}>
                    Email
                </Col>
                <Col className="gutter-row" span={this.colSpan}>
                    {userInfo && userInfo.Email}
                </Col>

                <Col className="label" span={this.colSpan}>
                    用户种类
                </Col>
                <Col className="gutter-row" span={this.colSpan}>
                    {userInfo && Enums.UserKind[userInfo.UserKind]}
                </Col>

                <Col className="label" span={this.colSpan}>
                    用户类型
                </Col>
                <Col className="gutter-row" span={this.colSpan}>
                    {userInfo && Enums.UserType[userInfo.UserType]}
                </Col>

                <Col className="label" span={this.colSpan}>
                    返点
                </Col>
                <Col className="gutter-row" span={this.colSpan}>
                    {userInfo && userInfo.Backpct}
                </Col>

                <Col className="label" span={this.colSpan}>
                    状态
                </Col>
                <Col className="gutter-row" span={this.colSpan}>
                    {userInfo && RenderHelper.render_status(Enums.UserStatus)(userInfo.Status)}
                </Col>

                <Col className="label" span={this.colSpan}>
                    注册时间
                </Col>
                <Col className="gutter-row" span={this.colSpan}>
                    {userInfo && RenderHelper.render_time(userInfo.CreateTime)}
                </Col>

                <Col className="label" span={this.colSpan}>
                    真实名称
                </Col>
                <Col className="gutter-row" span={this.colSpan}>
                    {userInfo && userInfo.Realname}
                </Col>

                <Col className="label" span={this.colSpan}>
                    手机号码
                </Col>
                <Col className="gutter-row" span={this.colSpan}>
                    {userInfo && userInfo.Mobile || 'N/A'}
                </Col>

                <Col className="label" span={this.colSpan}>
                    QQ号码
                </Col>
                <Col className="gutter-row" span={this.colSpan}>
                    {userInfo && userInfo.QQ || 'N/A'}
                </Col>
            </Row>
        );
    }

    renderBalance() {
        const { userInfo, billflow, isFetching } = this.props;
        return (
            <Row>
                <Col className="label" span={this.colSpan}>
                    余额
                </Col>
                <Col className="gutter-row" span={this.colSpan}>
                    {userInfo && RenderHelper.money_format(userInfo.Balance)}
                </Col>

                <Col className="label" span={this.colSpan}>
                    总充值
                </Col>
                <Col className="gutter-row" span={this.colSpan}>
                    {userInfo && RenderHelper.money_format(userInfo.Summary.Deposit)}
                </Col>

                <Col className="label" span={this.colSpan}>
                    总转入
                </Col>
                <Col className="gutter-row" span={this.colSpan}>
                    {userInfo && RenderHelper.money_format(userInfo.Summary.TransferIn)}
                </Col>

                <Col className="label" span={this.colSpan}>
                    冻结
                </Col>
                <Col className="gutter-row" span={this.colSpan}>
                    {userInfo && RenderHelper.money_format(userInfo.Freeze)}
                </Col>

                <Col className="label" span={this.colSpan}>
                    总提现
                </Col>
                <Col className="gutter-row" span={this.colSpan}>
                    {userInfo && RenderHelper.money_format(userInfo.Summary.Withdraw)}
                </Col>

                <Col className="label" span={this.colSpan}>
                    总转出
                </Col>
                <Col className="gutter-row" span={this.colSpan}>
                    {userInfo && RenderHelper.money_format(userInfo.Summary.TransferOut)}
                </Col>

                <Col className="label" span={this.colSpan}>
                    总投注
                </Col>
                <Col className="gutter-row" span={this.colSpan}>
                    {userInfo && RenderHelper.money_format(userInfo.Summary.TotalBet)}
                </Col>


                <Col className="label" span={this.colSpan}>
                    佣金
                </Col>
                <Col className="gutter-row" span={this.colSpan}>
                    {userInfo && RenderHelper.money_format(userInfo.Summary.Commission)}
                </Col>

                <Col className="label" span={this.colSpan}>
                    奖金
                </Col>
                <Col className="gutter-row" span={this.colSpan}>
                    {userInfo && RenderHelper.money_format(userInfo.Summary.Bonus)}
                </Col>

                <Col className="label" span={this.colSpan}>
                    有效总投注
                </Col>
                <Col className="gutter-row" span={this.colSpan}>
                    {userInfo && RenderHelper.money_format(userInfo.Summary.ValidBet)}
                </Col>

                <Col className="label" span={this.colSpan}>
                    盈亏
                </Col>
                <Col className="gutter-row" span={this.colSpan}>
                    {userInfo && RenderHelper.money_color(userInfo.Summary.Profit)}
                </Col>

                <Col className="label" span={this.colSpan}>
                    总盈亏
                </Col>
                <Col className="gutter-row" span={this.colSpan}>
                    {userInfo && RenderHelper.money_color(userInfo.Summary.Profit + userInfo.Summary.Commission + userInfo.Summary.Bonus)}
                </Col>
            </Row>
        );
    }

    renderBank() {

        const columns = [{ title: '#', render(text, record, index) { return index + 1; } },
        { title: '银行', dataIndex: 'BankName' },
        { title: "户名", dataIndex: "AccountName" },
        { title: "账号", dataIndex: "AccountNo" },
        { title: "分行", dataIndex: "BranchName" },
        {
            title: "状态", dataIndex: "Status", render(status) {
                const colors = {
                    0: "blue",
                    1: "red"
                }
                let className = `status color-${colors[status]}`;
                return (
                    <span>
                        <b className={className}></b>
                        {Enums.CommonStatus[status]}
                    </span>
                );
            }
        },
        { title: '时间', dataIndex: 'CreateTime', render: RenderHelper.render_time }
        ];

        let bank = this.props.bank.user || {};

        const self = this;
        const pagination = {
            total: bank.count || 0,
            onChange(current) {
                self.pageIndex = current;
                self.queryBank();
            },
        };

        return <Table columns={columns} dataSource={bank.data || []} loading={this.props.bank.isFetching} pagination={pagination} rowKey="Id" />

    }

}

export default connect(state => ({
    isFetching: state.user.isFetching,
    isBFFetching: state.bill.isFetching,
    userInfo: state.user.UserInfo,
    billflow: state.bill.Sum,
    bank: state.bank,
}))(UserInfo);