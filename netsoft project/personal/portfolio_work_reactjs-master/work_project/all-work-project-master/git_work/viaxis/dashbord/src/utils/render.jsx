import React from 'react';
import ReactDOM from 'react-dom';
import { Button, Modal, Badge, DatePicker } from 'antd';
import moment from 'moment';
import { dateFormat, milliFormat } from "./string";
import { isEmptyOrNull } from './common';

export const render_index = (text, record, index) => index + 1;

export const render_time = (time) => {
    // if (!time || time == 0)
    //     return '____-__-__ __:__:__';
    // return dateFormat(new Date(time * 1000));
    if (isEmptyOrNull(time))
        return '____-__-__ __:__:__';
    return time;
}

/**
 * 显示状态
 * @param  {[type]} statusEnum 状态枚举
 * @param  {[type]} status     状态
 * @return {[type]}            [description]
 */
export const render_status = (statusEnum) => {
    return (status) => <Badge status={statusEnum[status].status} text={statusEnum[status].toString()} />;
}

export const money_format = (val) => {
    return milliFormat(val.toFixed(2));
}

export const money_color = (val) => {
    var className = val < 0 ? "red" : "green";
    return <span className={className}>{milliFormat(val)}</span>
}

export const render_percent = (digits = 2) => {
    return (val) => (val * 100).toFixed(digits) + "%";
}

export const render_enum = (enumType) => {
    return (val) => enumType[val] || "未知";
}

export const rangeDatePick = (options) => {
    const beforeYesterday = moment().add(-2, "days");
    const yesterday = moment().add(-1, 'days');
    const today = moment();
    const thisMonth1th = moment().startOf('month');
    const lastMonth1th = moment().startOf('month').add(-1, 'months');
    const ranges = {
        '上月': [lastMonth1th, lastMonth1th.clone().endOf('month')],
        '上周': [moment().startOf('week').add(-1, "weeks"), moment().endOf('week').add(-1, "weeks")],
        '前天': [beforeYesterday, beforeYesterday.clone()],
        '昨天': [yesterday, yesterday.clone()],
        '今天': [today],
        '本周': [moment().startOf('week'), moment().endOf('week')],
        '本月': [thisMonth1th, moment().endOf('month')],
    };
    return <DatePicker.RangePicker {...options}
        ranges={ranges} />
}

export function dialog(title, body, options) {

    let div = document.createElement('div');
    document.body.appendChild(div);

    function close() {
        const unmountResult = ReactDOM.unmountComponentAtNode(div);
        if (unmountResult && div.parentNode) {
            div.parentNode.removeChild(div);
        }
    }

    var cloneBody = React.cloneElement(body, { destroy: close });

    ReactDOM.render(
        <Modal title={title} {...options} footer='' visible onCancel={close}>
            {cloneBody}
        </Modal>
        , div);

    return {
        destroy: close,
    };

}

export function modal(body) {
    let div = document.createElement('div');
    document.body.appendChild(div);

    function close() {
        const unmountResult = ReactDOM.unmountComponentAtNode(div);
        if (unmountResult && div.parentNode) {
            div.parentNode.removeChild(div);
        }
    }

    var cloneBody = React.cloneElement(body, { destroy: close });

    ReactDOM.render(
        <div>{cloneBody}</div>
        , div);

    return {
        destroy: close,
    };
}
