import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { message, Row, Col, Table, Tag, Button, Modal, Form, Input, Select,DatePicker } from 'antd';

import BasicComponent from "./common/BasicComponent";
import SearchForm from "./common/SearchForm";

import { queryConfigInfo, addConfigInfo, updateConfig } from '../actions/config_info';
import { render_index, render_time, render_status } from "../utils/render";
import { isEmptyObject } from "../utils/string";

const createForm = Form.create;
const FormItem = Form.Item;
const Option = Select.Option;
const RangePicker = DatePicker.RangePicker;
const Message = message;

class SearchFormComponent extends BasicComponent{
    constructor(props){
        super(props)
        this.state = { name: "" };
    }

    handleChange(e){
        var state = {};
        state[e.target.name] = e.target.value;
        this.setState(state);
    }

    handleClick(){
        this.props.onSearchClick(this.state);
    }

    handleReset(){
        var state = { name: "" };
        this.setState(state);
    }

    render(){
        return (
            <Form horizontal className="ant-advanced-search-form" ref="query_form">
                <Row gutter={16}>
                    <Col sm={6}>
                        <FormItem label="名称" labelCol={{ span: 10 }} wrapperCol={{ span: 14 }}>
                            <Input placeholder="名称" size="default" name="name" value={this.state.name} onChange={this.handleChange.bind(this)} />
                        </FormItem>
                    </Col>
                    <Col span={9} offset={9} style={{ textAlign: 'right' }}>
                        <Button type="primary" icon="search" onClick={this.handleClick.bind(this)}>搜索</Button>
                        <Button type="ghost" icon="reload" onClick={this.handleReset.bind(this)}>清空</Button>
                  </Col>
                </Row>
            </Form>
        );
    }
}

class ConfigInfo extends BasicComponent {

	constructor(props) {
		super(props);
		this.query={};
		this.state = {
            loading: false,
            popup: false,
            popupTitle: "",
            edit_item: {}
        };
	}

    componentWillMount() {
        const { data } = this.props;
        this.queryData();
    }

    queryData(){
        this.props.dispatch(queryConfigInfo(this.query)).then(act => {
            if (act.error)
                Message.error(act.error);
        });
    }

    addConfig(){
        this.setState({
            popup: true,
            popupTitle: "添加配置",
            edit_item: {}
        });
    }

    updateConfig(config){
        this.setState({
            popup: true,
            popupTitle: "编辑" + config.ConfigName,
            edit_item: config
        });
    }

    handleSubmit(e){
        const { dispatch } = this.props;
        e.preventDefault();
        this.props.form.validateFields((errors, values) => {
            if (errors) return;
            // edit
            if(!isEmptyObject(this.state.edit_item)){
                this.setState({loading:true});
                dispatch(updateConfig(this.state.edit_item.Id,values.name,values.value,values.desc)).then(act=>{
                    this.setState({edit_item:{},loading:false});
                    if(act.error){
                        Message.error(act.error);
                    }else{
                        Message.success("修改成功");
                        this.props.form.resetFields();
                        this.setState({popup:false});
                    }
                });
            }else{
                this.setState({loading:true});
                dispatch(addConfigInfo(values.name,values.value,values.desc)).then(act=>{
                    this.setState({edit_item:{},loading:false});
                    if(act.error){
                        Message.error(act.error);
                    }else{
                        Message.success("添加成功");
                        this.props.form.resetFields();
                        this.setState({popup:false});
                    }
                });
            }
        });
    }

    handleReset(e) {
        e.preventDefault();
        this.props.form.resetFields();
    }

    hideModal(){
        this.setState({popup:false});
    }

    tableColumns(){
        const { data } = this.props;
        const self = this;
    	const columns = [{title: '#',render:render_index}, 
    		{title: '名称',dataIndex: 'ConfigName'}, 
            {title: "配置值", dataIndex: "ConfigValue"},
            {title: "说明", dataIndex:"Desc"},
            {title: "创建时间", dataIndex:"CreateTime", render: render_time},
            {title: "修改时间" , dataIndex: "UpdateTime", render: render_time},
            {title: "操作", render(ci){
                return <Button type="primary" onClick={()=>self.updateConfig(ci)}>编辑</Button>
            }}];
        return columns;
    }

    onSearchClick(query){
        this.query = query;
        this.queryData();
    }

    onResetClick(){
        this.query={};
    }


    render() {
    	const { data } = this.props;
        let columns = this.tableColumns();

    	return (
    		<div>
                {this.renderSearch()}
                <div className="search-result-list">
        			<Table columns={columns} dataSource={data.configs} loading={data.isFetching} rowKey="Id"/>
                </div>
                {this.render_modal()}
    		</div>
    	);
    }

    renderSearch(){
        const formItemLayout = {
            labelCol: { span: 8 },
            wrapperCol: { span: 16 }
        };

        return (
            <SearchForm showAddBtn onAdd={this.addConfig.bind(this)} onSearch={this.onSearchClick.bind(this)} onReset={this.onResetClick.bind(this)}>
                <name label="名称" {...formItemLayout}>
                    <Input />
                </name>
            </SearchForm>
        );
    }

    render_modal(){
        const self = this;
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: { span: 7 },
            wrapperCol: { span: 12 },
        };
        return (
            <Modal title={this.state.popupTitle} visible={this.state.popup} onCancel={this.hideModal.bind(this)}
                footer={[
                    <Button key="back" type="ghost" size="large" onClick={this.hideModal.bind(this)}>取消</Button>,
                    <Button key="submit" type="primary" size="large" loading={this.state.loading} onClick={this.handleSubmit.bind(this)}>
                        提交
                    </Button>,
                ]}>
                    <Form horizontal>
                        <FormItem {...formItemLayout} label="配置名">
                            {getFieldDecorator('name', {
                                initialValue: this.state.edit_item.ConfigName || "",
                                rules: [{
                                    required: true,
                                    message: '请输入配置名称'
                                }]
                            })(
                                <Input type="name" autoComplete="off" />
                            )}
                        </FormItem>
                        <FormItem {...formItemLayout} label="配置值">
                            {getFieldDecorator('value', {
                                initialValue: this.state.edit_item.ConfigValue || "",
                                rules: [{
                                    required: true,
                                    message: '请输入配置值'
                                }]
                            })(
                                <Input type="text" autoComplete="off" />
                            )}
                        </FormItem>
                        <FormItem {...formItemLayout} label="配置说明">
                            {getFieldDecorator('desc', {
                                initialValue: this.state.edit_item.Desc || "",
                                rules: [{
                                    required: true,
                                    message: '请输入配置说明'
                                }]
                            })(
                                <Input type="textarea" autoComplete="off" />
                            )}
                        </FormItem>
                  </Form>
            </Modal>
        );
    }
}


export default connect(state => ({
	data: state.configinfo,
}))(Form.create()(ConfigInfo));
