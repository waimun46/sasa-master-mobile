import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { message, Badge, Table, Tag, Button, Modal, Input, Select, DatePicker } from 'antd';

import EditorModal from "../common/EditorModal";

import * as GameAction from '../../actions/game';
import { dateFormat, isEmptyObject } from "../../utils/string";
import * as RenderHelper from "../../utils/render";
import { CommonStatus, CommonStyleStatus } from "../../app_enums";

const Option = Select.Option;
const Message = message;

class GameTypeKind extends Component {

    constructor(props) {
        super(props);
        this.pageIndex = 1;
        this.query = {};
        this.state = {
            editItem: null,
            submiting: false
        };
    }

    /* ---------------------------- 系统函数 ------------------------------*/

    componentDidMount() {
        this.queryData();
    }

    /* ---------------------------- 自定义函数 ------------------------------*/

    queryData() {
        this.props.dispatch(GameAction.queryGameTypeKind(this.query)).then(act => {
            if (act.error)
                Message.error(act.error);
        });
    }

    addKind() {
        this.setState({
            editItem: {}
        });
    }

    updateKind(kind) {
        this.setState({
            editItem: kind
        });
    }

    /* ---------------------------- Events ------------------------------*/

    handleSubmit(values) {
        const { dispatch } = this.props;
        this.setState({ submiting: true });
        if (isEmptyObject(this.state.editItem)) {
            dispatch(GameAction.addGameTypeKind(values.name, values.weight)).then(act => {
                this.setState({ submiting: false });

                if (act.error || act.response.code != 0) {
                    Message.error(act.error || act.response.message);
                } else {
                    Message.success("添加成功");
                    this.setState({ editItem: null });
                    this.queryData();
                }
            });
        } else {
            dispatch(GameAction.updateGameTypeKind(this.state.editItem.Id, values.name, values.weight, values.status)).then(act => {
                this.setState({ submiting: false });
                if (act.error || act.response.code != 0) {
                    Message.error(act.error || act.response.message);
                } else {
                    Message.success("修改成功");
                    this.setState({ editItem: null });
                    this.queryData();
                }
            });
        }
    }

    /* ---------------------------- Render ------------------------------*/

    tableColumns() {
        const self = this;
        const columns = [{ title: '#', render(text, record, index) { return index + 1; } },
        { title: '名称', dataIndex: 'KindName' },
        { title: '权重', dataIndex: 'Weight' },
        { title: "状态", dataIndex: "Status", render: RenderHelper.render_status(CommonStyleStatus) },
        { title: '时间', dataIndex: 'CreateTime' },
        {
            title: "操作", render(type) {
                return <Button type="primary" onClick={() => self.updateKind(type)}>编辑</Button>
            }
        }];

        return columns;
    }

    render() {
        const kinds = this.props.Kinds || [];
        let columns = this.tableColumns();

        const pagination = {
            total: kinds.length
        };
        return (
            <div>
                <div style={{ marginBottom: 16 }}>
                    <Button type="primary" onClick={this.addKind.bind(this)}>添加</Button>
                </div>
                <Table columns={columns} dataSource={kinds} loading={this.props.isFetching} pagination={pagination} />
                {this.render_modal()}
            </div>
        );
    }

    render_modal() {

        const { editItem } = this.state;
        if (!editItem) return;

        const title = isEmptyObject(editItem) ? "添加游戏种类" : "编辑" + editItem.KindName;

        return (
            <EditorModal title={title} visible={true}
                submiting={this.state.submiting}
                onSubmit={this.handleSubmit.bind(this)}
                onClose={it => this.setState({ editItem: null })}>
                <name label="玩法种类名称" options={{
                    initialValue: editItem.KindName || "",
                    rules: [{
                        required: true,
                        message: '请输入玩法种类名称'
                    }]
                }}>
                    <Input type="name" autoComplete="off" />
                </name>
                <weight label="权重" options={{
                    initialValue: (editItem.Weight || '0').toString(),
                    rules: [
                        { required: true, message: '请输入权重' }
                    ]
                }}>
                    <Input autoComplete="off" />
                </weight>
                {!isEmptyObject(editItem) &&
                    <status label="状态" options={{
                        initialValue: (editItem.Status || 0).toString(),
                        rules: [{
                            required: true,
                            message: '请选择状态'
                        }]
                    }}>
                        <Select>
                            {Object.keys(CommonStatus).map(it => {
                                return <Option key={it} value={it}>{CommonStatus[it]}</Option>;
                            })}
                        </Select>
                    </status>}
            </EditorModal>
        );
    }
}

export default connect(state => ({
    isFetching: state.game.isFetching,
    Kinds: state.game.GameTypeKind,
}))(GameTypeKind);
