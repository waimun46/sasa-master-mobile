import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { message, Row, Col, Table, Tag, Button, Modal, Form, Input, Select,DatePicker } from 'antd';
import { queryRebateLevel, updateRebateLevel } from '../../actions/user';
import { render_index, render_time, render_milli_format,render_percent } from "../../utils/render";
import { isEmptyObject } from "../../utils/string";
import { CommonStatus } from "../../app_enums";

const FormItem = Form.Item;
const Option = Select.Option;
const RangePicker = DatePicker.RangePicker;
const Message = message;

class RebateLevel extends Component {

	constructor(props) {
		super(props);
		this.query={};
        this.state = {
            loading: false,
            popup: false,
            popupTitle: "",
            edit_item: {}
        };
	}

    componentDidMount() {
        this.queryData();
    }

    queryData(){
		this.props.dispatch(queryRebateLevel(this.query)).then(act => {
			if (act.error)
				Message.error(act.error);
		});
    }

    onUpdateClick(level){
        this.setState({
            popup: true,
            popupTitle: "编辑等级" + level.Level,
            edit_item: level
        });
    }

    handleSubmit(e){
        const { dispatch } = this.props;
        e.preventDefault();
        this.props.form.validateFields((errors, values) => {
            if (errors) return;
            // edit
            if(!isEmptyObject(this.state.edit_item)){
                dispatch(updateRebateLevel(this.state.edit_item.Level, values.turnover, values.backpct)).then(act=>{
                    this.setState({edit_item:{},loading:false});
                    if(act.error){
                        Message.error(act.error);
                    }else{
                        Message.success("修改成功");
                        this.props.form.resetFields();
                        this.setState({popup:false});
                    }
                });
            }
        });
    }

    handleReset(e) {
        e.preventDefault();
        this.props.form.resetFields();
    }

    hideModal(){
        this.setState({popup:false});
    }

    tableColumns(){
        const self = this;
    	const columns = [
            {title: "等级", dataIndex: "Level"},
            {title: "投注额", dataIndex: "Turnover", render: render_milli_format},
            {title: "返点", dataIndex:"Backpct", render: render_percent()},
            {titel: "编辑", render(level){
                return <Button type="primary" onClick={()=>self.onUpdateClick(level)}>编辑</Button>
            }}];
        return columns;
    }

    render() {
    	const { data } = this.props;
        let levels = data.rebate_levels || [];

        let columns = this.tableColumns();

    	return (
    		<div>
    			<Table columns={columns} dataSource={levels} loading={data.isFetching} rowKey='Level'/>
                {this.render_modal()}
    		</div>
    	);
    }

    render_modal(){
        const self = this;
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: { span: 7 },
            wrapperCol: { span: 12 },
        };
        return (
            <Modal title={this.state.popupTitle} visible={this.state.popup} onCancel={this.hideModal.bind(this)}
                footer={[
                    <Button key="back" type="ghost" size="large" onClick={this.hideModal.bind(this)}>取消</Button>,
                    <Button key="submit" type="primary" size="large" loading={this.state.loading} onClick={this.handleSubmit.bind(this)}>
                        提交
                    </Button>,
                ]}>
                    <Form horizontal>
                        {!this.state.edit_item.Level&&<FormItem {...formItemLayout} label="等级">
                            {getFieldDecorator('level', {
                                initialValue: (this.state.edit_item.Level || "").toString(),
                                rules: [{
                                    required: true,
                                    message: '请输入等级'
                                }]
                            })(
                                <Input type="text" autoComplete="off" />
                            )}
                        </FormItem>}

                        <FormItem {...formItemLayout} label="累计投注额">
                            {getFieldDecorator('turnover', {
                                initialValue: (this.state.edit_item.Turnover || "").toString(),
                                rules: [{
                                    required: true,
                                    message: '请输入累计投注额'
                                }]
                            })(
                                <Input type="text" autoComplete="off" />
                            )}
                        </FormItem>

                        <FormItem {...formItemLayout} label="返点">
                            {getFieldDecorator('backpct', {
                                initialValue: (this.state.edit_item.Backpct || "").toString(),
                                rules: [{
                                    required: true,
                                    message: '请输入返点'
                                }]
                            })(
                                <Input type="text" autoComplete="off" />
                            )}
                        </FormItem>
                  </Form>
            </Modal>
        );
    }
}


export default connect(state => ({
	data: state.user,
}))(Form.create()(RebateLevel));
