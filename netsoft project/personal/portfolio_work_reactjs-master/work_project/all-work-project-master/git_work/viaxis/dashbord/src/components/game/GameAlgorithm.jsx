import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { message, Row, Col, Table, Tag, Button, Modal, Form, Input, Select, DatePicker } from 'antd';

import EditorModal from "../common/EditorModal";

import * as GameAction from '../../actions/game';
import { render_index, render_time, render_status } from "../../utils/render";
import * as Utils from "../../utils/common";
import { CommonStatus } from "../../app_enums";

const Option = Select.Option;
const RangePicker = DatePicker.RangePicker;
const Message = message;

class GameAlgorithm extends Component {

    constructor(props) {
        super(props);
        this.query = {};
        this.state = {
            editItem: null,
            submiting: false
        };
    }

    /* ---------------------------- 系统函数 ------------------------------ */

    componentDidMount() {
        this.queryData();
    }

    /* ---------------------------- 自定义函数 ------------------------------ */

    queryData() {
        this.props.dispatch(GameAction.queryGameAlgorithm(this.query)).then(act => {
            if (act.error)
                Message.error(act.error);
        });
    }

    handleSubmit(values) {

        this.setState({ submiting: true });
        this.props.dispatch(GameAction.updateGameAlgorithm(this.state.editItem.Id,
            values.name,
            values.desc,
            values.status))
            .then(act => {
                this.setState({ submiting: false });
                if (act.error || act.response.code != 0) {
                    Message.error(act.error || act.response.message);
                } else {
                    Message.success("修改成功");
                    this.setState({ editItem: null });
                }
            });
    }

    /* ---------------------------- 自定义函数 ------------------------------ */

    /* ---------------------------- Renders ------------------------------ */

    tableColumns() {
        const self = this;
        const columns = [{ title: '#', render: render_index },
        { title: "Id", dataIndex: "Id" },
        { title: "名称", dataIndex: "Name" },
        { title: "说明", dataIndex: "Description" },
        {
            title: "状态", dataIndex: "Status", render(status) {
                const colors = {
                    0: "blue",
                    1: "red"
                }
                return render_status(colors[status], CommonStatus[status]);
            },
        },
        { title: "创建时间", dataIndex: "CreateTime", render: render_time, sorter: (a, b) => a.TimeStamp - b.TimeStamp },
        {
            title: "操作", render(item) {
                return <Button type="primary" onClick={() => self.setState({ editItem: item })}>编辑</Button>
            }
        }];

        return columns;
    }

    render() {

        const algorithms = this.props.Algorithms || [];

        let columns = this.tableColumns();

        return (
            <div>
                <Table columns={columns} dataSource={algorithms} loading={this.props.isFetching} />
                {this.renderEditModal()}
            </div>
        );
    }

    renderEditModal() {
        const { editItem } = this.state;
        if (!editItem) return;

        const title = Utils.isEmptyObject(editItem) ? "添加开奖算法" : "编辑" + editItem.Name;

        return (
            <EditorModal title={title} visible={true}
                submiting={this.state.submiting}
                onSubmit={this.handleSubmit.bind(this)}
                onClose={it => this.setState({ editItem: null })}>
                <name label="名称" options={{
                    initialValue: editItem.Name || "",
                    rules: [{
                        required: true,
                        message: '请输入算法名称'
                    }]
                }}>
                    <Input autoComplete="off" />
                </name>
                <desc label="说明" options={{
                    initialValue: editItem.Description || "",
                    rules: [{
                        required: true,
                        message: '请输入算法说明'
                    }]
                }}>
                    <Input type="textarea" autoComplete="off" />
                </desc>
                {!Utils.isEmptyObject(editItem) &&
                    <status label="状态" options={{
                        initialValue: (editItem.Status || 0).toString(),
                        rules: [{
                            required: true,
                            message: '请选择算法状态'
                        }]
                    }}>
                        <Select>
                            {Object.keys(CommonStatus).map(it => {
                                return <Option key={it} value={it}>{CommonStatus[it]}</Option>;
                            })}
                        </Select>
                    </status>
                }
            </EditorModal>
        );
    }
}


export default connect(state => ({
    isFetching: state.game.isFetching,
    Algorithms: state.game.Algorithm,
}))(GameAlgorithm);
