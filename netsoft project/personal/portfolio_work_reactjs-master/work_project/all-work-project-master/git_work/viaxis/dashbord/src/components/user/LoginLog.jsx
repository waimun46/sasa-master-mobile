import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { message, Row, Col, Table, Tag, Button, Modal, Form, Input, Select, DatePicker } from 'antd';

import BasicComponent from "../common/BasicComponent";
import SearchForm from "../common/SearchForm";

import { queryLoginLog } from '../../actions/user';
import { dateFormat } from "../../utils/string";
import moment from 'moment';

const createForm = Form.create;
const FormItem = Form.Item;
const Option = Select.Option;
const RangePicker = DatePicker.RangePicker;
const Message = message;

class LoginLog extends BasicComponent {

    constructor(props) {
        super(props)
        this.user_id = props.params.userID;
        this.query = {};
    }

    /* -------------------------- Sys ------------------------------ */

    componentWillMount() {
        this.queryData();
    }

    /* -------------------------- Functions ------------------------------ */

    queryData() {
        this.props.dispatch(queryLoginLog(this.user_id, this.pageIndex, this.query)).then(act => {
            if (act.error)
                Message.error(act.error);
        });
    }

    /* -------------------------- Events ------------------------------ */

    onSearchClick(query) {
        this.query = query;
        this.pageIndex = 1;
        this.queryData();
    }

    onResetClick() {
        this.query = {};
    }

    /* -------------------------- Renders ------------------------------ */

    render() {
        const { user } = this.props;
        let columns = this.tableColumns();

        let logs = user.loginlog || {};

        const self = this;
        const pagination = {
            current: logs.pageIndex || 1,
            total: logs.count || 0,
            onChange(current) {
                self.pageIndex = current;
                self.queryData();
            },
        };
        const rowKey = function (log) {
            return log.CreateTime
        }
        return (
            <div>
                {this.renderSearch()}
                <div className="search-result-list">
                    <Table columns={columns} dataSource={logs.data || []} loading={user.isFetching} pagination={pagination} rowKey={rowKey} />
                </div>
            </div>
        );
    }

    renderSearch() {
        const formItemLayout = {
            labelCol: { span: 8 },
            wrapperCol: { span: 16 }
        };

        return (
            <SearchForm onSearch={this.onSearchClick.bind(this)} onReset={this.onResetClick.bind(this)}>
                <email label="邮箱" {...formItemLayout}>
                    <Input />
                </email>
                <area label="区域" {...formItemLayout}>
                    <Input />
                </area>
                <ip label="IP" {...formItemLayout}>
                    <Input />
                </ip>
                <create_time label="时间" {...formItemLayout} format={(times) => Utils.dateRangeFormat(times)}>
                    <DatePicker.RangePicker />
                </create_time>
            </SearchForm>
        );
    }

    tableColumns() {
        const columns = [{ title: '#', render(text, record, index) { return index + 1; } },
        { title: '邮箱', dataIndex: 'Email' },
        { title: "IP", dataIndex: "IP" },
        { title: "区域", dataIndex: "Area" },
        { title: '时间', dataIndex: 'CreateTime' }];

        return columns;
    }
}

export default connect(state => ({
    user: state.user
}))(LoginLog);
