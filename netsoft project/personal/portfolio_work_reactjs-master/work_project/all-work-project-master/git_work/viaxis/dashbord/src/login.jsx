require('../pages/css/login.css');

import React from 'react';
import ReactDOM from 'react-dom';
import { browserHistory } from 'react-router';
import { message } from 'antd';
import auth from "./utils/auth";


class Login extends React.Component {

	constructor(props) {
		super(props);
	}

	handleSubmit(e) {
		e.preventDefault();
		if (!!sessionStorage.token)
			auth.removestorage();
		var username = document.getElementById("username").value;
		var password = document.getElementById("password").value;
		if (username == "" || username == null || username == undefined) {
			message.error("请输入用户名");
			return false;
		}
		if (password == "" || password == null || password == undefined) {
			message.error("请输入密码");
			return false;
		}
		auth.login(username, password, (result, error) => {
			if (result) {
				message.success("登陆成功");
				window.location.replace("/");
			}
			else
				message.error("登陆失败:" + error);
		});
		return false;
	}

	render() {
		return (
			<div className="form">
				<form onSubmit={this.handleSubmit.bind(this)}>
					<div className="forceColor"></div>
					<div className="topbar">
						<div className="spanColor"></div>
						<input type="input" className="input" id="username" placeholder="Username" autoComplete="off" />
					</div>
					<div className="topbar">
						<div className="spanColor"></div>
						<input type="password" className="input" id="password" placeholder="Password" />
					</div>
					<button className="submit" id="submit">Login</button>
				</form>
			</div>
		);
	}
}

export default Login;

// if(auth.isLogged())
//     browserHistory.push("/");

// ReactDOM.render(<Login/>, 
//                 document.getElementById('app'));

// if (module.hot)
//     module.hot.accept()