import fetch from 'isomorphic-fetch'
import Config from '../config';
import Auth from "./auth";

/*------------ Action -----------*/
export function POST(url, body, headers = {}) {
    let token = Auth.getAuthToken();
    return () => fetch(Config.API + url, {
        credentials: 'include',
        method: "POST",
        body: body,
        headers: Object.assign({}, {
            "Content-Type": "application/json",
            "Authorization": token
        }, headers)
    });
}

export function GET(url, headers = {}) {
    let token = Auth.getAuthToken();
    return () => fetch(Config.API + url, {
        credentials: 'include',
        method: "GET",
        headers: Object.assign({}, {
            "Authorization": token
        }, headers)
    });
}

/**
 * 检查一个字典是否为空
 * @param  {[type]}  obj [description]
 * @return {Boolean}     [description]
 */
export function isEmptyObject(obj) {
    if (!obj) return true;
    for (var n in obj) {
        return false
    }
    return true;
}

/**
 * 检查一个实例是否字典
 * @param  {[type]}  v [description]
 * @return {Boolean}   [description]
 */
export function isDict(v) {
    return typeof v === 'object' && v !== null && !(v instanceof Array) && !(v instanceof Date);
}

/* ****************************************************************************************************************************** *
 * ******************************************************* Format Helper ******************************************************** *
 * ****************************************************************************************************************************** */

/**
 * 千位符号
 * @param  {Function} () [description]
 * @return {[type]}      [description]
 */
export const milliFormat = (() => {
    const DIGIT_PATTERN = /(^|\s)\d+(?=\.?\d*($|\s))/g
    const MILLI_PATTERN = /(?=(?!\b)(\d{3})+$)/g

    return (input, digits = 2) => input && input.toFixed(digits)
        .replace(DIGIT_PATTERN, (m) => m.replace(MILLI_PATTERN, ','))
})()


/**
 * 将日期区间转换为字符串模式
 * @param  {[Moment]}  dateRange 时间区间
 * @param  {Boolean} accurate  是否精准转换(不做任何其他干涉)
 * @param  {String}  format    时间格式
 * @return {[type]}            [description]
 */
export function dateRangeFormat(dateRange, accurate = false, format = "YYYY-MM-DD HH:mm:ss") {
    if (!Array.isArray(dateRange) || dateRange.length != 2)
        return null;

    // 如果不是精准模式，则将日期设置为开始和结束时间。
    if (!accurate) {
        dateRange[0].set('hour', 0).set('minute', 0).set('second', 0);
        dateRange[1].set('hour', 23).set('minute', 59).set('second', 59);
    }

    var data = [];
    data[0] = dateRange[0].format(format);
    data[1] = dateRange[1].format(format);
    return data;
}




/* ****************************************************************************************************************************** */
/* ****************************************************** Time Helper *********************************************************** */
/* ****************************************************************************************************************************** */

/**
 * 校验服务器时间。
 * 此方法用于获取服务器时间，并在此时间上补上客户端与服务端之间的延迟。
 * @param {float} server_unit_timestamp      [description]
 * @param {float} client_send_unix_timestamp [description]
 */
export function CalibrateServerTime(server_unit_timestamp, client_send_unix_timestamp) {
    var now = new Date();
    var send_time = new Date(client_send_unix_timestamp * 1000);
    var server_time = new Date(server_unit_timestamp * 1000);
    server_time.setMilliseconds(server_time.getMilliseconds() + (now - send_time) / 2);
    return server_time;
}

/**
 * [SaveServerTimeDiff description]
 * @param {[type]} server_time [description]
 */
export function SaveServerTimeDiff(server_time) {
    var store = sessionStorage || window;

    var diffs = [];
    if (store.ServerClientTimeDiff)
        diffs = store.ServerClientTimeDiff.split(",");

    var diff = server_time.getTime() - new Date().getTime();

    diffs = [diff, ...diffs];
    // 保存十个已经足够
    store.ServerClientTimeDiff = diffs.slice(0, 10);
}

/**
 * 获取服务器时间戳
 */
export function GetServerTimeStamp() {
    var store = sessionStorage || window;
    var diffs = [];
    if (store.ServerClientTimeDiff)
        diffs = store.ServerClientTimeDiff.split(",").map(it => parseInt(it));

    if (!diffs || diffs.length == 0) return null;

    var ave = parseInt(diffs.reduce((a, b) => a + b) / diffs.length);

    return parseInt((new Date().getTime() + ave) / 1000);
}

/**
 * 将日期转换成unix timestamp 时间戳
 * @param  {[type]} time [description]
 * @return {[type]}      [description]
 */
export function unixtimestampFromDate(time) {
    return (time || new Date()).getTime() / 1000;
}

/**
 * 将unix timestamp 时间戳转换为 Date对象
 * @param  {[type]} timestamp [description]
 * @return {[type]}           [description]
 */
export function unixtimestampToDate(timestamp) {
    return new Date(timestamp * 1000);
}

export function dateFormat(date, fmt = "yyyy-MM-dd HH:mm:ss") {
    var o = {
        "M+": date.getMonth() + 1, //月份         
        "d+": date.getDate(), //日         
        "h+": date.getHours() % 12 == 0 ? 12 : date.getHours() % 12, //小时         
        "H+": date.getHours(), //小时         
        "m+": date.getMinutes(), //分         
        "s+": date.getSeconds(), //秒         
        "q+": Math.floor((date.getMonth() + 3) / 3), //季度         
        "S": date.getMilliseconds() //毫秒         
    };
    var week = {
        "0": "/u65e5",
        "1": "/u4e00",
        "2": "/u4e8c",
        "3": "/u4e09",
        "4": "/u56db",
        "5": "/u4e94",
        "6": "/u516d"
    };
    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
    }
    if (/(E+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, ((RegExp.$1.length > 1) ? (RegExp.$1.length > 2 ? "/u661f/u671f" : "/u5468") : "") + week[date.getDay() + ""]);
    }
    for (var k in o) {
        if (new RegExp("(" + k + ")").test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        }
    }
    return fmt;
}



/* ****************************************************************************************************************************** *
 * ***************************************************** String Helper ********************************************************** *
 * ****************************************************************************************************************************** */


export function isString(o) {
    return typeof o === "string";
}

export function isEmptyOrNull(s) {
    return !isString(s) || s == null || s == undefined || s == "";
}

export function isNonEmptyString(s) {
    return isString(s) && s !== "";
}

/**
 * String.format
 * @param  {[type]}    str  [description]
 * @param  {...[type]} args [description]
 * @return {[type]}         [description]
 */
export function format(str, ...args) {
    return str.replace(/{(\d+)}/g, function (match, number) {
        return typeof args[number] != 'undefined'
            ? args[number]
            : match
            ;
    });
}

export function maskStart(str, showLength) {
    if (str.length <= showLength) return str;

    return pad("", str.length - showLength, "*") + str.slice(str.length - showLength);
}

export function maskEnd(str, showLength) {
    if (str.length <= showLength) return str;

    return str.slice(0, showLength) + pad("", str.length - showLength, "*");
}

export function endWith(str, end) {
    if (end == null || end == "" || str.length == 0 || end.length > str.length)
        return false;
    if (str.substring(str.length - end.length) == end)
        return true;
    else
        return false;
    return true;
}

export function startWith(str, start) {
    if (start == null || start == "" || str.length == 0 || start.length > str.length)
        return false;
    if (str.substr(0, start.length) == start)
        return true;
    else
        return false;
    return true;
}

/**
 * 字符补全
 * @param  {[type]} source    原字符串或数字
 * @param  {[type]} width     最终要求字符串长度
 * @param  {String} padSymbol 填充内容
 * @return {[type]}           [description]
 */
export function pad(source, width, padSymbol = '0') {
    source = source + '';
    return source.length >= width ? source : new Array(width - source.length + 1).join(padSymbol) + source;
}

/**
 * 字符串分割
 * @param  {[type]} o       [description]
 * @param  {[Array|String]} symbols 分割的符号
 * @return {[type]}         [description]
 */
export function split(o, symbols) {
    if (Array.isArray(symbols)) {
        var tmp_symbol = "πø˙";
        symbols.forEach(function (symbol) {
            if (symbol == tmp_symbol) return;
            var re = RegExp(symbol, "g");
            o = o.replace(re, tmp_symbol);
        });
        return o.split(tmp_symbol);
    } else
        return o.split(symbols);
}

/**
 * 获取数组中第一个
 * @param  {Array|string} array         [description]
 * @param  {[type]} default_value [description]
 * @return {[type]}               [description]
 */
export function getFirst(array, default_value) {
    if (array && isString(array))
        return array;
    else if (Array.isArray(array) && array.length > 0)
        return array[0];
    else
        return default_value;
}

/**
 * 计算字符串所占的内存字节数，默认使用UTF-8的编码方式计算，也可制定为UTF-16
 * UTF-8 是一种可变长度的 Unicode 编码格式，使用一至四个字节为每个字符编码
 * 
 * 000000 - 00007F(128个代码)      0zzzzzzz(00-7F)                             一个字节
 * 000080 - 0007FF(1920个代码)     110yyyyy(C0-DF) 10zzzzzz(80-BF)             两个字节
 * 000800 - 00D7FF 
   00E000 - 00FFFF(61440个代码)    1110xxxx(E0-EF) 10yyyyyy 10zzzzzz           三个字节
 * 010000 - 10FFFF(1048576个代码)  11110www(F0-F7) 10xxxxxx 10yyyyyy 10zzzzzz  四个字节
 * 
 * 注: Unicode在范围 D800-DFFF 中不存在任何字符
 * {@link http://zh.wikipedia.org/wiki/UTF-8}
 * 
 * UTF-16 大部分使用两个字节编码，编码超出 65535 的使用四个字节
 * 000000 - 00FFFF  两个字节
 * 010000 - 10FFFF  四个字节
 * {@link http://zh.wikipedia.org/wiki/UTF-16}
 * @param  {[type]} charset charset utf-8, utf-16
 * @return {Number}
 */
export function sizeof(str, charset) {
    var total = 0,
        charCode,
        i,
        len;
    charset = charset ? charset.toLowerCase() : '';
    if (charset === 'utf-16' || charset === 'utf16') {
        for (i = 0, len = str.length; i < len; i++) {
            charCode = str.charCodeAt(i);
            if (charCode <= 0xffff) {
                total += 2;
            } else {
                total += 4;
            }
        }
    } else {
        for (i = 0, len = str.length; i < len; i++) {
            charCode = str.charCodeAt(i);
            if (charCode <= 0x007f) {
                total += 1;
            } else if (charCode <= 0x07ff) {
                total += 2;
            } else if (charCode <= 0xffff) {
                total += 3;
            } else {
                total += 4;
            }
        }
    }
    return total;
}

/* ****************************************************************************************************************************** *
 * ******************************************************** Url Helper ********************************************************** *
 * ****************************************************************************************************************************** */


/**
 * 从当前地址中获取指定名称的 queryString
 * @param  {[type]} name [description]
 * @return {[type]}      [description]
 */
export function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

/**
 * 获取传入地址或当前地址的QueryString
 * @param  {[type]} url [description]
 * @return {dict}     [description]
 */
export function getQueryString(url) {
    if (url === undefined || url == null)
        url = location.search;
    else {
        let urlPaths = url.split("?");
        if (urlPaths.length < 2)
            return {};
        url = urlPaths[1];
    }
    var querys = location.search.replace("?", "");
    var data = querys.split("&");

    var results = {};
    for (let i = 0; i < data.length; i++) {
        var param = data[i].split("=");
        results[param[0]] = param[1];
    }
    return results;
}

//增加回发参数
export function addActionToUrl(formUrl, action /*参数名*/, value /*参数值*/, clear /*是否清空参数*/) {
    //如果Url中含有参数
    if (formUrl.indexOf("?") >= 0) {
        //清空参数
        if (clear) formUrl = formUrl.substr(0, formUrl.indexOf("?") + 1);
        //Url中含有要添加的参数对
        if (formUrl.indexOf(action) > 0) {
            //获取所有参数对值
            var queryString = formUrl.substr(formUrl.indexOf("?") + 1, formUrl.length);
            //分组
            var queryStringArr = queryString.split("&");
            //不包含参数的Url
            formUrl = formUrl.substr(0, formUrl.indexOf("?") + 1);
            var isFirst = true;
            //遍历参数对
            for (var i = 0; i < queryStringArr.length; i++) {
                //如果参数对不等于要添加的Action.则添加到Url中
                if (queryStringArr[i].indexOf(action) < 0) {
                    if (isFirst) isFirst = false;
                    else formUrl += "&";
                    formUrl += queryStringArr[i];
                }
            }
        }
        //避免只有一个参数,并且参数相同时出现的?&情况
        if (formUrl.indexOf("?") + 1 != formUrl.length) formUrl += "&";
    } else {
        formUrl += "?";
    }

    formUrl += action + "=" + value;
    return formUrl;
}


/* ****************************************************************************************************************************** *
 * ****************************************************** Array Helper ********************************************************** *
 * ****************************************************************************************************************************** */

export function range(start, end) {
    return Array.from({ length: (end - start) }, (v, k) => k + start);
}

/**
 * 将一个时间对象数组转换成字符串格式数组
 * @param  {[type]}    timeRange 时间数组
 * @param  {...[type]} names     对应的时间格式名称
 * @return {[type]}              [description]
 */
export function timeRangeToString(timeRange, ...names) {
    if (!Array.isArray(timeRange) || timeRange.length != names.length)
        return null;

    var data = {};
    for (let i = 0; i < timeRange.length; i++) {
        data[names[i]] = timeRange[i].format("YYYY-MM-DD HH:mm:ss");
    }
    return data;
}

/* ****************************************************************************************************************************** *
 * ****************************************************** Copy Clipboard ******************************************************** *
 * ****************************************************************************************************************************** */

/**
 * 复制文本到前切板
 * @param {str} textToClipboard 要复制到剪切板的文本
 * @param {Function} fnSuccess       成功回调，可为空
 * @param {Function} fnFail          失败回调，可为空
 */
export function CopyToClipboard(textToClipboard, fnSuccess, fnFail) {

    var success = true;
    if (window.clipboardData) { // Internet Explorer
        window.clipboardData.setData("Text", textToClipboard);
    } else {
        // create a temporary element for the execCommand method
        var forExecElement = CreateElementForExecCommand(textToClipboard);

        /* Select the contents of the element 
            (the execCommand for 'copy' method works on the selection) */
        SelectContent(forExecElement);

        var supported = true;

        // UniversalXPConnect privilege is required for clipboard access in Firefox
        try {
            if (window.netscape && netscape.security) {
                netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
            }

            // Copy the selected content to the clipboard
            // Works in Firefox and in Safari before version 5
            success = document.execCommand("copy", false, null);
        } catch (e) {
            success = false;
        }

        // remove the temporary element
        document.body.removeChild(forExecElement);
    }

    if (success) {
        const fn = fnSuccess || function () {
            alert("The text is on the clipboard, try to paste it!");
        };
        fn();
    } else {
        const fn = fnFail || function () {
            alert("Your browser doesn't allow clipboard access!");
        };
        fn();
    }
}

function CreateElementForExecCommand(textToClipboard) {
    var forExecElement = document.createElement("div");
    // place outside the visible area
    forExecElement.style.position = "absolute";
    forExecElement.style.left = "-10000px";
    forExecElement.style.top = "-10000px";
    // write the necessary text into the element and append to the document
    forExecElement.textContent = textToClipboard;
    document.body.appendChild(forExecElement);
    // the contentEditable mode is necessary for the  execCommand method in Firefox
    forExecElement.contentEditable = true;

    return forExecElement;
}

function SelectContent(element) {
    // first create a range
    var rangeToSelect = document.createRange();
    rangeToSelect.selectNodeContents(element);

    // select the contents
    var selection = window.getSelection();
    selection.removeAllRanges();
    selection.addRange(rangeToSelect);
}

/* ****************************************************************************************************************************** *
 * ****************************************************** Session Storage ********************************************************** *
 * ****************************************************************************************************************************** */

/**
 * 这里主要用于解决SessionStorage只作用于一个标签的问题。
 * http://blog.kazaff.me/2016/09/09/%E8%AF%91-%E5%9C%A8%E5%A4%9A%E4%B8%AA%E6%A0%87%E7%AD%BE%E9%A1%B5%E4%B9%8B%E9%97%B4%E5%85%B1%E4%BA%ABsessionStorage/
 * https://blog.guya.net/2015/06/12/sharing-sessionstorage-between-tabs-for-secure-multi-tab-authentication/
 * http://stackoverflow.com/questions/20325763/browser-sessionstorage-share-between-tabs
 */

function sessionStorageTransfer(event) {
    if (!event) {
        event = window.event;
    }
    if (!event.newValue) return;

    if (event.key == 'getSessionStorage') {
        // Some tab asked for the sessionStorage -> send it

        localStorage.setItem('sessionStorage', JSON.stringify(sessionStorage));
        localStorage.removeItem('sessionStorage');

    } else if (event.key == 'sessionStorage' && !sessionStorage.length) {
        // sessionStorage is empty -> fill it

        var data = JSON.parse(event.newValue),
            value;

        for (let key in data) {
            sessionStorage.setItem(key, data[key]);
        }

    }
};

/**
 * 在程序入口处调用此方法
 * @return {[type]} [description]
 */
export function attachTabsShareSessionStorage() {
    if (window.addEventListener) {
        window.addEventListener("storage", sessionStorageTransfer, false);
    } else {
        window.attachEvent("onstorage", sessionStorageTransfer);
    };

    if (!sessionStorage.length) {
        // 这个调用能触发目标事件，从而达到共享数据的目的
        localStorage.setItem('getSessionStorage', Date.now());
    };
}

/**
 * 获取数值的最小精度
 * @param  {[type]} val    [description]
 * @return {[type]}        [description]
 */
export function getPrecision(val) {
    var index = val.indexOf('.');
    var length = val.length - (index + 1);
    if (index > -1)
        return 1 / Math.pow(10, length);
    else
        return 1;
}