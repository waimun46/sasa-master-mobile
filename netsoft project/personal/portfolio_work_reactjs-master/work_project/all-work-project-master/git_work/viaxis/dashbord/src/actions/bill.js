import $ from "jquery";
import * as app_const from "../app_const";
import * as Utils from "../utils/common";

/* -------------------- Deposit ---------------------- */

export function deposit(email, amount, remark, txid) {
	let payload = { Email: email, Amount: amount, Remark: remark, Txid: txid };
	return {
		name: app_const.MANUAL_DEPOSIT,
		callAPI: () => Utils.POST(`/api/bill/deposit`, JSON.stringify(payload))(),
	}
}

export function completeDeposit(req_id, txid, amount, deposit_type) {
	var payload = { Txid: txid, Amount: amount, Type: deposit_type };
	return {
		name: app_const.COMPLETE_DEPOSIT,
		callAPI: () => Utils.POST(`/api/deposit/${req_id}/complete`, JSON.stringify(payload))()
	};
}

export function refuseDeposit(req_id, remark) {
	var payload = { remark };
	return {
		name: app_const.REFUSE_DEPOSIT,
		callAPI: () => Utils.POST(`/api/deposit/${req_id}/refuse`, JSON.stringify(payload))()
	};
}

export function queryDeposit(pageIndex = 1, querys = {}) {
	var queryParams = Object.assign({}, querys, {
		pageIndex: pageIndex
	});
	var queryStr = JSON.stringify(queryParams);
	return {
		name: app_const.QUERY_DEPOSIT,
		callAPI: () => Utils.POST(`/api/deposit/list`, queryStr)(),
		payload: queryParams
	}
}

export function countDeposit(querys = {}) {
	var queryParams = Object.assign({}, querys);
	var queryStr = JSON.stringify(queryParams);
	return {
		name: app_const.COUNT_DEPOSIT,
		callAPI: () => Utils.POST(`/api/deposit/count`, queryStr)(),
		payload: queryParams
	}
}

/* -------------------- Withdraw --------------------- */

export function withdraw(email, amount, remark, txid) {
	let payload = { email, amount, remark, txid };
	return {
		name: app_const.MANUAL_WITHDRAW,
		callAPI: () => Utils.POST(`/api/withdraw`, JSON.stringify(payload))()
	}
}

export function checkingWithdraw(req_id) {
	return {
		name: app_const.CHECKING_WITHDRAW,
		callAPI: () => Utils.POST(`/api/withdraw/${req_id}/checking`)()
	}
}

export function checkedWithdraw(req_id) {
	return {
		name: app_const.CHECKED_WITHDRAW,
		callAPI: () => Utils.POST(`/api/withdraw/${req_id}/checked`)()
	}
}

export function processWithdraw(req_id) {
	return {
		name: app_const.PROCESS_WITHDRAW,
		callAPI: () => Utils.POST(`/api/withdraw/${req_id}/process`)()
	}
}

export function completeWithdraw(req_id, txid) {
	var payload = { txid };
	return {
		name: app_const.COMPLETE_WITHDRAW,
		callAPI: () => Utils.POST(`/api/withdraw/${req_id}/complete`, JSON.stringify(payload))()
	}
}

export function refuseWithdraw(req_id, remark) {
	let payload = { remark };
	return {
		name: app_const.REFUSE_WITHDRAW,
		callAPI: () => Utils.POST(`/api/withdraw/${req_id}/refuse`, JSON.stringify(payload))(),
	}
}

export function queryWithdraw(pageIndex = 1, querys = {}) {
	var queryParams = Object.assign({}, querys, {
		pageIndex
	});
	var queryStr = JSON.stringify(queryParams);
	return {
		name: app_const.QUERY_WITHDRAW,
		callAPI: () => Utils.POST(`/api/withdraw/history`, queryStr)()
	}
}

/**
 * 获取当前未处理提现申请数量
 * @return {[type]} [description]
 */
export function countWithdraw() {
	return {
		name: app_const.COUNT_WITHDRAW,
		callAPI: () => Utils.POST(`/api/withdraw/count?t${new Date().getTime()}`)()
	}
}

/**
 * 转账操作
 * @param  {[type]} fromEmail [description]
 * @param  {[type]} toEmail   [description]
 * @param  {[type]} amount    [description]
 * @param  {[type]} remark    [description]
 * @return {[type]}           [description]
 */
export function transfer(fromEmail, toEmail, amount, remark) {
	let payload = { fromUser: fromEmail, toUser: toEmail, amount, remark };
	return {
		name: app_const.TRANSFER,
		callAPI: () => Utils.POST(`/api/transfer`, JSON.stringify(payload))()
	}
}

export function queryTransfer(pageIndex, querys) {
	var queryParams = Object.assign({}, querys, {
		pageIndex
	});
	var queryStr = JSON.stringify(queryParams);
	return {
		name: app_const.QUERY_TRANSFER,
		callAPI: () => Utils.POST(`/api/transfer/history`, queryStr)()
	}
}


/* ------------------------ BillFlow ------------------------- */

export function queryBillflow(pageIndex = 1, querys = {}) {
	var queryParams = Object.assign({}, querys, {
		pageIndex: pageIndex
	});
	var queryStr = JSON.stringify(queryParams);
	return {
		name: app_const.QUERY_BILLFLOW,
		callAPI: () => Utils.POST(`/api/billflow`, queryStr)(),
		payload: queryParams
	}
}

export function sumBillFlow(userId, fromTime, toTime) {
	const payload = JSON.stringify({
		user_id: userId,
		from_time: fromTime,
		to_time: toTime
	});

	const url = userId ? `/api/billflow/sum/${userId}` : `/api/billflow/sum`;
	return {
		name: app_const.SUM_BILLFLOW,
		callAPI: () => Utils.POST(url, payload)()
	}
}

export function queryUserBalance(pageIndex = 1, querys = {}) {
	var queryParams = Object.assign({}, querys, { pageIndex: pageIndex });
	var queryStr = JSON.stringify(queryParams);
	return {
		name: app_const.QUERY_USER_BALANCE,
		callAPI: () => Utils.POST(`/api/balance`, queryStr)(),
		payload: queryParams
	}
}

/* --------------------------- Dividend Scale -------------------------- */

export function list_dividend_scale() {
	return {
		name: app_const.LIST_DIVIDEND_SCALE,
		callAPI: () => Utils.POST("/api/dividend")(),
	}
}

export function add_dividend_scale(scale, turnover) {
	var payload = { scale, turnover };
	return {
		name: app_const.ADD_DIVIDEND_SCALE,
		callAPI: () => Utils.POST(`/api/dividend/add`, JSON.stringify(payload))(),
	}
}

export function update_dividend_scale(id, scale, turnover) {
	var payload = { scale, turnover };
	return {
		name: app_const.UPDATE_DIVIDEND_SCALE,
		callAPI: () => Utils.POST(`/api/dividend/${id}/update`, JSON.stringify(payload))(),
	}
}