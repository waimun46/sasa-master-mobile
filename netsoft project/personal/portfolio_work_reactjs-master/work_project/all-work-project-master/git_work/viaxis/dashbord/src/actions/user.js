import $ from "jquery";
import * as app_const from "../app_const";
import * as Utils from "../utils/common";

export function queryUsers(pageIndex = 1, querys = {}) {
	let timestamp = new Date().getTime();
	var queryParams = Object.assign({}, querys, {
		pageIndex: pageIndex
	});
	var queryStr = JSON.stringify(queryParams);
	return {
		name: app_const.QUERY_USERS,
		callAPI: () => Utils.POST(`/api/user/list`, queryStr)(),
		payload: queryParams
	}
};

export function onlineUsers(query = {}) {
	return {
		name: app_const.ONLINE_USERS,
		callAPI: () => Utils.POST(`/api/user/online`, JSON.stringify(query))(),
	}
}

/**
 * 获取指定用户信息
 * @param  {[type]} userId [description]
 * @return {[type]}        [description]
 */
export function getUser(userId) {
	return {
		name: app_const.GET_USER,
		callAPI: () => Utils.POST(`/api/user/${userId}`)(),
	}
}

export function addUser(querys = {}) {
	//var payload={email,password,aid,backpct,user_kind,user_type,mobile,qq,daily_wages,enable_payout,enable_comm,enable_daily_wages};
	return {
		name: app_const.ADD_USER,
		callAPI: () => Utils.POST(`/api/user/add`, JSON.stringify(querys))()
	};
}

export function updateUser(id, querys = {}) {
	return {
		name: app_const.UPDATE_USER,
		callAPI: () => Utils.POST(`/api/user/${id}/update`, JSON.stringify(querys))()
	};
}

export function updateUserPassword(old_password, new_password) {
	var payload = { old_password, new_password };
	return {
		name: app_const.UPDATE_USER_PASSWORD,
		callAPI: () => Utils.POST(`/api/user/update/password`, JSON.stringify(payload))()
	}
}

/* ---------------------- team -------------------------------*/

/**
 * 获取团队下级信息,只获取直接下级
 * @param  {[type]} userId [description]
 * @param  {[type]} pageIndex   [description]
 * @return {[type]}        [description]
 */
export function getTeam(userId, pageIndex) {
	var body = JSON.stringify({ pageIndex });
	let url = `/api/user/team?${body}`;
	if (userId)
		url = `/api/user/${userId}/team`;

	return {
		name: app_const.QUERY_TEAM,
		callAPI: () => Utils.POST(url, body)()
	}
}

/**
 * 查询下级用户
 * @param  {[type]} pageIndex   [description]
 * @param  {[type]} querys [description]
 * @return {[type]}        [description]
 */
export function queryTeam(pageIndex, querys) {
	var queryParams = Object.assign({}, querys, {
		pageIndex
	});
	var body = JSON.stringify(queryParams);
	return {
		name: app_const.QUERY_TEAM,
		callAPI: () => Utils.POST(`/api/user/team/query`, body)()
	}
}

/**
 * 获取用户上级列表
 * @param  {[type]} userId [description]
 * @return {[type]}        [description]
 */
export function getParents(userId) {
	return {
		name: app_const.GET_PARENT,
		callAPI: () => Utils.POST(`/api/user/${userId}/parents`)()
	}
}

/* ---------------------- action log -------------------------*/

export function queryActionLog(userId, pageIndex = 1, querys = {}) {
	var queryParams = Object.assign({}, querys, {
		pageIndex: pageIndex
	});
	var queryStr = JSON.stringify(queryParams);
	var url = userId ? `/api/user/${userId}/action/log` : `/api/user/action/log`;

	return {
		name: app_const.QUERY_ACTIONLOG,
		callAPI: () => Utils.POST(url, queryStr)()
	}
}

/* ---------------------- login log ------------------------- */
export function queryLoginLog(userId, pageIndex = 1, querys = {}) {
	var queryParams = Object.assign({}, querys, {
		pageIndex: pageIndex
	});
	var queryStr = JSON.stringify(queryParams);
	var url = userId ? `/api/user/${userId}/loginlog` : `/api/user/loginlog`;

	return {
		name: app_const.QUERY_LOGINLOG,
		callAPI: () => Utils.POST(url, queryStr)()
	}
};

// rebate level
export function queryRebateLevel(querys = {}) {
	var queryStr = JSON.stringify(querys);
	return {
		name: app_const.QUERY_REBATELEVEL,
		callAPI: () => Utils.POST(`/api/rebatelevel`, queryStr)()
	}
}

export function updateRebateLevel(level, turnover, backpct) {
	var payload = { level, turnover, backpct };
	return {
		name: app_const.UPDATE_REBATELEVEL,
		callAPI: () => Utils.POST(`/api/rebatelevel/update`)(),
		payload
	};
}

export function gettestsearch() {
	return {

	};
}

/**
 * 检查邮箱是否存在
 * @param  {[type]} email [description]
 * @return {[type]}       [description]
 */
export function checkUsername(email) {
	return {
		name: "CheckUsername",
		callAPI: () => Utils.POST(`/api/user/checkusername?email=${email}`)()
	};
}

// referral
export function queryReferrals(pageIndex, query = {}) {
	var queryParams = Object.assign({}, query, {
		pageIndex
	});
	var body = JSON.stringify(queryParams);
	return {
		name: app_const.QUERY_REFERRAL,
		callAPI: () => Utils.POST(`/api/referral`, bo)()
	}
}