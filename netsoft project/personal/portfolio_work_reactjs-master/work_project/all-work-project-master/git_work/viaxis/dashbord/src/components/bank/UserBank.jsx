import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { message, Row, Col, Table, Tag, Button, Modal, Form, Input, Select, DatePicker } from 'antd';

import BasicComponent from "../common/BasicComponent";
import SearchForm from "../common/SearchForm";

import { queryUserBank, queryBank, updateUserBank } from '../../actions/bank';
import { dateFormat } from "../../utils/string";
import * as RenderHelper from "../../utils/render";
import { CommonStatus, CommonStyleStatus, BankType } from "../../app_enums";

const createForm = Form.create;
const FormItem = Form.Item;
const Option = Select.Option;
const RangePicker = DatePicker.RangePicker;
const Message = message;

class UserBank extends BasicComponent {
    constructor(props) {
        super(props);
        this.pageIndex = 1;
        this.query = { LoadUser: true };
    }

    /* ------------------------- Sys ------------------------ */

    componentWillMount() {
        this.queryData();
        // this.queryBankData();
    }

    /* ------------------------- Function ------------------------ */

    queryBankData() {
        this.props.dispatch(queryBank());
    }

    queryData() {
        this.props.dispatch(queryUserBank(this.pageIndex, this.query)).then(act => {
            if (act.error)
                Message.error(act.error);
        });
    }

    /* ------------------------- Events ------------------------ */

    onSearchClick(query) {
        this.query = Object.assign(query, this.query);
        this.queryData();
    }

    onResetClick() {
        this.query = { LoadUser: true };
    }

    /* ------------------------- Render ------------------------ */

    render() {
        const { data } = this.props;
        let bank = data.user || {};

        const self = this;
        const pagination = {
            total: bank.count || 0,
            onChange(current) {
                self.pageIndex = current;
                self.queryData();
            },
        };

        const rowKey = function (bank) {
            return bank.Id;
        }

        return (
            <div>
                {this.renderSearch()}
                <div className="search-result-list">
                    <Table columns={this.columns} dataSource={bank.data || []} loading={data.isFetching} pagination={pagination} rowKey={rowKey} />
                </div>
            </div>
        );
    }

    renderSearch() {
        const formItemLayout = {
            labelCol: { span: 8 },
            wrapperCol: { span: 16 }
        };

        return (
            <SearchForm onSearch={this.onSearchClick.bind(this)} onReset={this.onResetClick.bind(this)}>
                <email label="邮箱" {...formItemLayout}>
                    <Input />
                </email>
                <account_no label="银行账户" {...formItemLayout}>
                    <Input />
                </account_no>
                <account_name label="户名" {...formItemLayout}>
                    <Input />
                </account_name>
                <status label="状态" {...formItemLayout} options={{ initialValue: "" }}>
                    <Select name="status">
                        <Option value="">全部</Option>
                        {Object.keys(CommonStatus).map(it => {
                            return <Option key={it} value={it}>{CommonStatus[it]}</Option>;
                        })}
                    </Select>
                </status>
                <create_time label="创建时间" {...formItemLayout} format={(times) => Utils.dateRangeFormat(times)}>
                    <DatePicker.RangePicker />
                </create_time>
            </SearchForm>
        );
    }

    get columns() {
        const self = this;

        const columns = [{ title: '#', render(text, record, index) { return index + 1; } },
        { title: '邮箱', dataIndex: 'Email' },
        { title: '银行', dataIndex: 'BankName' },
        { title: "户名", dataIndex: "AccountName" },
        { title: "账号", dataIndex: "AccountNo" },
        { title: "分行", dataIndex: "BranchName" },
        { title: "状态", dataIndex: "Status", render: RenderHelper.render_status(CommonStyleStatus) },
        { title: '时间', dataIndex: 'CreateTime' },
        {
            title: "操作", render(userBank) {
                return (
                    <UserBankEdit userBank={userBank} banks={self.props.data.banks} dispatch={self.props.dispatch} queryData={self.queryData.bind(self)} />
                );
            }
        }
        ];

        return columns;
    }
}

class UserBankEditComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            popupTitle: "",
            popup: false,
            loading: false,
            userBank: {}
        }
    }

    show(userBank) {
        if (userBank) {
            const { form } = this.props;
            form.resetFields();
            this.setState({
                popupTitle: "编辑用户" + userBank.Email + "银行卡资料",
                popup: true,
                loading: false,
                userBank: userBank
            });
        }
    }

    hide() {
        this.setState({
            popup: false
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        const { dispatch, form, queryData } = this.props;
        form.validateFields((errors, values) => {
            if (errors) return;
            this.setState({ loading: true });
            dispatch(updateUserBank(this.state.userBank.Id, values.bank_id, values.account_no, values.account_name, values.branch, values.status)).then(act => {
                this.setState({ loading: false });
                if (act.error)
                    Message.error(act.error)
                else {
                    Message.success("编辑用户" + this.state.userBank.Email + "银行卡资料成功");
                    this.hide();
                    queryData();
                }
            });
        });
    }

    render() {
        const { banks, userBank } = this.props;
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: { span: 7 },
            wrapperCol: { span: 12 },
        };

        return (
            <div>
                <Button type="primary" onClick={this.show.bind(this, userBank)}>编辑</Button>
                <Modal title={this.state.popupTitle} visible={this.state.popup} onCancel={this.hide.bind(this)} footer={[
                    <Button key="back" type="ghost" size="large" onClick={this.hide.bind(this)}>取消</Button>,
                    <Button key="submit" type="primary" size="large" loading={this.state.loading} onClick={this.handleSubmit.bind(this)}>
                        提交
                    </Button>
                ]}>
                    <Form horizontal>
                        <FormItem {...formItemLayout} label="银行">
                            {getFieldDecorator('bank_id', {
                                initialValue: (this.state.userBank.BankId || "").toString(),
                                rules: [{ required: true, message: "请选择银行" }]
                            })(
                                <Select>
                                    {(banks || []).map(bank => {
                                        return <Option value={bank.ID.toString()} key={bank.ID}>{bank.Name}</Option>
                                    })}
                                </Select>
                            )}
                        </FormItem>
                        <FormItem {...formItemLayout} label="户名">
                            {getFieldDecorator('account_name', {
                                initialValue: (this.state.userBank.AccountName || ""),
                                rules: [{ required: true, whitespace: false, message: "请输入户名" }]
                            })(
                                <Input />
                            )}
                        </FormItem>
                        <FormItem {...formItemLayout} label="账号">
                            {getFieldDecorator('account_no', {
                                initialValue: (this.state.userBank.AccountNo || ""),
                                rules: [{ required: true, whitespace: false, message: "请输入账号" }]
                            })(
                                <Input />
                            )}
                        </FormItem>
                        <FormItem {...formItemLayout} label="分行">
                            {getFieldDecorator('branch', {
                                initialValue: (this.state.userBank.BranchName || "")
                            })(
                                <Input />
                            )}
                        </FormItem>
                        <FormItem {...formItemLayout} label="状态">
                            {getFieldDecorator('status', {
                                initialValue: (this.state.userBank.Status || 0).toString()
                            })(
                                <Select>
                                    {Object.keys(CommonStatus).map(it => {
                                        return <Option value={it.toString()} key={it}>{CommonStatus[it]}</Option>
                                    })}
                                </Select>
                            )}
                        </FormItem>
                    </Form>
                </Modal>
            </div>
        );
    }
}
const UserBankEdit = createForm()(UserBankEditComponent);


export default connect(state => ({
    data: state.bank,
}))(UserBank);
