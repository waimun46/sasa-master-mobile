import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { message, Row, Col, Popconfirm, Table, Tag, Button, Modal, Form, Input, Select, DatePicker } from 'antd';

import BasicComponent from "../common/BasicComponent";
import SearchForm from "../common/SearchForm";
import EditorModal from "../common/EditorModal";

import * as GameAction from '../../actions/game';
import * as RenderHelper from "../../utils/render";
import * as Utils from "../../utils/common";
import { GameDataStatus } from "../../app_enums";

class OpenQhForm extends BasicComponent {

    constructor(props) {
        super(props);
        this.state = { loading: false };
    }

    handleSubmit(e) {
        e.preventDefault();

        const { dispatch, group, qh, destroy } = this.props;

        this.props.form.validateFields((errors, values) => {
            if (errors) return;

            this.setState({ loading: true });

            dispatch(GameAction.openGameData(group.Id, qh, values)).then(act => {
                this.setState({ loading: false });

                this.assert(act, {
                    fnOk: it => {
                        destroy && destroy();
                        message.success("操作成功");
                    },
                    fnError: it => {
                        message.error(it.message)
                    }
                });

                return act;
            }).then(act => {
                this.props.onCompleted && this.props.onCompleted(act);
                return act;
            });

        });
    }

    /* ---------------------------- Renders  ------------------------------*/
    render() {
        const { getFieldDecorator } = this.props.form;

        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 14 },
        };

        const tailFormItemLayout = {
            wrapperCol: {
                span: 14,
                offset: 6,
            },
        };

        const { group } = this.props;

        return (
            <Form onSubmit={this.handleSubmit.bind(this)} autoComplete="off">
                <Row>
                    {Utils.range(0, group.NumCount).map((it) =>
                        <Col span={6} key={it} className="p5">
                            {
                                getFieldDecorator(`z${it + 1}`, {
                                    rules: [{
                                        required: it < group.NumCount, message: `请输入${it + 1}`
                                    }],
                                })(
                                    <Input addonBefore={it + 1} />
                                )
                            }
                        </Col>
                    )}
                    <Col span={6} className="p5">
                        {
                            getFieldDecorator("t1")(
                                <Input addonBefore="特" />
                            )
                        }
                    </Col>
                </Row>
                <br />
                <Form.Item {...tailFormItemLayout}>
                    <Button loading={this.state.loading} className="fn-width100" type="primary" htmlType="submit" size="large">确定</Button>
                </Form.Item>
            </Form>
        );
    }
}
OpenQhForm.propTypes = {
    dispatch: React.PropTypes.func.isRequired,
    /**
     * 要开奖的彩种对象
     * @type {[type]}
     */
    group: React.PropTypes.object.isRequired,
    /**
     * 期号
     * @type {[type]}
     */
    qh: React.PropTypes.number.isRequired,
    /**
     * 操作完成
     * @type {[type]}
     */
    onCompleted: React.PropTypes.func
};
const OpenQhFormWraper = Form.create()(OpenQhForm);


class GameData extends BasicComponent {

    constructor(props) {
        super(props);
        this.state = {
            // 正在取消的期号
            cancelQh: null
        };
        this.defaultQuery = this.query = Object.assign({}, { status: 1 }, Utils.getQueryString());
        this.pageIndex = 1;
    }

    /* ---------------------------- 系统函数 ------------------------------*/


    componentWillMount() {
        const { Groups } = this.props;
        if (!Groups || Groups.length == 0) {
            this.queryGroup();
        }
        this.queryData();
    }

    /* ---------------------------- 自定义函数 ------------------------------*/

    queryGroup() {
        this.props.dispatch(GameAction.queryGameGroup()).then(act => {
            if (act.error)
                message.error(act.error);
        });
    }

    queryData() {
        this.props.dispatch(GameAction.queryGameData(this.pageIndex, this.query)).then(act => {
            if (act.error)
                Message.error(act.error);
        });
    }

    cancelQh(groupId, qh) {
        this.setState({ cancelQh: { groupId, qh } });
        this.props.dispatch(GameAction.cancelGameData(groupId, qh)).then(act => {
            this.setState({ cancelQh: null });
            this.assert(act, {
                fnOk: (it) => {
                    message.success("操作成功");
                    this.queryData();
                }
            })
        });
    }

    openQh(groupId, qh) {
        const groups = this.props.Groups || [];
        const group = groups.find(it => it.Id == groupId);

        if (!group) {
            message.error("找不到对应的彩种，请刷新页面重试!");
            return;
        }

        RenderHelper.dialog(`${group.Name}第${qh}开奖`, <OpenQhFormWraper group={group} qh={qh}
            dispatch={this.props.dispatch}
            onCompleted={this.onOpenDataCompleted.bind(this)} />);
    }

    /* ---------------------------- Events ------------------------------*/

    onSearch(query) {
        this.pageIndex = 1;
        this.query = query;
        this.queryData();
    }

    onReset() {
        this.query = this.defaultQuery;
    }

    onOpenDataCompleted(act) {
        const resp = act.response;
        if (resp.code == 0)
            this.queryData();
    }

    /* ---------------------------- Renders ------------------------------*/

    tableColumns() {

        const groups = this.props.Groups || [];
        let group = {};
        for (let i in groups) {
            group[groups[i].Id] = groups[i].Name;
        }

        const columns = [{ title: '#', render: RenderHelper.render_index },
        {
            title: '彩种',
            dataIndex: 'GroupId',
            render(id) {
                return group[id] || "Unknow";
            }
        },
        { title: "期号", dataIndex: "Qh" },
        { title: "开奖结果", dataIndex: "Result" },
        { title: "状态", dataIndex: "Status", render: RenderHelper.render_status(GameDataStatus) },
        { title: "开始时间", dataIndex: "StartTime", render: RenderHelper.render_time },
        { title: "结束时间", dataIndex: "EndTime", render: RenderHelper.render_time },
        { title: "开奖时间", dataIndex: "OpenTime", render: RenderHelper.render_time },
        {
            title: "操作", render: (it) => {
                if (it.Status != 0) return null;

                return (
                    <span>
                        <Button type="primary" size="small" onClick={() => this.openQh(it.GroupId, it.Qh)}>开奖</Button>
                        &nbsp;&nbsp;
                        <Popconfirm placement="left" title="一旦取消期号，该期所有注单也会一并取消，您确定要继续此操作吗?"
                            onConfirm={() => this.cancelQh(it.GroupId, it.Qh)}>
                            <Button type="danger" size="small">取消</Button>
                        </Popconfirm>
                    </span>
                );
            }
        }
        ];

        return columns;
    }

    render() {
        const gamedata = this.props.GameData || {};
        const groups = this.props.Groups || [];
        const columns = this.tableColumns();

        const self = this;
        const pagination = {
            current: gamedata.pageIndex,
            total: gamedata.count || 0,
            onChange(current) {
                self.pageIndex = current;
                self.queryData();
            },
        };
        return (
            <div>
                {this.renderSearch()}
                <div className="search-result-list">
                    <Table columns={columns} dataSource={gamedata.data || []} loading={this.props.isFetching} pagination={pagination} />
                </div>
            </div>
        );
    }

    renderSearch() {
        const Groups = this.props.Groups || [];
        const formItemLayout = {
            labelCol: { span: 8 },
            wrapperCol: { span: 16 }
        };

        return (
            <SearchForm onSearch={this.onSearch.bind(this)} onReset={this.onReset.bind(this)}>
                <group_id label="彩种" {...formItemLayout} options={{ initialValue: "" }}>
                    <Select>
                        <Select.Option value="">全部</Select.Option>
                        {Groups.map(group => {
                            return <Select.Option value={group.Id.toString()} key={group.Id}>{group.Name}</Select.Option>
                        })}
                    </Select>
                </group_id>
                <qh label="期号" {...formItemLayout}>
                    <Input placeholder="期号" />
                </qh>
                <status label="状态" {...formItemLayout} options={{ initialValue: "1" }}>
                    <Select>
                        <Select.Option value="">全部</Select.Option>
                        {Object.keys(GameDataStatus).map(it => {
                            return <Select.Option value={it.toString()} key={it}>{GameDataStatus[it].name}</Select.Option>
                        })}
                    </Select>
                </status>
                <start_time label="开始时间" {...formItemLayout} format={(times) => Utils.dateRangeFormat(times)}>
                    {RenderHelper.rangeDatePick()}
                </start_time>
                <end_time label="结束时间" {...formItemLayout} format={(times) => Utils.dateRangeFormat(times)}>
                    {RenderHelper.rangeDatePick()}
                </end_time>
                <open_time label="开奖时间" {...formItemLayout} format={(times) => Utils.dateRangeFormat(times)}>
                    {RenderHelper.rangeDatePick()}
                </open_time>
            </SearchForm>
        );
    }
}

export default connect(state => ({
    isFetching: state.game.isFetching,
    Groups: state.game.Groups,
    GameData: state.game.GameData,
}))(GameData);
