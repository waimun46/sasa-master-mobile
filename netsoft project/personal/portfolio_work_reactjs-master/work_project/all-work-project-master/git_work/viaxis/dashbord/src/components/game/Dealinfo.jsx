import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { message, Icon, Popconfirm, Table, Tag, Button, Modal, Popover, Form, Input, Select, DatePicker, Tooltip } from 'antd';

import BasicComponent from "../common/BasicComponent";
import SearchForm from "../common/SearchForm";
import EditorModal from "../common/EditorModal";

import * as GameAction from '../../actions/game';
import * as RenderHelper from "../../utils/render";
import * as Utils from "../../utils/common";
import * as BetHelper from "../../utils/bet_helper";
import { DealStatus, GameGroup, WinLoss } from "../../app_enums";

const Option = Select.Option;
const Message = message;

class DealInfo extends BasicComponent {

    constructor(props) {
        super(props);
        this.query = this.defaultQuery = Object.assign({}, Utils.getQueryString(), { "LoadAll": true });
        this.pageIndex = 1;
    }

    /* ---------------------------- 系统函数 ------------------------------*/

    componentWillMount() {

        const { Groups } = this.props;

        if (!Groups || Groups.length == 0) {
            this.queryGroup();
        }
        this.queryData();
    }

    /* ---------------------------- 自定义函数 ------------------------------*/

    initQuery() {
        var queryString = Utils.getQueryString();
    }

    queryGroup() {
        this.props.dispatch(GameAction.queryGameGroup()).then(act => {
            if (act.error)
                Message.error(act.error);
        });
    }

    queryData() {
        this.props.dispatch(GameAction.queryDealInfo(this.pageIndex, this.query)).then(act => {
            if (act.error)
                Message.error(act.error);
        });
    }

    cancelDeal(dealId) {
        this.props.dispatch(GameAction.cancelDealInfo(dealId)).then(act => {
            this.assert(act, {
                fnOk: it => {
                    Message.success(`注单 ${dealId} 已标注为待取消。`);
                    this.queryData();
                }
            });
        });
    }

    /* ---------------------------- Events ------------------------------*/

    onSearch(query) {
        this.query = Object.assign({}, this.query, query);
        this.pageIndex = 1;
        this.queryData();
    }

    onReset() {
        this.query = this.defaultQuery;
    }

    /* ---------------------------- Render ------------------------------*/

    tableColumns() {
        let groups = this.props.Groups || [];
        let group = {};
        for (let i in groups) {
            group[groups[i].Id] = groups[i].Name;
        }

        const columns = [{ title: '#', render: RenderHelper.render_index, width: 30, fixed: 'left' },
        { title: '注单号', dataIndex: 'Id', width: 60, fixed: 'left' },
        { title: "邮箱", dataIndex: "Email", width: 150, fixed: 'left' },
        {
            title: "彩种", dataIndex: "GroupId", render(id) {
                return group[id] || "Unknow";
            }
        },
        {
            title: "期号", dataIndex: "Qh", width: 100, render: (qh, deal) => {
                return (
                    <Popover content={<GameDataContent group={deal.GameGroup} qh={qh} />} trigger="click">
                        <a>{qh}</a>
                    </Popover>
                );
            }
        },
        {
            title: "玩法", dataIndex: "GameTypeName", render: (name, it) => {
                return `${name}[追]`;
            }
        },
        {
            title: '投注内容', width: 120, dataIndex: 'BetNumber', render(num) {
                const beautityNum = BetHelper.betNumBeautify(num);
                return <Tooltip title={beautityNum}><p className="betnum">{beautityNum}</p></Tooltip>
            }
        },
        { title: "投注金额", dataIndex: "BetAmount", className: 'column-money', render: RenderHelper.money_format },
        { title: "总金额", dataIndex: "Turnover", className: 'column-money', render: RenderHelper.money_format },
        { title: "盈亏", dataIndex: "Profit", className: 'column-money', render: RenderHelper.money_color },
        { title: "余额", dataIndex: "Balance", className: 'column-money', render: RenderHelper.money_format },
        { title: "赔率", dataIndex: "Scale" },
        { title: "状态", dataIndex: "Status", width: 80, render: RenderHelper.render_status(DealStatus) },
        { title: "投注时间", dataIndex: "CreateTime", render: RenderHelper.render_time },
        { title: "结算时间", dataIndex: "SettleTime", render: RenderHelper.render_time },
        {
            title: "操作", render: it => {
                if (it.Status != 0) return null;

                return (
                    <Popconfirm placement="left" title="一旦取消将不可恢复，您确定要继续此操作吗?"
                        onConfirm={() => this.cancelDeal(it.Id)}>
                        <Button type="danger" size="small">取消</Button>
                    </Popconfirm>
                );
            }
        }
        ];

        return columns;
    }

    render() {

        const DeaInfos = this.props.DealInfos || {};
        const columns = this.tableColumns();
        const self = this;

        const pagination = {
            total: DeaInfos.count || 0,
            onChange(current) {
                self.pageIndex = current;
                self.queryData();
            },
        };
        return (
            <div>
                {this.render_search()}
                <div className="search-result-list">
                    <Table columns={columns} dataSource={DeaInfos.data || []}
                        scroll={{ x: 1500 }}
                        loading={this.props.isFetching}
                        pagination={pagination} rowKey="Id" />
                </div>
            </div>
        );
    }

    render_search() {
        const groups = this.props.Groups || [];
        const formItemLayout = {
            labelCol: { span: 8 },
            wrapperCol: { span: 16 }
        };

        return (
            <SearchForm onSearch={this.onSearch.bind(this)} onReset={this.onReset.bind(this)}>
                <email label="邮箱" {...formItemLayout}>
                    <Input placeholder="Email" size="default" name="email" />
                </email>
                <deal_id label="注单号" {...formItemLayout}>
                    <Input size="default" placeholder="注单号" />
                </deal_id>
                <group_id label="彩种" {...formItemLayout} options={{ initialValue: "" }}>
                    <Select>
                        <Option value="">全部</Option>
                        {groups.map(group => {
                            return <Option value={group.Id.toString()} key={group.Id}>{group.Name}</Option>
                        })}
                    </Select>
                </group_id>
                <qh label="期号" {...formItemLayout} options={{ initialValue: "" }}>
                    <Input placeholder="期号" size="default" />
                </qh>
                <status label="状态" options={{ initialValue: "" }}  {...formItemLayout} >
                    <Select optionFilterProp="children">
                        <Option value="">全部</Option>
                        {Object.keys(DealStatus).map(it => {
                            return <Option value={it.toString()} key={it}>{DealStatus[it].name}</Option>
                        })}
                    </Select>
                </status>
                <create_time label="投注时间" {...formItemLayout} format={(times) => Utils.dateRangeFormat(times)}>
                    {RenderHelper.rangeDatePick()}
                </create_time>
                <settle_time label="结算时间" {...formItemLayout} format={(times) => Utils.dateRangeFormat(times)}>
                    {RenderHelper.rangeDatePick()}
                </settle_time>
                <winloss label="盈亏" {...formItemLayout} options={{ initialValue: "" }}>
                    <Select>
                        <Option value="">全部</Option>
                        {Object.keys(WinLoss).map(it => {
                            return <Option value={it.toString()} key={it}>{WinLoss[it]}</Option>
                        })}
                    </Select>
                </winloss>
            </SearchForm>
        );
    }
}

class GameDataContent extends BasicComponent {

    componentWillMount() {

        const { hash, group, qh } = this.props;
        if (!this.gameData)
            this.queryData();

    }

    queryData() {
        const { group, qh } = this.props;
        this.props.dispatch(GameAction.getGameData(group, qh));
    }

    get gameData() {
        const { hash, group, qh } = this.props;

        return (hash && hash[group] && hash[group][qh]) || null;
    }

    render() {
        return (
            this.gameData
                ?
                <span>
                    {this.gameData.Result}
                </span>
                :
                <span><Icon type="loading" /> loading...</span>
        );
    }
}
GameDataContent.propTypes = {
    group: React.PropTypes.number.isRequired,
    qh: React.PropTypes.number.isRequired
}

GameDataContent = connect(state => ({
    hash: state.game.GameHash
}))(GameDataContent);

export default connect(state => ({
    isFetching: state.game.isFetching,
    Groups: state.game.Groups,
    DealInfos: state.game.DealInfo,
}))(DealInfo);
