var path = require('path');
var webpack = require("webpack");

var node_modules = path.resolve(__dirname, 'node_modules');
var pathToReact = path.resolve(node_modules, 'react/dist/react.min.js');

module.exports = {
    devtool: false,
    entry: {
        react: ['react', "react-dom", "react-router", "react-redux", 'babel-polyfill'],
        antd: ['antd'],
        // Hot replace
        main: './src/main.jsx',
        login: './src/login.jsx'
    },
    output: {
        path: "./dist/",
        filename: '[name].js',
        alias: {
            'react': pathToReact
        }
    },
    resolve: {
        extensions: ['', '.js', '.jsx']
    },
    resolveLoader: {
        modulesDirectories: ["node_modules"],
    },
    module: {
        loaders: [{
            test: /\.js[x]?$/,
            exclude: /node_modules/,
            loader: 'babel-loader?presets[]=es2015&presets[]=react'
        }, {
            test: /\.css$/,
            loader: 'style!css'
        }, {
            test: /\.(png|jpg)$/,
            loader: 'url-loader?limit=8192'
        },{
            test: /\.json$/,
            loader: 'json-loader'
        }],
        rules: [{
            test: /\.json$/,
            use: 'json-loader'
        }]
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin({
            beautify: false,
            mangle: {
                screw_ie8: true,
                keep_fnames: true
            },
            compress: {
                screw_ie8: true
            },
            comments: false
        }),
        new webpack.DefinePlugin({
            __DEV__: JSON.stringify(false),
            'process.env.NODE_ENV': JSON.stringify("production")
        })
    ]
};