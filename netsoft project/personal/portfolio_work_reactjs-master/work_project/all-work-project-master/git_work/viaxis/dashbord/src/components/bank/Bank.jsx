import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { message, Row, Col, Table, Tag, Button, Modal, Form, Input, Select, DatePicker } from 'antd';
import { queryBank, addBank, updateBank } from '../../actions/bank';
import { dateFormat, isEmptyObject } from "../../utils/string";
import * as RenderHelper from "../../utils/render";
import { CommonStatus, CommonStyleStatus, BankType } from "../../app_enums";

const FormItem = Form.Item;
const Option = Select.Option;
const RangePicker = DatePicker.RangePicker;
const Message = message;

class Bank extends Component {

    constructor(props) {
        super(props);
        this.query = {};
        this.state = {
            loading: false,
            popup: false,
            popupTitle: "",
            edit: false,
            current_bank: {}
        };
    }

    componentDidMount() {
        this.queryData();
    }

    queryData() {
        this.props.dispatch(queryBank(this.query)).then(act => {
            if (act.error)
                Message.error(act.error);
        });
    }

    addBank() {
        this.setState({
            popup: true,
            popupTitle: "添加银行",
            current_bank: {},
            edit: false
        });
    }

    updateBank(bank) {
        this.setState({
            popup: true,
            popupTitle: "编辑" + bank.Name,
            current_bank: bank,
            edit: true
        });
    }

    handleSubmit(e) {
        const { dispatch } = this.props;
        e.preventDefault();
        this.props.form.validateFields((errors, values) => {
            if (errors) return;

            if (this.state.edit) {
                let bank = this.state.current_bank;
                if (bank == null || JSON.stringify(bank) == '{}') return;

                this.setState({ loading: true });
                dispatch(updateBank(bank.ID, values.name, values.code, values.type, values.status)).then(act => {
                    this.setState({ edit: false, loading: false });
                    if (act.error) {
                        Message.error(act.error);
                    } else {
                        Message.success("修改成功");
                        this.props.form.resetFields();
                        this.setState({ popup: false });
                        this.queryData();
                    }
                });
            } else {
                dispatch(addBank(values.name, values.code, values.type)).then(act => {
                    this.setState({ edit: false, loading: false });
                    if (act.error) {
                        Message.error(act.error);
                    } else {
                        Message.success("添加成功");
                        this.props.form.resetFields();
                        this.setState({ popup: false });
                        this.queryData();
                    }
                });
            }
        });
    }

    handleReset(e) {
        e.preventDefault();
        this.props.form.resetFields();
    }

    hideModal() {
        this.setState({ popup: false });
    }

    tableColumns() {
        const self = this;
        const columns = [{ title: '#', render(text, record, index) { return index + 1; } },
        { title: '名称', dataIndex: 'Name' },
        { title: "代码", dataIndex: "BankCode" },
        {
            title: "类型", dataIndex: "Type", render(type) {
                return BankType[type];
            }
        },
        { title: "状态", dataIndex: "Status", render: RenderHelper.render_status(CommonStyleStatus) },
        { title: '时间', dataIndex: 'CreateTime' },
        {
            title: "操作", render(bank) {
                return <Button type="primary" onClick={() => self.updateBank(bank)}>编辑</Button>
            }
        }];

        return columns;
    }

    render() {
        const { data } = this.props;
        let banks = data.banks || [];

        let columns = this.tableColumns();
        const self = this;
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: { span: 7 },
            wrapperCol: { span: 12 },
        };
        return (
            <div>
                <div style={{ marginBottom: 16 }}>
                    <Button type="primary" onClick={this.addBank.bind(this)}>添加银行</Button>
                </div>
                <Table columns={columns} dataSource={banks} loading={data.isFetching} rowKey="ID" />
                <Modal title={this.state.popupTitle} visible={this.state.popup} onCancel={this.hideModal.bind(this)}
                    footer={[
                        <Button key="back" type="ghost" size="large" onClick={this.hideModal.bind(this)}>取消</Button>,
                        <Button key="submit" type="primary" size="large" loading={this.state.loading} onClick={this.handleSubmit.bind(this)}>
                            提交
                    </Button>,
                    ]}>
                    <Form horizontal>
                        <FormItem {...formItemLayout} label="银行名称">
                            {getFieldDecorator('name', {
                                initialValue: this.state.current_bank.Name || "",
                                rules: [{
                                    required: true,
                                    message: '请输入银行名称'
                                }]
                            })(
                                <Input type="name" autoComplete="off" />
                            )}
                        </FormItem>
                        <FormItem {...formItemLayout} label="银行代码">
                            {getFieldDecorator('code', {
                                initialValue: this.state.current_bank.BankCode || "",
                                rules: [{
                                    required: true,
                                    message: '请输入银行代码'
                                }]
                            })(
                                <Input type="code" autoComplete="off" />
                            )}
                        </FormItem>
                        <FormItem {...formItemLayout} label="银行类型">
                            {getFieldDecorator('type', {
                                initialValue: (this.state.current_bank.Type || 0).toString(),
                                rules: [{
                                    required: true,
                                    message: '请选择银行类型'
                                }]
                            })(
                                <Select>
                                    {Object.keys(BankType).map(it => {
                                        return <Option key={it} value={it}>{BankType[it]}</Option>;
                                    })}
                                </Select>
                            )}
                        </FormItem>
                        {this.state.edit && <FormItem {...formItemLayout} label="状态">
                            {getFieldDecorator('status', {
                                initialValue: (this.state.current_bank.Status || 0).toString(),
                                rules: [{
                                    required: true,
                                    message: '请选择银行状态'
                                }]
                            })(
                                <Select>
                                    {Object.keys(CommonStatus).map(it => {
                                        return <Option key={it} value={it}>{CommonStatus[it]}</Option>;
                                    })}
                                </Select>
                            )}
                        </FormItem>}
                    </Form>
                </Modal>
            </div>
        );
    }
}


export default connect(state => ({
    data: state.bank,
}))(Form.create()(Bank));
