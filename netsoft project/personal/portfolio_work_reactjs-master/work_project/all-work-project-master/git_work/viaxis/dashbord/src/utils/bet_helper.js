import * as Lodash from "lodash";
import * as Constants from "../app_const";
import * as Utils from "./common";


/**
 * 将投注号码美化为可读格式，主要用于投注列表显示
 * @param  {[type]} betNumStr [description]
 * @return {[type]}           [description]
 */
export function betNumBeautify(betNumStr) {
    if (!Utils.isString(betNumStr) || Utils.isEmptyOrNull(betNumStr)) return betNumStr;
    let betNums = betNumStr.split(Constants.UNIT_POSITION_SPLIT_CHAR);

    let unitPos = betNums.length == 2 ? betNums[0] : null;
    let numStr = betNums.length == 2 ? betNums[1] : betNums[0];

    var nums = numStr.split(Constants.MULTIPLE_NUMS_SPLIT_CHAR);
    nums = nums.map(it => /[\d\-]+/.test(it) ? it : Lodash.findKey(Constants.SUM_NUMS, x => x == it));
    var numBody = nums.join(Constants.MULTIPLE_NUMS_SPLIT_CHAR);
    if (unitPos) {
        let indexArr = [];
        unitPos.split("").forEach((x,i) => x == '1'&&indexArr.push(i));
        var unitPosStr = indexArr.map(x => Constants.DIGIT_UNIT_NAME_LIST[x]).join(",");
        numBody = `(${unitPosStr})${numBody}`;
    }
    return numBody;
}