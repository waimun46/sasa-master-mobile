module.exports = {
    API: "http://10.211.55.3:5000",
    WSUrl: "ws://10.211.55.3:5000/ws",
    // 彩种号码球数量
    GameDataNum: {
        FC3D: 3,
        CQSSC: 5,
        KL8: 20,
        KL10: 8,
        KENO: 20,
        PL3: 3,
        FFC: 5,
        ZRC: 5,
        PK10: 10
    }
};
