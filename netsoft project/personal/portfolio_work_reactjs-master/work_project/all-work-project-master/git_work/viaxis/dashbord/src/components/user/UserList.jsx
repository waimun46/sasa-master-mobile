import React, { Component, PropTypes } from 'react';
import ReactDOM from "react-dom";
import { connect } from 'react-redux';
import { Link } from "react-router";
import { message, Icon, Switch, Menu, Dropdown, Table, Button, Modal, Form, Input, Select, DatePicker, Badge, Tag } from 'antd';

import BasicComponent from "../common/BasicComponent";
import SearchForm from "../common/SearchForm";
import EditorModal from "../common/EditorModal";
import OperateUserForm from "../user/OperateUserForm";

import * as UserAction from '../../actions/user';
import * as RenderHelper from "../../utils/render";
import { UserType, UserKind, ClientType, UserStatus } from "../../app_enums";
import * as Constants from "../../app_const";
import * as Utils from "../../utils/common";
import auth, { Rights } from "../../utils/auth";
import * as Validators from "../../utils/validator";

const createForm = Form.create;
const FormItem = Form.Item;
const Option = Select.Option;
const RangePicker = DatePicker.RangePicker;
const Message = message;

class UserList extends BasicComponent {

    constructor(props) {
        super(props);
        this.query = {};
        this.pageIndex = 1;
        this.state = {
            popup: false,
            loading: false,
            user: null,
            popupTitle: "",
            isadd: true,
            parent: null
        }
    }

    /* ----------------------------- Sys ------------------------------ */

    componentWillMount() {
        this.queryUsers();
    }

    /* ----------------------------- Functions ------------------------------ */

    queryUsers() {
        this.props.dispatch(UserAction.queryUsers(this.pageIndex, this.query));
    }

    /* ----------------------------- Events ------------------------------ */

    onSearchClick(query) {
        this.pageIndex = 1;
        this.query = query;
        this.queryUsers();
    }

    onResetClick() {
        this.query = {};
    }

    onAddClick(user) {
        if (!user) return;

        if (UserKind[user.UserKind] == '会员' || UserKind[user.UserKind] == '子用户') {
            message.error('会员及子用户不能添加下级!');
            return;
        }
        if (!auth.hasRights(Rights.UCENTER_INSERT)) return;

        const modal = (user, parent) => RenderHelper.modal(<OperateUserForm user={user} parent={parent} isadd={true} modifypwd={true} onSubmit={this.onAddSubmit.bind(this)} />);
        this._dialog = modal(user, user);
    }

    onAddSubmit(user, values) {
        return this.props.dispatch(UserAction.addUser(values))
            .then(act => {
                const resp = act.response;
                if (resp.code == 0) {
                    if (this._dialog) {
                        this._dialog.destroy();
                        this._dialog = null;
                        this.queryUsers();
                    }
                }
                return act;
            });
    }

    OnEditClick(user) {
        if (!user) return;
        if (!auth.hasRights(Rights.UCENTER_UPDATE)) return;
        if (user.Aid == 0) {
            message.error(user.Email + '不可修改!');
            return;
        }

        const modal = (user, parent) => RenderHelper.modal(<OperateUserForm user={user} parent={parent} isadd={false} modifypwd={false} onSubmit={(user, values) => this.onEditSubmit(user, values)} />);

        const hide = message.loading('Loading...', 0);
        this.props.dispatch(UserAction.getUser(user.Aid)).then(act => {
            hide();
            const parent = act.response.data;
            this._dialog = modal(user, parent);
        });

    }

    onEditSubmit(user, values) {
        return this.props.dispatch(UserAction.updateUser(user.UserId, values))
            .then(act => {
                const resp = act.response;
                if (resp.code == 0) {
                    if (this._dialog) {
                        this._dialog.destroy();
                        this._dialog = null;
                        this.queryUsers();
                    }
                }
                return act;
            });
    }
    /* ----------------------------- Renders ------------------------------ */

    get columns() {
        const self = this;
        const { dispatch } = this.props;
        const fnValid = it => it ? "√" : "x";
        const columns = [{ title: '#', width: 32, render: RenderHelper.render_index },
        {
            title: '邮箱', children: [
                { title: "Email", dataIndex: 'Email', key: 'Email', render: (email, user, i) => <Link to={`/agent/${user.UserId}`}>{email}</Link> },
                { title: "验证", dataIndex: "IsMailVerified", key: 'IsMailVerified', render: fnValid }
            ]
        },
        {
            title: "资金", children: [
                { title: "余额", dataIndex: "Balance", className: 'column-money', render: RenderHelper.money_format },
                { title: "冻结", dataIndex: "Freeze", className: 'column-money', render: RenderHelper.money_format },
            ]
        },
        { title: "姓名", dataIndex: "Realname" },
        {
            title: "用户类型", children: [
                {
                    title: "种类", dataIndex: "UserKind", key: 'UserKind', render(user_kind) {
                        return UserKind[user_kind];
                    }
                },
                {
                    title: "类型", dataIndex: "UserType", key: 'UserType', render(user_type) {
                        return UserType[user_type];
                    }
                },
            ]
        },
        {
            title: "佣金", children: [
                { title: "返点", dataIndex: "Backpct", key: 'Backpct', render: RenderHelper.render_percent(2) },
                { title: "发放", dataIndex: "EnableComm", key: 'EnableComm', render: fnValid }
            ]
        },
        {
            title: "日工资", children: [
                { title: "比例", dataIndex: "DailyWages", key: 'DailyWages', render: RenderHelper.render_percent() },
                { title: "发放", dataIndex: "EnableDailyWages", key: 'EnableDailyWages', render: fnValid }
            ]
        },
        { title: "红利", dataIndex: "EnablePayout", render: fnValid },
        { title: "Aid", dataIndex: "Aid" },
        { title: "手机", dataIndex: "Mobile" },
        { title: "QQ", dataIndex: "QQ" },
        { title: "状态", dataIndex: "Status", render: RenderHelper.render_status(UserStatus) },
        { title: "在线", dataIndex: "IsOnline", render: online => <Badge status={online ? "processing" : "default"} text={online ? "在线" : "离线"} /> },
        { title: '注册时间', dataIndex: 'CreateTime', render: RenderHelper.render_time },
        {
            title: "操作", width: 200, fixed: 'right', render(user) {

                const menu = (
                    <Menu>
                        <Menu.Item key="1"><Link to={`/user/${user.UserId}/loginlog`}>登陆日志</Link></Menu.Item>
                        <Menu.Item key="2"><Link to={`/user/${user.UserId}/action/log`}>操作日志</Link></Menu.Item>
                    </Menu>
                );

                return (
                    <div>
                        <Link className="ant-btn ant-btn-dashed ant-btn-circle ant-btn-icon-only" to={`/user/${user.UserId}`}>
                            <Icon type="ellipsis" />
                        </Link>
                        &nbsp;
                        <Button onClick={() => self.onAddClick(user)} type="primary" shape="circle" icon="plus"></Button>
                        &nbsp;
                        <Button onClick={() => self.OnEditClick(user)} type="primary" shape="circle" icon="edit"></Button>
                        &nbsp;
                        <Dropdown overlay={menu}>
                            <Button>
                                日志 <Icon type="down" />
                            </Button>
                        </Dropdown>
                    </div>
                );
            }
        }];

        return columns;
    }

    render() {
        const self = this;
        const { isFetching } = this.props;
        const users = this.props.users || {};
        const data = users.data || [];

        const pagination = {
            current: users.pageIndex || 1,
            total: users.count || 0,
            onChange(current) {
                self.pageIndex = current;
                self.queryUsers();
            }
        };

        return (
            <div>
                {this.renderSearch()}
                <div className="search-result-list">
                    <Table bordered scroll={{ x: 1400 }} rowKey="UserId" columns={this.columns}
                        dataSource={data} loading={isFetching} pagination={pagination} />
                </div>
            </div>
        );
    }

    renderSearch() {
        const formItemLayout = {
            labelCol: { span: 8 },
            wrapperCol: { span: 16 }
        };

        return (
            <SearchForm onSearch={this.onSearchClick.bind(this)} onReset={this.onResetClick.bind(this)}>
                <email label="邮箱" {...formItemLayout}>
                    <Input />
                </email>
                <backpct label="返点" {...formItemLayout} >
                    <Input />
                </backpct>
                <user_type label="用户类型" {...formItemLayout} options={{ initialValue: "" }}>
                    <Select>
                        <Option value="">全部</Option>
                        {Object.keys(UserType).map(it => {
                            return <Select.Option key={it} value={it}>{UserType[it]}</Select.Option>;
                        })}
                    </Select>
                </user_type>
                <user_kind label="用户种类" {...formItemLayout} options={{ initialValue: "" }}>
                    <Select>
                        <Option value="">全部</Option>
                        {Object.keys(UserKind).map(it => {
                            return <Select.Option key={it} value={it}>{UserKind[it]}</Select.Option>;
                        })}
                    </Select>
                </user_kind>
                <status label="状态" {...formItemLayout} options={{ initialValue: "" }}>
                    <Select>
                        <Option value="">全部</Option>
                        {Object.keys(UserStatus).map(it => {
                            return <Select.Option key={it} value={it}>{UserStatus[it].name}</Select.Option>;
                        })}
                    </Select>
                </status>
                <create_time label="注册时间" {...formItemLayout} format={(times) => Utils.dateRangeFormat(times)}>
                    <DatePicker.RangePicker />
                </create_time>
            </SearchForm>
        );
    }
}

class OnlineList extends BasicComponent {

    constructor(props) {
        super(props);
        this.query = {};
    }

    /* ----------------------------- Sys ------------------------------ */

    componentWillMount() {
        this.queryUsers();
    }

    /* ----------------------------- Functions ------------------------------ */

    queryUsers() {
        this.props.dispatch(UserAction.onlineUsers(this.query));
    }

    onSearchClick(query) {
        this.query.pageIndex = 1;
        this.query = Object.assign(this.query, query);
        this.queryUsers();
    }

    onResetClick() {
        this.query = {};
    }

    /* ----------------------------- Renders ------------------------------ */

    render() {
        const self = this;
        const { isFetching } = this.props;
        const users = this.props.users || {};
        const data = users.data || [];

        const pagination = {
            current: users.pageIndex || 1,
            total: users.count || 0,
            onChange(current) {
                this.query.pageIndex = current;
                self.queryUsers();
            }
        };

        return (
            <div>
                {this.renderSearch()}
                <div className="search-result-list">
                    <Table bordered rowKey="Token" columns={this.columns}
                        dataSource={data} loading={isFetching} pagination={pagination} />
                </div>
            </div>
        );
    }

    get columns() {
        const self = this;
        const { dispatch } = this.props;
        const columns = [{ title: '#', width: 32, render: RenderHelper.render_index },
        { title: "Email", dataIndex: 'Email', render: (email, user, i) => <Link to={`/user/${user.UserId}`}>{email}</Link> },
        { title: "姓名", dataIndex: "Realname" },
        { title: "种类", dataIndex: "UserKind", render: RenderHelper.render_enum(UserKind) },
        { title: "类型", dataIndex: "UserType", render: RenderHelper.render_enum(UserType) },
        { title: '客户端类型', dataIndex: 'ClientType', render: RenderHelper.render_enum(ClientType) },
        { title: '登录时间', dataIndex: 'CreateTime', render: RenderHelper.render_time },
        { title: '最后在线', dataIndex: 'LastOnline', render: RenderHelper.render_time }
        ];

        return columns;
    }

    renderSearch() {
        const formItemLayout = {
            labelCol: { span: 8 },
            wrapperCol: { span: 16 }
        };

        return (
            <SearchForm onSearch={this.onSearchClick.bind(this)} onReset={this.onResetClick.bind(this)}>
                <email label="邮箱" {...formItemLayout}>
                    <Input />
                </email>
                <client_type label="客户端类型" {...formItemLayout} options={{ initialValue: "" }}>
                    <Select>
                        <Option value="">全部</Option>
                        {Object.keys(ClientType).map(it => {
                            return <Select.Option key={it} value={it}>{ClientType[it]}</Select.Option>;
                        })}
                    </Select>
                </client_type>
                <create_time label="登录时间" {...formItemLayout} format={(times) => Utils.dateRangeFormat(times)}>
                    <DatePicker.RangePicker />
                </create_time>
            </SearchForm>
        );
    }

}
const onlineList = connect(state => ({
    isFetching: state.user.isFetching,
    users: state.user.Online
}))(OnlineList);

export { onlineList as OnlineList };

export default connect(state => ({
    isFetching: state.user.isFetching,
    users: state.user.Users,
}))(UserList);
