import * as app_const from "../app_const";

class NotificationReducer {
    static reduce(state={isFetching:false}, action){
        switch(action.type){
            case app_const.COUNT_DEPOSIT:
                return Object.assign({},state,{isFetching:true});
            case app_const.COUNT_DEPOSIT_FAILURE:
                return Object.assign({},state,{isFetching:false});
            case app_const.COUNT_DEPOSIT_SUCCESS:
                return NotificationReducer._count_deposit_success(state,action);
            default:
                return state;
        }
    }

    static _process_failure(state,action){
        const resp = action.response;
        action.error=resp.message;
        action.error_code=resp.code;
        return Object.assign({}, state, {
            isFetching: false
        });
    }

    static _count_deposit_success(state, action){
        const resp = action.response;
        if(resp.code==0){
            return Object.assign({}, state, {
                isFetching: false,
                count: resp.data
            });
        } else
            return Notification._process_failure(state, action);
    }
}

export { NotificationReducer };