import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { message, Row, Col, Table, Tag, Button, Modal, Form, Input, Select, DatePicker } from 'antd';

import BasicComponent from "../common/BasicComponent";
import SearchForm from "../common/SearchForm";

import * as UserAction from '../../actions/user';
import * as RenderHelper from "../../utils/render";
import * as Utils from "../../utils/common";

import { UserType, UserKind, UserStatus, CommonStatus, CommonStyleStatus } from "../../app_enums";

class Referral extends BasicComponent {

    constructor(props) {
        super(props);
        this.query = {};
        this.pageIndex = 1;
    }

    /* ----------------------------- Sys ------------------------------ */

    componentDidMount() {
        this.queryData();
    }

    /* ----------------------------- Functions ------------------------------ */

    queryData() {
        this.props.dispatch(UserAction.queryReferrals(this.pageIndex, this.query));
    }

    /* ----------------------------- Events ------------------------------ */

    onSearchClick(query) {
        this.pageIndex = 1;
        this.query = query;
        this.queryData();
    }

    onResetClick() {
        this.query = {};
    }

    /* ----------------------------- Renders ------------------------------ */

    get columns() {
        const self = this;
        const columns = [
            { title: '#', render: RenderHelper.render_index },
            { title: "邮箱", dataIndex: "Email" },
            {
                title: "用户类型", dataIndex: "UserKind", render(user_kind) {
                    return UserKind[user_kind];
                }
            },
            { title: "返点", dataIndex: "Backpct", render: RenderHelper.render_percent(1) },
            {
                title: "链接", dataIndex: "Id", render(id) {
                    const url = `/register/${id}`;
                    return (
                        <div>
                            {url}
                        </div>
                    );
                }
            },
            { title: "状态", dataIndex: "Status", render: RenderHelper.render_status(CommonStyleStatus) },
            { title: "创建时间", dataIndex: "CreateTime", render: RenderHelper.render_time }
        ];

        return columns;
    }

    render() {
        const self = this;
        const { isFetching } = this.props;
        const referrals = this.props.referral || {};
        let data = referrals.data || [];

        const pagination = {
            current: referrals.pageIndex || 1,
            total: referrals.count || 0,
            onChange(current) {
                self.pageIndex = current;
                self.queryData();
            }
        };

        return (
            <div>
                {this.renderSearch()}
                <div className="search-result-list">
                    <Table rowKey="Id" columns={this.columns}
                        dataSource={data} loading={isFetching} pagination={pagination} />
                </div>
            </div>
        );
    }

    renderSearch() {
        const formItemLayout = {
            labelCol: { span: 8 },
            wrapperCol: { span: 16 }
        };

        return (
            <SearchForm onSearch={this.onSearchClick.bind(this)} onReset={this.onResetClick.bind(this)}>
                <email label="邮箱" {...formItemLayout}>
                    <Input />
                </email>
                <backpct label="返点" {...formItemLayout} >
                    <Input />
                </backpct>
                <user_kind label="用户种类" {...formItemLayout} options={{ initialValue: "" }}>
                    <Select>
                        <Option value="">全部</Option>
                        {Object.keys(UserKind).map(it => {
                            return <Select.Option key={it} value={it}>{UserKind[it]}</Select.Option>;
                        })}
                    </Select>
                </user_kind>
                <status label="状态" {...formItemLayout} options={{ initialValue: "" }}>
                    <Select>
                        <Select.Option value="">全部</Select.Option>
                        {Object.keys(CommonStatus).map(it => {
                            return <Select.Option key={it} value={it}>{CommonStatus[it]}</Select.Option>;
                        })}
                    </Select>
                </status>
                <create_time label="创建时间" {...formItemLayout} format={(times) => Utils.dateRangeFormat(times)}>
                    <DatePicker.RangePicker />
                </create_time>
            </SearchForm>
        );
    }
}

export default connect(state => ({
    isFetching: state.user.isFetching,
    referral: state.user.Referral,
}))(Referral);
