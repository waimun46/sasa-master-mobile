import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { message, Row, Col, Table, Tag, Button, Modal, Form, Input, Select, DatePicker } from 'antd';

import BasicComponent from "../common/BasicComponent";
import SearchForm from "../common/SearchForm";

import * as ArticleAction from '../../actions/article';
import * as RenderHelper from "../../utils/render";
import * as Enums from "../../app_enums";

class AddForm extends BasicComponent {

    constructor(props) {
        super(props);
        this.state = { loading: false };
    }

    handleSubmit(e) {
        e.preventDefault();

        const { dispatch } = this.props;

        this.props.form.validateFields((errors, values) => {
            if (errors) return;
            this.setState({ loading: true });
            this.props.onSubmit && this.props.onSubmit(values).then(act => {
                this.setState({ loading: false });

                const resp = act.response;
                if (resp.code == 0) {
                    message.success("添加成功");
                } else {
                    message.error(resp.message);
                }

            });

        });
    }

    /* ---------------------------- Renders  ------------------------------*/
    render() {
        const { getFieldDecorator } = this.props.form;

        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 14 },
        };

        const tailFormItemLayout = {
            wrapperCol: {
                span: 14,
                offset: 6,
            },
        };

        const { categorys } = this.props;

        return (
            <Form onSubmit={this.handleSubmit.bind(this)} autoComplete="off">
                <Form.Item label="目录名称" {...formItemLayout} hasFeedback>
                    {
                        getFieldDecorator('name', {
                            rules: [{
                                required: true, message: "请输入目录名称"
                            }],
                        })(
                            <Input />
                        )
                    }
                </Form.Item>
                <Form.Item label="标签名" {...formItemLayout} hasFeedback extra="英文、下划线及数字，用于在URL中显示。">
                    {getFieldDecorator('label', {
                        rules: [{
                            required: true, message: '请输入标签名'
                        }],
                    })(
                        <Input placeholder="如:help" />
                    )}
                </Form.Item>
                <Form.Item label="上级目录" {...formItemLayout} hasFeedback>
                    {getFieldDecorator('categoryId')(
                        <Select>
                            {categorys.map(it => <Select.Option value={it.Id.toString()} key={it}>{it.Name}</Select.Option>)}
                        </Select>
                    )}
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                    <Button loading={this.state.loading} className="fn-width100" type="primary" htmlType="submit" size="large">确定</Button>
                </Form.Item>
            </Form>
        );
    }
}
AddForm.propTypes = {
    categorys: React.PropTypes.array,
    /**
     * 确定提交事件
     * @param {dict} values 表单内容
     * @type {[type]}
     */
    OnSubmit: React.PropTypes.func
};
const AddFormWraper = Form.create()(AddForm);

class EditForm extends BasicComponent {

    constructor(props) {
        super(props);
        this.state = { loading: false };
    }

    handleSubmit(e) {
        e.preventDefault();

        const { category, dispatch } = this.props;

        this.props.form.validateFields((errors, values) => {
            if (errors) return;
            this.setState({ loading: true });
            this.props.onSubmit && this.props.onSubmit(category, values).then(act => {
                this.setState({ loading: false });

                const resp = act.response;
                if (resp.code == 0) {
                    message.success("修改成功");
                } else {
                    message.error(resp.message);
                }

            });

        });
    }

    /* ---------------------------- Renders  ------------------------------*/
    render() {
        const { getFieldDecorator } = this.props.form;

        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 14 },
        };

        const tailFormItemLayout = {
            wrapperCol: {
                span: 14,
                offset: 6,
            },
        };

        const { category, categorys } = this.props;
        return (
            <Form onSubmit={this.handleSubmit.bind(this)} autoComplete="off">
                <Form.Item label="目录名称" {...formItemLayout}>
                    {
                        getFieldDecorator('name', {
                            initialValue: category && category.Name,
                            rules: [{
                                required: true, message: "请输入目录名称"
                            }],
                        })(
                            <Input />
                        )
                    }
                </Form.Item>
                <Form.Item label="标签名" {...formItemLayout} extra="英文、下划线及数字，用于在URL中显示。">
                    {getFieldDecorator('label', {
                        initialValue: category && category.Label,
                        rules: [{
                            required: true, message: '请输入标签名'
                        }],
                    })(
                        <Input placeholder="如:help" />
                    )}
                </Form.Item>
                <Form.Item label="上级目录" {...formItemLayout}>
                    {getFieldDecorator('categoryId', {
                        initialValue: category && category.CategoryId > 0 ? category.CategoryId.toString() : null
                    }
                    )(
                        <Select>
                            {categorys.map(it => <Select.Option value={it.Id.toString()} key={it.Id}>{it.Name}</Select.Option>)}
                        </Select>
                    )}
                </Form.Item>
                <Form.Item label="状态" {...formItemLayout}>
                    {getFieldDecorator('status', {
                        initialValue: category && category.Status.toString()
                    }
                    )(
                        <Select>
                            {Object.keys(Enums.CommonStatus).map(it => <Select.Option value={it.toString()} key={it}>{Enums.CommonStatus[it]}</Select.Option>)}
                        </Select>
                    )}
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                    <Button loading={this.state.loading} className="fn-width100" type="primary" htmlType="submit" size="large">确定</Button>
                </Form.Item>
            </Form>
        );
    }
}
EditForm.propTypes = {
    categorys: React.PropTypes.array,
    category: React.PropTypes.object.isRequired,
    /**
     * 确定提交事件
     * @param {dict} values 表单内容
     * @type {[type]}
     */
    OnSubmit: React.PropTypes.func
};
const EditFormWraper = Form.create()(EditForm);

class Category extends BasicComponent {

    constructor(props) {
        super(props);
        this.query = {};
    }

    /* ----------------------- Sys ----------------------- */

    componentWillMount() {
        this.queryData();
    }

    /* ----------------------- Functions ---------------------- */

    queryData() {
        this.props.dispatch(ArticleAction.queryCategory(this.query));
    }

    /* ------------------------ Events ------------------------ */

    onSearchClick(query) {
        this.query = query;
        this.queryData();
    }

    onResetClick() {
        this.query = {};
    }

    onAddClick() {
        const categorys = this.props.category || [];
        this._dialog = RenderHelper.dialog("添加目录", <AddFormWraper categorys={categorys} onSubmit={this.onAddSubmit.bind(this)} />)
    }

    onAddSubmit(values) {
        return this.props.dispatch(ArticleAction.addCategory(values.name, values.label, values.categoryId)).then(act => {
            const resp = act.response;
            if (resp.code == 0) {
                if (this._dialog) {
                    this._dialog.destroy();
                    this._dialog = null;
                }
                this.queryData();
            }
            return act;
        });
    }

    onEdit(category) {
        const categorys = this.props.category || [];
        this._dialog = RenderHelper.dialog("编辑目录", <EditFormWraper category={category} categorys={categorys} onSubmit={this.onEditSubmit.bind(this)} />)
    }

    onEditSubmit(category, values) {
        return this.props.dispatch(ArticleAction.updateCategory(category.Id, values)).then(act => {
            const resp = act.response;
            if (resp.code == 0) {
                if (this._dialog) {
                    this._dialog.destroy();
                    this._dialog = null;
                }
                this.queryData();
            }
            return act;
        });
    }

    /* ------------------------ Renders ------------------------ */

    render() {
        const { isFetching } = this.props;

        const categorys = this.props.category || [];

        return (
            <div>
                {this.renderSearch()}
                <div className="search-result-list">
                    <Table columns={this.columns} dataSource={categorys} loading={isFetching} rowKey="Id" />
                </div>
            </div>
        );
    }

    renderSearch() {
        const formItemLayout = {
            labelCol: { span: 8 },
            wrapperCol: { span: 16 }
        };

        return (
            <SearchForm showAddBtn={this.hasRights(Enums.SysRights.CMS_INSERT)} onAdd={this.onAddClick.bind(this)} onSearch={this.onSearchClick.bind(this)} onReset={this.onResetClick.bind(this)}>
                <name label="名称" {...formItemLayout}>
                    <Input />
                </name>
                <label label="标签" {...formItemLayout}>
                    <Input />
                </label>
                <status label="状态" {...formItemLayout} options={{ initialValue: "" }}>
                    <Select>
                        <Select.Option value="">全部</Select.Option>
                        {Object.keys(Enums.CommonStatus).map(it => <Select.Option key={it} value={it.toString()}>{Enums.CommonStatus[it]}</Select.Option>)}
                    </Select>
                </status>
                <create_time label="时间" {...formItemLayout}>
                    <DatePicker.RangePicker />
                </create_time>
            </SearchForm>
        );
    }

    get columns() {

        const columns = [{ title: '#', render(text, record, index) { return index + 1; } },
        { title: '名称', dataIndex: 'Name' },
        { title: "标签", dataIndex: "Label" },
        { title: "状态", dataIndex: "Status", render: RenderHelper.render_status(Enums.CommonStyleStatus) },
        { title: '时间', dataIndex: 'CreateTime', render: RenderHelper.render_time },
        ];
        if (this.hasRights(Enums.SysRights.CMS_INSERT)) {
            columns.push({
                title: "操作", render: (it) => {
                    return (
                        <Button type="primary" size="small" onClick={() => this.onEdit(it)}>编辑</Button>
                    );
                }
            });
        }
        return columns;
    }
}

export default connect(state => ({
    isFetching: state.article.isFetching,
    category: state.article.Category,
}))(Category);