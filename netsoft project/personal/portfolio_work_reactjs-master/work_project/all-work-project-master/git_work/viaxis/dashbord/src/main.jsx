import React from 'react';
import ReactDOM from 'react-dom';
import Root from "./containers/root"
import getSocket from "./websocket";

const socket = getSocket();

ReactDOM.render(<Root />,
    document.getElementById('app'));

if (module.hot)
    module.hot.accept()