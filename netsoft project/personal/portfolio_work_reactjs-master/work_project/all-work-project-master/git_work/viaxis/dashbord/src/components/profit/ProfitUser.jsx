import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router";
import { message, Row, Col, Table, Button, Breadcrumb, Form, Input, Select, DatePicker } from 'antd';

import BasicComponent from "../common/BasicComponent";
import SearchForm from "../common/SearchForm";

import * as UserAction from '../../actions/user';
import * as ReportAction from '../../actions/report';
import * as RenderHelper from "../../utils/render";
import * as Utils from "../../utils/common";
import * as Enums from "../../app_enums";


class ProfitUser extends BasicComponent {

    constructor(props) {
        super(props);
        this.userId = this.getUserId(props);
        this.query = {};
    }

    /* ---------------------------- 系统函数 ------------------------------*/

    componentWillMount() {
        this.queryData();
    }

    componentWillReceiveProps(newProps) {
        const userId = this.getUserId(newProps);
        if (userId != this.userId) {
            this.userId = userId;
            this.query = Object.assign({}, this.query, { pageIndex: 1 });
            this.queryData();
        }
    }

    /* ---------------------------- Functions ----------------------------------- */

    queryData() {
        if (this.userId)
            this.props.dispatch(UserAction.getParents(this.userId));
        this.props.dispatch(ReportAction.queryProfitUser(this.userId, this.query))
    }

    getUserId(props) {
        let userId = props.params.userId;
        if (userId)
            userId = parseInt(userId);
        return userId;
    }

    get parents() {
        let parents = this.props.parents || null;
        if (parents == null || parents.length == 0)
            parents = [{ UserId: 1, UserKind: 0, Email: "Company", Backpct: 0.085 }];
        return parents;
    }

    goto(userId) {
        const url = `/profit/user/${userId}`;
        super.goto(url);
        if (this.query.email)
            delete this.query.email;
    }

    /* ---------------------------- Events -------------------------------------- */

    onSearchClick(values) {
        this.query = values;
        this.queryData();
    }

    onResetClick() {
        this.query = {};
    }

    /* ---------------------------- Render -------------------------------------- */

    render() {
        const profitUser = this.props.profitUser || {};
        const self = this;

        const pagination = {
            current: profitUser.pageIndex || 1,
            total: profitUser.count || 0,
            onChange(current) {
                self.query = Object.assign({}, self.query, { pageIndex: current });
                self.queryData();
            },
        };
        return (
            <div>
                {this.renderSearch()}
                {this.renderBreadcrumb()}
                <div className="search-result-list">
                    <Table columns={this.columns} dataSource={profitUser.data || []}
                        scroll={{ x: 1400 }}
                        loading={this.props.isFetching}
                        pagination={pagination} />
                </div>
            </div>
        );
    }

    renderSearch() {
        const formItemLayout = {
            labelCol: { span: 8 },
            wrapperCol: { span: 16 }
        };

        return (
            <SearchForm onSearch={this.onSearchClick.bind(this)} onReset={this.onResetClick.bind(this)}>
                <email label="邮箱" {...formItemLayout}>
                    <Input />
                </email>
                <create_time label="时间" {...formItemLayout} format={(times) => Utils.dateRangeFormat(times)}>
                    {RenderHelper.rangeDatePick()}
                </create_time>
            </SearchForm>
        );
    }

    renderBreadcrumb() {
        let parents = this.parents;

        if (parents == null)
            parents = []
        return (
            <Breadcrumb separator=">">
                {parents.map((it, i) => <Breadcrumb.Item key={i}><Link onClick={() => this.goto(it.UserId)}>[{Enums.UserKind[it.UserKind]}]{it.Email}</Link></Breadcrumb.Item>)}
            </Breadcrumb>
        );
    }

    get columns() {

        const columns = [{ title: '#', render(text, record, index) { return index + 1; } },
        {
            title: '邮箱', dataIndex: 'Email', render: (email, profit) => {
                if (profit.IsSum == 0)
                    return `[${Enums.UserKind[profit.UserKind]}] ${email}`;
                else
                    return <Link onClick={() => this.goto(profit.UserId)}>[{Enums.UserKind[profit.UserKind]}] {email}</Link>
            }
        },
        { title: "返点", dataIndex: "Backpct", render: RenderHelper.render_percent() },
        { title: "日工资", dataIndex: "DailyWages", render: RenderHelper.render_percent() },
        {
            title: "日期", render: it => {
                if (it.ToDate != it.Date)
                    return `${it.Date} - ${it.ToDate}`;
                return it.Date;
            }
        },
        { title: "订单数", dataIndex: "OrderCount", className: "column-money" },
        {
            title: "总投注", dataIndex: "TotalBet", className: "column-money", render: (it, profit) => {
                return <a target="_blank" href={`/dealinfo?email=${profit.Email}&load_all=1&from_time=${profit.Date}&to_time=${profit.ToDate}235959`}>{RenderHelper.money_format(it)}</a>
            }
        },
        { title: "有效投注", dataIndex: "ValidBet", className: "column-money", render: RenderHelper.money_format },
        { title: "奖金", dataIndex: "Bonus", className: "column-money", render: RenderHelper.money_format },
        { title: "佣金", dataIndex: "Commission", className: "column-money", render: RenderHelper.money_format },
        { title: "盈亏", dataIndex: "Profit", className: "column-money", render: RenderHelper.money_color },
        { title: "充值", dataIndex: "Deposit", className: "column-money", render: RenderHelper.money_format },
        { title: "提现", dataIndex: "Withdraw", className: "column-money", render: RenderHelper.money_format },
        { title: "充值数", dataIndex: "DepositCount", className: "column-money" },
        { title: "提现数", dataIndex: "WithdrawCount", className: "column-money" }
        ];

        return columns;
    }
}

export default connect(state => ({
    isFetching: state.report.isFetching,
    profitUser: state.report.ProfitUser,
    parents: state.user.Parents
}))(ProfitUser);