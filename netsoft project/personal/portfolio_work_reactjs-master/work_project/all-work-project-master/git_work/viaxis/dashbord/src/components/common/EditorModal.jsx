import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';

import { Spin, Modal, Form, Button } from 'antd';

const FormItem = Form.Item;

/**
 * 通用弹窗编辑组件
 * Example:
 * <EditorModal title="" visible={true} submiting={false} onSubmit={...} onClose={...}>
 *     <com_id label="label1" options={}>
 *         <Input placeholder="placeholder" />
 *     </com_id>
 *     <name label="label2">
 *         <Input placeholder="placeholder" />
 *     </name>
 * </EditorModal>
 */
class Editor extends Component {

    constructor(props) {
        super(props);
        this.state = {
            visible: props.visible || true,
            submiting: props.submiting || false
        }
    }

    /* ---------------------------- 系统函数 ------------------------------*/

    componentWillReceiveProps(nextProps) {
        if (nextProps.visible != this.state.visible) {
            this.setState({visible:nextProps.visible});
        }

        if (nextProps.submiting != this.state.submiting) {
            this.setState({submiting:nextProps.submiting});
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps != this.props || nextState != this.state;
    }


    /* ---------------------------- Events ------------------------------*/

    handleOk() {
        this.props.form.validateFields((errors, values) => {
            if (errors) return;
            this.props.onSubmit&&this.props.onSubmit(values);
        });
    }

    handleCancel(e) {
        this.setState({
            visible: false,
        });
        this.props.onClose && this.props.onClose();
    }

    /* ---------------------------- Renders ------------------------------*/
    get formItemLayout(){
        const item = {
            labelCol: { span: 7 },
            wrapperCol: { span: 12 },
        };
        return item;
    }

    renderFormItems() {
        const { getFieldDecorator } = this.props.form;

        let children = this.props.children;
        if (!children.length)
            children = [children];

        const childrens = [];

        for (let i = 0; i < children.length; i++) {
            let child = children[i];
            if (!child) continue;

            //如item后有附带标签
            const restchild = []
            for(let j = 0; j< child.props.children.length; j++) {
                if(j == 0) continue;
                restchild.push(child.props.children[j]);
            }

            childrens.push(
                <FormItem key={i} {...this.formItemLayout} label={child.props.label}>
                    {getFieldDecorator(child.type, child.props.options)(
                       child.props.children.length > 1 ? child.props.children[0] : child.props.children
                    )}
                    {restchild}
                </FormItem>
            );
        }

        return childrens;

    }

    render() {

        const { title, EditType, Groups } = this.props;

        return (
            <Modal visible={this.state.visible} title={title} onCancel={this.handleCancel.bind(this)}
                footer={[
                    <Button key="back" type="ghost" size="large" onClick={this.handleCancel.bind(this)}>取消</Button>,
                    <Button key="submit" type="primary" size="large" loading={this.state.submiting} onClick={this.handleOk.bind(this)}>
                      提交
                    </Button>
                ]}>
                <Spin spinning={this.props.loading||false}>
                    <Form horizontal>
                        {this.renderFormItems()}
                    </Form>
                </Spin>
            </Modal>
        );
    }

}

Editor.propTypes = {
    onSubmit: React.PropTypes.func,
    onClose: React.PropTypes.func,
};

const EditorModal = Form.create()(Editor);
export default EditorModal;