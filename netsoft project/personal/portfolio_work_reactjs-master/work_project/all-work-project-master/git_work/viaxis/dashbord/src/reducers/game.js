import * as app_const from "../app_const";
import ReduceBase from "./reduce_base";
import * as Utils from "../utils/common";


class GameReducer extends ReduceBase {
    static reduce(state = { isFetching: false }, action) {
        switch (action.type) {
            case app_const.QUERY_GAMEGROUP:
            case app_const.QUERY_GAMETYPE:
            case app_const.QUERY_GAMETYPEINFO:
            case app_const.QUERY_GAMEALGORITHM:
            case app_const.QUERY_GAMEDATA:
            case app_const.COUNT_FINISHED_UNOPEN_GAMEDATA:
            case app_const.QUERY_DEALINFO:
            case app_const.QUERY_PLAYLIMIT:
            case app_const.QUERY_REBATEREPORT:
            case app_const.QUERY_USERPROFIT:
                return Object.assign({}, state, {
                    isFetching: true
                });
            /* ----------------------- Game Group ---------------------- */
            case `${app_const.QUERY_GAMEGROUP}_SUCCESS`:
                return ReduceBase._list_success(state, action, "Groups");
            /* ----------------------- Game Algorithm ---------------------- */
            case `${app_const.QUERY_GAMEALGORITHM}_SUCCESS`:
                return ReduceBase._list_success(state, action, "Algorithm");
            case `${app_const.UPDATE_GAMEALGORITHM}_SUCCESS`:
                return GameReducer._update_gamealgorithm(state, action);
            /* ----------------------- Game Type ---------------------- */
            case `${app_const.QUERY_GAMETYPE}_SUCCESS`:
                return ReduceBase._query_success(state, action, "GameType");
            case `${app_const.QUERY_GAMETYPE_WITHOUTINFO}_SUCCESS`:
                return ReduceBase._list_success(state, action, "GameType");
            /* ----------------------- Game Type Info ---------------------- */
            case `${app_const.QUERY_GAMETYPEINFO}_SUCCESS`:
                return ReduceBase._query_success(state, action, "GameTypeInfo");
            /* ----------------------- Game Type Category ---------------------- */
            case `${app_const.QUERY_GAMETYPECATEGORY}_SUCCESS`:
                return ReduceBase._list_success(state, action, "GameTypeCategory");
            /* ----------------------- Game Type Sort ---------------------- */
            case `${app_const.QUERY_GAMETYPESORT}_SUCCESS`:
                return ReduceBase._list_success(state, action, "GameTypeSort");
            /* ----------------------- Game Type Kind ---------------------- */
            case `${app_const.QUERY_GAMETYPEKIND}_SUCCESS`:
                return ReduceBase._list_success(state, action, "GameTypeKind");
            /* ----------------------- Game Data ---------------------- */
            case `${app_const.QUERY_GAMEDATA}_SUCCESS`:
                return ReduceBase._query_success(state, action, "GameData");
            case `${app_const.GET_GAMEDATA}_SUCCESS`:
                return GameReducer._get_game_data(state, action);
            case `${app_const.COUNT_FINISHED_UNOPEN_GAMEDATA}_SUCCESS`:
                return GameReducer._list_success(state, action, "FinishedUnopen");
            /* ----------------------- DealInfo ---------------------- */
            case `${app_const.QUERY_DEALINFO}_SUCCESS`:
                return ReduceBase._query_success(state, action, "DealInfo");
            case `${app_const.QUERY_PLAYLIMIT}_SUCCESS`:
                return ReduceBase._query_success(state, action, "playlimit");
            case `${app_const.QUERY_REBATEREPORT}_SUCCESS`:
                return GameReducer._query_rebatereport_success(state, action);
            case `${app_const.QUERY_USERPROFIT}_SUCCESS`:
                return ReduceBase._query_success(state, action, "userprofit");
            case `${app_const.ADD_PLAYLIMIT}_SUCCESS`:
            case `${app_const.UPDATE_PLAYLIMIT}_SUCCESS`:
                return GameReducer._common_success(state, action);
            case `${app_const.QUERY_GAMEGROUP}_FAILURE`:
            case `${app_const.QUERY_GAMETYPE}_FAILURE`:
            case `${app_const.QUERY_GAMETYPEINFO}_FAILURE`:
            case `${app_const.QUERY_GAMEALGORITHM}_FAILURE`:
            case `${app_const.QUERY_GAMEDATA}_FAILURE`:
            case `${app_const.GET_GAMEDATA}_FAILURE`:
            case `${app_const.QUERY_DEALINFO}_FAILURE`:
            case `${app_const.QUERY_PLAYLIMIT}_FAILURE`:
            case `${app_const.QUERY_REBATEREPORT}_FAILURE`:
            case `${app_const.QUERY_USERPROFIT}_FAILURE`:
                return Object.assign({}, state, {
                    isFetching: false
                });
            default:
                return state;
        }
    }

    /* ----------------------- Game Data --------------------------- */
    static _get_game_data(state, action){

        const resp = action.response;
        if (resp && resp.code == 0) {

            let hash = state.GameHash || {};
            const data = resp.data;
            if(data == null)
                return state;
            let groupData = hash[data.GroupId]||{};
            groupData[data.Qh] = data;

            const updateHash = {};
            updateHash[data.GroupId] = groupData;

            hash = Object.assign({},hash,updateHash);
            return Object.assign({}, state, {GameHash:hash});

        }else{
            return ReduceBase._process_failure(state, action);
        }
    }

    /* ----------------------- Game Algorithm ---------------------------- */

    static _update_gamealgorithm(state, action) {
        let resp = action.response;
        if (resp.code == 0) {

            var algorithms = state.Algorithm;
            if (!algorithms)
                algorithms = [];

            const algorithm = algorithms.find(x=>x.Id==action.payload.id);
            if(!algorithm)return state;
            var update_index=algorithms.indexOf(algorithm);

            let new_algorithm = {
                Id: action.payload.id,
                Name: action.payload.name,
                Description: action.payload.description,
                Status: action.payload.status,
                TimeStamp: new Date().getTime() / 1000
            };

            algorithms = [
                ...algorithms.slice(0, update_index),
                new_algorithm,
                ...algorithms.slice(update_index + 1)
            ];

            return Object.assign({}, state, {
                isFetching: false,
                Algorithm: algorithms
            });

        } else
            return ReduceBase._process_failure(state, action);
    }

    static _query_rebatereport_success(state, action){
        let resp = action.response;
        if (resp.code == 0) {
            return Object.assign({}, state, {
                isFetching: false,
                rebatereports: resp.data
            });
        } else
            return GameReducer._process_failure(state, action);
    }

}

export { GameReducer };
