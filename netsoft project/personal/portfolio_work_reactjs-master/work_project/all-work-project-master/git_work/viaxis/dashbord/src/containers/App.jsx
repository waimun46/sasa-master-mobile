require('../../pages/css/style.css');
import React from 'react';
import ReactDOM from 'react-dom';
import { Link } from "react-router";
import { Menu, Breadcrumb, Icon, Tag, Dropdown } from "antd";
import { Menus, parseBreadcrumb } from "../slide_menu";
import { browserHistory } from 'react-router';

import BasicComponent from "../components/common/BasicComponent";
import UpdateUserPassword from "../components/user/UpdateUserPassword";
import WithdrawNotify from "../components/bill/WithdrawNotify";
import GameDataNotify from "../components/game/GameDataNotify";
import auth from "../utils/auth";
import * as Enums from "../app_enums";

const SubMenu = Menu.SubMenu;

class App extends BasicComponent {

	constructor(props) {
		super(props);
	}

	handleLogout() {
		auth.unauthorized();
	}

	render() {
		const { routes } = this.props;
		return (
			<div>
				<div className="ant-layout-aside">
					<aside className="ant-layout-sider">
						<div className="ant-layout-logo">
							<Link to="/">
								{auth.getstoragevalue('username')}
							</Link>
						</div>
						<Menu mode="inline" theme="dark">
							{Menus.map(it => {
								return (
									this.hasRights(it.rights) &&
									<SubMenu key={it.key} title={<span><Icon type={it.icon} />{it.name}</span>}>
										{it.children.map(x => {
											return this.hasRights(x.rights) && <Menu.Item key={x.key}><Link to={x.path}>{x.name}</Link></Menu.Item>
										})}
									</SubMenu>
								);
							})}
						</Menu>
					</aside>
					<div className="ant-layout-main">
						<div className="ant-layout-header">
							<a onClick={this.handleLogout.bind(this)}><Icon type="logout" /></a>
							<UpdateUserPassword />
							{this.hasRights(Enums.SysRights.BILL_INSERT) &&
								<WithdrawNotify />
							}
							{this.hasRights(Enums.SysRights.GAME_UPDATE) &&
								<GameDataNotify />
							}
						</div>
						<div className="ant-layout-breadcrumb">
							<Breadcrumb>
								{parseBreadcrumb(routes[routes.length - 1].path).map((it, index) => {
									return <Breadcrumb.Item key={index}>{it}</Breadcrumb.Item>
								})}
							</Breadcrumb>
						</div>
						<div className="ant-layout-container">
							<div className="ant-layout-content">
								<div style={{ minHeight: 590 }}>
									{this.props.children}
								</div>
							</div>
						</div>
						<div className="ant-layout-footer">
							帝国娱乐版权所有 © 2017 由帝国娱乐技术部支持
			        </div>
					</div>
				</div>
			</div>
		);
	}
}

export default App;
