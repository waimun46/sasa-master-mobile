import $ from "jquery";
import * as app_const from "../app_const";
import * as Utils from "../utils/common";


export function queryPlatformStatistic(startTime, endTime) {
    const payload = JSON.stringify({ startTime, endTime });
    return {
        name: app_const.QUERY_PLATFORM_STATISTIC,
        callAPI: () => Utils.POST(`/api/statistic/platform`, payload)()
    }
}

export function queryProfitUser(userId, query = {}) {
    const payload = JSON.stringify(query);
    const url = userId ? `/api/profit/user/${userId}` : `/api/profit/user`;
    return {
        name: app_const.QUERY_PROFIT_USER,
        callAPI: () => Utils.POST(url, payload)()
    }
}

export function queryProfitGameType(userId, query) {
    const payload = JSON.stringify(query);
    const url = userId ? `/api/profit/gametype/${userId}` : `/api/profit/gametype`;
    return {
        name: app_const.QUERY_PROFIT_GAME_TYPE,
        callAPI: () => Utils.POST(url, payload)()
    }
}

export function queryProfitGameTypeByUser(userId, query) {
    const payload = JSON.stringify(query);
    const url = userId ? `/api/profit/gametype/user/${userId}` : `/api/profit/gametype/user`;
    return {
        name: app_const.QUERY_PROFIT_GAME_TYPE_BY_USER,
        callAPI: () => Utils.POST(url, payload)()
    }
}

export function queryProfitDividend(userId, query) {
    const payload = JSON.stringify(query);
    const url = userId ? `/api/profit/dividend/${userId}` : `/api/profit/dividend`;
    return {
        name: app_const.QUERY_PROFIT_DIVIDEND,
        callAPI: () => Utils.POST(url, payload)()
    }
}