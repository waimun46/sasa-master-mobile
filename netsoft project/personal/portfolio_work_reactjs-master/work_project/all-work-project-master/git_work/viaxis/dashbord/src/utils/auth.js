import $ from "jquery";
import { sign } from 'jsonwebtoken';
import Config from "../config";

var auth = {
	login(username, password, callback) {
		if (this.isLogged()) {
			callback(true);
			return;
		}

		$.ajax({
			url: Config.API + "/api/passport/login",
			type: "POST",
			data: JSON.stringify({
				username,
				password
			}),
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			success: (response) => {
				if (response.code == 0 && response.data.User && response.data.Token != "") {
					sessionStorage.setItem("username", username);
					sessionStorage.setItem("user", JSON.stringify(response.data.User));
					sessionStorage.setItem("token", response.data.Token);
					callback(true);
				} else {
					callback(false, response.message);
				}
			}
		});
	},

	isLogged() {
		return !!sessionStorage.token;
	},

	getstoragevalue(name) {
		return sessionStorage.getItem(name);
	},

	removestorage() {
		sessionStorage.removeItem("token");
		sessionStorage.removeItem("username");
		sessionStorage.removeItem("user");
	},

	getAuthToken() {
		if (!this.isLogged()) return "";
		var user = JSON.parse(sessionStorage.user);
		if (user == null) return false;

		return "Bearer " + sign({ UserId: user.UserId }, sessionStorage.token);
	},

	hasRights(rights) {
		if (!this.isLogged()) return false;
		try {
			var user = JSON.parse(sessionStorage.user);
			if (user == null || !user.Rights) return false;

			return user.Rights.indexOf(rights.toLowerCase()) >= 0;
		}
		catch (err) {
			console.log(err);
			return false;
		}

	},

	/**
	 * 未登陆
	 * @return {[type]} [description]
	 */
	unauthorized() {
		this.removestorage();
		// 强制退出，确保残留的用户信息
		window.location.replace("/logout");
	},

	/**
	 * 已登陆，无权限
	 * @return {[type]} [description]
	 */
	forbidden() {

	}
};

export default auth;
export const Rights = {
	SYS_MANAGE: "sys.manage",
	UCENTER_SELECT: "ucenter.select",
	UCENTER_INSERT: "ucenter.insert",
	UCENTER_UPDATE: "ucenter.update",
	BILL_SELECT: "bill.select",
	BILL_INSERT: "bill.insert",
	BILL_UPDATE: "bill.update",
	GAME_SELECT: "game.select",
	GAME_INSERT: "game.insert",
	GAME_UPDATE: "game.update",
	ADMIN_SELECT: "admin.select",
	ADMIN_INSERT: "admin.insert",
	ADMIN_UPDATE: "admin.update"
};
