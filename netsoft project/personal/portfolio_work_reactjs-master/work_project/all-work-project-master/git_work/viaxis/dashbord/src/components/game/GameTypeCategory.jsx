import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { message, Spin, Badge, Tooltip, Row, Col, Table, Tag, Button, Modal, Form, Input, Select, DatePicker } from 'antd';

import EditorModal from "../common/EditorModal";

import * as GameAction from '../../actions/game';
import { dateFormat, isEmptyObject } from "../../utils/string";
import * as RenderHelper from "../../utils/render";
import { CommonStatus, CommonStyleStatus } from "../../app_enums";

const createForm = Form.create;
const FormItem = Form.Item;
const Option = Select.Option;
const RangePicker = DatePicker.RangePicker;
const Message = message;

class GameTypeCategory extends Component {

    constructor(props) {
        super(props);
        this.pageIndex = 1;
        this.query = {};
        this.state = {
            editType: null,
            submiting: false
        };
    }

    /* ---------------------------- 系统函数 ------------------------------*/

    componentDidMount() {
        this.queryData();
    }

    /* ---------------------------- 自定义函数 ------------------------------*/

    queryData() {
        this.props.dispatch(GameAction.queryGameTypeCategory(this.query)).then(act => {
            if (act.error)
                Message.error(act.error);
        });
    }

    addCategory() {
        this.setState({
            editType: {}
        });
    }

    updateCategory(category) {
        this.setState({
            editType: category
        });
    }

    /* ---------------------------- Events ------------------------------*/

    handleSubmit(values) {

        const { dispatch } = this.props;
        this.setState({ submiting: true });

        if (isEmptyObject(this.state.editType)) {
            dispatch(GameAction.addGameTypeCategory(values.name, values.weight)).then(act => {
                this.setState({
                    submiting: false
                });
                if (act.error || act.response.code != 0) {
                    Message.error(act.error || act.response.message);
                } else {
                    Message.success("添加成功");
                    this.setState({ editType: null });
                    this.queryData();
                }
            });
        } else {
            dispatch(GameAction.updateGameTypeCategory(this.state.editType.Id, values.name, values.weight, values.status)).then(act => {
                this.setState({ submiting: false });
                if (act.error || act.response.code != 0) {
                    Message.error(act.error || act.response.message);
                } else {
                    Message.success("修改成功");
                    this.setState({ editType: null });
                    this.queryData();
                }
            });
        }
    }

    onSearchClick(query) {
        this.query = query;
        this.queryData();
    }

    /* ---------------------------- Renders ------------------------------*/

    tableColumns() {
        const self = this;
        const columns = [{ title: '#', render(text, record, index) { return index + 1; } },
        { title: '名称', dataIndex: 'CategoryName' },
        { title: '权重', dataIndex: 'Weight' },
        { title: "状态", dataIndex: "Status", render: RenderHelper.render_status(CommonStyleStatus) },
        { title: '时间', dataIndex: 'CreateTime' },
        {
            title: "操作", render(type) {
                return <Button type="primary" onClick={() => self.updateCategory(type)}>编辑</Button>
            }
        }];

        return columns;
    }

    render() {
        const Categorys = this.props.Categorys || [];
        let columns = this.tableColumns();

        const pagination = {
            total: Categorys.length
        };
        return (
            <div>
                <div style={{ marginBottom: 16 }}>
                    <Button type="primary" loading={this.state.fetchingGroup} onClick={this.addCategory.bind(this)}>添加</Button>
                </div>
                <Table columns={columns} dataSource={Categorys || []} loading={this.props.isFetching} pagination={pagination} />
                {this.render_modal()}
            </div>
        );
    }

    render_modal() {
        const { editType } = this.state;
        if (!editType) return;

        const title = isEmptyObject(editType) ? "添加游戏目录" : "编辑" + editType.CategoryName;

        return (
            <EditorModal title={title} visible={true}
                submiting={this.state.submiting}
                onSubmit={this.handleSubmit.bind(this)}
                onClose={it => this.setState({ editType: null })}>
                <name label="目录分类名称" options={{
                    initialValue: editType.CategoryName || "",
                    rules: [{
                        required: true,
                        message: '请输入目录分类名称'
                    }]
                }}>
                    <Input type="name" autoComplete="off" />
                </name>
                <weight label="权重" options={{
                    initialValue: (editType.Weight || '0').toString(),
                    rules: [
                        { required: true, message: '请输入权重' }
                    ]
                }}>
                    <Input autoComplete="off" />
                </weight>
                {!isEmptyObject(editType) &&
                    <status label="状态" options={{
                        initialValue: (editType.Status || 0).toString(),
                        rules: [{
                            required: true,
                            message: '请选择状态'
                        }]
                    }}>
                        <Select>
                            {Object.keys(CommonStatus).map(it => {
                                return <Option key={it} value={it}>{CommonStatus[it]}</Option>;
                            })}
                        </Select>
                    </status>}
            </EditorModal>
        );
    }
}

export default connect(state => ({
    Categorys: state.game.GameTypeCategory,
}))(GameTypeCategory);
