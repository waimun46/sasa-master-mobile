import configureStore from './configure_store'
import Config from "./config";
import Auth from "./utils/auth";
import { ActionState } from "./app_const";
import notify from "./utils/notify";

class Socket {

    constructor() {
        this.conn = null;
        this.store = configureStore();
        this._time_tick = null;
    }

    connect() {
        if (this.conn == null || this.conn.readyState != 1)
            this.reconnect();
    }

    reconnect() {
        let url = `${Config.WSUrl}?token=${Auth.getAuthToken()}`;
        this.conn = new WebSocket(url);
        this.conn.onopen = function () {
            // WebSocket每次建立连接成功，即发送sid,建立身份
            //this.init();
        }.bind(this);

        this.conn.onmessage = function (e) {
            var data = null;
            try {
                data = JSON.parse(e.data);
                if (Config.Debug) {
                    console.log(data);
                }
            } catch (ex) {
                console.log("parse json failed.", ex);
                return;
            }
            this._process_receive_data(data);
            // this.events.trigger(data.command, data.data, data.error_code, data.message);

        }.bind(this);

        this.conn.error = function (event) {
            //WebSocket Status:: Error was reported
            console.log("Error");
        }.bind(this);

        this.conn.onclose = this.disconnect.bind(this);
    }

    disconnect() {
        let self = this;
        setTimeout(function () {
            self.reconnect();
        }, 500);
    }

    send(command, data = {}) {
        if (this.conn == null || this.conn.readyState > 1)
            this.reconnect();
        var opt = {
            command,
            data
        };
        if (this.conn.readyState == 1)
            this.conn.send(JSON.stringify(opt));
        else
            this.waitForSocketConnection(() => this.conn.send(JSON.stringify(opt)));
    }

    _process_receive_data(data) {
        if (data.command == "Login")
            Auth.unauthorized();
        else if (data.command.endsWith("Notify")) {
            notify(data.command, data.data);
        }
        else
            this.store.dispatch({
                type: data.command,
                state: ActionState.Success,
                response: {
                    data: data.data,
                    code: data.error_code,
                    message: data.message
                }
            });
    }

    waitForSocketConnection(callback) {
        setTimeout(
            function () {
                if (this.conn.readyState === 1) {
                    if (callback != null) {
                        callback();
                    }
                } else if (this.conn.readyState > 1) {
                    this.reconnect();
                    this.waitForSocketConnection(callback);
                } else {
                    this.waitForSocketConnection(callback);
                }
            }.bind(this), 5);
    }
}

export default function getSocket() {
    if (window._WEBSOCKET == null || window._WEBSOCKET == undefined) {
        window._WEBSOCKET = new Socket();
        window._WEBSOCKET.connect();
    }
    return window._WEBSOCKET;
}