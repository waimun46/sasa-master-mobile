import React from 'react';
import { connect } from 'react-redux';
import { message, Table, Tag, Button, Modal, Form, Input, Select, Switch, Spin, Row, Col } from 'antd';

import BasicComponent from "../common/BasicComponent";
import SearchForm from "../common/SearchForm";

import { loadRoleUsers, loadRoles, addRoleUser, removeRoleUser } from '../../actions/rights';
import { dateFormat } from "../../utils/string";

const Message = message;


class RoleUserList extends BasicComponent {

    constructor(props) {
        super(props);
        this.query = {};
    }

    /* --------------------------- Sys ---------------------------- */
    componentWillMount() {
        this.props.dispatch(loadRoles());
        this.queryData();
    }

    /* --------------------------- Functions ------------------------------ */
    queryData() {
        this.props.dispatch(loadRoleUsers(this.query)).then(act => {
            if (act.error)
                Message.error(act.error);
        });
    }

    /* --------------------------- Events -------------------------------- */

    onSearchClick(values) {
        this.query = values;
        this.queryData();
    }

    onResetClick() {

    }


    /* --------------------------- Renders ------------------------------- */

    render() {
        const { role, users, dispatch } = this.props;

        const pagination = {
            total: users.data.length,
        };

        return (
            <div>
                <div style={{ marginBottom: 16 }}>
                    <UserRoleAdd role={role} dispatch={dispatch} />
                </div>
                {this.renderSearch()}
                <div className="search-result-list">
                    <Table columns={this.columns} dataSource={users.data} loading={users.isFetching} pagination={pagination} rowKey="UserID" />
                </div>
            </div>
        );
    }

    get columns() {
        const { role, users, dispatch } = this.props;
        const columns = [
            { title: 'Email', dataIndex: 'Email' },
            {
                title: '角色', render(user) {
                    const roles = user.Roles;
                    let removeRole = (x) => {
                        dispatch(removeRoleUser(x.Id, user.UserId));
                    };
                    return roles.map((role) => <Tag closable color="#40a5ed" key={role.Id} onClose={() => removeRole(role)}> {role.Name} </Tag>);
                }
            },
            {
                title: "操作", render(user) {
                    return <RoleAdd user={user} role={role} dispatch={dispatch} />
                }
            }
        ];
        return columns;
    }

    renderSearch() {
        const formItemLayout = {
            labelCol: { span: 8 },
            wrapperCol: { span: 16 }
        };

        return (
            <SearchForm onSearch={this.onSearchClick.bind(this)} onReset={this.onResetClick.bind(this)}>
                <email label="邮箱" {...formItemLayout}>
                    <Input />
                </email>
                <role_name label="角色" {...formItemLayout} options={{ initialValue: "" }}>
                    <Select>
                        <Select.Option value="">全部</Select.Option>
                        {this.props.role.data.map(x => {
                            return <Select.Option value={x.Name} key={x.Id}>{x.Name}</Select.Option>
                        })}
                    </Select>
                </role_name>
            </SearchForm>
        );
    }
}

class UserRoleAddComponent extends BasicComponent {
    constructor() {
        super();
        this.state = { visible: false, loading: false };
    }
    isLoading(isLoading) {
        this.setState({ loading: isLoading });
    }
    isShow(visible) {
        this.setState({ visible: visible });
    }
    handleClick() {
        const { role } = this.props;
        if (role.data.length == 0) {
            this.isLoading(true);
            this.isShow(false);
            this.props.dispatch(loadRoles()).then(() => { this.isLoading(false); this.isShow(true); });
            return;
        }
        this.isShow(true);
    }
    handleSubmit() {
        this.props.form.validateFields((errors, values) => {
            if (errors) { return; }
            const { user } = this.props;
            const form = this.props.form.getFieldsValue();
            this.isLoading(true);
            this.props.dispatch(addRoleUser(parseInt(form.role), 0, form.email)).then((action) => {
                this.isLoading(false);
                if (action.error)
                    Message.error(action.error);
                else
                    this.isShow(false);
            });
        });
    }
    handleCancel() {
        this.isShow(false);
    }
    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <div>
                <Button type="primary" loading={this.state.loading} onClick={this.handleClick.bind(this)}>添加用户</Button>
                {this.state.visible && <Modal visible={this.state.visible} title="添加用户角色" onCancel={this.handleCancel.bind(this)} footer={[
                    <Button key="cancel" type="ghost" size="large" onClick={this.handleCancel.bind(this)}>取消</Button>,
                    <Button key="submit" type="primary" size="large" loading={this.state.loading} onClick={this.handleSubmit.bind(this)}>
                        确定
            </Button>,
                ]}>
                    <Form horizontal>
                        <Form.Item id="email" label="Email" labelCol={{ span: 6 }} wrapperCol={{ span: 14 }}>
                            {getFieldDecorator("email", {
                                rules: [
                                    { required: true, whitespace: false, message: "请输入用户Email" }
                                ]
                            })(
                                <Input autoComplete="off" />
                            )}
                        </Form.Item>
                        <Form.Item id="role" label="角色" labelCol={{ span: 6 }} wrapperCol={{ span: 14 }}>
                            {getFieldDecorator("role", {
                                rules: [
                                    { required: true, whitespace: false, message: "请选择要添加的角色" }
                                ]
                            })(
                                <Select optionFilterProp="children" notFoundContent="找不到内容">
                                    {this.props.role.data.map(x => {
                                        return <Option value={x.Id.toString()} key={x.Id}>{x.Name}</Option>
                                    })}
                                </Select>
                            )}
                        </Form.Item>
                    </Form>
                </Modal>
                }
            </div>
        );
    }
}

class RoleAddComponent extends BasicComponent {
    constructor() {
        super();
        this.state = { visible: false, loading: false };
        this.render_cb = [];
    }

    /* ---------------- Sys ------------------ */
    componentDidMount() {
        let cb = this.render_cb.pop()
        while (cb) {
            cb();
            cb = this.render_cb.pop();
        }
    }

    /* ----------------- Functions ------------------- */
    isLoading(isLoading) {
        this.setState({ loading: isLoading });
    }
    isShow(visible) {
        this.setState({ visible: visible });
    }

    /* -------------------- Events ------------------- */
    handleClick() {
        const { role } = this.props;
        if (role.data.length == 0) {
            this.isLoading(true);
            this.isShow(false);
            this.props.dispatch(loadRoles()).then(() => { this.isLoading(false); this.isShow(true); });
            return;
        }
        this.isShow(true);
    }
    handleSubmit(e) {
        this.props.form.validateFields((errors, values) => {
            if (errors) { return; }
            const { user } = this.props;
            const form = this.props.form.getFieldsValue();
            this.isLoading(true);
            this.props.dispatch(addRoleUser(parseInt(form.role), user.UserId)).then((action) => {
                this.isLoading(false);
                this.isShow(false);
                if (action.error)
                    Message.error(action.error);
            });
        });
    }
    handleCancel(e) {
        this.isShow(false);
    }

    /* -------------------------- Renders --------------------- */

    render() {
        const { role, user } = this.props;
        const { getFieldDecorator } = this.props.form;
        const btn = <Button type="primary" size="small" loading={this.state.loading} onClick={this.handleClick.bind(this)}>添加角色</Button>;
        if (this.state.visible && role.data.length == 0) {
            this.render_cb.push(() => this.isLoading(true));
            this.render_cb.push(() => this.isShow(false));
            this.props.dispatch(loadRoles()).then((e) => { this.isLoading(false); this.isShow(true); return e; });
            return btn;
        }
        const userRoles = user.Roles.map(x => x.Id);
        return (
            <div>
                {btn}
                {this.state.visible && <Modal visible={this.state.visible} title={user.Email + "添加角色"} onCancel={this.handleCancel.bind(this)} footer={[
                    <Button key="cancel" type="ghost" size="large" onClick={this.handleCancel.bind(this)}>取消</Button>,
                    <Button key="submit" type="primary" size="large" loading={this.state.loading} onClick={this.handleSubmit.bind(this)}>
                        确定
                    </Button>,
                ]}>
                    <Form horizontal>
                        <Form.Item id="role" label="角色" labelCol={{ span: 6 }} wrapperCol={{ span: 14 }}>
                            {getFieldDecorator("role", {
                                rules: [
                                    { required: true, whitespace: false, message: "请选择要添加的角色" }
                                ]
                            })(
                                <Select optionFilterProp="children" notFoundContent="找不到内容">
                                    {role.data.filter(x => userRoles.indexOf(x.Id) < 0).map(x => {
                                        return <Option value={x.Id.toString()} key={x.Id}>{x.Name}</Option>
                                    })}
                                </Select>
                            )}
                        </Form.Item>
                    </Form>
                </Modal>
                }
            </div>
        );
    }
}


const UserRoleAdd = Form.create()(UserRoleAddComponent);
const RoleAdd = Form.create()(RoleAddComponent);


export default connect(state => ({
    users: state.role_users,
    role: state.role,
}))(RoleUserList);
