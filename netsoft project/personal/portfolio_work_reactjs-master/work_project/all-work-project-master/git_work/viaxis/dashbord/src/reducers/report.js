import * as app_const from "../app_const";
import ReduceBase from "./reduce_base";
import * as Utils from "../utils/common";

class ReportReducer extends ReduceBase {
    static reduce(state = { isFetching: false }, action) {
        switch (action.type) {
            case app_const.QUERY_PLATFORM_STATISTIC:
            case app_const.QUERY_PROFIT_USER:
            case app_const.QUERY_PROFIT_GAME_TYPE:
            case app_const.QUERY_PROFIT_GAME_TYPE_BY_USER:
            case app_const.QUERY_PROFIT_DIVIDEND:
                return Object.assign({},state,{isFetching:true});
            case `${app_const.QUERY_PLATFORM_STATISTIC}_SUCCESS`:
                return ReportReducer._list_success(state, action, "PlatformStatistic");
            case `${app_const.QUERY_PROFIT_USER}_SUCCESS`:
                return ReportReducer._query_success(state, action, "ProfitUser");
            case `${app_const.QUERY_PROFIT_GAME_TYPE}_SUCCESS`:
            case `${app_const.QUERY_PROFIT_GAME_TYPE_BY_USER}_SUCCESS`:
                return ReportReducer._query_success(state, action, "ProfitGameType");
            case `${app_const.QUERY_PROFIT_DIVIDEND}_SUCCESS`:
                return ReportReducer._list_success(state, action, "ProfitDividend");
            default:
                return state;
        }
    }

}

export { ReportReducer };