import React from 'react';
import { connect } from 'react-redux';
import { message, Table, Tag, Button, Modal, Form, Input, Select } from 'antd';
import { loadRights, updateRights, addRights } from '../../actions/rights';
import { dateFormat } from "../../utils/string";
import auth, { Rights } from "../../utils/auth";

const createForm = Form.create;
const FormItem = Form.Item;
const Option = Select.Option;
const Message = message;

class RightsList extends React.Component {
	componentDidMount() {
		this.props.dispatch(loadRights(this.props.pageIndex)).then(act => {
			if (act.error)
				Message.error(act.error);
		});
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.pageIndex !== this.props.pageIndex) {
			this.props.dispatch(loadRights(nextProps.pageIndex));
		}
	}

	render() {

		const { rights, dispatch } = this.props;

		let render_acts = (rights) => {
			const RightsEdit = createForm()(RightsEditComponent);
			return (
				<RightsEdit rights={rights} dispatch={this.props.dispatch} />
			);
		};
		render_acts = render_acts.bind(this);
		const columns = [{
			title: '名称',
			dataIndex: 'Name'
		},
		{
			title: '说明',
			dataIndex: 'Description'
		},
		{
			title: '时间',
			dataIndex: 'CreateTime',
		}, {
			title: "状态",
			dataIndex: "Status",
			render(status) {
				return <Tag color={status == 0 ? '#40a5ed' : '#FF0000'}>{status == 0 ? "启用" : "禁用"}</Tag>
			}
		}, {
			title: "操作",
			render: render_acts
		}];

		var rightsList = rights.data;


		const pagination = {
			total: rightsList.length
		};

		const rowKey = function (rights) {
			return rights.ID;  //主键ID
		};

		return (
			<div>
				{auth.hasRights(Rights.ADMIN_INSERT) &&
					<div style={{ marginBottom: 16 }}>
						<RightsAdd dispatch={dispatch} />
					</div>
				}
				<Table columns={columns} dataSource={rightsList} loading={rights.isFetching} pagination={pagination} rowKey={rowKey} />
			</div>
		);
	}
}

class RightsAddComponent extends React.Component {
	constructor() {
		super();
		this.state = { visible: false, loading: false };
	}

	isLoading(isLoading) {
		this.setState({ loading: isLoading });
	}

	isShow(visible) {
		this.setState({ visible: visible });
	}

	handleCancel() {
		this.isShow(false);
	}
	handleSubmit() {
		this.props.form.validateFields((errors, values) => {
			if (errors) { return; }

			const form = this.props.form.getFieldsValue();
			this.isLoading(true);
			this.props.dispatch(addRights(form.name, form.description)).then((action) => {
				this.isLoading(false);
				if (action.error)
					Message.error(action.error);
				else {
					this.isShow(false);
					Message.success("添加成功");
				}
			});

		});
	}

	render() {
		const { getFieldDecorator } = this.props.form;
		return (
			<div>
				<Button type="primary" onClick={() => this.isShow(true)}>添加权限</Button>
				{this.state.visible && <Modal visible={this.state.visible} title="添加权限" onCancel={this.handleCancel.bind(this)} footer={[
					<Button key="cancel" type="ghost" size="large" onClick={this.handleCancel.bind(this)}>取消</Button>,
					<Button key="submit" type="primary" size="large" loading={this.state.loading} onClick={this.handleSubmit.bind(this)}>
						确定
                    </Button>,
				]}>
					<Form horizontal>
						<FormItem id="name" label="权限名称" labelCol={{ span: 6 }} wrapperCol={{ span: 14 }}>
							{getFieldDecorator("name", {
								rules: [
									{ required: true, whitespace: false, message: "请输入权限名称" }
								]
							})(
								<Input autoComplete="off" />
							)}
						</FormItem>
						<FormItem id="description" label="说明" labelCol={{ span: 6 }} wrapperCol={{ span: 14 }}>
							{getFieldDecorator("description", {
								rules: [
									{ required: true, whitespace: false, message: "请输入说明" }
								]
							})(
								<Input autoComplete="off" />
							)}
						</FormItem>
					</Form>
				</Modal>
				}
			</div>
		);
	}
}

const RightsAdd = createForm()(RightsAddComponent);

class RightsEditComponent extends React.Component {

	constructor() {
		super();
		this.state = { visible: false, loading: false };
	}

	handleClick(e) {
		this.setState({
			visible: true,
		});
	}

	handleSubmit(e) {
		this.props.form.validateFields((errors, values) => {
			if (errors) { return; }
			const { rights } = this.props;
			const form = this.props.form.getFieldsValue();
			this.setState({ loading: true });
			this.props.dispatch(updateRights(rights.Id, form.name, form.description, form.status)).then((act) => {
				if (act.response.code == 0)
					Message.success("操作成功");
				else
					Message.error(act.response.message);

			});
		});
	}

	handleCancel(e) {
		this.hideModal();
	}

	hideModal() {
		this.setState({
			visible: false,
		});
	}

	render() {
		const { rights } = this.props;
		if (rights.Updating == undefined) rights.Updating = false;
		const { getFieldDecorator } = this.props.form;
		return (
			<div>
				<Button type="primary" size="small" onClick={this.handleClick.bind(this)}>编辑</Button>
				<Modal visible={this.state.visible} title={"编辑" + rights.Name} onCancel={this.handleCancel.bind(this)} footer={[
					<Button key="cancel" type="ghost" size="large" onClick={this.handleCancel.bind(this)}>取消</Button>,
					<Button key="submit" type="primary" size="large" loading={this.state.loading} onClick={this.handleSubmit.bind(this)}>
						确定
            		</Button>,
				]}>
					<Form horizontal>
						<FormItem id="name" label="名称" labelCol={{ span: 6 }} wrapperCol={{ span: 14 }}>
							{getFieldDecorator("name", {
								initialValue: rights.Name,
								rules: [
									{ required: true, whitespace: false, message: "请填写名称" }
								]
							})(
								<Input autoComplete="off" />
							)}
						</FormItem>
						<FormItem id="description" label="说明" labelCol={{ span: 6 }} wrapperCol={{ span: 14 }}>
							{getFieldDecorator("description", { initialValue: rights.Description })(
								<Input autoComplete="off" />
							)}
						</FormItem>
						<FormItem id="{status}" label="状态" labelCol={{ span: 6 }} wrapperCol={{ span: 14 }}>
							{getFieldDecorator("status", { initialValue: rights.Status.toString() })(
								<Select size="large" style={{ width: 200 }}>
									<Option value="0">启用</Option>
									<Option value="1">禁用</Option>
								</Select>
							)}
						</FormItem>
					</Form>
				</Modal>
			</div>
		);
	}
}

export default connect(state => ({
	rights: state.rights
}))(RightsList);
