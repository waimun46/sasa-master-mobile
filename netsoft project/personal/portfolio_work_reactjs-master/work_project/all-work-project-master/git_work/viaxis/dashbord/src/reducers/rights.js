import ReduceBase from "./reduce_base";
import * as app_const from "../app_const"
import * as Utils from "../utils/common";

class RoleReducer extends ReduceBase {
    static reduce(state = { isFetching: false, pageIndex: 1, count: 0, data: [] }, action) {
        switch (action.type) {
            case app_const.LOAD_ROLES:
                return Object.assign({}, state, {
                    isFetching: true
                });
            case `${app_const.LOAD_ROLES}_SUCCESS`:
                return RoleReducer._load_roles_success(state, action);
            case app_const.LOAD_ROLES_FAILURE:
                return Object.assign({}, state, {
                    isFetching: false,
                    error: action.error
                });
            case `${app_const.ADD_ROLE}_SUCCESS`:
                return RoleReducer._add_role_success(state, action);
            case `${app_const.GET_ROLE_RIGHTS}_SUCCESS`:
                return RoleReducer._get_role_rights_success(state, action);
            case `${app_const.UPDATE_ROLE_RIGHTS}_SUCCESS`:
                return RoleReducer._update_role_rights_success(state, action);
            default: return state;
        }
    }

    static _add_role_success(state, action) {
        const resp = action.response;
        if (resp.code == 0) {
            var data = [
                resp.data,
                ...state.data
            ];
            return Object.assign({}, state, {
                data
            });
        } else {
            action.error = resp.message;
            action.error_code = resp.code;
            return state;
        }
    }

    static _load_roles_success(state, action) {
        if (action.response.code == 0) {
            const resp = action.response;
            return Object.assign({}, state, {
                isFetching: false,
                pageIndex: resp.data.PageIndex,
                count: resp.data.TotalCount,
                data: resp.data.Data
            })
        }
        return state;
    }

    static _get_role_rights_success(state, action) {
        if (action.response.code == 0) {
            const resp = action.response;
            const role = state.data.find(x => x.ID == action.payload.roleId);
            if (!role) return state;

            var update_index = state.data.indexOf(role);

            let data = [
                ...state.data.slice(0, update_index),
                Object.assign({}, role, {
                    Rights: resp.data
                }),
                ...state.data.slice(update_index + 1)
            ];
            return Object.assign({}, state, { data });
        }
        return state;
    }

    static _update_role_rights_success(state, action) {
        if (action.response.code == 0) {
            const rights = state.data.find(x => x.Id == action.payload.id);
            if (!rights) return state;

            const resp = action.response;
            var update_index = state.data.indexOf(rights);

            let update_role = {
                Name: action.payload.name,
                Description: action.payload.description,
                Status: action.payload.status
            };
            if (action.rights)
                update_role.Rights = null;

            let data = [
                ...state.data.slice(0, update_index),
                Object.assign({}, rights, update_role),
                ...state.data.slice(update_index + 1)
            ];
            return Object.assign({}, state, { data });
        } else {
            action.error = action.response.message;
            action.error_code = action.response.code;
            return state;
        }
    }
}

class RightsReducer extends ReduceBase {
    /**
     * rights reducer
     * @param  {Object} state  rights数据 {isFetching:bool,pageIndex:int,count:int,data:[{ID:int,Name:str,Description:str,Status:int,TimeStamp:int}]}
     * @param  {[type]} action [description]
     * @return {[type]} state  reduce 后的state
     */
    static reduce(state = { isFetching: false, pageIndex: 1, count: 0, data: [] }, action) {
        switch (action.type) {
            case app_const.LOAD_RIGHTS:
                return Object.assign({}, state, {
                    isFetching: true,
                    message: null
                });
            case `${app_const.LOAD_RIGHTS}_SUCCESS`:
                return RightsReducer._load_rights_success(state, action);
            case app_const.LOAD_RIGHTS_FAILURE:
                return Object.assign({}, state, {
                    isFetching: false,
                    error: action.error
                });
            case `${app_const.ADD_RIGHTS}_SUCCESS`:
                return RightsReducer._add_rights_success(state, action);
            // case app_const.UPDATE_RIGHTS:
            //  return RightsReducer._update_rights(state,action);
            case `${app_const.UPDATE_RIGHTS}_SUCCESS`:
                return RightsReducer._update_rights_success(state, action);
            default: return state;
        }
    }

    /**
     * 获取rights数据列表成功回调
     * @param  {[type]} state  [description]
     * @param  {[type]} action [description]
     * @return {[type]}        [description]
     */
    static _load_rights_success(state, action) {
        if (action.response.code == 0) {
            const resp = action.response;
            action.cb && action.cb(true, resp.message, resp.code);
            return Object.assign({}, state, {
                /**
                 * rights是否加载中
                 * @type {Boolean}
                 */
                isFetching: false,
                /**
                 * 当前数据页码
                 * @type {int}
                 */
                pageIndex: resp.data.PageIndex,
                /**
                 * 数据总量
                 * @type {int}
                 */
                count: resp.data.TotalCount,
                /**
                 * 数据列表
                 * @type {[type]}
                 */
                data: resp.data.Data
            })
        }
        return action.cb && action.cb(false, resp.message, resp.code);
        return state;
    }

    static _add_rights_success(state, action) {
        const resp = action.response;
        if (resp.code == 0) {
            var data = [
                resp.data,
                ...state.data
            ];
            return Object.assign({}, state, {
                data
            });
        } else {
            action.error = resp.message;
            action.error_code = resp.code;
            return state;
        }
    }

    /**
     * 修改rights成功回调
     * @param  {[type]} state  [description]
     * @param  {[type]} action [description]
     * @return {[type]}        [description]
     */
    static _update_rights_success(state, action) {
        if (action.response.code == 0) {
            const rights = state.data.find(x => x.Id == action.payload.id);
            if (!rights) return state;

            const resp = action.response;
            var update_index = state.data.indexOf(rights);

            let data = [
                ...state.data.slice(0, update_index),
                Object.assign({}, rights, {
                    Name: action.payload.name,
                    Description: action.payload.description,
                    Status: action.payload.status
                }),
                ...state.data.slice(update_index + 1)
            ];
            action.cb && action.cb(true);
            return Object.assign({}, state, { data });
        } else {
            action.cb && action.cb(false, resp.message, resp.code);
            return state;
        }
    }
}

class RoleUserReducer {
    static reduce(state = { isFetching: false, data: [] }, action) {
        switch (action.type) {
            case app_const.LOAD_ROLE_USERS:
                return Object.assign({}, state, {
                    isFetching: true
                });
            case `${app_const.LOAD_ROLE_USERS}_SUCCESS`:
                return RoleUserReducer._load_role_users_success(state, action);
            case `${app_const.ADD_ROLE_USER}_SUCCESS`:
                return RoleUserReducer._add_role_user_success(state, action);
            case `${app_const.REMOVE_ROLE_USER}_SUCCESS`:
                return RoleUserReducer._remove_role_user_success(state, action);
            default:
                return state;
        }
    }

    static _load_role_users_success(state, action) {
        const resp = action.response;
        if (resp.code == 0) {
            return Object.assign({}, state, {
                isFetching: false,
                data: resp.data
            });
        } else {
            action.error = resp.message;
            action.error_code = resp.code;
            return state;
        }
    }

    static _add_role_user_success(state, action) {
        const resp = action.response;
        if (resp.code == 0) {
            let data = resp.data;
            return Object.assign({}, state, { data });
        } else {
            action.error = resp.message;
            action.error_code = resp.code;
            return state;
        }
    }

    static _remove_role_user_success(state, action) {
        const resp = action.response;
        if (resp.code == 0) {
            let data = resp.data;
            return Object.assign({}, state, { data });
        } else {
            action.error = resp.message;
            action.error_code = resp.code;
            return state;
        }
    }
}


export {
    RoleReducer,
    RightsReducer,
    RoleUserReducer
};