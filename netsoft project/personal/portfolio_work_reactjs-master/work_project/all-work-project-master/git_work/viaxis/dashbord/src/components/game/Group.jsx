import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { message, Table, Button, Modal, Form, Input, Select } from 'antd';

import BasicComponent from "../common/BasicComponent";

import { queryGameGroup } from '../../actions/game';
import { isEmptyObject } from "../../utils/string";
import * as RenderHelper from "../../utils/render";
import { CommonStatus, CommonStyleStatus, SysRights } from "../../app_enums";

const FormItem = Form.Item;
const Option = Select.Option;
const Message = message;

class GameGroup extends BasicComponent {

    constructor(props) {
        super(props);
        this.query = {};
        this.state = {
            loading: false,
            popup: false,
            popupTitle: "",
            edit_group: {}
        };
    }

    /* ---------------------------- 系统函数 ------------------------------*/

    componentDidMount() {
        this.queryData();
    }

    /* ---------------------------- 自定义函数 ------------------------------*/

    queryData() {
        this.props.dispatch(queryGameGroup(this.query)).then(act => {
            if (act.error)
                Message.error(act.error);
        });
    }

    updateGroup(group) {
        this.setState({
            popup: true,
            popupTitle: "编辑" + group.Name,
            edit_group: group
        });
    }

    handleSubmit(e) {
        const { dispatch } = this.props;
        e.preventDefault();
        this.props.form.validateFields((errors, values) => {
            if (errors) return;

        });
    }

    handleReset(e) {
        e.preventDefault();
        this.props.form.resetFields();
    }

    hideModal() {
        this.setState({ popup: false });
    }

    /* ---------------------------- Renders ------------------------------*/

    tableColumns() {
        const self = this;
        const columns = [{ title: '#', render(text, record, index) { return index + 1; } },
        { title: '名称', dataIndex: 'Name' },
        { title: "状态", dataIndex: "Status", render: RenderHelper.render_status(CommonStyleStatus) },
        { title: '时间', dataIndex: 'CreateTime', render: RenderHelper.render_time },
        ];

        if (this.hasRights(SysRights.GAME_UPDATE)) {
            columns.push({
                title: "操作", render(group) {
                    return <Button type="primary" onClick={() => self.updateGroup(group)}>编辑</Button>
                }
            });
        }


        return columns;
    }

    render() {
        let groups = this.props.data || [];

        let columns = this.tableColumns();

        const rowKey = function (group) {
            return group.Id
        }

        return (
            <div>
                <Table columns={columns} dataSource={groups} loading={this.props.isFetching} />
                {this.render_modal()}
            </div>
        );
    }

    render_modal() {
        if (isEmptyObject(this.state.edit_group)) return;

        const self = this;
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: { span: 7 },
            wrapperCol: { span: 12 },
        };
        return (
            <Modal title={this.state.popupTitle} visible={this.state.popup} onCancel={this.hideModal.bind(this)}
                footer={[
                    <Button key="back" type="ghost" size="large" onClick={this.hideModal.bind(this)}>取消</Button>,
                    <Button key="submit" type="primary" size="large" loading={this.state.loading} onClick={this.handleSubmit.bind(this)}>
                        提交
                    </Button>,
                ]}>
                <Form horizontal>
                    <FormItem {...formItemLayout} label="彩种名称">
                        {getFieldDecorator('name', {
                            initialValue: (this.state.edit_group.Name || "").toString(),
                            rules: [{
                                required: true,
                                message: '请输入彩种名称'
                            }]
                        })(
                            <Input type="name" autoComplete="off" />
                        )}
                    </FormItem>
                    <FormItem {...formItemLayout} label="状态">
                        {getFieldDecorator('status', {
                            initialValue: (this.state.edit_group.Status || 0).toString(),
                            rules: [{
                                required: true,
                                message: '请选择彩种状态'
                            }]
                        })(
                            <Select>
                                {Object.keys(CommonStatus).map(it => {
                                    return <Option key={it} value={it}>{CommonStatus[it]}</Option>;
                                })}
                            </Select>
                        )}
                    </FormItem>
                </Form>
            </Modal>
        );
    }
}


export default connect(state => ({
    isFetching: state.game.isFetching,
    data: state.game.Groups,
}))(Form.create()(GameGroup));
