import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { message, Modal, Table, Button, Form, Input, Checkbox, Radio, Select } from 'antd';

import SearchForm from "../common/SearchForm";
import EditorModal from "../common/EditorModal";

import * as GameAction from '../../actions/game';
import * as RenderHelper from "../../utils/render";
import { dateFormat, isEmptyObject } from "../../utils/string";
import { CommonStatus, CommonStyleStatus } from "../../app_enums";

const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;
const Option = Select.Option;
const OptGroup = Select.OptGroup;
const Message = message;

class GameType extends Component {

    constructor(props) {
        super(props);
        this.pageIndex = 1;
        this.query = {};
        this.state = {
            editType: null,
            submiting: false
        };
    }

    /* ---------------------------- 系统函数 ------------------------------*/

    componentDidMount() {
        const { Groups, Algorithms } = this.props;

        if (!Groups)
            this.props.dispatch(GameAction.queryGameGroup());

        if (!Algorithms)
            this.props.dispatch(GameAction.queryGameAlgorithm());

        this.queryData();
    }

    /* ---------------------------- 自定义函数 ------------------------------*/

    queryData() {
        this.props.dispatch(GameAction.queryGameType(this.pageIndex, this.query)).then(act => {
            if (act.error)
                Message.error(act.error);
        });
    }

    updateType(type) {

        let groups = this.props.Groups;
        if (!groups) {
            this.props.dispatch(GameAction.queryGameGroup()).then(it => {
            });
            return;
        }
        this.setState({ editType: type });
    }

    /* ---------------------------- Events ------------------------------*/

    onSearch(query) {
        this.pageIndex = 1;
        this.query = query;
        this.queryData();
    }

    handleSubmit(values) {
        if (!this.state.editType) return;
        this.setState({ submiting: true });

        if (isEmptyObject(this.state.editType)) {
            // add
            this.props.dispatch(GameAction.addGameType(values))
                .then(act => {
                    this.setState({ submiting: false });
                    if (act.error || act.response.code != 0) {
                        Message.error(act.error || act.response.message);
                    } else {
                        Message.success("添加成功");
                        this.setState({ editType: null });
                        this.queryData();
                    }
                });
        } else {
            // update
            this.props.dispatch(GameAction.updateGameType(this.state.editType.Id, values))
                .then(act => {
                    this.setState({ submiting: false });
                    if (act.error || act.response.code != 0) {
                        Message.error(act.error || act.response.message);
                    } else {
                        Message.success("修改成功");
                        this.setState({ editType: null });
                        this.queryData();
                    }
                });
        }
    }

    /* ---------------------------- Renders ------------------------------*/

    tableColumns() {
        const self = this;
        const columns = [{ title: '#', render(text, record, index) { return index + 1; } },
        { title: '彩种', dataIndex: 'GroupName' },
        { title: 'Id', dataIndex: 'Id' },
        { title: '名称', dataIndex: 'Name' },
        { title: '赔率', dataIndex: 'Scale' },
        { title: '基础赔率', dataIndex: 'ScaleBase' },
        { title: '号码数', dataIndex: "BetNumCount" },
        { title: '选码器', dataIndex: "Style", render(style) { return style == 0 ? "直输" : "选码" } },
        { title: '注数计算器', dataIndex: "BetCountCalurator" },
        { title: "状态", dataIndex: "Status", render: RenderHelper.render_status(CommonStyleStatus) },
        { title: '时间', dataIndex: 'CreateTime' },
        {
            title: "操作", width: 80, fixed: 'right', render(type) {
                return <Button type="primary" onClick={() => self.updateType(type)}>编辑</Button>
            }
        }];

        return columns;
    }

    render() {
        let gametype = this.props.GameType || {};
        let columns = this.tableColumns();

        const self = this;
        const pagination = {
            total: gametype.count || 0,
            onChange(current) {
                self.pageIndex = current;
                self.queryData();
            },
        };
        return (
            <div>
                {this.renderSearch()}
                <div className="search-result-list">
                    <Table columns={columns} dataSource={gametype.data || []} loading={this.props.isFetching} pagination={pagination} scroll={{ x: 1300 }} rowKey="Id" />
                </div>
                {this.renderEditModal()}
            </div>
        );
    }

    renderSearch() {
        let groups = this.props.Groups || [];

        return (
            <SearchForm showAddBtn={true} onSearch={this.onSearch.bind(this)} onAdd={() => this.setState({ editType: {} })}>
                <name label="名称">
                    <Input placeholder="名称" size="default" name="name" />
                </name>
                <group_id label="彩种" options={{ initialValue: "" }}>
                    <Select>
                        <Option value="">全部</Option>
                        {groups.map(group => {
                            return <Option value={group.Id.toString()} key={group.Id}>{group.Name}</Option>
                        })}
                    </Select>
                </group_id>
                <status label="状态" options={{ initialValue: "" }}>
                    <Select>
                        <Option value="">全部</Option>
                        {Object.keys(CommonStatus).map(it => {
                            return <Option value={it.toString()} key={it}>{CommonStatus[it]}</Option>
                        })}
                    </Select>
                </status>
            </SearchForm>
        );
    }

    renderEditModal() {

        const { editType } = this.state;
        if (!editType) return;

        const { Algorithms } = this.props;
        const isNull = isEmptyObject(editType);
        const title = isNull ? "添加游戏玩法" : "编辑" + editType.Name;


        return (
            <EditorModal title={title} visible={true}
                submiting={this.state.submiting}
                onSubmit={this.handleSubmit.bind(this)}
                onClose={it => this.setState({ editType: null })}>
                {isNull && <id label="玩法Id" options={{
                    rules: [{
                        required: true,
                        message: '请输入玩法Id'
                    }]
                }}>
                    <Input autoComplete="off" />
                </id>
                }
                <group_id label="彩种" options={{
                    initialValue: !isNull && editType.GroupId.toString(),
                    rules: [{
                        required: true,
                        message: '请选择彩种'
                    }]
                }}>
                    <Select>
                        {this.props.Groups.map(it => {
                            return <Option key={it.Name} value={it.Id.toString()}>{it.Name}</Option>;
                        })}
                    </Select>
                </group_id>
                <name label="名称" options={{
                    initialValue: editType.Name || "",
                    rules: [{
                        required: true,
                        message: '请输入玩法名称'
                    }]
                }}>
                    <Input type="name" autoComplete="off" />
                </name>
                <algorithm_id label="开奖算法" options={{
                    initialValue: !isNull && editType.AlgorithmId.toString(),
                    rules: [
                        { required: true, message: '请选择开奖算法' }
                    ]
                }}>
                    <Select>
                        {Algorithms.map(it => {
                            return <Option key={it.Id} value={it.Id.toString()}>{it.Description}[{it.Name}]</Option>;
                        })}
                    </Select>
                </algorithm_id>
                <scale label="赔率" options={{
                    initialValue: isNull ? "" : editType.Scale.toString(),
                    rules: [
                        { required: true, message: '请输入玩法赔率' }
                    ]
                }}>
                    <Input autoComplete="off" />
                </scale>
                <scale_base label="基础赔率" options={{
                    initialValue: isNull ? "" : editType.ScaleBase.toString(),
                    rules: [
                        { required: true, message: '请输入玩法基础赔率' }
                    ]
                }}>
                    <Input autoComplete="off" />
                </scale_base>
                <bet_num_count label="投注号码数量" options={{
                    initialValue: isNull ? "" : editType.BetNumCount.toString(),
                    rules: [
                        { required: true, message: '请输入玩法投注号码数' }
                    ]
                }}>
                    <Input autoComplete="off" />
                </bet_num_count>
                <digit_pos_count label="数位栏数量" options={{
                    initialValue: isNull ? "" : editType.DigitPosCount.toString(),
                    rules: [
                        { required: true, message: '请输入数位栏数量' }
                    ]
                }}>
                    <Input autoComplete="off" />
                </digit_pos_count>
                <can_num_duplicate label="允许号码重复" options={{
                    valuePropName: 'checked',
                    initialValue: !isNull && editType.CanNumDuplicate,
                    rules: [
                        { required: true }
                    ]
                }}>
                    <Checkbox>允许</Checkbox>
                </can_num_duplicate>
                <unit_pos_count label="任选号码数" options={{
                    initialValue: isNull ? "" : editType.UnitPosCount.toString(),
                    rules: [
                        { required: true, message: '请输入任选号码数' }
                    ]
                }}>
                    <Input autoComplete="off" />
                </unit_pos_count>
                <style label="选码器类型" options={{
                    initialValue: !isNull && editType.Style.toString(),
                    rules: [
                        { required: true, message: '请选择选码器类型' }
                    ]
                }}>
                    <RadioGroup>
                        <RadioButton value="0">号码直输</RadioButton>
                        <RadioButton value="1">号码选码</RadioButton>
                    </RadioGroup>
                </style>
                <calurator label="注数计算器" options={{
                    initialValue: !isNull && editType.BetCountCalurator.toString(),
                    rules: [
                        { required: true, message: '请选择注数计算器' }
                    ]
                }}>
                    <Select>
                        <OptGroup label="直选">
                            <Option value="StraightPickTreeStyleBetCount">直选复式</Option>
                            <Option value="StraightPickLinearStyleBetCount">直选单式</Option>
                            <Option value="PositionBetCount">定位胆/不定位胆</Option>
                        </OptGroup>
                        <OptGroup label="组选">
                            <Option value="UniquePermutationBetCount">无重排列</Option>
                            <Option value="N6BoxPickTreeStyleBetCount">组六</Option>
                            <Option value="N3BoxPickTreeStyleBetCount">组三</Option>
                            <Option value="N2BoxPickTreeStyleBetCount">组二</Option>
                            <Option value="N3MixBoxPickTreeStyleBetCount">组三混合</Option>
                            <Option value="N2MixBoxPickTreeStyleBetCount">组二混合</Option>
                        </OptGroup>
                        <OptGroup label="重号组选">
                            <Option value="BasicBoxPickTreeStyleBetCount">组选120/24</Option>
                            <Option value="Duplicate1Odd3BetCount">1二重号3单号(五星组选60)</Option>
                            <Option value="Duplicate2Odd1BetCount">2二重号1单号(五星组选30)</Option>
                            <Option value="Duplicate2Odd3BetCount">1三重号3单号(五星组选20)</Option>
                            <Option value="Duplicate1Odd1BetCount">1三重号1二重号(五星组选10) 1四重号1单号(五星组选5) 1三重号1单号(四星组选4)</Option>
                            <Option value="Duplicate1Odd2BetCount">1二重号2单号(四星组选12)</Option>
                            <Option value="Duplicate2BetCount">2二重号(四星组选6)</Option>
                        </OptGroup>
                        <OptGroup label="总和">
                            <Option value="SUMBetCount">大小单双/前大后大</Option>
                        </OptGroup>
                    </Select>
                </calurator>
                {!isNull &&
                    <status label="状态" options={{
                        initialValue: (editType.Status || 0).toString(),
                        rules: [{
                            required: true,
                            message: '请选择玩法状态'
                        }]
                    }}>
                        <Select>
                            {Object.keys(CommonStatus).map(it => {
                                return <Option key={it} value={it}>{CommonStatus[it]}</Option>;
                            })}
                        </Select>
                    </status>
                }
            </EditorModal>
        );
    }
}

export default connect(state => ({
    isFetching: state.game.isFetching,
    Groups: state.game.Groups,
    GameType: state.game.GameType,
    Algorithms: state.game.Algorithm,
}))(GameType);
