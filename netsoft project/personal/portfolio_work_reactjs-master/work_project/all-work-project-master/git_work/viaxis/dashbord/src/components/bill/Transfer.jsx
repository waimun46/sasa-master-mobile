import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { message, Row, Col, Table, Tag, Button, Modal, Form, Input, InputNumber, Select, DatePicker } from 'antd';

import BasicComponent from "../common/BasicComponent";
import SearchForm from "../common/SearchForm";

import * as BillAction from "../../actions/bill";
import * as RenderHelper from "../../utils/render";
import * as Enums from "../../app_enums";
import * as Utils from "../../utils/common";

class Transfer extends BasicComponent {

    constructor(props) {
        super(props);
        this.state = { transfering: false };
    }

    /* --------------------- Events ------------------------ */

    handleSubmit(e) {
        const { dispatch } = this.props;
        e.preventDefault();
        this.props.form.validateFields((errors, values) => {
            if (errors) return;
            this.setState({ transfering: true });

            dispatch(BillAction.transfer(values.fromEmail, values.toEmail, values.amount, values.remark)).then(act => {
                this.setState({ transfering: false });
                if (act.error) {
                    message.error(act.error);
                } else {
                    message.success("转账成功");
                    this.props.form.resetFields();
                }
            });
        });
    }

    handleReset(e) {
        e.preventDefault();
        this.props.form.resetFields();
    }

    /* ------------------- Renders --------------------- */

    render() {
        const { getFieldDecorator, isFieldValidating, getFieldError } = this.props.form;
        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 8 },
        };
        return (
            <Form horizontal>
                <Form.Item {...formItemLayout} label="转出用户邮箱" hasFeedback>
                    {getFieldDecorator('fromEmail', {
                        rules: [
                            { required: true, message: '请输入转出的用户邮箱' },
                            { type: "email", message: "请输入正确的邮箱" }
                        ],
                    })(
                        <Input placeholder="转出的用户邮箱" />
                    )}
                </Form.Item>
                <Form.Item {...formItemLayout} label="转入用户邮箱" hasFeedback>
                    {getFieldDecorator('toEmail', {
                        rules: [
                            { required: true, message: '请输入转入的用户邮箱' },
                            { type: "email", message: "请输入正确的邮箱" }
                        ],
                    })(
                        <Input placeholder="转入的用户邮箱" />
                    )}
                </Form.Item>
                <Form.Item {...formItemLayout} label="金额" hasFeedback>
                    {getFieldDecorator('amount', {
                        rules: [
                            { required: true, whitespace: false, message: '请输入转账金额' },
                            { type: "string", pattern: /^\d+$/, message: "金额必须是数字" }
                        ],
                    })(
                        <Input autoComplete="off" />
                    )}
                </Form.Item>
                <Form.Item {...formItemLayout} label="备注">
                    {getFieldDecorator('remark', {
                        rules: [
                            { required: true, message: '备注必填' },
                        ],
                    })(
                        <Input type="textarea" placeholder="转账的原因，例如返利、奖励等。" />
                    )}
                </Form.Item>
                <Form.Item wrapperCol={{ span: 12, offset: 7 }}>
                    <Button type="primary" loading={this.state.transfering} onClick={this.handleSubmit.bind(this)}>提交</Button>
                    &nbsp;&nbsp;&nbsp;
                      <Button type="ghost" onClick={this.handleReset.bind(this)}>重置</Button>
                </Form.Item>
            </Form>
        );
    }
}

export default connect(state => ({
    bill: state.bill,
}))(Form.create()(Transfer));

class TransferList extends BasicComponent {
    constructor(props) {
        super(props);
        this.query = {};
        this.pageIndex = 1;
    }

    /* ---------------------------- Sys ------------------------------*/

    componentWillMount() {
        this.queryData();
    }

    /* ---------------------------- Function ------------------------------*/

    queryData() {
        this.props.dispatch(BillAction.queryTransfer(this.pageIndex, this.query)).then(act => {
            if (act.error)
                message.error(act.error);
        });
    }

    /* ------------------------ Events ------------------------ */

    onSearchClick(query) {
        this.query = query;
        this.pageIndex = 1;
        this.queryData();
    }

    onResetClick() {
        this.query = {};
    }

    /* ------------------------ Renders ------------------------ */

    render() {
        const { isFetching } = this.props;

        const transfer = this.props.transfer || {};
        const data = transfer.data || [];

        const self = this;
        const pagination = {
            current: transfer.pageIndex || 1,
            total: transfer.count || 0,
            onChange(current) {
                self.pageIndex = current;
                self.queryData();
            },
        };
        return (
            <div>
                {this.renderSearch()}
                <div className="search-result-list">
                    <Table bordered columns={this.columns} scroll={{ x: 1200 }} dataSource={data} loading={isFetching} pagination={pagination} rowKey="Id" />
                </div>
            </div>
        );
    }

    renderSearch() {
        const formItemLayout = {
            labelCol: { span: 8 },
            wrapperCol: { span: 16 }
        };

        return (
            <SearchForm onSearch={this.onSearchClick.bind(this)} onReset={this.onResetClick.bind(this)}>
                <email label="邮箱" {...formItemLayout}>
                    <Input />
                </email>
                <amount label="金额" {...formItemLayout}>
                    <Input />
                </amount>
                <remark label="备注" {...formItemLayout}>
                    <Input />
                </remark>
                <create_time label="时间" {...formItemLayout} format={(times) => Utils.dateRangeFormat(times)}>
                    <DatePicker.RangePicker />
                </create_time>
            </SearchForm>
        );
    }

    get columns() {

        const columns = [{ title: '#', render(text, record, index) { return index + 1; } },
        {
            title: "转出用户", children: [
                { title: '邮箱', dataIndex: 'FromEmail', key: 'FromEmail' },
                { title: "余额", dataIndex: "FromUserBalance", className: 'column-money', render: RenderHelper.money_format, key: 'FromUserBalance' },
            ]
        },
        { title: "金额", dataIndex: "Amount", className: "column-money", render: RenderHelper.money_color },
        {
            title: "转入用户", children: [
                { title: '邮箱', dataIndex: 'ToEmail', key: 'ToEmail' },
                { title: "余额", dataIndex: "ToUserBalance", className: 'column-money', render: RenderHelper.money_format, key: 'ToUserBalance' },
            ]
        },
        { title: "备注", dataIndex: "Remark" },
        { title: '时间', dataIndex: 'CreateTime', render: RenderHelper.render_time }];

        return columns;
    }

}

const transferList = connect(state => ({
    isFetching: state.bill.isFetching,
    transfer: state.bill.Transfer,
}))(TransferList);

export { transferList as TransferList };
