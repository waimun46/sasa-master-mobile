/**
 * 表单验证工具类。
 * 提供常用的验证方法。
 */

import configureStore from '../configure_store'
import * as Utils from "./common";
import * as UserAction from '../actions/user';

const store = configureStore();

/**
 * 检查两个元素值是否相等。
 * Example : 
 * {
 *     validator: checkEqual, message: "两次输入密码不一致。",
 *     getFieldValue: store.form.getFieldValue,
 *     equalEleName: "password"
 * }
 * @param  {[type]}   rule     [description]
 * @param  {[type]}   value    [description]
 * @param  {Function} callback [description]
 * @return {[type]}            [description]
 */
export function checkEqual(rule, value, callback) {
    if (value && value !== rule.getFieldValue(rule.equalEleName)) {
        callback(rule.message);
    } else {
        callback();
    }
}

/**
 * 检查中文字符长度
 * @param  {Number} len 具体转换长度详见 utils/common.js sizeof ，默认32对应10个中文字符
 * @return {[type]}     [description]
 */
export const checkChineseLength = (len = 32) => {
    return (rule, value, callback) => {
        if (value && Utils.sizeof(value) > len)
            callback(rule.message);
        else
            callback();
    };
}

/**
 * 手机号码
 * @param  {[type]}   rule     [description]
 * @param  {[type]}   value    [description]
 * @param  {Function} callback [description]
 * @return {[type]}            [description]
 */
export function mobile(rule, value, callback) {
    if (value && !/^1\d{10}$/.test(value))
        callback(rule.message);
    else
        callback();
}


/**
 * 检查QQ号码
 * @param  {[type]}   rule     [description]
 * @param  {[type]}   value    [description]
 * @param  {Function} callback [description]
 * @return {[type]}            [description]
 */
export function checkQQ(rule, value, callback) {
    if (value && !/\d{5,16}$/.test(value))
        callback(rule.message);
    else
        callback();
}

/**
 * 检查银行卡卡号格式.
 * Example:
 * {
 *     validator: Validators.checkBankCard,
 *     message: "无效的银行卡号"
 * }
 * @param  {[type]}   rule     [description]
 * @param  {[type]}   value    [description]
 * @param  {Function} callback [description]
 * @return {[type]}            [description]
 */
export function checkBankCard(rule, value, callback) {
    if (value && !Utils.isBankCard(value)) {
        callback(rule.message);
    } else {
        callback();
    }
}

/**
 * 检查邮箱是否已被使用
 * @param  {[type]}   rule     [description]
 * @param  {[type]}   value    [description]
 * @param  {Function} callback [description]
 * @return {[type]}            [description]
 */
export function checkEmailExisted(rule, value, callback) {
    if(!value){
        callback();
    }else{
        value && store.dispatch(UserAction.checkUsername(value)).then(act => {
            const existed = act.response;
            if (existed) {
                callback(rule.message || "该邮箱已被注册!");
            }
            callback();
        });
    }
}

/**
 * @param  {[type]}   rule     [description]
 * @param  {[type]}   value    [description]
 * @param  {Function} callback [description]
 * @return {[type]}            [description]
 */
export function checkPrecision(rule, value, callback) {
    if(value && !/^\d+(.\d{1,2})?$/.test(value)){
        callback(rule.message);
    }else{
        callback();
    }
}