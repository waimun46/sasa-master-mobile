import React, { Component } from 'react';
import { connect } from 'react-redux';
import { message, Row, Col, Table, Tag, Button, Modal, Form, Input, Select,DatePicker, Icon } from 'antd';
import { updateUserPassword } from '../../actions/user';

const createForm = Form.create;
const FormItem = Form.Item;
const Option = Select.Option;
const RangePicker = DatePicker.RangePicker;
const Message = message;

class UpdateUserPassword extends Component {
    constructor(props){
        super(props);
        this.state = {
            id: "",
            popup: false,
            loading: false,
        }
    }

    show(){
        const form = this.props.form;
        form.resetFields();
        var state = {
            popup: true,
            loading: false
        };
        this.setState(state);
    }

    hide(){
        this.setState({popup:false});
    }

    checkPassword(rule, value, callback){
        const form = this.props.form;
        if(value && form.getFieldValue('confirm_password')){
            form.validateFields(['confirm_password'], { force: true });
        }
        callback();
    }

    checkConfirm(rule, value, callback) {
        const form = this.props.form;
         if (value && value === form.getFieldValue('new_password'))
            callback();
        else
            callback("与新密码不一致");
    }

    handleSubmit(e){
        const { form, dispatch } = this.props;
        e.preventDefault();

        var old_password = form.getFieldValue('old_password');
        var new_password = form.getFieldValue('new_password');
        form.validateFields((errors, values)=>{
             if(errors) return;
             this.setState({loading:true});

             dispatch(updateUserPassword(old_password, new_password)).then(act=>{
                this.setState({loading:false});
                if(act.error)
                    Message.error(act.error);
                else{
                    Message.success("已成功修改登录密码");
                    this.hide();
                }
             });
         });
    }

    render(){
        const formItemLayout = {
            labelCol: { span: 7 },
            wrapperCol: { span: 12 }
        };
        const { getFieldDecorator } = this.props.form;

        return (
            <a onClick={this.show.bind(this)}>
                <Icon type="edit" /><Modal title="修改登录密码" visible={this.state.popup} onCancel={this.hide.bind(this)} footer={[
                    <Button key="submit" type="primary" size="large" loading={this.state.loading} onClick={this.handleSubmit.bind(this)}>提交</Button>,
                    <Button key="back" type="ghost" size="large" onClick={this.hide.bind(this)}>取消</Button>
                ]}>
                    <Form horizontal>
                        <FormItem {...formItemLayout} label="当前密码">
                            { getFieldDecorator('old_password', {
                                rules: [{ required: true, whitespace: false, message: "请输入当前密码"}]
                            })(
                                <Input type="password" placeholder="当前密码(必填)" />
                            )}
                        </FormItem>
                        <FormItem {...formItemLayout} label="新密码">
                            { getFieldDecorator('new_password', {
                                rules: [
                                    { required: true, whitespace: false, message: "请输入新密码"},
                                    { type: "string", pattern: /\S{6,}/, message: "密码长度至少6位"},
                                    { validator: this.checkPassword.bind(this) }
                                ]
                            })(
                                <Input type="password" placeholder="新密码(必填，长度至少6位)" />
                            )}
                        </FormItem>

                        <FormItem {...formItemLayout} label="确认密码">
                            { getFieldDecorator('confirm_password', {
                                rules: [
                                    { required: true, whitespace: false, message: "请确认密码"},
                                    { validator: this.checkConfirm.bind(this) }
                                ]
                            })(
                                <Input type="password" placeholder="确认密码，需与新密码一致" />
                            )}
                        </FormItem>
                    </Form>
                </Modal>
            </a>
        );
    }
}

module.exports = connect(state=>({ user: state.user }))(createForm()(UpdateUserPassword));