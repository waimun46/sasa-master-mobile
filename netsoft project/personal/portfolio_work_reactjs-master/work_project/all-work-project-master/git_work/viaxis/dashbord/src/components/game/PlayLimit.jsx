import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { message, Row, Col, Table, Tag, Button, Modal, Form, Input, Select, DatePicker } from 'antd';
import { addPlayLimit, updatePlayLimit, queryPlayLimit, queryGameData, queryGameGroup } from '../../actions/game';
import { render_index, render_time, render_status } from "../../utils/render";
import { PlayLimitType, CommonStatus, GameGroup, GameType } from "../../app_enums";

const createForm = Form.create;
const FormItem = Form.Item;
const Option = Select.Option;
const RangePicker = DatePicker.RangePicker;
const Message = message;

class SearchFormComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            game_group: "",
            game_type: "",
            game_type_list: [],
            email: "",
            type: "",
            status: ""
        };
    }

    componentDidMount() {
    }

    handleChange(e) {
        var state = {};
        state[e.target.name] = e.target.value;
        this.setState(state);
    }

    handleGameGroupChange(game_group) {
        this.updateGameTypeList(game_group)
    }

    handleGameTypeChange(game_type) {
        this.setState({ game_type: game_type });
    }

    handleTypeChange(type) {
        this.setState({ type: type });
    }

    handleStatusChange(val) {
        this.setState({ status: val });
    }

    handleClick() {
        this.props.onSearchClick(this.state);
    }

    updateGameTypeList(game_group) {
        var game_type_list = this.props.getFilteredGameTypes(game_group);
        this.setState({ game_group: game_group, game_type_list: game_type_list, game_type: "" });
    }

    handleReset() {
        var state = {
            email: "",
            type: "",
            status: ""
        };

        this.updateGameTypeList("");
        this.setState(state);
    }

    render() {
        const { gameGroups } = this.props;
        return (
            <Form horizontal className="ant-advanced-search-form">
                <Row gutter={16}>
                    <Col sm={6}>
                        <FormItem label="彩种" labelCol={{ span: 10 }} wrapperCol={{ span: 14 }}>
                            <Select optionFilterProp="children" value={this.state.game_group} notFoundContent="找不到内容" onChange={this.handleGameGroupChange.bind(this)}>
                                <Option value="">[不限彩种]</Option>
                                {Object.keys(this.props.gameGroups).map(it => {
                                    return <Option value={it.toString()} key={it}>{this.props.gameGroups[it]}</Option>
                                })}
                            </Select>
                        </FormItem>
                        <FormItem label="类型" labelCol={{ span: 10 }} wrapperCol={{ span: 14 }}>
                            <Select optionFilterProp="children" value={this.state.type} notFoundContent="找不到内容" onChange={this.handleTypeChange.bind(this)}>
                                <Option value="">[不限类型]</Option>
                                {Object.keys(PlayLimitType).map(it => {
                                    return <Option value={it.toString()} key={it}>{PlayLimitType[it]}</Option>
                                })}
                            </Select>
                        </FormItem>
                    </Col>
                    <Col sm={6}>
                        <FormItem label="玩法" labelCol={{ span: 10 }} wrapperCol={{ span: 14 }}>
                            <Select optionFilterProp="children" value={this.state.game_type} notFoundContent="找不到内容" onChange={this.handleGameTypeChange.bind(this)}>
                                <Option value="">[不限玩法]</Option>
                                {Object.keys(this.state.game_type_list).map(it => {
                                    var game_type = this.state.game_type_list[it]
                                    return <Option value={game_type.Id.toString()} key={game_type.Id}>{game_type.Name}</Option>
                                })}
                            </Select>
                        </FormItem>
                        <FormItem label="状态" labelCol={{ span: 10 }} wrapperCol={{ span: 14 }}>
                            <Select optionFilterProp="children" value={this.state.status} notFoundContent="找不到内容" onChange={this.handleStatusChange.bind(this)}>
                                <Option value="">全部</Option>
                                {Object.keys(CommonStatus).map(it => {
                                    return <Option value={it.toString()} key={it}>{CommonStatus[it]}</Option>
                                })}
                            </Select>
                        </FormItem>
                    </Col>
                    <Col sm={6}>
                        <FormItem label="用户" labelCol={{ span: 10 }} wrapperCol={{ span: 14 }}>
                            <Input placeholder="Email" size="default" name="email" value={this.state.email} onChange={this.handleChange.bind(this)} />
                        </FormItem>
                    </Col>
                </Row>
                <Row>
                    <Col span={12} offset={12} style={{ textAlign: 'right' }}>
                        <Button type="primary" icon="search" onClick={this.handleClick.bind(this)}>搜索</Button>
                        <Button type="ghost" icon="reload" onClick={this.handleReset.bind(this)}>清空</Button>
                    </Col>
                </Row>
            </Form>
        );
    }
}

class PlayLimit extends Component {

    constructor(props) {
        super(props);
        this.query = {};
        this.pageIndex = 1;
        this.state = {
            loading: false,
            popup: false,
            popupTitle: "",
            filtered_gametypes: [],
            edit: false,
            current_playlimit: {}
        };
    }

    componentDidMount() {
        this.queryData();
    }

    getFilteredGameTypes(groupId) {
        var gametypes = [];
        Object.keys(GameType).map(it => {
            var gt = GameType[it];
            if (groupId == 0 || gt.GroupId == groupId)
                gametypes.push(gt);
        });

        return gametypes;
    }

    queryData() {
        this.props.dispatch(queryPlayLimit(this.pageIndex, this.query)).then(act => {
            if (act.error)
                Message.error(act.error);
        });
    }

    addData() {
        this.props.form.resetFields();
        var filtered_gametypes = this.getFilteredGameTypes(0);
        this.setState({
            popup: true,
            popupTitle: "添加限额",
            current_playlimit: {},
            filtered_gametypes: filtered_gametypes,
            edit: false
        });
        this.props.form.setFieldsValue({ 'game_type': '0' });
    }

    updateData(playlimit) {
        this.props.form.resetFields();
        var gametype = playlimit.GameType || 0;
        var filtered_gametypes = this.getFilteredGameTypes(playlimit.GameGroup || 0);
        this.setState({
            popup: true,
            popupTitle: "编辑限额",
            current_playlimit: playlimit,
            filtered_gametypes: filtered_gametypes,
            edit: true
        });
        if (gametype)
            this.props.form.setFieldsValue({ 'game_type': GameType[gametype].Name });
    }

    handleSubmit(e) {
        const { dispatch } = this.props;
        e.preventDefault();

        this.props.form.validateFields((errors, values) => {
            if (errors) return;
            if (this.state.edit) {
                let playlimit = this.state.current_playlimit;
                if (playlimit == null || JSON.stringify(playlimit) == '{}') return;

                this.setState({ isLoading: true });
                dispatch(updatePlayLimit(playlimit.Id, values.game_group, values.game_type, values.email || '', values.type, values.amount, values.status)).then(act => {
                    this.setState({ loading: false });
                    if (act.error) {
                        Message.error(act.error);
                    } else {
                        Message.success("修改成功");
                        this.props.form.resetFields();
                        this.setState({ popup: false });
                        this.queryData();
                    }
                });
            } else {
                this.setState({ isLoading: true });
                dispatch(addPlayLimit(values.game_group, values.game_type, values.email || '', values.type, values.amount, values.status)).then(act => {
                    this.setState({ isLoading: false });
                    if (act.error) {
                        Message.error(act.error);
                    } else {
                        Message.success("添加成功");
                        this.props.form.resetFields();
                        this.setState({ popup: false });
                        this.queryData();
                    }
                });
            }
        });
    }

    changeGroup(groupId) {
        var filtered_gametypes = this.getFilteredGameTypes(groupId);
        this.setState({
            filtered_gametypes: filtered_gametypes,
        });
        this.props.form.setFieldsValue({ 'game_type': '0' });
    }

    changeGameType(gameType) {
        return;
        this.setState({
            gametype: gameType
        });
    }

    hideModal() {
        this.setState({ popup: false });
    }

    tableColumns() {
        const self = this;
        const columns = [
            { title: '#', render(text, record, index) { return index + 1; } },
            {
                title: "彩种",
                dataIndex: "GameGroup",
                render(game_group) {
                    return GameGroup[game_group] || "[不限彩种]";
                }
            },
            {
                title: "玩法",
                dataIndex: "GameType",
                render(game_type) {
                    return (GameType[game_type] && GameType[game_type].Name) || "[不限玩法]";
                }
            },
            {
                title: "用户",
                dataIndex: "Email",
                render(email) {
                    return email || "[所有用户]";
                }
            },
            {
                title: "类型",
                dataIndex: "Type",
                render(type) {
                    return PlayLimitType[type] || "[未知类型]";
                }
            },
            { title: "金额", dataIndex: "Amount" },
            {
                title: "状态",
                dataIndex: "Status",
                render(status) {
                    const colors = {
                        0: "blue",
                        1: "red"
                    }
                    return render_status(colors[status], CommonStatus[status]);
                }
            },
            {
                title: "操作", render(playlimit) {
                    return <Button type="primary" onClick={() => self.updateData(playlimit)}>编辑</Button>
                }
            }
        ];

        return columns;
    }

    onSearchClick(query) {
        this.query = query;
        this.queryData();
    }

    render() {
        const { data } = this.props;
        let playlimit = data.playlimit || [];
        let gameTypes = data.gameTypes || [];

        let columns = this.tableColumns();
        const self = this;
        const pagination = {
            total: playlimit.count || 0,
            onChange(current) {
                self.pageIndex = current;
                self.queryData();
            },
        };
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: { span: 7 },
            wrapperCol: { span: 12 },
        };
        return (
            <div>
                <SearchForm gameGroups={GameGroup} getFilteredGameTypes={this.getFilteredGameTypes.bind(this)} onSearchClick={this.onSearchClick.bind(this)} /><br />
                <div style={{ marginBottom: 16 }}>
                    <Button type="primary" onClick={this.addData.bind(this)}>添加限额</Button>
                </div><br />
                <Table columns={columns} dataSource={playlimit.data || []} loading={data.isFetching} pagination={pagination} />
                <Modal title={this.state.popupTitle} visible={this.state.popup} onCancel={this.hideModal.bind(this)}
                    footer={[
                        <Button key="back" type="ghost" size="large" onClick={this.hideModal.bind(this)}>取消</Button>,
                        <Button key="submit" type="primary" size="large" loading={this.state.loading} onClick={this.handleSubmit.bind(this)}>提交</Button>
                    ]}>
                    <Form horizontal>
                        <FormItem {...formItemLayout} label="遊戏彩种">
                            {getFieldDecorator('game_group', {
                                initialValue: (this.state.current_playlimit.GameGroup || 0).toString()
                            })(
                                <Select onChange={this.changeGroup.bind(this)}>
                                    <Option key='0' value='0'>[不限彩种]</Option>
                                    {Object.keys(GameGroup).map(it => {
                                        return <Option key={it} value={it}>{GameGroup[it]}</Option>;
                                    })}
                                </Select>
                            )}
                        </FormItem>
                        <FormItem {...formItemLayout} label="遊戏玩法">
                            {getFieldDecorator('game_type', {
                                initialValue: (this.state.current_playlimit.GameType || 0).toString()
                            })(
                                this.render_gametypes()
                            )}
                        </FormItem>
                        <FormItem {...formItemLayout} label="用户">
                            {getFieldDecorator('email', {
                                initialValue: (this.state.current_playlimit.Email || "")
                            })(
                                <Input autoComplete="off" />
                            )}
                        </FormItem>
                        <FormItem {...formItemLayout} label="类型">
                            {getFieldDecorator('type', {
                                initialValue: (this.state.current_playlimit.Type || 0).toString(),

                            })(
                                <Select>
                                    {Object.keys(PlayLimitType).map(it => {
                                        return <Option key={it} value={it}>{PlayLimitType[it]}</Option>;
                                    })}
                                </Select>
                            )}
                        </FormItem>
                        <FormItem {...formItemLayout} label="金额">
                            {getFieldDecorator('amount', {
                                initialValue: (this.state.current_playlimit.Amount || "").toString(),
                                rules: [
                                    { required: true, whitespace: false, message: '请输入金额' },
                                    { type: "string", pattern: /^\d+$/, message: "金额必须是数字" }
                                ]
                            })(
                                <Input autoComplete="off" />
                            )}
                        </FormItem>
                        <FormItem {...formItemLayout} label="状态">
                            {getFieldDecorator('status', {
                                initialValue: (this.state.current_playlimit.Status || 0).toString()
                            })(
                                <Select>
                                    {Object.keys(CommonStatus).map(it => {
                                        return <Option key={it} value={it}>{CommonStatus[it]}</Option>;
                                    })}
                                </Select>
                            )}
                        </FormItem>
                    </Form>
                </Modal>
            </div>
        );
    }

    render_gametypes() {
        var gametypes = this.state.filtered_gametypes || [];
        var items = [];
        items.push(<Option key='0' value='0' selected>[不限玩法]</Option>);
        gametypes.map(gt => {
            items.push(<Option key={gt.Id.toString()} value={gt.Id.toString()}>{gt.Name}</Option>);
        });
        return (<Select>{items}</Select>);
    }
}

const SearchForm = createForm()(SearchFormComponent);
export default connect(state => ({
    data: state.game,
}))(Form.create()(PlayLimit));
