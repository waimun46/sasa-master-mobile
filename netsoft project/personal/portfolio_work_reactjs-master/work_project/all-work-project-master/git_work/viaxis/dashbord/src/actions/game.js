import $ from "jquery";
import * as app_const from "../app_const";
import { GameType } from "../app_enums";
import * as Utils from "../utils/common";

/* ----------------------- Game Group ---------------------------- */
export function queryGameGroup(querys = {}) {
	var queryStr = JSON.stringify(querys);
	return {
		name: app_const.QUERY_GAMEGROUP,
		callAPI: () => Utils.POST(`/api/game/group`, queryStr)()
	}
}

/* ----------------------- Game Type ---------------------------- */

export function queryGameType(pageIndex = 1, querys = {}) {
	var queryParams = Object.assign({}, querys, {
		pageIndex
	});
	var queryStr = JSON.stringify(queryParams);
	return {
		name: app_const.QUERY_GAMETYPE,
		callAPI: () => Utils.POST(`/api/game/type`, queryStr)()
	}
}

export function addGameType(payload) {
	return {
		name: app_const.ADD_GAMETYPE,
		callAPI: () => Utils.POST(`/api/game/type/add`, JSON.stringify(payload))()
	};
}

export function updateGameType(type_id, payload) {
	return {
		name: app_const.UPDATE_GAMETYPE,
		callAPI: () => Utils.POST(`/api/game/type/${type_id}/update`, JSON.stringify(payload))()
	};
}

export function queryGameTypeWithoutInfo(groupId) {
	return {
		name: app_const.QUERY_GAMETYPE_WITHOUTINFO,
		callAPI: () => Utils.POST(`/api/game/type/withoutinfo?group_id=${groupId}`)()
	}
}

/* ----------------------- Game Type Info ---------------------------- */
export function queryGameTypeInfo(pageIndex = 1, querys = {}) {
	var queryParams = Object.assign({}, querys, {
		pageIndex
	});
	var queryStr = JSON.stringify(queryParams);
	return {
		name: app_const.QUERY_GAMETYPEINFO,
		callAPI: () => Utils.POST(`/api/game/typeinfo`, queryStr)()
	}
}

export function addGameTypeInfo(game_type_id, name, category, sort, kind, explain, example, weight) {
	var payload = { game_type_id, name, category, sort, kind, explain, example, weight };
	return {
		name: app_const.ADD_GAMETYPEINFO,
		callAPI: () => Utils.POST(`/api/game/typeinfo/add`, JSON.stringify(payload))()
	};
}

export function updateGameTypeInfo(game_type_id, name, category, sort, kind, explain, example, weight, status) {
	var payload = { name, category, sort, kind, explain, example, weight, status };
	return {
		name: app_const.UPDATE_GAMETYPEINFO,
		callAPI: () => Utils.POST(`/api/game/typeinfo/${game_type_id}/update`, JSON.stringify(payload))()
	};
}

/* ----------------------- Game Type Category ---------------------------- */
export function queryGameTypeCategory(querys = {}) {
	var queryStr = JSON.stringify(querys);
	return {
		name: app_const.QUERY_GAMETYPECATEGORY,
		callAPI: () => Utils.POST(`/api/game/type/category`, queryStr)()
	}
}

export function addGameTypeCategory(name, weight) {
	var payload = { name, weight };
	return {
		name: app_const.ADD_GAMETYPECATEGORY,
		callAPI: () => Utils.POST(`/api/game/type/category/add`, JSON.stringify(payload))()
	};
}

export function updateGameTypeCategory(category_id, name, weight, status) {
	var payload = { name, weight, status };
	return {
		name: app_const.UPDATE_GAMETYPECATEGORY,
		callAPI: () => Utils.POST(`/api/game/type/category/${category_id}/update`, JSON.stringify(payload))()
	};
}

/* ----------------------- Game Type Sort ---------------------------- */
export function queryGameTypeSort(querys = {}) {
	var queryStr = JSON.stringify(querys);
	return {
		name: app_const.QUERY_GAMETYPESORT,
		callAPI: () => Utils.POST(`/api/game/type/sort`, queryStr)()
	}
}

export function addGameTypeSort(name, weight) {
	var payload = { name, weight };
	return {
		name: app_const.ADD_GAMETYPESORT,
		callAPI: () => Utils.POST(`/api/game/type/sort/add`, JSON.stringify(payload))()
	};
}

export function updateGameTypeSort(sort_id, name, weight, status) {
	var payload = { name, weight, status };
	return {
		name: app_const.UPDATE_GAMETYPESORT,
		callAPI: () => Utils.POST(`/api/game/type/sort/${sort_id}/update`, JSON.stringify(payload))()
	};
}

/* ----------------------- Game Type Kind ---------------------------- */
export function queryGameTypeKind(querys = {}) {
	var queryStr = JSON.stringify(querys);
	return {
		name: app_const.QUERY_GAMETYPEKIND,
		callAPI: () => Utils.POST(`/api/game/type/kind`, queryStr)()
	}
}

export function addGameTypeKind(name, weight) {
	var payload = { name, weight };
	return {
		name: app_const.ADD_GAMETYPEKIND,
		callAPI: () => Utils.POST(`/api/game/type/kind/add`, JSON.stringify(payload))()
	};
}

export function updateGameTypeKind(kind_id, name, weight, status) {
	var payload = { name, weight, status };
	return {
		name: app_const.UPDATE_GAMETYPEKIND,
		callAPI: () => Utils.POST(`/api/game/type/kind/${kind_id}/update`, JSON.stringify(payload))()
	};
}

/* ----------------------- Game Algorithm ---------------------------- */
export function queryGameAlgorithm(querys = {}) {
	var queryStr = JSON.stringify(querys);
	return {
		name: app_const.QUERY_GAMEALGORITHM,
		callAPI: () => Utils.POST(`/api/game/algorithm`, queryStr)()
	}
}

export function updateGameAlgorithm(id, name, description, status) {
	var payload = { name, description, status };
	return {
		name: app_const.UPDATE_GAMEALGORITHM,
		callAPI: () => Utils.POST(`/api/game/algorithm/${id}`, JSON.stringify(payload))(),
		payload: Object.assign({}, payload, { id })
	};
}

/* ----------------------- Game Data ---------------------------- */
export function queryGameData(pageIndex = 1, querys = {}) {
	var queryParams = Object.assign({}, querys, {
		pageIndex
	});
	var queryStr = JSON.stringify(queryParams);
	return {
		name: app_const.QUERY_GAMEDATA,
		callAPI: () => Utils.POST(`/api/game/data`, queryStr)()
	}
}

/**
 * 查询指定彩种期号开奖数据
 * @param  {[type]} groupId [description]
 * @param  {[type]} qh         [description]
 * @return {[type]}            [description]
 */
export function getGameData(groupId, qh) {
	return {
		name: app_const.GET_GAMEDATA,
		callAPI: () => Utils.POST(`/api/game/data/${groupId}/${qh}`)()
	};
}

/**
 * 统计已开奖但未开奖数据
 * @return {[type]} [description]
 */
export function countFinishedUnOpenGameData() {
	return {
		name: app_const.COUNT_FINISHED_UNOPEN_GAMEDATA,
		callAPI: () => Utils.POST("/api/game/data/finish/count")()
	}
}

/**
 * 指定彩种期号开奖
 * @param  {[type]} groupId [description]
 * @param  {[type]} qh      [description]
 * @param  {[type]} nums    [description]
 * @return {[type]}         [description]
 */
export function openGameData(groupId, qh, nums) {
	return {
		name: app_const.OPEN_GAME_DATA,
		callAPI: () => Utils.POST(`/api/game/data/${groupId}/${qh}/open`, JSON.stringify(nums))()
	}
}

/**
 * 取消指定彩种期号
 * @param  {[type]} groupId [description]
 * @param  {[type]} qh      [description]
 * @param  {[type]} nums    [description]
 * @return {[type]}         [description]
 */
export function cancelGameData(groupId, qh) {
	return {
		name: app_const.CANCEL_GAME_DATA,
		callAPI: () => Utils.POST(`/api/game/data/${groupId}/${qh}/cancel`)()
	}
}

/* ----------------------- DealInfo ---------------------------- */
export function queryDealInfo(pageIndex = 1, querys = {}) {
	var queryParams = Object.assign({}, querys, {
		pageIndex
	});
	var queryStr = JSON.stringify(queryParams);
	return {
		name: app_const.QUERY_DEALINFO,
		callAPI: () => Utils.POST(`/api/dealinfo`, queryStr)()
	}
}

/**
 * 取消注单
 * @param  {[type]} dealId [description]
 * @return {[type]}        [description]
 */
export function cancelDealInfo(dealId) {
	return {
		name: app_const.CANCEL_DEALINFO,
		callAPI: () => Utils.POST(`/api/dealinfo/${dealId}/cancel`)()
	}
}

/* ----------------------- Play Limit ---------------------------- */
export function addPlayLimit(game_group, game_type, email, type, amount, status) {
	var payload = { game_group, game_type, email, type, amount, status };
	return {
		name: app_const.ADD_PLAYLIMIT,
		callAPI: () => Utils.POST(`/api/playlimit/add`, JSON.stringify(payload))()
	};
}

export function updatePlayLimit(id, game_group, game_type, email, type, amount, status) {
	var payload = { id, game_group, game_type, email, type, amount, status };
	return {
		name: app_const.UPDATE_PLAYLIMIT,
		callAPI: () => Utils.POST(`/api/playlimit/${id}/update`, JSON.stringify(payload))()
	};
}

export function queryPlayLimit(pageIndex = 1, querys = {}) {
	var queryParams = Object.assign({}, querys, { pageIndex });
	var queryStr = JSON.stringify(queryParams);
	return {
		name: app_const.QUERY_PLAYLIMIT,
		callAPI: () => Utils.POST(`/api/playlimit`, queryStr)()
	};
}

// rebate report
export function queryRebateReport(aid, email, from_date, to_date) {
	var queryParams = Object.assign({}, { aid: aid, email: email, from_date: from_date, to_date: to_date });
	var queryStr = JSON.stringify(queryParams);
	return {
		name: app_const.QUERY_REBATEREPORT,
		callAPI: () => Utils.POST(`/api/rebate/report`, queryStr)()
	};
}

// userprofit
export function queryUserProfit(pageIndex = 1, querys = {}) {
	var queryParams = Object.assign({}, querys, { pageIndex });
	var queryStr = JSON.stringify(queryParams);
	return {
		name: app_const.QUERY_USERPROFIT,
		callAPI: () => Utils.POST(`/api/userprofit`, queryStr)()
	};
}