import $ from "jquery";
import * as app_const from "../app_const";
import * as Utils from "../utils/common";

// config info
export function queryConfigInfo(querys = {}) {
	var queryStr = JSON.stringify(querys);
	return {
		name: app_const.QUERY_CONFIG,
		callAPI: () => Utils.POST(`/api/config`, queryStr)()
	}
}

export function addConfigInfo(name, value, description) {
	var payload = { name: name, value: value, desc: description };
	return {
		name: app_const.ADD_CONFIG,
		callAPI: () => Utils.POST(`/api/config/add`, JSON.stringify(payload))()
	};
}

export function updateConfig(id, name, value, desc) {
	var payload = { name, value, desc };
	return {
		name: app_const.UPDATE_CONFIG,
		callAPI: () => Utils.POST(`/api/config/update/${id}`, JSON.stringify(payload))(),
		payload: Object.assign({}, payload, { id })
	};
}