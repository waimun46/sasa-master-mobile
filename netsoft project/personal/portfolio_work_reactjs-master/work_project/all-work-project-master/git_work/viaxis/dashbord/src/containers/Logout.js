
import { hashHistory } from 'react-router';

export default function Logout() {
    // hashHistory.push('/login');
    window.location.href = "/login";
};