import ReduceBase from "./reduce_base";
import * as app_const from "../app_const";
import * as Utils from "../utils/common";


class NoticeReducer extends ReduceBase {
    static reduce(state = { isFetching: false }, action) {
        switch (action.type) {
            case app_const.QUERY_BULLETIN:
            case app_const.GET_BULLETIN:
            case app_const.ADD_BULLETIN:
            case app_const.UPDATE_BULLETIN:
            case app_const.QUERY_MAILMSG:
                return Object.assign({}, state, { isFetching: true });
            case `${app_const.QUERY_BULLETIN}_SUCCESS`:
                return NoticeReducer._query_success(state, action, "bulletins");
            case `${app_const.GET_BULLETIN}_SUCCESS`:
                return NoticeReducer._get_bulletin_success(state, action);
            case `${app_const.ADD_BULLETIN}_SUCCESS`:
            case `${app_const.UPDATE_BULLETIN}_SUCCESS`:
                return NoticeReducer._common_success(state, action);
            case `${app_const.QUERY_MAILMSG}_SUCCESS`:
                return NoticeReducer._query_success(state, action, "mailmsgs");
            case `${app_const.QUERY_BULLETIN}_FAILURE`:
            case `${app_const.GET_BULLETIN}_FAILURE`:
            case `${app_const.ADD_BULLETIN}_FAILURE`:
            case `${app_const.UPDATE_BULLETIN}_FAILURE`:
            case `${app_const.QUERY_MAILMSG}_FAILURE`:
                return Object.assign({}, state, { isFetching: false });
            default:
                return state;
        }
    }


    static _process_failure(state, action) {
        const resp = action.response;
        action.error = resp.message;
        action.error_code = resp.code;
        return Object.assign({}, state, {
            isFetching: false
        });
    }

    static _get_bulletin_success(state, action) {
        const resp = action.response;
        if (resp.code == 0) {
            return Object.assign({}, state, {
                isFetching: false,
                bulletin: resp.data
            });
        } else
            return NoticeReducer._process_failure(state, action);
    }

    /*static _query_success(state,action,field_name){
        const resp = action.response;
        if(resp.code==0){
            let data = {isFetching: false};
            data[field_name] = {
                pageIndex: resp.data.PageIndex,
                count: resp.data.TotalCount,
                data: resp.data.Data
            };
            return Object.assign({}, state, data);
        }else
            return NoticeReducer._process_failure(state,action);
    }*/
}

export { NoticeReducer };