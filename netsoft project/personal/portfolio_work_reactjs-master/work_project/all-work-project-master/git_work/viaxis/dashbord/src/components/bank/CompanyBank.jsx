import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { message, Row, Col, Table, Tag, Button, Modal, Form, Input, Select, DatePicker } from 'antd';

import BasicComponent from "../common/BasicComponent";

import { queryBank, queryCompanyBank, addCompanyBank, updateCompanyBank } from '../../actions/bank';
import { dateFormat, isEmptyObject } from "../../utils/string";
import { CommonStatus, BankType, SysRights, CommonStyleStatus } from "../../app_enums";
import * as RenderHelper from "../../utils/render";

const FormItem = Form.Item;
const Option = Select.Option;
const RangePicker = DatePicker.RangePicker;
const Message = message;

class CompanyBank extends BasicComponent {

    constructor(props) {
        super(props);
        this.pageIndex = 1;
        this.query = {};
        this.state = {
            loading: false,
            popup: false,
            popupTitle: "",
            edit: false,
            current_bank: {}
        };
    }

    componentWillMount() {
        this.queryData();
    }

    queryData() {
        this.props.dispatch(queryCompanyBank(this.pageIndex, this.query)).then(act => {
            if (act.error)
                Message.error(act.error);
        });
    }

    addBank() {
        const { data } = this.props;
        let banks = data.banks;
        if (!banks) {
            this.props.dispatch(queryBank()).then(it => {
                this.addBank();
            });
            return;
        }

        this.setState({
            popup: true,
            popupTitle: "添加银行",
            current_bank: {},
            edit: false
        });
    }

    updateBank(bank) {

        const { data } = this.props;
        let banks = data.banks;
        if (!banks) {
            this.props.dispatch(queryBank()).then(it => {
                this.updateBank(bank);
            });
            return;
        }

        this.setState({
            popup: true,
            popupTitle: "编辑" + bank.AccountNo,
            current_bank: bank,
            edit: true
        });
    }

    handleSubmit(e) {
        const { dispatch } = this.props;
        e.preventDefault();
        this.props.form.validateFields((errors, values) => {
            if (errors) return;
            if (this.state.edit) {
                let bank = this.state.current_bank;
                if (isEmptyObject(bank)) return;
                this.setState({ loading: true });
                dispatch(updateCompanyBank(bank.Id, values.bank_id, values.account_no, values.account_name,
                    values.branch, values.sign_key, values.gateway_url, values.deposit_limit, values.status)).then(act => {
                        this.setState({ loading: false });
                        if (act.error) {
                            Message.error(act.error);
                        } else {
                            if (act.response.code == 0) {
                                Message.success("修改成功");
                                this.props.form.resetFields();
                                this.setState({ edit: false, popup: false });
                                this.queryData();
                            } else
                                Message.error(act.resposen.message);
                        }
                    });
            } else {
                this.setState({ loading: true });
                dispatch(addCompanyBank(values.bank_id, values.account_no, values.account_name,
                    values.branch, values.sign_key, values.gateway_url, values.deposit_limit)).then(act => {
                        this.setState({ loading: false });
                        if (act.error) {
                            Message.error(act.error);
                        } else {
                            Message.success("添加成功");
                            this.props.form.resetFields();
                            this.setState({ edit: false, popup: false });
                            this.queryData();
                        }
                    });
            }
        });
    }

    handleReset(e) {
        e.preventDefault();
        this.props.form.resetFields();
    }

    hideModal() {
        this.setState({ popup: false });
    }

    tableColumns() {
        const self = this;
        const columns = [{ title: '#', render(text, record, index) { return index + 1; } },
        { title: '银行', dataIndex: 'BankName' },
        { title: "户名", dataIndex: "AccountName" },
        { title: "账号", dataIndex: "AccountNo" },
        { title: "分行", dataIndex: "BranchName" },
        { title: "累计充值限制", dataIndex: "TotalDepositLimit" },
        { title: "状态", dataIndex: "Status", render: RenderHelper.render_status(CommonStyleStatus) },
        { title: '时间', dataIndex: 'CreateTime' },
        ];

        if (this.hasRights(SysRights.BILL_UPDATE)) {
            columns.push({
                title: "操作", render(bank) {
                    return <Button type="primary" onClick={() => self.updateBank(bank)}>编辑</Button>
                }
            });
        }
        return columns;
    }

    render() {
        const { data } = this.props;
        let bank = data.company || {};

        let columns = this.tableColumns();
        const self = this;
        const pagination = {
            total: bank.count || 0,
            onChange(current) {
                self.pageIndex = current;
                self.queryData();
            },
        };

        const rowKey = function (bank) {
            return bank.Id;
        };

        return (
            <div>
                {
                    this.hasRights(SysRights.BILL_UPDATE) &&
                    <div style={{ marginBottom: 16 }}>
                        <Button type="primary" onClick={this.addBank.bind(this)}>添加银行</Button>
                    </div>
                }
                <Table columns={columns} dataSource={bank.data || []} loading={data.isFetching} pagination={pagination} rowKey={rowKey} />
                {this.render_modal()}
            </div>
        );
    }

    render_modal() {
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: { span: 5 },
            wrapperCol: { span: 18 },
        };
        const { banks } = this.props.data;
        if (!banks) return;
        return (
            <Modal title={this.state.popupTitle} visible={this.state.popup} onCancel={this.hideModal.bind(this)}
                footer={[
                    <Button key="back" type="ghost" size="large" onClick={this.hideModal.bind(this)}>取消</Button>,
                    <Button key="submit" type="primary" size="large" loading={this.state.loading} onClick={this.handleSubmit.bind(this)}>
                        提交
                </Button>,
                ]}>
                <Form horizontal>
                    <FormItem {...formItemLayout} label="银行">
                        {getFieldDecorator('bank_id', {
                            initialValue: (this.state.current_bank.BankId || "").toString(),
                            rules: [{
                                required: true,
                                message: '请选择发卡行'
                            }]
                        })(
                            <Select>
                                {banks.map(it => {
                                    return <Option key={it.BankCode} value={it.Id.toString()}>{it.Name}</Option>;
                                })}
                            </Select>
                        )}
                    </FormItem>
                    <FormItem {...formItemLayout} label="账户">
                        {getFieldDecorator('account_no', {
                            initialValue: this.state.current_bank.AccountNo || "",
                            rules: [{
                                required: true,
                                message: '请输入银行账户'
                            }]
                        })(
                            <Input autoComplete="off" />
                        )}
                    </FormItem>
                    <FormItem {...formItemLayout} label="户名">
                        {getFieldDecorator('account_name', {
                            initialValue: this.state.current_bank.AccountName || "",
                            rules: [{
                                required: true,
                                message: '请输入银行户名'
                            }]
                        })(
                            <Input autoComplete="off" />
                        )}
                    </FormItem>
                    <FormItem {...formItemLayout} label="开户行">
                        {getFieldDecorator('branch', {
                            initialValue: this.state.current_bank.BranchName || "",
                            rules: [{
                                required: true,
                                message: '请输入开户行'
                            }]
                        })(
                            <Input autoComplete="off" />
                        )}
                    </FormItem>
                    {this.state.edit && <FormItem {...formItemLayout} label="状态">
                        {getFieldDecorator('status', {
                            initialValue: (this.state.current_bank.Status || 0).toString(),
                            rules: [{
                                required: true,
                                message: '请选择状态'
                            }]
                        })(
                            <Select>
                                {Object.keys(CommonStatus).map(it => {
                                    return <Option key={it} value={it}>{CommonStatus[it]}</Option>;
                                })}
                            </Select>
                        )}
                    </FormItem>}
                    <FormItem {...formItemLayout} label="充值限制" help="只有用户的累计充值金额达到本设置，才允许使用本充值方式。">
                        {getFieldDecorator('deposit_limit', {
                            initialValue: (this.state.current_bank.TotalDepositLimit || '').toString(),
                            rules: [{ type: "string", pattern: /^\d+$/, message: "必须是数字" }]
                        })(
                            <Input autoComplete="off" />
                        )}
                    </FormItem>
                    <FormItem {...formItemLayout} label="签名密钥" help="只有第三方支付才需要填写">
                        {getFieldDecorator('sign_key', {
                            initialValue: this.state.current_bank.SignKey || ""
                        })(
                            <Input type="textarea" autoComplete="off" />
                        )}
                    </FormItem>
                    <FormItem {...formItemLayout} label="网关地址" help="只有第三方支付才需要填写">
                        {getFieldDecorator('gateway_url', {
                            initialValue: this.state.current_bank.GateWayUrl || ""
                        })(
                            <Input type="textarea" autoComplete="off" placeholder="http://xxx" />
                        )}
                    </FormItem>
                </Form>
            </Modal>
        );
    }
}


export default connect(state => ({
    data: state.bank,
}))(Form.create()(CompanyBank));
