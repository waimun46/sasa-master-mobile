import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Markdown from 'react-markdown';
import { message, Row, Col, Table, Tag, Button, Modal, Form, Input, Select, DatePicker } from 'antd';

import BasicComponent from "../common/BasicComponent";
import SearchForm from "../common/SearchForm";

import * as ArticleAction from '../../actions/article';
import * as RenderHelper from "../../utils/render";
import * as Enums from "../../app_enums";


class ArticleAdd extends BasicComponent {

    constructor(props) {
        super(props);
        this.state = {
            loading:false,
            markdownSrc:''
        };
    }

    /* ----------------------- Sys ----------------------- */

    componentWillMount() {
        this.queryData();
    }

    /* ----------------------- Functions ---------------------- */

    queryData(){
        this.props.dispatch(ArticleAction.queryCategory());
    }

    /* ------------------------ Events ------------------------ */

    handleSubmit(e){
        const { dispatch } = this.props;
        e.preventDefault();
        this.props.form.validateFields((errors, values) => {
            if (errors) return;
            this.setState({loading:true});

            dispatch(ArticleAction.addArticle(values.categoryId, values.urlName, values.meta, values.title, values.content)).then(act=>{
                this.setState({loading:false});
                if(act.error){
                    message.error(act.error);
                }else{
                    message.success("添加成功");
                    this.props.form.resetFields();
                }
            });
        });
    }

    handleReset(e){
        this.props.form.resetFields();
    }

    onMarkdownChange(event){
        this.setState({markdownSrc:event.target.value})
    }

    /* ------------------------ Renders ------------------------ */

    render() {
        const { isFetching } = this.props;
        const categorys = this.props.category || [];

        const { getFieldDecorator, isFieldValidating, getFieldError } = this.props.form;
        const formItemLayout = {
            labelCol: { span: 4 },
            wrapperCol: { span: 16 },
        };

        const formItemmarkedLayout = {
            labelCol: { span: 4 },
            wrapperCol: { span: 8 },
        };

        return (
            <Form horizontal>
                <Form.Item {...formItemLayout} label="标题" hasFeedback>
                    {getFieldDecorator('title', {
                        rules: [
                            { required: true, message: '请输入文章标题' },
                        ],
                    })(
                        <Input />
                    )}
                </Form.Item>
                <Form.Item {...formItemLayout} label="Url地址" hasFeedback>
                    {getFieldDecorator('urlName', {
                        rules: [
                            { required: true, message: '请输入Url地址' },
                        ],
                    })(
                        <Input />
                    )}
                </Form.Item>
                <Form.Item {...formItemLayout} label="目录" hasFeedback>
                    {getFieldDecorator('categoryId', {
                        rules: [
                            { required: true, message: '请选择文章所属目录' },
                        ],
                    })(
                        <Select>
                            {categorys.map(it=><Select.Option value={it.Id.toString()} key={it}>{it.Name}</Select.Option>)}
                        </Select>
                    )}
                </Form.Item>
                <Form.Item {...formItemLayout} label="Meta" hasFeedback>
                    {getFieldDecorator('meta', {
                        rules: [
                        { required: true, whitespace: false, message: '请输入Meta' },
                        ],
                    })(
                        <Input type="textarea" />
                    )}
                </Form.Item>
                <Form.Item {...formItemLayout} label="内容">
                    {getFieldDecorator('content', {
                        rules: [
                        { required: true, whitespace: false, message: '请输入内容' },
                        ],
                    })(
                        <Input type="textarea" autosize={{minRows:15}} onChange={this.onMarkdownChange.bind(this)}/>
                    )}
                </Form.Item>
                <Form.Item {...formItemLayout} label="preview">
                      <Markdown source={this.state.markdownSrc} />
                </Form.Item>
                <Form.Item wrapperCol={{ span: 12, offset: 7 }}>
                      <Button type="primary" loading={this.state.loading} onClick={this.handleSubmit.bind(this)}>提交</Button>
                      &nbsp;&nbsp;&nbsp;
                      <Button type="ghost" onClick={this.handleReset.bind(this)}>重置</Button>
                </Form.Item>
            </Form>
        );
    }


}

export default connect(state => ({
    isFetching: state.article.isFetching,
    category: state.article.Category
}))(Form.create()(ArticleAdd));