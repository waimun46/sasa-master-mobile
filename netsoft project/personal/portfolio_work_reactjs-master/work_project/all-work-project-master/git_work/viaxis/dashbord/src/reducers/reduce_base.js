/**
 * Action 格式说明：
 * {
 *      type:str,                   // 类型
 *      state:enums.ActionState,    // 状态
 *      data:object,                // 数据
 *      code:int,                   // 状态码
 *      message:str                 // 说明
 * }
 *
 * State 格式说明 :
 * {
 *  // 这是最外层的State
 *  passport:{
 *      // 这是第一层State
 *      userinfo:{
 *          // 这是具体State
 *          isFetching: bool    // 是否更新中，
 *          data:object         // 最终数据
 *      }
 *      ...
 *  }
 * }
 */

export default class ReduceBase {

    static _common_success(state, action) {

        const resp = action.response;
        if (resp.code == 0) return state;

        return ReduceBase._process_failure(state, action);
    }

    static _process_failure(state, action) {
        const resp = action.response;
        action.error = resp.message;
        action.error_code = resp.code;
        return Object.assign({}, state, {
            isFetching: false
        });
    }

    static _query(state, action, field_name) {
        return Object.assign({}, state, {
            isFetching: true
        });
    }

    static _query_success(state, action, field_name) {
        const resp = action.response;
        if (resp.code == 0) {
            let data = { isFetching: false };
            data[field_name] = {
                pageIndex: resp.data.PageIndex,
                count: resp.data.TotalCount,
                data: resp.data.Data
            };
            return Object.assign({}, state, data);
        } else
            return ReduceBase._process_failure(state, action);
    }

    static _list_success(state, action, field_name) {
        const resp = action.response;
        if (resp.code == 0) {
            let data = { isFetching: false };
            data[field_name] = resp.data;
            return Object.assign({}, state, data);
        } else
            return ReduceBase._process_failure(state, action);
    }
};