import React from 'react';
import { connect } from 'react-redux';
import { Icon, Tooltip, Spin } from 'antd';

import * as Utils from "../../../utils/common";
import * as GameAction from "../../../actions/game";

class GameType extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            // 玩法数据加载中
            loading: true,
            // 当前选中的玩法目录
            selectedCategory: null,
            // 当前选中的玩法种类
            selectedSort: null,
            // 当前选中的玩法
            selectedType: null,
        }
    }

    /* ------------------- Sys ------------------*/

    componentWillMount() {
        this.getGameTypeInfo(this.props);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.Group != this.props.Group)
            this.getGameTypeInfo(nextProps);

        let { game_type_info } = this.props;
        let new_game_type_info = nextProps.game_type_info;
        if (game_type_info != new_game_type_info) {
            // TODO:应该是由数据指定默认
            this.updateSelectType(new_game_type_info[0]);
        }
    }

    /* ------------------- Function ------------------*/

    getGameTypeInfo(props){
        this.props.dispatch(GameAction.getGameTypeInfo(props.Group)).then(it => this.setState({
            loading: false
        }));
    }

    sort(list) {
        list.sort((a, b) => b.Weight - a.Weight);
        return list;
    }
    

    render(){
        const { game_type_info } = this.props;
        return(
            <div className="game-trend-body-gametype">
                彩种:重庆时时彩
            </div>
        );
    }

    render_panel() {
        const { game_type_info } = this.props;
        console.log(this.props)
        let selectedCategoryIndex = game_type_info.indexOf(this.state.selectedCategory);
        let otherCategorys = [...game_type_info.slice(0,selectedCategoryIndex),...game_type_info.slice(selectedCategoryIndex+1)];
        return (
            <div className="game-bettype">
                <div className="game-bettype-group fn-clear">
                    {otherCategorys.map(category=><label key={category.Name} onClick={()=>this.onCategoryClick(category)}>{category.Name}</label>)}
                    <ul>
                        {this.state.selectedCategory.Sorts.map(sort=><li key={sort.Name} className={sort==this.state.selectedSort?"active":""} onClick={()=>this.onSortClick(sort)}>{sort.Name}</li>)}
                    </ul>
                </div>
                <div className="game-bettype-types">
                    {this.state.selectedSort.Kinds.map(kind=>
                        <div className="fn-clear" key={kind.Name}>
                            <label>{kind.Name}</label>
                            <ul>
                                {this.sort(kind.Types).map(type=><li key={type.Name} className={type==this.state.selectedType?"active":""} onClick={()=>this.onTypeClick(type)}><a>{type.Name}</a></li>)}
                            </ul>
                        </div>
                    )}
                </div>
                <div className="game-bettype-explain fn-clear">
                    <label>范例</label>
                    <Tooltip placement="topLeft" trigger={"click"} title={this.state.selectedType.Explain}>
                        <p>{this.state.selectedType.Explain}</p>
                    </Tooltip>
                    <Tooltip title={this.state.selectedType.Example}>
                        <Icon type="question-circle" />
                    </Tooltip>
                </div>
            </div>
        );
    }
}


module.exports = connect(state => ({
    game_type_info: state.game.game_type_info
}))(GameType);
