import $ from "jquery";
import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { Affix, Spin } from "antd";
import ReactMarkdown from "react-markdown";
import 'github-markdown-css';

import BasicComponent from "../common/BasicComponent";
import Footer from '../common/Footer';

import * as ArticleAction from '../../actions/article';
import * as Constants from "../../app_constants";
import * as Utils from "../../utils/common";
import * as RenderHelper from "../../utils/render_helper";


class Article extends BasicComponent {

    constructor(props) {
        super(props);
        this.category = props.params.category;
        this.articleName = props.params.articleName;
        this.state = {
            navPadTop: true
        }
    }

    /* ------------------- Sys ------------------- */

    componentWillMount(){
        this.getArticle();
        this.getArticleByCategory();
    }

    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll.bind(this));
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll.bind(this));
    }

    /* ------------------- Function ---------------------- */

    getArticle(){
        this.props.dispatch(ArticleAction.getArticle(this.category, this.articleName));
    }

    getArticleByCategory(){
        this.props.dispatch(ArticleAction.getArticleByCategory(this.category));
    }

    /* ------------------- Events ----------------------- */

    handleScroll(e) {
        // 滚动条离页面最上方的距离
        const scrollTop = document.body.scrollTop;
        if (scrollTop >= 20 && this.state.navPadTop)
            this.setState({navPadTop:false});
        else if (scrollTop < 20 && !this.state.navPadTop)
            this.setState({navPadTop:true});
    }

    /* ------------------- Renders ----------------------- */

    render() {
        let headerClass = "article-page-header";
        if (this.state.navPadTop)
            headerClass += " paddingTop";

        const article = this.props.article.data;

        return (
            <div className="article-page">
                <div className={headerClass}>
                    <div className="nav-wrapper">
                        <div className="nav-content fn-clear">
                            <a href="/" className="nav-logo">
                                <img src="/logo.svg" />
                            </a>

                            <ul className="nav">
                                <li><a>首页</a></li>
                                <li><a>活动</a></li>
                                <li><a>帮助</a></li>
                                <li><a>关于我们</a></li>
                                <li><Link to="/login">登陆</Link></li>
                                <li><Link to="/register">注册</Link></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="article-page-wrapper">
                {article?
                    this.content
                    :
                    <div className="fn-text-center pt10">
                        <Spin tip="努力加载中..."/>
                    </div>
                }
                </div>
                <Footer/>
            </div>
        );
    }

    get content(){
        const article = this.props.article.data;
        const articles = this.props.articles.data || [];
        const content = [<div className="list-wrapper">
                            <Affix offsetTop={120}>
                                <ul>
                                    {articles.map(it=>
                                        <li>
                                            <a href={`/${it.Category}/${it.UrlName}.html`}>{it.Title}</a>
                                        </li>
                                    )}
                                </ul>
                            </Affix>
                        </div>,
                        <section>
                            <article className="markdown-body">
                                <h1>{article&&article.Title}</h1>
                                <section>
                                    <ReactMarkdown source={article&&article.Content} />
                                </section>
                            </article>
                        </section>];

        return content;
    }

}

module.exports = connect(state => ({
    article: state.article.article,
    articles: state.article.articleCategory
}))(Article);
