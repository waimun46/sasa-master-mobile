import $ from "jquery";
import { ActionState } from "../app_constants";
import { POST, GET } from "../utils/common";
import getSocket from "../websocket";

const socket = getSocket();

export function getBulletins(status) {
	var body = JSON.stringify({
		status,
	});
	return {
		name: "Bulletins",
		callAPI: () => POST("/api/notice/bulletins", body)()
	};
}
