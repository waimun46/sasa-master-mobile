import React from 'react';
import { browserHistory,Link } from 'react-router'

class PageNotFound extends React.Component {
  render() {
    return (
      <div>
        <h1>Page Not Found.</h1>
        <p>Go to <Link to="/bet">Home Page</Link></p>
      </div>
    )
  }
}

module.exports = PageNotFound;
