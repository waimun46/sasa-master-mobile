import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { message, Table, Breadcrumb, Tooltip, DatePicker, Button, Form, Select, Input, Icon } from 'antd';
import Moment from 'moment';

import BasicComponent from "../common/BasicComponent";
import DashContainer from '../../containers/DashContainer';
import SearchForm from "../common/SearchForm";

import * as PassportAction from '../../actions/passport';
import * as ReportAction from "../../actions/report";
import * as Constants from "../../app_constants";
import * as Utils from "../../utils/common";
import * as RenderHelper from "../../utils/render_helper";


class ProfitDividend extends BasicComponent{

    constructor(props) {
        super(props);
        this.userId = this.getUserId(props);
        this.defaultQuerys = this.querys = {
            from_date: this.getLastProfitDate()
        }
    }

    /* ------------------ Sys -------------------- */

    componentWillMount(){
        this.queryData();
    }

    componentWillReceiveProps(newProps) {
        if(newProps!=this.props){
            const userId = this.getUserId(newProps);
            if (userId != this.userId && !isNaN(userId)) {
                this.userId = userId;
                this.queryData();
            }
        }

    }
    /* ------------------ Functions -------------------- */

    /**
     * 获取上一次生成红利的时间
     * @return {[type]} [description]
     */
    getLastProfitDate(){
        var now = Moment();

        if (now.date() >= 16)
            now.set("date", 1);
        else
            now.add(-1, "months").set("date", 16);

        return now.format("YYYY-MM-DD");
    }

    queryData(){
        let parents = this.props.parents.data;
        if (this.userId || parents != null || parent.length > 0)
            this.props.dispatch(PassportAction.getParents(this.userId));

        this.props.dispatch(ReportAction.queryProfitDividend(this.userId, this.querys));
    }

    getUserId(props){
        let userId = props.params.userId;
        if (userId)
            userId = parseInt(userId);
        return userId;
    }

    disabledDate(current){
        return !(current && [1, 16].indexOf(current.date()) >= 0);
    }

    /* ------------------ Events -------------------- */

    onSearchClick(values){
        this.querys = Object.assign({},this.querys,values);
        this.queryData();
    }

    onResetClick(){
        this.querys = this.defaultQuerys;
    }

    /* ------------------ Renders -------------------- */

    render(){
        const self = this;
        const { profitDividend } = this.props;
        let data = profitDividend.data || [];

        return (
            <DashContainer title="分红报表">
                {this.renderSearch()}
                {this.renderBreadcrumb()}
                <div className="search-result-list">
                    <Table loading={profitDividend.isFetching} columns={this.columns} dataSource={data} rowKey="UserId" />
                </div>
            </DashContainer>
        );
    }

    renderBreadcrumb(){
        let parents = this.props.parents.data;
        if (parents == null || parents.length == 0)
            if(this.props.userinfo.data)
                parents = [this.props.userinfo.data];

        if(parents==null)
            parents=[]
        return (
            <Breadcrumb separator=">">
                {parents.map((it,i) => <Breadcrumb.Item key={i}><Link to={`/profit/dividend/${it.UserId}`}>[{Constants.UserKind[it.UserKind]}]{it.Email}</Link></Breadcrumb.Item>)}
            </Breadcrumb>
        );
    }

    renderSearch(){
        const formItemLayout = {
            labelCol: { span: 8 },
            wrapperCol: { span: 16 }
        };

        return (
            <SearchForm onSearch={this.onSearchClick.bind(this)} onReset={this.onResetClick.bind(this)}>
                <email label="邮箱" {...formItemLayout}>
                    <Input />
                </email>
                <from_date label="时间" {...formItemLayout} format={it=>it.format("YYYY-MM-DD")}>
                    <DatePicker disabledDate={this.disabledDate.bind(this)} />
                </from_date>
            </SearchForm>
        );
    }

    get columns() {
        const userinfo = this.props.userinfo.data || {};
        const columns = [
            {title: '#',render: RenderHelper.render_index},
            {title: '邮箱', dataIndex: 'Email', render:(email, profit)=>{
                if(profit.UserId == this.userId || profit.UserId==userinfo.UserId)
                    return `[${Constants.UserKind[profit.UserKind]}] ${email}`;
                return <Link to={`/profit/dividend/${profit.UserId}`}>[{Constants.UserKind[profit.UserKind]}] {email}</Link>
            }}, 
            {title: "日期", render:it=>{
                return `${it.FromDate} - ${it.ToDate}`;
            }},
            {title: "红利比例", dataIndex: "Scale", className: "column-money", render: RenderHelper.render_percent()},
            {title: "团队总红利", dataIndex: "TotalDividend", className: "column-money", render: RenderHelper.money_format},
            {title: "自身红利", dataIndex: "SelfDividend", className: "column-money", render: RenderHelper.money_format},
            {title: <span>
                        团队盈亏&nbsp;&nbsp;
                        <Tooltip placement="topLeft" title="当团队盈亏与自身盈亏都为负数时，团队盈亏会自动减去自身盈亏。" arrowPointAtCenter>
                            <Icon type="info-circle-o" />
                        </Tooltip></span>,
                    dataIndex: "TeamProfit", className: "column-money", render: RenderHelper.money_format_color},
            {title: "自身盈亏", dataIndex: "SelfProfit", className: "column-money", render: RenderHelper.money_format_color},
            {title: "有效投注", dataIndex: "TotalValidBet", className: "column-money", render:RenderHelper.money_format}
        ];
        return columns;
    }
}

module.exports = connect(state => ({
    userinfo: state.passport.userinfo,
    profitDividend: state.report.profitDividend,
    parents: state.passport.parents
}))(ProfitDividend);
