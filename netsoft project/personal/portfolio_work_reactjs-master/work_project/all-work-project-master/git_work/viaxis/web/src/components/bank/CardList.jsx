import React from 'react';
import { connect } from 'react-redux';
import { message, Row, Col, Card, Alert, Spin, Button, Form, Select, Input, Icon } from 'antd';

import BasicComponent from "../common/BasicComponent";
import DashContainer from '../../containers/DashContainer';

import * as Passport from "../../actions/passport";
import * as Bank from "../../actions/bank";
import Auth from "../../auth";
import * as Constants from "../../app_constants";
import * as Utils from "../../utils/common";
import * as Validators from "../../utils/validator";
import * as RenderHelper from "../../utils/render_helper";

class AddForm extends BasicComponent {

    constructor(props) {
        super(props);
        this.state = { loading: false };
    }

    handleSubmit(e) {
        e.preventDefault();

        const { dispatch } = this.props;

        this.props.form.validateFields((errors, values) => {
            if (errors) return;
            this.setState({ loading: true });
            this.props.onSubmit && this.props.onSubmit(values).then(act => {
                this.setState({ loading: false });

                const resp = act.response;
                if (resp.code == 0) {
                    message.success("添加成功");
                    if (!this.realname && values.account_name) {
                        Auth.update("realname", values.account_name);
                    }
                } else {
                    message.error(resp.message);
                }

            });

        });
    }

    /* ---------------------------- Renders  ------------------------------*/
    render() {
        const { getFieldDecorator } = this.props.form;

        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 14 },
        };

        const tailFormItemLayout = {
            wrapperCol: {
                span: 14,
                offset: 6,
            },
        };

        const { banks } = this.props;

        return (
            <Form onSubmit={this.handleSubmit.bind(this)} autoComplete="off">
                <Form.Item label="开户银行" {...formItemLayout} hasFeedback>
                    {getFieldDecorator('BankId', {
                        rules: [{
                            required: true, message: '请选择您要添加的银行卡开户行'
                        }],
                    })(
                        <Select>
                            {banks.map(it => <Select.Option key={it.Id} value={it.Id.toString()}><i className={`iconfont icon-${it.BankCode}`}></i> {it.Name}</Select.Option>)}
                        </Select>
                    )}
                </Form.Item>
                <Form.Item label="持卡人" {...formItemLayout} hasFeedback extra="必须与您开卡账户姓名一致。且以后无法修改。">
                    {
                        this.realname
                            ?
                            <p><span className="ant-form-text">{Utils.maskStart(this.realname, 1)}</span></p>
                            :
                            getFieldDecorator('AccountName', {
                                rules: [{
                                    required: true, message: "请输入持卡人姓名"
                                }],
                            })(
                                <Input />
                            )
                    }
                </Form.Item>
                <Form.Item label="银行卡号" {...formItemLayout} hasFeedback>
                    {getFieldDecorator('AccountNo', {
                        rules: [{
                            required: true, message: "请输入您的银行卡号"
                        }, {
                            validator: Validators.checkBankCard,
                            message: "无效的银行卡号"
                        }],
                    })(
                        <Input />
                    )}
                </Form.Item>
                <Form.Item label="确认卡号" {...formItemLayout} hasFeedback>
                    {getFieldDecorator('AccountNoConfirm', {
                        rules: [{
                            required: true, message: "请输入您的银行卡号"
                        }, {
                            validator: Validators.checkEqual, message: "两次输入卡号不一致。",
                            getFieldValue: this.props.form.getFieldValue,
                            equalEleName: "AccountNo"
                        }],
                    })(
                        <Input />
                    )}
                </Form.Item>
                <Form.Item label="支行信息" {...formItemLayout} hasFeedback extra="开行具体名，可拨打银行服务热线咨询。">
                    {getFieldDecorator('BranchName', {
                        rules: [{
                            required: true, message: '请输入支行信息'
                        }],
                    })(
                        <Input placeholder="如:上海浦东人民支行" />
                    )}
                </Form.Item>
                <Form.Item label="安全密码" {...formItemLayout} hasFeedback>
                    {getFieldDecorator('Password', {
                        rules: [{
                            required: true, message: "请输入您的安全密码"
                        }],
                    })(
                        <Input type="password" />
                    )}
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                    <Button loading={this.state.loading} className="fn-width100" type="primary" htmlType="submit" size="large">确定</Button>
                </Form.Item>
            </Form>
        );
    }
}
AddForm.propTypes = {
    /**
     * 银行列表
     * @type {[type]}
     */
    banks: React.PropTypes.array.isRequired,
    /**
     * 确定提交事件
     * @param {dict} values 表单内容
     * @type {[type]}
     */
    OnSubmit: React.PropTypes.func
};
const AddFormWraper = Form.create()(AddForm);

class BankCardList extends BasicComponent {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            // 是否已设置安全密码
            hasSafePassword: true,
        };
    }

    /* ---------------------------- System  ------------------------------*/

    componentWillMount() {

        const { cards } = this.props;
        if (!cards.data)
            this.queryBankCard();

        this.queryHasSafePassword()
    }



    /* ---------------------------- Functions  ------------------------------*/
    queryBankCard() {
        this.props.dispatch(Bank.getBankCards());
    }

    queryHasSafePassword() {
        this.props.dispatch(Passport.hasSafePassword()).then(it => {

            const resp = it.response;
            if (resp.code == 0) {
                this.setState({ hasSafePassword: resp.data });
            }

        });
    }

    componentWillUpdate(nextProps, nextState) {
        // 未设置安全密码
        if (nextState.hasSafePassword != this.state.hasSafePassword && !nextState.hasSafePassword) {
            RenderHelper.warning("您未设置安全密码", "您需要设置安全密码后才可继续下列的操作", () => this.goto("/passport/password/safe?redirect=/bank"));
        }
    }

    /* ---------------------------- Events  ------------------------------*/

    onAddClick() {
        const { banks } = this.props;

        const cards = this.props.cards.data || [];


        const dialog = (data) => RenderHelper.dialog("添加银行卡", <AddFormWraper banks={data} onSubmit={this.onAddSubmit.bind(this)} />);

        if (!banks.data) {
            const hide = message.loading('Loading...', 0);
            this.props.dispatch(Bank.getBank()).then(act => {
                hide();
                this._dialog = dialog(act.response.data);
            });
            return;
        }

        this._dialog = dialog(banks.data);


    }

    onAddSubmit(form) {
        return this.props.dispatch(Bank.addBankCard(form.BankId, form.AccountNo,
            form.AccountName || this.realname,
            form.BranchName, form.Password)).then(act => {
                const resp = act.response;
                if (resp.code == 0) {
                    if (this._dialog) {
                        this._dialog.destroy();
                        this._dialog = null;
                    }
                } else {
                    message.error(resp.message);
                }
                return act;
            });
    }

    /* ---------------------------- Renders  ------------------------------*/
    render() {
        return (
            <DashContainer title="银行卡管理">
                <br />
                <div className="fn-clear">
                    <div className="bank-add">
                        <ul>
                            <li><Button type="primary" icon="plus" size="large" onClick={this.onAddClick.bind(this)}>添加银行卡</Button></li>
                            <li><Alert type="info" showIcon message="银行卡持卡人一经绑定以后无法修改，最多可绑定5张银行卡！" /></li>
                        </ul>
                        <br />
                    </div>
                    {this.renderCards()}
                </div>
            </DashContainer>
        );
    }

    renderCards() {
        const cards = this.props.cards.data || [];
        return (
            <div className="bank-card-list">
                {cards.map(it =>
                    <Card key={it.id} title={<span><i className={`iconfont icon-${it.BankCode}`}></i>{it.BankName}</span>} extra={`** ${it.AccountNo.slice(-4)}`}>
                        <div>
                            <b>持卡人</b>
                            {it.AccountName}
                        </div>
                        <div>
                            <b>支行信息</b>
                            {it.BranchName}
                        </div>
                    </Card>
                )}
            </div>
        );
    }
}

module.exports = connect(state => ({
    banks: state.bank.banks,
    cards: state.bank.cards,
}))(BankCardList);
