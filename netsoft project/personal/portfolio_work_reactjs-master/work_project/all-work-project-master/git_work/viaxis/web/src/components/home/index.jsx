import React from 'react';
import ReactDOM from 'react-dom';
import { scrollScreen } from 'rc-scroll-anim';

import Nav from './Nav';
import Banner from './Banner';
import HotLotterys from './HotLotterys';
import Content2 from './Content2';
import Content3 from './Content3';
import Content4 from './Content4';
import Content5 from './Content5';
import Footer from '../common/Footer';
import Point from './Point';

import './less/antMotion_style.less';

class Home extends React.Component {
    componentDidMount() {
        // 点的位置居中
        const list = ReactDOM.findDOMNode(this.refs.list);
        const listHeight = list.getBoundingClientRect().height;
        list.style.marginTop = ` -${listHeight / 2}px`;
    }

    render() {
        const children = [
            <Nav id="Nav" key="Nav"/>,
            <Banner id="Banner" key="Banner"/>,
            <HotLotterys id="HotLotterys" key="HotLotterys" />,
            <Content2 id="Content2" key="Content2"/>,
            <Content3 id="Content3" key="Content3"/>,
            <Content4 id="Content4" key="Content4"/>,
            <Content5 id="Content5" key="Content5"/>,
            <Footer id="Footer" key="Footer"/>,
            // 导航和页尾不进入锚点区，如果需要，自行添加;
            <Point key="list" ref="list" data={['Banner', 'HotLotterys', 'Content2', 'Content3', 'Content4', 'Content5']} />,
        ];
        return (
            <div className="home-wrapper">
                {children}
            </div>
        );
    }
}

module.exports = Home;
