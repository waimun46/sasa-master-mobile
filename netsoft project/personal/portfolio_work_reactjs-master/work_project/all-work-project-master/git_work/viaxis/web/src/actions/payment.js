import { POST, GET } from "../utils/common";

export function getPayChannelInfo(channelId) {
    var body = JSON.stringify({
        ChannelId: channelId
    });
    return {
        name: "PayChannelInfo",
        callAPI: () => POST(`/api/deposit/getpaychannel`, body)()
    };
}

export function submitPayRequest(amount) {
    var body = JSON.stringify({
        PayAmount: amount
    });
    return {
        name: "submitPayRequest",
        callAPI: () => POST(`/api/deposit/bipipay/payrequest`, body)()
    };
}