import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router'
import { message, Row, Col, Table, Breadcrumb, Tooltip, Switch, Spin, Form, Input, InputNumber, Button, Select, Icon } from 'antd';

import BasicComponent from "../common/BasicComponent";
import DashContainer from '../../containers/DashContainer';
import SearchForm from "../common/SearchForm";
import { Transfer } from "../bill/Transfer";

import * as PassportAction from '../../actions/passport';
import * as Constants from "../../app_constants";
import * as Enums from "../../app_enum";
import * as Utils from '../../utils/common';
import * as Validators from "../../utils/validator";
import * as RenderHelper from '../../utils/render_helper';

class UserList extends BasicComponent {

    constructor(props) {
        super(props);
        const userId = this.getUserId(props);
        this.userId = isNaN(userId) ? null : userId;
        this.pageIndex = 1;
        this.querys = {};
    }

    /* ---------------------------- Sys ------------------------------*/

    componentWillMount() {
        this.queryTeam();
    }

    componentWillReceiveProps(newProps) {
        if (newProps != this.props) {
            const userId = this.getUserId(newProps);
            if (userId != this.userId && !isNaN(userId)) {
                this.userId = userId;
                this.pageIndex = 1;
                this.queryTeam();
            }
        }
    }

    /* ---------------------------- Functions ------------------------------*/

    queryTeam() {
        if (this.userId)
            this.props.dispatch(PassportAction.getParents(this.userId));
        this.props.dispatch(PassportAction.queryTeam(this.userId, this.pageIndex));
    }

    queryData() {
        if (this.isNullOrEmpty(this.querys))
            this.queryTeam();
        else
            this.props.dispatch(PassportAction.queryUsers(this.pageIndex, this.querys));
    }

    getUserId(props) {
        let userId = props.params.userId;
        if (userId)
            userId = parseInt(userId);
        return userId;
    }

    isNullOrEmpty(values) {
        let isNull = true;
        try {
            Object.keys(values).forEach(it => {
                if (!Utils.isEmptyOrNull(values[it])) {
                    isNull = false;
                    throw "break";
                }
            });
        } catch (e) {
            if (e === "break") return;
            else
                throw e;
        }
        return isNull;
    }

    /* ---------------------------- Events ------------------------------*/

    onSearchClick(values) {
        this.querys = values;
        this.queryData();
    }

    onResetClick() {
        this.querys = {};
    }

    onAddChildClick() {
        let parents = this.props.parents.data;
        if (parents == null || parents.length == 0)
            if (this.props.userinfo.data)
                parents = [this.props.userinfo.data];

        if (parents == null || parents.length == 0) return;

        const parent = parents[parents.length - 1];
        if (parent == null) return;

        // 会员与子用户不允许
        if (parent.UserKind >= Enums.UserKind.Member) return;

        const userKind = Constants.UserKind[parent.UserKind + 1];

        const dialog = (it) => RenderHelper.dialog(`添加${userKind}`, <AddFormWraper parent={it} onSubmit={this.onAddSubmit.bind(this)} />);
        this._dialog = dialog(parent);
    }

    onAddSubmit(values) {
        if (!values.backpct) values.backpct = 0
        if (!values.dailywages) values.dailywages = 0
        return this.props.dispatch(PassportAction.addAgent(values.email, values.password, values.aid,
            values.backpct.toFixed(2), values.dailywages.toFixed(2), values.userKind, values.remark,
            values.mobile, values.qq, values.captcha))
            .then(act => {
                const resp = act.response;
                if (resp.code == 0) {
                    if (this._dialog) {
                        this._dialog.destroy();
                        this._dialog = null;
                    }
                }
                return act;
            });
    }

    onEditUserClick(user) {
        if (!user) return;

        let parents = this.props.parents.data;
        if (parents == null || parents.length == 0)
            if (this.props.userinfo.data)
                parents = [this.props.userinfo.data];

        if (parents == null || parents.length == 0) return;

        const parent = parents[parents.length - 1];
        if (parent == null) return;

        const dialog = () => RenderHelper.dialog(`编辑${user.Email}`, <EditFormWraper parent={parent} user={user} onSubmit={(it) => this.onEditSubmit(user, it)} />);
        this._dialog = dialog();
    }

    onEditSubmit(user, values) {
        if (!values.backpct) values.backpct = 0
        if (!values.dailywages) values.dailywages = 0
        return this.props.dispatch(PassportAction.updateUser(user.UserId, values))
            .then(act => {
                const resp = act.response;
                if (resp.code == 0) {
                    if (this._dialog) {
                        this._dialog.destroy();
                        this._dialog = null;
                        this.queryData()
                    }
                }
                return act;
            });
    }

    transfer(user) {
        if (!user) return;

        const dialog = () => RenderHelper.dialog(`转账 ${user.Email}`, <Transfer dispatch={this.props.dispatch}
            balance={this.props.balance}
            targetUser={user}
            onCompleted={() => {
                if (this._dialog) {
                    this._dialog.destroy();
                    this._dialog = null;
                    this.queryData()
                }
            }} />);
        this._dialog = dialog();
    }

    /* ---------------------------- Renders ---------------------------- */

    render() {
        const self = this;
        const { team } = this.props;
        let data = team.data || [];

        const pagination = {
            total: team.count || 0,
            onChange(current) {
                self.pageIndex = current;
                self.queryData();
            },
        };

        return (
            <DashContainer title="团队管理" extra={<Link to="/user/referral">会员推广</Link>}>
                {this.renderSearch()}
                {this.renderBreadcrumb()}
                <div className="search-result-list">
                    <Table loading={team.isFetching} columns={this.columns} dataSource={data} rowKey="UserId" pagination={pagination} />
                </div>
            </DashContainer>
        );
    }

    renderBreadcrumb() {
        let parents = this.props.parents.data;
        if (parents == null || parents.length == 0)
            if (this.props.userinfo.data)
                parents = [this.props.userinfo.data];

        if (parents == null)
            parents = []
        return (
            <Breadcrumb separator=">">
                {parents.map((it, i) => <Breadcrumb.Item key={i}><Link to={`/user/${it.UserId}`}>[{Constants.UserKind[it.UserKind]}]{it.Email}</Link></Breadcrumb.Item>)}
            </Breadcrumb>
        );
    }

    renderSearch() {
        const formItemLayout = {
            labelCol: { span: 8 },
            wrapperCol: { span: 16 }
        };

        const userinfo = this.props.userinfo.data;

        return (
            <SearchForm showAddBtn onSearch={this.onSearchClick.bind(this)} onReset={this.onResetClick.bind(this)} onAdd={this.onAddChildClick.bind(this)}>
                <email label="邮箱" {...formItemLayout}>
                    <Input />
                </email>
                {userinfo &&
                    <user_kind label="用户类型" {...formItemLayout} options={{ initialValue: "" }}>
                        <Select>
                            <Select.Option value="">全部</Select.Option>
                            {Object.keys(Constants.UserKind).filter(it => it > userinfo.UserKind).map(it => <Select.Option key={it} value={it.toString()}>{Constants.UserKind[it]}</Select.Option>)}
                        </Select>
                    </user_kind>
                }
                <backpct label="返点" {...formItemLayout}>
                    <Input />
                </backpct>
                <create_time label="时间" {...formItemLayout} format={(times) => Utils.dateRangeFormat(times)}>
                    {RenderHelper.rangeDatePick()}
                </create_time>
            </SearchForm>
        );
    }

    get columns() {
        const columns = [
            { title: '#', render: RenderHelper.render_index },
            {
                title: "用户",
                dataIndex: "Email",
                render(email, record, index) {
                    if (record.UserKind <= 3) {
                        const url = `/user/${record.UserId}`;
                        return (<Link to={url}>{email} </Link>);
                    } else
                        return email;
                }
            },
            { title: "备注", dataIndex: "Remark" },
            { title: "用户类型", dataIndex: "UserKind", render: RenderHelper.render_enum(Constants.UserKind) },
            { title: "余额", dataIndex: "Balance", className: 'column-money', render: RenderHelper.money_format_color },
            { title: "团队人数", dataIndex: "ChildCount", render(count) { return count + 1; } },
            { title: "团队余额", dataIndex: "ChildBalance", className: 'column-money', render: RenderHelper.money_format_color },
            { title: "返点", dataIndex: "Backpct", className: 'column-money', render: RenderHelper.render_percent() },
            { title: "日工资", dataIndex: "DailyWages", className: 'column-money', render: RenderHelper.render_percent() },
            { title: "手机号码", dataIndex: "Mobile" },
            { title: "QQ", dataIndex: "QQ" },
            { title: "状态", dataIndex: "Status", render: (status) => RenderHelper.render_status(Constants.UserStatus, status) },
            { title: "注册时间", dataIndex: "CreateTime", render: RenderHelper.render_time },
            {
                title: "操作", render: (user) => {
                    return (
                        <span>
                            <Button type="primary" icon="edit" size="small" onClick={() => this.onEditUserClick(user)}>编辑</Button>
                            &nbsp;&nbsp;
                        <Button type="action" icon="swap" size="small" onClick={() => this.transfer(user)}>转账</Button>
                        </span>
                    );
                }
            }
        ];
        return columns;
    }

}


class AddForm extends BasicComponent {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            captchUrl: `/captcha?_=${new Date().getTime()}`
        };
    }

    handleSubmit(e) {
        e.preventDefault();
        const { dispatch, parent } = this.props;
        this.props.form.validateFields((errors, values) => {
            if (errors) return;

            this.setState({ loading: true });
            values['aid'] = parent.UserId;
            values['userKind'] = parent.UserKind + 1;
            this.props.onSubmit && this.props.onSubmit(values).then(act => {
                this.setState({ loading: false });

                const resp = act.response;
                if (resp.code == 0) {
                    message.success("添加成功");
                } else {
                    message.error(resp.message);
                }

            });

        });
    }

    refreshCaptcha() {
        let captcha = this.props.form.getFieldInstance("captcha");
        captcha.refs.input.value = "";
        captcha.refs.input.focus();
        this.refs.captcha_img.src = Utils.addActionToUrl(this.refs.captcha_img.src, "_", new Date().getTime());
    }

    /* ---------------------------- Renders  ------------------------------*/
    render() {
        const { getFieldDecorator } = this.props.form;

        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 14 },
        };

        const tailFormItemLayout = {
            wrapperCol: {
                span: 14,
                offset: 6,
            },
        };

        const { parent } = this.props;
        const userKind = Constants.UserKind[parent.UserKind + 1];
        const backpct = parseFloat((parent.Backpct * 100).toFixed(2));
        const dailyWages = parseFloat((parent.DailyWages * 100).toFixed(2));

        return (
            <Form onSubmit={this.handleSubmit.bind(this)} autoComplete="off">

                <Form.Item label="邮箱" {...formItemLayout} hasFeedback>
                    {getFieldDecorator('email', {
                        validateTrigger: "onBlur",
                        rules: [{
                            required: true, message: `请输入${userKind}的邮箱。`
                        }, {
                            type: 'email',
                            message: '请输入合法信箱',
                        }, {
                            validator: Validators.checkEmailExisted.bind(this)
                        }],
                    })(
                        <Input />
                    )}
                </Form.Item>
                <Form.Item label="登陆密码" {...formItemLayout} hasFeedback>
                    {getFieldDecorator('password', {
                        rules: [{
                            required: true,
                            message: `请输入${userKind}登陆密码`
                        }, {
                            min: 6, max: 20,
                            message: "登陆密码长度必须在6与20之间"
                        }],
                    })(
                        <Input />
                    )}
                </Form.Item>
                <Form.Item label="返点" {...formItemLayout}>
                    {getFieldDecorator('backpct', {
                        initialValue: backpct,
                        rules: [{
                            validator: Validators.checkPrecision,
                            message: "小数点后最多只允许存在2位!"
                        }]
                    })(
                        <InputNumber min={0} max={backpct} step={Utils.getPrecision(backpct.toString())} />
                    )}
                    <span className="ant-form-text"> % (最大{backpct}%)</span>
                    <Tooltip placement="right" title={Constants.COMMISSION_EXPLAIN}>
                        <Icon type="question-circle" />
                    </Tooltip>
                </Form.Item>
                <Form.Item label="日工资" {...formItemLayout}>
                    {getFieldDecorator('dailywages', {
                        initialValue: dailyWages,
                        rules: [{
                            validator: Validators.checkPrecision,
                            message: "小数点后最多只允许存在2位!"
                        }]
                    })(
                        <InputNumber min={0} max={dailyWages} step={Utils.getPrecision(dailyWages.toString())} />
                    )}
                    <span className="ant-form-text"> % (最大{dailyWages}%)</span>
                    <Tooltip placement="right" title={Constants.DAILY_WAGES_EXPLAIN}>
                        <Icon type="question-circle" />
                    </Tooltip>
                </Form.Item>
                <Form.Item label="备注" {...formItemLayout} hasFeedback extra="该用户的备注信息，方便您自己识别用户。">
                    {getFieldDecorator('remark', {
                        rules: [{
                            validator: Validators.checkChineseLength(),
                            message: "只允许英文数字下划线(最多20个)与汉字(最多10个)"
                        }]
                    })(
                        <Input type="textarea" />
                    )}
                </Form.Item>
                <Form.Item label="手机号码" {...formItemLayout} hasFeedback>
                    {getFieldDecorator('mobile', {
                        rules: [{
                            validator: Validators.mobile,
                            message: "请输入正确的手机号码"
                        }]
                    })(
                        <Input />
                    )}
                </Form.Item>
                <Form.Item label="QQ" {...formItemLayout} hasFeedback>
                    {getFieldDecorator('qq', {
                        rules: [{
                            validator: Validators.checkQQ,
                            message: "请输入正确的QQ号码"
                        }]
                    })(
                        <Input />
                    )}
                </Form.Item>
                <Form.Item label="验证码" {...formItemLayout} hasFeedback>
                    <Row gutter={8}>
                        <Col span={12}>
                            {getFieldDecorator('captcha', {
                                rules: [{ required: true, message: "请输入验证码" }],
                            })(
                                <Input type="text" className="input-captcha" maxLength="4" />
                            )}
                        </Col>
                        <Col span={12}>
                            <img src={this.state.captchUrl} ref="captcha_img" className="img-captcha" onClick={this.refreshCaptcha.bind(this)} />
                        </Col>
                    </Row>
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                    <Button loading={this.state.loading} className="fn-width100" type="primary" htmlType="submit" size="large">确定</Button>
                </Form.Item>
            </Form>
        );
    }
}

AddForm.propTypes = {
    /**
     * 要添加下级的用户
     * @type {[type]}
     */
    parent: React.PropTypes.object.isRequired,
    /**
     * 确定提交事件
     * @param {dict} values 表单内容
     * @type {[type]}
     */
    OnSubmit: React.PropTypes.func
};
const AddFormWraper = Form.create()(AddForm);

class EditForm extends BasicComponent {

    constructor(props) {
        super(props);
        this.state = {
            loading: false
        };
    }

    handleSubmit(e) {
        e.preventDefault();

        const { dispatch, parent, user } = this.props;

        this.props.form.validateFields((errors, values) => {
            if (errors) return;

            const fnUpdate = () => {
                this.setState({
                    loading: true
                });
                this.props.onSubmit && this.props.onSubmit(values).then(act => {

                    this.setState({
                        loading: false
                    });

                    const resp = act.response;
                    if (resp.code == 0) {
                        message.success("修改成功");
                    } else {
                        message.error(resp.message);
                    }

                });
            };

            const newBackpct = parseFloat(values.backpct) / 100;
            if (newBackpct < user.Backpct) {
                RenderHelper.confirm("新返点小于旧返点", "这将会影响到下级用户的返点，您确定修改吗?", fnUpdate);
                return;
            }

            const newDailyWages = parseFloat(values.dailywages) / 100;
            if (newDailyWages < user.DailyWages) {
                RenderHelper.confirm("新日工资小于旧日工资", "这将会影响到下级用户的日工资，您确定修改吗?", fnUpdate);
                return;
            }

            fnUpdate();
        });
    }

    /* ---------------------------- Renders  ------------------------------*/

    render() {
        const { getFieldDecorator } = this.props.form;

        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 14 },
        };

        const tailFormItemLayout = {
            wrapperCol: {
                span: 14,
                offset: 6,
            },
        };

        const { parent, user } = this.props;
        const backpct = parseFloat((parent.Backpct * 100).toFixed(2));
        const dailyWages = parseFloat((parent.DailyWages * 100).toFixed(2));

        return (
            <Form onSubmit={this.handleSubmit.bind(this)} autoComplete="off">
                <Form.Item label="登陆密码" {...formItemLayout} hasFeedback extra="非修改登陆密码无需填写">
                    {getFieldDecorator('password', {
                        rules: [{
                            min: 6, max: 20,
                            message: "登陆密码长度必须在6与20之间"
                        }],
                    })(
                        <Input />
                    )}
                </Form.Item>
                <Form.Item label="返点" {...formItemLayout}>
                    {getFieldDecorator('backpct', {
                        initialValue: Utils.percent(user.Backpct),
                        rules: [{
                            validator: Validators.checkPrecision,
                            message: "小数点后最多只允许存在2位!"
                        }]
                    })(
                        <InputNumber min={0} max={backpct} step={Utils.getPrecision(Utils.percent(user.Backpct).toString())} />
                    )}
                    <span className="ant-form-text"> % (最大{backpct}%)</span>
                    <Tooltip placement="right" title={Constants.COMMISSION_EXPLAIN}>
                        <Icon type="question-circle" />
                    </Tooltip>
                </Form.Item>
                <Form.Item label="日工资" {...formItemLayout}>
                    {getFieldDecorator('dailywages', {
                        initialValue: Utils.percent(user.DailyWages),
                        rules: [{
                            validator: Validators.checkPrecision,
                            message: "小数点后最多只允许存在2位!"
                        }]
                    })(
                        <InputNumber min={0} max={dailyWages} step={Utils.getPrecision(Utils.percent(user.DailyWages).toString())} />
                    )}
                    <span className="ant-form-text"> % (最大{dailyWages}%)</span>
                    <Tooltip placement="right" title={Constants.DAILY_WAGES_EXPLAIN}>
                        <Icon type="question-circle" />
                    </Tooltip>
                </Form.Item>
                <Form.Item label="备注" {...formItemLayout} hasFeedback extra="该用户的备注信息，方便您自己识别用户。">
                    {getFieldDecorator('remark', {
                        initialValue: user.Remark,
                        rules: [{
                            validator: Validators.checkChineseLength(),
                            message: "只允许英文数字下划线(最多20个)与汉字(最多10个)"
                        }]
                    })(
                        <Input type="textarea" />
                    )}
                </Form.Item>
                <Form.Item label="手机号码" {...formItemLayout} hasFeedback>
                    {getFieldDecorator('mobile', {
                        initialValue: user.Mobile,
                        rules: [{
                            validator: Validators.mobile,
                            message: "请输入正确的手机号码"
                        }]
                    })(
                        <Input />
                    )}
                </Form.Item>
                <Form.Item label="QQ" {...formItemLayout} hasFeedback>
                    {getFieldDecorator('qq', {
                        initialValue: user.QQ,
                        rules: [{
                            validator: Validators.checkQQ,
                            message: "请输入正确的QQ号码"
                        }]
                    })(
                        <Input />
                    )}
                </Form.Item>
                <Form.Item label="修改说明" {...formItemLayout} hasFeedback>
                    {getFieldDecorator('desc', {
                        rules: [{
                            required: true,
                            message: "请填写修改说明"
                        }, {
                            validator: Validators.checkChineseLength(64),
                            message: "只允许英文数字下划线(最多40个)与汉字(最多20个)"
                        }]
                    })(
                        <Input type="textarea" placeholder="您本次修改的原因" />
                    )}
                </Form.Item>
                <Form.Item label="状态" {...formItemLayout}>
                    {getFieldDecorator('status', {
                        initialValue: user.Status == 0,
                        valuePropName: "checked"
                    })(
                        <Switch checkedChildren={'正常'} unCheckedChildren={'禁用'} />
                    )}
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                    <Button loading={this.state.loading} className="fn-width100" type="primary" htmlType="submit" size="large">确定</Button>
                </Form.Item>
            </Form>
        );
    }
}
EditForm.propTypes = {
    /**
     * 要修改的用户
     * @type {[type]}
     */
    user: React.PropTypes.object.isRequired,
    parent: React.PropTypes.object.isRequired,
    /**
     * 确定提交事件
     * @param {dict} values 表单内容
     * @type {[type]}
     */
    OnSubmit: React.PropTypes.func
};
const EditFormWraper = Form.create()(EditForm);


module.exports = connect(state => ({
    balance: state.bill.balance,
    userinfo: state.passport.userinfo,
    team: state.passport.team,
    parents: state.passport.parents
}))(UserList);
