import { ActionState } from "../app_constants";
import ReduceBase from "./reduce_base";
import * as Enums from "../app_enum";

const defaultState = {
    bulltins: {
		isFetching: false,
		// 一般公告
		info: [],
		// 警告公告
		warning: [],
    }
}

class NoticeReducer extends ReduceBase {
	static reduce(state=defaultState, action) {
		switch (action.type) {
			case "Bulletins":
				return NoticeReducer._reduce_bulltins(state, action);
			default:
                return state;
		}
	}

    static _reduce_bulltins(state, action) {

		let isFetching = action.state == ActionState.Doing;
		const resp = action.response;

		if (resp && resp.code == 0) {

			let infoBulltins = state.bulltins.info;
			let warningBulltins = state.bulltins.warning;
			const newBulltins = resp.data;
			
			newBulltins.forEach(bt => {
				if(bt.Type == Enums.BulletinType.Info)
					infoBulltins = NoticeReducer._reduce_info_bulltin(infoBulltins, bt);
				else if(bt.Type ==Enums.BulletinType.Warning)
					warningBulltins = NoticeReducer._reduce_warning_bulltin(warningBulltins, bt);
			});

			const bulltins = Object.assign({}, state.bulltins, {
				isFetching,
				info: infoBulltins,
				warning: warningBulltins
			});

            return Object.assign({}, state, {bulltins});
        }

		return Object.assign({}, state, 
			{bulltins: Object.assign({}, state.bulltins,{isFetching})}
		);
	}

    static _reduce_info_bulltin(infos, bulltin) {
    	const index = infos.findIndex(it => it.Id == bulltin.Id);
		if(index >= 0)
			// 删除公告
			if(bulltin.Status == Enums.CommonStatus.Disabled)
				infos = [...infos.slice(0, index), ...infos.slice(index + 1)];
			// 修改公告
			else
				infos=[...infos.slice(0, index), bulltin, ...infos.slice(index + 1)];
		else
			infos=[...infos, bulltin];
		return infos;
    }

    static _reduce_warning_bulltin(warnings, bulltin) {
    	const index = warnings.findIndex(it => it.Id == bulltin.Id);
		if(index >= 0)
			// 删除公告
			if(bulltin.Status == Enums.CommonStatus.Disabled)
				warnings = [...warnings.slice(0, index), ...warnings.slice(index + 1)];
			// 修改公告
			else
				warnings=[...warnings.slice(0, index), bulltin, ...warnings.slice(index + 1)];
		else
			warnings=[...warnings, bulltin];
		return warnings;
    }
}

module.exports = NoticeReducer;
