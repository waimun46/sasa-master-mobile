import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { message, Table, Breadcrumb, Button, Form, Select, Input, Icon } from 'antd';

import BasicComponent from "../common/BasicComponent";
import DashContainer from '../../containers/DashContainer';
import SearchForm from "../common/SearchForm";

import * as PassportAction from '../../actions/passport';
import * as ReportAction from "../../actions/report";
import * as GameAction from "../../actions/game";
import * as Constants from "../../app_constants";
import * as Utils from "../../utils/common";
import * as RenderHelper from "../../utils/render_helper";

/**
 * 用于标示Profit中显示什么数据
 * @type {Object}
 */
const DataFlags = {
    Group: 0,
    GameType: 1,
    User: 2
};

class ProfitList extends BasicComponent {

    constructor(props) {
        super(props);
        this.userId = this.getUserId(props);
        this.state = {
            flags: DataFlags.Group,
            gameType: null,
            querys: {}
        }
    }

    /* ------------------ Sys -------------------- */
    componentWillMount() {
        const { groups } = this.props;
        if (!groups || groups.length == 0)
            this.props.dispatch(GameAction.getGameGroup());

        this.initGroup(groups);
    }

    componentWillUpdate(nextProps, nextState) {
        if (nextProps.groups != this.props.groups) {
            this.initGroup(nextProps.groups);
        }
    }

    componentWillReceiveProps(newProps) {
        if (newProps != this.props) {
            const userId = this.getUserId(newProps);
            if (userId != this.userId && !isNaN(userId)) {
                this.userId = userId;
            }
        }
    }

    /* ------------------ Functions -------------------- */

    getUserId(props) {
        let userId = props.params.userId;
        if (userId)
            userId = parseInt(userId);
        return userId;
    }

    initGroup(groups) {
        const group = {};
        for (let i in groups) {
            group[groups[i].Id] = groups[i].Name;
        }
        this.group = group;
    }

    /* ------------------ Events -------------------- */

    onSearchClick(values) {
        this.setState({ querys: values });
    }

    onResetClick() {
        this.querys = {};
    }

    onGroupClick(groupId, fromDate, toDate) {
        this.setState({
            flags: DataFlags.GameType,
            groupId: groupId,
            fromDate,
            toDate
        })
    }

    onGameTypeClick(gameType, fromDate, toDate) {
        this.setState({
            flags: DataFlags.User,
            gameType
        });
    }

    onGameTypeGoBack() {
        this.setState({
            flags: DataFlags.Group,
            groupId: null
        });
    }

    onUserTypeGoBack() {
        this.setState({
            flags: DataFlags.GameType,
            gameType: null
        });
    }

    /* ------------------ Render ------------------ */
    render() {
        const { flags, gameType, querys } = this.state;
        return (
            <DashContainer title="游戏盈亏">
                {this.renderSearch()}
                <div className="search-result-list">
                    {flags == DataFlags.Group &&
                        <ProfitGroupGameType {...this.state}
                            onGroupClick={this.onGroupClick.bind(this)} />
                    }
                    {flags == DataFlags.GameType &&
                        <ProfitGameType userId={this.userId}
                            {...this.state}
                            onGameTypeClick={this.onGameTypeClick.bind(this)}
                            onGoBackClick={this.onGameTypeGoBack.bind(this)} />
                    }
                    {flags == DataFlags.User &&
                        <ProfitUserGameType userId={this.userId}
                            {...this.state}
                            onGoBackClick={this.onUserTypeGoBack.bind(this)} />
                    }
                </div>
            </DashContainer>
        );
    }

    renderSearch() {
        const { groups } = this.props;

        const formItemLayout = {
            labelCol: { span: 8 },
            wrapperCol: { span: 16 }
        };

        return (
            <SearchForm onSearch={this.onSearchClick.bind(this)} onReset={this.onResetClick.bind(this)}>
                <group_id label="彩种" {...formItemLayout} options={{ initialValue: "" }}>
                    <Select>
                        <Option value="">全部</Option>
                        {groups.map(group => {
                            return <Option value={group.Id.toString()} key={group.Id}>{group.Name}</Option>
                        })}
                    </Select>
                </group_id>
                <create_time label="日期" {...formItemLayout} format={(times) => Utils.dateRangeFormat(times)}>
                    {RenderHelper.rangeDatePick()}
                </create_time>
            </SearchForm>
        );
    }

}

class ProfitGroupGameType extends BasicComponent {

    constructor(props) {
        super(props);
    }

    /* ------------------ Sys -------------------- */
    componentWillMount() {
        this.queryData(this.props);
    }

    componentWillReceiveProps(props) {
        if (props != this.props && props.querys != this.props.querys)
            this.queryData(props);
    }

    /* ------------------ Functions -------------------- */

    queryData(props) {
        const { querys } = props;
        this.props.dispatch(ReportAction.queryProfitGameTypeGroup(querys));
    }

    /* ------------------ Events -------------------- */


    /* ------------------ Renders -------------------- */

    render() {
        const { profitGroupGameType } = this.props;
        let data = profitGroupGameType.data || [];

        return (
            <Table loading={profitGroupGameType.isFetching} columns={this.columns} dataSource={data} pagination={false} rowKey="GroupId" />
        );
    }

    get columns() {
        const self = this;
        const columns = [
            { title: '#', render: RenderHelper.render_index },
            {
                title: "日期", render: it => {
                    if (it.ToDate != it.Date)
                        return `${it.Date} - ${it.ToDate}`;
                    return it.Date;
                }
            },
            { title: "彩种", dataIndex: "GroupName", render: (it, profit) => <a onClick={() => this.props.onGroupClick(it.GroupId, it.Date, it.ToDate)}>{it}</a> },
            { title: "用户数", dataIndex: "UserCount" },
            {
                title: "订单数", dataIndex: "OrderCount", className: "column-money", render: (it, record) => {
                    return <a target="_blank" href={`/deals?group_id=${record.GroupId}&from_time=${record.Date}&to_time=${record.ToDate}235959`}>{it}</a>
                }
            },
            { title: "总投注", dataIndex: "TotalBet", className: "column-money", render: RenderHelper.money_format },
            { title: "有效投注", dataIndex: "ValidBet", className: "column-money", render: RenderHelper.money_format },
            { title: "盈亏", dataIndex: "Profit", className: "column-money", render: RenderHelper.money_format_color },
        ];

        return columns;
    }
}
ProfitGroupGameType.propTypes = {
    onGroupClick: React.PropTypes.func.isRequired
}
ProfitGroupGameType = connect(state => ({
    userinfo: state.passport.userinfo,
    profitGroupGameType: state.report.profitGroupGameType,
}))(ProfitGroupGameType);


class ProfitGameType extends BasicComponent {

    constructor(props) {
        super(props);
        this.pageIndex = 1;
    }

    /* ------------------ Sys -------------------- */
    componentWillMount() {
        this.queryData(this.props);
    }

    componentWillReceiveProps(props) {
        if (props != this.props && props.querys != this.props.querys)
            this.queryData(props);
    }

    /* ------------------ Functions -------------------- */

    queryData(props) {
        const { userId, querys } = props;
        this.props.dispatch(ReportAction.queryProfitGameType(userId, Object.assign({}, querys, { pageIndex: this.pageIndex })));
    }

    /* ------------------ Events -------------------- */


    /* ------------------ Renders -------------------- */

    render() {
        const self = this;
        const { profitGameType } = this.props;
        let data = profitGameType.data || [];

        const pagination = {
            total: profitGameType.count || 0,
            onChange(current) {
                self.pageIndex = current;
                self.queryData(self.props);
            },
        };

        return (
            <Table loading={profitGameType.isFetching} columns={this.columns} dataSource={data} pagination={pagination}
                title={() => <Button icon="rollback" type="primary" onClick={() => this.props.onGoBackClick()}>返回</Button>} rowKey="rn" />
        );
    }

    get columns() {
        const self = this;
        const columns = [
            { title: '#', render: RenderHelper.render_index },
            {
                title: "日期", render: it => {
                    if (it.ToDate != it.Date)
                        return `${it.Date} - ${it.ToDate}`;
                    return it.Date;
                }
            },
            { title: "彩种", dataIndex: "GroupName" },
            {
                title: "玩法", dataIndex: "GameName", render: (it, record) => {
                    return <a onClick={() => this.props.onGameTypeClick(record.GameType, record.Date, record.ToDate)}>{it}</a>
                }
            },
            { title: "用户数", dataIndex: "UserCount" },
            {
                title: "订单数", dataIndex: "OrderCount", className: "column-money", render: (it, record) => {
                    return <a target="_blank" href={`/deals?game_type=${record.GameType}&from_time=${record.Date}&to_time=${record.ToDate}235959`}>{it}</a>
                }
            },
            { title: "总投注", dataIndex: "TotalBet", className: "column-money", render: RenderHelper.money_format },
            { title: "有效投注", dataIndex: "ValidBet", className: "column-money", render: RenderHelper.money_format },
            { title: "盈亏", dataIndex: "Profit", className: "column-money", render: RenderHelper.money_format_color },
        ];

        return columns;
    }
}
ProfitGameType.PropTypes = {
    onGameTypeClick: React.PropTypes.func.isRequired,
    onGoBackClick: React.PropTypes.func.isRequired
}
ProfitGameType = connect(state => ({
    userinfo: state.passport.userinfo,
    profitGameType: state.report.profitGameType,
}))(ProfitGameType);


class ProfitUserGameType extends BasicComponent {
    constructor(props) {
        super(props);
        this.pageIndex = 1;
    }

    /* ------------------ Sys -------------------- */
    componentWillMount() {
        this.queryData(this.props);
    }

    componentWillReceiveProps(props) {
        if (props != this.props && props.querys != this.props.querys)
            this.queryData(props);
    }

    /* ------------------ Functions -------------------- */

    queryData(props) {
        let { userId, gameType, querys } = props;
        querys = Object.assign({}, querys, { game_type: gameType, pageIndex: this.pageIndex });
        this.props.dispatch(ReportAction.queryProfitGameTypeByUser(userId, querys));
    }

    /* ------------------ Events -------------------- */



    /* ------------------ Renders -------------------- */

    render() {
        const self = this;
        const { profitUserGameType } = this.props;
        let data = profitUserGameType.data || [];

        const pagination = {
            total: profitUserGameType.count || 0,
            onChange(current) {
                self.queryData(self.props);
            },
        };

        return (
            <div>
                <Table loading={profitUserGameType.isFetching}
                    columns={this.columns} dataSource={data} pagination={pagination}
                    title={() => <Button icon="rollback" type="primary" onClick={() => this.props.onGoBackClick()}>返回</Button>} />
            </div>
        );
    }

    get columns() {
        const self = this;
        const columns = [
            { title: '#', render: RenderHelper.render_index },
            {
                title: "日期", render: it => {
                    if (it.ToDate != it.Date)
                        return `${it.Date} - ${it.ToDate}`;
                    return it.Date;
                }
            },
            { title: "Email", dataIndex: "Email" },
            { title: "彩种", dataIndex: "GroupName" },
            { title: "玩法", dataIndex: "GameName" },
            {
                title: "订单数", dataIndex: "OrderCount", className: "column-money", render: (it, record) => {
                    return <a target="_blank" href={`/deals?email=${record.Email}&game_type=${record.GameType}&from_time=${record.Date}&to_time=${record.ToDate}235959`}>{it}</a>
                }
            },
            { title: "总投注", dataIndex: "TotalBet", className: "column-money", render: RenderHelper.money_format },
            { title: "有效投注", dataIndex: "ValidBet", className: "column-money", render: RenderHelper.money_format },
            { title: "盈亏", dataIndex: "Profit", className: "column-money", render: RenderHelper.money_format_color },
        ];

        return columns;
    }
}
ProfitUserGameType.propTypes = {
    onGoBackClick: React.PropTypes.func.isRequired
}
ProfitUserGameType = connect(state => ({
    userinfo: state.passport.userinfo,
    profitUserGameType: state.report.profitUserGameType,
}))(ProfitUserGameType);


module.exports = connect(state => ({
    groups: state.game.groups
}))(ProfitList);
