import React from 'react';
import { Icon } from 'antd';

class Layout extends React.Component {
    constructor(props){
        super(props);
    }
    render() {
        return (
            <div className="passport-body">
                <div className="passport-wrapper">
                    <div className="passport-header"></div>
                    <div className="passport-main">
                        <div className="passport-main-left">
                            <h1>线上游戏全新平台</h1>
                            <h2>新品牌 新体验</h2>
                        </div>
                        <div className="passport-main-right">
                            {this.props.children}
                            <br/>
                            <div className="cs-box">
                                <Icon type="question" />
                                &nbsp;&nbsp;如有问题请咨询 <a>在线客服</a>
                            </div>
                        </div>
                    </div>
                    <div className="passport-footer fn-left"></div>
                </div>
            </div>
        );
    }
}

module.exports = Layout;
