import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { message, Table, Breadcrumb, Button, Form, Select, Input, Icon } from 'antd';

import BasicComponent from "../common/BasicComponent";
import DashContainer from '../../containers/DashContainer';
import SearchForm from "../common/SearchForm";

import * as PassportAction from '../../actions/passport';
import * as ReportAction from "../../actions/report";
import * as Constants from "../../app_constants";
import * as Utils from "../../utils/common";
import * as RenderHelper from "../../utils/render_helper";
import * as Enums from "../../app_enum";


class ProfitUser extends BasicComponent {

    constructor(props) {
        super(props);
        this.userId = this.getUserId(props);
        this.querys = {}
    }

    /* ------------------ Sys -------------------- */
    componentWillMount() {
        this.queryData();
    }

    componentWillReceiveProps(newProps) {
        if (newProps != this.props) {
            const userId = this.getUserId(newProps);
            if (userId != this.userId && !isNaN(userId)) {
                this.userId = userId;
                this.queryData();
            }
        }

    }
    /* ------------------ Functions -------------------- */

    queryData() {
        let parents = this.props.parents.data;
        if (this.userId || parents != null || parent.length > 0)
            this.props.dispatch(PassportAction.getParents(this.userId));

        this.props.dispatch(ReportAction.queryProfitUser(this.userId, this.querys));
    }

    getUserId(props) {
        let userId = props.params.userId;
        if (userId)
            userId = parseInt(userId);
        return userId;
    }

    isMemberuser() {
        const userinfo = this.props.userinfo.data;

        return userinfo && userinfo.UserKind == Enums.UserKind.Member;
    }

    /* ------------------ Events -------------------- */

    onSearchClick(values) {
        this.querys = values;
        this.queryData();
    }

    onResetClick() {
        this.querys = {};
    }

    /* ------------------ Renders -------------------- */

    render() {
        const self = this;
        const { profitUser } = this.props;
        let data = profitUser.data || [];

        const pagination = {
            total: profitUser.count || 0,
            onChange(current) {
                self.querys = Object.assign({}, self.querys, { pageIndex: current });
                self.queryData();
            },
        };

        return (
            <DashContainer title="用户盈亏">
                {this.renderSearch()}
                {this.renderBreadcrumb()}
                <div className="search-result-list">
                    <Table loading={profitUser.isFetching} columns={this.columns} dataSource={data} rowKey="UserId" pagination={pagination} />
                </div>
            </DashContainer>
        );
    }

    renderBreadcrumb() {
        let parents = this.props.parents.data;
        if (parents == null || parents.length == 0)
            if (this.props.userinfo.data)
                parents = [this.props.userinfo.data];

        if (parents == null)
            parents = []
        return (
            <Breadcrumb separator=">">
                {parents.map((it, i) => <Breadcrumb.Item key={i}><Link to={`/profit/user/${it.UserId}`}>[{Constants.UserKind[it.UserKind]}]{it.Email}</Link></Breadcrumb.Item>)}
            </Breadcrumb>
        );
    }

    renderSearch() {
        const formItemLayout = {
            labelCol: { span: 8 },
            wrapperCol: { span: 16 }
        };

        return (
            <SearchForm onSearch={this.onSearchClick.bind(this)} onReset={this.onResetClick.bind(this)}>
                <email label="邮箱" {...formItemLayout}>
                    <Input />
                </email>
                <create_time label="时间" {...formItemLayout} format={(times) => Utils.dateRangeFormat(times)}>
                    {RenderHelper.rangeDatePick()}
                </create_time>
            </SearchForm>
        );
    }

    get columns() {
        const self = this;
        let isMember = this.isMemberuser();
        let columns = [
            { title: '#', render: RenderHelper.render_index },
            {
                title: '邮箱', dataIndex: 'Email', render: (email, profit) => {
                    if (profit.IsSum == 0)
                        return email;
                    else
                        return <Link to={`/profit/user/${profit.UserId}`}>{email}</Link>
                }
            },
            {
                title: "日期", render: it => {
                    if (it.ToDate != it.Date)
                        return `${it.Date} - ${it.ToDate}`;
                    return it.Date;
                }
            },
            { title: "订单数", dataIndex: "OrderCount", className: "column-money" },
            { title: "总投注", dataIndex: "TotalBet", className: "column-money", render: RenderHelper.money_format },
            {
                title: "有效投注", dataIndex: "ValidBet", className: "column-money", render: (it, profit) => {
                    return <a target="_blank" href={`/deals?email=${profit.Email}&load_all=1&from_time=${profit.Date}&to_time=${profit.ToDate}235959`}>{RenderHelper.money_format(it)}</a>
                }
            },
            { title: "奖金", dataIndex: "Bonus", className: isMember ? "column-hidden" : "column-money", render: RenderHelper.money_format },
            { title: "佣金", dataIndex: "Commission", className: isMember ? "column-hidden" : "column-money", render: RenderHelper.money_format },
            { title: "盈亏", dataIndex: "Profit", className: "column-money", render: RenderHelper.money_format_color },
            { title: "充值", dataIndex: "Deposit", className: "column-money", render: RenderHelper.money_format },
            { title: "提现", dataIndex: "Withdraw", className: "column-money", render: RenderHelper.money_format },
            { title: "充值数", dataIndex: "DepositCount", className: "column-money" },
            { title: "提现数", dataIndex: "WithdrawCount", className: "column-money" }
        ];
        return columns;
    }
}

module.exports = connect(state => ({
    userinfo: state.passport.userinfo,
    profitUser: state.report.profitUser,
    parents: state.passport.parents
}))(ProfitUser);
