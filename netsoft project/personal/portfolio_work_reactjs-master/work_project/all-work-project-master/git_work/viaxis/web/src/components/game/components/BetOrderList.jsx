import React from 'react';
import { connect } from 'react-redux';
import { Table, Icon, Popconfirm } from "antd";

import * as Constants from "../../../app_constants";
import * as BetHelper from "../../../utils/bet_helper";
import * as Utils from "../../../utils/common";
import * as Render from "../../../utils/render_helper";

class BetOrderList extends React.Component {
    constructor(props) {
        super(props);
    }

    /* ---------------------------- 系统函数 ------------------------------*/
    shouldComponentUpdate(nextProps, nextState) {
        return nextProps != this.props || nextState != this.state;
    }

    /* ---------------------------- Renders ------------------------------*/

    tableColumns(){
        var { user } = this.props;
        const self = this;
        const columns = [{title: '#',render(text, record, index){return index+1;}}, 
            {title: '玩法', render(item){ 
                var sb = [item.gameType.Sort];
                if (item.gameType.Sort != item.gameType.Kind)
                    sb.push(item.gameType.Kind);
                if (item.gameType.Name != item.gameType.Sort && item.gameType.Name != item.gameType.Kind)
                    sb.push(item.gameType.Name);
                return sb.join("-");
            }}, 
            {title: "号码", width: 200, render(item){
                var betNum = item.betNum;
                if(Utils.isString(item.betNum)){
                    var nums_split_symbols = item.gameType.MaxNum > 9 ? Constants.TWO_DIGITS_NUMS_SPLIT_CHARS : Constants.NUMS_SPLIT_CHARS;
                    var num_split_symbols = item.gameType.MaxNum > 9 ? Constants.NUM_SPLIT_CHAR : null;
                    betNum = BetHelper.removeInvalidBetNum(item.betNum, item.gameType.BetNumCount, nums_split_symbols, num_split_symbols);
                }
                return BetHelper.convertBetNumToString(item.gameType, betNum, item.unitPos);
            }},
            {title: "金额", dataIndex:"wager", render: Render.render_milli_format},
            {title: "注数", dataIndex:"betCount"},
            {title: "总金额", render(item){return Utils.milliFormat(item.wager*item.betCount);}},
            {title: "赔率", render(item){
                var odds = user.data ? (item.gameType.Scale + item.gameType.ScaleBase * user.data.Backpct) : null;
                if (odds && !Number.isInteger(odds))
                    odds = parseFloat(odds.toFixed(2));
                return `${odds*2}/${odds}`;
            }},
            {title: <Popconfirm title="您确定要清空所有记录吗？" onConfirm={()=>self.props.onClear&&self.props.onClear()}><a><Icon type="delete" /></a></Popconfirm>, render(text, record, index){
                return (
                    <Popconfirm title="您确定要删除吗？" onConfirm={()=>self.props.onRemove&&self.props.onRemove(index)}>
                        <a><Icon type="close" /></a>
                    </Popconfirm>
                );
            }},
        ];

        return columns;
    }

    render() {
        let columns = this.tableColumns();
        let { data } = this.props;

        return (
            <div className="game-orders">
                <Table columns={columns} dataSource={data} size="middle" pagination={false}  />
            </div>
        );
    }
}

BetOrderList.propTypes = {
    // 投注内容
    data: React.PropTypes.array.isRequired,
    /**
     * 清空所有投注记录
     * @type {[type]}
     */
    onClear: React.PropTypes.func,
    /**
     * 删除指定下标记录
     * @param {int} index
     */
    onRemove: React.PropTypes.func
}

module.exports = connect(state => ({
    user: state.passport.userinfo
}))(BetOrderList);
