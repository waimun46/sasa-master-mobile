import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router'
import { notification, Table, Badge, Button, Select, Input, DatePicker, Icon } from 'antd';
import moment from 'moment';

import BasicComponent from "../common/BasicComponent";
import DashContainer from '../../containers/DashContainer';
import SearchForm from "../common/SearchForm";

import * as BillAction from "../../actions/bill";
import * as Constants from "../../app_constants";
import * as Utils from "../../utils/common";
import * as RenderHelper from "../../utils/render_helper";


class WithdrawList extends BasicComponent {
    constructor(props) {
        super(props);
        this.querys = {}
    }

    /* ---------------------------- System  ------------------------------*/

    componentWillMount() {
        this.queryWithdraw();
    }

    /* ---------------------------- Functions  ------------------------------*/

    queryWithdraw() {
        this.props.dispatch(BillAction.queryWithdraw(this.querys));
    }

    /* ---------------------------- Events  ------------------------------*/

    onSearchClick(values) {

        this.querys = values;
        this.queryWithdraw();
    }

    onResetClick() {
        this.querys = {};
    }


    /* ---------------------------- Renders  ------------------------------*/

    render() {

        const self = this;
        const { withdraw } = this.props;
        let data = withdraw.data || [];

        const pagination = {
            total: withdraw.count || 0,
            onChange(current) {
                self.querys.pageIndex = current;
                self.queryWithdraw();
            },
        };

        return (
            <DashContainer title="提现历史记录">
                {this.renderSearch()}
                <div className="search-result-list">
                    <Table loading={withdraw.isFetching} columns={this.columns} dataSource={data} rowKey="Id" pagination={pagination} />
                </div>
            </DashContainer>
        );
    }

    renderSearch() {
        const formItemLayout = {
            labelCol: { span: 8 },
            wrapperCol: { span: 16 }
        };

        return (
            <SearchForm onSearch={this.onSearchClick.bind(this)} onReset={this.onResetClick.bind(this)}>
                <create_time label="申请时间" {...formItemLayout} format={(times) => Utils.dateRangeFormat(times)}>
                    <DatePicker.RangePicker
                        ranges={{ '今天': [moment(), moment()], '本月': [moment(), moment().endOf('month')] }} />
                </create_time>
                <txid label="流水号" {...formItemLayout}>
                    <Input />
                </txid>
                <status label="状态" {...formItemLayout} options={{ initialValue: "" }}>
                    <Select>
                        <Select.Option value="">全部</Select.Option>
                        {Object.keys(Constants.WithdrawStatus).map(it =>
                            <Select.Option key={it} value={it.toString()}>
                                {Constants.WithdrawStatus[it].toString()}
                            </Select.Option>
                        )}
                    </Select>
                </status>
            </SearchForm>
        );
    }

    get columns() {
        const self = this;
        const columns = [
            { title: '#', render: RenderHelper.render_index },
            {
                title: "银行", render(record) {
                    return (
                        <span>
                            <i className={`iconfont icon-${record.BankCode}`}></i>&nbsp;
                        {record.BankName}
                        </span>
                    );
                }
            },
            { title: "卡号", dataIndex: "AccountNo", render: (num) => Utils.maskStart(num, 4).slice(num.length - 6) },
            { title: "户名", dataIndex: "AccountName", render: (name) => Utils.maskStart(name, name.length - 1) },
            { title: "金额", dataIndex: "Amount", className: 'column-money', render: RenderHelper.money_format },
            { title: "余额", dataIndex: "Balance", className: 'column-money', render: RenderHelper.money_format },
            { title: "流水号", dataIndex: "Txid" },
            { title: "状态", dataIndex: "Status", render: (status) => RenderHelper.render_status(Constants.WithdrawStatus, status) },
            { title: "备注", dataIndex: "Remark" },
            { title: "申请时间", dataIndex: "CreateTime", render: RenderHelper.render_time },
        ];
        return columns;
    }
}

module.exports = connect(state => ({
    withdraw: state.bill.withdraw
}))(WithdrawList);
