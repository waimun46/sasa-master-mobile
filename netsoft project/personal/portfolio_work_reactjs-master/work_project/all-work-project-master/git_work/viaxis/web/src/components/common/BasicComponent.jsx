import React from 'react';
import { browserHistory } from 'react-router'
import { message, Modal } from 'antd';

import Auth from "../../auth";
import * as Constants from "../../app_constants";

/**
 * 组件基类
 * 在这里实现一些通用的方法
 */
class BasicComponent extends React.Component{
    
    constructor(props) {
        super(props);
    }

    /* ---------------------------- 系统函数 ------------------------------*/
    /**
     * executed before rendering
     * @return {[type]} [description]
     */
    componentWillMount() {
    }

    /**
     * executed after first render only on the client side
     * @return {[type]} [description]
     */
    componentDidMount() {
    }

    /**
     * invoked as soon as the props are updated before another render is called.
     * @param  {[type]} newProps [description]
     * @return {[type]}          [description]
     */
    componentWillReceiveProps(newProps) {
    }

    /**
     * should return true or false value. This will determine if component will be updated or not. 
     * This is set to true by default. If you are sure that component doesn't need to render after state or props are updated, 
     * you can return false value.
     * @param  {[type]} nextProps [description]
     * @param  {[type]} nextState [description]
     * @return {[type]}           [description]
     */
    shouldComponentUpdate(nextProps, nextState) {
        return nextProps != this.props || nextState != this.state;
    }

    /**
     * called just before rendering.
     * @param  {[type]} nextProps [description]
     * @param  {[type]} nextState [description]
     * @return {[type]}           [description]
     */
    componentWillUpdate(nextProps, nextState) {
    }

    /**
     * called just after rendering.
     * @param  {[type]} prevProps [description]
     * @param  {[type]} prevState [description]
     * @return {[type]}           [description]
     */
    componentDidUpdate(prevProps, prevState) {
    }

    /**
     * called after the component is unmounted from the dom
     * @return {[type]} [description]
     */
    componentWillUnmount() {
    }

    /* ---------------------------- 自定义函数 ------------------------------*/

    /**
     * 获取父级组件
     * @return {[type]} [description]
     */
    get parent(){
        return this._reactInternalInstance._currentElement._owner._instance;
    }

    get isLogin() {
        return Auth.isLogged();
    }

    get realname() {
        return Auth.get('realname');
    }

    logout(){
        Modal.confirm({title:"您确定要退出吗?",onOk:Auth.logout});
    }

    /**
     * 跳转到指定地址(Route跳转，不刷新)
     * @param  {[type]} url [description]
     * @return {[type]}     [description]
     */
    goto(url) {
        browserHistory.push(url);
    }

    /**
     * 跳转到指定页面(强制跳转，刷新)
     * @param  {[type]} url [description]
     * @return {[type]}     [description]
     */
    redirect(url){
        window.location.replace(url);
    }

    /**
     * action断言，用于Action返回中调用。通用方法，避免编写大量重复代码
     * @param  {[type]} action  Action对象
     * @param  {dict} options {fnOK,fnError,...} 
     * @return {[type]}         [description]
     */
    assert(action, options) {
        if (action.state == Constants.ActionState.Success) {
            const resp = action.response;
            if (resp.code == 0) {
                const fnOk = options && options.fnOk;
                fnOk ? fnOk(resp) : message.success("操作成功");
            } else {
                const fnError = options && options.fnError;
                fnError ? fnError(resp) : message.error(resp.message);
            }
        } else if (action.state == Constants.ActionState.Failure) {
            message.error(action.error);
        }
    }
}

module.exports = BasicComponent;
