import { ActionState } from "../app_constants";
import ReduceBase from "./reduce_base";

const defaultState = {
    article: {
        isFetching: false,
        data: null
    },
    articleCategory:{
        isFetching: false,
        data: null
    }
}

class ArticleReducer extends ReduceBase {
    static reduce(state=defaultState, action) {
        switch (action.type) {
            case "Article":
                return ArticleReducer._reduce_get_article(state,action);
            case "CategoryArticle":
                return ArticleReducer._process_list(state, action, "articleCategory");
            default:
                return state;
        }
    }

    static _reduce_get_article(state, action) {
        let isFetching = action.state == ActionState.Doing;
        const resp = action.response;
        if (resp && resp.code == 0) {
            const data = resp.data;
            const article = {
                isFetching,
                data
            };
            return Object.assign({}, state, {article});
        }
        return state;
    }

}

module.exports = ArticleReducer;
