import React from 'react';
import { connect } from 'react-redux';
import { Icon, Tooltip, Spin } from 'antd';

import * as Utils from "../../../utils/common";
import * as GameAction from "../../../actions/game";


class GameTrend extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        return(
            <div className="game-trend-body-trendtype">
                走势图
            </div>
        );
    }
}

export default GameTrend
