import React from 'react';
import { connect } from 'react-redux';
import { Icon, Tooltip, Spin } from 'antd';

import BasicComponent from "../../common/BasicComponent";
import * as Utils from "../../../utils/common";
import * as GameAction from "../../../actions/game";

class BetTypeSelect extends BasicComponent {

    constructor(props) {
        super(props);
        this.state = {
            // 玩法数据加载中
            loading: true,
            // 当前选中的玩法目录
            selectedCategory: null,
            // 当前选中的玩法种类
            selectedSort: null,
            // 当前选中的玩法
            selectedType: null,
        };
    }

    /* ---------------------------- 系统函数 ------------------------------*/

    componentWillMount() {
        this.getGameTypeInfo(this.props);

        //  在非第一次加载时，如从追号面板切换回来,game_type_info不为空，而selectedCategory等为空，导致异常。
        //  所以如果加载时判断进行初始参数构造。
        let { game_type_info } = this.props;
        if (game_type_info.data && game_type_info.data.length)
            this.updateSelectType(game_type_info.data[0]);
    }

    componentWillReceiveProps(nextProps) {

        if(nextProps.Group != this.props.Group)
            this.getGameTypeInfo(nextProps);

        let { game_type_info } = this.props;
        let new_game_type_info = nextProps.game_type_info;
        if (game_type_info != new_game_type_info) {
            // TODO:应该是由数据指定默认
            this.updateSelectType(new_game_type_info.data[0]);
        }
    }

    /* ---------------------------- 自定义函数 ------------------------------*/

    getGameTypeInfo(props){
        this.props.dispatch(GameAction.getGameTypeInfo(props.Group)).then(it => this.setState({
            loading: false
        }));
    }

    updateSelectType(selectedCategory) {
        if (!selectedCategory) return;
        this.setState({selectedCategory });
        let selectedSort = this.sort(selectedCategory.Sorts)[0];
        this.setState({selectedSort});
        let selectedType = this.sort(this.sort(selectedSort.Kinds)[0].Types)[0];
        this.setState({selectedType});
        this.props.onChange&&this.props.onChange(selectedType);
    }

    sort(list) {
        list.sort((a, b) => b.Weight - a.Weight);
        return list;
    }

    /* ---------------------------- Events ------------------------------*/

    onCategoryClick(category) {
        if (category == this.state.selectedCategory) return;
        this.updateSelectType(category);
    }

    onSortClick(sort) {
        if (sort == this.state.selectedSort) return;
        this.setState({
            selectedSort: sort
        });

        let selectedType = this.sort(this.sort(sort.Kinds)[0].Types)[0];
        this.setState({selectedType});
        this.props.onChange&&this.props.onChange(selectedType);
    }

    onTypeClick(type) {
        if (type == this.state.selectedType) return;
        this.setState({
            selectedType: type
        });
        this.props.onChange && this.props.onChange(type);
    }

    /* ---------------------------- Renders ------------------------------*/

    render(){
        const { game_type_info } = this.props;
        return (
            !game_type_info.isFetching && game_type_info.data && game_type_info.data.length
            ?
            this.renderPanel()
            :
            <div className="game-bettype-loading">
                <Spin />
                加载中...
            </div>
        );
    }

    renderPanel() {
        const { game_type_info } = this.props;
        let selectedCategoryIndex = game_type_info.data.indexOf(this.state.selectedCategory);
        let otherCategorys = [...game_type_info.data.slice(0,selectedCategoryIndex),...game_type_info.data.slice(selectedCategoryIndex+1)];
        return (
            <div className="game-bettype">
                <div className="game-bettype-group fn-clear">
                    {otherCategorys.map(category=><label key={category.Name} onClick={()=>this.onCategoryClick(category)}>{category.Name}</label>)}
                    <ul>
                        {this.state.selectedCategory.Sorts.map(sort=><li key={sort.Name} className={sort==this.state.selectedSort?"active":""} onClick={()=>this.onSortClick(sort)}>{sort.Name}</li>)}
                    </ul>
                </div>
                <div className="game-bettype-types">
                    {this.state.selectedSort.Kinds.map(kind=>
                        <div className="fn-clear" key={kind.Name}>
                            <label>{kind.Name}</label>
                            <ul>
                                {this.sort(kind.Types).map(type=><li key={type.Name} className={type==this.state.selectedType?"active":""} onClick={()=>this.onTypeClick(type)}><a>{type.Name}</a></li>)}
                            </ul>
                        </div>
                    )}
                </div>
                <div className="game-bettype-explain fn-clear">
                    <label>范例</label>
                    <Tooltip placement="topRight" trigger="click" title={this.state.selectedType.Explain}>
                        <p>{this.state.selectedType.Explain}</p>
                    </Tooltip>
                    <Tooltip title={this.state.selectedType.Example}>
                        <Icon type="question-circle" />
                    </Tooltip>
                </div>
            </div>
        );
    }
}

BetTypeSelect.propTypes = {
    // 游戏组
    Group: React.PropTypes.string.isRequired,
    // 选中游戏玩法改变
    onChange: React.PropTypes.func
}

module.exports = connect(state => ({
    game_type_info: state.game.game_type_info
}))(BetTypeSelect);
