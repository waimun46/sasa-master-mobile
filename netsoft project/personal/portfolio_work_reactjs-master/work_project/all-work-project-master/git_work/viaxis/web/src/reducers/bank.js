import { ActionState } from "../app_constants";
import ReduceBase from "./reduce_base";

const defaultState = {
    banks: {
        isFetching: false,
        data: null
    },
    // 用户绑定银行卡
    cards: {
        isFetching: false,
        data: null
    },
    // 充值银行
    depositbank: {
		isFetching: false,
        data: []
	},
    //扫码充值
    depositsp: {
        isFetching: false,
        combankid: 0
    }
}

class BankReducer extends ReduceBase {
    static reduce(state=defaultState, action) {
        switch (action.type) {
            case "Bank":
                return ReduceBase._process_list(state,action,"banks");
            case "BankCard":
                return ReduceBase._process_list(state,action,"cards");
            case "BankCardAdd":
                return BankReducer._reduce_add_card(state, action);
            case "DepositBank":
                return BankReducer._reduce_deposit_bank(state, action);
            case "DepositSP":
                return BankReducer._reduce_deposit_sp(state, action);
            default:
                return state;
        }
    }

    static _reduce_add_card(state, action) {
        let isFetching = action.state == ActionState.Doing;
        const resp = action.response;
        if (resp && resp.code == 0) {
            let card = resp.data;
            let cards = state.cards.data || [];
            cards = {
                isFetching,
                data:[card,...cards]
            };
            return Object.assign({}, state, {cards});
        }
        return state;
    }

    static _reduce_deposit_bank(state, action) {
    	let isFetching = action.state == ActionState.Doing;
		const resp = action.response;
		if (resp && resp.code == 0) {
			let depositbank = {
				isFetching,
				data: resp.data
			};
            return Object.assign({}, state, {depositbank});
        }
		return Object.assign({}, state, {
			depositbank: {
				isFetching
			}
		});	
    }

    static _reduce_deposit_sp(state, action) {
    	let isFetching = action.state == ActionState.Doing;
		const resp = action.response;
		if (resp && resp.code == 0) {
			let depositsp = {
				isFetching,
				combankid: resp.data || 0
			};
            return Object.assign({}, state, {depositsp});
        }
		return Object.assign({}, state, {
			depositsp: {
				isFetching
			}
		});	
    }

}

module.exports = BankReducer;
