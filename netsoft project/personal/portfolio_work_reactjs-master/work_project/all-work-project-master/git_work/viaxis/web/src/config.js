
module.exports = {
	Debug: __DEV__,
	API: "http://api.firmbcpt.com",
	WSUrl: "ws://api.firmbcpt.com/ws",
	// 彩种号码球数量
	GameDataNum: {
		FC3D: 3,
		CQSSC: 5,
		KL8: 20,
		KL10: 8,
		KENO: 20,
		PL3: 3,
		FFC: 5,
		ZRC: 5,
		PK10: 10
	}
};
