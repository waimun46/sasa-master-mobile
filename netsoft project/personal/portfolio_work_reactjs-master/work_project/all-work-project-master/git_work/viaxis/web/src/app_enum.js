/**
 * [UserKindEnum description]
 * @type {Object}
 */
export const UserKind = {
    Super: 1,
    Master: 2,
    Agent: 3,
    Member: 4,
    Shadow: 5
};

/**
 * 通用状态
 * @type {Object}
 */
export const CommonStatus = {
    Enable: 0,
    Disabled: 1
};

/**
 * 流水类型
 * @type {Object}
 */
export const BillFlowType = {
    Deposit: 0,
    Withdraw: 1,
    Transfer: 2,
    Game: 3,
    Commission: 4,
    Bonus: 5
};

/**
 * 注单状态
 * @type {Object}
 */
export const DealStatus = {
    UnOpen: 0,
    Settle: 1,
    Profit: 2,
    Commission: 3,
    PreCancel: 4,
    Canceled: 5
};

/**
 * 公告类型
 * @type {Object}
 */
export const BulletinType = {
    Info: 0,
    Warning: 1
}

/**
 * 支付类型
 * @type {Object}
 */
export const BankType = {
    Bank: 0,
    TTP: 1,
    SP: 2
}
