import React from 'react';
import { connect } from 'react-redux';
import { browserHistory,Link } from 'react-router';
import { Modal, Button, Icon, Input, Table,Checkbox, DatePicker,Form } from 'antd';
import $ from "jquery";

import BasicComponent from "../common/BasicComponent";
import DashContainer from '../../containers/DashContainer';

const CheckboxGroup = Checkbox.Group;
const RangePicker = DatePicker.RangePicker;
const FormItem = Form.Item;

import Config from "../../config";
import {getUserInfo} from "../../actions/passport";
import * as GameAction from "../../actions/game";
import * as Constants from "../../app_constants";
import * as Utils from "../../utils/common";

class Trend extends BasicComponent{

    constructor(props){
        super(props);
        let gameGroup = (props.params.gameGroup || Constants.GameGroup[0]).toUpperCase();
        if (Constants.GameGroup.indexOf(gameGroup) < 0)
            browserHistory.push("/404");
        this.group = gameGroup;

        let gameDataNum = Config.GameDataNum[this.group];
        if(gameDataNum < 0 )
            browserHistory.push("/404");
        this.num = gameDataNum;
        this.trendtype = ['遗漏', '折线', '遗漏分成','分隔线'];
        this.state={
            minNum:0,
            maxNum:0,
            top:30,
            name:'',
            starttime:null,
            endtime:null
        };
    }

    /* ---------------------------- 系统函数 ------------------------------*/

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps != this.props || nextState != this.state;
    }

    componentWillMount() {
        this.queryTrendinfo();
    }


    componentDidUpdate(prevProps, prevState) {
        this.createcanvas(0);
    }


    componentWillReceiveProps(nextProps){
        if(!!nextProps.gamegroupinfo)
            this.setState({
                minNum:nextProps.gamegroupinfo.MinNum,
                maxNum:nextProps.gamegroupinfo.MaxNum,
                name:nextProps.gamegroupinfo.Name,
            })
    }

    /* ---------------------------- Functions ------------------------------*/

    queryTrendinfo(){
        var group_name = this.group;
        this.props.dispatch(GameAction.getGameTrendData(group_name,this.state.top,this.state.starttime,this.state.endtime));
        this.props.dispatch(GameAction.getGameGroupById(group_name));
    }

    loadByCount(count){
        this.setState({top: count,starttime:null,endtime:null}, ()=>{ this.queryTrendinfo()})
    }

    quertByTime(dates,dateStrings){
        let starttime = new Date(dateStrings[0])
        let endtime = new Date(dateStrings[1])
        starttime = new Date(starttime.getFullYear(),starttime.getMonth(),starttime.getDate(),0,0,0);
        endtime = new Date(endtime.getFullYear(),endtime.getMonth(),endtime.getDate(),23,59,59);
        this.setState({
            starttime: Utils.unixtimestampFromDate(starttime),
            endtime: Utils.unixtimestampFromDate(endtime)
		})
    }

    LoadByDate(days){
        let timenow = new Date();
        let starttime = new Date(timenow.getFullYear(),timenow.getMonth(),timenow.getDate() - days,23,59,59);
        let endtime = new Date(timenow.getFullYear(),timenow.getMonth(),timenow.getDate(),23,59,59);
        this.setState({top: 0,starttime:Utils.unixtimestampFromDate(starttime),endtime:Utils.unixtimestampFromDate(endtime)}, ()=>{ this.queryTrendinfo()})
    }

    tableColumns(){
        const {dispatch} = this.props;
        const nums = Utils.range(this.state.minNum,this.state.maxNum + 1);
        let resultsLength = 0;
        const numCols=Utils.range(0,Config.GameDataNum[this.group]).map((id)=>{
            let winclassname = '';
            if(this.group == 'KL10'){
                winclassname = 'game-trend-body-gametype-table-20-hot-' + (id + 1);
                resultsLength = 150;
            }else if(this.group == 'KL8' || this.group == 'KENO'){
                winclassname = 'game-trend-body-gametype-table-80-hot-' + (id + 1);
                resultsLength = 400;
            }else{
                winclassname = 'game-trend-body-gametype-table-hot-' + (id + 1);
                resultsLength = 150;
            }
            return {
                title:`第${id+1}球`,
                key:`z${id+1}`,
                width:200,
                children:nums.map((child,index)=>{
                    return {
                        title:child,
                        dataIndex:`z${id+1}-${child}`,
                        key:`z${id+1}-${child}`,
                        render: (text) => {
                            return <span name={text >= 0 ? "hotSpan" : "" } className={ text >= 0 ? `${winclassname}` : "loseSpan"}>{Math.abs(text)}</span>
                        }
                    }
                })
        }});

        const columns = [
            {title: '期号',width: 100,dataIndex:'Qh',key:'Qh'},
            {title: '开奖号码',width: resultsLength,dataIndex: 'Results',key:'Results'},
            ...numCols
        ];

        return columns;
    };

    tableFooter(){
        const self = this;
        const {dispatch} = this.props;
        const nums = Utils.range(this.state.minNum,this.state.maxNum + 1);
        let resultsLength = 250;
        if(this.group == 'KL8' || this.group == 'KENO') resultsLength = 500;
        const numCols = Utils.range(0, Config.GameDataNum[this.group]).map((id)=>{
            return {
                title:`第${id+1}球`,
                key:`z${id+1}`,
                width:100,
                children:nums.map((child,index)=>{
                    return {
                        title:child,
                        dataIndex:`z${id+1}-${child}`,
                        key:`z${id+1}-${child}`
                    }
                })
        }});

        const columns = [
            {title: '数据统计',width: resultsLength,dataIndex:'Results',key:'Results'},
            ...numCols
        ];

        return columns;
    }

    createcanvas(defaultlength){
        window.scrollTo(0,0);
        $("#game-trend-chart canvas").remove();
        let lastObj;
        const obj = this;
        let chartlength = 0;
        if(this.props.trenddata.data != null)
            chartlength = 370 + (this.props.trenddata.data.length * 30)
        if(defaultlength > 0) 
            chartlength -= defaultlength;
        console.log('top:' + chartlength )
        var hei = document.documentElement.scrollHeight||document.body.scrollHeight;
        $('.ant-table-body').append('<div id="game-trend-chart" style="position:relative;top:0px;left:-142px;"></div>');
        $('#game-trend-chart').css('top','-' + chartlength + 'px');
        $('.ant-table-body').css('overflow-y','hidden');
        $('.ant-table-body').css('overflow-x','scroll');
        const colors = ['rgb(251, 167, 94)','rgb(31, 166, 232)','rgb(8, 191, 2)','rgb(133, 133, 251)','rgb(70, 189, 149)','rgb(226, 107, 171)','rgb(242, 182, 83)','rgb(98, 142, 243)','rgb(94, 198, 66)','rgb(246, 109, 109)','#BF3EFF','#8B3A3A','#CDAD00','#CD661D','#CD4F39','#C5C1AA','#B22222','#FF7F24','#FF4500','#FF34B3']
        for(let i = 1;i <= this.num;i++){
            let currentObj = null;
            if(this.group == 'KL10'){
                currentObj = $('.game-trend-body-gametype-table-20-hot-' + i + '');
            }else if(this.group == 'KL8' || this.group == 'KENO'){
                currentObj = $('.game-trend-body-gametype-table-80-hot-' + i + '');
            }else{
                currentObj = $('.game-trend-body-gametype-table-hot-' + i + '');
            }
            let color = colors[i-1];
            currentObj.each(function(){
                if(lastObj == null ){
                    lastObj = $(this);
                    return true;
                }
                let currentX = $(this).offset().left;
                let lastX = lastObj.offset().left;

                let currentY = $(this).offset().top;
                let lastY = lastObj.offset().top;

                if(currentX > lastX){
                    let offX = lastObj.offset().left;
                    let offY = lastObj.offset().top;
                    obj.drawLine(currentX - lastX, currentY - lastY, offX, offY, 0, color);
                }else if(currentX == lastX){
                    let offX = lastObj.offset().left;
                    let offY = lastObj.offset().top;
                    obj.drawLine(20, 30, offX-10, offY+4, 2, color);
                }
                else{
                    let offX = $(this).offset().left;
                    let offY = lastObj.offset().top;
                    obj.drawLine(lastX - currentX,currentY - lastY, offX, offY, 1, color);
                }
                lastObj = $(this);
            })
        }
    }

    drawLine(width,height,x,y,nag,color){
        const canvas = $('<canvas width='+(width - 5)+' height=' + height + ' style="position: absolute; visibility: visible; top: ' + (y + 10) +'px; left: '+ (x + 10) +'px;"/>');
        $('#game-trend-chart').append(canvas);
        const context = canvas[0].getContext('2d');
        context.fillStyle = 'rgba(255, 255, 255, 0)';
        context.strokeStyle = color;
        context.imageSmoothingEnabled = true;
        context.fillRect(50,50,75,50);
        switch(nag){
            case 1:
                context.moveTo(width, 0);
                context.lineTo(0,height);
            break;
            case 2:
                context.moveTo(10, 0);
                context.lineTo(10,40);
            break;
            default:
                context.moveTo(0, 0);
                context.lineTo(width,height);
            break;
        }
        context.fill();
        context.stroke();
    }

    onTrendTypeChange(selectedtrendtype){
        for(let i = 0;i < this.trendtype.length;i++){
            if($.inArray(this.trendtype[i], selectedtrendtype) >= 0 ){
                this.showTrendType(this.trendtype[i],true)
            }else{
                this.showTrendType(this.trendtype[i],false)
            }
        }
    }

    showTrendType(type,isShow){
        let displayvalue = 'block';
        if(!isShow) displayvalue = 'none';
        switch(type){
            case '遗漏':
                    $(".loseSpan").css("display", displayvalue);
            break;
            case '折线':
                    $('#game-trend-chart').css("display", displayvalue);
            break;
            case '遗漏分成':
            break;
            case '分隔线':
                let px = 0;
                if(isShow) px = 2; else px = 0.5;
                var tr = $(".ant-table-tbody tr");
                var trLen = tr.length;
                tr.each(function(i, tr) {
                    var tdLen = $(this).find("td").length;
                    if(i > 0 && (i + 1) % 5 == 0) {
                        $(tr).find('td').css('border-bottom','' + px + 'px solid #dbdbdb');
                    }
                });
                if(isShow)
                    this.createcanvas(20);
                else
                    this.createcanvas(25);
            break;
        }
    }

    /* ---------------------------- Renders ------------------------------*/

    get data(){
        let data = this.props.trenddata.data;
        if(data&&data.length){
            const self = this;
            const numCount = Utils.range(this.state.minNum,this.state.maxNum + 1);
            let count = this.state.minNum;
            for(let i = 0;i < data.length;i++){
                data[i].Results = data[i].Results.replace(/,/g, " ");
                let lostData=data[i].DataMiss.split(",").forEach((it,index)=>{
                    let zIndex = parseInt(index / numCount.length) + 1;
                    let numIndex = (index % numCount.length);
                    data[i][`z${zIndex}-${count}`]= it == 0 ? count : it;
                    if(count == this.state.maxNum)
                        count = this.state.minNum
                    else
                        count++;
                });
            }
        }
        return data;
    }

    get footerData(){
        let data = this.props.statisticsdata.data;
        if(data&&data.length){
            const self = this;
            const numCount = Utils.range(this.state.minNum,this.state.maxNum + 1);
            let count = this.state.minNum;
            for(let i = 0;i < data.length;i++){
                data[i].Results = data[i].Results.replace(/,/g, " ");
                let lostData=data[i].DataMiss.split(",").forEach((it,index)=>{
                    let zIndex = parseInt(index / numCount.length) + 1;
                    let numIndex = (index % numCount.length);
                    data[i][`z${zIndex}-${count}`]= it == 0 ? 0 : it;
                    if(count == this.state.maxNum)
                        count = this.state.minNum
                    else
                        count++;
                });
            }
        }
        return data;
    }

    render(){
        let columns = this.tableColumns();
        let data = this.data;
        let footerColumns = this.tableFooter();
        const footdata = this.footerData;
        let scrollLength = 0;
        let tableClassName = '';
        let footTableClassName = '';
        if(this.num == 5)
            scrollLength = (this.state.maxNum - this.state.minNum + 1) * this.num * 45;
        else if(this.num == 10)
            scrollLength = (this.state.maxNum - this.state.minNum + 1) * this.num * 42.7;
        else if(this.num == 8)
            scrollLength = (this.state.maxNum - this.state.minNum + 1) * this.num * 41.7;
        else if(this.num == 20)
            scrollLength = (this.state.maxNum - this.state.minNum + 1) * this.num * 40.4;
        if(this.group == 'KL10'){
            tableClassName = 'game-trend-body-gametype-table-20';
            footTableClassName = 'game-trend-body-gametype-footertable-20';
        }else if(this.group == 'KL8' || this.group == 'KENO'){
            tableClassName = 'game-trend-body-gametype-table-80';
            footTableClassName = 'game-trend-body-gametype-footertable-80';
        }else{
            tableClassName = 'game-trend-body-gametype-table';
            footTableClassName = 'game-trend-body-gametype-footertable';
        }

        return(
            <DashContainer title={`彩种:${this.state.name}`}>
                <div className="game-trend-body">
                    <div className="game-trend-body-trendtype">
                        <div><label>走势图</label></div>
                        <CheckboxGroup options={this.trendtype} defaultValue={['折线','遗漏']} onChange={this.onTrendTypeChange.bind(this)}/>
                    </div>
                    <div className="game-trend-body-gametype-query">
                        <ul>
                            <li><a onClick={()=>this.loadByCount(30)}>近30期</a></li>
                            <li><a onClick={()=>this.loadByCount(50)}>近50期</a></li>
                            <li><a onClick={()=>this.LoadByDate(1)}>今天数据</a></li>
                            <li><a onClick={()=>this.LoadByDate(2)}>近2天</a></li>
                            <li><a onClick={()=>this.LoadByDate(5)}>近5天</a></li>
                        </ul>
                    </div>
                    <div className="game-trend-body-gametype-queryform">
                        <form>
                            <FormItem>
                                <RangePicker name="open_time" onChange={this.quertByTime.bind(this)}/>
                                <Button type="primary" icon="search" onClick={()=>this.queryTrendinfo()}>查看</Button>
                            </FormItem>
                        </form>
                    </div>
                    <div className={tableClassName} >
                        <Table columns={columns} ref="datatable" size="Small" dataSource={data} bordered  pagination={false} loading={this.props.trenddata.isFetching} scroll={{ x:scrollLength }} rowKey="Id" />
                    </div>
                    <div className={footTableClassName}>
                        <Table columns={footerColumns} ref="foottable" size="Small" dataSource={footdata} bordered pagination={false} scroll={{ x:scrollLength }} rowKey="Results" />
                    </div>
                </div>
            </DashContainer>
        );
    }
}

module.exports = connect(state => ({
    trenddata: state.game.trenddata,
    statisticsdata: state.game.statisticsdata,
    gamegroupinfo: state.game.gamegroup,
}))(Trend);
