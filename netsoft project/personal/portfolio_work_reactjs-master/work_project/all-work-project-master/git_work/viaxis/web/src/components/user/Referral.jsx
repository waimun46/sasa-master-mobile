import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router'
import CopyToClipboard from 'react-copy-to-clipboard';
import $ from "jquery";

import { message, Row, Col, Table, Breadcrumb, Switch, Spin, Form, Input, InputNumber, Button, Select, Icon } from 'antd';

import BasicComponent from "../common/BasicComponent";
import DashContainer from '../../containers/DashContainer';
import SearchForm from "../common/SearchForm";

import * as PassportAction from '../../actions/passport';
import * as Constants from "../../app_constants";
import * as Enums from "../../app_enum";
import * as Utils from '../../utils/common';
import * as Validators from "../../utils/validator";
import * as RenderHelper from '../../utils/render_helper';

class ReferralList extends BasicComponent {

    constructor(props) {
        super(props);
        this.querys = {};
    }

    /* ---------------------------- Sys ------------------------------*/

    componentWillMount() {
        this.queryData();
    }

    /* ---------------------------- Functions ------------------------------*/

    queryData() {
        this.props.dispatch(PassportAction.queryReferral(this.querys));
    }

    get myBackpct() {
        const userinfo = this.props.userinfo.data;
        if (!userinfo) return 0;

        const backpct = parseFloat((userinfo.Backpct * 100).toFixed(1));
        return backpct;
    }

    /* ---------------------------- Events ------------------------------*/

    onSearchClick(values) {

    }

    onResetClick() {

    }

    onAddClick() {

        const backpct = this.myBackpct;

        const dialog = () => RenderHelper.dialog(`添加会员注册链接`, <AddFormWraper backpct={backpct} onSubmit={this.onAddSubmit.bind(this)} />);
        this._dialog = dialog();
    }

    onEdit(referral) {
        const backpct = this.myBackpct;

        const dialog = () => RenderHelper.dialog(`编辑会员注册链接`, <EditFormWraper referral={referral} backpct={backpct} onSubmit={(it) => this.onEditSubmit(referral, it)} />);
        this._dialog = dialog();
    }

    onAddSubmit(values) {
        return this.props.dispatch(PassportAction.addReferral(values.backpct, values.captcha))
            .then(act => {
                const resp = act.response;
                if (resp.code == 0) {
                    if (this._dialog) {
                        this._dialog.destroy();
                        this._dialog = null;
                    }
                }
                return act;
            });
    }

    onEditSubmit(referral, values) {
        return this.props.dispatch(PassportAction.updateReferral(referral.Id, values.backpct, values.status))
            .then(act => {
                const resp = act.response;
                if (resp.code == 0) {
                    if (this._dialog) {
                        this._dialog.destroy();
                        this._dialog = null;
                    }
                    this.queryData();
                }
                return act;
            });
    }

    /* ---------------------------- Renders ------------------------------*/

    render() {
        const self = this;
        const { referral } = this.props;
        let data = referral.data || [];

        return (
            <DashContainer title="推广链接">
                <br />
                <Button type="primary" onClick={this.onAddClick.bind(this)}>添加</Button>
                <div className="search-result-list">
                    <Table loading={referral.isFetching} columns={this.columns} dataSource={data} pagination={false} rowKey="Id" />
                </div>
            </DashContainer>
        );
    }

    renderSearch() {
        const formItemLayout = {
            labelCol: { span: 8 },
            wrapperCol: { span: 16 }
        };

        const userinfo = this.props.userinfo.data;

        return (
            <SearchForm showAddBtn onSearch={this.onSearchClick.bind(this)} onReset={this.onResetClick.bind(this)} onAdd={this.onAddClick.bind(this)}>
                <email label="邮箱" {...formItemLayout}>
                    <Input />
                </email>
                <backpct label="返点" {...formItemLayout}>
                    <Input />
                </backpct>
                <create_time label="时间" {...formItemLayout} format={(times) => Utils.dateRangeFormat(times)}>
                    {RenderHelper.rangeDatePick()}
                </create_time>
            </SearchForm>
        );
    }

    get columns() {
        const self = this;
        const columns = [
            { title: '#', render: RenderHelper.render_index },
            {
                title: "链接", dataIndex: "Id", render(id) {
                    const url = `http://${window.location.host}/register/${id}`;
                    return (
                        <div>
                            {url}
                            &nbsp;
                        <CopyToClipboard text={url} onCopy={() => message.success("复制成功！")}>
                                <Button type="primary" size="small" icon="copy">复制</Button>
                            </CopyToClipboard>
                        </div>
                    );
                }
            },
            { title: "返点", dataIndex: "Backpct", render: RenderHelper.render_percent(1) },
            { title: "状态", dataIndex: "Status", render: (status) => RenderHelper.render_status(Constants.UserStatus, status) },
            { title: "创建时间", dataIndex: "CreateTime", render: RenderHelper.render_time },
            {
                title: "操作", render: (record) => {
                    return (
                        <span>
                            <Button type="primary" icon="edit" onClick={() => this.onEdit(record)}>编辑</Button>
                        </span>
                    );
                }
            }
        ];
        return columns;
    }

}


class AddForm extends BasicComponent {
    constructor(props) {
        super(props);
        this.state = { loading: false };
        this.time = new Date();
    }

    handleSubmit(e) {
        e.preventDefault();

        const { dispatch, parent } = this.props;

        this.props.form.validateFields((errors, values) => {
            if (errors) return;
            this.setState({ loading: true });
            this.props.onSubmit && this.props.onSubmit(values).then(act => {
                this.setState({ loading: false });

                const resp = act.response;
                if (resp.code == 0) {
                    message.success("添加成功");
                } else {
                    message.error(resp.message);
                }

            });

        });
    }


    refreshCaptcha() {
        let captcha = this.props.form.getFieldInstance("captcha");
        captcha.refs.input.value = "";
        captcha.refs.input.focus();
        this.refs.captcha_img.src = Utils.addActionToUrl(this.refs.captcha_img.src, "_", new Date().getTime());
    }

    /* ---------------------------- Renders  ------------------------------*/

    render() {
        const { getFieldDecorator } = this.props.form;

        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 14 },
        };

        const tailFormItemLayout = {
            wrapperCol: {
                span: 14,
                offset: 6,
            },
        };

        const { backpct } = this.props;

        const captchaUrl = Utils.addActionToUrl("/captcha", "_", this.time.getTime());

        return (
            <Form onSubmit={this.handleSubmit.bind(this)} autoComplete="off">
                <Form.Item label="返点" {...formItemLayout}>
                    {getFieldDecorator('backpct', { initialValue: 0 })(
                        <InputNumber min={0} max={backpct} step={0.1} />
                    )}
                    <span className="ant-form-text"> % (最大{backpct}%)</span>
                </Form.Item>
                <Form.Item label="验证码" {...formItemLayout} hasFeedback>
                    <Row gutter={8}>
                        <Col span={12}>
                            {getFieldDecorator('captcha', {
                                rules: [{ required: true, message: "请输入验证码" }],
                            })(
                                <Input type="text" className="input-captcha" maxLength="4" />
                            )}
                        </Col>
                        <Col span={12}>
                            <img src={captchaUrl} ref="captcha_img" className="img-captcha" onClick={this.refreshCaptcha.bind(this)} />
                        </Col>
                    </Row>
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                    <Button loading={this.state.loading} className="fn-width100" type="primary" htmlType="submit" size="large">确定</Button>
                </Form.Item>
            </Form>
        );
    }
}

AddForm.propTypes = {
    /**
     * 自身返点
     * @type {[type]}
     */
    backpct: React.PropTypes.number.isRequired,
    /**
     * 确定提交事件
     * @param {dict} values 表单内容
     * @type {[type]}
     */
    OnSubmit: React.PropTypes.func
};
const AddFormWraper = Form.create()(AddForm);



class EditForm extends BasicComponent {
    constructor(props) {
        super(props);
        this.state = { loading: false };
    }

    handleSubmit(e) {
        e.preventDefault();

        const { dispatch, parent } = this.props;

        this.props.form.validateFields((errors, values) => {
            if (errors) return;
            this.setState({ loading: true });
            this.props.onSubmit && this.props.onSubmit(values).then(act => {
                this.setState({ loading: false });

                const resp = act.response;
                if (resp.code == 0) {
                    message.success("修改成功");
                } else {
                    message.error(resp.message);
                }

            });

        });
    }

    /* ---------------------------- Renders  ------------------------------*/

    render() {
        const { getFieldDecorator } = this.props.form;

        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 14 },
        };

        const tailFormItemLayout = {
            wrapperCol: {
                span: 14,
                offset: 6,
            },
        };

        const { backpct, referral } = this.props;

        const currentBackpct = parseFloat((referral.Backpct * 100).toFixed(1));

        return (
            <Form onSubmit={this.handleSubmit.bind(this)} autoComplete="off">
                <Form.Item label="返点" {...formItemLayout}>
                    {getFieldDecorator('backpct', { initialValue: currentBackpct })(
                        <InputNumber min={0} max={backpct} step={0.1} />
                    )}
                    <span className="ant-form-text"> % (最大{backpct}%)</span>
                </Form.Item>
                <Form.Item label="状态" {...formItemLayout}>
                    {getFieldDecorator('status', {
                        initialValue: referral.Status == 0,
                        valuePropName: "checked"
                    })(
                        <Switch checkedChildren={'正常'} unCheckedChildren={'禁用'} />
                    )}
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                    <Button loading={this.state.loading} className="fn-width100" type="primary" htmlType="submit" size="large">确定</Button>
                </Form.Item>
            </Form>
        );
    }
}

EditForm.propTypes = {
    /**
     * 自身返点
     * @type {[type]}
     */
    backpct: React.PropTypes.number.isRequired,
    /**
     * 编辑的链接实体
     * @type {[type]}
     */
    referral: React.PropTypes.object.isRequired,
    /**
     * 确定提交事件
     * @param {dict} values 表单内容
     * @type {[type]}
     */
    OnSubmit: React.PropTypes.func
};
const EditFormWraper = Form.create()(EditForm);



module.exports = connect(state => ({
    userinfo: state.passport.userinfo,
    referral: state.passport.referral
}))(ReferralList);
