import React from 'react';
import Header from '../components/common/Header';
import SliderMenu from '../components/common/SliderMenu';
import BasicComponent from "../components/common/BasicComponent";

class DashLayout extends BasicComponent {

    constructor(props){
        super(props);
    }

    render() {
        return (
            <div className="ant-layout-aside">
                <SliderMenu />
                <div className="ant-layout-main">
                    <Header />
                    <div className="ant-layout-container fn-clear">
                        {this.props.children}
                    </div>
                </div>
            </div>
        );
    }
}

module.exports = DashLayout;
