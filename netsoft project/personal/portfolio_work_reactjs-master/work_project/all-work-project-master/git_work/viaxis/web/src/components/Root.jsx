import React from 'react';
import { browserHistory } from 'react-router'

class Root extends React.Component {
    constructor(props){
        super(props);
    }

    render() {
        const baseProps = {
            setRoute: function(route, e){
                if(e) e.preventDefault();
                browserHistory.push(route);
            },
            imagesFolder: "..",
        }
        const childrenWithProps = React.Children.map(this.props.children, function(child){
                    return React.cloneElement(child, baseProps)
                });
        return (
            <div>
                {childrenWithProps}
            </div>
        );
    }
}

module.exports = Root;
