import React from 'react';
import ReactDOM from 'react-dom';
import classNames from 'classnames';
import { Button, Modal, Icon } from 'antd';
import BasicComponent from "./BasicComponent";

/**
 * 自定义弹窗，主要用于倒计时通知
 */
class Dialog extends BasicComponent{

    constructor(props) {
        super(props);

        this.state = {
            // 是否倒计时
            isCowndown: false,
            // 倒计时时长
            seconds: 3
        };
    }

    /* ---------------------------- 系统函数 ------------------------------*/

    componentWillMount() {
        const { seconds } = this.props;

        if (seconds && !isNaN(seconds)) {
            this.setState({isCowndown:true, seconds});

            this.interval = setInterval(() => {
                const {seconds} = this.state;

                if (seconds <= 1) {
                    clearInterval(this.interval);
                    this.interval = null;
                    (this.props.onOk && this.props.onOk()) || (this.props.onClose && this.props.onClose());
                    return;
                }

                this.setState({
                    seconds: seconds - 1,
                });

            }, 1000);
        }
    }

    componentWillUnmount() {
        if(this.interval){
            clearInterval(this.interval);
            this.interval = null;
        }
    }


    /* ---------------------------- Render ------------------------------*/

    get okText(){
        let text = this.props.okText || "确定";
        if (this.state.isCowndown)
            text = `${text}(${this.state.seconds})`;
        return text;
    }

    render(){

        const props = Object.assign({iconType: 'question-circle' }, this.props);
        const prefixCls = props.prefixCls || 'confirm';

        let width = props.width || 416;
        let style = props.style || {}; 

        // 默认为 true，保持向下兼容
        if (!('okCancel' in props)) {
            props.okCancel = true;
        }

        props.cancelText = props.cancelText || "取消";

        let body = (
            <div className={`${prefixCls}-body`}>
                <Icon type={props.iconType} />
                <span className={`${prefixCls}-title`}>{props.title}</span>
                <div className={`${prefixCls}-content`}>{props.content}</div>
            </div>
        );

        let footer: React.ReactElement<any> | null = null;

        if (props.okCancel) {
            footer = (
                <div className={`${prefixCls}-btns fn-clear`}>
                    <Button type="ghost" onClick={()=>props.onCancel&&props.onCancel()||props.onClose&&props.onClose()}>
                        {props.cancelText}
                    </Button>
                    <Button type="primary" onClick={()=>props.onOk&&props.onOk()||props.onClose&&props.onClose()} autoFocus>
                        {this.okText}
                    </Button>
                </div>
            );
        } else {
            footer = (
                <div className={`${prefixCls}-btns`}>
                    <Button type="primary" onClick={()=>props.onOk&&props.onOk()||props.onClose&&props.onClose()} autoFocus>
                        {this.okText}
                    </Button>
                </div>
            );
        }
        const classString = classNames("", {
            [`${prefixCls}-body-wrapper`]: true,
        }, props.className);

        return (
            <Modal
                onCancel={this.props.onClose}
                visible
                footer=''
                width={width}>
                <div className={classString}>
                    {body}{footer}
                </div>
            </Modal>
        );
    }
}

module.exports = Dialog;
