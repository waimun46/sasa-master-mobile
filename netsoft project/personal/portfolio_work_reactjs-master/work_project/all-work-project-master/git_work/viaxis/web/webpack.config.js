const path = require('path');
const webpack = require('webpack');

const node_modules = path.resolve(__dirname, 'node_modules');

const webpackDevHost = '0.0.0.0';
const webpackDevPort = 8885;

module.exports = function () {
    return {
        context: __dirname + '/src',
        entry: {
            app: './index.js',
        },
        output: {
            path: __dirname + '/dist', // `dist` is the destination
            publicPath: '/assets/',
            filename: 'bundle.js',
        },
        plugins: [
            new webpack.HotModuleReplacementPlugin(),
            new webpack.NoErrorsPlugin(),
            new webpack.DefinePlugin({
                __DEV__: JSON.stringify(true),
                __PRERELEASE__: JSON.stringify(JSON.parse(process.env.BUILD_PRERELEASE || 'false'))
            })
        ],
        resolve: {
            extensions: ['', '.js', '.jsx']
        },
        devServer: {
            hot: true,
            open: true, // to open the local server in browser
            contentBase: __dirname + '/pages',
            host: webpackDevHost,
            port: webpackDevPort,
            historyApiFallback: true,
        },
        module: {
            loaders: [{
                test: /\.jsx?$/,
                loader: 'babel?presets[]=react,presets[]=es2015,presets[]=stage-0'
            }, {
                test: /\.json$/,
                loader: "json"
            },
            // LESS
            {
                test: /\.less$/,
                loader: 'style!css!less'
            },
            {
                test: /\.css$/,
                loader: 'style!css'
            },
            ],
        }
    };
}();
