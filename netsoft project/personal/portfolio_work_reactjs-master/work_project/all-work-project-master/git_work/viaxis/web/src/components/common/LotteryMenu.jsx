import React from 'react';
import ReactDOM from 'react-dom';
import $ from "jquery";
import { Icon, Menu } from 'antd';
import { connect } from 'react-redux';
import { browserHistory, Link } from 'react-router'

import BasicComponent from "./BasicComponent";


class LotteryMenu extends BasicComponent{

    constructor(props) {
        super(props);
        this.state = {
            visual: false,
            openKeys: ["热门彩种"]
        };
    }

    /* ------------------ Sys ----------------- */

    componentWillMount() {}

    componentDidMount(){
        document.addEventListener('click', this.clickDocument.bind(this), false);
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.clickDocument.bind(this), false);
    }


    /* ------------------ Events ----------------- */

    clickDocument(e){
        const component = ReactDOM.findDOMNode(this);
        if (!component.contains(e.target)) {
            this.setState({visual:false});
        }
    }

    onOpenChange(openKeys){
        const state = this.state;
        const latestOpenKey = openKeys.find(key => !(state.openKeys.indexOf(key) > -1));
        const latestCloseKey = state.openKeys.find(key => !(openKeys.indexOf(key) > -1));

        let nextOpenKeys = [];
        if (latestOpenKey) {
            nextOpenKeys = this.getAncestorKeys(latestOpenKey).concat(latestOpenKey);
        }
        if (latestCloseKey) {
            nextOpenKeys = this.getAncestorKeys(latestCloseKey);
        }
        this.setState({ openKeys: nextOpenKeys });
    }

    getAncestorKeys(key){
        const map = {
          sub3: ['sub2'],
        };
        return map[key] || [];
    }

    onTitleClick(){
        this.setState({visual:!this.state.visual});
    }

    onEnumItemClick(e){
        browserHistory.push(e.key);
        this.setState({visual:false});
    }

    /* ------------------ Function ----------------- */

    get lotters() {
        return [
            ["热门彩种", [{
                name: "重庆时时彩",
                url: "/bet"
            }, 

            // {
            //     name: "帝国分分彩",
            //     url: "/bet/ffc"
            // }, 
            
            {
                name: "北京PK10",
                url: "/bet/pk10"
            }]],

            ["时时彩", [{
                name: "重庆时时彩",
                url: "/bet/cqssc"
            }]],

            ["高频彩", [{
                name: "重庆幸运农场",
                url: "/bet/kl10"
            }, {
                name: "北京快乐8",
                url: "/bet/kl8"
            }]],

            ["境外彩", [{
                name: "KENO",
                url: "/bet/keno"
            }]],

            ["全国彩", [{
                name: "福彩3D",
                url: "/bet/fc3d"
            }, {
                name: "排列三",
                url: "/bet/pl3"
            }]],
        ];
    }

    /* ------------------ Renders ----------------- */
    render(){
        const { visual } = this.state;
        return (
            <div className="category" ref="lotteryMenu">
                <span className="category-title" onClick={this.onTitleClick.bind(this)}>
                    彩票 <Icon type={`${visual?"up":"down"}-circle-o`} />
                </span>
                {visual&&
                    <div className="category-content">
                        <Menu mode="inline" openKeys={this.state.openKeys} 
                            onOpenChange={this.onOpenChange.bind(this)} 
                            onClick={this.onEnumItemClick.bind(this)} 
                            inlineIndent={0} style={{ width: 220 }}>
                            {this.lotters.map(it=><Menu.SubMenu key={it[0]} title={<span>{it[0]}</span>}>
                                {it[1].map(x=><Menu.Item key={x.url}>{x.name}</Menu.Item>)}
                            </Menu.SubMenu>)}
                        </Menu>
                    </div>
                }
            </div>
        );

    }
}

module.exports = LotteryMenu;
