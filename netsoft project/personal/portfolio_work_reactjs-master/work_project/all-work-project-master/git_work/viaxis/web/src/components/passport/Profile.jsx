import React from 'react';
import { connect } from 'react-redux';
import { Row, Col, Spin, Form, Input, Button, Icon } from 'antd';

import BasicComponent from "../common/BasicComponent";
import DashContainer from '../../containers/DashContainer';

import * as PassportAction from '../../actions/passport';
import * as Utils from '../../utils/common';
import * as RenderHelper from '../../utils/render_helper';

class Profile extends BasicComponent {

    constructor(props) {
        super(props);
    }

    /* ---------------------------- Sys ------------------------------*/

    componentDidMount(){
        this.getUserInfo();
        this.getLastLogin();
    }

    /* ---------------------------- Functions ------------------------------*/

    getUserInfo(){
        this.props.dispatch(PassportAction.queryUserInfo());
    }

    getLastLogin(){
        this.props.dispatch(PassportAction.getLastLoginLog());
    }

    /* ---------------------------- Events ------------------------------*/

    /* ---------------------------- Renders ------------------------------*/

    render(){
        const userinfo = this.props.userinfo.data;
        const balance = userinfo && userinfo.Summary;
        const loginInfo = this.props.lastlogin;
        return (
            <DashContainer title="个人信息">
                <div className="profile">
                    <div className="profile-info">
                        <span className="profile-info-avatar"></span>
                        <p>{userinfo&&userinfo.Email}</p>
                        <dl>
                          <dt><Icon type="mobile" />手机</dt>
                          <dd>{userinfo&&userinfo.Mobile||"未绑定"}</dd>
                          <dt><i className="iconfont icon-qq"></i>QQ</dt>
                          <dd>{userinfo&&userinfo.QQ||"未绑定"}</dd>
                          <dt><Icon type="file-text" />注册时间</dt>
                          <dd>{userinfo&&RenderHelper.render_time(userinfo.CreateTime)}</dd>
                        </dl>
                    </div>
                    
                    <div className="profile-summary fn-clear">
                        <Row>
                            <Col span={6}>
                                <h3>{balance&&RenderHelper.money_format(balance.Deposit)}</h3>
                                <p>总充值</p>
                            </Col>
                            <Col span={6}>
                                <h3>{balance&&RenderHelper.money_format(balance.Withdraw)}</h3>
                                <p>总提现</p>
                            </Col>
                            <Col span={6}>
                                <Row>
                                    <Col span={6}>总投注</Col> 
                                    <Col span={6}><b>￥{balance&&RenderHelper.money_format(balance.ValidBet)}</b></Col>
                                </Row>
                                <Row>
                                    <Col span={6}>总佣金</Col>
                                    <Col span={6}><b>￥{balance&&RenderHelper.money_format(balance.Commission)}</b></Col>
                                </Row>
                            </Col>
                            <Col span={6}>
                                <Row>
                                    <Col span={6}>总盈亏</Col>
                                    <Col span={6}><b>￥{balance&&RenderHelper.money_format(balance.Profit)}</b></Col>
                                </Row>
                                <Row>
                                    <Col span={6}>总红利</Col>
                                    <Col span={6}><b>￥{balance&&RenderHelper.money_format(balance.Bonus)}</b></Col>
                                </Row>
                            </Col>
                        </Row>
                    </div>

                    <div className="profile-lastlogin">
                        <ul>
                            <li>
                                <label>上次登陆时间:</label>
                                {loginInfo&&RenderHelper.render_time(loginInfo.CreateTime)}
                            </li>
                            <li>
                                <label>上次登陆地区:</label>
                                {loginInfo&&loginInfo.Area}
                            </li>
                            <li>
                                <label>上次登陆IP:</label>
                                {loginInfo&&loginInfo.IP}
                            </li>
                        </ul>
                    </div>
                    
                </div> 
            </DashContainer>
        );
    }
}

module.exports = connect(state => ({
    userinfo: state.passport.userinfo,
    lastlogin: state.passport.lastlogin,
}))(Profile);
