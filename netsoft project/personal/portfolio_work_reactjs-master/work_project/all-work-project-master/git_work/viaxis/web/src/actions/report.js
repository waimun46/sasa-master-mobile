import $ from "jquery";
import { ActionState } from "../app_constants";
import { POST, GET } from "../utils/common";

/* ------------------------- ProfitUser ------------------------- */

/**
 * 获取用户盈亏报表
 * @param {int} userId 用户Id
 * @param  {[type]} query    [查询条件]
 * @return {[type]}          [description]
 */
export function queryProfitUser(userId, querys) {
    var queryStr = JSON.stringify(querys);
    var url = userId ? `/api/profit/user/${userId}` : `/api/profit/user`;
    return {
        name: "ProfitUser",
        callAPI: () => POST(url, queryStr)()
    }
}

/**
 * 获取游戏分类盈亏报表
 * @param  {[type]} userId [description]
 * @param  {[type]} querys [description]
 * @return {[type]}        [description]
 */
export function queryProfitGameType(userId, querys) {
    var queryStr = JSON.stringify(querys);
    var url = userId ? `/api/profit/gametype/${userId}` : `/api/profit/gametype`;
    return {
        name: "ProfitGameType",
        callAPI: () => POST(url, queryStr)()
    }
}

/**
 * 获取用户的游戏分类盈亏报表
 * @param  {[type]} userId [description]
 * @param  {[type]} querys [description]
 * @return {[type]}        [description]
 */
export function queryProfitGameTypeByUser(userId, querys) {
    var queryStr = JSON.stringify(querys);
    var url = userId ? `/api/profit/gametype/user/${userId}` : `/api/profit/gametype/user`;
    return {
        name: "ProfitUserGameType",
        callAPI: () => POST(url, queryStr)()
    }
}

export function queryProfitGameTypeGroup(querys) {
    var queryStr = JSON.stringify(querys);
    return {
        name: "ProfitGroupGameType",
        callAPI: () => POST(`/api/profit/gametype/group`, queryStr)()
    }
}

/**
 * 获取分红报表
 * @param  {[type]} userId [description]
 * @param  {[type]} querys [description]
 * @return {[type]}        [description]
 */
export function queryProfitDividend(userId, querys) {
    var queryStr = JSON.stringify(querys);
    var url = userId ? `/api/profit/dividend/${userId}` : `/api/profit/dividend`;
    return {
        name: "ProfitDividend",
        callAPI: () => POST(url, queryStr)()
    }
}
