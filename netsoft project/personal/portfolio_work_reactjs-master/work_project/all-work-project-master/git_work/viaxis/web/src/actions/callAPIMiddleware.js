import Auth from "../auth";
import {ActionState} from "../app_constants";
/**
 * 调用复合型 Action。
 * dispatch({name:str,callAPI:Function,shouldCallAPI:Func<bool>,payload:dict})
 * name : 表示
 * @param  {[type]} options.dispatch [description]
 * @param  {[type]} options.getState [description]
 * @return {[type]}                  [description]
 */
export default function callAPIMiddleware({dispatch, getState}){
	return function(next) {
		return function(action) {
			const { name, callAPI, shouldCallAPI = () => true, payload = {}} = action;

			if (!name) {
				//普通 action
				return next(action);
			}

			if (!typeof name === "string" || name === "" || name === null || name === undefined) {
				throw new Error("Expected name to be a not null string.");
			}

			if (typeof callAPI !== "function") {
				throw new Error("Expected fetch to be a function.");
			}

			if (!shouldCallAPI(getState())) {
				return;
			}


			dispatch(Object.assign({}, payload, {
				type: name,
				state: ActionState.Doing
			}));

			return callAPI().then(response =>{
					if(response.ok){
						return response.json().then(json => {
							if (json.code == 401 || json.code == 407) {
								Auth.logout();
							} else if (json.code == 403) {
								// auth.forbidden()
								return dispatch(Object.assign({}, payload, {
									type: name,
									error: "没有权限",
									state: ActionState.Success
								}))
							}else{
								return dispatch(Object.assign({}, payload, {
									type: name,
									state: ActionState.Success,
									response: json
								}))
							}
						})
					} else if (response.status == 401) {
						Auth.logout();
					} else {
						return dispatch(Object.assign({}, payload, {
							type: name,
							error: response.statusText,
							state: ActionState.Failure
						}));
					}
				})
				.catch(error => dispatch(Object.assign({}, payload, {
						type: name,
						state: ActionState.Failure,
						error: error
					}))
				);
		}
	}
}
