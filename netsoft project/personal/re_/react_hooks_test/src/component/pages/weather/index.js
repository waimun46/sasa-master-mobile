import React, { Component, useState, useEffect, useRef } from "react";
import { Card, Icon, Avatar, Row, Col, Input, Button, } from 'antd';
import './index.css';
import cloud from '../../../assets/images/cloud.jpg';
import rain from '../../../assets/images/rain3.jpeg';
import clear from '../../../assets/images/clear.jpg';
import mists from '../../../assets/images/mist.jpeg';

const { Meta } = Card;


// class WeatherApp extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       todos: [],
//       temperature: 0,
//       weatherCondition: null,
//       catcherr: null,
//     }
//   }


//   componentDidMount() {
//     this.fetchWeather();
//   }


// /************************* Fetch Weather in Api *************************/
// fetchWeather(city = this.newText.value, API_KEY = '7a522fdd67758b65b2eb6872f9f08d45') {
//   fetch(`https://api.openweathermap.org/data/2.5/weather?q=${city}&APPID=${API_KEY}&units=metric`)
//     .then((response) => {
//       //console.log(response, 'response----------');
//       if (!response.ok) throw new Error(response.status);
//       else return response.json();
//     })
//     .then(json => {
//       //alert(JSON.stringify(json));
//       //console.log(JSON.stringify(json));
//       this.setState({
//         temperature: json.main.temp.toFixed(0),
//         weatherCondition: json.weather[0].main,
//         locationName: json.name,
//         isLoading: false,
//         data: json
//       }, () => this.setState({
//         todos: this.state.todos.concat({
//           temValue: json.main.temp.toFixed(0),
//           cityValue: this.newText.value,
//           condition: json.weather[0].main
//         })
//       }))
//     })
//     .catch((error) => {
//       //console.log(error, 'response----------error');
//       this.setState({
//         catcherr: error.message
//       });
//     });

// }

// addWeather = (e) => {
//   e.preventDefault();
//   let todos = [...this.state.todos];
//   this.setState({
//     todos,
//   }, () => this.fetchWeather());
// }

// removeWeather = index => () => {
//   console.log('removeWeather----')
//   this.setState({
//     todos: this.state.todos.filter((s, sidx) => index !== sidx)
//   })
// }

// render() {
//   const { todos } = this.state;
//   console.log('todos-----', todos)
//   console.log('temData', this.state.temData)


//   return (
//     <div className="weather">
//       <Row className={todos.length === 0 ? null : "weather_card_warp"} >
//         {
//           todos.map((item, index) => {
//             console.log('item', item.name);
//             return (
//               <Col className="card_warpper" key={index}>
//                 <Card
//                   title={item.cityValue} extra={
//                     // <Button type="primary" >remove</Button>
//                     <Icon type="close" onClick={this.removeWeather(index)} className="icon_sty" />
//                   }
//                   className="weather_card"
//                   cover={
//                     <img
//                       alt="example"
//                       src={
//                         item.condition === 'Rain' ? rain :
//                           item.condition === 'Clouds' ? cloud :
//                             item.condition === 'Clear' ? clear :
//                               item.condition === 'Mist' ? mists : null
//                       }
//                       className="image_weather"
//                     />
//                   }

//                 >
//                   <Meta title={
//                     <Row >
//                       <Col span={12} >{item.temValue}°</Col>
//                       <Col span={12} className="status">{item.condition}</Col>
//                     </Row>
//                   } />
//                 </Card>
//               </Col>
//             )
//           })
//         }
//       </Row>
//       <Row gutter={[8, 16]} className="enter_city">
//         <Col span={12} offset={6} align="center" className="enter">Enter City</Col>
//         <Col span={12} offset={6}>
//           <Col span={24}>
//             <form onSubmit={this.addWeather}>
//               <input type="text" ref={(ip) => { this.newText = ip }} />
//             </form>
//           </Col>
//           {/* <Col span={6} >
//       <Button type="primary" onClick={this.addWeather.bind(this)}>Search</Button>
//     </Col> */}

//         </Col>
//       </Row>
//     </div>
//   );
// }
// }




const WeatherApp = () => {
  const [weatherElement, setWeatherElement] = useState({
    catcherr: null,
    forecast: null,
    APP_ID: '7a522fdd67758b65b2eb6872f9f08d45',
    noData: null
  });
  const [cityNameValue, setCityNameValue] = useState('');
  const [APP_ID, setAPP_ID] = useState('7a522fdd67758b65b2eb6872f9f08d45');
  const [tempValue, setTempValue] = useState('');
  const [conditionValue, setConditionValue] = useState('');
  const [todos, setTodos] = useState([]);



  useEffect(() => {
    const fetchData = async () => {
      const [currentWeather] = await Promise.all([
        fetchWeather(),
      ]);
      setWeatherElement({
        ...currentWeather,
      });
    };

    fetchData();
  }, []);


  const fetchWeather = () => {
    return fetch(
      `https://api.openweathermap.org/data/2.5/weather?q=${cityNameValue}&APPID=${APP_ID}&units=metric`,
    )
      .then(response => response.json())
      .then(data => {
        console.log('data', data);
        setTempValue(data.main.temp.toFixed(0));
        setConditionValue(data.weather[0].main);
      })
      .catch((error) => {
        console.log(error, 'response----------error');
        return {
          catcherr: error.message
        };
      });
  };

  const addWeather = (e) => {
    e.preventDefault();
    setTodos([...todos, {
      value: cityNameValue,
      tem: tempValue,
      condition: conditionValue
    }], fetchWeather());
  };

  const removeWeather = index => () => {
    console.log('removeWeather----')
    setTodos(todos.filter((s, sidx) => index !== sidx));
  }


  return (
    console.log('cityName', weatherElement.cityName),
    console.log('temperature----return', tempValue),
    console.log('todos-----', todos),


    <div className="weather">
      <Row className="weather_card_warp">
        {
          todos.map((item, index) => {
            console.log('item', item.name);
            return (
              <Col className="card_warpper" key={index}>
                <Card
                  title={item.value} extra={
                    <Icon type="close" onClick={removeWeather(index)} className="icon_sty" />
                  }
                  className="weather_card"
                  cover={
                    <img
                      alt="example"
                      src={
                        item.condition === 'Rain' ? rain :
                          item.condition === 'Clouds' ? cloud :
                            item.condition === 'Clear' ? clear :
                              item.condition === 'Mist' ? mists : null
                      }
                      className="image_weather"
                    />
                  }

                >
                  <Meta title={
                    <Row >
                      <Col span={12} >{item.tem}°</Col>
                      <Col span={12} className="status">{item.condition}</Col>
                    </Row>
                  } />
                </Card>
              </Col>
            )
          })
        }
      </Row>
      <Row gutter={[8, 16]} className="enter_city">
        <Col span={12} offset={6} align="center" className="enter">Enter City</Col>
        <Col span={12} offset={6}>
          <Col span={24}>
            <form onSubmit={addWeather}>
              <Input type="text"
                value={cityNameValue}
                onChange={(event) => {
                  setCityNameValue(event.target.value);
                }} />
            </form>
          </Col>
        </Col>
      </Row>
    </div>


  );
};

export default WeatherApp;
