import React, { Component } from 'react';
import  WeatherApp  from './component/pages/weather';
import './App.css';


function App() {
  return (
    <div className="App">

<div className="overlay"></div>
      <WeatherApp/>
      
    
    </div>
  );
}
export default App;