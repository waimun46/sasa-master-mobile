import React, { useState, useEffect, useRef } from 'react';
import { View, TextInput, StyleSheet } from 'react-native';
import { Button, Text } from 'native-base';



function LoginScreen({ navigation }) {

  /**************************** SET STATE ***************************/
  const [security, setSecurity] = useState('90af0fc8532a32f41b69d7097f2f1ca6');
  const [phone, setPhone] = useState('');
  const [password, setPassword] = useState('');


  /**************************** SUBMIT FUNCTION ***************************/
  const SubmitLogin = () => {

    /************** OUTPUT JSON **************/
    const loginDetails = {
      'Phone': phone,
      'Password': password
    }
    console.log(loginDetails);

    /************** POST API AND SET PARAMS **************/
    let url = `http://apps.idealtech.com.my/api/member_login.php?security_code=${security}&contact=${phone}&password=${password}`;
    console.log('url-----', url);

    fetch(url).then((res) => res.json())
      .then((resData) => {
        if (resData[0].status === 1) {
          navigation.navigate('Home')
          // navigation.navigate('Home', { MMID: resData[0].MMID })
          // alert(resData[0].MMID)
        } else {
          alert(resData[0].error)
        }
      })
  }


  return (
    <View style={styles.container}>
      <View style={styles.warpper}>
        <Text> LoginScreen </Text>
        <TextInput
          placeholder='Phone Number'
          onChangeText={text => setPhone(text)}
          defaultValue={phone}
          style={styles.inputSty}
        />
        <TextInput
          placeholder='Password'
          onChangeText={text => setPassword(text)}
          defaultValue={password}
          style={styles.inputSty}
        />
        <Button block info onPress={SubmitLogin} style={styles.btnSty}>
          <Text>Info</Text>
        </Button>
        {/* <Button block info onPress={() => navigation.navigate('Home')} style={styles.btnSty}>
          <Text>home</Text>
        </Button> */}
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#fff', justifyContent: 'center', alignItems: 'center', },
  inputSty: { padding: 10, borderBottomWidth: .5, width: '100%', borderBottomColor: '#ccc', marginTop: 20 },
  btnSty: { marginTop: 50, },
  warpper: { paddingLeft: 20, paddingRight: 20, width: '100%', alignItems: 'center', }
})

export default LoginScreen



