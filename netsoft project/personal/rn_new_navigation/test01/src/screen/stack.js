import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Text, TouchableOpacity } from 'react-native';

import HomeScreen from '../components/home';
import LoginScreen from '../components/login';
import PromotionScreen from '../components/promotion';
import AboutUsScreen from '../components/about';

//////////////////////////// createStackNavigator ///////////////////////////
const HomeStack = createStackNavigator();
const PromotionStack = createStackNavigator();
const AboutUsStack = createStackNavigator();


//////////////////////////// HomeStackScreen ///////////////////////////
export const HomeStackScreen = ({ navigation }) => {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen
        name="Home"
        component={HomeScreen}
        options={{
          // headerTitle: 'Home',
          headerLeft: () => (
            <TouchableOpacity onPress={() => navigation.toggleDrawer()}>
              <Text style={{ color: '#000', marginLeft: 10 }}>menu</Text>
            </TouchableOpacity>
          ),
          headerRight: () => (
            <TouchableOpacity onPress={() => navigation.navigate('Login')}>
              <Text style={{ color: '#000', marginRight: 10 }}>login</Text>
            </TouchableOpacity>
          )
        }}
      />
      <HomeStack.Screen
        name="Login"
        component={LoginScreen}
        options={{
          headerBackTitle: '  '
        }}
      />
    </HomeStack.Navigator>
  )
}

//////////////////////////// PromotionStackScreen ///////////////////////////
export const PromotionStackScreen = () => {
  return (
    <PromotionStack.Navigator>
      <PromotionStack.Screen
        name="Promotios"
        component={PromotionScreen}
        options={{
          headerLeft: () => (
            <TouchableOpacity onPress={() => navigation.toggleDrawer()}>
              <Text style={{ color: '#000', marginLeft: 10 }}>menu</Text>
            </TouchableOpacity>
          ),
          headerRight: () => (
            <TouchableOpacity onPress={() => navigation.navigate('Login')}>
              <Text style={{ color: '#000', marginRight: 10 }}>login</Text>
            </TouchableOpacity>
          )
        }}
      />
    </PromotionStack.Navigator>
  )
}

//////////////////////////// AboutUsStackScreen ///////////////////////////
export const AboutUsStackScreen = ({ navigation }) => {
  return (
    <AboutUsStack.Navigator>
      <AboutUsStack.Screen
        name="AboutUs"
        component={AboutUsScreen}
        options={{
          headerLeft: () => (
            <TouchableOpacity onPress={() => navigation.toggleDrawer()}>
              <Text style={{ color: '#000', marginLeft: 10 }}>menu</Text>
            </TouchableOpacity>
          ),
          headerRight: () => (
            <TouchableOpacity onPress={() => navigation.navigate('Login')}>
              <Text style={{ color: '#000', marginRight: 10 }}>login</Text>
            </TouchableOpacity>
          )
        }}
      />
    </AboutUsStack.Navigator>
  )
}


