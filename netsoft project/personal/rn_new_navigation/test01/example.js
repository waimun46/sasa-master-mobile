/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TouchableOpacity
} from 'react-native';
import { Icon, } from 'native-base';


import HomeScreen from './src/components/home';
import LoginScreen from './src/components/login';


// const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
const Tab = createBottomTabNavigator();

const loginStack = createStackNavigator();
const HomeStack = createStackNavigator();


const HomeStackScreen = ({navigation}) => {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen name="Home" component={HomeScreen}
        options={{
          // headerTitle: 'Home',
          headerLeft: () => (
            <TouchableOpacity onPress={() => navigation.toggleDrawer()}>
              <Text style={{ color: '#000', paddingLeft: 10  }}>menu</Text>
            </TouchableOpacity>
          ),
        }}
      />
      {/* <HomeStack.Screen name="Login" component={LoginScreen} /> */}
    </HomeStack.Navigator>
  )

}

const LoginStackScreen = ({navigation}) => {
  return (
    <loginStack.Navigator>
      <loginStack.Screen name="Login" component={LoginScreen}
        options={{
          headerLeft: () => (
            <TouchableOpacity onPress={() => navigation.toggleDrawer()}>
              <Text style={{ color: '#000', paddingLeft: 10 }}>menu</Text>
            </TouchableOpacity>
          ),
        }}
      />
      {/* <loginStack.Screen name="Home" component={HomeScreen} /> */}
    </loginStack.Navigator>
  )

}

const Tabs = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Login" component={LoginStackScreen} />
      <Tab.Screen name="Home" component={HomeStackScreen} />
    </Tab.Navigator>
  )
}


const App: () => React$Node = () => {
  return (
    <NavigationContainer>
      <StatusBar barStyle="dark-content" />

      <Drawer.Navigator>
        <Drawer.Screen name="Login" component={Tabs} />
        <Drawer.Screen name="Home" component={HomeStackScreen} />
      </Drawer.Navigator>

    </NavigationContainer>
  );
};

const styles = StyleSheet.create({

});

export default App;
