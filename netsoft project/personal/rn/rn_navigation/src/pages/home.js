import React, { Component } from 'react';
import { StyleSheet, Text, View,Button } from 'react-native';
import ANT from 'react-native-vector-icons/AntDesign';

class HomeScreen extends Component {


  render() {
    return (
      <View style={styles.container}>
        <Text style={{color: '#000'}}>HomeScreen</Text>
        <Button title="Search" onPress={() => this.props.navigation.navigate('Search')} />
      </View>
    )
  }

}





const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default HomeScreen;