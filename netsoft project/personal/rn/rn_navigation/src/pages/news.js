import React, { Component } from 'react';
import { StyleSheet, Text, View,Button } from 'react-native';


class NewsScreen extends Component {
  static navigationOptions = {
    title: 'News',
  };

  render() {
    return (
      <View style={styles.container}>
        <Text style={{color: '#000'}}>HomeScreen</Text>
        <Button title="blog" onPress={() => this.props.navigation.navigate('Blog')} />
      </View>
    )
  }

}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default NewsScreen;