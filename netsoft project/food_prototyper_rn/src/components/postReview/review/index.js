import React, { Component } from 'react';
import { View, StyleSheet, Image, TouchableOpacity, ScrollView, FlatList } from 'react-native';
import { Button, Text, Card, CardItem, Item, List, ListItem, Left, Body, Right, Thumbnail, } from 'native-base';
import ANT from 'react-native-vector-icons/AntDesign';
import MAT from 'react-native-vector-icons/MaterialIcons';

const data = [
  {
    photo: 'https://images.unsplash.com/photo-1497316730643-415fac54a2af?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80',
    name: 'John', date: '11-03-2020', content: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry Lorem Ipsum has been the industry'
  },
  {
    photo: 'https://images.unsplash.com/photo-1497316730643-415fac54a2af?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80',
    name: 'John', date: '11-03-2020', content: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry Lorem Ipsum has been the industry'
  },
  {
    photo: 'https://images.unsplash.com/photo-1497316730643-415fac54a2af?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80',
    name: 'John', date: '11-03-2020', content: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry Lorem Ipsum has been the industry'
  },
  {
    photo: 'https://images.unsplash.com/photo-1497316730643-415fac54a2af?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80',
    name: 'John', date: '11-03-2020', content: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry Lorem Ipsum has been the industry'
  },
  {
    photo: 'https://images.unsplash.com/photo-1497316730643-415fac54a2af?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80',
    name: 'John', date: '11-03-2020', content: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry Lorem Ipsum has been the industry'
  },
  
]

class ReviewScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  _renderReview = ({ item, index }) => {
    return (
      <Card style={{ marginTop: 10 }}>
        <View style={{ padding: 10 }}>
          <List >
            <ListItem avatar style={{ marginLeft: 0, }}>
              <Body style={{ borderBottomWidth: 0, marginLeft: 0, marginRight: 0 }}>
                <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                  <Thumbnail source={{ uri: item.photo }} />
                  <View style={{ marginLeft: 10 }}>
                    <Text style={{ marginBottom: 5 }}>{item.name}</Text>
                    <Text note style={{ color: '#000', textAlign: 'right' }}>{item.date}</Text>
                  </View>
                </View>
                <View style={{ marginTop: 10,  }}>
                  <Text note style={{ marginRight: 0 }}>{item.content}</Text>
                </View>
              </Body>
            </ListItem>
          </List>

        </View>
      </Card>
    )
  }

  render() {
    // const review_list = this.props.navigation.state.params.data
    // console.log('data-review', this.props.navigation.state.params.data)
    return (
      <View style={styles.container}>
        <ScrollView>

          <View style={{ position: 'relative' , marginBottom: 10}}>
            <Image source={{ uri: 'https://cdn.nohat.cc/thumb/f/720/0fec843cd5c84a2d9514.jpg' }} style={styles.imgSty} />
          </View>

          <View style={styles.title}>
            <Text note style={{textAlign: 'right'}}>100 Comment</Text>
          </View>

          <FlatList
            data={data}
            renderItem={this._renderReview}
            keyExtractor={(item, index) => index.toString()}
            contentContainerStyle={{ paddingBottom: 50 }}
          />

        </ScrollView>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, },
  imgSty: { width: '100%', height: 250 },
  title: { marginTop: 5, paddingRight: 10, paddingLeft: 10  },

});


export default ReviewScreen;
