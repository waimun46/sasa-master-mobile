import React, { Component } from 'react';
import { View, ScrollView, FlatList, TouchableOpacity, Image, Modal, StyleSheet, } from 'react-native';
import { Card, CardItem, Text, } from 'native-base';
import { WebView } from 'react-native-webview';
import ANT from 'react-native-vector-icons/AntDesign';



const dataVideo = [
  {
    url: 'https://fishmarketrestaurantkk.com/gamtong/image/catalog/web/video/video08.mp4',
    img: require('../../assest/video/01.jpg'),
  },
  {
    url: 'https://fishmarketrestaurantkk.com/gamtong/image/catalog/web/video/video07.mp4',
    img: require('../../assest/video/02.jpg'),
  },
  {
    url: 'http://fishmarketrestaurantkk.com/gamtong/image/catalog/web/video/video06.mp4',
    img: require('../../assest/video/03.jpg'),
  },
  {
    url: 'https://fishmarketrestaurantkk.com/gamtong/image/catalog/web/video/video05.mp4',
    img: require('../../assest/video/04.jpg'),
  },
  {
    url: 'https://fishmarketrestaurantkk.com/gamtong/image/catalog/web/video/video04.mp4',
    img: require('../../assest/video/05.jpg'),
  },
  {
    url: 'https://fishmarketrestaurantkk.com/gamtong/image/catalog/web/video/video03.mp4',
    img: require('../../assest/video/06.jpg'),
  },
  {
    url: 'https://fishmarketrestaurantkk.com/gamtong/image/catalog/web/video/video02.mp4',
    img: require('../../assest/video/07.jpg'),
  },
  {
    url: 'https://fishmarketrestaurantkk.com/gamtong/image/catalog/web/video/video01.mp4',
    img: require('../../assest/video/08.jpg'),
  },
] 

class VideoGallery extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      modalData: { video: '' }
    };
  }

  _renderVideo = ({ item }) => {
    return (
      <Card style={{ marginBottom: 15 }}>
        <TouchableOpacity onPress={() => this.openModal(item.url)}>
          <Image source={item.img} style={{ width: '100%', height: 200 }} />
        </TouchableOpacity>
      </Card>
    )
  }

  openModal(url) {
    const modalData = dataVideo.find(element => {
      return element.url === url
    })
    this.setState({
      modalVisible: true,
      modalData: { video: modalData.url }
    });
  };

  /***************************************** closeModal ****************************************/
  closeModal = () => {
    console.log('openImage')
    this.setState({
      modalVisible: false
    })
  }



  render() {
    const { modalVisible, modalData } = this.state;
    console.log('modalData', modalData.video)
    return (
      <View>
        <FlatList
          data={dataVideo}
          contentContainerStyle={{ padding: 10 }}
          renderItem={this._renderVideo}
          keyExtractor={(item, index) => index.toString()}
        />
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <View style={styles.imgModalWarp}>
            <TouchableOpacity onPress={this.closeModal} style={styles.btnClose}>
              <ANT name="close" size={30} style={{ color: '#fff', }} />
            </TouchableOpacity>
            <View style={{ width: '100%', height: 300, backgroundColor: '#000' }}>

              <WebView
                javaScriptEnabled={true}
                domStorageEnabled={true}
                source={{ uri: modalData.video }}
              />
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { backgroundColor: '#fff', flex: 1 },
  imgModalWarp: { flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: '#000000c9', position: 'relative' },
  btnClose: { alignItems: 'flex-end', position: 'absolute', top: 30, right: 20, width: '50%' },

})

export default VideoGallery;
