import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { Card, CardItem, Text, Body } from 'native-base';



class InboxContent extends Component {
  static navigationOptions = {
    title: 'Your Mail',
  };
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    console.log('props', this.props)
    console.log('getData', this.props.navigation.state.params.data)
    const mail = this.props.navigation.state.params.data
    return (
      <View style={styles.container}>
        <View style={{padding: 10}}>
          <Card>
            <CardItem header bordered>
              <Text>{mail.name}</Text>
            </CardItem>
            <CardItem bordered> 
              <Body>
                <Text>{mail.description}</Text>
              </Body>
            </CardItem>
            <CardItem footer>
              <Text>{mail.time}</Text>
            </CardItem>
          </Card>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { backgroundColor: '#fff', flex: 1 },
})

export default InboxContent;
