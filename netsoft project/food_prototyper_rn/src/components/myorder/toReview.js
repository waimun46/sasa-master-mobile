import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, TouchableOpacity, FlatList, Image } from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Left, Body, Right } from 'native-base';
import ANT from 'react-native-vector-icons/AntDesign';
import logo from '../../assest/logo/logo2.png';


const data = [
  {
    item: 'PRODUCT 1', name: 'This is my products.', price: '40.00', date: '22-03-2020', like: '367',
    img: 'https://cdn.nohat.cc/thumb/f/720/0fec843cd5c84a2d9514.jpg', comment: '78'
  },
  {
    item: 'PRODUCT 2', name: 'This is my products.', price: '20.00', date: '23-03-2020', like: '235',
    img: 'https://cdn.nohat.cc/thumb/f/720/0fec843cd5c84a2d9514.jpg', comment: '66'
  },
  {
    item: 'PRODUCT 3', name: 'This is my products.', price: '10.00', date: '24-03-2020', like: '112',
    img: 'https://cdn.nohat.cc/thumb/f/720/0fec843cd5c84a2d9514.jpg', comment: '33'
  },
  {
    item: 'PRODUCT 4', name: 'This is my products.', price: '40.00', date: '25-03-2020', like: '465',
    img: 'https://cdn.nohat.cc/thumb/f/720/0fec843cd5c84a2d9514.jpg', comment: '20'
  },
  {
    item: 'PRODUCT 5', name: 'This is my products.', price: '20.00', date: '30-03-2020', like: '213',
    img: 'https://cdn.nohat.cc/thumb/f/720/0fec843cd5c84a2d9514.jpg', comment: '50'
  },
  {
    item: 'PRODUCT 6', name: 'This is my products.', price: '10.00', date: '01-03-2020', like: '500',
    img: 'https://cdn.nohat.cc/thumb/f/720/0fec843cd5c84a2d9514.jpg', comment: '30'
  },
]


class ToReviewScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  _renderReview = ({ item, index }) => {
    const navigateActions = this.props.navigation.navigate;
    return (
      <TouchableOpacity  onPress={() => navigateActions('review', {data: item})}>
        <Card style={{ marginTop: 10 }}>
          <CardItem>
            <Left>
              <Thumbnail source={logo} />
              <Body>
                <Text>{item.item}</Text>
                <Text note>{item.date}</Text>
              </Body>
            </Left>
          </CardItem>
          <CardItem cardBody>
            <Image source={{ uri: item.img }} style={{ height: 200, width: null, flex: 1 }} />
          </CardItem>
          <CardItem >
            <View style={styles.commentStatus}>
              <View style={{ flexDirection: 'row', }}>
                <ANT name="heart" color="#f48120" size={15} style={{ paddingRight: 5 }} />
                <Text note style={{ color: '#000' }}>{item.like} Like</Text>
              </View>
              <View style={{ flexDirection: 'row', }}>
                <ANT name="message1" color="#f48120" size={15} style={{ paddingRight: 5 }} />
                <Text note style={{ color: '#000' }}>{item.comment} Comments</Text>
              </View>
            </View>
          </CardItem>
        </Card>
      </TouchableOpacity>
    )
  }


  render() {
    return (
      <View style={styles.container}>
        <ScrollView>

          <FlatList
            data={data}
            renderItem={this._renderReview}
            keyExtractor={(item, index) => index.toString()}
            contentContainerStyle={{ paddingBottom: 50 }}
          />

        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, },
  title: { marginTop: 15, paddingBottom: 10, paddingLeft: 10, paddingRight: 10 },
  commentStatus: { flexDirection: 'row', justifyContent: 'space-between', width: '100%', paddingBottom: 5, paddingTop: 5, },

})

export default ToReviewScreen;
