import React, { Component } from 'react';
import { View, Image, StyleSheet, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import { ListItem, CheckBox, Text, Body, Right, Item, Input, Icon, Button, Label } from 'native-base';
import mylogo from '../../../assest/maybank2.png'
import visaLogo from '../../../assest/c3.png';
import ANT from 'react-native-vector-icons/AntDesign';
import FOT from 'react-native-vector-icons/Fontisto';







class MemberPayments extends Component {
  constructor(props) {
    super(props);
    this.state = {
      number: '',
      expiry_date: '',
      cvv: '',
      amount: ''
    };
  }

  toggleMaybank() {
    this.setState({ maybank: true, creadit: false })
  }

  toggleCredit() {
    this.setState({ creadit: true, maybank: false })
  }


  onChangeTextInput(text, field) {
    //console.log(text)
    if (field === 'amount') { this.setState({ amount: text }) }
    if (field === 'number') { this.setState({ number: text }) }
    if (field === 'expiry_date') { this.setState({ expiry_date: text }) }
    if (field === 'cvv') { this.setState({ cvv: text }) }
  }



  render() {
    const { number, expiry_date, cvv, amount } = this.state;
    const navigateActions = this.props.navigation.navigate;


    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={{ width: '100%', alignItems: 'center' }}>
            <View style={{ width: 200, height: 80, marginTop: 20 }}>
              <Image source={mylogo} style={{ width: '100%', height: '100%', resizeMode: 'contain' }} />
            </View>
          </View>

          <View style={{ alignItems: 'center', marginTop: 20 }}>
            <View style={{ width: '85%', }}>
              <View style={{ marginBottom: 20, alignItems: 'center' }}>
                <Text note style={{ marginBottom: 10, textAlign: 'center' }}>Payment Amount</Text>
                <View style={{
                  borderWidth: .5, borderRadius: 5,
                  borderColor: '#ccc', padding: 14, width: '50%',
                }}>
                  <View style={{ width: '100%', flexDirection: 'row', }}>
                    <Text style={{ fontSize: 20, fontWeight: 'bold' }}>RM</Text>
                    <TextInput
                      value={amount}
                      placeholder=""
                      placeholderTextColor="#000"
                      onChangeText={(text) => this.onChangeTextInput(text, 'amount')}
                      autoCorrect={false}
                      autoCapitalize="none"
                      //secureTextEntry={true}
                      keyboardType={'numeric'}
                      style={{ fontSize: 20, paddingLeft: 25, paddingRight: 35, width: '100%', fontWeight: 'bold' }}
                    />
                  </View>
                </View>
              </View>

              <View style={{marginTop: 10}}>
                <Text note>Card Number</Text>
                <Item >
                  <Input
                    value={number}
                    placeholder="0000 0000 0000 0000"
                    placeholderTextColor="#000"
                    onChangeText={(text) => this.onChangeTextInput(text, 'number')}
                    autoCorrect={false}
                    autoCapitalize="none"
                    //secureTextEntry={true}
                    keyboardType={'numeric'}
                    style={{ fontSize: 15 }}

                  />
                  <FOT name="mastercard" size={20} style={styles.iconsty} />
                </Item>
              </View>

              <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 30 }}>
                <View style={{ width: '50%', paddingRight: 15 }}>
                  <View>
                    <Text note>Expiry Date</Text>
                    <Item >
                      <Input
                        value={expiry_date}
                        placeholder="00/00"
                        placeholderTextColor="#000"
                        onChangeText={(text) => this.onChangeTextInput(text, 'expiry_date')}
                        autoCorrect={false}
                        autoCapitalize="none"
                        //secureTextEntry={true}
                        keyboardType={'numeric'}
                        style={{ fontSize: 15 }}
                      />
                      <FOT name="calendar" size={20} style={styles.iconsty} />
                    </Item>
                  </View>
                </View>
                <View style={{ width: '50%', paddingLeft: 15 }}>
                  <View>
                    <Text note>CVV</Text>
                    <Item >
                      <Input
                        value={cvv}
                        placeholder="***"
                        placeholderTextColor="#000"
                        onChangeText={(text) => this.onChangeTextInput(text, 'cvv')}
                        autoCorrect={false}
                        autoCapitalize="none"
                        //secureTextEntry={true}
                        keyboardType={'numeric'}
                        style={{ fontSize: 15 }}
                      />
                      <FOT name="credit-card" size={18} style={styles.iconsty} />
                    </Item>
                  </View>
                </View>

              </View>
            </View>
          </View>

          <View style={{ padding: 20, marginBottom: 50 }}>
            <Button full dark block onPress={() => navigateActions('payment_status')}
              style={{ borderRadius: 5, marginTop: 30, height: 50 }}>
              <Text>CHECKOUT</Text>
            </Button>
          </View>



          {/* <ListItem >
          <CheckBox checked={maybank} onPress={() => this.toggleMaybank()} />
          <Body style={{borderBottomWidth: 0}}>
            <Text style={{ paddingLeft: 10 }}>MayBank</Text>
          </Body>
          <Right style={{borderBottomWidth: 0}}>
            <Image source={mylogo} style={{width: 50, height:35,  resizeMode: 'contain',  textAlign: 'right'}} />
          </Right>
        </ListItem>
        <ListItem>
          <CheckBox checked={creadit} onPress={() => this.toggleCredit()} />
          <Body style={{borderBottomWidth: 0}}>
            <Text style={{ paddingLeft: 10 }}>VISA / Credit </Text>
          </Body>
          <Right style={{borderBottomWidth: 0}}>
            <Image source={visaLogo} style={{width: 80, height:35, resizeMode: 'contain', textAlign: 'right'}} />
          </Right>
        </ListItem> */}
        </ScrollView>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#fff' },
  iconsty: { color: '#ccc' }

})

export default MemberPayments;
