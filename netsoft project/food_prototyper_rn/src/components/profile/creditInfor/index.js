import React, { Component } from 'react';
import { View, StyleSheet, FlatList, ScrollView, } from 'react-native';
import { List, ListItem, Thumbnail, Text, Left, Body, Right, Button } from 'native-base';

const data = [
  {
    date: '01-04-2020', credit: '200', status: 'IN', description: 'Share referral ID'
  },
  {
    date: '14-04-2020', credit: '100', status: 'OUT', description: 'Use for food'
  },
  {
    date: '20-04-2020', credit: '200', status: 'IN', description: 'Share referral ID'
  },
]


class CreditInfor extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  _renderList = ({ item, index }) => {
    return (
      <List >
        <ListItem style={{ paddingLeft: 0, marginLeft: 5 }} >
          <Body>
            <Text style={{}}>{item.description}</Text>
            <Text numberOfLines={1} style={{ marginTop: 5, }}>Credits : {item.credit}</Text>
            <Text note numberOfLines={1} style={{ marginTop: 5, }}>Date: {item.date}</Text>
          </Body>
          <Right>
            <Button transparent>
              <Text style={{ color: item.status === 'IN' ? 'green' : 'red', fontWeight: 'bold' }}>{item.status}</Text>
            </Button>
          </Right>
        </ListItem>
      </List>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          <FlatList
            data={data}
            renderItem={this._renderList}
            keyExtractor={(item, index) => index.toString()}
            contentContainerStyle={{ paddingBottom: 30, }}
          />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { backgroundColor: '#fff', flex: 1 },


})

export default CreditInfor;
