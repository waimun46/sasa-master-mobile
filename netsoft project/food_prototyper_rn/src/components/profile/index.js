import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, TouchableOpacity, Share } from 'react-native';
import { Content, Text, ListItem, Left, Body, Right, Button, Thumbnail } from 'native-base';
import ETY from 'react-native-vector-icons/Entypo';
import OCT from 'react-native-vector-icons/Octicons';
import ANT from 'react-native-vector-icons/AntDesign';
import FET from 'react-native-vector-icons/Feather';


class ProfilePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  /************************************ sharing function ***********************************/
  sharing() {
    console.log('sharing');
    Share.share(
      {
        message: 'Share your app',
        title: 'Golden Destinations',
      }
    )
  }


  render() {
    const navigateActions = this.props.navigation.navigate;

    return (
      <View style={styles.container}>
        <ScrollView>

          <View style={{ backgroundColor: '#f48120', padding: 10, }}>

            {/********************** Personal info  **********************/}
            <ListItem avatar style={{ borderWidth: 0, marginLeft: 0, }}>
              <Left >
                <Thumbnail source={{ uri: 'https://i.pinimg.com/originals/2e/2f/ac/2e2fac9d4a392456e511345021592dd2.jpg' }} />
              </Left>
              <Body style={{ borderBottomWidth: 0, }}>
                <Text style={{ color: '#fff', fontWeight: 'bold' }}>Kumar Pratik</Text>
                <Text note style={{ color: '#fff', marginTop: 5, }}>Member ID : 1234567</Text>
              </Body>
              <Right style={{ borderBottomWidth: 0, }}>
                <TouchableOpacity onPress={() => navigateActions('edit_acc')}>
                  <ANT name="setting" size={25} style={{ color: '#fff' }} />
                </TouchableOpacity>
              </Right>
            </ListItem>

            <View style={styles.selectContainer}>
              {/********************** G Points  **********************/}
              <TouchableOpacity style={styles.btnSelect} onPress={() => navigateActions('g_points')}>
                <Text style={styles.btnSelectText}>300</Text>
                <Text note style={styles.btnSelectTextColor}>G Points</Text>
              </TouchableOpacity>

              {/********************** Creadts  **********************/}
              <TouchableOpacity onPress={() => navigateActions('credit_wallet')} style={styles.btnSelect}>
                <Text style={styles.btnSelectText}>400</Text>
                <Text note style={styles.btnSelectTextColor}>My Credits</Text>
              </TouchableOpacity>

              {/********************** My Wishlist  **********************/}
              <TouchableOpacity style={[styles.btnSelect, { borderRightWidth: 0 }]} onPress={() => navigateActions('wishlist')}>
                <Text style={styles.btnSelectText}>1</Text>
                <Text note style={styles.btnSelectTextColor}>My Wishlist</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={{ backgroundColor: '#fff', padding: 10 }}>
            {/********************** Purchases  **********************/}
            <View style={styles.purchasesWarp}>
              <Text style={styles.purchasesText}>My Purchases</Text>
              <TouchableOpacity onPress={() => navigateActions('to_receive')}>
                <Text note style={styles.purchasesTextColor}>View Purchases History ></Text>
              </TouchableOpacity>
            </View>

            {/********************** deliver status **********************/}
            <View style={styles.deliverWarp}>
              <TouchableOpacity style={{ alignItems: 'center' }} onPress={() => navigateActions('to_ship')}>
                <FET name="truck" size={30} style={styles.deliverIcon} />
                <Text note style={styles.deliverText}>To Ship</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ alignItems: 'center' }} onPress={() => navigateActions('to_receive')}>
                <OCT name="package" size={30} style={styles.deliverIcon} />
                <Text note style={styles.deliverText}>To Receive</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ alignItems: 'center' }} onPress={() => navigateActions('to_review')}>
                <ANT name="message1" size={30} style={styles.deliverIcon} />
                <Text note style={styles.deliverText}>To Review</Text>
              </TouchableOpacity>
            </View>

            {/* <View style={{ padding: 10, marginTop: 5, backgroundColor: '#f7f7f7' }}>
              <View style={{ marginBottom: 5 }}>
                <Text style={{ color: '#000', }}>Track Package</Text>
              </View>
              <ListItem thumbnail style={{ marginLeft: 0, paddingLeft: 0 }} onPress={() => navigateActions('delivery')}>
                <Left >
                  <Thumbnail square source={{ uri: 'https://cdn.nohat.cc/thumb/f/720/0fec843cd5c84a2d9514.jpg' }} />
                </Left>
                <Body style={{ borderBottomWidth: 0 }}>
                  <Text style={{ color: 'green', marginBottom: 5 }}>Delivered ></Text>
                  <Text note style={{ color: '#000' }}>Doing what you like will always keep you happy . .</Text>
                </Body>
                <Right style={{ borderBottomWidth: 0 }}>
                  <Text note style={{ color: '#55595d' }}>3:43 pm</Text>
                </Right>
              </ListItem>
            </View> */}
          </View>

          {/********************** Share and logout **********************/}
          <View style={{ backgroundColor: '#fff', marginTop: 10 }}>
            <Content >
              {/********************** Share & Earn Credits **********************/}
              <ListItem icon onPress={this.sharing.bind(this)} >
                <Left style={{ justifyContent: 'center' }}>
                  <Button style={{ backgroundColor: '#00BFFF', justifyContent: 'center' }}>
                    <ETY name="share" size={18} style={styles.iconsty} />
                  </Button>
                </Left>
                <Body style={{ justifyContent: 'center' }}>
                  <Text>Share & Earn Credits</Text>
                </Body>
                <Right>
                  <ANT name="right" size={16} style={{ color: '#ccc' }} />
                </Right>
              </ListItem>
              {/********************** LogOut **********************/}
              <ListItem icon onPress={() => navigateActions('home')} >
                <Left style={{ justifyContent: 'center' }}>
                  <Button style={{ backgroundColor: '#DC143C', justifyContent: 'center' }}>
                    <ANT name="logout" size={18} style={styles.iconsty} />
                  </Button>
                </Left>
                <Body style={{ justifyContent: 'center' }}>
                  <Text >LogOut</Text>
                </Body>
                <Right>
                  <ANT name="right" size={16} style={{ color: '#ccc' }} />
                </Right>
              </ListItem>
            </Content>
          </View>

        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, },
  iconsty: { color: '#fff', },
  selectContainer: { flexDirection: 'row', justifyContent: 'space-between', paddingBottom: 20, marginTop: 20 },
  btnSelect: { alignItems: 'center', width: '33.33%', borderRightWidth: .5, borderRightColor: '#ffffff4a', },
  btnSelectText: { color: '#fff', marginBottom: 5, fontWeight: 'bold' },
  btnSelectTextColor: { color: '#ffffffb5', },
  purchasesWarp: { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' },
  purchasesText: { color: '#000', fontWeight: 'bold' },
  purchasesTextColor: { color: '#FF6347', },
  deliverWarp: { flexDirection: 'row', justifyContent: 'space-between', padding: 20, marginTop: 5 },
  deliverIcon: { color: '#6a737d', marginBottom: 5 },
  deliverText: { color: '#6a737d' },
})

export default ProfilePage;
