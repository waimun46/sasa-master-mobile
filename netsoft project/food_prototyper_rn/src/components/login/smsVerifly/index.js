import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, TextInput } from 'react-native';
import { Text, Button } from 'native-base';
import { showMessage, hideMessage } from "react-native-flash-message";
import Countdown from 'react-countdown-now';


class SmsVerifly extends Component {
  constructor(props) {
    super(props);
    this.state = {
      code: '',
      mmidTokenForget: '',
      restartTime: Date.now() + 180000,
      isSubmit: false

    };
  }

  /****************************************** onChangeTextInput ********************************************/
  onChangeTextInput(text, field) {
    //console.log(text)
    if (field === 'code') { this.setState({ code: text }) }
  }

  /****************************************************** resendTacCode ********************************************************/
  resendTacCode() {
    let path = "forgot_password_recode";
    let parameters = { MMID: this.state.mmidTokenForget };
    let ApiData = FetchApi(path, parameters);
    //console.log(ApiData, 'ApiData');

    this.setState({
      restartTime: Date.now() + 180000,
    })

    let that = this;

    // fetch(ApiData).then((res) => res.json())
    //   .then(function (myJson) {
    //     //console.log(myJson, 'myJson-----------');
    //     if (myJson[0].status === 1) {
    //       Alert.alert('Tac Code already SMS your phone number', '',
    //         [
    //           {
    //             text: 'OK', onPress: () => that.setState({ isSubmit: false, })
    //           },
    //         ],
    //         { cancelable: false },
    //       );
    //     }
    //     else {
    //       Alert.alert(myJson[0].error, '',
    //         [
    //           {
    //             text: 'OK', onPress: () => that.setState({ isSubmit: false, })
    //           },
    //         ],
    //         { cancelable: false },
    //       );
    //     }
    //   })
  }

  /****************************************** renderer resend tac ********************************************/
  renderer = ({ minutes, seconds, completed }) => {
    if (completed) {
      return (
        <TouchableOpacity style={styles.resendBtn} onPress={() => this.resendTacCode()}>
          <Text note style={{ color: '#0095ff', }}>Resend TAC Code</Text>
        </TouchableOpacity>
      );
    }
    else {
      return (
        <View style={styles.timeCount}>
          <Text style={styles.secondText}>{minutes}:{seconds}</Text>
        </View>
      );
    }
  }


  render() {
    const { code, mmidTokenForget, restartTime, isSubmit } = this.state;

    return (
      <ScrollView>

        <View style={{ flex: 1, alignItems: 'center', backgroundColor:' #fff' }}>
          {/************************** code input ****************************/}
          <View style={{ width: '80%', marginTop: 30 }}>
            <TextInput
              value={code}
              placeholder="******"
              placeholderTextColor="#8c8c8c"
              style={styles.inputsty}
              onChangeText={(text) => this.onChangeTextInput(text, 'code')}
              autoCorrect={false}
              autoCapitalize="none"
              keyboardType={'numeric'}
            />

            <Text note>
              We have sent you an SMS with a code to the number above,
              To complete your phone number verification, please enter the 6-digit activation code.
          </Text>

            {/************************** Countdown ****************************/}
            <View style={{ marginTop: 10 }}>
              <Countdown
                date={restartTime}
                renderer={this.renderer}
                key={restartTime}
              />
            </View>

            {/************************** button submit ****************************/}
            <Button full dark style={styles.btnwarp} onPress={() => this.onSubmit()}>
              <Text style={styles.btntext}>VERIFY</Text>
            </Button>


          </View>

        </View>

      </ScrollView>
    );
  }
}


const styles = StyleSheet.create({
  inputsty: {
    height: 50, borderColor: '#ccc', borderWidth: .5, padding: 10, borderRadius: 5, color: '#000',
    marginBottom: 20, textAlign: 'center', letterSpacing: 10
  },
  btnwarp: { marginTop: 30, borderRadius: 5, height: 50, },
  btntext: { color: '#fff', },
  resendBtn: { height: 30, justifyContent: 'center', alignItems: 'flex-end' },
  timeCount: { height: 30, justifyContent: 'flex-end', alignItems: 'flex-end' },
  secondText: { color: 'red', fontSize: 18, textAlign: 'right' },
  modalBackground: { flex: 1, alignItems: 'center', flexDirection: 'column', justifyContent: 'space-around', backgroundColor: '#00000040' },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF', height: 100, width: 100, borderRadius: 10, display: 'flex',
    alignItems: 'center', justifyContent: 'space-around'
  },

});

export default SmsVerifly;
