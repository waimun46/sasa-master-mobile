import React, { Component } from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import { Text, Form, Item, Input, Label, Button } from 'native-base';


class RegisterPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      first_name: null,
      last_name: null,
      phone_number: null,
      email: null,
      address: null,
      password: null,
      confirm_password: null
    };
  }

  onChangeTextInput(text, field) {
    //console.log(text)
    if (field === 'first_name') { this.setState({ first_name: text }) }
    if (field === 'last_name') { this.setState({ last_name: text }) }
    if (field === 'phone_number') { this.setState({ phone_number: text }) }
    if (field === 'email') { this.setState({ email: text }) }
    if (field === 'address') { this.setState({ address: text }) }
    if (field === 'password') { this.setState({ password: text }) }
    if (field === 'confirm_password') { this.setState({ confirm_password: text }) }
  }

  render() {
    const { first_name, last_name, phone_number, email, address, password, confirm_password } = this.state;
    const navigateActions = this.props.navigation.navigate;

    return (
      <View style={styles.container}>
        <ScrollView >
          <Form style={{ marginTop: 5 }}>
            <Item stackedLabel style={{ marginLeft: 0, paddingLeft: 15 }}>
              <Label >First Name</Label>
              <Input
                value={first_name}
                placeholder="fill in your first name"
                placeholderTextColor="#ccc"
                onChangeText={(text) => this.onChangeTextInput(text, 'first_name')}
                autoCorrect={false}
                autoCapitalize="none"
                //secureTextEntry={true}
                // keyboardType={'numeric'}
                style={{ fontSize: 15 }}
              />
            </Item>
            <Item stackedLabel style={{ marginLeft: 0, paddingLeft: 15 }}>
              <Label >Last Name</Label>
              <Input
                value={last_name}
                placeholder="fill in your last name"
                placeholderTextColor="#ccc"
                onChangeText={(text) => this.onChangeTextInput(text, 'last_name')}
                autoCorrect={false}
                autoCapitalize="none"
                //secureTextEntry={true}
                // keyboardType={'numeric'}
                style={{ fontSize: 15 }}
              />
            </Item>
            <Item stackedLabel style={{ marginLeft: 0, paddingLeft: 15 }}>
              <Label >Phone Number</Label>
              <Input
                value={phone_number}
                placeholder="fill in your phone number"
                placeholderTextColor="#ccc"
                onChangeText={(text) => this.onChangeTextInput(text, 'phone_number')}
                autoCorrect={false}
                autoCapitalize="none"
                //secureTextEntry={true}
                keyboardType={'numeric'}
                style={{ fontSize: 15 }}
              />
            </Item>
            <Item stackedLabel style={{ marginLeft: 0, paddingLeft: 15 }}>
              <Label >Email</Label>
              <Input
                value={email}
                placeholder="fill in your email"
                placeholderTextColor="#ccc"
                onChangeText={(text) => this.onChangeTextInput(text, 'email')}
                autoCorrect={false}
                autoCapitalize="none"
                //secureTextEntry={true}
                // keyboardType={'numeric'}
                style={{ fontSize: 15 }}
              />
            </Item>

            <Item stackedLabel style={{ marginLeft: 0, paddingLeft: 15 }}>
              <Label >Address</Label>
              <Input
                value={address}
                placeholder="fill in your address"
                placeholderTextColor="#ccc"
                onChangeText={(text) => this.onChangeTextInput(text, 'address')}
                autoCorrect={false}
                autoCapitalize="none"
                //secureTextEntry={true}
                // keyboardType={'numeric'}
                style={{ fontSize: 15 }}
              />
            </Item>
            <Item stackedLabel style={{ marginLeft: 0, paddingLeft: 15 }}>
              <Label>Password</Label>
              <Input
                value={password}
                placeholder="fill in your password"
                placeholderTextColor="#ccc"
                onChangeText={(text) => this.onChangeTextInput(text, 'password')}
                autoCorrect={false}
                autoCapitalize="none"
                secureTextEntry={true}
                style={{ fontSize: 15 }}
              />
            </Item>
            <Item stackedLabel style={{ marginLeft: 0, paddingLeft: 15 }}>
              <Label >Confirm Password</Label>
              <Input
                value={confirm_password}
                placeholder="fill in your password"
                placeholderTextColor="#ccc"
                onChangeText={(text) => this.onChangeTextInput(text, 'confirm_password')}
                autoCorrect={false}
                autoCapitalize="none"
                secureTextEntry={true}
                style={{ fontSize: 15 }}
              />
            </Item>
          </Form>
          <View style={{ padding: 20, marginTop: 20, paddingBottom: 50 }}>
            <Button onPress={() => navigateActions('home')} block style={{ backgroundColor: '#FF6347', height: 50 }}>
              <Text>SIGN UP</Text>
            </Button>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { backgroundColor: '#fff', flex: 1 },

})

export default RegisterPage;
