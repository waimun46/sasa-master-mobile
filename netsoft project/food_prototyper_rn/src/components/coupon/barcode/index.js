import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, ScrollView } from 'react-native';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import code from '../../../assest/barcode.png';
import line from '../../../assest/barline.png';



class BarCode extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    console.log('data---barcode', this.props.navigation.state.params.data);
    const data = this.props.navigation.state.params.data
    return (
      <View style={styles.container}>
        <ScrollView style={{ width: '100%' }}>
          <View style={styles.barcodeWarp}>
            <View style={{ width: '70%', marginTop: 30 }}>
              <View style={{ height: 110 }}>
                <Image source={line} style={styles.barcode} />
              </View>
              <View style={{ height: 200, marginTop: 20 }}>
                <Image source={code} style={styles.barcode} />
              </View>
              <Text note style={styles.expText}>EXP : {data.exp}</Text>
            </View>
          </View>

          <View style={{ marginTop: 40, }}>
            <Text style={{ textAlign: 'center' }}>Your Coupon Balance</Text>
            <Text style={styles.priceText}>RM {data.price}</Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, alignItems: 'center', },
  barcodeWarp: { backgroundColor: '#fff', width: '100%', alignItems: 'center', paddingBottom: 20, },
  barcode: { width: '100%', height: '100%', resizeMode: 'contain' },
  priceText: { textAlign: 'center', marginTop: 10, fontWeight: 'bold', fontSize: RFPercentage('5') },
  expText: { textAlign: 'center', marginTop: 20, color: 'gray' },

});

export default BarCode;
