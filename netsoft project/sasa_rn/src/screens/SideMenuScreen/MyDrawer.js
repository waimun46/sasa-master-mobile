
import React, { Component } from 'react';
import Drawer from 'react-native-drawer';
import HomeMenu from './HomeMenu';
export default class MyDrawer extends Component {  
  closeControlPanel = () => {
    this._drawer.close()
  };
  openControlPanel = () => {
    this._drawer.open()
  };
  render () {
    return (
      <Drawer
        ref={(ref) => this._drawer = ref}
        content={<HomeMenu />}
        >
  
      </Drawer>
    )
  }
}