
import React, { Component } from 'react';
import { Platform, FlatList, Text, View, Image, ScrollView, TextInput, TouchableOpacity, ImageBackground, Dimensions, Alert } from 'react-native';
import { Container, Header, Content, Button, ListItem, List, Left, Right } from 'native-base';
import styles from '../styles/styleOnlinePurchaseTransaction'

import Moment from 'moment';

import Progressbar from 'react-native-loading-spinner-overlay';
import Service from '../config/Service';
import CanonicalPath from '../config/CanonicalPath';
import { futuraPtMedium } from '../styles/styleText'

export default class StorePurchase extends Component {

    static navigationOptions = ({
        headerLeft: null,
        headerStyle: {
            backgroundColor: 'black',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontSize: 22,
            fontFamily: futuraPtMedium,
            fontWeight: '500'
        },
    })
    constructor(props) {
        super(props);
        this.state = {
            isVisible: false,
            transactionsStore: [],
        }

    }
    redemPress = () => {
        Alert.alert(
            '',
            'Are you at store cashier now? ',
            [

                { text: 'Cancel', },
                { text: 'OK', },
            ],
            { cancelable: false }
        )
    }

    setStateAsync(state) {
        return new Promise((resolve) => {
            this.setState(state, resolve)
        });
    }

    onSuccess = async (filter, json) => {
        this.setStateAsync({ isLoading: false });

        switch (filter) {
            case "TRANSACTION_STORE":

                this.setState({
                    transactionsStore: json,
                })

                break
        }

    }

    onError = (filter, e) => {
        this.setStateAsync({ isLoading: false });

        alert(e)
    }

    async transactions_store() {
        await this.setStateAsync({ isLoading: true });

        var params = await {
            contentType: "multipart/form-data",
            Accept: "application/json",
            keepAlive: true,
            method: 'GET',
            useToken: true,
            data: {},
            canonicalPath: CanonicalPath.TRANSACTION_STORE
        };

        await Service.request(this.onSuccess, this.onError, "TRANSACTION_STORE", params);
    }

    renderRow = (item) => {
        var dateString = item.item.date;
        var momentObj = Moment(dateString, 'yyyy-MM-DD HH:mm:ss');
        var date = momentObj.format('DD MMMM YYYY');
        return (
            <ListItem selected>
                <Left>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={styles.containerLeft} >
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text style={{ fontWeight: '300', fontSize: 18, color: '#000', fontFamily: futuraPtMedium }}>Bill No: #{item.item.bill_no}</Text>

                            </View>
                            <Text style={styles.textHeading}>Date: {date}</Text>
                            <Text style={styles.textHeading1}>Store: {item.item.location_name}</Text>
                            <Text style={styles.textHeading1}>Total: RM{item.item.total}</Text>
                        </View>
                    </View>
                </Left>
                <Right>


                </Right>
            </ListItem>
        )
    }

    componentDidMount() {
        this.transactions_store()
    }

    render() {
        return (

            <View style={{ flex: 1, backgroundColor: '#fff' }}>

                <Progressbar cancelable={true} visible={this.state.isLoading} overlayColor="rgba(0, 0, 0, 0.8)" animation="fade" textContent="" style={{ fontSize: 16, textAlign: 'center', justifyContent: 'center', height: 30, alignItems: 'center' }} />

                {
                    (this.state.transactionsStore.length > 0) ?
                        <FlatList
                            vertical
                            data={this.state.transactionsStore}
                            renderItem={item => this.renderRow(item)}>
                        </FlatList>
                        :
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                            <Text>No Record</Text>
                        </View>

                }

                {/* <List>
                    <ListItem selected>
                        <Left>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <View style={styles.containerLeft} >
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                        <Text style={{ fontWeight: '300', fontSize: 18, color: '#000', fontFamily: futuraPtMedium }}>Bill No: #0123</Text>

                                    </View>
                                    <Text style={styles.textHeading}>Date: 6 July 2018</Text>
                                    <Text style={styles.textHeading1}>Store: SASA Midvalley</Text>
                                    <Text style={styles.textHeading1}>Total: RM300</Text>
                                </View>
                            </View>
                        </Left>
                        <Right>


                        </Right>
                    </ListItem>



                    <ListItem selected>
                        <Left>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <View style={styles.containerLeft} >
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                        <Text style={{ fontWeight: '300', fontSize: 18, color: '#000', fontFamily: futuraPtMedium }}>Bill No: #0123</Text>


                                    </View>
                                    <Text style={styles.textHeading}>Date: 6 July 2018</Text>
                                    <Text style={styles.textHeading1}>Store: SASA Midvalley</Text>
                                    <Text style={styles.textHeading1}>Total: RM300</Text>
                                </View>
                            </View>
                        </Left>
                        <Right>

                        </Right>
                    </ListItem>




                    <ListItem selected>
                        <Left>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <View style={styles.containerLeft} >
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                        <Text style={{ fontWeight: '300', fontSize: 18, color: '#000', fontFamily: futuraPtMedium }}>Bill No: #0123</Text>

                                    </View>
                                    <Text style={styles.textHeading}>Date: 6 July 2018</Text>
                                    <Text style={styles.textHeading1}>Store: SASA Midvalley</Text>
                                    <Text style={styles.textHeading1}>Total: RM300</Text>
                                </View>
                            </View>
                        </Left>
                        <Right>

                        </Right>
                    </ListItem>
                </List> */}


            </View>
        )
    }
}
