import React, { Component } from 'react';
import { FlatList, StyleSheet, Text, View, Image, ScrollView, TextInput, TouchableOpacity, Dimensions, Alert } from 'react-native';
import Moment from 'moment';
import { Container, Header, Content, Button, ListItem, List, Card, Left, Right, Icon } from 'native-base';
import { futuraPtMedium, futuraPtBook } from '../styles/styleText'

export default class BillNoScreen extends Component {

    static navigationOptions = ({ navigation }) => ({
        title: `${navigation.state.params.item.order_id}`,
        headerStyle: {
            backgroundColor: 'black',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontSize: 22,
            fontFamily: futuraPtMedium
        },
    })

    constructor(props) {
        super(props);
        this.state = {
            isVisible: false,
            item: {},
            productLength: 0,
            cartList: [],
        }

    }

    componentDidMount() {
        var item = this.props.navigation.getParam('item');
        this.setState({
            item: item,
            productLength: item.detail.length,
        })


    }

    renderRow = ({ item, index }) => {

        return (
            <View style={styles.styleViewOne} >
                <View>
                    <Card style={{ padding: 4 }}><TouchableOpacity><Image style={{ width: 70, height: 100, }} source={{ uri: item.product_image }}>
                    </Image></TouchableOpacity></Card></View>
                <View style={{ flex: 1, flexDirection: 'column', marginLeft: 10, marginRight: 10 }}>
                    <Text style={{ marginLeft: 4, marginTop: 4, fontSize: 18, fontFamily: futuraPtMedium, color: '#000', fontWeight: '300' }}>{item.product_name}   </Text>
                    <Text style={{ marginLeft: 4, marginTop: 4, fontSize: 16, fontFamily: futuraPtMedium,  fontWeight: '200' }}>Quantity: {item.quantity}   </Text>
                    <Text style={{ marginLeft: 4, marginTop: 4, fontSize: 16, color: '#ff4da6', fontFamily: futuraPtBook, fontWeight: '300', }}>RM{item.product_price} </Text>
                </View>
                <View style={{ height: 2, marginHorizontal: 10, backgroundColor: '#ddd', marginTop: 10 }} />
            </View>
        )
    }

    render() {
        var dateString = this.state.item.date;
        var momentObj = Moment(dateString, 'yyyy-MM-DD HH:mm:ss');
        var date = momentObj.format('DD MMMM YYYY');
        var time = momentObj.format('HH:mm:ss');

        return (
            <ScrollView style={{ flex: 1, backgroundColor: "#fff" }}>
                <View style={styles.container}>
                    <Text style={styles.text}>ORDER SUMMARY</Text>
                    <View style={styles.conText1}>
                        <Text style={styles.text1}>Order No:</Text>
                        <Text style={styles.detailText}>  #{this.state.item.order_id}</Text>
                    </View>
                    <View style={styles.conText1}>
                        <Text style={styles.text1}>                  Date:</Text>
                        <Text style={styles.detailText}>  {date}</Text>
                    </View>
                    <View style={styles.conText1}>
                        <Text style={styles.text1}>              Time:</Text>
                        <Text style={styles.detailText}>  {time}</Text>
                    </View>

                    <View style={styles.conText1}>
                        <Text style={styles.text1}>               Total</Text>
                        <Text style={styles.detailText}>    RM {this.state.item.total}</Text>
                    </View>
                    <View style={styles.conText1}>
                        <Text style={styles.text1}>           Paid By:</Text>
                        <Text style={styles.detailText}>  {this.state.item.paid_by}</Text>
                    </View>
                    <View style={styles.containerRight1} >


                        <TouchableOpacity style={styles.colleTouch}  >
                            <Text style={{ color: '#fff', fontWeight: '300', fontFamily: futuraPtMedium }}>{this.state.item.status}</Text>
                        </TouchableOpacity>
                    </View>

                </View>
                <View style={styles.writeReview} >


                    <TouchableOpacity style={styles.writeTouch} onPress={() => this.props.navigation.navigate('AddReview')}>
                        <Image style={{ backgroundColor: '#fff', height: 20, width: 20 }} source={require('../assets/review.png')} />
                        <Text style={{ color: '#fff', fontWeight: '300', fontSize: 20, fontFamily: futuraPtMedium }}> WRITE REVIEW</Text>
                    </TouchableOpacity>
                </View>
                <Text style={{ marginTop: 20, marginLeft: 20, fontSize: 18, color: "#111111", fontWeight: '300', fontFamily: futuraPtMedium }}>{this.state.productLength} ITEMS</Text>

                <FlatList
                    vertical
                    data={this.state.item.detail}
                    renderItem={item => this.renderRow(item)}>
                </FlatList>

            </ScrollView>
        )
    }
}
const styles = StyleSheet.create({

    container: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        fontSize: 18,
        fontWeight: '300',
        color: '#111111',
        fontFamily: futuraPtMedium,
        marginTop: 20
    },
    conText1: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 10
    },
    text1: {
        fontSize: 16,
        color: '#000',
        fontFamily: futuraPtBook,
        fontWeight: '300'
    },
    detailText: {
        fontSize: 16,
        fontFamily: futuraPtBook,
        fontWeight: '300',
        color: 'lightgrey'
    },
    containerRight1: {
        marginTop: 20,
        width: 150,

        height: 10
    },
    colleTouch: {
        backgroundColor: '#66BB6A',
        justifyContent: "center",
        alignItems: 'center',
        height: 35
    },
    writeReview: {
        marginTop: 50,
        marginLeft: 20,
        marginRight: 20,

        height: 50,

    },
    writeTouch: {
        backgroundColor: '#E4007C',
        justifyContent: "center",
        alignItems: 'center',
        height: 50,
        marginBottom: 100,
        flexDirection: 'row'
    },
    styleViewOne: {
        flex: 1, flexDirection: 'row',
        backgroundColor: '#fff',
        marginTop: 20,
        marginLeft: 20,
        marginRight: 20

    },


})
