

import { createStackNavigator, StackActions, NavigationActions} from 'react-navigation'; // Version can be specified in package.json
import CheckBox from 'react-native-checkbox';
import { Col, Row, Grid } from "react-native-easy-grid";
import React, {Component} from 'react';
import styles from '../styles/styleActivate';
import {Platform, StyleSheet, Text, View,Image,ScrollView,TextInput,TouchableOpacity,ImageBackground,Dimensions, Linking, Alert, ActivityIndicator} from 'react-native';
import { Container, Header, Content, Button,ListItem,List  } from 'native-base';
import Config from '../config/Config'
import CanonicalPath from '../config/CanonicalPath';
import { futuraPtMedium } from '../styles/styleText'
import { Formik } from 'formik'
import * as Yup from 'yup'

import Input from '../components/Input'

const dimWidth= Dimensions.get('window').width
const dimHeight= Dimensions.get('window').height

const schema = Yup.object().shape({
    membershipNumber: Yup.string(),
    phoneNumber: Yup.number()
            .required( "Please enter your phone number" )
            .min(5, "Phone number not valid" ),
    term1: Yup.boolean()
            .required()
            .oneOf([true], ''),
    term2: Yup.boolean()
            .required()
            .oneOf([true], '')
})


export default class Activate extends Component {
  static navigationOptions=({
      title:'Activate ',
    headerLeft:null,
    headerStyle:{
        backgroundColor:'black'
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
        fontSize: 22,
        fontFamily:futuraPtMedium,
        fontWeight:'500'
      },
})



constructor(props){
    super(props);

    this.state={
        check:true,
        selectedIndex:0,

        myNumber:'',
        phoneNumber:'',
        myNumberError:false,
        validation: {
             myNumber:{error: false, message:""},
             phoneNumber:{error: false, message: ""}
         }
    }
}


validation = async()=>{
    let error = false;
    let valid = {
         myNumber:{error: false, message:""},
         phoneNumber:{error: false, message: ""}
     }
    // this.setState({
    //     myNumberError:false
    // })
    // if(this.state.myNumber == ''){
    //     error = true;
    //     valid.myNumber.error = true;
    //     valid.myNumber.message = "Please Enter your membership number"
    // }
    if(this.state.phoneNumber == ''){
        error = true;
        valid.phoneNumber.error = true;
        valid.phoneNumber.message = "Please Enter your phone number"
    }

    if(!error){
        const phone = "+60"+this.state.phoneNumber;
        const res = await this.activate(this.state.myNumber, phone);
        if(res.success){
            this.props.navigation.navigate('PhoneVerification1',{
                ContactNo:this.state.phoneNumber
            })
        }else{
            Alert.alert(res.message)
        }
    }else{
        this.setState({ validation: valid})
    }
}

async activate(cardNumber, phoneNumber){
    // return { success: true, message: ""};
    let result = {success: false, message: "wrong tac code"};
    try{
        const url = Config.baseUrl+CanonicalPath.ACTIVATE;
        console.log('phoneVerify ', url, cardNumber, phoneNumber)
        const body = new FormData();
        body.append("secode", Config.secode);
        body.append("card", cardNumber);
        body.append("phone", phoneNumber);
        body.append("subscribe", 1);
        const response = await fetch(url, { method: "POST", body: body });
        const res = await response.json();
        const {status, error} = res[0];
        console.log('res ', res, response, status, error)
        if(status == 1 ){
            result = { success: true, message: ""}
        }else{
            const {status, error} = res[0];
            result = { success: false, message: error}
        }
    }catch(err){
        console.log('error ', err)
        result.message = err.message;
    }
    return result;
}

async handleSubmit( values, actions ){
    console.log('handleSubmit activate ', values, actions)

    const res = await this.activate( values.membershipNumber, values.phoneNumber);
    if(res.success){
        this.props.navigation.navigate('PhoneVerification1',{
            ContactNo: values.membershipNumber
        })
    }else{
        Alert.alert(res.message)
    }
    actions.setSubmitting(false)

    return false;
}

renderForm( props ){
    return(
        <View style={{ flex: 1 }}>
            <Input
                title="Membership Number"
                name="membershipNumber"
                autoCapitalize='none'
                value={props.values.membershipNumber}
                error={props.errors.membershipNumber}
                setFieldValue={props.setFieldValue}
                setFieldTouched={()=>{}}
            />
            <Input
                title="Phone Number"
                name="phoneNumber"
                autoCapitalize='none'
                keyboardType="numeric"
                value={props.values.phoneNumber}
                error={props.errors.phoneNumber}
                setFieldValue={props.setFieldValue}
                setFieldTouched={()=>{}}
            />

            <View style={styles.viewChkMainOne}>
                <View style={styles.viewChkOne}>
                    <CheckBox
                        style={styles.chkOne}
                        label=''
                        onChange={( checked ) => {
                            console.log('onChange checkbox1 ', checked)
                            props.setFieldValue( "term1", checked)
                        }}  />
                </View>
                <View style={styles.viewChkTxtOne}>
                    <Text  style={styles.chkTextOne}>
                       I agree and accept all Terms and Conditions and Privacy Policy of SASA Malaysia.
                    </Text>
                    {
                        ( props.errors.term1 && props.errors.term1.length > 0)
                        ? <View><Text style={ styles.nameError }>Please tick Agree to policies</Text></View>
                        : null
                    }
                </View>
            </View>

            <View style={styles.viewChkMainOne}>
                <View style={styles.viewChkOne}>
                    <CheckBox
                        style={styles.chkOne}
                        label=''
                        checked={this.state.checked}
                        onChange={( checked ) => {
                            console.log('onChange checkbox1 ', checked)
                            props.setFieldValue( "term2", checked)
                        }}  />
                </View>
                <View   style={styles.viewChkTxtOne}>
                    <Text  style={styles.chkTextOne}>
                       I agree to receive newsletter SMS from SASA Malaysia.
                    </Text>
                    {
                        ( props.errors.term2 && props.errors.term2.length > 0)
                        ? <View><Text style={ styles.nameError }>Please tick Agree to policies</Text></View>
                        : null
                    }
                </View>
            </View>

            <View style={{ marginBottom: 20, marginLeft: 20, marginRight: 20, height: 50 }}>
                {
                    props.isSubmitting
                    ?   <ActivityIndicator size="small"/>
                    :   <View style={styles.viewButton}>
                            <TouchableOpacity
                                style={ styles.touch }
                                onPress={ (e) => {
                                    props.handleSubmit(e)
                                }}
                            >
                                <Text style={styles.textButton}>Activate</Text>
                            </TouchableOpacity>
                        </View>
                }
            </View>
        </View>
    )
}

 render() {
     const {myNumber, phoneNumber, validation} = this.state;

    return (
       <ScrollView style={styles.container}>
           <Formik
               onSubmit={ this.handleSubmit.bind(this) }
               validationSchema={ schema }
               render={ this.renderForm.bind(this) }
               validateOnChange={ false }
           />

            <TouchableOpacity style={{margin:20, alignItems:'center'}}
                onPress={() => {
                    Linking.openURL("tel:0392896877")
                }}
            >
                <Text style={{textAlign:'center', color: 'blue'}}>
                    Having trouble to activate your account? Contact us now at 03-9289 6877
                </Text>
            </TouchableOpacity>
        </ScrollView>
    );
  }
}
