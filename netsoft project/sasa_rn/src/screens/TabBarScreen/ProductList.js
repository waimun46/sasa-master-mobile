

import { createStackNavigator, StackActions, NavigationActions } from 'react-navigation'; // Version can be specified in package.json
import { Col, Row, Grid } from "react-native-easy-grid";
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Image,ScrollView,TextInput,TouchableOpacity,ImageBackground,Dimensions,Alert} from 'react-native';
import { Container, Header, Content, Button,ListItem,List,Left,Right  } from 'native-base';
import {Card} from 'native-base'
import StarRating from 'react-native-star-rating';
import Dialog, { DialogContent,DialogButton,SlideAnimation } from 'react-native-popup-dialog';
const width= Dimensions.get('window').width
const height= Dimensions.get('window').height
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import { futuraPtMedium, futuraPtBook } from '../../styles/styleText'
export default class ProductList extends Component {

    static propTypes = {

        onPress : PropTypes.func.isRequired,
    }

  static navigationOptions=({
    title:'Make Up',
  headerLeft:null,
  headerStyle:{
      backgroundColor:'black',

  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontSize: 22,
    fontFamily:futuraPtMedium,
    fontWeight:'500'
    },
})
constructor(props){
  super(props);
    this.state={
      isVisible:false,
      isVisible1:false,
      selectBrands:false,
      productList:[],
      countList:'',
      picture:'',
      code:''

    }

  }
//   componentWillMount(){
//     const { navigation } = this.props;
//     const code = navigation.getParam('code');
//     this.getProduct(code)
//   }
//    getProduct=(code)=>{
//     fetch('http://cekapbuy.com/api/category.php?secode=sasaapp20181019&&'+code, {
//     method: 'GET',
//     headers: {
//     'Content-Type': 'application/json'
//     },

//     })
//     .then((serviceResponse) => { return serviceResponse.json() } )
//     .catch((error) => console.warn("fetch error:", error))
//     .then((serviceResponse) => {

//         this.setState({productList:serviceResponse,})


//    Alert.alert(JSON.stringify(this.state.productList))
//     console.log(JSON.stringify(serviceResponse));
//     });
//     }


  render() {
    const { onPress } = this.props;
    return (
     <View style={{backgroundColor:'#fff',flex:1}}>
     {this.state.productList.map(index =>

    <View>
        <Text key={index.id}>{index.id}</Text>
        <Image
      key={index.product_pic}
      source={{uri: this.state.picture}}
      style={{width: 208, height: 208}} />
        </View>
    )}
       <ScrollView>
           <View style={{flexDirection:'row',justifyContent:'space-between',backgroundColor:'#fff',marginTop:20,marginLeft:10}}>
                <View style={{flexDirection:'column',flex:1}}>
                <TouchableOpacity>
                    <Image style={styles.imageContainer} source={require('../../assets/deo.png')}/>
                    </TouchableOpacity>
                    <View style = {{justifyContent:"center", alignItems:'center', width:156 , backgroundColor:'#F48FB1',marginLeft:10 }}>
                        <Text style={{color:'#FFFFFF', alignItems:'center',fontFamily:futuraPtBook}}>10X POINTS</Text>
                    </View>
                        <View style={{width:100,height:20, flex: 1 , flexDirection:'row',marginBottom:4,marginTop:8,marginLeft:10}}>
                              <StarRating
                                  disabled={false}
                                  maxStars={5}
                                  rating= {5}
                                  starSize={18}
                                  fullStarColor = {'#DAA520'} />
                                  <Text style={{marginLeft:10,fontSize:14}}>(37)</Text>
                        </View>
                        <Text style={{marginLeft:10,fontWeight: '300',fontFamily:futuraPtMedium,fontSize:18,color:'#000000',marginTop:5}}>Dr.G-Hyper</Text>
                        <Text style={{marginLeft:10,fontWeight: '300',fontFamily:futuraPtMedium,fontSize:18,color:'#000000',marginTop:5}}>Brightening Cream</Text>
                        <Text style={{marginLeft:10,fontFamily:futuraPtBook,fontSize:16,fontWeight:'300',marginTop:5,color:'#111111'}}>by Silkygirl</Text>
                        <Text style={{ textDecorationLine: 'line-through',fontFamily:futuraPtBook, marginLeft:10,marginTop:5}}>RM14.60</Text>



                        <List style={{marginTop:-20 ,}}>
                            <ListItem>
                              <Left>
                                    <Text style={{color: '#ff4da6' }}>RM9.90</Text>
                              </Left>
                              <TouchableOpacity>
                              <Right>
                                   <View style={{backgroundColor:'#FFEBEE',height:40,width:40,justifyContent:'center',alignItems:'center',marginRight:10}}>
                                  <Image  source={require('../../assets/cart.png')}/>
                                  </View>
                              </Right>
                              </TouchableOpacity>
                            </ListItem>
                        </List>
                  </View>


                   <View style={{flexDirection:'column',flex:1}}>
                   <TouchableOpacity onPress={()=>this.props.click.navigation.navigate('ProductDetailsScreen')}>
                    <Image style={styles.imageContainer} source={require('../../assets/deo.png')}/>
                    </TouchableOpacity>
                    <View style = {{justifyContent:"center", alignItems:'center', width:156 , backgroundColor:'#F48FB1',marginLeft:10 }}>
                        <Text style={{color:'#FFFFFF', alignItems:'center',fontFamily:futuraPtBook}}>10X POINTS</Text>
                    </View>
                        <View style={{width:100,height:20, flex: 1 , flexDirection:'row',marginBottom:4,marginTop:8,marginLeft:10}}>
                              <StarRating
                                  disabled={false}
                                  maxStars={5}
                                  rating= {5}
                                  starSize={18}
                                  fullStarColor = {'#DAA520'} />
                                  <Text style={{marginLeft:10,fontSize:14}}>(37)</Text>
                        </View>
                        <Text style={{marginLeft:10,fontWeight: '300',fontFamily:futuraPtMedium,fontSize:18,color:'#000000',marginTop:5}}>Dr.G-Hyper</Text>
                        <Text style={{marginLeft:10,fontWeight: '300',fontFamily:futuraPtMedium,fontSize:18,color:'#000000',marginTop:5}}>Brightening Cream</Text>
                        <Text style={{marginLeft:10,fontFamily:futuraPtBook,fontSize:16,fontWeight:'300',marginTop:5,color:'#111111'}}>by Silkygirl</Text>
                        <Text style={{ textDecorationLine: 'line-through',fontFamily:futuraPtBook, marginLeft:10,marginTop:5}}>RM14.60</Text>



                        <List style={{marginTop:-20 ,}}>
                            <ListItem>
                              <Left>
                                    <Text style={{color: '#ff4da6' }}>RM9.90</Text>
                              </Left>
                              <TouchableOpacity>
                              <Right>
                                   <View style={{backgroundColor:'#FFEBEE',height:40,width:40,justifyContent:'center',alignItems:'center',marginRight:10}}>
                                  <Image  source={require('../../assets/cart.png')}/>
                                  </View>
                              </Right>
                              </TouchableOpacity>
                            </ListItem>
                        </List>
                  </View>
                  {/*****

                  <TouchableOpacity onPress={() => this.props.navigation.navigate('ProductDetailsScreen')}>
                        <Image style={{marginRight:20}} source={require('../assets/02.png')}/>
                  </TouchableOpacity>

                  ******/}
            </View>


   <View style={{flexDirection:'row',justifyContent:'space-between',backgroundColor:'#fff',marginTop:20,marginLeft:10}}>
                <View style={{flexDirection:'column',flex:1}}>
                <TouchableOpacity>
                    <Image style={styles.imageContainer} source={require('../../assets/deo.png')}/>
                    </TouchableOpacity>
                    <View style = {{justifyContent:"center", alignItems:'center', width:156 , backgroundColor:'#F48FB1',marginLeft:10 }}>
                        <Text style={{color:'#FFFFFF', alignItems:'center',fontFamily:futuraPtBook}}>10X POINTS</Text>
                    </View>
                        <View style={{width:100,height:20, flex: 1 , flexDirection:'row',marginBottom:4,marginTop:8,marginLeft:10}}>
                              <StarRating
                                  disabled={false}
                                  maxStars={5}
                                  rating= {5}
                                  starSize={18}
                                  fullStarColor = {'#DAA520'} />
                                  <Text style={{marginLeft:10,fontSize:14}}>(37)</Text>
                        </View>
                        <Text style={{marginLeft:10,fontWeight: '300',fontFamily:futuraPtMedium,fontSize:18,color:'#000000',marginTop:5}}>Dr.G-Hyper</Text>
                        <Text style={{marginLeft:10,fontWeight: '300',fontFamily:futuraPtMedium,fontSize:18,color:'#000000',marginTop:5}}>Brightening Cream</Text>
                        <Text style={{marginLeft:10,fontFamily:futuraPtBook,fontSize:16,fontWeight:'300',marginTop:5,color:'#111111'}}>by Silkygirl</Text>
                        <Text style={{ textDecorationLine: 'line-through',fontFamily:futuraPtBook, marginLeft:10,marginTop:5}}>RM14.60</Text>



                        <List style={{marginTop:-20 ,}}>
                            <ListItem>
                              <Left>
                                    <Text style={{color: '#ff4da6' }}>RM9.90</Text>
                              </Left>
                              <TouchableOpacity>
                              <Right>
                                   <View style={{backgroundColor:'#FFEBEE',height:40,width:40,justifyContent:'center',alignItems:'center',marginRight:10}}>
                                  <Image  source={require('../../assets/cart.png')}/>
                                  </View>
                              </Right>
                              </TouchableOpacity>
                            </ListItem>
                        </List>
                  </View>


                   <View style={{flexDirection:'column',flex:1}}>
                   <TouchableOpacity>
                    <Image style={styles.imageContainer} source={require('../../assets/deo.png')}/>
                    </TouchableOpacity>
                    <View style = {{justifyContent:"center", alignItems:'center', width:156 , backgroundColor:'#F48FB1',marginLeft:10 }}>
                        <Text style={{color:'#FFFFFF', alignItems:'center',fontFamily:futuraPtBook}}>10X POINTS</Text>
                    </View>
                        <View style={{width:100,height:20, flex: 1 , flexDirection:'row',marginBottom:4,marginTop:8,marginLeft:10}}>
                              <StarRating
                                  disabled={false}
                                  maxStars={5}
                                  rating= {5}
                                  starSize={18}
                                  fullStarColor = {'#DAA520'} />
                                  <Text style={{marginLeft:10,fontSize:14}}>(37)</Text>
                        </View>
                        <Text style={{marginLeft:10,fontWeight: '300',fontFamily:futuraPtMedium,fontSize:18,color:'#000000',marginTop:5}}>Dr.G-Hyper</Text>
                        <Text style={{marginLeft:10,fontWeight: '300',fontFamily:futuraPtMedium,fontSize:18,color:'#000000',marginTop:5}}>Brightening Cream</Text>
                        <Text style={{marginLeft:10,fontFamily:futuraPtBook,fontSize:16,fontWeight:'300',marginTop:5,color:'#111111'}}>by Silkygirl</Text>
                        <Text style={{ textDecorationLine: 'line-through',fontFamily:futuraPtBook, marginLeft:10,marginTop:5}}>RM14.60</Text>



                        <List style={{marginTop:-20 ,}}>
                            <ListItem>
                              <Left>
                                    <Text style={{color: '#ff4da6' }}>RM9.90</Text>
                              </Left>
                              <TouchableOpacity>
                              <Right>
                                   <View style={{backgroundColor:'#FFEBEE',height:40,width:40,justifyContent:'center',alignItems:'center',marginRight:10}}>
                                  <Image  source={require('../../assets/cart.png')}/>
                                  </View>
                              </Right>
                              </TouchableOpacity>
                            </ListItem>
                        </List>
                  </View>
                  {/*****

                  <TouchableOpacity onPress={() => this.props.navigation.navigate('ProductDetailsScreen')}>
                        <Image style={{marginRight:20}} source={require('../assets/02.png')}/>
                  </TouchableOpacity>

                  ******/}
            </View>

            </ScrollView>
             </View>




    );
  }
}
const styles=StyleSheet.create({
  menuItems:{
    flexDirection:'row',
    justifyContent:'center',
    height:50
  },
  image1:{
    height:20,
    width:25,
marginLeft:20,
marginTop:15
  },
  text1:{
    fontSize:15,
    marginLeft:10,
    marginTop:15,
    color:'black',
    fontWeight:'300',
    fontFamily:"future-pt-book"
  },
  image2:{
    height:30,
    width:30,
    marginLeft:30,
    marginTop:10

  },
  text2:{
    fontSize:15,
    marginTop:15
  },
  cardContainer:{
    flexDirection:'row',

    marginTop:10,
    padding:10
  },
  imageContainer:{
    marginLeft:10 ,

    width:156 , height: 200,borderWidth:1,borderColor:'grey'

  }

})
