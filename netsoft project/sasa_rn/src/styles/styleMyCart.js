import {StyleSheet, Dimensions} from 'react-native';
import { futuraPtMedium, futuraPtBook } from './styleText'

const dimWidth= Dimensions.get('window').width
const dimHeight= Dimensions.get('window').height
const buttonColor='#ff4da6'
const textColor='#fff'
const backgroundColor='#fff'

const styles=StyleSheet.create({
    container:{
        height:dimHeight
    },
  containerBackground:{
        backgroundColor:backgroundColor
    },
    viewText:{
        marginTop: 6,
        marginBottom: 10,
        marginLeft:10,
        flex: 1 ,
        flexDirection:'column',

    },
    textHeading:{
        color:'#111111',
        fontSize:18,
        fontFamily:futuraPtBook,
        fontWeight:'300',


    },
    textHeading1:{
        color:'#111111',
        fontSize:18,
        fontWeight:'300',
        fontFamily:futuraPtMedium
    },
    textNormal:{
        color:'#C0C0C0',
        fontSize:14,
        marginTop: 2,
        marginBottom: 2,
        justifyContent:"center",
        alignItems:'center'
    },
     bottomLine:{
        backgroundColor:'#D3D3D3',
        height:0.50,
        marginRight:4,
        marginLeft:4,
        marginTop:10,
        marginBottom:10
   },
   styleViewOne:{
       flex: 1 , flexDirection:'row'
   },
   styleTextRight:{
       textAlign: 'right', color: '#ff4da6',marginRight:4
   },
   bottomLine:{
        backgroundColor:'#D3D3D3',
        height:0.50,

        marginTop:20,
        marginBottom:10
   },
    btnContainer:{
    marginTop:20,
    marginBottom:20,
    marginLeft:20,
    marginRight:20,
    height:50
  },
   btnTouch:{
    backgroundColor:'#E4007C',
    justifyContent:"center",
    alignItems:'center',
    height:50
  },
   btnText:{
    color:'white',
    fontSize:20,
    fontFamily:futuraPtMedium
  },


});
export default styles;
