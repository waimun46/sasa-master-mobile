import {StyleSheet, Dimensions} from 'react-native';
import { futuraPtMedium, futuraPtBook, futuraPtLight } from './styleText'

const dimWidth = Dimensions.get('window').width
const height = Dimensions.get('window').height
const backgroundColor='#fff'
const buttonColor='#E4007C'
const textColor='#fff'

const styles=StyleSheet.create({
    container:{
        height:height
    },
  containerBackground:{
        backgroundColor:backgroundColor
    },

    viewCreateNewPsw:{
         flex: 1,
         alignItems: 'center',
         justifyContent: 'center' ,
         marginTop: 20
    },
    textCreateNewPsw:{
        color:'#000' ,
        justifyContent:"center",
        alignItems:'center',
        fontWeight:'500',
        fontSize:18,
        fontFamily:futuraPtMedium
    },
    viewResetView:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center' ,
        marginTop: 8,
        marginLeft: 30,
        marginRight: 20
    },
    textResetView:{
        color:'#000' ,
        justifyContent:"center",
        alignItems:'center',
        fontFamily:futuraPtBook,
        fontSize:16,
        fontWeight:'300'
    },
    viewEmail:{
        marginTop: 20,
        marginLeft:20
    },
    textEmail:{
        color:'#000',
        justifyContent:"center",
        alignItems:'center',
        fontFamily:futuraPtLight,
        fontWeight:'300',
        fontSize:16
    },
    inputTextEmail:{
        color:'#000',
        justifyContent:"center",
        alignItems:'center',
        marginTop:10,
        fontFamily:futuraPtBook,
        fontSize:16,
        fontWeight:'300'
    },

    downLine:{
        backgroundColor:'#D3D3D3',
        height:1,
        marginRight:20,
        marginLeft:20
    },

    emailError:{
        color:'red',
        fontSize:15,
        marginLeft:15,
        marginTop:10,
        fontSize:16,
        fontFamily:futuraPtBook
    },
    viewButton:{
        marginTop:20,
        marginLeft:20,
        marginRight:20,
        height:50,

        backgroundColor:buttonColor
    },


    textButton:{
       color:textColor,fontSize:20,
       fontFamily:futuraPtMedium
  },
  touch:{
      justifyContent:"center",
      alignItems:'center',
      height:50
  }



});
export default styles;
