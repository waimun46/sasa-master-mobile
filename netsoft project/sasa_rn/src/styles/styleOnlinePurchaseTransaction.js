import {StyleSheet, Dimensions} from 'react-native';
import { futuraPtLight } from './styleText'

const dimWidth= Dimensions.get('window').width
const dimHeight= Dimensions.get('window').height
const buttonColor='#ff4da6'
const textColor='#fff'
const backgroundColor='#fff'

const styles=StyleSheet.create({
    container:{
        height:dimHeight
    },
    containerLeft:{

        marginTop:10
    },
    containerRight:{
        marginTop:20,
      width:150,

        height:10
    },
    containerRight1:{
        marginTop:20,
      width:150,

        height:10
    },
    containerRight2:{
        marginTop:20,
      width:150,

        height:10
    },
  containerBackground:{
        backgroundColor:backgroundColor
    },
    viewText:{
        marginTop: 4,
        marginBottom: 4,
        marginLeft:10
    },
    signinTouch:{
        backgroundColor:'#FFEBEE',
        justifyContent:"center",
        alignItems:'center',
        height:35
      },
      signinTouch1:{
        backgroundColor:'#66BB6A',
        justifyContent:"center",
        alignItems:'center',
        height:35
      },
      signinTouch2:{
        backgroundColor:'#E4007C',
        justifyContent:"center",
        alignItems:'center',
        height:35
      },
    textHeading:{
        fontWeight:'300',
        fontFamily:futuraPtLight,
         color:'#000000',
         marginTop:10,

         fontSize:18,
    },
    textHeading1:{
        fontWeight:'300',
        fontFamily:futuraPtLight,
         color:'#000000',
        marginTop:10,

        fontSize:18,



        fontSize:16,
   },
    textNormal:{

        color:'#000',
        fontSize:14,
        marginTop: 2,
        marginBottom: 2,
        justifyContent:"center",
        alignItems:'center'
    },

});
export default styles;
