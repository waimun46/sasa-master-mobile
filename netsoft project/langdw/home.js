/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Button,
    TouchableOpacity,
} from 'react-native';

import DataRepository from './js/dao/DataRepository'
import { I18n } from './js/language/I18n'
import DeviceInfo from 'react-native-device-info'

export default class App extends Component {

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
            {I18n.t('english')}
        </Text>
       
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    buttonMargin: {
        marginVertical: 20,
        backgroundColor: '#FA8072'
    },
    text: {
        color: 'white',
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    }
});
