import { AsyncStorage } from 'react-native';
import moment from 'moment';
import duration from 'moment-duration-format';

var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/='

class Util {

    setAsyncStorage = async (name, value) =>{        
        await AsyncStorage.setItem(name, value)
    }

    getAsyncStorage = async (name) =>{
        let value = await AsyncStorage.getItem(name)
        return value
    }

    strReplace = (source, replace, replaceWith) => {
        var value = source
        var i = 0
        for (i; i < value.length; i++) {
            value = value.replace(replace, replaceWith)
        }
        console.log(value)
        return value;
    }

    upperCaseString = (i) =>{
        if (typeof i === 'string') {
            return i.toUpperCase()
        }
        return i
    }

    formattingNumber = (i) =>{
        if (typeof i === 'number') {
            return i.toLocaleString(navigator.language, { minimumFractionDigits: 0 });
        }
        return i
    }

    validateEmail = (text) => {
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (reg.test(text) === false) {
            return false;
        } else {
            return true;
        }
    }

    getDate(value) {
        let date = moment(value, ['YYYY-MM-DD HH:mm:ss', 'DD-MM-YYYY HH:mm:ss']).format('DD MMMM YYYY')
        // let parse = moment(value).parformat('DD-MMMM-YYYY')
        console.log('Date', date)
        return date
    }

    formatChangeImsiBirthDate(value) {
        let date = moment(value, ['YYYY-MM-DD', 'DD-MM-YYYY']).format('YYYY-MM-DD')
        // let parse = moment(value).parformat('DD-MMMM-YYYY')
        console.log('Date', date)
        return date
    }

    formatDate(value) {
        let date = moment(value, ['YYYY-MM-DD HH:mm:ss', 'DD-MM-YYYY HH:mm:ss']).format('DD MMMM YYYY HH:mm:ss')
        return date
    }

    formatTime(value) {
        let date = moment(value, ['YYYY-MM-DD HH:mm:ss', 'DD-MM-YYYY HH:mm:ss']).format('HH:mm:ss')
        return date
    }

    formatTimeCountDown(value) {
        var time
        if (value < 60) {
            time = '00:' + value
        } else {
            time = moment.duration(value, 'seconds').format('DD   HH   mm   ss')
        }
        return time
    }

    formatMoney(num) {
        num = num + ""
        //return parseInt( val ).toLocaleString('ID');
        if(num == "" || num == "0")        
            return "";
        
        //var n = num.replace(/\./g, "");
        var num_parts = num.toString().split(".");
        num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        return "Rp " + num_parts.join(".");
    }

    getParamsFromNavigation(navigation, paramName) {
        return navigation.state.params[paramName]
    }

    _createDateData() {
        let date = [];
        for(let i=1950;i<2030;i++){
            let month = [];
            for(let j = 1;j<13;j++){
                let day = [];
                if(j === 2){
                    for(let k=1;k<29;k++){
                        day.push(k);
                    }
                    //Leap day for years that are divisible by 4, such as 2000, 2004
                    if(i%4 === 0){
                        day.push(29);
                    }
                }
                else if(j in {1:1, 3:1, 5:1, 7:1, 8:1, 10:1, 12:1}){
                    for(let k=1;k<32;k++){
                        day.push(k);
                    }
                }
                else{
                    for(let k=1;k<31;k++){
                        day.push(k);
                    }
                }
                let _month = {};
                _month[j] = day;
                month.push(_month);
            }
            let _date = {};
            _date[i] = month;
            date.push(_date);
        }
        return date;
    }

    base64ToByteArray(base64String) {
        try {            
            var sliceSize = 1024;
            var byteCharacters = this.atob(base64String);
            var bytesLength = byteCharacters.length;
            var slicesCount = Math.ceil(bytesLength / sliceSize);
            var byteArrays = new Array(slicesCount);

            for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
                var begin = sliceIndex * sliceSize;
                var end = Math.min(begin + sliceSize, bytesLength);

                var bytes = new Array(end - begin);
                for (var offset = begin, i = 0; offset < end; ++i, ++offset) {
                    bytes[i] = byteCharacters[offset].charCodeAt(0);
                }
                byteArrays[sliceIndex] = new Uint8Array(bytes);
            }
            return byteArrays;
        } catch (e) {
            console.log("Couldn't convert to byte array: " + e);
            return undefined;
        }
    }

    atob(input){
        let str = input.replace(/=+$/, '');
        let output = '';
    
        if (str.length % 4 == 1) {
          throw new Error("'atob' failed: The string to be decoded is not correctly encoded.");
        }
        for (let bc = 0, bs = 0, buffer, i = 0;
          buffer = str.charAt(i++);
    
          ~buffer && (bs = bc % 4 ? bs * 64 + buffer : buffer,
            bc++ % 4) ? output += String.fromCharCode(255 & bs >> (-2 * bc & 6)) : 0
        ) {
          buffer = chars.indexOf(buffer);
        }
    
        return output;
    }

    alert(title, message) {
        Alert.alert(
            title,
            message,
            [
                { text: 'OK', onPress: () => console.log('OK Pressed') },
            ],
            { cancelable: false }
        )
    }

    alertAction(title, message, onOk) {
        Alert.alert(
            title,
            message, [
                // {
                //     text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'
                // }, 
                {
                    text: 'OK',
                    onPress: () => {
                        onOk();
                    }
                },], {
                cancelable: false
            }
        )
        return true;
    }

    alertConfirm(title, message, onOk, onCancel) {
        Alert.alert(
            title,
            message, [
                {
                    text: 'Cancel', onPress: () => onCancel(), style: 'cancel'
                }, {
                    text: 'OK',
                    onPress: () => {
                        onOk();
                    }
                },], {
                cancelable: false
            }
        )
        return true;
    }


}

export default new Util();

