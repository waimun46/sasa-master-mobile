import { AsyncStorage} from 'react-native';
import query from 'query-string';
import config from './Config'

class Service {

    constructor() {

    }

    request = async (onSuccess, onError, filter, params) => {

        var options = await {
            timeout: 10 * 1000,
            method: params.method,
        };
        var headers = {
            // add some header
        }
        
        var path = params.canonicalPath
        if (params.method == 'POST' || params.method == 'PUT') {
            // add some header
            headers['Content-Type'] = params.contentType
            headers['Accept'] = params.Accept

            params.data.append('secode', config.secode);
            let useToken = params.useToken
            if (useToken) {
                params.data.append('access_token', await AsyncStorage.getItem('access_token'));
            }
            
            options.body = params.data;
        } else {
            const param = Object.assign({}, params);
            param.data.secode = config.secode;

            let useToken = params.useToken
            if (useToken) {
                param.data.access_token = await AsyncStorage.getItem('access_token');
            }
            if (param.data != null) {
                console.log(param.data)
                let json = JSON.parse(JSON.stringify(param.data))
                path = path + '?'
                path += query.stringify(json)
            }

        }

        options.headers = headers

        let url = this._getUrlStringWithPath(path)

        console.log("========================= REQUEST =========================");
        console.log("URL", url);
        console.log("Path", path);
        console.log("Method", options.method);
        console.log("Body", options.body)
        console.log("=====================END OF REQUEST =========================");

        let response = await this.sendRequest(url, options)

        try {
            console.log("========================= RESPONSE =========================");
            console.log("Response", JSON.stringify(response));
            return onSuccess(filter, JSON.parse(response._bodyText.substring(2)))
        }
        catch (e) {
            console.log('error')
            return onError(filter, e);
        }
    }

    _getUrlStringWithPath(path) {
        var baseURL = config.baseUrl
        return `${baseURL}${path}`;
    }

    sendRequest = async (url, options) => {
        let response = await fetch(url, options)
        console.log('Response ', response)
        return response
    }

}

export default new Service()