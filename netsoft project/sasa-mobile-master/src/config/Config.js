import configure from '../../package.json';

export default class config {
    static appName = configure.name
    static appVersion = configure.version

    // static baseUrl = 'http://cekapbuy.com'
    static baseUrl = 'https://sasa.com.my/test/_adminCP/'
    static secode = 'sasaapp20181019'

    static oneSignalAppId = '0a66a683-8075-4fcc-af79-236c77c99f99'
    static oneSignalIosId = "fd6980a0-026b-4df8-b573-be879fc4f1b1"
    // static oneSignalAppId = '65a0a76b-445a-42bc-b913-b1a33e02fd81'

}
