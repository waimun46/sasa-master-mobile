import React, { Component } from 'react';
import { Platform, AsyncStorage, Text, View, Image, ScrollView, TextInput, TouchableOpacity, ImageBackground, Dimensions, Alert, Linking, StatusBar } from 'react-native';
import Progressbar from 'react-native-loading-spinner-overlay';
import Service from '../config/Service';
import CanonicalPath from '../config/CanonicalPath';
import styles from '../styles/login'
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
//  import {FBLogin, FBLoginManager,LoginBehaviors,AccessToken} from 'react-native-facebook-login';
import FBLoginView from './FBLoginView'
import { LOGIN_URL, CONNECTFB_URL } from '../config/Global'
import axios from 'axios';
import FBSDK, { LoginButton, LoginManager, AccessToken, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';
export default class LoginScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    header: null

  })

  constructor(props) {
    super(props);
    this.state = {
      myEmail: '',
      passWord: '',
      numberError: false,
      passError: false,
      secode: 'sasaapp20181019',
      data1: '',
      user: null,
      isfbLogin: false,
      emailinfo: '',
      isLoading: false,

    }
  }

  responseInfoCallback = (error, result, data) => {
    if (error) {
      console.log(error);
      console.log('Error fetching data=', error.toString());
    } else {
      console.log("sasa result: ", result)
      if (result.email) {
        const email = result.email

        var details = {
          'secode': 'sasaapp20181019',
          'email': email,
          'fb_token': data.accessToken
        };
        console.log("detailss", details)
        var formBody = [];
        for (var property in details) {
          var encodedKey = encodeURIComponent(property);
          var encodedValue = encodeURIComponent(details[property]);
          formBody.push(encodedKey + "=" + encodedValue);
        }
        formBody = formBody.join("&");
        fetch(CONNECTFB_URL, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          },
          body: formBody
        })
          .then((serviceResponse) => { return serviceResponse.json() })
          .catch((error) => console.warn("fetch error:", error))
          .then((serviceResponse) => {
            console.log("login ok dong", serviceResponse)
            if (serviceResponse[0].status == 1) {
              this.onLogin(data.accessToken)
            } else {
              Alert.alert(serviceResponse[0].error);
            //   this.props.navigation.navigate('SignUp')
            }
          });

      } else {
        this.props.navigation.navigate('SignUp')
        return;
      }
    }
  };

  handleFBSuccess = async (result) => {
    if (result.isCancelled) {
      Alert.alert('Login cancelled');

    } else {
      try {
        const data = AccessToken.getCurrentAccessToken()
        const accessToken = data.accessToken;
        const infoRequest = new GraphRequest(
          '/me',
          {
            accessToken,
            parameters: {
              fields: {
                string: 'email,name,first_name,middle_name,last_name',
              },
            },
          },
          (error, result) => this.responseInfoCallback(error, result, data),
        );
        new GraphRequestManager().addRequest(infoRequest).start();
      } catch (error) {
        console.log('ERROR GET ACCESS TOKEN ', error)
      }
    }
  }

  handleFBError = (error) => {
    Alert.alert('Login fail with error: ' + error);
  }

  _fbAuth = async () => {

    LoginManager.logInWithReadPermissions(['email', 'public_profile', 'user_link']).then(this.handleFBSuccess, this.handleFBError);

  }
  async onLogin(accessToken) {
    await AsyncStorage.setItem('access_token', accessToken)
    this.navigation.navigate('HomeScreen')
  }

  setStateAsync(state) {
    return new Promise((resolve) => {
      this.setState(state, resolve)
    });
  }

  isSpaceEmail(value) {

    this.setState({
      myEmail: value.replace(/\s/g, '')
    })
  }
  isSpacePassword(value) {

    this.setState({
      passWord: value.replace(/\s/g, '')
    })
  }


  setStateAsync(state) {
    return new Promise((resolve) => {
      this.setState(state, resolve)
    });
  }

  onSuccess = async (filter, json) => {
    this.setStateAsync({ isLoading: false });

    switch (filter) {
      case "LOGIN":
        var status = json[0].status;
        if (status == 1) {
          await AsyncStorage.setItem('access_token', json[0].access_token)
          this.props.navigation.navigate('HomeScreen')
        } else {
          Alert.alert(json[0].error)
        }

        break
    }

  }

  login = async () => {

    this.setState({
      emailError: false,
      passError: false
    })

    if (this.state.myEmail === '') {
      this.setState({
        emailError: true,
      })
      return;
    }
    if (this.state.passWord === '') {
      this.setState({
        passError: true
      })
      return;
    }

    var validator = require("email-validator");
    if (!validator.validate(this.state.myEmail)) {
      this.setState({
        emailError: true
      })
      return;
    }

    // this.props.navigation.navigate('App')

    await this.setStateAsync({ isLoading: true });

    var formData = new FormData();

    formData.append('email', this.state.myEmail);
    formData.append('password', this.state.passWord);
    var params = await {
      contentType: "multipart/form-data",
      Accept: "application/json",
      keepAlive: true,
      method: 'POST',
      useToken: false,
      data: formData,
      canonicalPath: CanonicalPath.LOGIN
    };


    await Service.request(this.onSuccess, this.onError, "LOGIN", params);

  }


  render() {
    if (this.state.isfbLogin !== false) {
      debugger
      this.navigation.navigate('HomeScreen')

    }
    return (
      <ScrollView scrollEnabled={true} >
        <StatusBar
          backgroundColor="black"
          barStyle="light-content"
        />

        <Progressbar cancelable={true} visible={this.state.isLoading} overlayColor="rgba(0, 0, 0, 0.8)" animation="fade" textContent="" style={{ fontSize: 16, textAlign: 'center', justifyContent: 'center', height: 30, alignItems: 'center' }}></Progressbar>

        <ImageBackground style={{ height: height }} source={require('../assets/sasabackground.png')} >

          <View style={styles.logoContainer}>
            <Image source={require('../assets/sasalogo.png')} resizeMode="contain" style={{ width: 200, height: 100 }} />
          </View>

          <View style={styles.phoneInptContainer}>
            <TextInput placeholderTextColor="#fff" style={styles.phoneText} placeholder='Email Address'
              value={this.state.myEmail}
              autoCapitalize='none'
              borderBottomColor='#D3D3D3'
              borderBottomWidth={1}
              onChangeText={myEmail => this.isSpaceEmail(myEmail)} />
          </View>
          {this.state.emailError == true &&
            <View style={styles.alertBox}>
              <Text style={styles.phoneErrorText}> Please enter your valid email address  </Text>
            </View>
          }
          <View style={styles.passInptContainer}>
            <TextInput placeholderTextColor="#fff" style={styles.passText} placeholder='Password' secureTextEntry={true}
              value={this.state.passWord}
              autoCapitalize='none'
              borderBottomColor='#D3D3D3'
              borderBottomWidth={1}
              onChangeText={passWord => this.isSpacePassword(passWord)} />


          </View>

          {this.state.passError == true &&
            <View style={styles.alertBox}>
              <Text style={styles.phoneErrorText}> Please enter your password </Text>
            </View>
          }


          <View style={styles.signinContainer}>
            <TouchableOpacity style={styles.signinTouch} onPress={() => this.login()} >
              <Text style={styles.signinText}>SIGN IN</Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity style={styles.forgotPassContainer} onPress={() => this.props.navigation.navigate('ForgotPassword')} >
            <Text style={styles.forgotPassText}>Forgot Password?
                       </Text>
          </TouchableOpacity>

          <View style={styles.signinBottomLine}></View>
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text style={styles.firstLogin}>First Time Login?
                       </Text>
          </View>
          <View style={styles.activateContainer}>

            <TouchableOpacity style={styles.activateTouch}
              onPress={() => this.props.navigation.navigate('Activate')}>
              <Text style={styles.activateText}>ACTIVATE</Text>
            </TouchableOpacity>
          </View>
          {/* {this.renderFBLogin()} */}
          <View style={styles.fbContainer}>
            <TouchableOpacity style={styles.fbTouch} onPress={this._fbAuth} >
              <Image style={styles.fbIcon} source={require('../assets/fb.png')} />
              <Text style={styles.fbText}> CONTINUE WITH FACEBOOK</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.fbBottomLine}></View>

          <TouchableOpacity onPress={() => this.props.navigation.navigate('SignUp')}>
            <View style={styles.signupContainer}>
              <Text style={styles.signupText1}>Not a member? </Text>
              <Text style={styles.signupText2}> Sign Up today</Text>
            </View>
          </TouchableOpacity>
          <View>
            <Text style={styles.phoneText} > </Text>
            <Text style={styles.phoneText} > </Text>

          </View>
        </ImageBackground>
      </ScrollView>
    )
  }
}