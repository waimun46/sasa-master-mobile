import React, { Component } from 'react';
import { FlatList, StyleSheet, Text, View, Image, ScrollView, TextInput, TouchableOpacity, Dimensions, Alert, ActivityIndicator, AsyncStorage, } from 'react-native';
import Dialog, { DialogContent, SlideAnimation } from 'react-native-popup-dialog';
import Icon1 from 'react-native-vector-icons/Foundation';
import { ListItem, List, Left, Right } from 'native-base';
import Icon3 from 'react-native-vector-icons/MaterialCommunityIcons';
import StarRating from 'react-native-star-rating';

import Progressbar from 'react-native-loading-spinner-overlay';
import Service from '../config/Service';
import CanonicalPath from '../config/CanonicalPath';
import { futuraPtMedium, futuraPtBook } from '../styles/styleText'

export default class MakeUp extends Component {

  static navigationOptions = ({ navigation }) => ({
    title: `${navigation.state.params.name}`,
    headerRight: (
      <TouchableOpacity style={{ marginRight: 10 }} onPress={() => navigation.navigate('MyCartScreen')} >
        <Icon3 name="cart-outline" color='white' size={30} />
      </TouchableOpacity>
    ),
    headerStyle: {
      backgroundColor: 'black',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontSize: 22,
      fontFamily: futuraPtMedium
    },
  })

  goBack = () => this.props.navigation.goBack();

  constructor(props) {
    super(props);
    this.state = {
      isVisible1: false,
      selectBrands: false,
      selectedItems: 0,
      selectedItems1: 1,
      code: '',
      productList: [],
      minPrice: 0,
      maxPrice: 1000000,
      brandSelected: '',
      sortSelected: 'bestselling',
      item: {},
      cartList: [],
      subTotal: 0,
      offset: 0,
      limit: 10,
      modalCart: false,
      brands: [],
      firstTime: true,
      isLoadingMore: false,

    }

  }

  async componentWillMount() {
    const { navigation } = this.props;
    const code = navigation.getParam('code');
    this.setState({
      code: code,
    })

    await this.setStateAsync({ isLoading: true });
    this.product()
  }

  handleBackPress() {
    if (this.state.modalCart) {
      this.setState({ modalCart: false })
    } else {
      this.goBack()
    }
    return true
  }

  setStateAsync(state) {
    return new Promise((resolve) => {
      this.setState(state, resolve)
    });
  }

  onSuccess = async (filter, json) => {
    this.setStateAsync({ isLoading: false, isLoadingMore: false });
    console.log('onSuccess product ', filter, json)
    switch (filter) {
      case "LIST_PRODUCT":
        if (this.state.firstTime) {
          this.setState({
            offset: this.state.offset + this.state.limit,
            productList: json.product_list,
            brands: json.brand_list,
            firstTime: false,
          })
        } else {
          var joined = this.state.productList.concat(json.product_list);
          this.setState({
            offset: this.state.offset + this.state.limit,
            productList: joined,
            isLoadingMore: false,
          })
        }

        break
    }

  }

  onError = (filter, e) => {
    this.setStateAsync({ isLoading: false });

    Alert.alert(e)
  }

  async onEndReached(distance) {
    console.log('onEndReached ', distance.distanceFromEnd)
    if (distance.distanceFromEnd > 0) {

      this.setState({
        isLoadingMore: true,
      })

      this.product();
    }
  }

  async product() {
    if (this.state.isLoadingMore) { return }
    var data = {
      "category_id": this.props.navigation.getParam('code'),
      "sort": this.state.sortSelected,
      "offset": this.state.offset,
      "limit": this.state.limit,
    }
    if (this.state.minPrice) { data['price_min'] = this.state.minPrice }
    if (this.state.maxPrice) { data['price_max'] = this.state.maxPrice }
    if (this.state.brandSelected) { data['brand'] = this.state.brandSelected }

    var params = await {
      contentType: "multipart/form-data",
      Accept: "application/json",
      keepAlive: true,
      method: 'GET',
      useToken: true,
      data: data,
      canonicalPath: CanonicalPath.LIST_PRODUCT
    };

    console.log('loadProduct ', params)

    await Service.request(this.onSuccess, this.onError, "LIST_PRODUCT", params);
  }

  async addCart(item) {

    if (item.quantity == "0") {
      Alert.alert("Out of Stock")
      return;
    }

    var cartStorage = await AsyncStorage.getItem('cart')
    console.log('on addCart ', cartStorage, item)
    if (cartStorage != null) {
      var cart = JSON.parse(cartStorage)
      for (var i = 0; i < cart.length; i++) {
        if (cart[i].id == item.id) {
          this.setState({
            cartList: cart,
            modalCart: true,
          });
          this.subTotal()
          return;
        }
      }
    } else {
      var cart = [];
    }

    item.amount = 1;
    item.subTotal = ((parseFloat(item.offer_price)) == 0 ? item.price : item.offer_price);

    cart.push(item)
    await AsyncStorage.setItem('cart', JSON.stringify(cart))

    this.setState({
      cartList: cart,
      item: item,
      modalCart: true,
    });

    this.subTotal()

  }

  showDialog() {
    this.setState({
      modalCart: true,
      listImage: []
    });

  }

  async deleteItem(index) {
    var array = [...this.state.cartList]; // make a separate copy of the array
    if (index !== -1) {
      array.splice(index, 1);
      this.setState({ cartList: array });
    }

    await AsyncStorage.setItem('cart', JSON.stringify(array))

    this.subTotal()
  }

  changeAmount = async (value, index) => {

    // if (value == "") value = 1

    const newArray = [...this.state.cartList];
    newArray[index].amount = value;
    var subTotalProduct = value * (parseFloat(newArray[index].offer_price) == 0 ? newArray[index].price : newArray[index].offer_price);
    newArray[index].subTotal = parseFloat(subTotalProduct).toFixed(2);
    this.setState({ cartList: newArray });

    await AsyncStorage.setItem('cart', JSON.stringify(newArray))

    this.subTotal()
  }

  subTotal() {
    var subTotal = 0
    for (var i = 0; i < this.state.cartList.length; i++) {
      if (this.state.cartList[i].offer_price != 0) {
        var total = subTotal + parseFloat(this.state.cartList[i].offer_price) * (this.state.cartList[i].amount ? this.state.cartList[i].amount : 1);
      } else {
        var total = subTotal + parseFloat(this.state.cartList[i].price) * (this.state.cartList[i].amount ? this.state.cartList[i].amount : 1);
      }
      subTotal = total;
    }
    this.setState({
      subTotal: parseFloat(subTotal).toFixed(2),
    })
  }

  onCheckout() {

    if (this.state.cartList.length < 1) {
      return;
    }

    if (this.state.itemTotal < 25) {
      Alert.alert("Minimum Purchase of RM25 is required to checkout.")
      return;
    }

    for (var i = 0; i < this.state.cartList.length; i++) {
      if (this.state.cartList[i].amount === undefined || this.state.cartList[i].amount < 1) {
        Alert.alert("Please fill amount")
        return;
      } else if (this.state.cartList[i].amount > this.state.cartList[i].quantity) {
        Alert.alert(this.state.cartList[i].product_name + " Insufficient Stock")
        return;
      } else if (this.state.cartList[i].amount > 6) {
        Alert.alert(this.state.cartList[i].product_name + " Maximum Quantity Allowed Should be 6 Only")
        return;
      }
    }

    this.props.navigation.navigate('CheckoutScreen')
    this.setState({ modalCart: false })
  }

  async search() {
    await this.setStateAsync({ isLoading: true });
    this.setState({
      isVisible1: false,
      productList: [],
      offset: 0,
      limit: 10,
    })
    this.product();
  }

  async resetFilter() {
    await this.setStateAsync({ isLoading: true });
    this.setState({
      isVisible1: false,
      brandSelected: '',
      productList: [],
      minPrice: '',
      maxPrice: '',
      offset: 0,
      limit: 10,
    })
    this.product();
  }

  async sort(selectedItems1, sortSelected) {
    await this.setStateAsync({ isLoading: true });
    this.setState({
      visible: false,
      selectedItems1: selectedItems1,
      sortSelected: sortSelected,
      productList: [],
      offset: 0,
      limit: 10,
    });
    this.product();
  }

  renderCart = ({ item, index }) => {
    if (!item.image_url) { return null }
    return (
      <View key={index} style={{ flex: 1, flexDirection: 'row', }} >

        <Image style={{ width: 70, height: 80, marginTop: 7, borderWidth: 1, borderColor: 'grey' }} source={{ uri: item.image_url }}></Image>
        <View style={{ flex: 1, flexDirection: 'column', width: 200 }}>

          <Text numberOfLines={3} style={{ marginLeft: 4, marginTop: 4, fontSize: 16, fontFamily: futuraPtMedium, fontWeight: '500' }}>{item.product_name}</Text>
          <Text style={{ marginLeft: 4, marginTop: 4, fontSize: 16, textDecorationLine: 'line-through', fontFamily: futuraPtBook, fontWeight: '300' }}>{(parseFloat(item.offer_price)) == 0 ? "" : "RM" + item.price} </Text>
          <Text style={{ marginLeft: 4, marginTop: 4, fontSize: 16, color: '#ff4da6', fontFamily: futuraPtBook, fontWeight: '300' }}>{(parseFloat(item.offer_price)) == 0 ? "RM" + item.price : "RM" + item.offer_price} </Text>
        </View>

        <View style={{ flex: 1, flexDirection: 'column', alignItems: 'flex-end', }} >
          <TouchableOpacity style={{ marginRight: 10, marginTop: 5 }} onPress={() => this.deleteItem(index)}>
            <Icon1 name="trash" size={25} color="lightgrey" style={{}} />

          </TouchableOpacity>

          <View style={{ marginTop: 10, height: 40, width: 60, alignItems: 'flex-end', justifyContent: 'center' }} >
            <View style={{ flexDirection: 'row', }}>
              <TextInput
                style={{ height: 40, width: 40, borderColor: 'black', borderWidth: 0.5, padding: 10, }}
                keyboardType='numeric'
                onChangeText={(text) => this.changeAmount(text, index)}
                value={this.state.cartList[index].amount}
                defaultValue="1"
              />
            </View>
          </View>

          <Text style={{ fontSize: 14, color: 'black', marginTop: 5, fontFamily: futuraPtMedium, fontWeight: '300' }}>
            RM {this.state.cartList[index].subTotal}
          </Text>

        </View>

      </View>)
  }

  promotion(item) {

    if (item.extra_point && item.extra_point != "NONE") {
      return (
        <View style={{ justifyContent: "center", alignItems: 'center', resizeMode: 'cover', backgroundColor: '#F48FB1', marginLeft: 10 }}>
          <Text style={{ color: '#FFFFFF', alignItems: 'center', fontFamily: futuraPtBook }}>
            {item.extra_point} X POINTS
          </Text>
        </View>
      )
    } else if (item.buy_free && item.buy_free != "NONE") {
      return (
        <View style={{ justifyContent: "center", alignItems: 'center', resizeMode: 'cover', backgroundColor: '#F48FB1', marginLeft: 10 }}>
          <Text style={{ color: '#FFFFFF', alignItems: 'center', fontFamily: futuraPtBook }}>
            {item.buy_free}
          </Text>
        </View>
      )
    } else if (item.promotion_type && item.promotion_type != "NONE") {
      return (
        <View style={{ justifyContent: "center", alignItems: 'center', resizeMode: 'cover', backgroundColor: '#F48FB1', marginLeft: 10 }}>
          <Text style={{ color: '#FFFFFF', alignItems: 'center', fontFamily: futuraPtBook }}>
            {item.promotion_type}
          </Text>
        </View>
      )
    } else {
      return null;
    }
  }

  renderProduct = ({ item, index }) => {

    const { height, width } = Dimensions.get('window');
    const itemWidth = (width - 15) / 2;

    return (
      <View style={{ marginTop: 2, width: itemWidth, }}>
        <View style={{ flexDirection: 'column', flex: 1 }}>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('ProductDetailsScreen', {
            item: item,
          })}>
            <Image
              style={{
                marginLeft: 10,

                resizeMode: 'cover', height: 200, borderWidth: 1, borderColor: '#e5e5e5'
              }}
              source={{ uri: item.image_url }}
            />
          </TouchableOpacity>
          {this.promotion(item)}
          <View style={{ width: 100, height: 20, flex: 1, flexDirection: 'row', marginBottom: 4, marginTop: 8, marginLeft: 10 }}>
            <StarRating
              disabled={false}
              maxStars={5}
              rating={parseInt(item.rating)}
              starSize={18}
              fullStarColor={'#DAA520'} />
            <Text style={{ marginLeft: 10, fontSize: 14 }}>({item.rating_count})</Text>
          </View>
          <View style={{ height: 100 }}>
            <Text numberOfLines={3} style={{ marginLeft: 10, fontWeight: '300', fontFamily: futuraPtMedium, fontSize: 18, color: '#000000', marginTop: 5 }}>{item.product_name}</Text>
            <Text style={{ marginLeft: 10, fontFamily: futuraPtBook, fontSize: 16, fontWeight: '300', marginTop: 5, color: '#111111' }}>by {item.brand}</Text>
            <Text style={{ textDecorationLine: 'line-through', fontFamily: futuraPtBook, marginLeft: 10, marginTop: 5 }}>{(parseFloat(item.offer_price)) == 0 ? "" : "RM" + item.price}</Text>
          </View>
          {
            (item.quantity == '0') ?
              <Text style={{ marginLeft: 10, marginBottom: -10, fontFamily: futuraPtBook, fontSize: 16, fontWeight: '300', marginTop: 5, color: '#000000' }}>OUT OF STOCK</Text>
              :
              null
          }

          <List >
            <ListItem>
              <Left>
                <Text style={{ color: '#ff4da6', fontFamily: futuraPtBook, fontWeight: '300', fontSize: 20 }}>{(parseFloat(item.offer_price)) == 0 ? "RM" + item.price : "RM" + item.offer_price}</Text>
              </Left>
              <TouchableOpacity onPress={() => this.addCart(item)}>
                <Right>
                  <View style={{ backgroundColor: '#FFEBEE', height: 40, width: 40, justifyContent: 'center', alignItems: 'center', marginRight: -10, marginLeft: 10 }}>
                    <Image source={require('../assets/cart.png')} />
                  </View>
                </Right>
              </TouchableOpacity>
            </ListItem>
          </List>
        </View>
      </View>
    )
  }

  render() {
    return (
      <View style={{ backgroundColor: '#fff', flex: 1 }}>

        <Progressbar cancelable={true} visible={this.state.isLoading} overlayColor="rgba(0, 0, 0, 0.8)" animation="fade" textContent="" style={{ fontSize: 16, textAlign: 'center', justifyContent: 'center', height: 30, alignItems: 'center' }} />

        <View style={{ flexDirection: 'row', marginLeft: 40, marginRight: 40, justifyContent: 'space-between' }}>
          <TouchableOpacity style={styles.menuItems} onPress={() => {
            this.setState({ visible: true });
          }}>

            <Image style={styles.image1} source={require('../assets/ic_filter_list_24px.png')} />
            <Text style={styles.text1}> SORT BY </Text>


          </TouchableOpacity>
          <View style={{ backgroundColor: 'grey', height: 30, width: 1, justifyContent: 'center', alignItems: 'center', marginTop: 10, }}></View>
          <TouchableOpacity style={styles.menuItems} onPress={() => {
            this.setState({ isVisible1: true });
          }}>

            <Image style={styles.image1} source={require('../assets/ic_tune_24px.png')} />
            <Text style={styles.text1}> FILTER BY </Text>


          </TouchableOpacity>
        </View>
        <View style={{
          backgroundColor: '#D3D3D3',
          height: 0.50,
          marginTop: 10,
          marginRight: 0,
          marginLeft: 0
        }}></View>
        <ScrollView>
          <Dialog style={{ marginLeft: 10, marginRight: 10, }}
            visible={this.state.visible}
            containerStyle={{ justifyContent: 'flex-end' }}
            dialogAnimation={new SlideAnimation({
              slideFrom: 'bottom',
            })}
            rounded={false}
            width={Dimensions.get('window').width}
            height={250}
            onTouchOutside={() => {
              this.setState({ visible: false });
            }}
          >
            <DialogContent>
              <View style={styles.container}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                  <Text style={{ fontSize: 20, fontWeight: '300', color: 'black', marginTop: 20, fontFamily: futuraPtMedium }}>SORT BY </Text>
                  <TouchableOpacity onPress={() => {
                    this.setState({ visible: false });
                  }}>
                    <Image style={{ height: 25, width: 25, marginTop: 20 }} source={require('../assets/cross.png')} />
                  </TouchableOpacity>

                </View>

                <View style={{ height: 0.50, color: 'grey', marginTop: 10 }}></View>

                <View>
                  <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'space-between' }}
                    onPress={() => {
                      this.sort(1, 'bestselling');
                    }}>
                    {this.state.selectedItems1 === 1 ? <Text style={styles.textSortSelected}>Bestsellers</Text> : <Text style={styles.textSort}>Bestsellers</Text>}
                    {this.state.selectedItems1 === 1 ? <Image style={{ marginTop: 20 }} source={require('../assets/tick.png')} /> : null}
                  </TouchableOpacity>

                  <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'space-between' }}
                    onPress={() => {
                      this.sort(0, 'product_name');
                    }}>
                    {this.state.selectedItems1 === 0 ? <Text style={styles.textSortSelected}>Product Name A-Z</Text> : <Text style={styles.textSort}>Product Name A-Z</Text>}
                    {this.state.selectedItems1 === 0 ? <Image style={{ marginTop: 20 }} source={require('../assets/tick.png')} /> : null}

                  </TouchableOpacity>

                  <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'space-between' }}
                    onPress={() => {
                      this.sort(2, 'price_low');
                    }}>
                    {this.state.selectedItems1 === 2 ? <Text style={styles.textSortSelected}>Price Low-High</Text> : <Text style={styles.textSort}>Price Low-High</Text>}
                    {this.state.selectedItems1 === 2 ? <Image style={{ marginTop: 20 }} source={require('../assets/tick.png')} /> : null}
                  </TouchableOpacity>
                  <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'space-between' }}
                    onPress={() => {
                      this.sort(3, 'price_high');
                    }}>
                    {this.state.selectedItems1 === 3 ? <Text style={styles.textSortSelected}>Price High-Low</Text> : <Text style={styles.textSort}>Price High-Low</Text>}
                    {this.state.selectedItems1 === 3 ? <Image style={{ marginTop: 20 }} source={require('../assets/tick.png')} /> : null}
                  </TouchableOpacity>

                </View>

              </View>
            </DialogContent>
          </Dialog>
        </ScrollView>
        <Dialog style={{ marginLeft: 10, marginRight: 10, }}
          visible={this.state.isVisible1}
          containerStyle={{ justifyContent: 'flex-end' }}
          dialogAnimation={new SlideAnimation({
            slideFrom: 'bottom',
          })}
          rounded={false}
          width={Dimensions.get('window').width}
          height={300}
          onTouchOutside={() => {
            this.setState({ isVisible1: false });
          }}
        >
          <DialogContent>
            <View style={styles.container}>
              <TouchableOpacity
                style={{ flexDirection: 'row', justifyContent: 'space-between' }}
                onPress={() => {
                  // this.setState({ isVisible1: false, minPrice: '', maxPrice: '', brandSelected: '' });
                  // this.product()
                  this.resetFilter();

                }}
              >
                <Text style={{ fontSize: 20, fontWeight: '300', color: 'black', marginTop: 20, fontFamily: futuraPtMedium }}>FILTER BY </Text>
                <Image style={{ height: 25, width: 25, marginTop: 20 }} source={require('../assets/cross.png')} />

              </TouchableOpacity>
              <View style={{ marginTop: 10, backgroundColor: 'grey', height: 1 }}></View>

              <Text style={{ fontSize: 18, color: '#111111', marginTop: 20, fontWeight: '300', fontFamily: futuraPtMedium }}>Price Range</Text>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <View style={{ height: 40, width: 150, backgroundColor: '#EEEEEE', marginTop: 10 }}>
                  <TextInput style={{ fontSize: 15, justifyContent: 'center', marginLeft: 10, fontFamily: futuraPtBook, fontWeight: '300' }} placeholder='Min'
                    value={this.state.minPrice}
                    keyboardType="numeric"
                    onChangeText={minPrice => this.setState({ minPrice })}></TextInput>
                </View>
                <View style={{ height: 40, width: 150, backgroundColor: '#EEEEEE', marginTop: 10 }}>
                  <TextInput style={{ fontSize: 15, justifyContent: 'center', marginLeft: 10, fontFamily: futuraPtBook, fontWeight: '300' }} placeholder='Max'
                    value={this.state.maxPrice}
                    keyboardType="numeric"
                    onChangeText={maxPrice => this.setState({ maxPrice })}></TextInput>
                </View>


              </View>



              <Text style={{ fontSize: 18, color: '#111111', marginTop: 20, fontWeight: '300', fontFamily: futuraPtMedium }}>Brands</Text>

              <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'space-between' }} onPress={() => {
                this.setState({ selectBrands: true || this.setState({ visible1: false }) });
              }}>

                <Text style={{ fontSize: 16, marginTop: 20, fontWeight: '300', fontFamily: futuraPtBook, color: '#111111' }}>
                  {this.state.brandSelected ? this.state.brandSelected : 'Select Brands'}
                </Text>
                <Image style={{ height: 25, width: 25, marginTop: 20 }} source={require('../assets/forward.png')} />
              </TouchableOpacity>

              <Dialog style={{ marginLeft: 10, marginRight: 10, }}
                visible={this.state.selectBrands}
                containerStyle={{ justifyContent: 'flex-end' }}
                dialogAnimation={new SlideAnimation({
                  slideFrom: 'bottom',
                })}
                rounded={false}
                width={Dimensions.get('window').width}
                height={300}
                onTouchOutside={() => {
                  this.setState({ selectBrands: false });
                }}
              >
                <DialogContent>
                  <View style={styles.container}>

                    <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'space-between' }} onPress={() => {
                      this.setState({ selectBrands: false })
                    }}>
                      <Image style={{ height: 30, width: 30, marginTop: 18 }} source={require('../assets/arrowback.png')} />

                      <Text style={{ fontSize: 20, fontWeight: '300', color: 'black', marginTop: 20, marginRight: 140, fontFamily: futuraPtMedium }}>SELECT BRANDS </Text>

                      <Image style={{ height: 25, width: 25, marginTop: 20 }} source={require('../assets/cross.png')} />

                    </TouchableOpacity>

                    <ScrollView>
                      <View>
                        {
                          this.state.brands.map((brand, iter) => {
                            const selected = this.state.selectedItems == iter;
                            return (
                              <TouchableOpacity
                                style={{ flexDirection: 'row', justifyContent: 'space-between' }}
                                onPress={() => {
                                  this.setState({ selectBrands: false, selectedItems: iter, brandSelected: brand.brand_name })
                                }}
                              >
                                <Text style={{ fontSize: 15, color: (selected ? '#E4007C' : '#000'), marginTop: 20, fontFamily: futuraPtMedium }}>{brand.brand_name}</Text>
                                {selected && <Image style={{ marginTop: 20 }} source={require('../assets/tick.png')} />}
                              </TouchableOpacity>
                            )
                          })
                        }
                      </View>
                    </ScrollView>
                  </View>
                </DialogContent>
              </Dialog>
              <TouchableOpacity
                style={styles.btnSearch}
                onPress={() => {
                  this.search();
                }}
              >
                <Text style={styles.textSearch}>Search</Text>
              </TouchableOpacity>
            </View>
          </DialogContent>
        </Dialog>

        {
          (this.state.productList.length > 0) ?
            <FlatList
              vertical
              data={this.state.productList}
              onEndReached={this.onEndReached.bind(this)}
              onEndReachedThreshold={0.5}
              numColumns={2}
              keyExtractor={(item, index) => index.toString()}
              ListFooterComponent={() => {
                return (
                  (!this.state.isLoadingMore) ? null
                    :

                    <View
                      style={{
                        paddingVertical: 20,
                        borderTopWidth: 1,
                        borderColor: "#CED0CE"
                      }}
                    >
                      <ActivityIndicator animating size="large" />
                    </View>
                );
              }}
              renderItem={item => this.renderProduct(item)}>
            </FlatList>

            :
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
              <Text>No Record</Text>
            </View>

        }

        <Dialog
          onHardwareBackPress={this.handleBackPress.bind(this)}
          visible={this.state.modalCart}
          dialogAnimation={new SlideAnimation({
            slideFrom: 'bottom',
          })}
          rounded={false}
          width={300}
          height={400}
          onTouchOutside={() => {
            this.setState({ modalCart: false });
          }}
        >
          <DialogContent>
            <ScrollView style={{ backgroundColor: '#FFF' }}>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>

                <Text style={{ fontSize: 18, color: 'black', fontFamily: futuraPtMedium }}>ADD TO CART </Text>

                <Image style={{ marginTop: 10 }} ource={require('../assets/cross.png')} />
              </View>
              <View style={{ marginTop: 5, backgroundColor: 'grey', height: 0.5, width: '100%' }}></View>

              {
                (this.state.cartList.length > 0) ?
                  <FlatList
                    vertical
                    data={this.state.cartList}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={this.renderCart.bind(this)}>
                  </FlatList>

                  :
                  <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                    <Text>No Record</Text>
                  </View>

              }

              <Text style={{ fontSize: 14, fontFamily: futuraPtBook }}> </Text>


              <Text style={{ fontSize: 14, fontFamily: futuraPtBook }}>TOTAL</Text>
              <Text style={{ fontSize: 16, color: 'black', marginTop: 5, fontFamily: futuraPtMedium, fontWeight: '300' }}>RM {this.state.subTotal}</Text>

              <View style={{ marginTop: 10, marginLeft: 10, marginRight: 10, height: 50 }}>
                <TouchableOpacity style={{ backgroundColor: '#E4007C', justifyContent: "center", alignItems: 'center', height: 50 }}
                  onPress={() => this.onCheckout()}>
                  <Text style={{ color: 'white', fontSize: 18, fontFamily: futuraPtMedium }}>Checkout</Text>
                </TouchableOpacity>
              </View>

              <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', marginTop: 10 }} onPress={() => {
                this.setState({ modalCart: false });
              }}>
                <Text style={{ fontSize: 18, color: '#E4007C', fontFamily: futuraPtBook }}> Continue Shopping </Text>
              </TouchableOpacity>

            </ScrollView>
          </DialogContent>
        </Dialog>


      </View >




    );
  }

}
const styles = StyleSheet.create({
  menuItems: {
    flexDirection: 'row',
    justifyContent: 'center',
    height: 50,


  },
  gridView: {
    flex: 1
  },
  image1: {
    height: 20,
    width: 25,

    marginTop: 15
  },
  text1: {
    fontSize: 20,
    marginLeft: 10,
    marginTop: 15,

    fontWeight: '400',
    fontFamily: futuraPtBook,
    color: '#111111'
  },
  image2: {
    height: 30,
    width: 30,
    marginLeft: 30,
    marginTop: 10

  },
  text2: {
    fontSize: 15,
    marginTop: 15
  },
  cardContainer: {
    flexDirection: 'row',

    marginTop: 10,
    padding: 10
  },
  textSort: {
    fontSize: 16, marginTop: 20, fontWeight: '300', fontFamily: futuraPtBook
  },
  textSortSelected: {
    fontSize: 16, marginTop: 20, fontFamily: futuraPtMedium, color: '#E4007C',
  },
  btnSearch: {
    marginVertical: 10,
    backgroundColor: '#E4007C',
    justifyContent: "center",
    alignItems: 'center',
    height: 45
  },
  textSearch: {
    color: 'white',
    fontSize: 16,
    fontFamily: futuraPtMedium,
    fontWeight: '500'
  }

})
