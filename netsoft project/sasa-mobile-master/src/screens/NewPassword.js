import { createStackNavigator, StackActions, NavigationActions } from 'react-navigation'; // Version can be specified in package.json
import { Col, Row, Grid } from "react-native-easy-grid";
import React, {Component} from 'react';
import styles from '../styles/styleNewPassword'
import {Platform, StyleSheet, Text, View,Image,ScrollView,TextInput,TouchableOpacity,ImageBackground,Dimensions} from 'react-native';
import { Container, Header, Content, Button,ListItem,List  } from 'native-base';
import { futuraPtMedium } from '../styles/styleText'



export default class NewPassword extends Component {
  static navigationOptions=({
    title:'New Password',
  headerLeft:null,
  headerStyle:{
      backgroundColor:'black'
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontSize: 22,
    fontFamily:futuraPtMedium,
    fontWeight:'500'
    },
})

constructor(props){
    super(props);
     this.state={
     check:true,
     selectedIndex:0,
     newPassword:'',
     newPasswordError:false,
     confirmPassword:'',
     confirmPasswordError:false

   }
}


    validation=()=>{
         this.setState({
            newPasswordError:false,
            confirmPasswordError:false
        })

        if(this.state.newPassword==''){
            this.setState({
            newPasswordError:true
            })
        }
        else if(this.state.confirmPassword != this.state.newPassword){
            this.setState({
            confirmPasswordError:true
            })
        }
        else{
               this.props.navigation.navigate('PhoneVerification1')
        }

      }

    render() {
        return (
            <ScrollView   style={styles.containerBackground}>

                <View style={styles.lineDown}></View>
                <View style={styles.viewNewPsw}>
                    <Text  style={styles.textNewPsw} >New Password</Text>
                    <TextInput  style={styles.inputNewPsw} placeholder='**********' secureTextEntry={true}
                    onChangeText={newPassword => this.setState({ newPassword })}
                    />
                </View>
                <View style={styles.lineDown}></View>

               {this.state.newPasswordError==true &&
                <View>
                     <Text style={styles.pswError}> Please enter password </Text>
                </View>
                }


                <View style={styles.viewConfirmPsw}>
                    <Text  style={styles.textConfirmPsw} >Confirm New Password</Text>
                    <TextInput  style={styles.inputConfirmPsw} placeholder='**********' secureTextEntry={true}
                   onChangeText={confirmPassword => this.setState({ confirmPassword })}
                    />
                </View>
                <View style={styles.lineDown}></View>

                {this.state.confirmPasswordError==true &&
                <View>
                    <Text style={styles.conFirError}> Password and confirm password must be same </Text>
                </View>
                }

                <View style={styles.viewButton}>
                    <TouchableOpacity   style={styles.touch}
                      onPress={ this.validation} >
                        <Text style={styles.textButton}>CHANGE PASSWORD</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
         );
       }
    }
