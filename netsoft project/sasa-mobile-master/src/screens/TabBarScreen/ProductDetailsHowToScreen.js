

import { createStackNavigator, StackActions, NavigationActions } from 'react-navigation'; // Version can be specified in package.json
import { Col, Row, Grid } from "react-native-easy-grid";
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Image,ScrollView,TextInput,TouchableOpacity,ImageBackground,Dimensions} from 'react-native';
import { Container, Header, Content, Button,ListItem,List  } from 'native-base';
import styles from '../../styles/styleProductDetailsHowTo';
import StarRating from 'react-native-star-rating';
import { futuraPtMedium } from '../../styles/styleText'

const dimWidth= Dimensions.get('window').width
const dimHeight= Dimensions.get('window').height

const backgroundColor='#fff'
const buttonColor='#ff4da6'
const textColor='#fff'

export default class ProductDetailsHowToScreen extends Component {
  static navigationOptions=({
      title:'',
    headerLeft:null,
    headerStyle:{
        backgroundColor:'black'
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
        fontSize: 22,
        fontFamily:futuraPtMedium,
        fontWeight:'500'
      },
})


  render() {
    return (
       <ScrollView   style={{backgroundColor:backgroundColor}}>
           {/******************** v 1  : Product Image ***************************** */}
            <View style={styles.container}>
                <View style={{padding:8}}>
                    <Image  style={{width: 150 }} source={require('../../assets/deo.png')}></Image>
                </View>
            </View>
            {/******************** Heart Image ***************************** */}

            <View style={styles.rightContainer}>
                <View style={{flex: 1, flexDirection: 'row',  justifyContent: 'flex-start', }}>

                </View>
                <View style={styles.rightIcon}>
                    <Image  style={styles.styleTextRight}  source={require('../../assets/icon_heart.png')}></Image>
                </View>
            </View>


            {/********************* v 2 : Product Details **************************** */}

            <View style={styles.productDetailContainer}>
                    <Text style={{marginLeft:4,marginTop:8,fontSize:14,color:'#ff4da6'}}>Silkygirl</Text>
                    <Text style={{marginLeft:4,marginTop:4,fontSize:18}}>Cyber Colors - Serum Lipstics  </Text>
                    <Text style={{marginLeft:4,marginTop:4,fontSize:16,textDecorationLine: 'line-through'}}>RM14.60 </Text>
                    <View style={{ flex: 1 , flexDirection:'row',}}>
                        <Text style={{marginLeft:4,marginTop:4,fontSize:16,color:'#ff4da6'}}>RM9.90 </Text>
                        <Button rounded style={{width:60, height:30,backgroundColor:'#ff4da6',justifyContent:"center",alignItems:'center', marginLeft:8 }}>
                            <Text style={{color:'#fff' }}>Buy</Text>
                        </Button>
                    </View>
                    {/************ Sub view : Rating Bar ****************** */}
                    <View style={{width:120,flex: 1 , flexDirection:'row',marginBottom:16,marginTop:10}}>
                        <StarRating
                            disabled={false}
                            maxStars={5}
                            rating= {3.5}
                            starSize={20}
                            fullStarColor = {'#DAA520'} />
                            <Text style={{marginLeft:4,marginTop:4,fontSize:14}}>(37)</Text>
                    </View>
            </View>
            {/********************* v3 : Blank View **************************** */}

             <View style={{height:20,backgroundColor:'#A9A9A9'}}>
            </View>
             {/********************* Text Description **************************** */}
             <View style={styles.productDetailContainer}>
                 <Text style={styles.textHeadingOne}>Step 1: Moisturizer</Text>
                 <Text style={styles.textHeadingTwo}>Before you begin applying your makeup, take the time to prep your skin with a high quality moisturizer. Choosing the right kind of moisturizer.</Text>
             </View>

                 {/********************* Button  **************************** */}
              <View>

                            <View style={styles.btnContainer}>
                                    <TouchableOpacity   style={styles.btnTouch}  >
                                    <Image style={{marginRight:8}}  source={require('../../assets/icon_add_shopping_cart.png')}></Image>
                                        <Text style={styles.btnText}>ADD To CART</Text>

                                    </TouchableOpacity>
                            </View>
               </View>
          </ScrollView>
    );
  }
}
