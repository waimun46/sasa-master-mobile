import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, ScrollView, TextInput, TouchableOpacity, ImageBackground, Dimensions, Alert, StatusBar, FlatList, SectionList, ActivityIndicator } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import Icon3 from 'react-native-vector-icons/MaterialCommunityIcons';
import { DASHBOARD_PRODUCT_URL } from '../../config/Global'
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
const backgroundColor = '#fff'

import Progressbar from 'react-native-loading-spinner-overlay';
import Service from '../../config/Service';
import CanonicalPath from '../../config/CanonicalPath';
import Config from '../../config/Config';
import { futuraPtMedium } from '../../styles/styleText'

import OneSignal from 'react-native-onesignal';

const OS = Platform.OS;

export default class HomeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isVisible: true,
            seCode: '',
            accessToken: '',
            itemsArray: null,
            countList: '',
            animating: true,
            isError: false,
            photos: '',
            tabIcon: false,
            promotions: [],
            activeSlide: 0,
        }

        let requiresConsent = false;
        OneSignal.setLogLevel(6, 0);
        OneSignal.setRequiresUserPrivacyConsent(requiresConsent);
        const id = OS == 'ios' ? Config.oneSignalIosId : Config.oneSignalAppId;
        OneSignal.init(id, {kOSSettingsKeyAutoPrompt : true});

    }

    async componentDidMount(){
        OneSignal.setLocationShared(true);

        OneSignal.inFocusDisplaying(2)

        OneSignal.addEventListener('received', this.onReceived);
        OneSignal.addEventListener('opened', this.onOpened);
    }

    componentWillMount() {
        this.getData()

        OneSignal.removeEventListener('received', this.onReceived);
        OneSignal.removeEventListener('opened', this.onOpened);
    }

    onReceived(notification) {
        console.log("Notification received: ", notification);

    }

    onOpened(openResult) {
      console.log('Message: ', openResult.notification.payload.body);
      console.log('Data: ', openResult.notification.payload.additionalData);
      console.log('isActive: ', openResult.notification.isAppInFocus);
      console.log('openResult: ', openResult);

    }

    getData = () => {
        fetch(DASHBOARD_PRODUCT_URL + '?secode=sasaapp20181019', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            },

        })
            .then((serviceResponse) => { return serviceResponse.json() })
            .catch((error) => console.warn("fetch error:", error))
            .then((serviceResponse) => {

                obj = serviceResponse;
                var count = Object.keys(obj).length
                // Alert.alert(count.toString())
                this.setState({ countList: count })
                this.setState({ itemsArray: obj })

                this.getPromotions()
            });
    }

    onSuccess = async (filter, json) => {
        // this.setStateAsync({ isLoading: false });

        switch (filter) {
            case "CONNECT_ONESIGNAL":
                console.log("one signal success")
                break;
            case "PROMOTIONS":
                for(var i = 0; i < json.length; i++){
                    if(json[i].on_carousell == "0"){
                        json.splice(i, 1);
                    }
                }
                this.setState({
                    promotions: json
                })
                this.connect_onesignal()

                break;
        }

    }

    onError = (filter, e) => {
        // this.setStateAsync({ isLoading: false });

        Alert.alert(e)
    }

    async getPromotions() {

        var params = await {
            contentType: "multipart/form-data",
            Accept: "application/json",
            keepAlive: true,
            method: 'GET',
            useToken: true,
            data: {},
            canonicalPath: CanonicalPath.PROMOTIONS
        };

        await Service.request(this.onSuccess, this.onError, "PROMOTIONS", params);
    }

    async connect_onesignal() {

        var formData = new FormData();

        formData.append('onesignal_id', Config.oneSignalAppId);

        var params = await {
            contentType: "multipart/form-data",
            Accept: "application/json",
            keepAlive: true,
            method: 'POST',
            useToken: true,
            data: formData,
            canonicalPath: CanonicalPath.CONNECT_ONESIGNAL
        };

        await Service.request(this.onSuccess, this.onError, "CONNECT_ONESIGNAL", params);

    }

    renderItems() {

        return this.state.itemsArray.map((data) => {
            return (
                <View style={{ flex: 1 }}>
                    <Text style={{ fontSize: 23, marginLeft: 10, marginTop: 20, fontFamily: futuraPtMedium, fontWeight: '400', color: '#111111' }}>{data.name}
                    </Text>
                    <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', }}
                        onPress={() => this.props.navigation.navigate('MakeUp', {
                            code: data.code,
                            name: data.name
                        })}>

                        {
                            (data.image_url === "") ?
                                <Image style={{ width: '90%', height: 170, resizeMode: 'contain' }} source={require('../../assets/image_not_found.png')} />
                                :
                                <Image style={{ width: '90%', height: 180, resizeMode: 'contain' }} source={{ uri: data.image_url }} />
                        }

                    </TouchableOpacity>
                </View>
            );
        })
    }

    renderSwiper({ item, index }) {
        return (
            <View style={{ backgroundColor: '#fff' }}>
                <TouchableOpacity
                    // onPress={() => this.props.navigation.navigate('TrickNdTreat', { data: item })}>
                    onPress={() =>
                        (item.type === 4 ?
                            this.props.navigation.navigate('TrickNdTreat', { data: item })
                            :
                            this.props.navigation.navigate('DetailPromotion', { data: item }))
                    }>
                    {
                        (item.image_url === "") ?
                            <Image style={{ width: '100%', height: 150, resizeMode: 'contain' }} source={require('../../assets/image_not_found.png')} />
                            :
                            <Image style={{ width: '100%', height: 150, resizeMode: 'contain' }} source={{ uri: item.image_url }} />
                    }
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        let bounds = Dimensions.get('window');
        let width = bounds.width - 60

        var cardView = [];
        const itemsArray = this.state.itemsArray;
        if (itemsArray === null) {
            if (this.state.isError === true) {

            }
            else {
                return <View style={{
                    justifyContent: 'center',
                    alignItems: 'center', flex: 1,
                }}>
                    <ActivityIndicator size="large" color="red" animating={true} />

                </View>;
            }

        }
        return (
            <View>
                <StatusBar
                    backgroundColor="black"
                    barStyle="light-content"
                />
                <ScrollView scrollEnabled style={{ backgroundColor: backgroundColor }}>

                    <View>

                        <Text style={{ fontSize: 23, marginLeft: 10, marginTop: 10, fontFamily: futuraPtMedium, fontWeight: '400', color: '#111111', marginBottom: 5 }}> PROMOTIONS</Text>
                        <View style={{ justifyContent: 'center', alignItems: 'center', marginLeft: 10, marginRight: 10, marginBottom: 10 }}>

                            <Carousel
                                ref={(c) => this._carousel = c}
                                data={this.state.promotions}
                                renderItem={this.renderSwiper.bind(this)}
                                sliderWidth={bounds.width}
                                sliderHeight={150}
                                itemWidth={width}
                                itemHeight={150}
                                onSnapToItem={(index) => this.setState({ activeSlide: index })}
                                loop={true}
                            />
                            <Pagination
                                style
                                dotsLength={this.state.promotions.length}
                                activeDotIndex={this.state.activeSlide}
                                containerStyle={{ backgroundColor: '#00000000', paddingTop: 0, paddingBottom: 0, marginTop: -20 }}
                                dotStyle={{
                                    width: 10,
                                    height: 10,
                                    borderRadius: 5,
                                    backgroundColor: 'white'
                                }}
                                inactiveDotOpacity={0.4}
                                inactiveDotScale={0.6}
                            />

                        </View>
                    </View>

                    {this.renderItems()}
                </ScrollView>
            </View>

        );
    }
}
