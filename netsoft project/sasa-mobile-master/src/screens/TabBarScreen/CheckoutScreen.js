
import React, { Component } from 'react';
import { Platform, AsyncStorage, Text, View, Image, ScrollView, TextInput, TouchableOpacity, PermissionsAndroid, Dimensions, Alert } from 'react-native';
import { Container, Header, Content, Button, ListItem, List, Left, Icon, Right } from 'native-base';
import styles from '../../styles/styleCheckout';
import Icon1 from 'react-native-vector-icons/dist/SimpleLineIcons';
import styles1 from '../../styles/styleStoreLocationListView'
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";

import Progressbar from 'react-native-loading-spinner-overlay';
import Service from '../../config/Service';
import CanonicalPath from '../../config/CanonicalPath';
import { futuraPtMedium } from '../../styles/styleText'

export default class CheckoutScreen extends Component {
    static navigationOptions = ({
        title: 'Checkout',
        headerLeft: null,
        headerStyle: {
            backgroundColor: 'black'
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontSize: 22,
            fontFamily: futuraPtMedium,
            fontWeight: '500'
        },
    })
    constructor(props) {
        super(props);
        this.state = {
            check: true,
            selectedIndex: 0,
            isProductPick: false,

            myname: '',
            contactNo: '',
            myEmail: '',

            nameError: false,
            numberError: false,
            emailError: false,

            location: null,
            itemId: null,

        }
    }
    onStoreLocation = async () => {

        if( Platform.OS == 'ios' ){
            this.props.navigation.navigate('SelectStoreScreen', { showCollectHere: true })
            return true;
        }

        const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);

        if (granted) {
            console.log("You can use the ACCESS_FINE_LOCATION")
            if (Platform.OS === 'android') {
                LocationServicesDialogBox.checkLocationServicesIsEnabled({
                    message: "<h2>Use Location?</h2> \
                            This app wants to change your device settings:<br/><br/>\
                            Use GPS for location<br/><br/>",
                    ok: "YES",
                    cancel: "NO"
                }).then(() => {
                    this.props.navigation.navigate('SelectStoreScreen', { showCollectHere: true })
                })
            }
        }
        else {
            console.log("ACCESS_FINE_LOCATION permission denied")
        }


        // this.props.navigation.navigate('SelectStoreScreen',
        //     { onSelectPickup: this.onSelectPickup})

    }

    onSelectPickup = data => {
        this.setState({
            cartFill: data.cartFill,
            location: data.location,
        });
    };

    checkout = async () => {

        var locationStorage = await AsyncStorage.getItem('location')
        // var location = JSON.parse(locationStorage)

        if (locationStorage === null) {
            Alert.alert("Please select store to pickup your order.");
            return;
        }

        this.setState({
            nameError: false,
            numberError: false,
            emailError: false
        })

        if (this.state.myname == '') {
            this.setState({
                nameError: true
            })
        }
        else if (this.state.contactNo == '') {
            this.setState({
                numberError: true
            })
        }

        else if (this.state.myEmail == '') {
            this.setState({
                emailError: true
            })
        }


        else {
            if (this.state.myEmail != '') {

                var validator = require("email-validator");

                if (validator.validate(this.state.myEmail)) {

                    this.summary()

                }
                else {
                    this.setState({
                        emailError: true
                    })
                }

            }

        }
    }
    isEmail(value) {

        this.setState({
            myEmail: value.replace(/\s/g, '')
        })
    }

    setStateAsync(state) {
        return new Promise((resolve) => {
            this.setState(state, resolve)
        });
    }

    onSuccess = async (filter, json) => {
        this.setStateAsync({ isLoading: false });

        switch (filter) {
            case "SUMMARY":
                console.log("berhasil", json)

                var user = {
                    name: this.state.myname,
                    phone: this.state.contactNo,
                    email: this.state.myEmail,
                }

                this.props.navigation.navigate('CheckoutPaymentScreen', { user: user, data: json[0] })
                break
            case "GET_DETAIL":

                var status = json[0].status;
                if (status == 1) {

                    this.setState({
                        myname: json[0].name,
                        contactNo: json[0].phone,
                        myEmail: json[0].email,
                    })

                } else {
                    Alert.alert(json[0].error)
                }

                break
        }

    }

    onError = (filter, e) => {
        this.setStateAsync({ isLoading: false });

        Alert.alert(e)
    }

    async get_detail() {
        await this.setStateAsync({ isLoading: true });

        var params = await {
            contentType: "multipart/form-data",
            Accept: "application/json",
            keepAlive: true,
            method: 'GET',
            useToken: true,
            data: {},
            canonicalPath: CanonicalPath.GET_DETAIL
        };

        await Service.request(this.onSuccess, this.onError, "GET_DETAIL", params);
    }

    async summary() {

        await this.setStateAsync({ isLoading: true });

        var formData = new FormData();

        var cartStorage = await AsyncStorage.getItem('cart')
        let cartList = JSON.parse(cartStorage);

        for (var i = 0; i < cartList.length; i++) {
            formData.append('products[' + i + '][0]', cartList[i].id);
            formData.append('products[' + i + '][1]', cartList[i].amount);
        }

        var params = await {
            contentType: "multipart/form-data",
            Accept: "application/json",
            keepAlive: true,
            method: 'POST',
            useToken: true,
            data: formData,
            canonicalPath: CanonicalPath.SUMMARY
        };

        await Service.request(this.onSuccess, this.onError, "SUMMARY", params);

        // var user = {
        //     name: this.state.myname,
        //     phone: this.state.contactNo,
        //     email: this.state.myEmail,
        // }

        // this.props.navigation.navigate('CheckoutPaymentScreen', { user: user })
    }

    componentDidMount() {
        this.get_detail()
        this.setState({
            itemId: this.props.navigation.getParam('cartFill')
        })
    }

    render() {
        const { navigation } = this.props;
        const itemId = navigation.getParam('cartFill');
        const location = navigation.getParam('location')
        return (

            <ScrollView style={styles.container} scrollEnabled={true} >

                <Progressbar cancelable={true} visible={this.state.isLoading} overlayColor="rgba(0, 0, 0, 0.8)" animation="fade" textContent="" style={{ fontSize: 16, textAlign: 'center', justifyContent: 'center', height: 30, alignItems: 'center' }}></Progressbar>

                <View style={{ backgroundColor: '#fff' }}>
                    <Text style={styles.textHeader} > Personal Information</Text>



                    {/* ***************NAME**************************** */}
                    <View>
                        <Text style={styles.nameText} > Name</Text>
                        <TextInput style={styles.nameInputText}
                            placeholder='Please enter name'
                            value={this.state.myname}
                            onChangeText={myname => this.setState({ myname })}
                        />
                    </View>
                    <View style={styles.nameBottomLine}></View>
                    {this.state.nameError == true &&
                        <View>
                            <Text style={styles.nameError}> Please Enter your name </Text>
                        </View>
                    }


                    {/* ***************Phone Number**************************** */}
                    <View>
                        <Text style={styles.phoneText} > Phone Number</Text>
                        <TextInput
                            style={styles.phoneInputText}
                            keyboardType="numeric"
                            placeholder='Please enter phone number'
                            value={this.state.contactNo}
                            onChangeText={contactNo => this.setState({ contactNo })}
                        />
                    </View>
                    <View style={styles.phoneBottomLine}></View>
                    {this.state.numberError == true &&
                        <View>
                            <Text style={styles.phoneError}> Please Enter your phone number </Text>
                        </View>
                    }


                    {/* ***************Email Address**************************** */}
                    <View>
                        <Text style={styles.emailText} > Email Address</Text>
                        <TextInput style={styles.emailInputText} placeholder='Please enter email address'
                            onChangeText={myEmail => this.isEmail(myEmail)}
                            value={this.state.myEmail}
                        />
                    </View>
                    <View style={styles.nameBottomLine}></View>
                    {this.state.emailError == true &&
                        <View>
                            <Text style={styles.emailError}> Please Enter valid Email address</Text>
                        </View>
                    }


                    {/**************************  MSG 1 *********************************** */}

                    <View>
                        <Text style={styles.msgOneText} >
                            Please make sure your name and phone number are accurate, as we will send you order confirmation and collection notice via SMS.
                </Text>

                    </View>


                    <View style={{ height: 20, backgroundColor: '#EEEEEE' }} />


                    <List>
                        <ListItem>
                            <Left>
                                <TouchableOpacity style={styles.optionView} onPress={this.onStoreLocation}>
                                    {/* <Image style={styles.viewTextTop} source={require('../../assets/icon_shopping_bag.png')}></Image> */}
                                    <Icon1 name="bag" size={25} color="black" style={{ marginRight: 10 }} />
                                    <View style={styles.viewTextTop}>
                                        <Text style={styles.textHeading}>Select Store to Pick Up</Text>
                                    </View>
                                </TouchableOpacity>
                            </Left>
                            <Right>
                                <Icon style={{ marginRight: 4 }} name="arrow-forward" />
                            </Right>
                        </ListItem>
                    </List>


                    {
                        itemId !== "yes" ?
                            // this.state.itemId !== "yes" ?
                            <View>
                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={styles.msgTwoText} >
                                        Please select store to pickup your order.
                                    </Text>
                                </View>
                            </View> :

                            <View>
                                <View style={styles.viewText}>
                                    <View style={styles1.styleViewOne} >


                                        {/* <Text style={{ fontSize: 15, color: 'black' }}>1</Text> */}

                                        <Text style={{ color: '#000', fontWeight: '600' }}> {location.name} </Text>
                                        <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                                            <Text style={styles1.styleTextRight}>{parseFloat(location.distance_in_km).toFixed(2)}  km </Text>
                                        </View>

                                    </View>

                                    <View style={styles1.styleViewOne}>
                                        <Image />
                                        <Text style={{ color: '#000' }}>{location.address} </Text>
                                    </View>

                                    <View style={styles1.bottomLine}></View>
                                </View>



                            </View>
                    }
                    {/*********************************************************************** */}

                </View>

                <View style={{ marginTop: 20, marginLeft: 20, marginRight: 20, height: 50, backgroundColor: '#EEEEEE' }}>
                    <TouchableOpacity style={{ backgroundColor: '#E4007C', justifyContent: "center", alignItems: 'center', height: 50 }}
                        onPress={() =>
                            this.checkout()
                            // this.props.navigation.navigate('CheckoutPaymentScreen', {
                            // name: this.state.myname,
                            // email: this.state.myEmail,
                            // number: this.state.contactNo
                            // })
                        }>
                        <Text style={{ color: 'white', fontSize: 20, fontFamily: futuraPtMedium }}>CONTINUE TO PAYMENT</Text>
                    </TouchableOpacity>
                </View>












            </ScrollView>

        );
    }
}
