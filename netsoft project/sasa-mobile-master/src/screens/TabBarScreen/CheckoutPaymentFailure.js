import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, ScrollView, TextInput, TouchableOpacity, AsyncStorage, Dimensions } from 'react-native';
import { Container, Header, Content, Button, ListItem, List } from 'native-base';
import styles from '../../styles/styleCheckoutPayment';
import { futuraPtMedium } from '../../styles/styleText'


const backgroundColor = '#fff'

export default class CheckoutPaymentFailure extends Component {

  static navigationOptions = ({
    title: 'Payment Failed',
    headerLeft: null,
    headerStyle: {
      backgroundColor: 'black'
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontSize: 22,
      fontFamily: futuraPtMedium,
      fontWeight: '500'
    },
  })

  constructor(props) {
    super(props);
    this.state = {
        data: {},

    }
}

  componentDidMount = async () => {
    this.setState({
      data: this.props.navigation.getParam('data')
    })

    // await AsyncStorage.removeItem('cart');
    // await AsyncStorage.removeItem('location');
  }

  render() {
    return (
      <ScrollView style={{ backgroundColor: backgroundColor }}>


        <View style={styles.styleTop}>
          <Image source={require('../../assets/icon_checkmark.png')} />
          {/* <Text style={styles.textHeading} > Thank You!</Text> */}
          <View style={styles.optionView}>
            <Text style={styles.nameText} > Payment Proccess Failed. Please contact Us.</Text>
          </View>

        </View>

        <View style={styles.nameBottomLine}></View>



        <View style={styles.styleMsg}>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('HomeScreen')}>
            <Text style={styles.textContinueShopping} >
              CONTINUE SHOPPING
                </Text>
          </TouchableOpacity>
        </View>

      </ScrollView>
    );
  }
}
