import React from 'react'
import {View} from 'react-native'

const styles2 = {
    outerCircle: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    innerCircle: {
        overflow: 'hidden',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFF',

    },
    leftWrap: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: 21,
        height: 42
    },
    halfCircle: {
        position: 'absolute',
        top: 0,
        left: 0,
        borderTopRightRadius: 0,
        borderBottomRightRadius: 0,

    },
}

const circle =  (props) => {
    const {size, thick, content, color1, color2} = props;
    const halfSize = size/2;
    const sizeInner = size-thick;

    return(
        <View style={[
            styles2.outerCircle,
            {
                backgroundColor: color1,
                width: size,
                height: size,
                borderRadius: (halfSize)
            }]}>
            <View style={styles2.leftWrap}>
            <View
              style={[
                styles2.halfCircle,
                {
                  backgroundColor: color2,
                  width: halfSize,
                  height: size,
                  borderRadius: halfSize,
                  transform: [
                    { translateX: halfSize / 2 },
                    { rotate: '180deg' },
                    { translateX: -halfSize / 2 },
                  ],
                },
              ]}
            />
            </View>
            <View style={[
                styles2.innerCircle,
                {
                    width: sizeInner,
                    height: sizeInner,
                    borderRadius: (sizeInner/2)
                }
            ]} >
                { content() }
            </View>
        </View>

    )
}

circle.defaultProps = {
    size: 100,
    thick: 30,
    color1: 'red',
    color2: 'blue',
    content: () => {}
}

export default circle;
