import React from 'react'
import { ScrollView } from 'react-native'
import HTML from 'react-native-render-html';

const styles = {
    container: {
        backgroundColor: '#fff',
        padding: 10,
        paddingBottom: 70
    }

}
const html = ({ html }) => {
    return(
        <ScrollView style={ styles.container } >
            <HTML html={ html } />
        </ScrollView>
    )
}

html.defaultProps = {
    html: "<html></html>"
}

export default html;
