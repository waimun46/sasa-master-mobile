import {StyleSheet, Dimensions} from 'react-native';
import { futuraPtMedium, futuraPtBook } from './styleText'

const dimWidth= Dimensions.get('window').width
const dimHeight= Dimensions.get('window').height
const styles=StyleSheet.create({
    container:{
        backgroundColor:'#fff'
    },
    view:{
        marginTop: 10,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    text:{
        fontSize:16,
    fontWeight:'300',
    fontFamily:'future-lt-book'
    },
    view1:{
        flexDirection:'row',
        justifyContent:'space-between',
        marginLeft:50,
        marginRight:30,
        marginTop:0
    },
    text1:{
        fontSize:40,
    fontWeight:'bold',
    color:'black'
    },
    textTimer:{
        fontSize:16,
    fontWeight:'bold',
    color:'black'
    },
    trContainer:{
        marginTop:20,
    marginLeft:20,
    marginRight:20,
    height:50
    },
    takereward:{
        backgroundColor:'#E4007C',
        justifyContent:"center",
        alignItems:'center',
        height:50
    },
    trText:{
        color:'white',
    fontSize:18,
    fontFamily:futuraPtMedium,
    fontWeight:'500'
    },
    discText:{
        fontSize:22,
        color:'#E4007C',
        marginLeft:20,
        marginRight:20,
        marginTop:20,
        fontFamily:futuraPtMedium,
        fontWeight:'500'
    },
    dateText:{
        fontSize:18,
        marginRight:20,
        marginLeft:20,
        marginTop:10,
        color:'black',
        fontFamily:futuraPtMedium,

    },
    dateText1:{
        fontSize:18,
        marginRight:20,
        marginLeft:20,
        marginTop:10,
       fontWeight:'300',
        fontFamily:futuraPtBook,
    },
    text2:{
        fontSize:20,
    marginLeft:20,
    color:'#E4007C',
    marginTop:10,
    fontFamily:futuraPtBook,

    },
    text3:{
        fontSize:20,
        marginLeft:20,
        color:'#E4007C',
        marginTop:10,
        fontFamily:futuraPtBook,
    },
    imgPromotion:{
        marginTop:20,
        justifyContent:'center',
        alignItems:'center'
    }

})
export default styles;
