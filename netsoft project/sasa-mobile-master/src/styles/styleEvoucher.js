import {StyleSheet, Dimensions} from 'react-native';
import { futuraPtMedium, futuraPtBook } from './styleText'

const dimWidth= Dimensions.get('window').width
const finalWidth = dimWidth-60
const dimHeight= Dimensions.get('window').height
const buttonColor='#ff4da6'
const textColor='#fff'
const backgroundColor='#fff'
const newBackgroundColor='#C0C0C0'

const styles=StyleSheet.create({

  containerBackground:{
         backgroundColor:backgroundColor
    },

    styleText:{
        marginTop: 6,
        marginLeft:6,
        color:'#000',
        fontSize:18,
    },
    logoImg:{
        width: finalWidth,
         marginTop: 14,
        marginLeft: 14,
        marginRight: 14,
        marginBottom: 10
    },

     viewText:{
        marginTop: 6,
        marginBottom: 10,
        marginLeft:10
    },
    textHeading:{
        color:'#000',
        fontSize:20,
        marginTop: 6,
        marginBottom: 2,
        marginLeft:10,

        fontFamily:futuraPtMedium
    },
    textNormal:{
        color:'#C0C0C0',
        fontSize:15,
        marginTop: 2,

        marginBottom: 12,
        marginLeft:10,
        fontFamily:futuraPtBook
    },
    styleCard:{
        marginTop: 14,
        marginLeft: 14,
        marginRight: 14,
        marginBottom:10,
        borderWidth:0.40,
        borderRadius:4,
        borderColor:'grey'
    },

    viewButton:{
        marginTop:10,
        marginBottom:10,
        marginLeft:10,
        marginRight:10,
    height:40,
    backgroundColor:'#E4007C',
    paddingLeft:10,
    paddingRight:10,
    justifyContent:"center",
    alignItems:'center'
},
    textButton:{
        color:'#FFFFFF',
        fontSize:14,
        fontFamily:futuraPtMedium,

       },

  touch:{
      justifyContent:"center",
      alignItems:'center',
      height:50
  }
});
export default styles;
