import {StyleSheet, Dimensions} from 'react-native';
import { futuraPtMedium, futuraPtBook } from './styleText'

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
const styles=StyleSheet.create({
    container:{
     backgroundColor:'#fff',
      flex:1
    },
    textHeader:{
        color:'#808080',
        marginTop: 16,
        marginLeft: 10,
        fontSize: 18,
    },
    nameText:{
        color:'#777777',
        marginTop: 10,
        marginLeft: 1,
        fontSize: 18,
        fontFamily:futuraPtBook
    },

    nameBottomLine:{
        backgroundColor:'#D3D3D3',
        height:0.50,
        marginRight:20,
        marginLeft:20,
        marginTop:20,

   },



    msgOneText:{
        color:'grey',
        marginTop: 16,
        marginLeft: 20,
        marginRight: 20,
        marginBottom:10,
        fontSize: 18,
        fontFamily:futuraPtBook,
        fontWeight:'300'
    },

     msgTwoText:{
        color:'#C0C0C0',
        marginTop: 16,
        marginLeft: 10,
         marginBottom:10,
        fontSize: 14,
    },


    optionView:{
        flex:1 , flexDirection: 'row' , marginTop:1 , marginBottom:1,
    },
     viewTextTop:{

        marginTop: 1,
        marginBottom: 1,
        marginLeft:10,

    },
     textHeading:{
        marginTop:10,
        textAlignVertical: 'top',
        color:'#000',
        fontSize:24,
        fontFamily:futuraPtMedium

    },
     orderText:{
        color:'#000',
        marginTop: 10,
        marginLeft: 1,
        fontSize: 18,
        fontFamily:futuraPtBook,
        fontWeight:'300'
    },
     textContinueShopping:{
        textAlignVertical: 'top',
        marginTop: 10,
        color:'#E4007C',
        fontSize:20,
        fontFamily:futuraPtBook,
        fontWeight:'300'

    },
    styleTop:{
        justifyContent:'center',alignItems:'center',marginTop:50
    },
    styleMsg:{
        justifyContent:'center',alignItems:'center', marginTop:10
    },
    btnView:{
        marginTop:20,marginLeft:20,marginRight:20,height:50
    },
    styleTouch:{
        backgroundColor:'#E4007C',justifyContent:"center",alignItems:'center',height:50
    },
    btnText:{
        color:'white',fontSize:20,fontFamily:futuraPtMedium
    }

})
export default styles;
