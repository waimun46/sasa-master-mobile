import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, ScrollView, TextInput, TouchableOpacity, FlatList, Dimensions, Alert } from 'react-native';
import styles from '../../styles/styleEvoucher'
import Moment from 'moment';
import PropTypes from 'prop-types';
import Progressbar from 'react-native-loading-spinner-overlay';
import Service from '../../config/Service';
import CanonicalPath from '../../config/CanonicalPath';
import { futuraPtMedium } from '../../styles/styleText'

export default class EvoucherInStore extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'EVoucher',
    headerLeft: null,
    headerStyle: {
      backgroundColor: 'black',

    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontSize: 22,
      fontFamily: futuraPtMedium,
      fontWeight: '500'
    },
  })
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      evouchers: [],
    }

  }
  static propTypes = {
    onPress1: PropTypes.func.isRequired,
  }

  setStateAsync(state) {
    return new Promise((resolve) => {
      this.setState(state, resolve)
    });
  }

  onSuccess = async (filter, json) => {
    this.setStateAsync({ isLoading: false });

    switch (filter) {
      case "EVOUCHER":
        this.setState({
          evouchers: json
        })
        break
    }

  }

  onError = (filter, e) => {
    this.setStateAsync({ isLoading: false });

    alert(e)
  }

  async evoucher_store() {
    await this.setStateAsync({ isLoading: true });

    var data = await {
      "type": "store"
    }

    var params = await {
      contentType: "multipart/form-data",
      Accept: "application/json",
      keepAlive: true,
      method: 'GET',
      useToken: true,
      data: data,
      canonicalPath: CanonicalPath.EVOUCHER
    };

    await Service.request(this.onSuccess, this.onError, "EVOUCHER", params);
  }

  componentDidMount() {
    this.evoucher_store()

  }

  toDetailVoucher( item ){
      this.props.navigation.navigate('BirthdayVoucher', {
          item: item.item,
          isPayment: true
       })
  }

  renderRow = (item) => {

    var dateString = item.item.expiry_date;
    var momentObj = Moment(dateString, 'yyyy-MM-DD HH:mm:ss');
    var momentString = momentObj.format('DD MMMM YYYY');

    return (
      <View style={styles.styleCard}>
        <TouchableOpacity style={{ justifyContent: "center", alignItems: 'center', }} onPress={ () => this.toDetailVoucher( item ) }  >
          <Image style={{ width: '100%', height: 145, resizeMode: 'contain' }}  source={{ uri: item.item.image_url }} />
        </TouchableOpacity>
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <View style={{ flex: 1, flexDirection: 'column' }}>
            <Text style={styles.textHeading} >{item.item.title}</Text>
            <Text style={styles.textNormal} >Valid until {momentString}</Text>
          </View>
          <View>
            {/* <View style={styles.viewButton}> */}
              {/* <TouchableOpacity style={styles.touch}   >
                <Text style={styles.textButton}>USE VOUCHER</Text>
              </TouchableOpacity> */}
            {/* </View> */}
          </View>
        </View>
      </View>
    )
  }

  render() {
    const { onPress1 } = this.props;

    return (
      <ScrollView style={styles.containerBackground}>

        <Progressbar cancelable={true} visible={this.state.isLoading} overlayColor="rgba(0, 0, 0, 0.8)" animation="fade" textContent="" style={{ fontSize: 16, textAlign: 'center', justifyContent: 'center', height: 30, alignItems: 'center' }} />

        {
          (this.state.evouchers.length > 0) ?
            <FlatList
              vertical
              data={this.state.evouchers}
              renderItem={item => this.renderRow(item)}>
            </FlatList>
            :
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
              <Text>No Record</Text>
            </View>

        }

        {/* <View style={styles.styleCard}>

          <TouchableOpacity style={{ justifyContent: "center", alignItems: 'center', }} onPress={() => this.props.navigation.navigate('BirthdayVoucher')}  >
            <Image style={{ width: '100%' }} source={require('../../assets/Pay-with-Points_C2.png')} />

          </TouchableOpacity>

          <View style={{ flex: 1, flexDirection: 'row' }}>

            <View style={{ flex: 1, flexDirection: 'column' }}>
              <Text style={styles.textHeading} >Birthday Voucher</Text>
              <Text style={styles.textNormal} >Valid until 31 Dec 2018</Text>
            </View>

            <View>
              <View style={styles.viewButton}>
                <TouchableOpacity style={styles.touch}   >
                  <Text style={styles.textButton}>USE VOUCHER</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>

        <View style={styles.styleCard}>
          <TouchableOpacity style={{ justifyContent: "center", alignItems: 'center', }} onPress={onPress1} >
            <Image style={{ width: '100%' }} source={require('../../assets/Pay-with-Points_C2.png')} />

          </TouchableOpacity>

          <View style={{ flex: 1, flexDirection: 'row' }}>

            <View style={{ flex: 1, flexDirection: 'column' }}>


              <Text style={styles.textHeading} >Birthday Voucher</Text>
              <Text style={styles.textNormal} >Valid until 31 Dec 2018</Text>
            </View>

            <View>
              <View style={styles.viewButton}>
                <TouchableOpacity style={styles.touch}  >
                  <Text style={styles.textButton}>USE VOUCHER</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.styleCard}>

          <TouchableOpacity style={{ justifyContent: "center", alignItems: 'center', }}   >
            <Image style={{ width: '100%' }} source={require('../../assets/Pay-with-Points_C2.png')} />

          </TouchableOpacity>

          <View style={{ flex: 1, flexDirection: 'row' }}>

            <View style={{ flex: 1, flexDirection: 'column' }}>
              <Text style={styles.textHeading} >Birthday Voucher</Text>
              <Text style={styles.textNormal} >Valid until 31 Dec 2018</Text>
            </View>
            <View>
              <View style={styles.viewButton}>
                <TouchableOpacity style={styles.touch}   >
                  <Text style={styles.textButton}>USE VOUCHER</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View> */}



      </ScrollView>
    );
  }
}
