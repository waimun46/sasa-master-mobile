import React,{Component} from "react";
import { Header } from "react-navigation";
import { View, Platform ,Image,TouchableOpacity,Alert} from "react-native";
import PropTypes from 'prop-types';
import { DrawerActions } from 'react-navigation';
import SideMenu from 'react-native-side-menu';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/FontAwesome5';
import HomeMenu from '../SideMenuScreen/HomeMenu'
import { futuraPtMedium } from '../../styles/styleText'

 class CustomHeader extends Component {
   constructor(props){
     super(props);
     this.state={
       isVisible:false
     }
   }
   onClick=()=>{

     this.setState({
      isVisible:!this.state.isVisible
     })
   }
   openDrawer = () => {
    this.props.navigation.dispatch(DrawerActions.openDrawer());
  }
   click(){
    const menu = <HomeMenu click={this.props}/>;
    // Alert.alert('fgvff')
     return(

      <SideMenu
   menu={menu}
   isOpen={this.state.isOpen}

 />

     );
   }
    static navigationOptions = {
        headerStyle: {
           backgroundColor: '#0080ff',
           height:0
         },
        };
    static propTypes = {

         onPress:PropTypes.func.isRequired,
        onPress1:PropTypes.func.isRequired,
        title : PropTypes.string.isRequired,
        date : PropTypes.string.isRequired,
        time : PropTypes.string.isRequired,
      }
      render(){

        const { onPress,onPress1, } = this.props;
    return (
        <View style={{flexDirection:'row', justifyContent:'space-between',backgroundColor:'black',height:50}}>



        <TouchableOpacity onPress={this.props}>
        {/* <Image

        style={{
        width: 25,
        height:20,
        marginTop:15,
        marginLeft:10
       }}
        source={require('../../assets/toggle.png')}
        /> */}
         <Icon style={{height:30,width:30,marginLeft:10,marginTop:15,}}name="navicon" size={25}  color="white" />
        </TouchableOpacity>
         <Image

      style={{
      width: 80,
      height:30,
      marginTop:10
     }}
      source={require('../../assets/sasalogo1.png')}
      />
      <TouchableOpacity onPress={onPress1}>
       {/* <Image

      style={{
      width: 30,
      height:30,
      marginTop:10,
      marginRight:10
     }}
      source={require('../../assets/user-icon.png')}
      /> */}
         {/* <Icon2 style={{height:30,width:30,marginLeft:10,marginTop:15,marginRight:10}}name="user-circle" size={25}  color="white" /> */}
         <Image

      style={{
      width: 25,
      height:25,
      marginTop:15,
      marginLeft:10
     }}
      source={require('../../assets/sasahead.png')}
      />
      </TouchableOpacity>
        </View>
    );
  }
}

  export default CustomHeader;
