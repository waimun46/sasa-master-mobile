import Dialog, { DialogContent,DialogButton,SlideAnimation } from 'react-native-popup-dialog';
import React,{Component} from 'react';
import { Button,View,StyleSheet,Text,TouchableOpacity,Dimensions ,Image} from 'react-native'
export default class MyDilogBox extends Component{
    constructor(props){
        super(props);
        this.state={
            visible:false
          }
}
render(){
    return(

<View style={styles.container}>
  <Button
    title="Show Dialog"
    onPress={() => {
      this.setState({ visible: true });
    }}
  />
  <Dialog
    visible={this.state.visible}
    dialogAnimation={new SlideAnimation({
        slideFrom: 'bottom',
      })}
    rounded={false}
  width={300}
    height={300}
    onTouchOutside={() => {
      this.setState({ visible: false });
    }}
  >
    <DialogContent>
      <View style={{backgroundColor:'#fff'}}>
      <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:10}}> 
          <Text style={{fontSize:18,color:'black'}}>ADD TO CART </Text>
          <Image style={{height:25,width:25,marginTop:10}}ource={require('../../assets/cross.png')}/>
          </View>
          <Image style={{width:250}} source={require('../../assets/popimage1.png')}/>
          <Text style={{fontSize:13,marginTop:10}}>SubTotal</Text>
             
             
         
          <Text style={{fontSize:15,color:'black',marginTop:10}}>RM 118.8</Text>
          <View style={{marginTop:20,marginLeft:10,marginRight:10,height:50}}>
                    <TouchableOpacity   style={{backgroundColor:'#E4007C',justifyContent:"center",alignItems:'center',height:50}}
                    onPress={ this.validUser} >
                        <Text style={{color:'white',fontSize:18,}}>CHECKOUT</Text>
                    </TouchableOpacity>
                </View>
             
         <View style={{justifyContent:'center',alignItems:'center',marginTop:10}}>
          <Text style={{fontSize:16,color:'#E4007C'}}> Continiue to shoping </Text>
          </View>
              
          </View>
    </DialogContent>
  </Dialog>
</View>
    )
}}
const styles=StyleSheet.create({
    container:{
     marginTop:10,
     backgroundColor:'#fff',
    },
    text1:{
        fontSize:18,
        fontWeight:'bold',
        color:'black'
    },
    line1Text:{
        flexDirection:'row',
    color:'#4A4646',
        marginTop:15

    }
    

})