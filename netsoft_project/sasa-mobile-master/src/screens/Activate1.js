

import { createStackNavigator, StackActions, NavigationActions} from 'react-navigation'; // Version can be specified in package.json
import CheckBox from 'react-native-checkbox';
import { Col, Row, Grid } from "react-native-easy-grid";
import React, {Component} from 'react';
import styles from '../styles/ActivationStyle';
import {Platform, StyleSheet, Text, View,Image,ScrollView,TextInput,TouchableOpacity,ImageBackground,Dimensions} from 'react-native';
import { Container, Header, Content, Button,ListItem,List  } from 'native-base';
import { futuraPtMedium } from '../styles/styleText'

const dimWidth= Dimensions.get('window').width
const dimHeight= Dimensions.get('window').height

export default class Activate1 extends Component {
  static navigationOptions=({
      title:'Active My Account ',
    headerLeft:null,
    headerStyle:{
        backgroundColor:'black'
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
        fontSize: 22,
        fontFamily:futuraPtMedium,
        fontWeight:'500'
      },
})



constructor(props){
    super(props);
    this.state={
        check:true,
        selectedIndex:0,

        useremail:'',
        userPassowrd: '',
        rePassowrd: '',
        emailError:false,
        passwordError: false,
        rePassowrdError: false

        }
}


validation=()=>{
if(this.state.useremail === ''){
    this.setState({emailError: true})
}
else {
    this.setState({emailError: false})
}
 if(this.state.userPassowrd === ''){

    this.setState({passwordError: true})
}
else {

    this.setState({passwordError: false})
}
 if(this.state.rePassowrd === ''){
    this.setState({rePassowrdError: true})

}
else {
    this.setState({rePassowrdError: false})

}
if(this.state.password === this.state.rePassowrd){

}
else{

    this.props.navigation.navigate('HomeScreen')

}

}

isEmail(value){
    this.setState({
        myEmail: value.replace(/\s/g, '')
    })
 }


 render() {
    return (
       <ScrollView style={{flex:1,backgroundColor:'#fff'}}>
            <View>
                {this.state.emailError === true &&  <Text  style={styles.errorFieldtext}> Email address</Text>}
                {this.state.emailError !== true &&  <Text  style={styles.fieldtext}> Email address</Text>}

                <TextInput  style={styles.email} placeholder='email'
                onChangeText={myEmail => this.isEmail(myEmail)}
                value={this.state.myEmail}
                />

              <View style={styles.lineDown}></View>

               {this.state.passwordError ===true &&  <Text  style={styles.errorFieldtext}>Password</Text>}
               {this.state.passwordError !== true &&  <Text  style={styles.fieldtext}>Password</Text>}

                <TextInput secureTextEntry={true}  style={styles.email} placeholder='Password'
                onChangeText={password => this.setState({ password })}
                />

                <View style={styles.lineDown}></View>
                {this.state.rePassowrdError ===true &&  <Text  style={styles.errorFieldtext}>Confirm Password</Text>}
                {this.state.rePassowrdError !==true &&  <Text  style={styles.fieldtext}>Confirm Password</Text>}


                <TextInput secureTextEntry={true}  style={styles.email} placeholder='Confirm Password'
                onChangeText={confirmPassowrd => this.setState({ confirmPassowrd })}
                />

                <View style={styles.lineDown}></View>

            </View>


            <View style={styles.viewButton}>
                <TouchableOpacity   style={styles.touch}
                     onPress={ this.validation} >
                    <Text style={styles.textButton}>Done</Text>
                </TouchableOpacity>
            </View>
        </ScrollView>
    );
  }
}
