import {StyleSheet, Dimensions} from 'react-native';
import { futuraPtMedium, futuraPtBook } from './styleText'

const dimWidth= Dimensions.get('window').width
const finalWidth = dimWidth-60
const dimHeight= Dimensions.get('window').height
const buttonColor='#ff4da6'
const textColor='#fff'
const backgroundColor='#fff'
const newBackgroundColor='#C0C0C0'

const styles=StyleSheet.create({
    container:{
        height:dimHeight
    },
  containerBackground:{
         backgroundColor:backgroundColor
    },

    styleText:{
        marginTop: 6,
        marginLeft:6,
        color:'#000',
        fontSize:18,
    },
    logoImg:{
        width: '100%',



        marginBottom: 10
    },

     viewText:{
        marginTop: 6,
        marginBottom: 10,
        marginLeft:10
    },
    textHeading:{
        color:'#000000',
        fontSize:22,
        marginTop: 6,
        marginBottom: 2,
        marginLeft:10,
        fontWeight:'400',
        fontFamily:futuraPtMedium
    },
    textNormal:{
        color:'darkgrey',
        fontSize:16,
        marginTop: 5,
        marginBottom: 12,
        marginLeft:10,
        fontFamily:futuraPtBook,
        fontWeight:'300'
    },
    styleCard:{
        marginTop: 14,
        marginLeft: 14,
        marginRight: 14,
        marginBottom:10
    }
});
export default styles;
