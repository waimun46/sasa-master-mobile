import {StyleSheet, Dimensions} from 'react-native';
import { futuraPtMedium, futuraPtBook } from './styleText'

const dimWidth= Dimensions.get('window').width
const dimHeight= Dimensions.get('window').height
const buttonColor='#E4007C'
const textColor='#fff'
const backgroundColor='#fff'

const styles=StyleSheet.create({
    container:{
        height:dimHeight
    },
  containerBackground:{
        backgroundColor:backgroundColor
    },
    viewNewPsw:{
        marginTop: 20,
        marginLeft:20
    },
    textNewPsw:{

        justifyContent:"center",
        alignItems:'center',
        fontFamily:futuraPtMedium,
        fontSize:16,
        fontWeight:'300'
    },
    inputNewPsw:{
        color:'#000',
        width:dimWidth*80/100,
        justifyContent:"center",
        alignItems:'center'
    },
    lineDown:{
        borderBottomColor:'#D3D3D3',
        borderBottomWidth:1,
        backgroundColor:'#000',
        height:0.5,
        marginLeft:20,
        marginRight:20,
        marginTop:4
    },
    pswError:{
        color:'red',
        fontSize:15,
        marginLeft:15,
        marginTop:10,
        fontFamily:futuraPtBook,
        fontWeight:'300'
    },

    viewConfirmPsw:{
        marginTop: 20,
        marginLeft:20
    },
    textConfirmPsw:{

        justifyContent:"center",
        alignItems:'center',
        fontFamily:futuraPtMedium,
        fontSize:16,
        fontWeight:'300'
    },
    inputConfirmPsw:{
        color:'#000',
        width:dimWidth*80/100,
        justifyContent:"center",
        alignItems:'center'
    },
    conFirError:{
        color:'red',
        fontSize:15,
        marginLeft:15,
        marginTop:10,
        fontFamily:futuraPtBook,
        fontWeight:'300'
    },


    viewButton:{
        marginTop:20,
        marginLeft:20,
        marginRight:20,
        height:50,
        backgroundColor:buttonColor
    },
    textButton:{
        color:textColor,
        fontSize:20,
        fontFamily:futuraPtMedium
    },

  touch:{
      justifyContent:"center",
      alignItems:'center',
      height:50
  }
});
export default styles;
